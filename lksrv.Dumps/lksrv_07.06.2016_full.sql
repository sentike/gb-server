--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-06-08 02:25:16

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 18 (class 2615 OID 16619)
-- Name: Achievements; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Achievements";


ALTER SCHEMA "Achievements" OWNER TO postgres;

--
-- TOC entry 12 (class 2615 OID 16620)
-- Name: League; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "League";


ALTER SCHEMA "League" OWNER TO postgres;

--
-- TOC entry 17 (class 2615 OID 49807)
-- Name: Matches; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Matches";


ALTER SCHEMA "Matches" OWNER TO postgres;

--
-- TOC entry 13 (class 2615 OID 16621)
-- Name: Players; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Players";


ALTER SCHEMA "Players" OWNER TO postgres;

--
-- TOC entry 14 (class 2615 OID 16622)
-- Name: Store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Store";


ALTER SCHEMA "Store" OWNER TO postgres;

--
-- TOC entry 16 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2628 (class 0 OID 0)
-- Dependencies: 16
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 15 (class 2615 OID 16623)
-- Name: vrs; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA vrs;


ALTER SCHEMA vrs OWNER TO postgres;

SET search_path = "Achievements", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 237 (class 1259 OID 91111)
-- Name: AchievementContainer; Type: TABLE; Schema: Achievements; Owner: postgres
--

CREATE TABLE "AchievementContainer" (
    "AchievementTypeId" smallint NOT NULL
);


ALTER TABLE "AchievementContainer" OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 91118)
-- Name: AchievementInstance; Type: TABLE; Schema: Achievements; Owner: postgres
--

CREATE TABLE "AchievementInstance" (
    "AchievementBonusId" integer NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "Amount" integer DEFAULT 0 NOT NULL,
    "BonusPackId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "AchievementInstance" OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 91116)
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE; Schema: Achievements; Owner: postgres
--

CREATE SEQUENCE "AchievementInstance_AchievementBonusId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AchievementInstance_AchievementBonusId_seq" OWNER TO postgres;

--
-- TOC entry 2630 (class 0 OID 0)
-- Dependencies: 238
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE OWNED BY; Schema: Achievements; Owner: postgres
--

ALTER SEQUENCE "AchievementInstance_AchievementBonusId_seq" OWNED BY "AchievementInstance"."AchievementBonusId";


SET search_path = "League", pg_catalog;

--
-- TOC entry 208 (class 1259 OID 16647)
-- Name: LeagueEntity; Type: TABLE; Schema: League; Owner: postgres
--

CREATE TABLE "LeagueEntity" (
    "LeagueId" uuid NOT NULL,
    "Cash_Money" smallint NOT NULL,
    "Cash_Donate" smallint NOT NULL,
    "Info_Name" character varying(20) NOT NULL,
    "Info_Abbr" character varying(5) NOT NULL,
    "Info_FoundedDate" timestamp without time zone NOT NULL,
    "Info_AccessType" smallint NOT NULL,
    "Info_JoinPrice" smallint NOT NULL
);


ALTER TABLE "LeagueEntity" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16650)
-- Name: LeagueMemberEntity; Type: TABLE; Schema: League; Owner: postgres
--

CREATE TABLE "LeagueMemberEntity" (
    "LeagueId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Access" smallint NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "LeagueMemberEntity" OWNER TO postgres;

SET search_path = "Matches", pg_catalog;

--
-- TOC entry 221 (class 1259 OID 49808)
-- Name: MatchEntity; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchEntity" (
    "MatchId" uuid NOT NULL,
    "NodeId" uuid NOT NULL,
    "GameModeTypeId" smallint NOT NULL,
    "GameMapTypeId" smallint NOT NULL,
    "Created" timestamp without time zone NOT NULL,
    "Started" timestamp without time zone NOT NULL,
    "Finished" timestamp without time zone NOT NULL,
    "WinnerTeamId" uuid
);


ALTER TABLE "MatchEntity" OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 49819)
-- Name: MatchMember; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchMember" (
    "MemberId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "TeamId" uuid NOT NULL,
    "Level" smallint DEFAULT 1 NOT NULL,
    "MatchId" uuid NOT NULL,
    "Award_Money" smallint DEFAULT 0 NOT NULL,
    "Award_Experience" smallint DEFAULT 0 NOT NULL,
    "Award_Reputation" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "MatchMember" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 49813)
-- Name: MatchTeam; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchTeam" (
    "TeamId" uuid NOT NULL,
    "MatchId" uuid NOT NULL
);


ALTER TABLE "MatchTeam" OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 49825)
-- Name: MemberAchievements; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MemberAchievements" (
    "AchievementId" uuid NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "AchievementCount" smallint NOT NULL,
    "MemberId" uuid NOT NULL
);


ALTER TABLE "MemberAchievements" OWNER TO postgres;

SET search_path = "Players", pg_catalog;

--
-- TOC entry 211 (class 1259 OID 16658)
-- Name: FriendEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "FriendEntity" (
    "MemberId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FriendId" uuid NOT NULL,
    "State" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FriendEntity" OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 115889)
-- Name: InstanceOfInstalledAddon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfInstalledAddon" (
    "AddonId" uuid NOT NULL,
    "ItemId" uuid,
    "AddonSlot" smallint NOT NULL,
    "ContainerId" uuid NOT NULL
);


ALTER TABLE "InstanceOfInstalledAddon" OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 107659)
-- Name: InstanceOfItemAddon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfItemAddon" (
    "ItemId" uuid NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value smallint DEFAULT 0,
    modification_2_value smallint DEFAULT 0,
    modification_3_value smallint DEFAULT 0,
    modification_4_value smallint DEFAULT 0,
    modification_5_value smallint DEFAULT 0
);


ALTER TABLE "InstanceOfItemAddon" OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 107654)
-- Name: InstanceOfItemSkin; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfItemSkin" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfItemSkin" OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 91015)
-- Name: InstanceOfPlayerArmour; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerArmour" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfPlayerArmour" OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16654)
-- Name: InstanceOfPlayerCharacter; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerCharacter" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "InstanceOfPlayerCharacter" OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 90987)
-- Name: InstanceOfPlayerItem; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerItem" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "ModelId" smallint NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value real DEFAULT 0 NOT NULL,
    modification_2_value real DEFAULT 0 NOT NULL,
    modification_3_value real DEFAULT 0 NOT NULL,
    modification_4_value real DEFAULT 0 NOT NULL,
    modification_5_value real DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "InstanceOfPlayerItem" OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 91032)
-- Name: InstanceOfPlayerWeapon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerWeapon" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfPlayerWeapon" OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 91170)
-- Name: InstanceOfTargetItem; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfTargetItem" (
    "ItemId" uuid NOT NULL,
    "TargetPlayerItemId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "InstanceOfTargetItem" OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 91126)
-- Name: PlayerAchievement; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerAchievement" (
    "PlayerAchievementId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "LastAchievementInstanceId" smallint,
    "Amount" smallint NOT NULL
);


ALTER TABLE "PlayerAchievement" OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16662)
-- Name: PlayerEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerName" character varying(24) NOT NULL,
    "Experience_Level" smallint DEFAULT 0 NOT NULL,
    "Experience_Experience" bigint DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "PremiumEndDate" timestamp without time zone,
    "LastActivitiDate" timestamp without time zone DEFAULT now() NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL,
    "CurrentFractionTypeId" smallint DEFAULT 1 NOT NULL,
    "Reputation_Keepers" bigint DEFAULT 0 NOT NULL,
    "Reputation_Keepers_Friend" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT_Friend" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "PlayerEntity" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16632)
-- Name: PlayerProfileEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerProfileEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CharacterId" uuid NOT NULL,
    "HelmetId" uuid,
    "MaskId" uuid,
    "ArmourId" uuid,
    "BackpackId" uuid,
    "GlovesId" uuid,
    "PantsId" uuid,
    "BootsId" uuid,
    "PrimaryWeaponId" uuid,
    "SecondaryWeaponId" uuid,
    "PrimaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "PrimaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "PlayerId" uuid NOT NULL,
    "IsEnabled" boolean DEFAULT true NOT NULL,
    "Name" character varying(24),
    "IsUnlocked" boolean DEFAULT false NOT NULL,
    CONSTRAINT "PrimaryWeaponAmmo" CHECK ((("PrimaryWeaponAmmo_Primary" + "PrimaryWeaponAmmo_Secondary") > 0))
);


ALTER TABLE "PlayerProfileEntity" OWNER TO postgres;

SET search_path = "Store", pg_catalog;

--
-- TOC entry 228 (class 1259 OID 90937)
-- Name: AbstractItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AbstractItemInstance" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 1 NOT NULL,
    "IsAvalible" boolean DEFAULT true NOT NULL,
    "DefaultSkinId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "AbstractItemInstance" OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 107613)
-- Name: AddonItemContainer; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AddonItemContainer" (
    "ContainerId" integer NOT NULL,
    "ModelId" smallint NOT NULL,
    "ItemId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    "ModelCategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AddonItemContainer" OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 107611)
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE; Schema: Store; Owner: postgres
--

CREATE SEQUENCE "AddonItemContainer_ContainerId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AddonItemContainer_ContainerId_seq" OWNER TO postgres;

--
-- TOC entry 2631 (class 0 OID 0)
-- Dependencies: 243
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE OWNED BY; Schema: Store; Owner: postgres
--

ALTER SEQUENCE "AddonItemContainer_ContainerId_seq" OWNED BY "AddonItemContainer"."ContainerId";


--
-- TOC entry 242 (class 1259 OID 107606)
-- Name: AddonItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AddonItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AddonItemInstance" OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 91071)
-- Name: AmmoItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AmmoItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AmmoItemInstance" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 90955)
-- Name: ArmourItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "ArmourItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "ArmourItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 1))
);


ALTER TABLE "ArmourItemInstance" OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 90980)
-- Name: CharacterItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "CharacterItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "CharacterItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 5))
);


ALTER TABLE "CharacterItemInstance" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16682)
-- Name: FractionEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "FractionEntity" (
    "FractionId" smallint NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" smallint DEFAULT 0 NOT NULL,
    "Discount_Ammo" smallint DEFAULT 0 NOT NULL,
    "Discount_Weapon" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FractionEntity" OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 90974)
-- Name: SkinItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "SkinItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "SkinItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 7))
);


ALTER TABLE "SkinItemInstance" OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 90967)
-- Name: WeaponItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "WeaponItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "WeaponItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 0))
);


ALTER TABLE "WeaponItemInstance" OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- TOC entry 225 (class 1259 OID 66321)
-- Name: GlobalChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "GlobalChatMessage" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "EditorId" uuid,
    "LanguageId" smallint NOT NULL
);


ALTER TABLE "GlobalChatMessage" OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 66326)
-- Name: PrivateChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "PrivateChatMessage" (
    "MessageId" uuid NOT NULL,
    "MessageDataId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL
);


ALTER TABLE "PrivateChatMessage" OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 66331)
-- Name: PrivateChatMessageData; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "PrivateChatMessageData" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "ReceiverId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "IsReaded" boolean NOT NULL
);


ALTER TABLE "PrivateChatMessageData" OWNER TO postgres;

SET search_path = vrs, pg_catalog;

--
-- TOC entry 214 (class 1259 OID 16734)
-- Name: Deposits; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Deposits" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserId" uuid NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "Value" bigint DEFAULT 0 NOT NULL,
    "Currency" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "Deposits" OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16740)
-- Name: Roles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Roles" (
    "Id" uuid NOT NULL,
    "Name" character varying(256) NOT NULL
);


ALTER TABLE "Roles" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16743)
-- Name: UserClaims; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserClaims" (
    "Id" integer NOT NULL,
    "ClaimType" character varying(256),
    "ClaimValue" character varying(256),
    "UserId" uuid NOT NULL
);


ALTER TABLE "UserClaims" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16749)
-- Name: UserClaims_Id_seq; Type: SEQUENCE; Schema: vrs; Owner: postgres
--

CREATE SEQUENCE "UserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "UserClaims_Id_seq" OWNER TO postgres;

--
-- TOC entry 2632 (class 0 OID 0)
-- Dependencies: 217
-- Name: UserClaims_Id_seq; Type: SEQUENCE OWNED BY; Schema: vrs; Owner: postgres
--

ALTER SEQUENCE "UserClaims_Id_seq" OWNED BY "UserClaims"."Id";


--
-- TOC entry 218 (class 1259 OID 16751)
-- Name: UserLogins; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserLogins" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "LoginProvider" character varying(128) NOT NULL,
    "ProviderKey" character varying(128) NOT NULL
);


ALTER TABLE "UserLogins" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16755)
-- Name: UserRoles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserRoles" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RoleId" uuid NOT NULL
);


ALTER TABLE "UserRoles" OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16759)
-- Name: Users; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Users" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserName" character varying(256) NOT NULL,
    "PasswordHash" character varying(256),
    "SecurityStamp" uuid,
    "Email" character varying(256) DEFAULT NULL::character varying,
    "EmailConfirmed" boolean DEFAULT false NOT NULL,
    "PhoneNumber" character varying(32),
    "PhoneNumberConfirmed" boolean DEFAULT false NOT NULL,
    "TwoFactorEnabled" boolean DEFAULT false NOT NULL,
    "LockoutEndDateUtc" timestamp without time zone,
    "LockoutEnabled" boolean DEFAULT false NOT NULL,
    "AccessFailedCount" smallint DEFAULT 0 NOT NULL,
    "PlayerEntityId" bigint,
    "Balance" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "Users" OWNER TO postgres;

SET search_path = "Achievements", pg_catalog;

--
-- TOC entry 2308 (class 2604 OID 91121)
-- Name: AchievementBonusId; Type: DEFAULT; Schema: Achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementInstance" ALTER COLUMN "AchievementBonusId" SET DEFAULT nextval('"AchievementInstance_AchievementBonusId_seq"'::regclass);


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2313 (class 2604 OID 107616)
-- Name: ContainerId; Type: DEFAULT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer" ALTER COLUMN "ContainerId" SET DEFAULT nextval('"AddonItemContainer_ContainerId_seq"'::regclass);


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2266 (class 2604 OID 16773)
-- Name: Id; Type: DEFAULT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"UserClaims_Id_seq"'::regclass);


SET search_path = "Achievements", pg_catalog;

--
-- TOC entry 2613 (class 0 OID 91111)
-- Dependencies: 237
-- Data for Name: AchievementContainer; Type: TABLE DATA; Schema: Achievements; Owner: postgres
--

INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (0);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (1);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (2);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (3);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (4);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (5);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (6);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (7);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (8);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (9);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (10);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (11);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (12);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (13);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (14);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (15);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (16);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (17);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (18);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (19);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (20);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (21);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (22);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (23);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (24);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (25);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (26);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (27);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (28);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (29);


--
-- TOC entry 2615 (class 0 OID 91118)
-- Dependencies: 239
-- Data for Name: AchievementInstance; Type: TABLE DATA; Schema: Achievements; Owner: postgres
--

INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (13, 0, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (14, 0, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (15, 0, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (16, 0, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (17, 0, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (18, 0, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (19, 0, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (20, 1, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (21, 1, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (22, 1, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (23, 1, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (24, 1, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (25, 1, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (26, 1, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (27, 2, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (28, 2, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (29, 2, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (30, 2, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (31, 2, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (32, 2, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (33, 2, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (34, 3, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (35, 3, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (36, 3, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (37, 3, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (38, 3, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (39, 3, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (40, 3, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (41, 4, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (42, 4, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (43, 4, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (44, 4, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (45, 4, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (46, 4, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (47, 4, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (48, 5, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (49, 5, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (50, 5, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (51, 5, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (52, 5, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (53, 5, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (54, 5, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (55, 6, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (56, 6, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (57, 6, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (58, 6, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (59, 6, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (60, 6, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (61, 6, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (62, 7, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (63, 7, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (64, 7, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (65, 7, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (66, 7, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (67, 7, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (68, 7, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (69, 8, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (70, 8, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (71, 8, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (72, 8, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (73, 8, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (74, 8, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (75, 8, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (76, 9, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (77, 9, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (78, 9, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (79, 9, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (80, 9, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (81, 9, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (82, 9, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (83, 10, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (84, 10, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (85, 10, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (86, 10, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (87, 10, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (88, 10, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (89, 10, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (90, 11, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (91, 11, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (92, 11, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (93, 11, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (94, 11, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (95, 11, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (96, 11, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (97, 12, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (98, 12, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (99, 12, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (100, 12, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (101, 12, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (102, 12, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (103, 12, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (104, 13, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (105, 13, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (106, 13, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (107, 13, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (108, 13, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (109, 13, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (110, 13, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (111, 14, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (112, 14, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (113, 14, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (114, 14, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (115, 14, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (116, 14, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (117, 14, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (118, 15, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (119, 15, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (120, 15, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (121, 15, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (122, 15, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (123, 15, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (124, 15, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (125, 16, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (126, 16, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (127, 16, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (128, 16, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (129, 16, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (130, 16, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (131, 16, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (132, 17, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (133, 17, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (134, 17, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (135, 17, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (136, 17, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (137, 17, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (138, 17, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (139, 18, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (140, 18, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (141, 18, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (142, 18, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (143, 18, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (144, 18, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (145, 18, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (146, 19, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (147, 19, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (148, 19, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (149, 19, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (150, 19, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (151, 19, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (152, 19, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (153, 20, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (154, 20, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (155, 20, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (156, 20, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (157, 20, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (158, 20, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (159, 20, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (160, 21, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (161, 21, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (162, 21, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (163, 21, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (164, 21, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (165, 21, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (166, 21, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (167, 22, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (168, 22, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (169, 22, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (170, 22, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (171, 22, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (172, 22, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (173, 22, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (174, 23, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (175, 23, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (176, 23, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (177, 23, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (178, 23, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (179, 23, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (180, 23, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (181, 24, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (182, 24, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (183, 24, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (184, 24, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (185, 24, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (186, 24, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (187, 24, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (188, 25, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (189, 25, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (190, 25, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (191, 25, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (192, 25, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (193, 25, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (194, 25, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (195, 26, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (196, 26, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (197, 26, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (198, 26, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (199, 26, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (200, 26, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (201, 26, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (202, 27, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (203, 27, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (204, 27, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (205, 27, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (206, 27, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (207, 27, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (208, 27, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (209, 28, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (210, 28, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (211, 28, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (212, 28, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (213, 28, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (214, 28, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (215, 28, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (216, 29, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (217, 29, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (218, 29, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (219, 29, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (220, 29, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (221, 29, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (222, 29, 100000, 0);


--
-- TOC entry 2633 (class 0 OID 0)
-- Dependencies: 238
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE SET; Schema: Achievements; Owner: postgres
--

SELECT pg_catalog.setval('"AchievementInstance_AchievementBonusId_seq"', 222, true);


SET search_path = "League", pg_catalog;

--
-- TOC entry 2584 (class 0 OID 16647)
-- Dependencies: 208
-- Data for Name: LeagueEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--

INSERT INTO "LeagueEntity" ("LeagueId", "Cash_Money", "Cash_Donate", "Info_Name", "Info_Abbr", "Info_FoundedDate", "Info_AccessType", "Info_JoinPrice") VALUES ('ff0ec814-a947-4dfb-9fb5-cc3d266d774b', 10, 100, 'VRS PRO', 'VRS', '2016-02-10 23:33:16.507', 1, 1000);


--
-- TOC entry 2585 (class 0 OID 16650)
-- Dependencies: 209
-- Data for Name: LeagueMemberEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--



SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2597 (class 0 OID 49808)
-- Dependencies: 221
-- Data for Name: MatchEntity; Type: TABLE DATA; Schema: Matches; Owner: postgres
--



--
-- TOC entry 2599 (class 0 OID 49819)
-- Dependencies: 223
-- Data for Name: MatchMember; Type: TABLE DATA; Schema: Matches; Owner: postgres
--



--
-- TOC entry 2598 (class 0 OID 49813)
-- Dependencies: 222
-- Data for Name: MatchTeam; Type: TABLE DATA; Schema: Matches; Owner: postgres
--



--
-- TOC entry 2600 (class 0 OID 49825)
-- Dependencies: 224
-- Data for Name: MemberAchievements; Type: TABLE DATA; Schema: Matches; Owner: postgres
--



SET search_path = "Players", pg_catalog;

--
-- TOC entry 2587 (class 0 OID 16658)
-- Dependencies: 211
-- Data for Name: FriendEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2623 (class 0 OID 115889)
-- Dependencies: 247
-- Data for Name: InstanceOfInstalledAddon; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2622 (class 0 OID 107659)
-- Dependencies: 246
-- Data for Name: InstanceOfItemAddon; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2621 (class 0 OID 107654)
-- Dependencies: 245
-- Data for Name: InstanceOfItemSkin; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('05f2f1b7-6dea-4db0-81d2-44ecea169c82');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('2534a1c1-0654-48b5-850f-c0d40fbe1307');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('2b01eea3-21d9-4f05-94bf-567c46fab806');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('3b2918f2-a130-459c-86e3-b3dc28467919');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('8f2c02a0-3bb4-44b3-9e29-49f7eaa1245e');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('9571e3f1-f35f-4a65-86e5-dff7e42ac69c');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('9615a965-17bd-45fb-b23a-62275b8211a9');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('ca774074-7e9e-4747-bef9-01ce43d93a1d');


--
-- TOC entry 2610 (class 0 OID 91015)
-- Dependencies: 234
-- Data for Name: InstanceOfPlayerArmour; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerArmour" ("ItemId") VALUES ('2e7d0dfb-f1bb-4f9e-b52a-940d172bf03d');
INSERT INTO "InstanceOfPlayerArmour" ("ItemId") VALUES ('8ac87daa-fa80-42c6-b23b-fa63a2e2d24b');


--
-- TOC entry 2586 (class 0 OID 16654)
-- Dependencies: 210
-- Data for Name: InstanceOfPlayerCharacter; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerCharacter" ("ItemId") VALUES ('43363cf8-9dfd-45ca-871b-45c6805b77af');
INSERT INTO "InstanceOfPlayerCharacter" ("ItemId") VALUES ('ed789b81-9288-4b1f-960b-6a1f9b533991');


--
-- TOC entry 2609 (class 0 OID 90987)
-- Dependencies: 233
-- Data for Name: InstanceOfPlayerItem; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('2e7d0dfb-f1bb-4f9e-b52a-940d172bf03d', '45f7df6f-1ad8-411f-9a31-350cedcde4d4', 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('43363cf8-9dfd-45ca-871b-45c6805b77af', '45f7df6f-1ad8-411f-9a31-350cedcde4d4', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('52d378f8-f182-4923-bab8-bc5d2d8aa4a9', '45f7df6f-1ad8-411f-9a31-350cedcde4d4', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('d1145219-e1c4-45c5-8620-67a032105615', '45f7df6f-1ad8-411f-9a31-350cedcde4d4', 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('f3ef5d5f-0efc-43ef-9c2f-1e301a106a62', '45f7df6f-1ad8-411f-9a31-350cedcde4d4', 0, 0, 27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('5c132079-a388-4e62-988b-f91b4a3f1245', '193088f9-01b3-48ac-85e5-b678d9c99b97', 0, 0, 27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('8ac87daa-fa80-42c6-b23b-fa63a2e2d24b', '193088f9-01b3-48ac-85e5-b678d9c99b97', 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('9430ae00-0357-4d8c-bc98-26e0aa47d6c9', '193088f9-01b3-48ac-85e5-b678d9c99b97', 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('c065c28c-36f9-4580-8796-cb68eb6108f1', '193088f9-01b3-48ac-85e5-b678d9c99b97', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('ed789b81-9288-4b1f-960b-6a1f9b533991', '193088f9-01b3-48ac-85e5-b678d9c99b97', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5);


--
-- TOC entry 2611 (class 0 OID 91032)
-- Dependencies: 235
-- Data for Name: InstanceOfPlayerWeapon; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('52d378f8-f182-4923-bab8-bc5d2d8aa4a9');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('d1145219-e1c4-45c5-8620-67a032105615');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('f3ef5d5f-0efc-43ef-9c2f-1e301a106a62');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('5c132079-a388-4e62-988b-f91b4a3f1245');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('9430ae00-0357-4d8c-bc98-26e0aa47d6c9');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('c065c28c-36f9-4580-8796-cb68eb6108f1');


--
-- TOC entry 2617 (class 0 OID 91170)
-- Dependencies: 241
-- Data for Name: InstanceOfTargetItem; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('05f2f1b7-6dea-4db0-81d2-44ecea169c82', '2e7d0dfb-f1bb-4f9e-b52a-940d172bf03d', '45f7df6f-1ad8-411f-9a31-350cedcde4d4', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('2534a1c1-0654-48b5-850f-c0d40fbe1307', '52d378f8-f182-4923-bab8-bc5d2d8aa4a9', '45f7df6f-1ad8-411f-9a31-350cedcde4d4', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('2b01eea3-21d9-4f05-94bf-567c46fab806', 'f3ef5d5f-0efc-43ef-9c2f-1e301a106a62', '45f7df6f-1ad8-411f-9a31-350cedcde4d4', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('3b2918f2-a130-459c-86e3-b3dc28467919', 'd1145219-e1c4-45c5-8620-67a032105615', '45f7df6f-1ad8-411f-9a31-350cedcde4d4', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('8f2c02a0-3bb4-44b3-9e29-49f7eaa1245e', '5c132079-a388-4e62-988b-f91b4a3f1245', '193088f9-01b3-48ac-85e5-b678d9c99b97', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('9571e3f1-f35f-4a65-86e5-dff7e42ac69c', 'c065c28c-36f9-4580-8796-cb68eb6108f1', '193088f9-01b3-48ac-85e5-b678d9c99b97', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('9615a965-17bd-45fb-b23a-62275b8211a9', '9430ae00-0357-4d8c-bc98-26e0aa47d6c9', '193088f9-01b3-48ac-85e5-b678d9c99b97', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('ca774074-7e9e-4747-bef9-01ce43d93a1d', '8ac87daa-fa80-42c6-b23b-fa63a2e2d24b', '193088f9-01b3-48ac-85e5-b678d9c99b97', 0, 7);


--
-- TOC entry 2616 (class 0 OID 91126)
-- Dependencies: 240
-- Data for Name: PlayerAchievement; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2588 (class 0 OID 16662)
-- Dependencies: 212
-- Data for Name: PlayerEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate", "CurrentFractionTypeId", "Reputation_Keepers", "Reputation_Keepers_Friend", "Reputation_RIFT", "Reputation_RIFT_Friend") VALUES ('45f7df6f-1ad8-411f-9a31-350cedcde4d4', 'орорп', 0, 0, 0, 1500, 0, '2016-06-07 22:45:09.168316', '2016-06-10 22:45:09.168316', '2016-06-07 22:45:09.467874', '2016-06-07 22:45:09.168316', 0, 10000000, 0, 10000000, 0);
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate", "CurrentFractionTypeId", "Reputation_Keepers", "Reputation_Keepers_Friend", "Reputation_RIFT", "Reputation_RIFT_Friend") VALUES ('193088f9-01b3-48ac-85e5-b678d9c99b97', 'hghf', 0, 0, 0, 1500, 0, '2016-06-08 00:57:06.973863', '2016-06-11 00:57:06.973863', '2016-06-08 00:57:07.226941', '2016-06-08 00:57:06.973863', 0, 0, 0, 0, 0);


--
-- TOC entry 2583 (class 0 OID 16632)
-- Dependencies: 207
-- Data for Name: PlayerProfileEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "PlayerProfileEntity" ("AssetId", "CharacterId", "HelmetId", "MaskId", "ArmourId", "BackpackId", "GlovesId", "PantsId", "BootsId", "PrimaryWeaponId", "SecondaryWeaponId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId", "IsEnabled", "Name", "IsUnlocked") VALUES ('e5dbe0c2-8b47-4aed-a753-fbdd2f01d9c0', '43363cf8-9dfd-45ca-871b-45c6805b77af', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f3ef5d5f-0efc-43ef-9c2f-1e301a106a62', NULL, 10, 0, 10, 0, '45f7df6f-1ad8-411f-9a31-350cedcde4d4', false, NULL, false);
INSERT INTO "PlayerProfileEntity" ("AssetId", "CharacterId", "HelmetId", "MaskId", "ArmourId", "BackpackId", "GlovesId", "PantsId", "BootsId", "PrimaryWeaponId", "SecondaryWeaponId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId", "IsEnabled", "Name", "IsUnlocked") VALUES ('01772331-58ce-4526-84e0-89df8a8009e0', 'ed789b81-9288-4b1f-960b-6a1f9b533991', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, 10, 0, '193088f9-01b3-48ac-85e5-b678d9c99b97', false, NULL, false);


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2604 (class 0 OID 90937)
-- Dependencies: 228
-- Data for Name: AbstractItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 5000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 10, 10000, true, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (9, 1, 1000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (10, 1, 1000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (11, 1, 1000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (28, 1, 1000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 0, 5000, false, 0, 0, 0, 1, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 0, 3500, false, 0, 0, 0, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 0, 4500, false, 0, 0, 0, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 0, 7000, false, 0, 0, 0, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 0, 175000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 0, 23543, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 0, 14477, true, 0, 0, 0, 1, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (25, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (27, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (17, 2, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (18, 3, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (20, 4, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (26, 5, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (23, 6, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (22, 7, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (24, 2, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (19, 3, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (12, 4, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (13, 5, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (14, 6, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (15, 7, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (16, 8, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (21, 9, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 2, 50700, false, 0, 0, 0, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 1, 58700, false, 0, 0, 0, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 3, 50000, false, 0, 0, 0, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 4, 15000, false, 0, 0, 0, 0, 1, true, 0);


--
-- TOC entry 2620 (class 0 OID 107613)
-- Dependencies: 244
-- Data for Name: AddonItemContainer; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2634 (class 0 OID 0)
-- Dependencies: 243
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE SET; Schema: Store; Owner: postgres
--

SELECT pg_catalog.setval('"AddonItemContainer_ContainerId_seq"', 1, false);


--
-- TOC entry 2618 (class 0 OID 107606)
-- Dependencies: 242
-- Data for Name: AddonItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2612 (class 0 OID 91071)
-- Dependencies: 236
-- Data for Name: AmmoItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2605 (class 0 OID 90955)
-- Dependencies: 229
-- Data for Name: ArmourItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 1);


--
-- TOC entry 2608 (class 0 OID 90980)
-- Dependencies: 232
-- Data for Name: CharacterItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 5);


--
-- TOC entry 2589 (class 0 OID 16682)
-- Dependencies: 213
-- Data for Name: FractionEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (1, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (2, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (3, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (4, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (0, 0, 0, 0, 0, 0);


--
-- TOC entry 2607 (class 0 OID 90974)
-- Dependencies: 231
-- Data for Name: SkinItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 7);


--
-- TOC entry 2606 (class 0 OID 90967)
-- Dependencies: 230
-- Data for Name: WeaponItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (20, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (21, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (22, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (23, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (24, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (25, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (26, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (27, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (28, 0);


SET search_path = public, pg_catalog;

--
-- TOC entry 2601 (class 0 OID 66321)
-- Dependencies: 225
-- Data for Name: GlobalChatMessage; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2602 (class 0 OID 66326)
-- Dependencies: 226
-- Data for Name: PrivateChatMessage; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2603 (class 0 OID 66331)
-- Dependencies: 227
-- Data for Name: PrivateChatMessageData; Type: TABLE DATA; Schema: public; Owner: postgres
--



SET search_path = vrs, pg_catalog;

--
-- TOC entry 2590 (class 0 OID 16734)
-- Dependencies: 214
-- Data for Name: Deposits; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2591 (class 0 OID 16740)
-- Dependencies: 215
-- Data for Name: Roles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2592 (class 0 OID 16743)
-- Dependencies: 216
-- Data for Name: UserClaims; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2635 (class 0 OID 0)
-- Dependencies: 217
-- Name: UserClaims_Id_seq; Type: SEQUENCE SET; Schema: vrs; Owner: postgres
--

SELECT pg_catalog.setval('"UserClaims_Id_seq"', 2, true);


--
-- TOC entry 2594 (class 0 OID 16751)
-- Dependencies: 218
-- Data for Name: UserLogins; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2595 (class 0 OID 16755)
-- Dependencies: 219
-- Data for Name: UserRoles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2596 (class 0 OID 16759)
-- Dependencies: 220
-- Data for Name: Users; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('a8135189-0f90-4c4e-99a7-56038bd350f9', 'serinc00@vrs.com', 'AL/hYnP8BTFiUGpXgHDTuOMxWigUIdqDh5b7mU/YOZT6zAbxfoRZZCeHPfNbodPpfA==', '96ae9fc6-1aa0-4117-a236-eb92f8d7d01b', 'serinc00@vrs.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('99b733a7-5bef-43b4-b96b-4a167caa2998', 'serinc@vrs.com', 'AA1JALfBFjDOPvhUim7TfzPLEFAqbkMSTDpH8/lp6pV0Y/F2d5jaXKWP34qEkmLOQg==', '92268ad0-5161-41d7-80fe-d0e01eaa99cf', 'serinc@vrs.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('493847e6-f716-47c7-89f3-6f7e2327d901', 'serinc0001@vrs.com', 'AMImqwexEbgn0BypvP0kQ+5G6u3b18o5bS2FcD0GExz8ctwEr4nVeVVyRvn64XqnnQ==', 'e8b47df5-a4ff-4b17-bb06-2d779fe69371', 'serinc0001@vrs.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('fd0aa240-d750-453f-b6b0-4b2de30fc199', 'serinc101@vrs.com', 'ALIB5LRhSCoaspX9szCqBuxh4N1jfUdfYFlQaYv4cZXILWVBEPO88mgOz4Y8yUWm4Q==', '4f4de0b8-3f42-4872-a2c8-d63c08aa8cd5', 'serinc101@vrs.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('bbf606b0-9fbd-4096-b80e-5f91caf72fef', 'serinc1010@vrs.com', 'ALNos8ThZPubAlcohRG8r/HlGanST/In2gJlWVjLZLj8Smi1bJCZLECLb3fVGYji+A==', 'bcadb817-8c8f-4508-8fa2-e7857bd2fe3b', 'serinc1010@vrs.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('193088f9-01b3-48ac-85e5-b678d9c99b97', 'serin0@vrs.com', 'ANHyEZMBVcdFgaUDQPyQf4VFeWIs2k8TpWPq2diImrRkt1vmoUcjOAIla0NtWKU+xg==', 'f04fd352-252b-46fe-8ff6-c6aac32d7b8f', 'serin0@vrs.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('45f7df6f-1ad8-411f-9a31-350cedcde4d4', 'serin01@vrs.com', 'AGm7wGh4SHTKUihN/Rah5b/3xr14by4ts3taa3q/cKEmWESumFkPShglT7RkTC68VA==', '841c38fc-4747-4718-9f6d-e7e3b12ca765', 'serin01@vrs.com', false, NULL, false, false, NULL, false, 0, NULL, 0);


SET search_path = "Achievements", pg_catalog;

--
-- TOC entry 2397 (class 2606 OID 91115)
-- Name: AchievementContainer_pkey; Type: CONSTRAINT; Schema: Achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementContainer"
    ADD CONSTRAINT "AchievementContainer_pkey" PRIMARY KEY ("AchievementTypeId");


--
-- TOC entry 2399 (class 2606 OID 91125)
-- Name: AchievementInstance_pkey; Type: CONSTRAINT; Schema: Achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementInstance"
    ADD CONSTRAINT "AchievementInstance_pkey" PRIMARY KEY ("AchievementBonusId");


SET search_path = "League", pg_catalog;

--
-- TOC entry 2327 (class 2606 OID 16785)
-- Name: LeagueEntity_Info_Abbr_key; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Abbr_key" UNIQUE ("Info_Abbr");


--
-- TOC entry 2329 (class 2606 OID 16787)
-- Name: LeagueEntity_Info_Name_key; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Name_key" UNIQUE ("Info_Name");


--
-- TOC entry 2331 (class 2606 OID 16789)
-- Name: LeagueEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_pkey" PRIMARY KEY ("LeagueId");


--
-- TOC entry 2333 (class 2606 OID 16791)
-- Name: LeagueMemberEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2361 (class 2606 OID 49892)
-- Name: MatchEntity_WinnerTeamId_key; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_key" UNIQUE ("WinnerTeamId");


--
-- TOC entry 2363 (class 2606 OID 49812)
-- Name: MatchEntity_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_pkey" PRIMARY KEY ("MatchId");


--
-- TOC entry 2367 (class 2606 OID 49846)
-- Name: MatchMember_PlayerId_TeamId_key; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_TeamId_key" UNIQUE ("PlayerId", "TeamId");


--
-- TOC entry 2369 (class 2606 OID 49824)
-- Name: MatchMember_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_pkey" PRIMARY KEY ("MemberId");


--
-- TOC entry 2365 (class 2606 OID 49818)
-- Name: MatchTeam_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_pkey" PRIMARY KEY ("TeamId");


--
-- TOC entry 2371 (class 2606 OID 49829)
-- Name: MemberAchievements_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_pkey" PRIMARY KEY ("AchievementId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2389 (class 2606 OID 91004)
-- Name: AbstractPlayerItemInstance_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "AbstractPlayerItemInstance_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2337 (class 2606 OID 16797)
-- Name: FriendEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_pkey" PRIMARY KEY ("MemberId");


--
-- TOC entry 2427 (class 2606 OID 115905)
-- Name: InstanceOfInstalledAddon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_pkey" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2425 (class 2606 OID 107673)
-- Name: InstanceOfItemAddon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "InstanceOfItemAddon_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2409 (class 2606 OID 91174)
-- Name: InstanceOfItemSkin_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfItemSkin_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2423 (class 2606 OID 107658)
-- Name: InstanceOfItemSkin_pkey1; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "InstanceOfItemSkin_pkey1" PRIMARY KEY ("ItemId");


--
-- TOC entry 2391 (class 2606 OID 91095)
-- Name: InstanceOfPlayerArmour_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "InstanceOfPlayerArmour_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2335 (class 2606 OID 91081)
-- Name: InstanceOfPlayerCharacter_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "InstanceOfPlayerCharacter_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2393 (class 2606 OID 91088)
-- Name: InstanceOfPlayerWeapon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "InstanceOfPlayerWeapon_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2401 (class 2606 OID 91134)
-- Name: PlayerAchievement_AchievementTypeId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_AchievementTypeId_key" UNIQUE ("AchievementTypeId");


--
-- TOC entry 2403 (class 2606 OID 91136)
-- Name: PlayerAchievement_PlayerId_LastAchievementInstanceId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_PlayerId_LastAchievementInstanceId_key" UNIQUE ("PlayerId", "LastAchievementInstanceId");


--
-- TOC entry 2405 (class 2606 OID 91132)
-- Name: PlayerAchievement_PlayerId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_PlayerId_key" UNIQUE ("PlayerId");


--
-- TOC entry 2407 (class 2606 OID 91130)
-- Name: PlayerAchievement_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_pkey" PRIMARY KEY ("PlayerAchievementId");


--
-- TOC entry 2325 (class 2606 OID 16783)
-- Name: PlayerAssetEnity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PlayerAssetEnity_pkey" PRIMARY KEY ("AssetId");


--
-- TOC entry 2339 (class 2606 OID 16799)
-- Name: PlayerEntity_PlayerName_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerName_key" UNIQUE ("PlayerName");


--
-- TOC entry 2341 (class 2606 OID 16801)
-- Name: PlayerEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2379 (class 2606 OID 90954)
-- Name: AbstractItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "AbstractItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2415 (class 2606 OID 107620)
-- Name: AddonItemContainer_AddonId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_AddonId_key" UNIQUE ("ModelId");


--
-- TOC entry 2417 (class 2606 OID 107624)
-- Name: AddonItemContainer_CategoryTypeId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_CategoryTypeId_key" UNIQUE ("CategoryTypeId");


--
-- TOC entry 2419 (class 2606 OID 107622)
-- Name: AddonItemContainer_ItemId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ItemId_key" UNIQUE ("ItemId");


--
-- TOC entry 2421 (class 2606 OID 107618)
-- Name: AddonItemContainer_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_pkey" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2411 (class 2606 OID 107626)
-- Name: AddonItemInstance_ModelId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "AddonItemInstance_ModelId_key" UNIQUE ("ModelId");


--
-- TOC entry 2395 (class 2606 OID 91076)
-- Name: AmmoItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "AmmoItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2381 (class 2606 OID 90959)
-- Name: ArmourItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2387 (class 2606 OID 90985)
-- Name: CharacterItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "CharacterItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2343 (class 2606 OID 16813)
-- Name: FractionEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "FractionEntity_pkey" PRIMARY KEY ("FractionId");


--
-- TOC entry 2413 (class 2606 OID 107610)
-- Name: ItemAddonInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "ItemAddonInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2385 (class 2606 OID 90979)
-- Name: SkinItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "SkinItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2383 (class 2606 OID 90972)
-- Name: WeaponItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "WeaponItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


SET search_path = public, pg_catalog;

--
-- TOC entry 2373 (class 2606 OID 66325)
-- Name: GlobalChatMessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2377 (class 2606 OID 66335)
-- Name: PrivateChatMessageData_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2375 (class 2606 OID 66330)
-- Name: PrivateChatMessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_pkey" PRIMARY KEY ("MessageId");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2345 (class 2606 OID 16825)
-- Name: Deposits_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Deposits"
    ADD CONSTRAINT "Deposits_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2347 (class 2606 OID 16827)
-- Name: Roles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2350 (class 2606 OID 16829)
-- Name: UserClaims_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "UserClaims_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2353 (class 2606 OID 16831)
-- Name: UserLogins_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "UserLogins_pkey" PRIMARY KEY ("UserId", "LoginProvider", "ProviderKey");


--
-- TOC entry 2357 (class 2606 OID 16833)
-- Name: UserRoles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "UserRoles_pkey" PRIMARY KEY ("UserId", "RoleId");


--
-- TOC entry 2359 (class 2606 OID 16835)
-- Name: Users_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2348 (class 1259 OID 16836)
-- Name: IX_UserClaims_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserClaims_UserId" ON "UserClaims" USING btree ("UserId");


--
-- TOC entry 2351 (class 1259 OID 16837)
-- Name: IX_UserLogins_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserLogins_UserId" ON "UserLogins" USING btree ("UserId");


--
-- TOC entry 2354 (class 1259 OID 16838)
-- Name: IX_UserRoles_RoleId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserRoles_RoleId" ON "UserRoles" USING btree ("RoleId");


--
-- TOC entry 2355 (class 1259 OID 16839)
-- Name: IX_UserRoles_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserRoles_UserId" ON "UserRoles" USING btree ("UserId");


SET search_path = "League", pg_catalog;

--
-- TOC entry 2429 (class 2606 OID 16875)
-- Name: LeagueMemberEntity_LeagueId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_LeagueId_fkey" FOREIGN KEY ("LeagueId") REFERENCES "LeagueEntity"("LeagueId") ON DELETE CASCADE;


--
-- TOC entry 2430 (class 2606 OID 16880)
-- Name: LeagueMemberEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2440 (class 2606 OID 49886)
-- Name: MatchEntity_WinnerTeamId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_fkey" FOREIGN KEY ("WinnerTeamId") REFERENCES "MatchTeam"("TeamId");


--
-- TOC entry 2444 (class 2606 OID 49870)
-- Name: MatchMember_MatchId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2442 (class 2606 OID 49835)
-- Name: MatchMember_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2443 (class 2606 OID 49840)
-- Name: MatchMember_TeamId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_TeamId_fkey" FOREIGN KEY ("TeamId") REFERENCES "MatchTeam"("TeamId") ON DELETE CASCADE;


--
-- TOC entry 2441 (class 2606 OID 49830)
-- Name: MatchTeam_MatchId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2445 (class 2606 OID 49847)
-- Name: MemberAchievements_MemberId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_MemberId_fkey" FOREIGN KEY ("MemberId") REFERENCES "MatchMember"("MemberId") ON DELETE CASCADE;


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2432 (class 2606 OID 16895)
-- Name: FriendEntity_FriendId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_FriendId_fkey" FOREIGN KEY ("FriendId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2433 (class 2606 OID 16900)
-- Name: FriendEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2467 (class 2606 OID 115894)
-- Name: InstanceOfInstalledAddon_AddonId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_AddonId_fkey" FOREIGN KEY ("AddonId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2468 (class 2606 OID 115899)
-- Name: InstanceOfInstalledAddon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2466 (class 2606 OID 107674)
-- Name: InstanceOfItemAddon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "InstanceOfItemAddon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2465 (class 2606 OID 107679)
-- Name: InstanceOfItemSkin_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "InstanceOfItemSkin_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2462 (class 2606 OID 91180)
-- Name: InstanceOfItemSkin_TargetPlayerItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfItemSkin_TargetPlayerItemId_fkey" FOREIGN KEY ("TargetPlayerItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2455 (class 2606 OID 91096)
-- Name: InstanceOfPlayerArmour_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "InstanceOfPlayerArmour_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2431 (class 2606 OID 91082)
-- Name: InstanceOfPlayerCharacter_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "InstanceOfPlayerCharacter_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2453 (class 2606 OID 91198)
-- Name: InstanceOfPlayerItem_ModelId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "InstanceOfPlayerItem_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "Store"."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2454 (class 2606 OID 91101)
-- Name: InstanceOfPlayerItem_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "InstanceOfPlayerItem_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2456 (class 2606 OID 91089)
-- Name: InstanceOfPlayerWeapon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "InstanceOfPlayerWeapon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2461 (class 2606 OID 107689)
-- Name: InstanceOfTargetItem_ModelId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfTargetItem_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "Store"."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2460 (class 2606 OID 107684)
-- Name: InstanceOfTargetItem_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfTargetItem_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2458 (class 2606 OID 91142)
-- Name: PlayerAchievement_AchievementTypeId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_AchievementTypeId_fkey" FOREIGN KEY ("AchievementTypeId") REFERENCES "Achievements"."AchievementContainer"("AchievementTypeId") ON DELETE CASCADE;


--
-- TOC entry 2459 (class 2606 OID 91152)
-- Name: PlayerAchievement_LastAchievementInstanceId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_LastAchievementInstanceId_fkey" FOREIGN KEY ("LastAchievementInstanceId") REFERENCES "Achievements"."AchievementInstance"("AchievementBonusId");


--
-- TOC entry 2457 (class 2606 OID 91137)
-- Name: PlayerAchievement_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2435 (class 2606 OID 66289)
-- Name: PlayerEntity_FractionTypeId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_FractionTypeId_fkey" FOREIGN KEY ("CurrentFractionTypeId") REFERENCES "Store"."FractionEntity"("FractionId");


--
-- TOC entry 2434 (class 2606 OID 16905)
-- Name: PlayerEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES vrs."Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2428 (class 2606 OID 16870)
-- Name: PreSetEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PreSetEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2464 (class 2606 OID 107632)
-- Name: AddonItemContainer_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ItemId_fkey" FOREIGN KEY ("ItemId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2463 (class 2606 OID 107640)
-- Name: AddonItemContainer_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ModelId_fkey" FOREIGN KEY ("ModelId", "ModelCategoryTypeId") REFERENCES "AddonItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2452 (class 2606 OID 90960)
-- Name: ArmourItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


SET search_path = public, pg_catalog;

--
-- TOC entry 2447 (class 2606 OID 124086)
-- Name: GlobalChatMessage_EditorId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_EditorId_fkey" FOREIGN KEY ("EditorId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2446 (class 2606 OID 124081)
-- Name: GlobalChatMessage_SenderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2451 (class 2606 OID 66356)
-- Name: PrivateChatMessageData_ReceiverId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_ReceiverId_fkey" FOREIGN KEY ("ReceiverId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2450 (class 2606 OID 66351)
-- Name: PrivateChatMessageData_SenderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2449 (class 2606 OID 66346)
-- Name: PrivateChatMessage_MessageDataId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_MessageDataId_fkey" FOREIGN KEY ("MessageDataId") REFERENCES "PrivateChatMessageData"("MessageId") ON DELETE CASCADE;


--
-- TOC entry 2448 (class 2606 OID 66336)
-- Name: PrivateChatMessage_PlayerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2436 (class 2606 OID 16950)
-- Name: FK_UserClaims_Users_User_Id; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "FK_UserClaims_Users_User_Id" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2437 (class 2606 OID 16955)
-- Name: FK_UserLogins_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "FK_UserLogins_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2438 (class 2606 OID 16960)
-- Name: FK_UserRoles_Roles_RoleId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Roles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "Roles"("Id") ON DELETE CASCADE;


--
-- TOC entry 2439 (class 2606 OID 16965)
-- Name: FK_UserRoles_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2629 (class 0 OID 0)
-- Dependencies: 16
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-06-08 02:25:16

--
-- PostgreSQL database dump complete
--

