--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-07-02 05:45:19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = "Players", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 252 (class 1259 OID 214715)
-- Name: PlayerPromoCode; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerPromoCode" (
    "PlayerCodeId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "CodeId" uuid NOT NULL,
    "ActivationDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "PlayerPromoCode" OWNER TO postgres;

--
-- TOC entry 2209 (class 2606 OID 214719)
-- Name: PlayerPromoCode_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerPromoCode"
    ADD CONSTRAINT "PlayerPromoCode_pkey" PRIMARY KEY ("PlayerCodeId");


--
-- TOC entry 2211 (class 2606 OID 214725)
-- Name: PlayerPromoCode_CodeId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerPromoCode"
    ADD CONSTRAINT "PlayerPromoCode_CodeId_fkey" FOREIGN KEY ("CodeId") REFERENCES "Promo"."Codes"("PromoId") ON DELETE CASCADE;


--
-- TOC entry 2210 (class 2606 OID 214720)
-- Name: PlayerPromoCode_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerPromoCode"
    ADD CONSTRAINT "PlayerPromoCode_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId");


-- Completed on 2016-07-02 05:45:19

--
-- PostgreSQL database dump complete
--

