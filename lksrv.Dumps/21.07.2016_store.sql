--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-07-21 16:58:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 8 (class 2615 OID 48103)
-- Name: store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA store;


ALTER SCHEMA store OWNER TO postgres;

SET search_path = store, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 219 (class 1259 OID 48449)
-- Name: AbstractItemInstance; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AbstractItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL,
    "IsAvalible" boolean DEFAULT false NOT NULL,
    "Level" integer DEFAULT 0 NOT NULL,
    "DefaultSkinId" integer DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer DEFAULT 0 NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL
);


ALTER TABLE "AbstractItemInstance" OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 48460)
-- Name: AddonItemContainer; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AddonItemContainer" (
    "ContainerId" integer NOT NULL,
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "ModelCategoryTypeId" integer DEFAULT 0 NOT NULL,
    "ItemId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "AddonItemContainer" OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 48467)
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE; Schema: store; Owner: postgres
--

CREATE SEQUENCE "AddonItemContainer_ContainerId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AddonItemContainer_ContainerId_seq" OWNER TO postgres;

--
-- TOC entry 2294 (class 0 OID 0)
-- Dependencies: 221
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE OWNED BY; Schema: store; Owner: postgres
--

ALTER SEQUENCE "AddonItemContainer_ContainerId_seq" OWNED BY "AddonItemContainer"."ContainerId";


--
-- TOC entry 222 (class 1259 OID 48469)
-- Name: AddonItemInstance; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AddonItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "AddonItemInstance" OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 48474)
-- Name: AmmoItemInstance; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AmmoItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "AmmoItemInstance" OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 48479)
-- Name: ArmourItemInstance; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ArmourItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "ArmourItemInstance" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 48484)
-- Name: CharacterItemInstance; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "CharacterItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "CharacterItemInstance" OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 48489)
-- Name: FractionEntity; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "FractionEntity" (
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" integer DEFAULT 0 NOT NULL,
    "Discount_Ammo" integer DEFAULT 0 NOT NULL,
    "Discount_Weapon" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "FractionEntity" OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 48496)
-- Name: KitItemInstance; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "KitItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "KitItemInstance" OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 48501)
-- Name: SkinItemInstance; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "SkinItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "SkinItemInstance" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 48506)
-- Name: WeaponItemInstance; Type: TABLE; Schema: store; Owner: postgres; Tablespace: 
--

CREATE TABLE "WeaponItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "WeaponItemInstance" OWNER TO postgres;

--
-- TOC entry 2111 (class 2604 OID 48552)
-- Name: ContainerId; Type: DEFAULT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer" ALTER COLUMN "ContainerId" SET DEFAULT nextval('"AddonItemContainer_ContainerId_seq"'::regclass);


--
-- TOC entry 2279 (class 0 OID 48449)
-- Dependencies: 219
-- Data for Name: AbstractItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (9, 0, false, 1, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (10, 0, false, 1, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (11, 0, false, 1, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 0, false, 0, 0, 1, 5000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 0, false, 0, 0, 2, 3500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 0, false, 0, 0, 2, 4500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 0, false, 0, 0, 2, 7000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 0, false, 0, 0, 0, 175000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 0, false, 0, 0, 0, 23543, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (8, 0, false, 0, 0, 1, 14477, true);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 7, true, 1, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 0, true, 0, 0, 2, 5000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (24, 0, true, 0, 0, 2, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (26, 0, true, 1, 0, 2, 2600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (27, 0, true, 2, 0, 2, 8600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (25, 0, true, 2, 0, 2, 11200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (12, 0, true, 0, 0, 0, 6800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (15, 0, true, 0, 0, 0, 2600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (13, 0, true, 1, 0, 0, 9800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (14, 0, true, 1, 0, 0, 4200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (17, 0, true, 2, 0, 0, 22100, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (28, 0, true, 1, 0, 2, 5400, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 1, true, 1, 0, 2, 2600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 1, true, 1, 0, 2, 4200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 1, true, 1, 0, 2, 3200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (8, 1, true, 1, 0, 2, 1100, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (4, 1, true, 2, 0, 2, 1800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 4, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 4, true, 1, 0, 2, 100, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 4, true, 2, 0, 2, 500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 4, true, 1, 0, 2, 400, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (4, 4, true, 2, 0, 2, 850, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 4, true, 1, 0, 2, 250, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 4, true, 0, 0, 2, 100, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 4, true, 1, 0, 2, 400, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (8, 4, true, 2, 0, 2, 450, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (9, 4, true, 3, 0, 2, 600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (10, 4, true, 3, 0, 2, 800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (11, 4, true, 3, 0, 2, 900, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (12, 4, true, 2, 0, 2, 200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (13, 4, true, 2, 0, 2, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (14, 4, true, 2, 0, 2, 400, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (15, 4, true, 3, 0, 2, 600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (16, 4, true, 3, 0, 2, 800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (17, 4, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (18, 4, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (19, 4, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 7, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 7, true, 1, 0, 0, 500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 7, true, 1, 0, 0, 1500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (4, 7, true, 1, 0, 0, 1600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 7, true, 1, 0, 0, 2000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (8, 7, true, 1, 0, 0, 100000, true);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (29, 0, true, 3, 0, 2, 11200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 7, true, 1, 0, 0, 10000, true);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 7, true, 1, 0, 0, 100000, true);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 5, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 5, true, 10, 0, 0, 50000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 5, true, 20, 0, 0, 80000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 5, true, 20, 0, 0, 80000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (4, 5, true, 40, 0, 0, 150000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 5, true, 30, 0, 0, 110000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 5, true, 38, 0, 0, 140000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 9, true, 1, 0, 2, 800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 9, true, 1, 0, 0, 900, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (20, 4, true, 2, 0, 2, 800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (21, 4, true, 2, 0, 2, 600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 5, true, 10, 0, 0, 45600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (11, 1, true, 1, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (14, 1, false, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (15, 1, false, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (16, 1, false, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (17, 1, false, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (30, 0, true, 3, 0, 2, 12600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (10, 1, true, 1, 0, 0, 2200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (9, 1, true, 1, 0, 0, 2800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (12, 1, true, 2, 0, 0, 3600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 1, true, 3, 0, 0, 6800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 1, true, 3, 0, 0, 38500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 1, true, 3, 0, 0, 68500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 1, true, 3, 0, 0, 41000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (13, 1, true, 4, 0, 0, 7200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (21, 0, true, 2, 0, 0, 5600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (19, 0, true, 3, 0, 0, 20500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (18, 0, true, 3, 0, 0, 12500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (20, 0, true, 4, 0, 0, 34400, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (16, 0, true, 4, 0, 0, 48600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (22, 0, true, 5, 0, 0, 67550, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (23, 0, true, 5, 0, 0, 65785, false);


--
-- TOC entry 2280 (class 0 OID 48460)
-- Dependencies: 220
-- Data for Name: AddonItemContainer; Type: TABLE DATA; Schema: store; Owner: postgres
--



--
-- TOC entry 2295 (class 0 OID 0)
-- Dependencies: 221
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE SET; Schema: store; Owner: postgres
--

SELECT pg_catalog.setval('"AddonItemContainer_ContainerId_seq"', 1, false);


--
-- TOC entry 2282 (class 0 OID 48469)
-- Dependencies: 222
-- Data for Name: AddonItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (20, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (21, 4);


--
-- TOC entry 2283 (class 0 OID 48474)
-- Dependencies: 223
-- Data for Name: AmmoItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--



--
-- TOC entry 2284 (class 0 OID 48479)
-- Dependencies: 224
-- Data for Name: ArmourItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 1);


--
-- TOC entry 2285 (class 0 OID 48484)
-- Dependencies: 225
-- Data for Name: CharacterItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 5);


--
-- TOC entry 2286 (class 0 OID 48489)
-- Dependencies: 226
-- Data for Name: FractionEntity; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (1, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (2, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (3, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (4, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (0, 0, 0, 0);


--
-- TOC entry 2287 (class 0 OID 48496)
-- Dependencies: 227
-- Data for Name: KitItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "KitItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 9);
INSERT INTO "KitItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 9);


--
-- TOC entry 2288 (class 0 OID 48501)
-- Dependencies: 228
-- Data for Name: SkinItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 7);


--
-- TOC entry 2289 (class 0 OID 48506)
-- Dependencies: 229
-- Data for Name: WeaponItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (20, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (21, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (22, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (23, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (24, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (25, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (26, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (27, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (28, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (29, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (30, 0);


--
-- TOC entry 2132 (class 2606 OID 48639)
-- Name: PK_store.AbstractItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "PK_store.AbstractItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2136 (class 2606 OID 48641)
-- Name: PK_store.AddonItemContainer; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "PK_store.AddonItemContainer" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2139 (class 2606 OID 48643)
-- Name: PK_store.AddonItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "PK_store.AddonItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2142 (class 2606 OID 48645)
-- Name: PK_store.AmmoItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "PK_store.AmmoItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2145 (class 2606 OID 48647)
-- Name: PK_store.ArmourItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "PK_store.ArmourItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2148 (class 2606 OID 48649)
-- Name: PK_store.CharacterItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "PK_store.CharacterItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2150 (class 2606 OID 48651)
-- Name: PK_store.FractionEntity; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "PK_store.FractionEntity" PRIMARY KEY ("FractionId");


--
-- TOC entry 2153 (class 2606 OID 48653)
-- Name: PK_store.KitItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "PK_store.KitItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2155 (class 2606 OID 48655)
-- Name: PK_store.SkinItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "PK_store.SkinItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2158 (class 2606 OID 48657)
-- Name: PK_store.WeaponItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "PK_store.WeaponItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2130 (class 1259 OID 48719)
-- Name: AbstractItemInstance_IX_FractionId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "AbstractItemInstance_IX_FractionId" ON "AbstractItemInstance" USING btree ("FractionId");


--
-- TOC entry 2133 (class 1259 OID 48720)
-- Name: AddonItemContainer_IX_ItemId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "AddonItemContainer_IX_ItemId_CategoryTypeId" ON "AddonItemContainer" USING btree ("ItemId", "CategoryTypeId");


--
-- TOC entry 2134 (class 1259 OID 48721)
-- Name: AddonItemContainer_IX_ModelId_ModelCategoryTypeId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "AddonItemContainer_IX_ModelId_ModelCategoryTypeId" ON "AddonItemContainer" USING btree ("ModelId", "ModelCategoryTypeId");


--
-- TOC entry 2137 (class 1259 OID 48722)
-- Name: AddonItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "AddonItemInstance_IX_ModelId_CategoryTypeId" ON "AddonItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2140 (class 1259 OID 48723)
-- Name: AmmoItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "AmmoItemInstance_IX_ModelId_CategoryTypeId" ON "AmmoItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2143 (class 1259 OID 48724)
-- Name: ArmourItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "ArmourItemInstance_IX_ModelId_CategoryTypeId" ON "ArmourItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2146 (class 1259 OID 48725)
-- Name: CharacterItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "CharacterItemInstance_IX_ModelId_CategoryTypeId" ON "CharacterItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2151 (class 1259 OID 48726)
-- Name: KitItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "KitItemInstance_IX_ModelId_CategoryTypeId" ON "KitItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2156 (class 1259 OID 48727)
-- Name: SkinItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "SkinItemInstance_IX_ModelId_CategoryTypeId" ON "SkinItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2159 (class 1259 OID 48728)
-- Name: WeaponItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres; Tablespace: 
--

CREATE INDEX "WeaponItemInstance_IX_ModelId_CategoryTypeId" ON "WeaponItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2160 (class 2606 OID 48980)
-- Name: FK_store.AbstractItemInstance_store.FractionEntity_FractionId; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "FK_store.AbstractItemInstance_store.FractionEntity_FractionId" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2161 (class 2606 OID 48985)
-- Name: FK_store.AddonItemContainer_store.AbstractItemInstance_ItemId_C; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "FK_store.AddonItemContainer_store.AbstractItemInstance_ItemId_C" FOREIGN KEY ("ItemId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2162 (class 2606 OID 48990)
-- Name: FK_store.AddonItemContainer_store.AddonItemInstance_ModelId_Mod; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "FK_store.AddonItemContainer_store.AddonItemInstance_ModelId_Mod" FOREIGN KEY ("ModelId", "ModelCategoryTypeId") REFERENCES "AddonItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2163 (class 2606 OID 48995)
-- Name: FK_store.AddonItemInstance_store.AbstractItemInstance_ModelId_C; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "FK_store.AddonItemInstance_store.AbstractItemInstance_ModelId_C" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2164 (class 2606 OID 49000)
-- Name: FK_store.AmmoItemInstance_store.AbstractItemInstance_ModelId_Ca; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "FK_store.AmmoItemInstance_store.AbstractItemInstance_ModelId_Ca" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2165 (class 2606 OID 49005)
-- Name: FK_store.ArmourItemInstance_store.AbstractItemInstance_ModelId_; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "FK_store.ArmourItemInstance_store.AbstractItemInstance_ModelId_" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2166 (class 2606 OID 49010)
-- Name: FK_store.CharacterItemInstance_store.AbstractItemInstance_Model; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "FK_store.CharacterItemInstance_store.AbstractItemInstance_Model" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2167 (class 2606 OID 49015)
-- Name: FK_store.KitItemInstance_store.AbstractItemInstance_ModelId_Cat; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "FK_store.KitItemInstance_store.AbstractItemInstance_ModelId_Cat" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2168 (class 2606 OID 49020)
-- Name: FK_store.SkinItemInstance_store.AbstractItemInstance_ModelId_Ca; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "FK_store.SkinItemInstance_store.AbstractItemInstance_ModelId_Ca" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2169 (class 2606 OID 49025)
-- Name: FK_store.WeaponItemInstance_store.AbstractItemInstance_ModelId_; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "FK_store.WeaponItemInstance_store.AbstractItemInstance_ModelId_" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


-- Completed on 2016-07-21 16:58:44

--
-- PostgreSQL database dump complete
--

