--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-06-05 11:46:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = "Players", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 247 (class 1259 OID 115889)
-- Name: InstanceOfInstalledAddon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfInstalledAddon" (
    "AddonId" uuid NOT NULL,
    "ItemId" uuid,
    "AddonSlot" smallint NOT NULL,
    "ContainerId" uuid NOT NULL
);


ALTER TABLE "InstanceOfInstalledAddon" OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 107659)
-- Name: InstanceOfItemAddon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfItemAddon" (
    "ItemId" uuid NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value smallint DEFAULT 0,
    modification_2_value smallint DEFAULT 0,
    modification_3_value smallint DEFAULT 0,
    modification_4_value smallint DEFAULT 0,
    modification_5_value smallint DEFAULT 0
);


ALTER TABLE "InstanceOfItemAddon" OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 107654)
-- Name: InstanceOfItemSkin; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfItemSkin" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfItemSkin" OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 91170)
-- Name: InstanceOfTargetItem; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfTargetItem" (
    "ItemId" uuid NOT NULL,
    "TargetPlayerItemId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "InstanceOfTargetItem" OWNER TO postgres;

SET search_path = "Store", pg_catalog;

--
-- TOC entry 244 (class 1259 OID 107613)
-- Name: AddonItemContainer; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AddonItemContainer" (
    "ContainerId" integer NOT NULL,
    "ModelId" smallint NOT NULL,
    "ItemId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    "ModelCategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AddonItemContainer" OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 107611)
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE; Schema: Store; Owner: postgres
--

CREATE SEQUENCE "AddonItemContainer_ContainerId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AddonItemContainer_ContainerId_seq" OWNER TO postgres;

--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 243
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE OWNED BY; Schema: Store; Owner: postgres
--

ALTER SEQUENCE "AddonItemContainer_ContainerId_seq" OWNED BY "AddonItemContainer"."ContainerId";


--
-- TOC entry 242 (class 1259 OID 107606)
-- Name: AddonItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AddonItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AddonItemInstance" OWNER TO postgres;

--
-- TOC entry 2196 (class 2604 OID 107616)
-- Name: ContainerId; Type: DEFAULT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer" ALTER COLUMN "ContainerId" SET DEFAULT nextval('"AddonItemContainer_ContainerId_seq"'::regclass);


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2226 (class 2606 OID 115905)
-- Name: InstanceOfInstalledAddon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_pkey" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2224 (class 2606 OID 107673)
-- Name: InstanceOfItemAddon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "InstanceOfItemAddon_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2208 (class 2606 OID 91174)
-- Name: InstanceOfItemSkin_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfItemSkin_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2222 (class 2606 OID 107658)
-- Name: InstanceOfItemSkin_pkey1; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "InstanceOfItemSkin_pkey1" PRIMARY KEY ("ItemId");


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2214 (class 2606 OID 107620)
-- Name: AddonItemContainer_AddonId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_AddonId_key" UNIQUE ("ModelId");


--
-- TOC entry 2216 (class 2606 OID 107624)
-- Name: AddonItemContainer_CategoryTypeId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_CategoryTypeId_key" UNIQUE ("CategoryTypeId");


--
-- TOC entry 2218 (class 2606 OID 107622)
-- Name: AddonItemContainer_ItemId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ItemId_key" UNIQUE ("ItemId");


--
-- TOC entry 2220 (class 2606 OID 107618)
-- Name: AddonItemContainer_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_pkey" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2210 (class 2606 OID 107626)
-- Name: AddonItemInstance_ModelId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "AddonItemInstance_ModelId_key" UNIQUE ("ModelId");


--
-- TOC entry 2212 (class 2606 OID 107610)
-- Name: ItemAddonInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "ItemAddonInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2234 (class 2606 OID 115894)
-- Name: InstanceOfInstalledAddon_AddonId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_AddonId_fkey" FOREIGN KEY ("AddonId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2235 (class 2606 OID 115899)
-- Name: InstanceOfInstalledAddon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2233 (class 2606 OID 107674)
-- Name: InstanceOfItemAddon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "InstanceOfItemAddon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2232 (class 2606 OID 107679)
-- Name: InstanceOfItemSkin_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "InstanceOfItemSkin_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2229 (class 2606 OID 91180)
-- Name: InstanceOfItemSkin_TargetPlayerItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfItemSkin_TargetPlayerItemId_fkey" FOREIGN KEY ("TargetPlayerItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2228 (class 2606 OID 107689)
-- Name: InstanceOfTargetItem_ModelId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfTargetItem_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "Store"."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2227 (class 2606 OID 107684)
-- Name: InstanceOfTargetItem_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfTargetItem_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2231 (class 2606 OID 107632)
-- Name: AddonItemContainer_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ItemId_fkey" FOREIGN KEY ("ItemId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2230 (class 2606 OID 107640)
-- Name: AddonItemContainer_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ModelId_fkey" FOREIGN KEY ("ModelId", "ModelCategoryTypeId") REFERENCES "AddonItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


-- Completed on 2016-06-05 11:46:44

--
-- PostgreSQL database dump complete
--

