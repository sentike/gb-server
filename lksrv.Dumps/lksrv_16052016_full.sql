--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-05-16 02:43:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 12 (class 2615 OID 16619)
-- Name: Inventory; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Inventory";


ALTER SCHEMA "Inventory" OWNER TO postgres;

--
-- TOC entry 13 (class 2615 OID 16620)
-- Name: League; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "League";


ALTER SCHEMA "League" OWNER TO postgres;

--
-- TOC entry 18 (class 2615 OID 49807)
-- Name: Matches; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Matches";


ALTER SCHEMA "Matches" OWNER TO postgres;

--
-- TOC entry 14 (class 2615 OID 16621)
-- Name: Players; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Players";


ALTER SCHEMA "Players" OWNER TO postgres;

--
-- TOC entry 15 (class 2615 OID 16622)
-- Name: Store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Store";


ALTER SCHEMA "Store" OWNER TO postgres;

--
-- TOC entry 17 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2523 (class 0 OID 0)
-- Dependencies: 17
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 16 (class 2615 OID 16623)
-- Name: vrs; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA vrs;


ALTER SCHEMA vrs OWNER TO postgres;

SET search_path = "Inventory", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 207 (class 1259 OID 16624)
-- Name: ArmourEntity; Type: TABLE; Schema: Inventory; Owner: postgres
--

CREATE TABLE "ArmourEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "PrimaryAmmoAmount" smallint DEFAULT 0 NOT NULL,
    "SecondaryAmmoAmount" smallint DEFAULT 0 NOT NULL,
    "ItemModelId" smallint NOT NULL
);


ALTER TABLE "ArmourEntity" OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16632)
-- Name: PreSetEntity; Type: TABLE; Schema: Inventory; Owner: postgres
--

CREATE TABLE "PreSetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CharacterId" uuid NOT NULL,
    "HelmetId" uuid,
    "MaskId" uuid,
    "ArmourId" uuid,
    "BackpackId" uuid,
    "GlovesId" uuid,
    "PantsId" uuid,
    "BootsId" uuid,
    "PrimaryWeaponId" uuid,
    "SecondaryWeaponId" uuid,
    "PrimaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "PrimaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "PlayerId" uuid NOT NULL,
    "IsEnabled" boolean DEFAULT true NOT NULL,
    "Name" character varying(24),
    "IsUnlocked" boolean DEFAULT false NOT NULL,
    CONSTRAINT "PrimaryWeaponAmmo" CHECK ((("PrimaryWeaponAmmo_Primary" + "PrimaryWeaponAmmo_Secondary") > 0))
);


ALTER TABLE "PreSetEntity" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16641)
-- Name: WeaponEntity; Type: TABLE; Schema: Inventory; Owner: postgres
--

CREATE TABLE "WeaponEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "ItemModelId" smallint NOT NULL
);


ALTER TABLE "WeaponEntity" OWNER TO postgres;

SET search_path = "League", pg_catalog;

--
-- TOC entry 210 (class 1259 OID 16647)
-- Name: LeagueEntity; Type: TABLE; Schema: League; Owner: postgres
--

CREATE TABLE "LeagueEntity" (
    "LeagueId" uuid NOT NULL,
    "Cash_Money" smallint NOT NULL,
    "Cash_Donate" smallint NOT NULL,
    "Info_Name" character varying(20) NOT NULL,
    "Info_Abbr" character varying(5) NOT NULL,
    "Info_FoundedDate" timestamp without time zone NOT NULL,
    "Info_AccessType" smallint NOT NULL,
    "Info_JoinPrice" smallint NOT NULL
);


ALTER TABLE "LeagueEntity" OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16650)
-- Name: LeagueMemberEntity; Type: TABLE; Schema: League; Owner: postgres
--

CREATE TABLE "LeagueMemberEntity" (
    "LeagueId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Access" smallint NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "LeagueMemberEntity" OWNER TO postgres;

SET search_path = "Matches", pg_catalog;

--
-- TOC entry 230 (class 1259 OID 49808)
-- Name: MatchEntity; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchEntity" (
    "MatchId" uuid NOT NULL,
    "NodeId" uuid NOT NULL,
    "GameModeTypeId" smallint NOT NULL,
    "GameMapTypeId" smallint NOT NULL,
    "Created" timestamp without time zone NOT NULL,
    "Started" timestamp without time zone NOT NULL,
    "Finished" timestamp without time zone NOT NULL,
    "WinnerTeamId" uuid
);


ALTER TABLE "MatchEntity" OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 49819)
-- Name: MatchMember; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchMember" (
    "MemberId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "TeamId" uuid NOT NULL,
    "Level" smallint DEFAULT 1 NOT NULL,
    "MatchId" uuid NOT NULL,
    "Award_Money" smallint DEFAULT 0 NOT NULL,
    "Award_Experience" smallint DEFAULT 0 NOT NULL,
    "Award_Reputation" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "MatchMember" OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 49813)
-- Name: MatchTeam; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchTeam" (
    "TeamId" uuid NOT NULL,
    "MatchId" uuid NOT NULL
);


ALTER TABLE "MatchTeam" OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 49825)
-- Name: MemberAchievements; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MemberAchievements" (
    "AchievementId" uuid NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "AchievementCount" smallint NOT NULL,
    "MemberId" uuid NOT NULL
);


ALTER TABLE "MemberAchievements" OWNER TO postgres;

SET search_path = "Players", pg_catalog;

--
-- TOC entry 212 (class 1259 OID 16654)
-- Name: CharacterEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "CharacterEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "ItemModelId" smallint NOT NULL
);


ALTER TABLE "CharacterEntity" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16658)
-- Name: FriendEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "FriendEntity" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FriendId" uuid NOT NULL
);


ALTER TABLE "FriendEntity" OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16662)
-- Name: PlayerEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerName" character varying(24) NOT NULL,
    "Experience_Level" smallint DEFAULT 0 NOT NULL,
    "Experience_Experience" bigint DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "PremiumEndDate" timestamp without time zone,
    "LastActivitiDate" timestamp without time zone DEFAULT now() NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL,
    "CurrentFractionTypeId" smallint DEFAULT 1 NOT NULL,
    "Reputation_Keepers" bigint DEFAULT 0 NOT NULL,
    "Reputation_Keepers_Friend" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT_Friend" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "PlayerEntity" OWNER TO postgres;

SET search_path = "Store", pg_catalog;

--
-- TOC entry 215 (class 1259 OID 16674)
-- Name: CharacterArmourAssetEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "CharacterArmourAssetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ItemId" smallint NOT NULL,
    "CharacterId" smallint NOT NULL
);


ALTER TABLE "CharacterArmourAssetEntity" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16678)
-- Name: CharacterWeaponAssetEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "CharacterWeaponAssetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ItemId" smallint NOT NULL,
    "CharacterId" smallint NOT NULL
);


ALTER TABLE "CharacterWeaponAssetEntity" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16682)
-- Name: FractionEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "FractionEntity" (
    "FractionId" smallint NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" smallint DEFAULT 0 NOT NULL,
    "Discount_Ammo" smallint DEFAULT 0 NOT NULL,
    "Discount_Weapon" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FractionEntity" OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16690)
-- Name: ItemAmmoEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "ItemAmmoEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" smallint NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemAmmoEntity" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16699)
-- Name: ItemArmourEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "ItemArmourEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemArmourEntity" OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16708)
-- Name: ItemCharacterEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "ItemCharacterEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer DEFAULT 5000 NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemCharacterEntity" OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16718)
-- Name: ItemWeaponEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "ItemWeaponEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemWeaponEntity" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16727)
-- Name: ModificationEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "ModificationEntity" (
    "ModificationId" uuid NOT NULL,
    "ModificationTypeId" smallint DEFAULT 0 NOT NULL,
    "Range_Minimum" smallint DEFAULT 2 NOT NULL,
    "Range_Maximum" smallint DEFAULT 10 NOT NULL,
    "Range_Chance" smallint DEFAULT 50 NOT NULL
);


ALTER TABLE "ModificationEntity" OWNER TO postgres;

SET search_path = vrs, pg_catalog;

--
-- TOC entry 223 (class 1259 OID 16734)
-- Name: Deposits; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Deposits" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserId" uuid NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "Value" bigint DEFAULT 0 NOT NULL,
    "Currency" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "Deposits" OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 16740)
-- Name: Roles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Roles" (
    "Id" uuid NOT NULL,
    "Name" character varying(256) NOT NULL
);


ALTER TABLE "Roles" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16743)
-- Name: UserClaims; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserClaims" (
    "Id" integer NOT NULL,
    "ClaimType" character varying(256),
    "ClaimValue" character varying(256),
    "UserId" uuid NOT NULL
);


ALTER TABLE "UserClaims" OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 16749)
-- Name: UserClaims_Id_seq; Type: SEQUENCE; Schema: vrs; Owner: postgres
--

CREATE SEQUENCE "UserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "UserClaims_Id_seq" OWNER TO postgres;

--
-- TOC entry 2525 (class 0 OID 0)
-- Dependencies: 226
-- Name: UserClaims_Id_seq; Type: SEQUENCE OWNED BY; Schema: vrs; Owner: postgres
--

ALTER SEQUENCE "UserClaims_Id_seq" OWNED BY "UserClaims"."Id";


--
-- TOC entry 227 (class 1259 OID 16751)
-- Name: UserLogins; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserLogins" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "LoginProvider" character varying(128) NOT NULL,
    "ProviderKey" character varying(128) NOT NULL
);


ALTER TABLE "UserLogins" OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 16755)
-- Name: UserRoles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserRoles" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RoleId" uuid NOT NULL
);


ALTER TABLE "UserRoles" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16759)
-- Name: Users; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Users" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserName" character varying(256) NOT NULL,
    "PasswordHash" character varying(256),
    "SecurityStamp" uuid,
    "Email" character varying(256) DEFAULT NULL::character varying,
    "EmailConfirmed" boolean DEFAULT false NOT NULL,
    "PhoneNumber" character varying(32),
    "PhoneNumberConfirmed" boolean DEFAULT false NOT NULL,
    "TwoFactorEnabled" boolean DEFAULT false NOT NULL,
    "LockoutEndDateUtc" timestamp without time zone,
    "LockoutEnabled" boolean DEFAULT false NOT NULL,
    "AccessFailedCount" smallint DEFAULT 0 NOT NULL,
    "PlayerEntityId" bigint,
    "Balance" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "Users" OWNER TO postgres;

--
-- TOC entry 2252 (class 2604 OID 16773)
-- Name: Id; Type: DEFAULT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"UserClaims_Id_seq"'::regclass);


SET search_path = "Inventory", pg_catalog;

--
-- TOC entry 2492 (class 0 OID 16624)
-- Dependencies: 207
-- Data for Name: ArmourEntity; Type: TABLE DATA; Schema: Inventory; Owner: postgres
--

INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('1ed82a40-c8d2-43e2-98ef-236f9b16cc6d', 'f107b51f-e306-41bf-a8cc-73ab7e56d0d3', 0, 0, 0, 0, 0);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('c3197b76-d66e-4f33-8ab9-604502cba2da', 'f107b51f-e306-41bf-a8cc-73ab7e56d0d3', 0, 0, 0, 0, 1);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('0566d22e-cb23-4f82-b0b3-562bcde82f53', 'f107b51f-e306-41bf-a8cc-73ab7e56d0d3', 0, 0, 0, 0, 3);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('8f565d13-7bb4-4a20-9f2d-7140d3414962', 'f107b51f-e306-41bf-a8cc-73ab7e56d0d3', 0, 0, 0, 0, 2);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('1f848494-a64c-40ad-aa83-f3f56e00e129', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 0, 0, 2);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('24901de2-4844-4574-9852-215119a925d0', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 0, 0, 1);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('d056f396-f5fd-4c64-a3c5-f036748e00a5', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 0, 0, 3);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('dd18702a-b7a4-4708-874b-f2a3626689a1', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 0, 0, 0);


--
-- TOC entry 2493 (class 0 OID 16632)
-- Dependencies: 208
-- Data for Name: PreSetEntity; Type: TABLE DATA; Schema: Inventory; Owner: postgres
--

INSERT INTO "PreSetEntity" ("AssetId", "CharacterId", "HelmetId", "MaskId", "ArmourId", "BackpackId", "GlovesId", "PantsId", "BootsId", "PrimaryWeaponId", "SecondaryWeaponId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId", "IsEnabled", "Name", "IsUnlocked") VALUES ('19acdc1e-3b27-4d69-8a68-3d2967249457', 'b8e8e99d-1bb7-4250-96a0-4760b7a028df', 'dd18702a-b7a4-4708-874b-f2a3626689a1', NULL, '24901de2-4844-4574-9852-215119a925d0', '1f848494-a64c-40ad-aa83-f3f56e00e129', NULL, 'd056f396-f5fd-4c64-a3c5-f036748e00a5', NULL, 'e062711f-3388-4378-a3cc-d53303c38f78', NULL, 10, 0, 10, 0, '8d3b530c-30d3-44e5-9f00-b46a779141a4', false, NULL, false);
INSERT INTO "PreSetEntity" ("AssetId", "CharacterId", "HelmetId", "MaskId", "ArmourId", "BackpackId", "GlovesId", "PantsId", "BootsId", "PrimaryWeaponId", "SecondaryWeaponId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId", "IsEnabled", "Name", "IsUnlocked") VALUES ('0cb29f81-d5b0-4127-a3f3-79cbe265c2da', 'c165fa7d-c114-4889-8264-c92afa75c242', '1ed82a40-c8d2-43e2-98ef-236f9b16cc6d', NULL, 'c3197b76-d66e-4f33-8ab9-604502cba2da', '8f565d13-7bb4-4a20-9f2d-7140d3414962', NULL, '0566d22e-cb23-4f82-b0b3-562bcde82f53', NULL, '320ccc46-36a3-49af-ac88-b5edb5221548', NULL, 10, 2, 0, 0, 'f107b51f-e306-41bf-a8cc-73ab7e56d0d3', false, NULL, false);


--
-- TOC entry 2494 (class 0 OID 16641)
-- Dependencies: 209
-- Data for Name: WeaponEntity; Type: TABLE DATA; Schema: Inventory; Owner: postgres
--

INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('0793aeeb-8fe7-49e6-8cc7-77139c95ada1', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 3);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('7157e5fb-40f8-4185-acd1-8f86e981f705', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 7);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('994a079a-ffb5-4e42-b08b-9a9925bb39e2', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 5);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('c01d63d6-8272-4c63-84d6-829a63df2e62', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 6);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('cecfde4d-0208-4cf3-97c0-9ffb1ad771a0', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 1);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('e062711f-3388-4378-a3cc-d53303c38f78', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0, 0, 0);


SET search_path = "League", pg_catalog;

--
-- TOC entry 2495 (class 0 OID 16647)
-- Dependencies: 210
-- Data for Name: LeagueEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--

INSERT INTO "LeagueEntity" ("LeagueId", "Cash_Money", "Cash_Donate", "Info_Name", "Info_Abbr", "Info_FoundedDate", "Info_AccessType", "Info_JoinPrice") VALUES ('ff0ec814-a947-4dfb-9fb5-cc3d266d774b', 10, 100, 'VRS PRO', 'VRS', '2016-02-10 23:33:16.507', 1, 1000);


--
-- TOC entry 2496 (class 0 OID 16650)
-- Dependencies: 211
-- Data for Name: LeagueMemberEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--



SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2515 (class 0 OID 49808)
-- Dependencies: 230
-- Data for Name: MatchEntity; Type: TABLE DATA; Schema: Matches; Owner: postgres
--



--
-- TOC entry 2517 (class 0 OID 49819)
-- Dependencies: 232
-- Data for Name: MatchMember; Type: TABLE DATA; Schema: Matches; Owner: postgres
--



--
-- TOC entry 2516 (class 0 OID 49813)
-- Dependencies: 231
-- Data for Name: MatchTeam; Type: TABLE DATA; Schema: Matches; Owner: postgres
--



--
-- TOC entry 2518 (class 0 OID 49825)
-- Dependencies: 233
-- Data for Name: MemberAchievements; Type: TABLE DATA; Schema: Matches; Owner: postgres
--



SET search_path = "Players", pg_catalog;

--
-- TOC entry 2497 (class 0 OID 16654)
-- Dependencies: 212
-- Data for Name: CharacterEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "CharacterEntity" ("ItemId", "PlayerId", "ItemModelId") VALUES ('c165fa7d-c114-4889-8264-c92afa75c242', 'f107b51f-e306-41bf-a8cc-73ab7e56d0d3', 0);
INSERT INTO "CharacterEntity" ("ItemId", "PlayerId", "ItemModelId") VALUES ('b8e8e99d-1bb7-4250-96a0-4760b7a028df', '8d3b530c-30d3-44e5-9f00-b46a779141a4', 0);


--
-- TOC entry 2498 (class 0 OID 16658)
-- Dependencies: 213
-- Data for Name: FriendEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2499 (class 0 OID 16662)
-- Dependencies: 214
-- Data for Name: PlayerEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate", "CurrentFractionTypeId", "Reputation_Keepers", "Reputation_Keepers_Friend", "Reputation_RIFT", "Reputation_RIFT_Friend") VALUES ('f107b51f-e306-41bf-a8cc-73ab7e56d0d3', 'serinc', 15, 700, 0, 353500, 35000, '2016-04-01 11:16:17.200065', '2016-04-04 11:16:17.200065', '2016-04-01 08:16:17.246', '2016-04-01 11:16:17.200065', 0, 0, 0, 98985258, 3525568);
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate", "CurrentFractionTypeId", "Reputation_Keepers", "Reputation_Keepers_Friend", "Reputation_RIFT", "Reputation_RIFT_Friend") VALUES ('8d3b530c-30d3-44e5-9f00-b46a779141a4', 'Fdgdfgd', 0, 0, 0, 0, 0, '2016-05-04 03:22:46.995092', '2016-05-07 03:22:46.995092', '2016-05-04 03:23:42.202677', '2016-05-04 03:22:46.995092', 0, 775, 0, 7750000, 0);


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2500 (class 0 OID 16674)
-- Dependencies: 215
-- Data for Name: CharacterArmourAssetEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2501 (class 0 OID 16678)
-- Dependencies: 216
-- Data for Name: CharacterWeaponAssetEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2502 (class 0 OID 16682)
-- Dependencies: 217
-- Data for Name: FractionEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (1, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (2, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (3, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (4, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (0, 0, 0, 0, 0, 0);


--
-- TOC entry 2503 (class 0 OID 16690)
-- Dependencies: 218
-- Data for Name: ItemAmmoEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ItemAmmoEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (0, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemAmmoEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (1, 0, 7500, false, 0, 0, 0, 0);


--
-- TOC entry 2504 (class 0 OID 16699)
-- Dependencies: 219
-- Data for Name: ItemArmourEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ItemArmourEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (1, 0, 50000, false, 0, 0, 0, 0);
INSERT INTO "ItemArmourEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (2, 0, 50700, false, 0, 0, 0, 0);
INSERT INTO "ItemArmourEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (0, 0, 15000, false, 0, 0, 0, 0);
INSERT INTO "ItemArmourEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (3, 0, 58700, false, 0, 0, 0, 0);


--
-- TOC entry 2505 (class 0 OID 16708)
-- Dependencies: 220
-- Data for Name: ItemCharacterEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (0, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (1, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (2, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (3, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (4, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (5, 0, 5000, false, 0, 0, 0, 0);


--
-- TOC entry 2506 (class 0 OID 16718)
-- Dependencies: 221
-- Data for Name: ItemWeaponEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (0, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (6, 1, 175000, false, 0, 0, 0, 0);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (7, 1, 23543, false, 0, 0, 0, 0);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (1, 0, 5000, false, 0, 0, 0, 1);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (3, 0, 4500, false, 0, 0, 0, 2);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (8, 5, 14477, true, 0, 0, 0, 1);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (5, 0, 7000, false, 0, 0, 0, 2);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (2, 2, 3500, false, 0, 0, 0, 2);


--
-- TOC entry 2507 (class 0 OID 16727)
-- Dependencies: 222
-- Data for Name: ModificationEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



SET search_path = vrs, pg_catalog;

--
-- TOC entry 2508 (class 0 OID 16734)
-- Dependencies: 223
-- Data for Name: Deposits; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2509 (class 0 OID 16740)
-- Dependencies: 224
-- Data for Name: Roles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2510 (class 0 OID 16743)
-- Dependencies: 225
-- Data for Name: UserClaims; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2526 (class 0 OID 0)
-- Dependencies: 226
-- Name: UserClaims_Id_seq; Type: SEQUENCE SET; Schema: vrs; Owner: postgres
--

SELECT pg_catalog.setval('"UserClaims_Id_seq"', 2, true);


--
-- TOC entry 2512 (class 0 OID 16751)
-- Dependencies: 227
-- Data for Name: UserLogins; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "UserLogins" ("UserId", "LoginProvider", "ProviderKey") VALUES ('f125e168-9157-4fd8-a36d-9e88371dc1ce', 'Steam', 'http://steamcommunity.com/openid/id/76561198274135679');


--
-- TOC entry 2513 (class 0 OID 16755)
-- Dependencies: 228
-- Data for Name: UserRoles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2514 (class 0 OID 16759)
-- Dependencies: 229
-- Data for Name: Users; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('f107b51f-e306-41bf-a8cc-73ab7e56d0d3', 'serinc@vrs.com', 'AHy4QUybIPhl0BpG6gf1c31o/BEie72v4aJ1NMjKW5Z25f7k/lFjW2NN58+r5oC/4w==', 'fe0f1db2-b969-44e7-aa0c-f9e98b401daf', 'serinc@vrs.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('8d3b530c-30d3-44e5-9f00-b46a779141a4', 'serinc2@vrs.com', 'AKU6Y7Dh1RNjoNq1cDH41CpApgmIbX+63fTYfj5wU4uxkTCGfS2alHGH9Pj61r0+gA==', '98ca1461-e965-46a1-af25-cb0ce6f71815', 'serinc2@vrs.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('f125e168-9157-4fd8-a36d-9e88371dc1ce', 'sentike161rus2@gmail.com', NULL, '21e834a9-45de-41b2-ad18-c432d9d25c8a', 'sentike161rus2@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);


SET search_path = "Inventory", pg_catalog;

--
-- TOC entry 2268 (class 2606 OID 16775)
-- Name: ArmourEntity_PlayerId_ItemModelId_key; Type: CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_PlayerId_ItemModelId_key" UNIQUE ("PlayerId", "ItemModelId");


--
-- TOC entry 2270 (class 2606 OID 16777)
-- Name: ArmourEntity_pkey; Type: CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2274 (class 2606 OID 16779)
-- Name: ItemWeaponEntity_PlayerId_ItemModelId_key; Type: CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_PlayerId_ItemModelId_key" UNIQUE ("PlayerId", "ItemModelId");


--
-- TOC entry 2276 (class 2606 OID 16781)
-- Name: ItemWeaponEntity_pkey; Type: CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2272 (class 2606 OID 16783)
-- Name: PlayerAssetEnity_pkey; Type: CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PlayerAssetEnity_pkey" PRIMARY KEY ("AssetId");


SET search_path = "League", pg_catalog;

--
-- TOC entry 2278 (class 2606 OID 16785)
-- Name: LeagueEntity_Info_Abbr_key; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Abbr_key" UNIQUE ("Info_Abbr");


--
-- TOC entry 2280 (class 2606 OID 16787)
-- Name: LeagueEntity_Info_Name_key; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Name_key" UNIQUE ("Info_Name");


--
-- TOC entry 2282 (class 2606 OID 16789)
-- Name: LeagueEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_pkey" PRIMARY KEY ("LeagueId");


--
-- TOC entry 2284 (class 2606 OID 16791)
-- Name: LeagueMemberEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2334 (class 2606 OID 49892)
-- Name: MatchEntity_WinnerTeamId_key; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_key" UNIQUE ("WinnerTeamId");


--
-- TOC entry 2336 (class 2606 OID 49812)
-- Name: MatchEntity_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_pkey" PRIMARY KEY ("MatchId");


--
-- TOC entry 2340 (class 2606 OID 49846)
-- Name: MatchMember_PlayerId_TeamId_key; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_TeamId_key" UNIQUE ("PlayerId", "TeamId");


--
-- TOC entry 2342 (class 2606 OID 49824)
-- Name: MatchMember_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_pkey" PRIMARY KEY ("MemberId");


--
-- TOC entry 2338 (class 2606 OID 49818)
-- Name: MatchTeam_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_pkey" PRIMARY KEY ("TeamId");


--
-- TOC entry 2344 (class 2606 OID 49829)
-- Name: MemberAchievements_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_pkey" PRIMARY KEY ("AchievementId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2286 (class 2606 OID 16793)
-- Name: CharacterEntity_PlayerId_ModelId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterEntity_PlayerId_ModelId_key" UNIQUE ("PlayerId", "ItemModelId");


--
-- TOC entry 2288 (class 2606 OID 16795)
-- Name: CharacterItemEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterItemEntity_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2290 (class 2606 OID 16797)
-- Name: FriendEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2292 (class 2606 OID 16799)
-- Name: PlayerEntity_PlayerName_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerName_key" UNIQUE ("PlayerName");


--
-- TOC entry 2294 (class 2606 OID 16801)
-- Name: PlayerEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2296 (class 2606 OID 16803)
-- Name: CharacterArmourAssetEntity_ItemId_CharacterId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_ItemId_CharacterId_key" UNIQUE ("ItemId", "CharacterId");


--
-- TOC entry 2298 (class 2606 OID 16805)
-- Name: CharacterArmourAssetEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_pkey" PRIMARY KEY ("AssetId");


--
-- TOC entry 2310 (class 2606 OID 16807)
-- Name: CharacterEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemCharacterEntity"
    ADD CONSTRAINT "CharacterEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2300 (class 2606 OID 16809)
-- Name: CharacterWeaponAssetEntity_ItemId_CharacterId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_ItemId_CharacterId_key" UNIQUE ("ItemId", "CharacterId");


--
-- TOC entry 2302 (class 2606 OID 16811)
-- Name: CharacterWeaponAssetEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_pkey" PRIMARY KEY ("AssetId");


--
-- TOC entry 2304 (class 2606 OID 16813)
-- Name: FractionEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "FractionEntity_pkey" PRIMARY KEY ("FractionId");


--
-- TOC entry 2306 (class 2606 OID 16815)
-- Name: ItemAmmoEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemAmmoEntity"
    ADD CONSTRAINT "ItemAmmoEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2308 (class 2606 OID 16817)
-- Name: ItemArmourBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemArmourEntity"
    ADD CONSTRAINT "ItemArmourBaseEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2312 (class 2606 OID 16819)
-- Name: ItemWeaponBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemWeaponEntity"
    ADD CONSTRAINT "ItemWeaponBaseEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2314 (class 2606 OID 16821)
-- Name: ModificatioBaseEntity_ModificationTypeId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_ModificationTypeId_key" UNIQUE ("ModificationTypeId");


--
-- TOC entry 2316 (class 2606 OID 16823)
-- Name: ModificatioBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_pkey" PRIMARY KEY ("ModificationId");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2318 (class 2606 OID 16825)
-- Name: Deposits_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Deposits"
    ADD CONSTRAINT "Deposits_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2320 (class 2606 OID 16827)
-- Name: Roles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2323 (class 2606 OID 16829)
-- Name: UserClaims_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "UserClaims_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2326 (class 2606 OID 16831)
-- Name: UserLogins_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "UserLogins_pkey" PRIMARY KEY ("UserId", "LoginProvider", "ProviderKey");


--
-- TOC entry 2330 (class 2606 OID 16833)
-- Name: UserRoles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "UserRoles_pkey" PRIMARY KEY ("UserId", "RoleId");


--
-- TOC entry 2332 (class 2606 OID 16835)
-- Name: Users_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2321 (class 1259 OID 16836)
-- Name: IX_UserClaims_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserClaims_UserId" ON "UserClaims" USING btree ("UserId");


--
-- TOC entry 2324 (class 1259 OID 16837)
-- Name: IX_UserLogins_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserLogins_UserId" ON "UserLogins" USING btree ("UserId");


--
-- TOC entry 2327 (class 1259 OID 16838)
-- Name: IX_UserRoles_RoleId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserRoles_RoleId" ON "UserRoles" USING btree ("RoleId");


--
-- TOC entry 2328 (class 1259 OID 16839)
-- Name: IX_UserRoles_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserRoles_UserId" ON "UserRoles" USING btree ("UserId");


SET search_path = "Inventory", pg_catalog;

--
-- TOC entry 2345 (class 2606 OID 16840)
-- Name: ArmourEntity_ItemModelId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_ItemModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemArmourEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2346 (class 2606 OID 16845)
-- Name: ArmourEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2350 (class 2606 OID 16850)
-- Name: InvertoryItemBaseEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "InvertoryItemBaseEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2351 (class 2606 OID 16855)
-- Name: ItemWeaponEntity_ItemModelId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_ItemModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemWeaponEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2347 (class 2606 OID 16860)
-- Name: PlayerAssetEnity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PlayerAssetEnity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "Players"."CharacterEntity"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2348 (class 2606 OID 16865)
-- Name: PreSetEntity_HelmetId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PreSetEntity_HelmetId_fkey" FOREIGN KEY ("HelmetId") REFERENCES "ArmourEntity"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2349 (class 2606 OID 16870)
-- Name: PreSetEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PreSetEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "League", pg_catalog;

--
-- TOC entry 2352 (class 2606 OID 16875)
-- Name: LeagueMemberEntity_LeagueId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_LeagueId_fkey" FOREIGN KEY ("LeagueId") REFERENCES "LeagueEntity"("LeagueId") ON DELETE CASCADE;


--
-- TOC entry 2353 (class 2606 OID 16880)
-- Name: LeagueMemberEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2372 (class 2606 OID 49886)
-- Name: MatchEntity_WinnerTeamId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_fkey" FOREIGN KEY ("WinnerTeamId") REFERENCES "MatchTeam"("TeamId");


--
-- TOC entry 2376 (class 2606 OID 49870)
-- Name: MatchMember_MatchId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2374 (class 2606 OID 49835)
-- Name: MatchMember_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2375 (class 2606 OID 49840)
-- Name: MatchMember_TeamId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_TeamId_fkey" FOREIGN KEY ("TeamId") REFERENCES "MatchTeam"("TeamId") ON DELETE CASCADE;


--
-- TOC entry 2373 (class 2606 OID 49830)
-- Name: MatchTeam_MatchId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2377 (class 2606 OID 49847)
-- Name: MemberAchievements_MemberId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_MemberId_fkey" FOREIGN KEY ("MemberId") REFERENCES "MatchMember"("MemberId") ON DELETE CASCADE;


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2354 (class 2606 OID 16885)
-- Name: CharacterEntity_ModelId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterEntity_ModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemCharacterEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2355 (class 2606 OID 16890)
-- Name: CharacterEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2356 (class 2606 OID 16895)
-- Name: FriendEntity_FriendId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_FriendId_fkey" FOREIGN KEY ("FriendId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2357 (class 2606 OID 16900)
-- Name: FriendEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2359 (class 2606 OID 66289)
-- Name: PlayerEntity_FractionTypeId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_FractionTypeId_fkey" FOREIGN KEY ("CurrentFractionTypeId") REFERENCES "Store"."FractionEntity"("FractionId");


--
-- TOC entry 2358 (class 2606 OID 16905)
-- Name: PlayerEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES vrs."Users"("Id") ON DELETE CASCADE;


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2360 (class 2606 OID 16910)
-- Name: CharacterArmourAssetEntity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "ItemCharacterEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2361 (class 2606 OID 16915)
-- Name: CharacterArmourAssetEntity_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "ItemArmourEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2362 (class 2606 OID 16920)
-- Name: CharacterWeaponAssetEntity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "ItemCharacterEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2363 (class 2606 OID 16925)
-- Name: CharacterWeaponAssetEntity_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "ItemWeaponEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2364 (class 2606 OID 16930)
-- Name: ItemAmmoEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemAmmoEntity"
    ADD CONSTRAINT "ItemAmmoEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2365 (class 2606 OID 16935)
-- Name: ItemArmourEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemArmourEntity"
    ADD CONSTRAINT "ItemArmourEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2366 (class 2606 OID 16940)
-- Name: ItemCharacterEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemCharacterEntity"
    ADD CONSTRAINT "ItemCharacterEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2367 (class 2606 OID 16945)
-- Name: ItemWeaponEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemWeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2368 (class 2606 OID 16950)
-- Name: FK_UserClaims_Users_User_Id; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "FK_UserClaims_Users_User_Id" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2369 (class 2606 OID 16955)
-- Name: FK_UserLogins_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "FK_UserLogins_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2370 (class 2606 OID 16960)
-- Name: FK_UserRoles_Roles_RoleId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Roles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "Roles"("Id") ON DELETE CASCADE;


--
-- TOC entry 2371 (class 2606 OID 16965)
-- Name: FK_UserRoles_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2524 (class 0 OID 0)
-- Dependencies: 17
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-05-16 02:43:24

--
-- PostgreSQL database dump complete
--

