--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-07-19 02:14:53

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- TOC entry 2809 (class 1262 OID 12373)
-- Name: postgres; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


ALTER DATABASE postgres OWNER TO postgres;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2810 (class 1262 OID 12373)
-- Dependencies: 2809
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- TOC entry 17 (class 2615 OID 16619)
-- Name: Achievements; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Achievements";


ALTER SCHEMA "Achievements" OWNER TO postgres;

--
-- TOC entry 19 (class 2615 OID 215314)
-- Name: GameMode; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "GameMode";


ALTER SCHEMA "GameMode" OWNER TO postgres;

--
-- TOC entry 11 (class 2615 OID 16620)
-- Name: League; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "League";


ALTER SCHEMA "League" OWNER TO postgres;

--
-- TOC entry 16 (class 2615 OID 49807)
-- Name: Matches; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Matches";


ALTER SCHEMA "Matches" OWNER TO postgres;

--
-- TOC entry 12 (class 2615 OID 16621)
-- Name: Players; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Players";


ALTER SCHEMA "Players" OWNER TO postgres;

--
-- TOC entry 18 (class 2615 OID 214686)
-- Name: Promo; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Promo";


ALTER SCHEMA "Promo" OWNER TO postgres;

--
-- TOC entry 13 (class 2615 OID 16622)
-- Name: Store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Store";


ALTER SCHEMA "Store" OWNER TO postgres;

--
-- TOC entry 15 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2811 (class 0 OID 0)
-- Dependencies: 15
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 14 (class 2615 OID 16623)
-- Name: vrs; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA vrs;


ALTER SCHEMA vrs OWNER TO postgres;

--
-- TOC entry 2 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2813 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 1 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 2814 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 3 (class 3079 OID 16582)
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- TOC entry 2815 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = "Achievements", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 223 (class 1259 OID 91111)
-- Name: AchievementContainer; Type: TABLE; Schema: Achievements; Owner: postgres
--

CREATE TABLE "AchievementContainer" (
    "AchievementTypeId" smallint NOT NULL
);


ALTER TABLE "AchievementContainer" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 91118)
-- Name: AchievementInstance; Type: TABLE; Schema: Achievements; Owner: postgres
--

CREATE TABLE "AchievementInstance" (
    "AchievementBonusId" integer NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "Amount" integer DEFAULT 0 NOT NULL,
    "BonusPackId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "AchievementInstance" OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 91116)
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE; Schema: Achievements; Owner: postgres
--

CREATE SEQUENCE "AchievementInstance_AchievementBonusId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AchievementInstance_AchievementBonusId_seq" OWNER TO postgres;

--
-- TOC entry 2816 (class 0 OID 0)
-- Dependencies: 224
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE OWNED BY; Schema: Achievements; Owner: postgres
--

ALTER SEQUENCE "AchievementInstance_AchievementBonusId_seq" OWNED BY "AchievementInstance"."AchievementBonusId";


SET search_path = "GameMode", pg_catalog;

--
-- TOC entry 246 (class 1259 OID 215315)
-- Name: AbstractGameModeEntity; Type: TABLE; Schema: GameMode; Owner: postgres
--

CREATE TABLE "AbstractGameModeEntity" (
    "GameModeId" smallint NOT NULL,
    "Level_Min" smallint DEFAULT 0 NOT NULL,
    "Level_Max" smallint DEFAULT 10 NOT NULL
);


ALTER TABLE "AbstractGameModeEntity" OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 215322)
-- Name: GameMapEntity; Type: TABLE; Schema: GameMode; Owner: postgres
--

CREATE TABLE "GameMapEntity" (
    "InstanceId" smallint NOT NULL,
    "GameModeId" smallint DEFAULT 0 NOT NULL,
    "MapId" smallint DEFAULT 0 NOT NULL,
    "Members_Min" smallint DEFAULT 16 NOT NULL,
    "Members_Max" smallint DEFAULT 128 NOT NULL,
    "RoundTimeInSeconds" smallint DEFAULT 600 NOT NULL,
    CONSTRAINT "GameMapEntity_Members_Min_check" CHECK (("Members_Min" >= 2)),
    CONSTRAINT "GameMapEntity_RoundTimeInSeconds_check" CHECK (("RoundTimeInSeconds" > 60)),
    CONSTRAINT "GameMapEntity_check" CHECK (("Members_Max" > "Members_Min"))
);


ALTER TABLE "GameMapEntity" OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 215343)
-- Name: TeamGameModeEntity; Type: TABLE; Schema: GameMode; Owner: postgres
--

CREATE TABLE "TeamGameModeEntity" (
    "GameModeId" smallint NOT NULL
);


ALTER TABLE "TeamGameModeEntity" OWNER TO postgres;

SET search_path = "League", pg_catalog;

--
-- TOC entry 194 (class 1259 OID 16647)
-- Name: LeagueEntity; Type: TABLE; Schema: League; Owner: postgres
--

CREATE TABLE "LeagueEntity" (
    "LeagueId" uuid NOT NULL,
    "Cash_Money" smallint NOT NULL,
    "Cash_Donate" smallint NOT NULL,
    "Info_Name" character varying(20) NOT NULL,
    "Info_Abbr" character varying(5) NOT NULL,
    "Info_FoundedDate" timestamp without time zone NOT NULL,
    "Info_AccessType" smallint NOT NULL,
    "Info_JoinPrice" smallint NOT NULL
);


ALTER TABLE "LeagueEntity" OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 16650)
-- Name: LeagueMemberEntity; Type: TABLE; Schema: League; Owner: postgres
--

CREATE TABLE "LeagueMemberEntity" (
    "LeagueId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Access" smallint NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "LeagueMemberEntity" OWNER TO postgres;

SET search_path = "Matches", pg_catalog;

--
-- TOC entry 207 (class 1259 OID 49808)
-- Name: MatchEntity; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchEntity" (
    "MatchId" uuid NOT NULL,
    "NodeId" uuid,
    "GameModeTypeId" smallint NOT NULL,
    "GameMapTypeId" smallint NOT NULL,
    "Created" timestamp without time zone NOT NULL,
    "Started" timestamp without time zone NOT NULL,
    "Finished" timestamp without time zone NOT NULL,
    "WinnerTeamId" uuid,
    "NumberOfBots" smallint DEFAULT 0 NOT NULL,
    "RoundTimeInSeconds" smallint DEFAULT 600 NOT NULL
);


ALTER TABLE "MatchEntity" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 49819)
-- Name: MatchMember; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchMember" (
    "MemberId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "TeamId" uuid NOT NULL,
    "Level" smallint DEFAULT 1 NOT NULL,
    "MatchId" uuid NOT NULL,
    "Award_Money" smallint DEFAULT 0 NOT NULL,
    "Award_Experience" smallint DEFAULT 0 NOT NULL,
    "Award_Reputation" smallint DEFAULT 0 NOT NULL,
    "Award_FractionId" smallint DEFAULT 0 NOT NULL,
    "Squad" smallint NOT NULL,
    "Statistic_Kills" bigint DEFAULT 0 NOT NULL,
    "Statistic_Deads" bigint DEFAULT 0 NOT NULL,
    "Statistic_Assists" bigint DEFAULT 0 NOT NULL,
    "Statistic_TeamKills" bigint DEFAULT 0 NOT NULL,
    "Statistic_Shots" bigint DEFAULT 0 NOT NULL,
    "Statistic_Hits" bigint DEFAULT 0 NOT NULL,
    "Statistic_IncomingDamage" bigint DEFAULT 0 NOT NULL,
    "Statistic_OutgoingDamage" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_1" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_2" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_3" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_4" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_5" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "MatchMember" OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 49813)
-- Name: MatchTeam; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchTeam" (
    "TeamId" uuid NOT NULL,
    "MatchId" uuid NOT NULL
);


ALTER TABLE "MatchTeam" OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 49825)
-- Name: MemberAchievements; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MemberAchievements" (
    "AchievementId" uuid NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "AchievementCount" smallint NOT NULL,
    "MemberId" uuid NOT NULL
);


ALTER TABLE "MemberAchievements" OWNER TO postgres;

SET search_path = "Players", pg_catalog;

--
-- TOC entry 197 (class 1259 OID 16658)
-- Name: FriendEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "FriendEntity" (
    "MemberId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FriendId" uuid NOT NULL,
    "State" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FriendEntity" OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 115889)
-- Name: InstanceOfInstalledAddon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfInstalledAddon" (
    "AddonId" uuid NOT NULL,
    "ItemId" uuid,
    "AddonSlot" smallint NOT NULL,
    "ContainerId" uuid NOT NULL
);


ALTER TABLE "InstanceOfInstalledAddon" OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 107659)
-- Name: InstanceOfItemAddon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfItemAddon" (
    "ItemId" uuid NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value smallint DEFAULT 0,
    modification_2_value smallint DEFAULT 0,
    modification_3_value smallint DEFAULT 0,
    modification_4_value smallint DEFAULT 0,
    modification_5_value smallint DEFAULT 0
);


ALTER TABLE "InstanceOfItemAddon" OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 107654)
-- Name: InstanceOfItemSkin; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfItemSkin" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfItemSkin" OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 91015)
-- Name: InstanceOfPlayerArmour; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerArmour" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfPlayerArmour" OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16654)
-- Name: InstanceOfPlayerCharacter; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerCharacter" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "InstanceOfPlayerCharacter" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 90987)
-- Name: InstanceOfPlayerItem; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerItem" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "ModelId" smallint NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value real DEFAULT 0 NOT NULL,
    modification_2_value real DEFAULT 0 NOT NULL,
    modification_3_value real DEFAULT 0 NOT NULL,
    modification_4_value real DEFAULT 0 NOT NULL,
    modification_5_value real DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "InstanceOfPlayerItem" OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 214748)
-- Name: InstanceOfPlayerKit; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerKit" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfPlayerKit" OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 91032)
-- Name: InstanceOfPlayerWeapon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerWeapon" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfPlayerWeapon" OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 91170)
-- Name: InstanceOfTargetItem; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfTargetItem" (
    "ItemId" uuid NOT NULL,
    "TargetPlayerItemId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "InstanceOfTargetItem" OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 91126)
-- Name: PlayerAchievement; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerAchievement" (
    "PlayerAchievementId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "LastAchievementInstanceId" smallint,
    "Amount" smallint NOT NULL
);


ALTER TABLE "PlayerAchievement" OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16662)
-- Name: PlayerEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerName" character varying(64) NOT NULL,
    "Experience_Level" smallint DEFAULT 0 NOT NULL,
    "Experience_Experience" bigint DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "PremiumEndDate" timestamp without time zone,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL,
    "CurrentFractionId" uuid,
    "Experience_LastLevel" smallint DEFAULT 0 NOT NULL,
    "WeekBonus_JoinDate" timestamp without time zone DEFAULT now() NOT NULL,
    "WeekBonus_JoinCount" smallint DEFAULT 0 NOT NULL,
    "WeekBonus_NotifyDate" timestamp without time zone DEFAULT now() NOT NULL,
    "FirstPartnerId" uuid,
    "MatchMemberId" uuid,
    "Statistic_Kills" bigint DEFAULT 0 NOT NULL,
    "Statistic_Deads" bigint DEFAULT 0 NOT NULL,
    "Statistic_Assists" bigint DEFAULT 0 NOT NULL,
    "Statistic_TeamKills" bigint DEFAULT 0 NOT NULL,
    "Statistic_Shots" bigint DEFAULT 0 NOT NULL,
    "Statistic_Hits" bigint DEFAULT 0 NOT NULL,
    "Statistic_IncomingDamage" bigint DEFAULT 0 NOT NULL,
    "Statistic_OutgoingDamage" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_1" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_2" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_3" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_4" bigint DEFAULT 0 NOT NULL,
    "Statistic_Reserved_5" bigint DEFAULT 0 NOT NULL,
    "QueueMemberId" uuid,
    CONSTRAINT "PlayerEntity_Experience_Level_check" CHECK (("Experience_Level" < 250)),
    CONSTRAINT "PlayerEntity_Experience_WeekJoinCount_check" CHECK (("WeekBonus_JoinCount" < 8))
);


ALTER TABLE "PlayerEntity" OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 16632)
-- Name: PlayerProfileEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerProfileEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CharacterId" uuid NOT NULL,
    "PrimaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "PrimaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "PlayerId" uuid NOT NULL,
    "IsEnabled" boolean DEFAULT true NOT NULL,
    "Name" character varying(24),
    "IsUnlocked" boolean DEFAULT false NOT NULL
);


ALTER TABLE "PlayerProfileEntity" OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 214715)
-- Name: PlayerPromoCode; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerPromoCode" (
    "PlayerCodeId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "CodeId" uuid NOT NULL,
    "ActivationDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "PlayerPromoCode" OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 149147)
-- Name: PlayerReputationEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerReputationEntity" (
    "ReputationId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FractionId" smallint NOT NULL,
    "CurrentRank" smallint NOT NULL,
    "LastRank" smallint NOT NULL,
    "Reputation" bigint NOT NULL,
    CONSTRAINT "PlayerReputationEntity_CurrentRank_check" CHECK (("CurrentRank" <= 10)),
    CONSTRAINT "PlayerReputationEntity_FractionId_check" CHECK (("FractionId" < 5)),
    CONSTRAINT "PlayerReputationEntity_LastRank_check" CHECK (("LastRank" <= 10))
);


ALTER TABLE "PlayerReputationEntity" OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 214753)
-- Name: ProfileItemEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "ProfileItemEntity" (
    "ProfileItemId" uuid NOT NULL,
    "ProfileId" uuid NOT NULL,
    "ItemId" uuid NOT NULL,
    "SlotId" smallint NOT NULL
);


ALTER TABLE "ProfileItemEntity" OWNER TO postgres;

SET search_path = "Promo", pg_catalog;

--
-- TOC entry 236 (class 1259 OID 214698)
-- Name: Codes; Type: TABLE; Schema: Promo; Owner: postgres
--

CREATE TABLE "Codes" (
    "PromoId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PartnerId" uuid NOT NULL,
    "Name" character varying(64) NOT NULL,
    "Validity" timestamp without time zone DEFAULT now(),
    "Confirmed" boolean DEFAULT true NOT NULL
);


ALTER TABLE "Codes" OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 214693)
-- Name: Partners; Type: TABLE; Schema: Promo; Owner: postgres
--

CREATE TABLE "Partners" (
    "PartnerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" character varying(128) NOT NULL,
    "Login" character varying(32) NOT NULL,
    "Password" character varying(32) NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "Partners" OWNER TO postgres;

SET search_path = "Store", pg_catalog;

--
-- TOC entry 214 (class 1259 OID 90937)
-- Name: AbstractItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AbstractItemInstance" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 1 NOT NULL,
    "IsAvalible" boolean DEFAULT true NOT NULL,
    "DefaultSkinId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "AbstractItemInstance" OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 107613)
-- Name: AddonItemContainer; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AddonItemContainer" (
    "ContainerId" integer NOT NULL,
    "ModelId" smallint NOT NULL,
    "ItemId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    "ModelCategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AddonItemContainer" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 107611)
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE; Schema: Store; Owner: postgres
--

CREATE SEQUENCE "AddonItemContainer_ContainerId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AddonItemContainer_ContainerId_seq" OWNER TO postgres;

--
-- TOC entry 2817 (class 0 OID 0)
-- Dependencies: 229
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE OWNED BY; Schema: Store; Owner: postgres
--

ALTER SEQUENCE "AddonItemContainer_ContainerId_seq" OWNED BY "AddonItemContainer"."ContainerId";


--
-- TOC entry 228 (class 1259 OID 107606)
-- Name: AddonItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AddonItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AddonItemInstance" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 91071)
-- Name: AmmoItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "AmmoItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AmmoItemInstance" OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 90955)
-- Name: ArmourItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "ArmourItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "ArmourItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 1))
);


ALTER TABLE "ArmourItemInstance" OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 90980)
-- Name: CharacterItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "CharacterItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "CharacterItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 5))
);


ALTER TABLE "CharacterItemInstance" OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16682)
-- Name: FractionEntity; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "FractionEntity" (
    "FractionId" smallint NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" smallint DEFAULT 0 NOT NULL,
    "Discount_Ammo" smallint DEFAULT 0 NOT NULL,
    "Discount_Weapon" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FractionEntity" OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 214787)
-- Name: KitItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "KitItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "KitItemInstance" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 90974)
-- Name: SkinItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "SkinItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "SkinItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 7))
);


ALTER TABLE "SkinItemInstance" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 90967)
-- Name: WeaponItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "WeaponItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "WeaponItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 0))
);


ALTER TABLE "WeaponItemInstance" OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- TOC entry 244 (class 1259 OID 214952)
-- Name: ClusterInstanceEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "ClusterInstanceEntity" (
    "ClusterId" uuid NOT NULL,
    "MachineName" character varying(128) NOT NULL,
    "MachineAddress" inet NOT NULL,
    "RegistrationDate" timestamp without time zone NOT NULL,
    "LastActivityDate" timestamp without time zone NOT NULL,
    "State" smallint NOT NULL,
    "LimitActiveNodes" smallint NOT NULL,
    "ShoutDownReaspon" smallint DEFAULT 0 NOT NULL,
    "RegionTypeId" smallint DEFAULT 0 NOT NULL,
    "UpdateStatus" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ClusterInstanceEntity" OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 214960)
-- Name: ClusterNodeEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "ClusterNodeEntity" (
    "NodeId" uuid NOT NULL,
    "ClusterId" uuid NOT NULL,
    "State" smallint NOT NULL,
    "Address_Host" inet NOT NULL,
    "Address_Port" smallint NOT NULL,
    "RegistrationDate" timestamp without time zone NOT NULL,
    "LastActivityDate" timestamp without time zone NOT NULL,
    "ActiveMatchId" uuid,
    "ShutDownDate" timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT "ClusterNodeEntity_Port_check" CHECK ((("Address_Port" > 7000) AND ("Address_Port" < 10000)))
);


ALTER TABLE "ClusterNodeEntity" OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 214873)
-- Name: GameSessionEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE UNLOGGED TABLE "GameSessionEntity" (
    "SessionId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "LastActivityDate" timestamp without time zone NOT NULL,
    "LastMessageAccess" timestamp without time zone NOT NULL
);


ALTER TABLE "GameSessionEntity" OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 66321)
-- Name: GlobalChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE UNLOGGED TABLE "GlobalChatMessage" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "EditorId" uuid,
    "LanguageId" smallint NOT NULL
);


ALTER TABLE "GlobalChatMessage" OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 66326)
-- Name: PrivateChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE UNLOGGED TABLE "PrivateChatMessage" (
    "MessageId" uuid NOT NULL,
    "MessageDataId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL
);


ALTER TABLE "PrivateChatMessage" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 66331)
-- Name: PrivateChatMessageData; Type: TABLE; Schema: public; Owner: postgres
--

CREATE UNLOGGED TABLE "PrivateChatMessageData" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "ReceiverId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "IsReaded" boolean NOT NULL
);


ALTER TABLE "PrivateChatMessageData" OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 214885)
-- Name: SessionQueueEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "SessionQueueEntity" (
    "PlayerId" uuid NOT NULL,
    "GameModeTypeId" smallint DEFAULT 1 NOT NULL,
    "RegionTypeId" smallint DEFAULT 1 NOT NULL,
    "MatchId" uuid,
    "CheckDate" timestamp without time zone DEFAULT now() NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT now() NOT NULL,
    "Level_Min" smallint DEFAULT 0 NOT NULL,
    "Level_Max" smallint DEFAULT 0 NOT NULL,
    "Ready" boolean DEFAULT false NOT NULL,
    "QueueId" uuid NOT NULL
);


ALTER TABLE "SessionQueueEntity" OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 214892)
-- Name: SessionQueueMember; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "SessionQueueMember" (
    "QueueId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Ready" boolean DEFAULT false NOT NULL,
    "QueueMemberId" uuid DEFAULT gen_random_uuid() NOT NULL
);


ALTER TABLE "SessionQueueMember" OWNER TO postgres;

SET search_path = vrs, pg_catalog;

--
-- TOC entry 200 (class 1259 OID 16734)
-- Name: Deposits; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Deposits" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserId" uuid NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "Value" bigint DEFAULT 0 NOT NULL,
    "Currency" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "Deposits" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16740)
-- Name: Roles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Roles" (
    "Id" uuid NOT NULL,
    "Name" character varying(256) NOT NULL
);


ALTER TABLE "Roles" OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16743)
-- Name: UserClaims; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserClaims" (
    "Id" integer NOT NULL,
    "ClaimType" character varying(256),
    "ClaimValue" character varying(256),
    "UserId" uuid NOT NULL
);


ALTER TABLE "UserClaims" OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16749)
-- Name: UserClaims_Id_seq; Type: SEQUENCE; Schema: vrs; Owner: postgres
--

CREATE SEQUENCE "UserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "UserClaims_Id_seq" OWNER TO postgres;

--
-- TOC entry 2818 (class 0 OID 0)
-- Dependencies: 203
-- Name: UserClaims_Id_seq; Type: SEQUENCE OWNED BY; Schema: vrs; Owner: postgres
--

ALTER SEQUENCE "UserClaims_Id_seq" OWNED BY "UserClaims"."Id";


--
-- TOC entry 204 (class 1259 OID 16751)
-- Name: UserLogins; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserLogins" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "LoginProvider" character varying(128) NOT NULL,
    "ProviderKey" character varying(128) NOT NULL
);


ALTER TABLE "UserLogins" OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16755)
-- Name: UserRoles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserRoles" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RoleId" uuid NOT NULL
);


ALTER TABLE "UserRoles" OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16759)
-- Name: Users; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Users" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserName" character varying(256) NOT NULL,
    "PasswordHash" character varying(256),
    "SecurityStamp" uuid,
    "Email" character varying(256) DEFAULT NULL::character varying,
    "EmailConfirmed" boolean DEFAULT false NOT NULL,
    "PhoneNumber" character varying(32),
    "PhoneNumberConfirmed" boolean DEFAULT false NOT NULL,
    "TwoFactorEnabled" boolean DEFAULT false NOT NULL,
    "LockoutEndDateUtc" timestamp without time zone,
    "LockoutEnabled" boolean DEFAULT false NOT NULL,
    "AccessFailedCount" smallint DEFAULT 0 NOT NULL,
    "Balance" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "Users" OWNER TO postgres;

SET search_path = "Achievements", pg_catalog;

--
-- TOC entry 2344 (class 2604 OID 91121)
-- Name: AchievementBonusId; Type: DEFAULT; Schema: Achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementInstance" ALTER COLUMN "AchievementBonusId" SET DEFAULT nextval('"AchievementInstance_AchievementBonusId_seq"'::regclass);


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2349 (class 2604 OID 107616)
-- Name: ContainerId; Type: DEFAULT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer" ALTER COLUMN "ContainerId" SET DEFAULT nextval('"AddonItemContainer_ContainerId_seq"'::regclass);


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2291 (class 2604 OID 16773)
-- Name: Id; Type: DEFAULT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"UserClaims_Id_seq"'::regclass);


SET search_path = "Achievements", pg_catalog;

--
-- TOC entry 2779 (class 0 OID 91111)
-- Dependencies: 223
-- Data for Name: AchievementContainer; Type: TABLE DATA; Schema: Achievements; Owner: postgres
--

INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (0);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (1);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (2);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (3);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (4);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (5);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (6);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (7);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (8);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (9);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (10);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (11);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (12);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (13);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (14);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (15);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (16);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (17);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (18);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (19);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (20);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (21);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (22);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (23);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (24);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (25);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (26);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (27);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (28);
INSERT INTO "AchievementContainer" ("AchievementTypeId") VALUES (29);


--
-- TOC entry 2781 (class 0 OID 91118)
-- Dependencies: 225
-- Data for Name: AchievementInstance; Type: TABLE DATA; Schema: Achievements; Owner: postgres
--

INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (13, 0, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (14, 0, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (15, 0, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (16, 0, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (17, 0, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (18, 0, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (19, 0, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (20, 1, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (21, 1, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (22, 1, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (23, 1, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (24, 1, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (25, 1, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (26, 1, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (27, 2, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (28, 2, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (29, 2, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (30, 2, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (31, 2, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (32, 2, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (33, 2, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (34, 3, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (35, 3, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (36, 3, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (37, 3, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (38, 3, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (39, 3, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (40, 3, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (41, 4, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (42, 4, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (43, 4, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (44, 4, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (45, 4, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (46, 4, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (47, 4, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (48, 5, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (49, 5, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (50, 5, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (51, 5, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (52, 5, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (53, 5, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (54, 5, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (55, 6, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (56, 6, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (57, 6, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (58, 6, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (59, 6, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (60, 6, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (61, 6, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (62, 7, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (63, 7, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (64, 7, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (65, 7, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (66, 7, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (67, 7, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (68, 7, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (69, 8, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (70, 8, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (71, 8, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (72, 8, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (73, 8, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (74, 8, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (75, 8, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (76, 9, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (77, 9, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (78, 9, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (79, 9, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (80, 9, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (81, 9, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (82, 9, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (83, 10, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (84, 10, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (85, 10, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (86, 10, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (87, 10, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (88, 10, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (89, 10, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (90, 11, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (91, 11, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (92, 11, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (93, 11, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (94, 11, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (95, 11, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (96, 11, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (97, 12, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (98, 12, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (99, 12, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (100, 12, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (101, 12, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (102, 12, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (103, 12, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (104, 13, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (105, 13, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (106, 13, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (107, 13, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (108, 13, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (109, 13, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (110, 13, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (111, 14, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (112, 14, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (113, 14, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (114, 14, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (115, 14, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (116, 14, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (117, 14, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (118, 15, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (119, 15, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (120, 15, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (121, 15, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (122, 15, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (123, 15, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (124, 15, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (125, 16, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (126, 16, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (127, 16, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (128, 16, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (129, 16, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (130, 16, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (131, 16, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (132, 17, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (133, 17, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (134, 17, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (135, 17, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (136, 17, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (137, 17, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (138, 17, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (139, 18, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (140, 18, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (141, 18, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (142, 18, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (143, 18, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (144, 18, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (145, 18, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (146, 19, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (147, 19, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (148, 19, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (149, 19, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (150, 19, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (151, 19, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (152, 19, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (153, 20, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (154, 20, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (155, 20, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (156, 20, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (157, 20, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (158, 20, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (159, 20, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (160, 21, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (161, 21, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (162, 21, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (163, 21, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (164, 21, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (165, 21, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (166, 21, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (167, 22, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (168, 22, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (169, 22, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (170, 22, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (171, 22, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (172, 22, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (173, 22, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (174, 23, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (175, 23, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (176, 23, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (177, 23, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (178, 23, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (179, 23, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (180, 23, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (181, 24, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (182, 24, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (183, 24, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (184, 24, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (185, 24, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (186, 24, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (187, 24, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (188, 25, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (189, 25, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (190, 25, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (191, 25, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (192, 25, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (193, 25, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (194, 25, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (195, 26, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (196, 26, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (197, 26, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (198, 26, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (199, 26, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (200, 26, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (201, 26, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (202, 27, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (203, 27, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (204, 27, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (205, 27, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (206, 27, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (207, 27, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (208, 27, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (209, 28, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (210, 28, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (211, 28, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (212, 28, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (213, 28, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (214, 28, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (215, 28, 100000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (216, 29, 10, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (217, 29, 50, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (218, 29, 100, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (219, 29, 500, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (220, 29, 1000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (221, 29, 10000, 0);
INSERT INTO "AchievementInstance" ("AchievementBonusId", "AchievementTypeId", "Amount", "BonusPackId") VALUES (222, 29, 100000, 0);


--
-- TOC entry 2819 (class 0 OID 0)
-- Dependencies: 224
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE SET; Schema: Achievements; Owner: postgres
--

SELECT pg_catalog.setval('"AchievementInstance_AchievementBonusId_seq"', 222, true);


SET search_path = "GameMode", pg_catalog;

--
-- TOC entry 2802 (class 0 OID 215315)
-- Dependencies: 246
-- Data for Name: AbstractGameModeEntity; Type: TABLE DATA; Schema: GameMode; Owner: postgres
--

INSERT INTO "AbstractGameModeEntity" ("GameModeId", "Level_Min", "Level_Max") VALUES (1, 0, 10);
INSERT INTO "AbstractGameModeEntity" ("GameModeId", "Level_Min", "Level_Max") VALUES (2, 0, 10);
INSERT INTO "AbstractGameModeEntity" ("GameModeId", "Level_Min", "Level_Max") VALUES (3, 0, 10);


--
-- TOC entry 2803 (class 0 OID 215322)
-- Dependencies: 247
-- Data for Name: GameMapEntity; Type: TABLE DATA; Schema: GameMode; Owner: postgres
--

INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (0, 1, 0, 2, 128, 600);
INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (1, 2, 0, 2, 128, 600);
INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (2, 3, 0, 2, 128, 600);


--
-- TOC entry 2804 (class 0 OID 215343)
-- Dependencies: 248
-- Data for Name: TeamGameModeEntity; Type: TABLE DATA; Schema: GameMode; Owner: postgres
--

INSERT INTO "TeamGameModeEntity" ("GameModeId") VALUES (1);
INSERT INTO "TeamGameModeEntity" ("GameModeId") VALUES (2);
INSERT INTO "TeamGameModeEntity" ("GameModeId") VALUES (3);


SET search_path = "League", pg_catalog;

--
-- TOC entry 2750 (class 0 OID 16647)
-- Dependencies: 194
-- Data for Name: LeagueEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--

INSERT INTO "LeagueEntity" ("LeagueId", "Cash_Money", "Cash_Donate", "Info_Name", "Info_Abbr", "Info_FoundedDate", "Info_AccessType", "Info_JoinPrice") VALUES ('ff0ec814-a947-4dfb-9fb5-cc3d266d774b', 10, 100, 'VRS PRO', 'VRS', '2016-02-10 23:33:16.507', 1, 1000);


--
-- TOC entry 2751 (class 0 OID 16650)
-- Dependencies: 195
-- Data for Name: LeagueMemberEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--



SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2763 (class 0 OID 49808)
-- Dependencies: 207
-- Data for Name: MatchEntity; Type: TABLE DATA; Schema: Matches; Owner: postgres
--

INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "WinnerTeamId", "NumberOfBots", "RoundTimeInSeconds") VALUES ('64910acd-ea6e-49ee-a694-9780d10c6133', NULL, 1, 0, '2016-07-18 03:20:57.671317', '0001-01-01 00:00:00', '0001-01-01 00:00:00', NULL, 1, 600);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "WinnerTeamId", "NumberOfBots", "RoundTimeInSeconds") VALUES ('1f4bf71b-a574-4ceb-828e-f9982ce8c8fd', NULL, 1, 0, '2016-07-18 06:12:17.137748', '0001-01-01 00:00:00', '0001-01-01 00:00:00', NULL, 1, 600);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "WinnerTeamId", "NumberOfBots", "RoundTimeInSeconds") VALUES ('ea28c8eb-7a70-45e5-abe0-c08ad5f5efba', NULL, 1, 0, '2016-07-18 06:49:23.973371', '0001-01-01 00:00:00', '0001-01-01 00:00:00', NULL, 1, 600);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "WinnerTeamId", "NumberOfBots", "RoundTimeInSeconds") VALUES ('5640862a-d8f6-4c1a-960b-2cb15c24bc3e', NULL, 1, 0, '2016-07-18 07:25:26.871987', '0001-01-01 00:00:00', '0001-01-01 00:00:00', NULL, 1, 600);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "WinnerTeamId", "NumberOfBots", "RoundTimeInSeconds") VALUES ('190628f6-e4e9-42a7-96ce-17c0ae35a46b', NULL, 1, 0, '2016-07-18 07:29:53.099646', '0001-01-01 00:00:00', '0001-01-01 00:00:00', NULL, 1, 600);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "WinnerTeamId", "NumberOfBots", "RoundTimeInSeconds") VALUES ('8d6d05b2-02fa-49f9-ae72-b271dbe3db96', NULL, 1, 0, '2016-07-18 08:06:05.405045', '0001-01-01 00:00:00', '0001-01-01 00:00:00', NULL, 1, 600);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "WinnerTeamId", "NumberOfBots", "RoundTimeInSeconds") VALUES ('81bfbe66-deb9-499b-b98e-512f11705a36', NULL, 1, 2, '2016-07-18 08:10:55.875799', '2016-07-18 08:11:00.690966', '0001-01-01 00:00:00', NULL, 1, 600);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "WinnerTeamId", "NumberOfBots", "RoundTimeInSeconds") VALUES ('13e91d82-2466-4dfa-8370-9cbae5c67588', 'a12e2f8b-dcf5-48bc-adc5-8e0bf56e1186', 1, 2, '2016-07-18 08:23:27.768387', '2016-07-18 08:37:04.436772', '0001-01-01 00:00:00', NULL, 1, 600);


--
-- TOC entry 2765 (class 0 OID 49819)
-- Dependencies: 209
-- Data for Name: MatchMember; Type: TABLE DATA; Schema: Matches; Owner: postgres
--

INSERT INTO "MatchMember" ("MemberId", "PlayerId", "TeamId", "Level", "MatchId", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Squad", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5") VALUES ('2b286bb8-eddb-4369-b013-5a5b645b5f06', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 'd22dea3e-a714-453e-bdfe-fb6d4a599c21', 0, '64910acd-ea6e-49ee-a694-9780d10c6133', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "PlayerId", "TeamId", "Level", "MatchId", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Squad", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5") VALUES ('4d2127b7-6337-40fd-8035-0c11190450c3', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 'e405679c-d9cd-4739-9926-fbd150be61fa', 0, '1f4bf71b-a574-4ceb-828e-f9982ce8c8fd', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "PlayerId", "TeamId", "Level", "MatchId", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Squad", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5") VALUES ('2a9c482f-82cf-43f4-b7a7-d4128183b53e', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', '33720f67-3f23-46a9-a0d0-951751a2c39a', 0, 'ea28c8eb-7a70-45e5-abe0-c08ad5f5efba', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "PlayerId", "TeamId", "Level", "MatchId", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Squad", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5") VALUES ('cbf34432-7a9c-418e-afe7-249b21752470', '65499e3c-5d50-4443-baf2-2a009b635dc7', '41d14fd7-7093-42b0-994d-e16fe5bc1ffd', 0, '5640862a-d8f6-4c1a-960b-2cb15c24bc3e', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "PlayerId", "TeamId", "Level", "MatchId", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Squad", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5") VALUES ('0571bdae-5996-4deb-a859-8432a95cd190', '65499e3c-5d50-4443-baf2-2a009b635dc7', '16cffdb5-632e-4303-b885-56e2e133b311', 0, '190628f6-e4e9-42a7-96ce-17c0ae35a46b', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "PlayerId", "TeamId", "Level", "MatchId", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Squad", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5") VALUES ('1c0bc42d-6478-4a19-816b-b3db4dab4e3e', '65499e3c-5d50-4443-baf2-2a009b635dc7', 'cba5c98f-02ae-4d57-ac14-c4f518d2f56f', 0, '8d6d05b2-02fa-49f9-ae72-b271dbe3db96', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "PlayerId", "TeamId", "Level", "MatchId", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Squad", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5") VALUES ('b38fddd8-179e-4575-acb7-c9462f07b404', '65499e3c-5d50-4443-baf2-2a009b635dc7', 'f967bc65-da2d-4d66-b89c-14abd5d44743', 0, '81bfbe66-deb9-499b-b98e-512f11705a36', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "PlayerId", "TeamId", "Level", "MatchId", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Squad", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5") VALUES ('451186a4-a6cc-4529-95a3-af95d9d25d15', '65499e3c-5d50-4443-baf2-2a009b635dc7', 'f2bf83e6-9e22-4741-9789-b8232badb30f', 0, '13e91d82-2466-4dfa-8370-9cbae5c67588', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);


--
-- TOC entry 2764 (class 0 OID 49813)
-- Dependencies: 208
-- Data for Name: MatchTeam; Type: TABLE DATA; Schema: Matches; Owner: postgres
--

INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('4cad0463-12cc-4ebe-8c1c-fd22d387e1e8', '64910acd-ea6e-49ee-a694-9780d10c6133');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('d22dea3e-a714-453e-bdfe-fb6d4a599c21', '64910acd-ea6e-49ee-a694-9780d10c6133');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('07e2fec2-f177-40cd-9c67-9132d8f12b8e', '1f4bf71b-a574-4ceb-828e-f9982ce8c8fd');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('e405679c-d9cd-4739-9926-fbd150be61fa', '1f4bf71b-a574-4ceb-828e-f9982ce8c8fd');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('2a27bd2f-592f-4c14-a8f2-36b12ddc9afa', 'ea28c8eb-7a70-45e5-abe0-c08ad5f5efba');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('33720f67-3f23-46a9-a0d0-951751a2c39a', 'ea28c8eb-7a70-45e5-abe0-c08ad5f5efba');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('41d14fd7-7093-42b0-994d-e16fe5bc1ffd', '5640862a-d8f6-4c1a-960b-2cb15c24bc3e');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('4e3a21c2-df2b-403e-9882-e9719057886a', '5640862a-d8f6-4c1a-960b-2cb15c24bc3e');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('16cffdb5-632e-4303-b885-56e2e133b311', '190628f6-e4e9-42a7-96ce-17c0ae35a46b');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('688737e9-e5b1-4320-b0a4-e89d2ac6d4fb', '190628f6-e4e9-42a7-96ce-17c0ae35a46b');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('cba5c98f-02ae-4d57-ac14-c4f518d2f56f', '8d6d05b2-02fa-49f9-ae72-b271dbe3db96');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('d47c4d33-5e08-4e07-af27-e32fc304e4c6', '8d6d05b2-02fa-49f9-ae72-b271dbe3db96');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('e575a251-d430-469a-b24d-aee7af386267', '81bfbe66-deb9-499b-b98e-512f11705a36');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('f967bc65-da2d-4d66-b89c-14abd5d44743', '81bfbe66-deb9-499b-b98e-512f11705a36');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('75cbdf01-5a67-4316-8619-48218772101a', '13e91d82-2466-4dfa-8370-9cbae5c67588');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('f2bf83e6-9e22-4741-9789-b8232badb30f', '13e91d82-2466-4dfa-8370-9cbae5c67588');


--
-- TOC entry 2766 (class 0 OID 49825)
-- Dependencies: 210
-- Data for Name: MemberAchievements; Type: TABLE DATA; Schema: Matches; Owner: postgres
--



SET search_path = "Players", pg_catalog;

--
-- TOC entry 2753 (class 0 OID 16658)
-- Dependencies: 197
-- Data for Name: FriendEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2789 (class 0 OID 115889)
-- Dependencies: 233
-- Data for Name: InstanceOfInstalledAddon; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2788 (class 0 OID 107659)
-- Dependencies: 232
-- Data for Name: InstanceOfItemAddon; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2787 (class 0 OID 107654)
-- Dependencies: 231
-- Data for Name: InstanceOfItemSkin; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('41dd165b-9378-4e73-a83a-b8923fe40005');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('eb16e9d6-9965-4c91-9d31-4af0a61e0e02');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('04b9ccc2-2123-4055-805f-c4892d24a430');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('37674aaf-5945-4f53-92ec-c4c46a6e60f1');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('44d30fe7-6aaf-470f-a7a9-b1475fa6a4d0');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('6c202fb6-5f2a-4272-acc4-4d1fa3a247d0');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('1194f45a-62f6-4a95-9827-1bf583125cb6');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('3f27c38c-5b7f-4319-aeef-9dd2a61d244e');


--
-- TOC entry 2776 (class 0 OID 91015)
-- Dependencies: 220
-- Data for Name: InstanceOfPlayerArmour; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2752 (class 0 OID 16654)
-- Dependencies: 196
-- Data for Name: InstanceOfPlayerCharacter; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerCharacter" ("ItemId") VALUES ('9b06d8ec-69c2-46a9-8a1f-5322582e1164');
INSERT INTO "InstanceOfPlayerCharacter" ("ItemId") VALUES ('400074cf-d7ae-4c74-8925-1eefcf87844c');
INSERT INTO "InstanceOfPlayerCharacter" ("ItemId") VALUES ('d5974ec5-23ef-4d1b-ba99-4b80338cf432');
INSERT INTO "InstanceOfPlayerCharacter" ("ItemId") VALUES ('14b04bed-36e7-4515-8513-e8da4cc212ca');


--
-- TOC entry 2775 (class 0 OID 90987)
-- Dependencies: 219
-- Data for Name: InstanceOfPlayerItem; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('9b06d8ec-69c2-46a9-8a1f-5322582e1164', '65499e3c-5d50-4443-baf2-2a009b635dc7', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('7891a1de-f5f7-43fd-b765-8c97c072df33', '65499e3c-5d50-4443-baf2-2a009b635dc7', 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('cd7fe53b-1fe5-4f79-8e29-449c04ff30a8', '65499e3c-5d50-4443-baf2-2a009b635dc7', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('400074cf-d7ae-4c74-8925-1eefcf87844c', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('852c1f6b-3509-433f-b3a9-c442980b30af', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('92771862-3ce9-449c-9e8b-3b46e9d2a7a8', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('d5974ec5-23ef-4d1b-ba99-4b80338cf432', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('71be95d0-9cb7-48bd-ae1b-48faccb629ff', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('7aa5663f-7343-4ecc-892d-650fa1cf0ecc', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('14b04bed-36e7-4515-8513-e8da4cc212ca', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('396785a6-a3cd-4bac-8302-ef906719adc9', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "PlayerId", "ModelId", modification_1_type, modification_2_type, modification_3_type, modification_4_type, modification_5_type, modification_1_value, modification_2_value, modification_3_value, modification_4_value, modification_5_value, "CategoryTypeId") VALUES ('673630a8-5612-4ce4-9bf7-99e19a911c40', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);


--
-- TOC entry 2794 (class 0 OID 214748)
-- Dependencies: 238
-- Data for Name: InstanceOfPlayerKit; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2777 (class 0 OID 91032)
-- Dependencies: 221
-- Data for Name: InstanceOfPlayerWeapon; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('7891a1de-f5f7-43fd-b765-8c97c072df33');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('cd7fe53b-1fe5-4f79-8e29-449c04ff30a8');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('852c1f6b-3509-433f-b3a9-c442980b30af');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('92771862-3ce9-449c-9e8b-3b46e9d2a7a8');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('71be95d0-9cb7-48bd-ae1b-48faccb629ff');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('7aa5663f-7343-4ecc-892d-650fa1cf0ecc');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('396785a6-a3cd-4bac-8302-ef906719adc9');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('673630a8-5612-4ce4-9bf7-99e19a911c40');


--
-- TOC entry 2783 (class 0 OID 91170)
-- Dependencies: 227
-- Data for Name: InstanceOfTargetItem; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('41dd165b-9378-4e73-a83a-b8923fe40005', '7891a1de-f5f7-43fd-b765-8c97c072df33', '65499e3c-5d50-4443-baf2-2a009b635dc7', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('eb16e9d6-9965-4c91-9d31-4af0a61e0e02', 'cd7fe53b-1fe5-4f79-8e29-449c04ff30a8', '65499e3c-5d50-4443-baf2-2a009b635dc7', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('04b9ccc2-2123-4055-805f-c4892d24a430', '92771862-3ce9-449c-9e8b-3b46e9d2a7a8', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('37674aaf-5945-4f53-92ec-c4c46a6e60f1', '852c1f6b-3509-433f-b3a9-c442980b30af', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('44d30fe7-6aaf-470f-a7a9-b1475fa6a4d0', '71be95d0-9cb7-48bd-ae1b-48faccb629ff', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('6c202fb6-5f2a-4272-acc4-4d1fa3a247d0', '7aa5663f-7343-4ecc-892d-650fa1cf0ecc', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('1194f45a-62f6-4a95-9827-1bf583125cb6', '673630a8-5612-4ce4-9bf7-99e19a911c40', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 0, 7);
INSERT INTO "InstanceOfTargetItem" ("ItemId", "TargetPlayerItemId", "PlayerId", "ModelId", "CategoryTypeId") VALUES ('3f27c38c-5b7f-4319-aeef-9dd2a61d244e', '396785a6-a3cd-4bac-8302-ef906719adc9', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 0, 7);


--
-- TOC entry 2782 (class 0 OID 91126)
-- Dependencies: 226
-- Data for Name: PlayerAchievement; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2754 (class 0 OID 16662)
-- Dependencies: 198
-- Data for Name: PlayerEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivityDate", "CurrentFractionId", "Experience_LastLevel", "WeekBonus_JoinDate", "WeekBonus_JoinCount", "WeekBonus_NotifyDate", "FirstPartnerId", "MatchMemberId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5", "QueueMemberId") VALUES ('0cf267ab-2934-42a2-8a19-cb9dff9757bf', 'gfg44344', 0, 0, 0, 1500, 0, '2016-07-17 11:02:31.629457', '2016-07-20 11:02:31.629457', '2016-07-17 11:02:31.629457', '161aa5fa-a233-49b5-aae6-6d5fb2c5b0a3', 0, '2016-07-18 06:49:40.625013', 0, '2016-07-18 03:20:40.253999', NULL, '2a9c482f-82cf-43f4-b7a7-d4128183b53e', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0970514c-89ab-4390-8766-98f869133671');
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivityDate", "CurrentFractionId", "Experience_LastLevel", "WeekBonus_JoinDate", "WeekBonus_JoinCount", "WeekBonus_NotifyDate", "FirstPartnerId", "MatchMemberId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5", "QueueMemberId") VALUES ('e9f3992c-f293-47f0-ad05-3b080fe305c2', 'рпа4', 0, 0, 0, 1500, 0, '2016-07-17 06:40:16.672555', '2016-07-20 06:40:16.672555', '2016-07-17 06:40:16.672555', '10b75673-b20e-4142-aae6-2b2119f6f4a0', 0, '2016-07-17 06:40:16.837557', 1, '2016-07-17 06:40:16.908555', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivityDate", "CurrentFractionId", "Experience_LastLevel", "WeekBonus_JoinDate", "WeekBonus_JoinCount", "WeekBonus_NotifyDate", "FirstPartnerId", "MatchMemberId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5", "QueueMemberId") VALUES ('4ca36b0c-3ad8-4d37-9a48-4429d6631609', 'gfdg4f3', 0, 0, 0, 1500, 0, '2016-07-17 10:00:23.779762', '2016-07-20 10:00:23.779762', '2016-07-17 10:00:23.779762', '48ed4e29-e35e-4356-a2aa-529eda2b6a06', 0, '2016-07-17 10:23:42.489199', 1, '2016-07-17 10:00:24.018771', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '122c10c4-bbc9-4475-b492-83903032e816');
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivityDate", "CurrentFractionId", "Experience_LastLevel", "WeekBonus_JoinDate", "WeekBonus_JoinCount", "WeekBonus_NotifyDate", "FirstPartnerId", "MatchMemberId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Statistic_Reserved_1", "Statistic_Reserved_2", "Statistic_Reserved_3", "Statistic_Reserved_4", "Statistic_Reserved_5", "QueueMemberId") VALUES ('65499e3c-5d50-4443-baf2-2a009b635dc7', 'SeNTike', 0, 0, 0, 1500, 0, '2016-07-17 03:18:01.304342', '2016-07-20 03:18:01.304342', '2016-07-17 03:18:01.304342', '4563d5de-b41f-4d00-8501-c3ac7763328e', 0, '2016-07-18 08:22:47.21505', 0, '2016-07-18 07:25:12.655381', NULL, '451186a4-a6cc-4529-95a3-af95d9d25d15', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0b6a373d-1dc4-4042-8e2e-d6624fc9a886');


--
-- TOC entry 2749 (class 0 OID 16632)
-- Dependencies: 193
-- Data for Name: PlayerProfileEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "PlayerProfileEntity" ("AssetId", "CharacterId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId", "IsEnabled", "Name", "IsUnlocked") VALUES ('2cb27fa2-47fc-4309-9185-1309f622ded6', '9b06d8ec-69c2-46a9-8a1f-5322582e1164', 0, 0, 0, 0, '65499e3c-5d50-4443-baf2-2a009b635dc7', false, NULL, false);
INSERT INTO "PlayerProfileEntity" ("AssetId", "CharacterId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId", "IsEnabled", "Name", "IsUnlocked") VALUES ('03b3cc97-97c4-4f07-a0d9-990248fee38a', '400074cf-d7ae-4c74-8925-1eefcf87844c', 0, 0, 0, 0, 'e9f3992c-f293-47f0-ad05-3b080fe305c2', false, NULL, false);
INSERT INTO "PlayerProfileEntity" ("AssetId", "CharacterId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId", "IsEnabled", "Name", "IsUnlocked") VALUES ('62ac6c18-afe1-4ca8-8eec-bb07a7fc5a3f', 'd5974ec5-23ef-4d1b-ba99-4b80338cf432', 0, 0, 0, 0, '4ca36b0c-3ad8-4d37-9a48-4429d6631609', false, NULL, false);
INSERT INTO "PlayerProfileEntity" ("AssetId", "CharacterId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId", "IsEnabled", "Name", "IsUnlocked") VALUES ('b796dc59-3f52-4fac-8ee8-824365282ea2', '14b04bed-36e7-4515-8513-e8da4cc212ca', 0, 0, 0, 0, '0cf267ab-2934-42a2-8a19-cb9dff9757bf', false, NULL, false);


--
-- TOC entry 2793 (class 0 OID 214715)
-- Dependencies: 237
-- Data for Name: PlayerPromoCode; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2790 (class 0 OID 149147)
-- Dependencies: 234
-- Data for Name: PlayerReputationEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('c334f0fd-9d15-4704-8196-f98b7c73dd88', '65499e3c-5d50-4443-baf2-2a009b635dc7', 1, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('e8704840-6df4-413f-8234-65ddd64d1bb1', '65499e3c-5d50-4443-baf2-2a009b635dc7', 3, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('f0857e93-ec6d-4892-9f55-898aed272c8f', '65499e3c-5d50-4443-baf2-2a009b635dc7', 0, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('4563d5de-b41f-4d00-8501-c3ac7763328e', '65499e3c-5d50-4443-baf2-2a009b635dc7', 2, 1, 1, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('971e7ddb-57d5-426c-8348-5e2edb174fec', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', 3, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('9b5b9473-f0d5-4402-ac68-99649df24e99', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', 1, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('c84e7c5a-b119-4d9b-9af8-0e7780679ade', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', 0, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('10b75673-b20e-4142-aae6-2b2119f6f4a0', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', 2, 1, 1, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('35ca08c7-6aa5-4085-85ce-924b446a846c', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', 3, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('6cc30d1c-d544-4cd9-a9c2-412ddccbc26c', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', 0, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('bf783b3a-7a7b-41c0-83a2-f070adaf8e93', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', 1, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('48ed4e29-e35e-4356-a2aa-529eda2b6a06', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', 2, 1, 1, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('0254c512-0b8c-42c9-835c-3010471de76c', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 0, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('b140380d-b49b-4a47-a0e8-4f00b698e44e', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 3, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('d9156314-5e34-4d5f-87fd-17f939bfbb77', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 1, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('161aa5fa-a233-49b5-aae6-6d5fb2c5b0a3', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', 2, 1, 1, 0);


--
-- TOC entry 2795 (class 0 OID 214753)
-- Dependencies: 239
-- Data for Name: ProfileItemEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "ProfileItemEntity" ("ProfileItemId", "ProfileId", "ItemId", "SlotId") VALUES ('9f3072fc-182e-49b5-ae1b-130391fe4067', '2cb27fa2-47fc-4309-9185-1309f622ded6', 'cd7fe53b-1fe5-4f79-8e29-449c04ff30a8', 9);
INSERT INTO "ProfileItemEntity" ("ProfileItemId", "ProfileId", "ItemId", "SlotId") VALUES ('517a4bfb-7255-4252-8596-7ba34144d93f', '03b3cc97-97c4-4f07-a0d9-990248fee38a', '92771862-3ce9-449c-9e8b-3b46e9d2a7a8', 9);
INSERT INTO "ProfileItemEntity" ("ProfileItemId", "ProfileId", "ItemId", "SlotId") VALUES ('543f5e2e-afee-41ec-9d84-a72d91d01b18', '62ac6c18-afe1-4ca8-8eec-bb07a7fc5a3f', '7aa5663f-7343-4ecc-892d-650fa1cf0ecc', 9);
INSERT INTO "ProfileItemEntity" ("ProfileItemId", "ProfileId", "ItemId", "SlotId") VALUES ('34759be3-1e1c-40c0-ba0b-efc87974c40d', 'b796dc59-3f52-4fac-8ee8-824365282ea2', '396785a6-a3cd-4bac-8302-ef906719adc9', 9);


SET search_path = "Promo", pg_catalog;

--
-- TOC entry 2792 (class 0 OID 214698)
-- Dependencies: 236
-- Data for Name: Codes; Type: TABLE DATA; Schema: Promo; Owner: postgres
--

INSERT INTO "Codes" ("PromoId", "PartnerId", "Name", "Validity", "Confirmed") VALUES ('e088e337-acf9-445c-a0fe-95cead7a39bf', '373a6856-d9d5-4744-939d-ef7b395b43b3', 'Hello World', '2016-07-02 06:07:22.588697', true);
INSERT INTO "Codes" ("PromoId", "PartnerId", "Name", "Validity", "Confirmed") VALUES ('fa20f6a6-290b-4a27-9590-57497341b194', '56cfb1e7-a393-4131-893f-1f30aeaf2504', 'sentike', '2017-07-04 05:38:43.852077', false);
INSERT INTO "Codes" ("PromoId", "PartnerId", "Name", "Validity", "Confirmed") VALUES ('58127dfb-271c-43b2-bd64-e6a125481598', 'e7013c7d-040d-4e96-ac0d-820fc80bb5b9', 'sentike1', '2017-07-04 05:39:20.184639', false);


--
-- TOC entry 2791 (class 0 OID 214693)
-- Dependencies: 235
-- Data for Name: Partners; Type: TABLE DATA; Schema: Promo; Owner: postgres
--

INSERT INTO "Partners" ("PartnerId", "Name", "Login", "Password", "RegistrationDate") VALUES ('c5dfdb4d-b0b0-49de-853c-0c101e49b0ec', 'By SeNTike', 'sentike', 'sentike', '2016-07-02 06:02:13.819039');
INSERT INTO "Partners" ("PartnerId", "Name", "Login", "Password", "RegistrationDate") VALUES ('373a6856-d9d5-4744-939d-ef7b395b43b3', 'qazwsz', 'qazwsz', 'qazwsz', '2016-07-02 06:05:07.71076');
INSERT INTO "Partners" ("PartnerId", "Name", "Login", "Password", "RegistrationDate") VALUES ('56cfb1e7-a393-4131-893f-1f30aeaf2504', 'sentike', 'sentike@gmail.com', 'https://vk.com/sentike', '2016-07-04 05:38:43.851577');
INSERT INTO "Partners" ("PartnerId", "Name", "Login", "Password", "RegistrationDate") VALUES ('e7013c7d-040d-4e96-ac0d-820fc80bb5b9', 'sentike1', 'sen1tike@gmail.com', 'https://vk.com/sentike1', '2016-07-04 05:39:20.184639');


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2770 (class 0 OID 90937)
-- Dependencies: 214
-- Data for Name: AbstractItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (9, 1, 1000, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (10, 1, 1000, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (11, 1, 1000, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 0, 5000, false, 1, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 0, 3500, false, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 0, 4500, false, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 0, 7000, false, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 0, 175000, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 0, 23543, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 0, 14477, true, 1, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 1, 1000, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (18, 3, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (20, 4, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (23, 6, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (22, 7, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (19, 3, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (21, 9, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 2, 50700, false, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 1, 58700, false, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 3, 50000, false, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 4, 15000, false, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 5000, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (24, 0, 1000, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (26, 1, 2600, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (27, 2, 8600, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (25, 2, 11200, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (12, 0, 6800, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (15, 0, 2600, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (13, 1, 9800, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (14, 1, 4200, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (16, 2, 18600, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (17, 2, 22100, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (28, 1, 5400, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 1, 2600, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 1, 4200, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 1, 3200, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 1, 1100, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 2, 1800, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 0, false, 0, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 1, 100, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 2, 500, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 1, 400, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 2, 850, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 1, 250, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 0, 100, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 1, 400, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 2, 450, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (9, 3, 600, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (10, 3, 800, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (11, 3, 900, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (12, 2, 200, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (13, 2, 0, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (14, 2, 400, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (15, 3, 600, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (16, 3, 800, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (17, 0, 0, false, 0, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (18, 0, 0, false, 0, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (19, 0, 0, false, 0, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 0, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 1, 500, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 1, 1500, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 1, 1600, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 1, 2000, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 1, 100000, true, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (29, 3, 11200, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 1, 10000, true, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 1, 100000, true, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 0, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 10, 50000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 20, 80000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 20, 80000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 40, 150000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 30, 110000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 38, 140000, false, 0, 5, true, 0);


--
-- TOC entry 2786 (class 0 OID 107613)
-- Dependencies: 230
-- Data for Name: AddonItemContainer; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2820 (class 0 OID 0)
-- Dependencies: 229
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE SET; Schema: Store; Owner: postgres
--

SELECT pg_catalog.setval('"AddonItemContainer_ContainerId_seq"', 1, false);


--
-- TOC entry 2784 (class 0 OID 107606)
-- Dependencies: 228
-- Data for Name: AddonItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 4);


--
-- TOC entry 2778 (class 0 OID 91071)
-- Dependencies: 222
-- Data for Name: AmmoItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2771 (class 0 OID 90955)
-- Dependencies: 215
-- Data for Name: ArmourItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 1);


--
-- TOC entry 2774 (class 0 OID 90980)
-- Dependencies: 218
-- Data for Name: CharacterItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 5);


--
-- TOC entry 2755 (class 0 OID 16682)
-- Dependencies: 199
-- Data for Name: FractionEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (1, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (2, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (3, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (4, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (0, 0, 0, 0, 0, 0);


--
-- TOC entry 2796 (class 0 OID 214787)
-- Dependencies: 240
-- Data for Name: KitItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2773 (class 0 OID 90974)
-- Dependencies: 217
-- Data for Name: SkinItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 7);


--
-- TOC entry 2772 (class 0 OID 90967)
-- Dependencies: 216
-- Data for Name: WeaponItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (20, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (21, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (22, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (23, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (24, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (25, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (26, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (27, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (28, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (29, 0);


SET search_path = public, pg_catalog;

--
-- TOC entry 2800 (class 0 OID 214952)
-- Dependencies: 244
-- Data for Name: ClusterInstanceEntity; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "RegionTypeId", "UpdateStatus") VALUES ('143882e2-c1fe-4cb9-8a10-9ac3af0bed33', 'DESKTOP-LLB1F78', '127.0.0.1', '2016-07-18 08:22:36.257786', '2016-07-18 08:22:36.258286', 0, 30, 0, 2, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "RegionTypeId", "UpdateStatus") VALUES ('cae9bcbd-6478-4f75-88d8-6c6722293b27', 'DESKTOP-LLB1F78', '127.0.0.1', '2016-07-18 22:42:39.125453', '2016-07-18 22:42:39.125453', 0, 30, 1, 2, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "RegionTypeId", "UpdateStatus") VALUES ('71e5ed97-f45a-42fa-b540-9a56af19ab10', 'DESKTOP-LLB1F78', '127.0.0.1', '2016-07-18 22:57:39.81083', '2016-07-18 22:57:39.81083', 0, 30, 1, 2, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "RegionTypeId", "UpdateStatus") VALUES ('6a317fb4-5896-4427-b9ef-4f2e69d4b781', 'DESKTOP-LLB1F78', '127.0.0.1', '2016-07-18 22:58:46.515399', '2016-07-18 22:58:46.515399', 0, 30, 1, 2, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "RegionTypeId", "UpdateStatus") VALUES ('360f7666-f5a6-4f84-b07a-3cc06dfa3417', 'DESKTOP-LLB1F78', '127.0.0.1', '2016-07-19 01:08:03.933156', '2016-07-19 01:08:03.933156', 0, 30, 1, 2, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "RegionTypeId", "UpdateStatus") VALUES ('fa6521fc-d075-428a-966e-77e4d4e97d19', 'DESKTOP-LLB1F78', '127.0.0.1', '2016-07-19 01:11:13.991188', '2016-07-19 01:11:13.991188', 0, 30, 1, 2, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "RegionTypeId", "UpdateStatus") VALUES ('f9284f5d-c50a-493f-a6bf-61d810e4599d', 'DESKTOP-LLB1F78', '127.0.0.1', '2016-07-19 01:13:10.210391', '2016-07-19 01:13:10.210391', 0, 30, 1, 2, 0);


--
-- TOC entry 2801 (class 0 OID 214960)
-- Dependencies: 245
-- Data for Name: ClusterNodeEntity; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "State", "Address_Host", "Address_Port", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "ShutDownDate") VALUES ('a12e2f8b-dcf5-48bc-adc5-8e0bf56e1186', '143882e2-c1fe-4cb9-8a10-9ac3af0bed33', 7, '127.0.0.1', 7777, '2016-07-18 08:23:05.215171', '2016-07-18 08:37:43.776113', '13e91d82-2466-4dfa-8370-9cbae5c67588', '2016-07-18 22:50:31.496993');
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "State", "Address_Host", "Address_Port", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "ShutDownDate") VALUES ('cd45f5d6-4488-4880-8424-c11e926e0d27', '71e5ed97-f45a-42fa-b540-9a56af19ab10', 3, '127.0.0.1', 7777, '2016-07-18 22:58:06.515827', '2016-07-18 22:58:19.069364', NULL, '2016-07-18 22:58:20.715774');
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "State", "Address_Host", "Address_Port", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "ShutDownDate") VALUES ('e88aa4a6-cbc9-4d6c-8544-ef89912d4321', '360f7666-f5a6-4f84-b07a-3cc06dfa3417', 3, '127.0.0.1', 7777, '2016-07-19 01:08:30.573646', '2016-07-19 01:08:40.637897', NULL, '2016-07-19 01:09:36.042596');
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "State", "Address_Host", "Address_Port", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "ShutDownDate") VALUES ('71f30cfa-597d-4ce7-b650-e02ca28a04f2', 'fa6521fc-d075-428a-966e-77e4d4e97d19', 3, '127.0.0.1', 7777, '2016-07-19 01:11:40.993901', '2016-07-19 01:11:46.062632', NULL, '2016-07-19 01:12:36.742334');
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "State", "Address_Host", "Address_Port", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "ShutDownDate") VALUES ('87886fa7-7ca7-4c59-b08a-25c18d179199', 'f9284f5d-c50a-493f-a6bf-61d810e4599d', 3, '127.0.0.1', 7777, '2016-07-19 01:13:36.763265', '2016-07-19 01:13:39.31635', NULL, '2016-07-19 01:13:58.388349');


--
-- TOC entry 2797 (class 0 OID 214873)
-- Dependencies: 241
-- Data for Name: GameSessionEntity; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "GameSessionEntity" ("SessionId", "PlayerId", "LastActivityDate", "LastMessageAccess") VALUES ('66923f51-f801-441a-99bf-be9eb91726ab', 'e9f3992c-f293-47f0-ad05-3b080fe305c2', '2016-07-17 10:00:30.743234', '2016-07-17 09:59:45.743234');
INSERT INTO "GameSessionEntity" ("SessionId", "PlayerId", "LastActivityDate", "LastMessageAccess") VALUES ('d3dcbf8c-d0fa-4bfd-a018-7ebf8fbd7603', '4ca36b0c-3ad8-4d37-9a48-4429d6631609', '2016-07-17 11:02:38.189828', '2016-07-17 11:02:11.433521');
INSERT INTO "GameSessionEntity" ("SessionId", "PlayerId", "LastActivityDate", "LastMessageAccess") VALUES ('2e5a8210-8791-467a-abd2-81c04cbc6eee', '65499e3c-5d50-4443-baf2-2a009b635dc7', '2016-07-18 08:37:10.855678', '2016-07-18 08:23:32.672048');
INSERT INTO "GameSessionEntity" ("SessionId", "PlayerId", "LastActivityDate", "LastMessageAccess") VALUES ('7ed31f3a-9127-4d41-b635-97069c0a365b', '0cf267ab-2934-42a2-8a19-cb9dff9757bf', '2016-07-18 06:50:53.832433', '2016-07-18 06:50:33.834436');


--
-- TOC entry 2767 (class 0 OID 66321)
-- Dependencies: 211
-- Data for Name: GlobalChatMessage; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2768 (class 0 OID 66326)
-- Dependencies: 212
-- Data for Name: PrivateChatMessage; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2769 (class 0 OID 66331)
-- Dependencies: 213
-- Data for Name: PrivateChatMessageData; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2798 (class 0 OID 214885)
-- Dependencies: 242
-- Data for Name: SessionQueueEntity; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "SessionQueueEntity" ("PlayerId", "GameModeTypeId", "RegionTypeId", "MatchId", "CheckDate", "JoinDate", "Level_Min", "Level_Max", "Ready", "QueueId") VALUES ('65499e3c-5d50-4443-baf2-2a009b635dc7', 1, 16, '13e91d82-2466-4dfa-8370-9cbae5c67588', '2016-07-18 08:23:10.256708', '2016-07-18 08:23:10.251702', 1, 1, true, 'a3cd1f72-b85a-4ab7-a908-f06a1d198a73');


--
-- TOC entry 2799 (class 0 OID 214892)
-- Dependencies: 243
-- Data for Name: SessionQueueMember; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "SessionQueueMember" ("QueueId", "PlayerId", "Ready", "QueueMemberId") VALUES ('a3cd1f72-b85a-4ab7-a908-f06a1d198a73', '65499e3c-5d50-4443-baf2-2a009b635dc7', true, '0b6a373d-1dc4-4042-8e2e-d6624fc9a886');


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2756 (class 0 OID 16734)
-- Dependencies: 200
-- Data for Name: Deposits; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2757 (class 0 OID 16740)
-- Dependencies: 201
-- Data for Name: Roles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2758 (class 0 OID 16743)
-- Dependencies: 202
-- Data for Name: UserClaims; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2821 (class 0 OID 0)
-- Dependencies: 203
-- Name: UserClaims_Id_seq; Type: SEQUENCE SET; Schema: vrs; Owner: postgres
--

SELECT pg_catalog.setval('"UserClaims_Id_seq"', 2, true);


--
-- TOC entry 2760 (class 0 OID 16751)
-- Dependencies: 204
-- Data for Name: UserLogins; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "UserLogins" ("UserId", "LoginProvider", "ProviderKey") VALUES ('65499e3c-5d50-4443-baf2-2a009b635dc7', 'Steam', 'http://steamcommunity.com/openid/id/76561198310348310');


--
-- TOC entry 2761 (class 0 OID 16755)
-- Dependencies: 205
-- Data for Name: UserRoles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2762 (class 0 OID 16759)
-- Dependencies: 206
-- Data for Name: Users; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "Balance") VALUES ('65499e3c-5d50-4443-baf2-2a009b635dc7', '76561198310348310', NULL, 'd55732e9-ad39-4171-ba5b-024edeb1bae2', NULL, false, NULL, false, false, NULL, false, 0, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "Balance") VALUES ('e9f3992c-f293-47f0-ad05-3b080fe305c2', '123451g0@vrs.com', 'ALcVJ03yrvLaAe3QsfKYpWO8+vF0i5lBsnSPJyuGsxxnY9RYwIzqeBJvIWLVKcvkOQ==', '84310dab-af10-4e86-a572-a1c09866e148', '123451g0@vrs.com', false, NULL, false, false, NULL, false, 0, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "Balance") VALUES ('4ca36b0c-3ad8-4d37-9a48-4429d6631609', '123451g00@vrs.com', 'AMebZRc2b3BAgj4wsZvNc4iuPgwfKjhm9Rjb+iAltVxqGt/zxY/LMA+kKZvPm4dmfg==', 'b498f6fd-ada7-4fdd-bcaf-698fecb5a6e7', '123451g00@vrs.com', false, NULL, false, false, NULL, false, 0, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "Balance") VALUES ('0cf267ab-2934-42a2-8a19-cb9dff9757bf', '123451g00g@vrs.com', 'APC0860w8lGgv7NmmQGIAgiwQaNxmQFZ7gKUg1VwkAH46rTHFvwy0ffH2mllXgRAuA==', 'eb5d0d6b-c454-4c63-a98a-5a828a8855d5', '123451g00g@vrs.com', false, NULL, false, false, NULL, false, 0, 0);


SET search_path = "Achievements", pg_catalog;

--
-- TOC entry 2482 (class 2606 OID 91115)
-- Name: AchievementContainer_pkey; Type: CONSTRAINT; Schema: Achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementContainer"
    ADD CONSTRAINT "AchievementContainer_pkey" PRIMARY KEY ("AchievementTypeId");


--
-- TOC entry 2484 (class 2606 OID 91125)
-- Name: AchievementInstance_pkey; Type: CONSTRAINT; Schema: Achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementInstance"
    ADD CONSTRAINT "AchievementInstance_pkey" PRIMARY KEY ("AchievementBonusId");


SET search_path = "GameMode", pg_catalog;

--
-- TOC entry 2560 (class 2606 OID 215321)
-- Name: AbstractGameModeEntity_pkey; Type: CONSTRAINT; Schema: GameMode; Owner: postgres
--

ALTER TABLE ONLY "AbstractGameModeEntity"
    ADD CONSTRAINT "AbstractGameModeEntity_pkey" PRIMARY KEY ("GameModeId");


--
-- TOC entry 2562 (class 2606 OID 215333)
-- Name: GameMapEntity_MapId_GameModeId_key; Type: CONSTRAINT; Schema: GameMode; Owner: postgres
--

ALTER TABLE ONLY "GameMapEntity"
    ADD CONSTRAINT "GameMapEntity_MapId_GameModeId_key" UNIQUE ("MapId", "GameModeId");


--
-- TOC entry 2564 (class 2606 OID 215331)
-- Name: GameMapEntity_pkey; Type: CONSTRAINT; Schema: GameMode; Owner: postgres
--

ALTER TABLE ONLY "GameMapEntity"
    ADD CONSTRAINT "GameMapEntity_pkey" PRIMARY KEY ("InstanceId");


--
-- TOC entry 2566 (class 2606 OID 215347)
-- Name: TeamGameModeEntity_pkey; Type: CONSTRAINT; Schema: GameMode; Owner: postgres
--

ALTER TABLE ONLY "TeamGameModeEntity"
    ADD CONSTRAINT "TeamGameModeEntity_pkey" PRIMARY KEY ("GameModeId");


SET search_path = "League", pg_catalog;

--
-- TOC entry 2396 (class 2606 OID 16785)
-- Name: LeagueEntity_Info_Abbr_key; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Abbr_key" UNIQUE ("Info_Abbr");


--
-- TOC entry 2398 (class 2606 OID 16787)
-- Name: LeagueEntity_Info_Name_key; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Name_key" UNIQUE ("Info_Name");


--
-- TOC entry 2400 (class 2606 OID 16789)
-- Name: LeagueEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_pkey" PRIMARY KEY ("LeagueId");


--
-- TOC entry 2402 (class 2606 OID 16791)
-- Name: LeagueMemberEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2441 (class 2606 OID 49892)
-- Name: MatchEntity_WinnerTeamId_key; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_key" UNIQUE ("WinnerTeamId");


--
-- TOC entry 2443 (class 2606 OID 49812)
-- Name: MatchEntity_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_pkey" PRIMARY KEY ("MatchId");


--
-- TOC entry 2448 (class 2606 OID 49846)
-- Name: MatchMember_PlayerId_TeamId_key; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_TeamId_key" UNIQUE ("PlayerId", "TeamId");


--
-- TOC entry 2451 (class 2606 OID 49824)
-- Name: MatchMember_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_pkey" PRIMARY KEY ("MemberId");


--
-- TOC entry 2446 (class 2606 OID 49818)
-- Name: MatchTeam_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_pkey" PRIMARY KEY ("TeamId");


--
-- TOC entry 2454 (class 2606 OID 49829)
-- Name: MemberAchievements_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_pkey" PRIMARY KEY ("AchievementId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2473 (class 2606 OID 91004)
-- Name: AbstractPlayerItemInstance_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "AbstractPlayerItemInstance_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2406 (class 2606 OID 16797)
-- Name: FriendEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_pkey" PRIMARY KEY ("MemberId");


--
-- TOC entry 2512 (class 2606 OID 115905)
-- Name: InstanceOfInstalledAddon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_pkey" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2510 (class 2606 OID 107673)
-- Name: InstanceOfItemAddon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "InstanceOfItemAddon_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2494 (class 2606 OID 91174)
-- Name: InstanceOfItemSkin_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfItemSkin_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2508 (class 2606 OID 107658)
-- Name: InstanceOfItemSkin_pkey1; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "InstanceOfItemSkin_pkey1" PRIMARY KEY ("ItemId");


--
-- TOC entry 2476 (class 2606 OID 91095)
-- Name: InstanceOfPlayerArmour_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "InstanceOfPlayerArmour_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2404 (class 2606 OID 91081)
-- Name: InstanceOfPlayerCharacter_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "InstanceOfPlayerCharacter_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2529 (class 2606 OID 214752)
-- Name: InstanceOfPlayerKit_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerKit"
    ADD CONSTRAINT "InstanceOfPlayerKit_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2478 (class 2606 OID 91088)
-- Name: InstanceOfPlayerWeapon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "InstanceOfPlayerWeapon_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2486 (class 2606 OID 91134)
-- Name: PlayerAchievement_AchievementTypeId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_AchievementTypeId_key" UNIQUE ("AchievementTypeId");


--
-- TOC entry 2488 (class 2606 OID 91136)
-- Name: PlayerAchievement_PlayerId_LastAchievementInstanceId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_PlayerId_LastAchievementInstanceId_key" UNIQUE ("PlayerId", "LastAchievementInstanceId");


--
-- TOC entry 2490 (class 2606 OID 91132)
-- Name: PlayerAchievement_PlayerId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_PlayerId_key" UNIQUE ("PlayerId");


--
-- TOC entry 2492 (class 2606 OID 91130)
-- Name: PlayerAchievement_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_pkey" PRIMARY KEY ("PlayerAchievementId");


--
-- TOC entry 2394 (class 2606 OID 16783)
-- Name: PlayerAssetEnity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PlayerAssetEnity_pkey" PRIMARY KEY ("AssetId");


--
-- TOC entry 2411 (class 2606 OID 214819)
-- Name: PlayerEntity_PlayerName_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerName_key" UNIQUE ("PlayerName");


--
-- TOC entry 2414 (class 2606 OID 16801)
-- Name: PlayerEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_pkey" PRIMARY KEY ("PlayerId");


--
-- TOC entry 2527 (class 2606 OID 214719)
-- Name: PlayerPromoCode_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerPromoCode"
    ADD CONSTRAINT "PlayerPromoCode_pkey" PRIMARY KEY ("PlayerCodeId");


--
-- TOC entry 2515 (class 2606 OID 149173)
-- Name: PlayerReputationEntity_PlayerId_FractionId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_PlayerId_FractionId_key" UNIQUE ("PlayerId", "FractionId");


--
-- TOC entry 2517 (class 2606 OID 149151)
-- Name: PlayerReputationEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_pkey" PRIMARY KEY ("ReputationId");


--
-- TOC entry 2531 (class 2606 OID 214757)
-- Name: ProfileItemEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "ProfileItemEntity"
    ADD CONSTRAINT "ProfileItemEntity_pkey" PRIMARY KEY ("ProfileItemId");


SET search_path = "Promo", pg_catalog;

--
-- TOC entry 2523 (class 2606 OID 214741)
-- Name: Codes_Name_key; Type: CONSTRAINT; Schema: Promo; Owner: postgres
--

ALTER TABLE ONLY "Codes"
    ADD CONSTRAINT "Codes_Name_key" UNIQUE ("Name");


--
-- TOC entry 2525 (class 2606 OID 214702)
-- Name: Codes_pkey; Type: CONSTRAINT; Schema: Promo; Owner: postgres
--

ALTER TABLE ONLY "Codes"
    ADD CONSTRAINT "Codes_pkey" PRIMARY KEY ("PromoId");


--
-- TOC entry 2519 (class 2606 OID 214739)
-- Name: Partners_Login_key; Type: CONSTRAINT; Schema: Promo; Owner: postgres
--

ALTER TABLE ONLY "Partners"
    ADD CONSTRAINT "Partners_Login_key" UNIQUE ("Login");


--
-- TOC entry 2521 (class 2606 OID 214697)
-- Name: Partners_pkey; Type: CONSTRAINT; Schema: Promo; Owner: postgres
--

ALTER TABLE ONLY "Partners"
    ADD CONSTRAINT "Partners_pkey" PRIMARY KEY ("PartnerId");


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2463 (class 2606 OID 90954)
-- Name: AbstractItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "AbstractItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2500 (class 2606 OID 107620)
-- Name: AddonItemContainer_AddonId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_AddonId_key" UNIQUE ("ModelId");


--
-- TOC entry 2502 (class 2606 OID 107624)
-- Name: AddonItemContainer_CategoryTypeId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_CategoryTypeId_key" UNIQUE ("CategoryTypeId");


--
-- TOC entry 2504 (class 2606 OID 107622)
-- Name: AddonItemContainer_ItemId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ItemId_key" UNIQUE ("ItemId");


--
-- TOC entry 2506 (class 2606 OID 107618)
-- Name: AddonItemContainer_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_pkey" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2496 (class 2606 OID 107626)
-- Name: AddonItemInstance_ModelId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "AddonItemInstance_ModelId_key" UNIQUE ("ModelId");


--
-- TOC entry 2480 (class 2606 OID 91076)
-- Name: AmmoItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "AmmoItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2465 (class 2606 OID 90959)
-- Name: ArmourItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2471 (class 2606 OID 90985)
-- Name: CharacterItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "CharacterItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2416 (class 2606 OID 16813)
-- Name: FractionEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "FractionEntity_pkey" PRIMARY KEY ("FractionId");


--
-- TOC entry 2498 (class 2606 OID 107610)
-- Name: ItemAddonInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "ItemAddonInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2533 (class 2606 OID 214791)
-- Name: KitItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "KitItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2469 (class 2606 OID 90979)
-- Name: SkinItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "SkinItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2467 (class 2606 OID 90972)
-- Name: WeaponItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "WeaponItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


SET search_path = public, pg_catalog;

--
-- TOC entry 2552 (class 2606 OID 214959)
-- Name: ClusterInstanceEntity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterInstanceEntity"
    ADD CONSTRAINT "ClusterInstanceEntity_pkey" PRIMARY KEY ("ClusterId");


--
-- TOC entry 2554 (class 2606 OID 214975)
-- Name: ClusterNodeEntity_ActiveMatchId_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterNodeEntity"
    ADD CONSTRAINT "ClusterNodeEntity_ActiveMatchId_key" UNIQUE ("ActiveMatchId");


--
-- TOC entry 2556 (class 2606 OID 214977)
-- Name: ClusterNodeEntity_ClusterId_Host_Port_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterNodeEntity"
    ADD CONSTRAINT "ClusterNodeEntity_ClusterId_Host_Port_key" UNIQUE ("ClusterId", "Address_Host", "Address_Port");


--
-- TOC entry 2558 (class 2606 OID 214967)
-- Name: ClusterNodeEntity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterNodeEntity"
    ADD CONSTRAINT "ClusterNodeEntity_pkey" PRIMARY KEY ("NodeId");


--
-- TOC entry 2457 (class 2606 OID 66325)
-- Name: GlobalChatMessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2461 (class 2606 OID 66335)
-- Name: PrivateChatMessageData_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2459 (class 2606 OID 66330)
-- Name: PrivateChatMessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2535 (class 2606 OID 214879)
-- Name: SessionEntity_PlayerId_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GameSessionEntity"
    ADD CONSTRAINT "SessionEntity_PlayerId_key" UNIQUE ("PlayerId");


--
-- TOC entry 2537 (class 2606 OID 214877)
-- Name: SessionEntity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GameSessionEntity"
    ADD CONSTRAINT "SessionEntity_pkey" PRIMARY KEY ("SessionId");


--
-- TOC entry 2541 (class 2606 OID 214891)
-- Name: SessionQueueEntity_PlayerId_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionQueueEntity"
    ADD CONSTRAINT "SessionQueueEntity_PlayerId_key" UNIQUE ("PlayerId");


--
-- TOC entry 2543 (class 2606 OID 223548)
-- Name: SessionQueueEntity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionQueueEntity"
    ADD CONSTRAINT "SessionQueueEntity_pkey" PRIMARY KEY ("QueueId");


--
-- TOC entry 2545 (class 2606 OID 214898)
-- Name: SessionQueueMember_PlayerId_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionQueueMember"
    ADD CONSTRAINT "SessionQueueMember_PlayerId_key" UNIQUE ("PlayerId");


--
-- TOC entry 2547 (class 2606 OID 223570)
-- Name: SessionQueueMember_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionQueueMember"
    ADD CONSTRAINT "SessionQueueMember_pkey" PRIMARY KEY ("QueueMemberId");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2418 (class 2606 OID 16825)
-- Name: Deposits_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Deposits"
    ADD CONSTRAINT "Deposits_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2420 (class 2606 OID 16827)
-- Name: Roles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2423 (class 2606 OID 16829)
-- Name: UserClaims_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "UserClaims_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2428 (class 2606 OID 16831)
-- Name: UserLogins_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "UserLogins_pkey" PRIMARY KEY ("UserId", "LoginProvider", "ProviderKey");


--
-- TOC entry 2432 (class 2606 OID 16833)
-- Name: UserRoles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "UserRoles_pkey" PRIMARY KEY ("UserId", "RoleId");


--
-- TOC entry 2439 (class 2606 OID 16835)
-- Name: Users_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("Id");


SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2449 (class 1259 OID 215668)
-- Name: MatchMember_btree_player_match; Type: INDEX; Schema: Matches; Owner: postgres
--

CREATE INDEX "MatchMember_btree_player_match" ON "MatchMember" USING btree ("PlayerId", "MatchId");


--
-- TOC entry 2452 (class 1259 OID 223630)
-- Name: MatchMember_team; Type: INDEX; Schema: Matches; Owner: postgres
--

CREATE INDEX "MatchMember_team" ON "MatchMember" USING btree ("TeamId");


--
-- TOC entry 2444 (class 1259 OID 223629)
-- Name: Match_WinnerTeam; Type: INDEX; Schema: Matches; Owner: postgres
--

CREATE UNIQUE INDEX "Match_WinnerTeam" ON "MatchEntity" USING btree ("WinnerTeamId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2474 (class 1259 OID 215674)
-- Name: InstanceOfPlayerItem_PlayerId; Type: INDEX; Schema: Players; Owner: postgres
--

CREATE INDEX "InstanceOfPlayerItem_PlayerId" ON "InstanceOfPlayerItem" USING btree ("PlayerId");


--
-- TOC entry 2407 (class 1259 OID 215671)
-- Name: PlayerEntity_LastActivity; Type: INDEX; Schema: Players; Owner: postgres
--

CREATE INDEX "PlayerEntity_LastActivity" ON "PlayerEntity" USING btree ("LastActivityDate");


--
-- TOC entry 2408 (class 1259 OID 215669)
-- Name: PlayerEntity_Level; Type: INDEX; Schema: Players; Owner: postgres
--

CREATE INDEX "PlayerEntity_Level" ON "PlayerEntity" USING btree ("Experience_Level");


--
-- TOC entry 2409 (class 1259 OID 215673)
-- Name: PlayerEntity_Login; Type: INDEX; Schema: Players; Owner: postgres
--

CREATE INDEX "PlayerEntity_Login" ON "PlayerEntity" USING hash ("PlayerName");


--
-- TOC entry 2412 (class 1259 OID 215670)
-- Name: PlayerEntity_Premium; Type: INDEX; Schema: Players; Owner: postgres
--

CREATE INDEX "PlayerEntity_Premium" ON "PlayerEntity" USING btree ("PremiumEndDate");


--
-- TOC entry 2513 (class 1259 OID 215675)
-- Name: PlayerReputationEntity_PlayerId; Type: INDEX; Schema: Players; Owner: postgres
--

CREATE INDEX "PlayerReputationEntity_PlayerId" ON "PlayerReputationEntity" USING btree ("PlayerId");


SET search_path = public, pg_catalog;

--
-- TOC entry 2548 (class 1259 OID 223633)
-- Name: ClusterInstanceEntity_Address; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "ClusterInstanceEntity_Address" ON "ClusterInstanceEntity" USING btree ("MachineAddress");


--
-- TOC entry 2549 (class 1259 OID 223632)
-- Name: ClusterInstanceEntity_Region; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "ClusterInstanceEntity_Region" ON "ClusterInstanceEntity" USING btree ("RegionTypeId");


--
-- TOC entry 2550 (class 1259 OID 223631)
-- Name: ClusterInstanceEntity_State; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "ClusterInstanceEntity_State" ON "ClusterInstanceEntity" USING btree ("State");


--
-- TOC entry 2455 (class 1259 OID 215679)
-- Name: GlobalChatMessage_Date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "GlobalChatMessage_Date" ON "GlobalChatMessage" USING btree ("Date");


--
-- TOC entry 2538 (class 1259 OID 215677)
-- Name: SessionQueueEntit_PlayerID; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "SessionQueueEntit_PlayerID" ON "SessionQueueEntity" USING btree ("PlayerId");


--
-- TOC entry 2539 (class 1259 OID 215678)
-- Name: SessionQueueEntit_PlayerReady; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "SessionQueueEntit_PlayerReady" ON "SessionQueueEntity" USING btree ("PlayerId", "Ready");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2421 (class 1259 OID 16836)
-- Name: IX_UserClaims_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserClaims_UserId" ON "UserClaims" USING btree ("UserId");


--
-- TOC entry 2424 (class 1259 OID 16837)
-- Name: IX_UserLogins_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserLogins_UserId" ON "UserLogins" USING btree ("UserId");


--
-- TOC entry 2429 (class 1259 OID 16838)
-- Name: IX_UserRoles_RoleId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserRoles_RoleId" ON "UserRoles" USING btree ("RoleId");


--
-- TOC entry 2430 (class 1259 OID 16839)
-- Name: IX_UserRoles_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserRoles_UserId" ON "UserRoles" USING btree ("UserId");


--
-- TOC entry 2425 (class 1259 OID 223627)
-- Name: UserLogins_Key; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE UNIQUE INDEX "UserLogins_Key" ON "UserLogins" USING btree ("ProviderKey");


--
-- TOC entry 2426 (class 1259 OID 223628)
-- Name: UserLogins_Provider; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "UserLogins_Provider" ON "UserLogins" USING hash ("LoginProvider");


--
-- TOC entry 2433 (class 1259 OID 215681)
-- Name: Users_Email; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "Users_Email" ON "Users" USING hash ("Email");


--
-- TOC entry 2434 (class 1259 OID 223626)
-- Name: Users_Email_BTree; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE UNIQUE INDEX "Users_Email_BTree" ON "Users" USING btree ("Email");


--
-- TOC entry 2435 (class 1259 OID 215680)
-- Name: Users_Login; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "Users_Login" ON "Users" USING hash ("UserName");


--
-- TOC entry 2436 (class 1259 OID 223625)
-- Name: Users_Login_BTree; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE UNIQUE INDEX "Users_Login_BTree" ON "Users" USING btree ("UserName");


--
-- TOC entry 2437 (class 1259 OID 223624)
-- Name: Users_Phone; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE UNIQUE INDEX "Users_Phone" ON "Users" USING btree ("PhoneNumber");


SET search_path = "GameMode", pg_catalog;

--
-- TOC entry 2633 (class 2606 OID 215338)
-- Name: GameMapEntity_GameModeId_fkey; Type: FK CONSTRAINT; Schema: GameMode; Owner: postgres
--

ALTER TABLE ONLY "GameMapEntity"
    ADD CONSTRAINT "GameMapEntity_GameModeId_fkey" FOREIGN KEY ("GameModeId") REFERENCES "AbstractGameModeEntity"("GameModeId") ON DELETE CASCADE;


--
-- TOC entry 2634 (class 2606 OID 215348)
-- Name: TeamGameModeEntity_GameModeId_fkey; Type: FK CONSTRAINT; Schema: GameMode; Owner: postgres
--

ALTER TABLE ONLY "TeamGameModeEntity"
    ADD CONSTRAINT "TeamGameModeEntity_GameModeId_fkey" FOREIGN KEY ("GameModeId") REFERENCES "AbstractGameModeEntity"("GameModeId") ON DELETE CASCADE;


SET search_path = "League", pg_catalog;

--
-- TOC entry 2569 (class 2606 OID 16875)
-- Name: LeagueMemberEntity_LeagueId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_LeagueId_fkey" FOREIGN KEY ("LeagueId") REFERENCES "LeagueEntity"("LeagueId") ON DELETE CASCADE;


--
-- TOC entry 2570 (class 2606 OID 16880)
-- Name: LeagueMemberEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2583 (class 2606 OID 223583)
-- Name: MatchEntity_NodeId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_NodeId_fkey" FOREIGN KEY ("NodeId") REFERENCES public."ClusterNodeEntity"("NodeId") ON DELETE SET NULL;


--
-- TOC entry 2582 (class 2606 OID 49886)
-- Name: MatchEntity_WinnerTeamId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_fkey" FOREIGN KEY ("WinnerTeamId") REFERENCES "MatchTeam"("TeamId");


--
-- TOC entry 2587 (class 2606 OID 49870)
-- Name: MatchMember_MatchId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2585 (class 2606 OID 49835)
-- Name: MatchMember_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2586 (class 2606 OID 49840)
-- Name: MatchMember_TeamId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_TeamId_fkey" FOREIGN KEY ("TeamId") REFERENCES "MatchTeam"("TeamId") ON DELETE CASCADE;


--
-- TOC entry 2584 (class 2606 OID 49830)
-- Name: MatchTeam_MatchId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2588 (class 2606 OID 49847)
-- Name: MemberAchievements_MemberId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_MemberId_fkey" FOREIGN KEY ("MemberId") REFERENCES "MatchMember"("MemberId") ON DELETE CASCADE;


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2572 (class 2606 OID 16895)
-- Name: FriendEntity_FriendId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_FriendId_fkey" FOREIGN KEY ("FriendId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2573 (class 2606 OID 16900)
-- Name: FriendEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2617 (class 2606 OID 115894)
-- Name: InstanceOfInstalledAddon_AddonId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_AddonId_fkey" FOREIGN KEY ("AddonId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2616 (class 2606 OID 115899)
-- Name: InstanceOfInstalledAddon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2615 (class 2606 OID 107674)
-- Name: InstanceOfItemAddon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "InstanceOfItemAddon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2614 (class 2606 OID 107679)
-- Name: InstanceOfItemSkin_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "InstanceOfItemSkin_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2610 (class 2606 OID 91180)
-- Name: InstanceOfItemSkin_TargetPlayerItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfItemSkin_TargetPlayerItemId_fkey" FOREIGN KEY ("TargetPlayerItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2602 (class 2606 OID 91096)
-- Name: InstanceOfPlayerArmour_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "InstanceOfPlayerArmour_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2571 (class 2606 OID 91082)
-- Name: InstanceOfPlayerCharacter_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "InstanceOfPlayerCharacter_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2600 (class 2606 OID 91198)
-- Name: InstanceOfPlayerItem_ModelId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "InstanceOfPlayerItem_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "Store"."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2601 (class 2606 OID 91101)
-- Name: InstanceOfPlayerItem_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "InstanceOfPlayerItem_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2603 (class 2606 OID 91089)
-- Name: InstanceOfPlayerWeapon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "InstanceOfPlayerWeapon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2608 (class 2606 OID 107689)
-- Name: InstanceOfTargetItem_ModelId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfTargetItem_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "Store"."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2609 (class 2606 OID 107684)
-- Name: InstanceOfTargetItem_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfTargetItem_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2606 (class 2606 OID 91142)
-- Name: PlayerAchievement_AchievementTypeId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_AchievementTypeId_fkey" FOREIGN KEY ("AchievementTypeId") REFERENCES "Achievements"."AchievementContainer"("AchievementTypeId") ON DELETE CASCADE;


--
-- TOC entry 2605 (class 2606 OID 91152)
-- Name: PlayerAchievement_LastAchievementInstanceId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_LastAchievementInstanceId_fkey" FOREIGN KEY ("LastAchievementInstanceId") REFERENCES "Achievements"."AchievementInstance"("AchievementBonusId");


--
-- TOC entry 2607 (class 2606 OID 91137)
-- Name: PlayerAchievement_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2576 (class 2606 OID 149167)
-- Name: PlayerEntity_CurrentFractionId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_CurrentFractionId_fkey" FOREIGN KEY ("CurrentFractionId") REFERENCES "PlayerReputationEntity"("ReputationId");


--
-- TOC entry 2574 (class 2606 OID 214731)
-- Name: PlayerEntity_FirstPartnerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_FirstPartnerId_fkey" FOREIGN KEY ("FirstPartnerId") REFERENCES "Promo"."Partners"("PartnerId");


--
-- TOC entry 2575 (class 2606 OID 16905)
-- Name: PlayerEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES vrs."Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2577 (class 2606 OID 214868)
-- Name: PlayerEntity_SessionMatchId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_SessionMatchId_fkey" FOREIGN KEY ("MatchMemberId") REFERENCES "Matches"."MatchMember"("MemberId") ON DELETE SET NULL;


--
-- TOC entry 2567 (class 2606 OID 214808)
-- Name: PlayerProfileEntity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PlayerProfileEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "InstanceOfPlayerCharacter"("ItemId");


--
-- TOC entry 2622 (class 2606 OID 214725)
-- Name: PlayerPromoCode_CodeId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerPromoCode"
    ADD CONSTRAINT "PlayerPromoCode_CodeId_fkey" FOREIGN KEY ("CodeId") REFERENCES "Promo"."Codes"("PromoId") ON DELETE CASCADE;


--
-- TOC entry 2621 (class 2606 OID 214803)
-- Name: PlayerPromoCode_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerPromoCode"
    ADD CONSTRAINT "PlayerPromoCode_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2619 (class 2606 OID 149152)
-- Name: PlayerReputationEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "Store"."FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2618 (class 2606 OID 149157)
-- Name: PlayerReputationEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2568 (class 2606 OID 16870)
-- Name: PreSetEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PreSetEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2624 (class 2606 OID 214758)
-- Name: ProfileItemEntity_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "ProfileItemEntity"
    ADD CONSTRAINT "ProfileItemEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2623 (class 2606 OID 214763)
-- Name: ProfileItemEntity_ProfileId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "ProfileItemEntity"
    ADD CONSTRAINT "ProfileItemEntity_ProfileId_fkey" FOREIGN KEY ("ProfileId") REFERENCES "PlayerProfileEntity"("AssetId") ON DELETE CASCADE;


SET search_path = "Promo", pg_catalog;

--
-- TOC entry 2620 (class 2606 OID 214710)
-- Name: Codes_PartnerId_fkey; Type: FK CONSTRAINT; Schema: Promo; Owner: postgres
--

ALTER TABLE ONLY "Codes"
    ADD CONSTRAINT "Codes_PartnerId_fkey" FOREIGN KEY ("PartnerId") REFERENCES "Partners"("PartnerId") ON DELETE CASCADE;


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2595 (class 2606 OID 148767)
-- Name: AbstractItemInstance_FractionId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "AbstractItemInstance_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2613 (class 2606 OID 107632)
-- Name: AddonItemContainer_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ItemId_fkey" FOREIGN KEY ("ItemId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2612 (class 2606 OID 107640)
-- Name: AddonItemContainer_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ModelId_fkey" FOREIGN KEY ("ModelId", "ModelCategoryTypeId") REFERENCES "AddonItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2611 (class 2606 OID 214782)
-- Name: AddonItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "AddonItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2604 (class 2606 OID 148757)
-- Name: AmmoItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "AmmoItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2596 (class 2606 OID 90960)
-- Name: ArmourItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2599 (class 2606 OID 214777)
-- Name: CharacterItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "CharacterItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2625 (class 2606 OID 214792)
-- Name: KitItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "KitItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2598 (class 2606 OID 148762)
-- Name: SkinItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "SkinItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2597 (class 2606 OID 214772)
-- Name: WeaponItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "WeaponItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


SET search_path = public, pg_catalog;

--
-- TOC entry 2632 (class 2606 OID 214983)
-- Name: ClusterNodeEntity_ActiveMatchId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterNodeEntity"
    ADD CONSTRAINT "ClusterNodeEntity_ActiveMatchId_fkey" FOREIGN KEY ("ActiveMatchId") REFERENCES "Matches"."MatchEntity"("MatchId") ON DELETE SET NULL;


--
-- TOC entry 2631 (class 2606 OID 214978)
-- Name: ClusterNodeEntity_ClusterId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterNodeEntity"
    ADD CONSTRAINT "ClusterNodeEntity_ClusterId_fkey" FOREIGN KEY ("ClusterId") REFERENCES "ClusterInstanceEntity"("ClusterId") ON DELETE CASCADE;


--
-- TOC entry 2589 (class 2606 OID 124086)
-- Name: GlobalChatMessage_EditorId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_EditorId_fkey" FOREIGN KEY ("EditorId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2590 (class 2606 OID 124081)
-- Name: GlobalChatMessage_SenderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2594 (class 2606 OID 66356)
-- Name: PrivateChatMessageData_ReceiverId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_ReceiverId_fkey" FOREIGN KEY ("ReceiverId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2593 (class 2606 OID 66351)
-- Name: PrivateChatMessageData_SenderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2592 (class 2606 OID 66346)
-- Name: PrivateChatMessage_MessageDataId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_MessageDataId_fkey" FOREIGN KEY ("MessageDataId") REFERENCES "PrivateChatMessageData"("MessageId") ON DELETE CASCADE;


--
-- TOC entry 2591 (class 2606 OID 66336)
-- Name: PrivateChatMessage_PlayerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2627 (class 2606 OID 223549)
-- Name: SessionQueueEntity_GameModeTypeId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionQueueEntity"
    ADD CONSTRAINT "SessionQueueEntity_GameModeTypeId_fkey" FOREIGN KEY ("GameModeTypeId") REFERENCES "GameMode"."AbstractGameModeEntity"("GameModeId") ON DELETE CASCADE;


--
-- TOC entry 2626 (class 2606 OID 223559)
-- Name: SessionQueueEntity_MatchId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionQueueEntity"
    ADD CONSTRAINT "SessionQueueEntity_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "Matches"."MatchEntity"("MatchId") ON DELETE SET NULL;


--
-- TOC entry 2628 (class 2606 OID 214936)
-- Name: SessionQueueEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionQueueEntity"
    ADD CONSTRAINT "SessionQueueEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2630 (class 2606 OID 214946)
-- Name: SessionQueueMember_PlayerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionQueueMember"
    ADD CONSTRAINT "SessionQueueMember_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2629 (class 2606 OID 223576)
-- Name: SessionQueueMember_QueueId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionQueueMember"
    ADD CONSTRAINT "SessionQueueMember_QueueId_fkey" FOREIGN KEY ("QueueId") REFERENCES "SessionQueueEntity"("QueueId") ON DELETE CASCADE;


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2578 (class 2606 OID 16950)
-- Name: FK_UserClaims_Users_User_Id; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "FK_UserClaims_Users_User_Id" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2579 (class 2606 OID 16955)
-- Name: FK_UserLogins_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "FK_UserLogins_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2580 (class 2606 OID 16960)
-- Name: FK_UserRoles_Roles_RoleId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Roles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "Roles"("Id") ON DELETE CASCADE;


--
-- TOC entry 2581 (class 2606 OID 16965)
-- Name: FK_UserRoles_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2812 (class 0 OID 0)
-- Dependencies: 15
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-07-19 02:14:53

--
-- PostgreSQL database dump complete
--

