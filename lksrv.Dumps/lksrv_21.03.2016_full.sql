--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.4.5
-- Started on 2016-03-21 20:07:36

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 19 (class 2615 OID 78532)
-- Name: Inventory; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Inventory";


ALTER SCHEMA "Inventory" OWNER TO postgres;

--
-- TOC entry 18 (class 2615 OID 45439)
-- Name: League; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "League";


ALTER SCHEMA "League" OWNER TO postgres;

--
-- TOC entry 17 (class 2615 OID 44951)
-- Name: Players; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Players";


ALTER SCHEMA "Players" OWNER TO postgres;

--
-- TOC entry 16 (class 2615 OID 44666)
-- Name: Store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Store";


ALTER SCHEMA "Store" OWNER TO postgres;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2543 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 14 (class 2615 OID 18804)
-- Name: vrs; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA vrs;


ALTER SCHEMA vrs OWNER TO postgres;

SET search_path = "Inventory", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 285 (class 1259 OID 78522)
-- Name: ArmourEntity; Type: TABLE; Schema: Inventory; Owner: postgres; Tablespace: 
--

CREATE TABLE "ArmourEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "PrimaryAmmoAmount" smallint DEFAULT 0 NOT NULL,
    "SecondaryAmmoAmount" smallint DEFAULT 0 NOT NULL,
    "ItemModelId" smallint NOT NULL
);


ALTER TABLE "ArmourEntity" OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 45159)
-- Name: PreSetEntity; Type: TABLE; Schema: Inventory; Owner: postgres; Tablespace: 
--

CREATE TABLE "PreSetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CharacterId" uuid NOT NULL,
    "HelmetId" uuid,
    "MaskId" uuid,
    "ArmourId" uuid,
    "BackpackId" uuid,
    "GlovesId" uuid,
    "PantsId" uuid,
    "BootsId" uuid,
    "PrimaryWeaponId" uuid,
    "SecondaryWeaponId" uuid,
    "PrimaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "PrimaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "PlayerId" uuid NOT NULL
);


ALTER TABLE "PreSetEntity" OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 44966)
-- Name: WeaponEntity; Type: TABLE; Schema: Inventory; Owner: postgres; Tablespace: 
--

CREATE TABLE "WeaponEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "ItemModelId" smallint NOT NULL
);


ALTER TABLE "WeaponEntity" OWNER TO postgres;

SET search_path = "League", pg_catalog;

--
-- TOC entry 279 (class 1259 OID 45440)
-- Name: LeagueEntity; Type: TABLE; Schema: League; Owner: postgres; Tablespace: 
--

CREATE TABLE "LeagueEntity" (
    "LeagueId" uuid NOT NULL,
    "Cash_Money" smallint NOT NULL,
    "Cash_Donate" smallint NOT NULL,
    "Info_Name" character varying(20) NOT NULL,
    "Info_Abbr" character varying(5) NOT NULL,
    "Info_FoundedDate" timestamp without time zone NOT NULL,
    "Info_AccessType" smallint NOT NULL,
    "Info_JoinPrice" smallint NOT NULL
);


ALTER TABLE "LeagueEntity" OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 45449)
-- Name: LeagueMemberEntity; Type: TABLE; Schema: League; Owner: postgres; Tablespace: 
--

CREATE TABLE "LeagueMemberEntity" (
    "LeagueId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Access" smallint NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "LeagueMemberEntity" OWNER TO postgres;

SET search_path = "Players", pg_catalog;

--
-- TOC entry 276 (class 1259 OID 45051)
-- Name: CharacterEntity; Type: TABLE; Schema: Players; Owner: postgres; Tablespace: 
--

CREATE TABLE "CharacterEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "ItemModelId" smallint NOT NULL
);


ALTER TABLE "CharacterEntity" OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 45420)
-- Name: FriendEntity; Type: TABLE; Schema: Players; Owner: postgres; Tablespace: 
--

CREATE TABLE "FriendEntity" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FriendId" uuid NOT NULL
);


ALTER TABLE "FriendEntity" OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 44952)
-- Name: PlayerEntity; Type: TABLE; Schema: Players; Owner: postgres; Tablespace: 
--

CREATE TABLE "PlayerEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerName" character varying(24) NOT NULL,
    "Experience_Level" smallint DEFAULT 0 NOT NULL,
    "Experience_Experience" bigint DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "PremiumEndDate" timestamp without time zone,
    "LastActivitiDate" timestamp without time zone DEFAULT now() NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "PlayerEntity" OWNER TO postgres;

SET search_path = "Store", pg_catalog;

--
-- TOC entry 286 (class 1259 OID 78562)
-- Name: CharacterArmourAssetEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "CharacterArmourAssetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ItemId" smallint NOT NULL,
    "CharacterId" smallint NOT NULL
);


ALTER TABLE "CharacterArmourAssetEntity" OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 78573)
-- Name: CharacterWeaponAssetEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "CharacterWeaponAssetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ItemId" smallint NOT NULL,
    "CharacterId" smallint NOT NULL
);


ALTER TABLE "CharacterWeaponAssetEntity" OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 44674)
-- Name: FractionEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "FractionEntity" (
    "FractionId" smallint NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" smallint DEFAULT 0 NOT NULL,
    "Discount_Ammo" smallint DEFAULT 0 NOT NULL,
    "Discount_Weapon" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FractionEntity" OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 78501)
-- Name: ItemAmmoEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ItemAmmoEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" smallint NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemAmmoEntity" OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 78469)
-- Name: ItemArmourEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ItemArmourEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemArmourEntity" OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 78481)
-- Name: ItemCharacterEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ItemCharacterEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer DEFAULT 5000 NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemCharacterEntity" OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 78453)
-- Name: ItemWeaponEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ItemWeaponEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemWeaponEntity" OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 44936)
-- Name: ModificationEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModificationEntity" (
    "ModificationId" uuid NOT NULL,
    "ModificationTypeId" smallint DEFAULT 0 NOT NULL,
    "Range_Minimum" smallint DEFAULT 2 NOT NULL,
    "Range_Maximum" smallint DEFAULT 10 NOT NULL,
    "Range_Chance" smallint DEFAULT 50 NOT NULL
);


ALTER TABLE "ModificationEntity" OWNER TO postgres;

SET search_path = vrs, pg_catalog;

--
-- TOC entry 260 (class 1259 OID 19096)
-- Name: Deposits; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "Deposits" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserId" uuid NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "Value" bigint DEFAULT 0 NOT NULL,
    "Currency" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "Deposits" OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 19102)
-- Name: Roles; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "Roles" (
    "Id" uuid NOT NULL,
    "Name" character varying(256) NOT NULL
);


ALTER TABLE "Roles" OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 19105)
-- Name: UserClaims; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "UserClaims" (
    "Id" integer NOT NULL,
    "ClaimType" character varying(256),
    "ClaimValue" character varying(256),
    "UserId" uuid NOT NULL
);


ALTER TABLE "UserClaims" OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 19111)
-- Name: UserClaims_Id_seq; Type: SEQUENCE; Schema: vrs; Owner: postgres
--

CREATE SEQUENCE "UserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "UserClaims_Id_seq" OWNER TO postgres;

--
-- TOC entry 2545 (class 0 OID 0)
-- Dependencies: 263
-- Name: UserClaims_Id_seq; Type: SEQUENCE OWNED BY; Schema: vrs; Owner: postgres
--

ALTER SEQUENCE "UserClaims_Id_seq" OWNED BY "UserClaims"."Id";


--
-- TOC entry 264 (class 1259 OID 19113)
-- Name: UserLogins; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "UserLogins" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "LoginProvider" character varying(128) NOT NULL,
    "ProviderKey" character varying(128) NOT NULL
);


ALTER TABLE "UserLogins" OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 19116)
-- Name: UserRoles; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "UserRoles" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RoleId" uuid NOT NULL
);


ALTER TABLE "UserRoles" OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 19119)
-- Name: Users; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "Users" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserName" character varying(256) NOT NULL,
    "PasswordHash" character varying(256),
    "SecurityStamp" uuid,
    "Email" character varying(256) DEFAULT NULL::character varying,
    "EmailConfirmed" boolean DEFAULT false NOT NULL,
    "PhoneNumber" character varying(32),
    "PhoneNumberConfirmed" boolean DEFAULT false NOT NULL,
    "TwoFactorEnabled" boolean DEFAULT false NOT NULL,
    "LockoutEndDateUtc" timestamp without time zone,
    "LockoutEnabled" boolean DEFAULT false NOT NULL,
    "AccessFailedCount" smallint DEFAULT 0 NOT NULL,
    "PlayerEntityId" bigint,
    "Balance" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "Users" OWNER TO postgres;

--
-- TOC entry 2243 (class 2604 OID 19149)
-- Name: Id; Type: DEFAULT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"UserClaims_Id_seq"'::regclass);


SET search_path = "Inventory", pg_catalog;

--
-- TOC entry 2536 (class 0 OID 78522)
-- Dependencies: 285
-- Data for Name: ArmourEntity; Type: TABLE DATA; Schema: Inventory; Owner: postgres
--

INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('bcb6a5e7-9b81-438c-a5b3-6ba0f7b231ca', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0, 0, 0, 0, 0);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('e5ff33b2-dfa9-49a5-8b87-0639b384a1eb', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0, 0, 0, 0, 1);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('2441cf67-ee5f-4ec1-970e-366f1d27f1f8', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0, 0, 0, 0, 2);
INSERT INTO "ArmourEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "PrimaryAmmoAmount", "SecondaryAmmoAmount", "ItemModelId") VALUES ('9543e908-8e97-4a91-95cd-081cdec223f2', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0, 0, 0, 0, 3);


--
-- TOC entry 2528 (class 0 OID 45159)
-- Dependencies: 277
-- Data for Name: PreSetEntity; Type: TABLE DATA; Schema: Inventory; Owner: postgres
--

INSERT INTO "PreSetEntity" ("AssetId", "CharacterId", "HelmetId", "MaskId", "ArmourId", "BackpackId", "GlovesId", "PantsId", "BootsId", "PrimaryWeaponId", "SecondaryWeaponId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId") VALUES ('c7c57fcd-59c7-457a-aa4f-d5310a773f40', '714a5816-207b-4fa6-b81b-bb510228df73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8b0bebc7-6f5a-468d-be48-1f743630a5c5', NULL, 0, 0, 0, 0, '2a5e0603-ee62-49b6-8a94-3dac8d7432ea');
INSERT INTO "PreSetEntity" ("AssetId", "CharacterId", "HelmetId", "MaskId", "ArmourId", "BackpackId", "GlovesId", "PantsId", "BootsId", "PrimaryWeaponId", "SecondaryWeaponId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId") VALUES ('ee5afb22-92bf-4b26-af4d-0ebafab1ff6e', 'dfa63b31-4424-4a83-80e1-596867db0065', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '908bc34b-cc7e-4554-bba1-5103e0c19328', NULL, 0, 0, 0, 0, 'cdf4bdbb-f012-497a-aedf-7bc01bfb8d41');
INSERT INTO "PreSetEntity" ("AssetId", "CharacterId", "HelmetId", "MaskId", "ArmourId", "BackpackId", "GlovesId", "PantsId", "BootsId", "PrimaryWeaponId", "SecondaryWeaponId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId") VALUES ('ce391e5b-3658-4d93-979a-e96ea6819f9a', 'd338da0d-0009-417d-ade6-593afede3b9b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a53dbcc9-aeea-4a88-b7ea-f64a3aaa1ca8', NULL, 0, 0, 0, 0, '784e067e-5086-447d-adc7-c5f31c538618');
INSERT INTO "PreSetEntity" ("AssetId", "CharacterId", "HelmetId", "MaskId", "ArmourId", "BackpackId", "GlovesId", "PantsId", "BootsId", "PrimaryWeaponId", "SecondaryWeaponId", "PrimaryWeaponAmmo_Primary", "PrimaryWeaponAmmo_Secondary", "SecondaryWeaponAmmo_Primary", "SecondaryWeaponAmmo_Secondary", "PlayerId") VALUES ('f24908c2-89d7-4749-b0c1-3fbf5e117f0c', 'a15c4ef7-e681-4e37-aeee-525e0c630f4b', 'bcb6a5e7-9b81-438c-a5b3-6ba0f7b231ca', NULL, 'e5ff33b2-dfa9-49a5-8b87-0639b384a1eb', '2441cf67-ee5f-4ec1-970e-366f1d27f1f8', NULL, '9543e908-8e97-4a91-95cd-081cdec223f2', NULL, '662b7305-3450-46db-8858-b3157a0a9cdc', NULL, 0, 0, 0, 0, 'a92a73f5-899e-49ce-b360-e9e63aee7083');


--
-- TOC entry 2526 (class 0 OID 44966)
-- Dependencies: 275
-- Data for Name: WeaponEntity; Type: TABLE DATA; Schema: Inventory; Owner: postgres
--

INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('0753c1c3-3913-408b-88d3-00c03495a13a', 'b4fe3a5a-20fb-4736-bfd4-af1391542ada', 0, 0, 5);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('647178bb-c53b-4da7-bc80-b29730da40ed', 'b4fe3a5a-20fb-4736-bfd4-af1391542ada', 0, 0, 2);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('d7ea7da9-7658-4c52-8ab9-a2331fee82e5', 'b4fe3a5a-20fb-4736-bfd4-af1391542ada', 0, 0, 3);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('f483a0b5-9453-43cd-baab-01d7ec34787d', 'b4fe3a5a-20fb-4736-bfd4-af1391542ada', 0, 0, 0);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('fb230993-617c-47a5-a707-9a26f9a6082a', 'b4fe3a5a-20fb-4736-bfd4-af1391542ada', 0, 0, 1);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('543ee773-eabf-4efa-8d52-650d7b537a2e', '647345f0-2714-469c-af28-d70e3084796e', 0, 0, 0);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('6f769a2c-37bf-4c0c-aa30-1dc134395bc2', '647345f0-2714-469c-af28-d70e3084796e', 0, 0, 5);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('9eabeb90-ada2-4696-9ab9-255109cb13a2', '647345f0-2714-469c-af28-d70e3084796e', 0, 0, 3);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('ba895ce9-0709-42be-8ebb-26e9bdc8192a', '647345f0-2714-469c-af28-d70e3084796e', 0, 0, 2);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('d4160dce-885e-419a-a7a4-b29345633835', '647345f0-2714-469c-af28-d70e3084796e', 0, 0, 1);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('8b0bebc7-6f5a-468d-be48-1f743630a5c5', '2a5e0603-ee62-49b6-8a94-3dac8d7432ea', 0, 0, 0);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('9325100b-287d-4598-ae39-35c37ccc8158', '2a5e0603-ee62-49b6-8a94-3dac8d7432ea', 0, 0, 5);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('9f83ecb9-794c-48dc-bd19-9fcbf7acabd9', '2a5e0603-ee62-49b6-8a94-3dac8d7432ea', 0, 0, 2);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('cb108000-95f2-44b9-a1e4-ec7a0ec8180b', '2a5e0603-ee62-49b6-8a94-3dac8d7432ea', 0, 0, 3);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('d1a51980-2970-4a29-bb03-d095810f9c29', '2a5e0603-ee62-49b6-8a94-3dac8d7432ea', 0, 0, 1);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('908bc34b-cc7e-4554-bba1-5103e0c19328', 'cdf4bdbb-f012-497a-aedf-7bc01bfb8d41', 0, 0, 0);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('9ed33c0c-f126-420e-9996-28a9dbdf409c', 'cdf4bdbb-f012-497a-aedf-7bc01bfb8d41', 0, 0, 2);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('baab6e22-213d-47dc-afb8-c82adf657cce', 'cdf4bdbb-f012-497a-aedf-7bc01bfb8d41', 0, 0, 3);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('c3a8c145-3ab8-4a69-9807-3b0c39c43f34', 'cdf4bdbb-f012-497a-aedf-7bc01bfb8d41', 0, 0, 1);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('cdc0654c-1ce8-42bb-8e7f-ad0a7a1315bc', 'cdf4bdbb-f012-497a-aedf-7bc01bfb8d41', 0, 0, 5);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('3e62e1a4-7768-4ed2-a959-0e7bb8674f5f', '784e067e-5086-447d-adc7-c5f31c538618', 0, 0, 5);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('679a0f23-84cf-450e-9e55-2901bbbe42ac', '784e067e-5086-447d-adc7-c5f31c538618', 0, 0, 3);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('a53dbcc9-aeea-4a88-b7ea-f64a3aaa1ca8', '784e067e-5086-447d-adc7-c5f31c538618', 0, 0, 0);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('da42657b-c885-4f96-af7a-d9b670d27d87', '784e067e-5086-447d-adc7-c5f31c538618', 0, 0, 1);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('f273630f-7df3-403a-a605-514a0d67ccb9', '784e067e-5086-447d-adc7-c5f31c538618', 0, 0, 2);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('0a5b9424-a3f0-4165-8b68-f204a62fa2d8', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0, 0, 1);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('662b7305-3450-46db-8858-b3157a0a9cdc', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0, 0, 0);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('666f8c0d-1bc2-48e1-8780-479438878783', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0, 0, 3);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('98fe4ad7-3ffe-4e6c-8937-7974b978f1cd', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0, 0, 5);
INSERT INTO "WeaponEntity" ("ItemId", "PlayerId", "Skin_MaterialId", "Skin_TextureId", "ItemModelId") VALUES ('da3b6022-1f0f-4c52-99cf-076d27e1da7c', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0, 0, 2);


SET search_path = "League", pg_catalog;

--
-- TOC entry 2530 (class 0 OID 45440)
-- Dependencies: 279
-- Data for Name: LeagueEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--

INSERT INTO "LeagueEntity" ("LeagueId", "Cash_Money", "Cash_Donate", "Info_Name", "Info_Abbr", "Info_FoundedDate", "Info_AccessType", "Info_JoinPrice") VALUES ('ff0ec814-a947-4dfb-9fb5-cc3d266d774b', 10, 100, 'VRS PRO', 'VRS', '2016-02-10 23:33:16.507', 1, 1000);


--
-- TOC entry 2531 (class 0 OID 45449)
-- Dependencies: 280
-- Data for Name: LeagueMemberEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--



SET search_path = "Players", pg_catalog;

--
-- TOC entry 2527 (class 0 OID 45051)
-- Dependencies: 276
-- Data for Name: CharacterEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "CharacterEntity" ("ItemId", "PlayerId", "ItemModelId") VALUES ('a01b0ecc-2857-43a2-a17d-9a49e5473f84', 'b4fe3a5a-20fb-4736-bfd4-af1391542ada', 0);
INSERT INTO "CharacterEntity" ("ItemId", "PlayerId", "ItemModelId") VALUES ('0314c738-665a-4a1e-b737-91953b28cb84', '647345f0-2714-469c-af28-d70e3084796e', 0);
INSERT INTO "CharacterEntity" ("ItemId", "PlayerId", "ItemModelId") VALUES ('714a5816-207b-4fa6-b81b-bb510228df73', '2a5e0603-ee62-49b6-8a94-3dac8d7432ea', 0);
INSERT INTO "CharacterEntity" ("ItemId", "PlayerId", "ItemModelId") VALUES ('dfa63b31-4424-4a83-80e1-596867db0065', 'cdf4bdbb-f012-497a-aedf-7bc01bfb8d41', 0);
INSERT INTO "CharacterEntity" ("ItemId", "PlayerId", "ItemModelId") VALUES ('d338da0d-0009-417d-ade6-593afede3b9b', '784e067e-5086-447d-adc7-c5f31c538618', 0);
INSERT INTO "CharacterEntity" ("ItemId", "PlayerId", "ItemModelId") VALUES ('a15c4ef7-e681-4e37-aeee-525e0c630f4b', 'a92a73f5-899e-49ce-b360-e9e63aee7083', 0);


--
-- TOC entry 2529 (class 0 OID 45420)
-- Dependencies: 278
-- Data for Name: FriendEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2525 (class 0 OID 44952)
-- Dependencies: 274
-- Data for Name: PlayerEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate") VALUES ('b4fe3a5a-20fb-4736-bfd4-af1391542ada', 'Qdfsf2d', 0, 0, 0, 0, 0, '2016-03-15 23:53:13.281715', '2016-03-18 23:53:13.282215', '2016-03-15 20:53:13.341', '2016-03-15 23:53:13.282215');
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate") VALUES ('647345f0-2714-469c-af28-d70e3084796e', 'gfdhfdh4', 0, 0, 0, 0, 0, '2016-03-17 00:22:07.123371', '2016-03-20 00:22:07.123371', '2016-03-16 21:22:07.13', '2016-03-17 00:22:07.123371');
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate") VALUES ('2a5e0603-ee62-49b6-8a94-3dac8d7432ea', 'fgh5g5', 0, 0, 0, 0, 0, '2016-03-17 03:15:40.252019', '2016-03-20 03:15:40.252019', '2016-03-17 00:15:40.324', '2016-03-17 03:15:40.252019');
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate") VALUES ('cdf4bdbb-f012-497a-aedf-7bc01bfb8d41', 'jhg5h', 0, 0, 0, 0, 0, '2016-03-17 03:16:03.896738', '2016-03-20 03:16:03.896738', '2016-03-17 00:16:03.9', '2016-03-17 03:16:03.896738');
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate") VALUES ('784e067e-5086-447d-adc7-c5f31c538618', 'fh44hfg', 0, 0, 0, 0, 0, '2016-03-17 03:16:46.639195', '2016-03-20 03:16:46.639195', '2016-03-17 00:16:46.643', '2016-03-17 03:16:46.639195');
INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "LastActivityDate") VALUES ('a92a73f5-899e-49ce-b360-e9e63aee7083', 'fdgg3f', 0, 0, 0, 0, 0, '2016-03-17 03:23:29.628635', '2016-03-20 03:23:29.628635', '2016-03-17 00:23:29.668', '2016-03-17 03:23:29.628635');


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2537 (class 0 OID 78562)
-- Dependencies: 286
-- Data for Name: CharacterArmourAssetEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2538 (class 0 OID 78573)
-- Dependencies: 287
-- Data for Name: CharacterWeaponAssetEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2523 (class 0 OID 44674)
-- Dependencies: 272
-- Data for Name: FractionEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (1, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (2, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (3, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (4, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (0, 0, 0, 0, 0, 0);


--
-- TOC entry 2535 (class 0 OID 78501)
-- Dependencies: 284
-- Data for Name: ItemAmmoEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ItemAmmoEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (0, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemAmmoEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (1, 0, 7500, false, 0, 0, 0, 0);


--
-- TOC entry 2533 (class 0 OID 78469)
-- Dependencies: 282
-- Data for Name: ItemArmourEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ItemArmourEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (1, 0, 50000, false, 0, 0, 0, 0);
INSERT INTO "ItemArmourEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (2, 0, 50700, false, 0, 0, 0, 0);
INSERT INTO "ItemArmourEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (0, 0, 15000, false, 0, 0, 0, 0);
INSERT INTO "ItemArmourEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (3, 0, 58700, false, 0, 0, 0, 0);


--
-- TOC entry 2534 (class 0 OID 78481)
-- Dependencies: 283
-- Data for Name: ItemCharacterEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (0, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (1, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (2, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (3, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (4, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemCharacterEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (5, 0, 5000, false, 0, 0, 0, 0);


--
-- TOC entry 2532 (class 0 OID 78453)
-- Dependencies: 281
-- Data for Name: ItemWeaponEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (0, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (1, 0, 5000, false, 0, 0, 0, 0);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (2, 0, 3500, false, 0, 0, 0, 0);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (3, 0, 4500, false, 0, 0, 0, 0);
INSERT INTO "ItemWeaponEntity" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId") VALUES (5, 0, 7000, false, 0, 0, 0, 0);


--
-- TOC entry 2524 (class 0 OID 44936)
-- Dependencies: 273
-- Data for Name: ModificationEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



SET search_path = vrs, pg_catalog;

--
-- TOC entry 2516 (class 0 OID 19096)
-- Dependencies: 260
-- Data for Name: Deposits; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2517 (class 0 OID 19102)
-- Dependencies: 261
-- Data for Name: Roles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2518 (class 0 OID 19105)
-- Dependencies: 262
-- Data for Name: UserClaims; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2546 (class 0 OID 0)
-- Dependencies: 263
-- Name: UserClaims_Id_seq; Type: SEQUENCE SET; Schema: vrs; Owner: postgres
--

SELECT pg_catalog.setval('"UserClaims_Id_seq"', 2, true);


--
-- TOC entry 2520 (class 0 OID 19113)
-- Dependencies: 264
-- Data for Name: UserLogins; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2521 (class 0 OID 19116)
-- Dependencies: 265
-- Data for Name: UserRoles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2522 (class 0 OID 19119)
-- Dependencies: 266
-- Data for Name: Users; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('b4fe3a5a-20fb-4736-bfd4-af1391542ada', 'wsg6r35@gmail.com', 'AJbw526lLFwJYMob/VNfcnhFdZg84CL+zr2p3Iz0bPu2tq8O/arI2ZpRx1KQGuxyuw==', '130b5d3f-b094-444e-95b8-77ab17861de8', 'wsg6r35@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('647345f0-2714-469c-af28-d70e3084796e', 'wsg6r375@gmail.com', 'AJS5p8j4JhJzZWhOU0qHo4desXDRGNLSRbDHIcZugTP8PzbdPQ1dANgBocuFNFBnqA==', '56b45519-0abe-4044-b9c2-1aaea083d76c', 'wsg6r375@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('830f0d52-4c4d-4758-824b-a83938dfd5ac', 'wsgh6r375@gmail.com', 'ADA3Zvdr+n5JFduqdP5KWPnBIpoNAEepTh5wAbEb5TWwJ61x4egDxiIGaqCVOP0fNw==', 'daa2db9f-db61-4d29-8090-3c76cabbab62', 'wsgh6r375@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('2a5e0603-ee62-49b6-8a94-3dac8d7432ea', 'wsgh6r3j75@gmail.com', 'AJPvd2FBRsBUIFxMzeJBDJFNAPG3pLTXjAkuvxaeZVtFQBiXEOuhzh+hub6LnkDlmA==', '2bfd27c8-d2ee-4583-a036-59c0b6b01ae5', 'wsgh6r3j75@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('cdf4bdbb-f012-497a-aedf-7bc01bfb8d41', '6wsgh6r3j75@gmail.com', 'ADodpKofRCFp2rPP1DMk4u2OgJq8bFh3he4PgxzYtFNiWVKMv2SZlxd6zvKDgfasew==', '262c139e-0fdd-4f9d-9a3d-203045727d89', '6wsgh6r3j75@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('784e067e-5086-447d-adc7-c5f31c538618', '6hgfr3j75@gmail.com', 'ACf2xCmBwNLMWW34o1bd+2/ypzSWPg83pyMEbCotgVfEbwUi/pkpaa3tgeD/ECr6DA==', 'eb500a13-f7b0-4b67-8ed8-5d3daa02e9ca', '6hgfr3j75@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('a92a73f5-899e-49ce-b360-e9e63aee7083', 'h6hgfr3j75@gmail.com', 'AINMlImBCngqg4b+q9H3C9rguWuZ6P9Q/9Uitzts9dgCccgGZ6H0bS8k++33AeiYlg==', '2b1d38d8-f07f-4a1e-a9c2-e20d2a248e25', 'h6hgfr3j75@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);


SET search_path = "Inventory", pg_catalog;

--
-- TOC entry 2370 (class 2606 OID 78536)
-- Name: ArmourEntity_PlayerId_ItemModelId_key; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_PlayerId_ItemModelId_key" UNIQUE ("PlayerId", "ItemModelId");


--
-- TOC entry 2372 (class 2606 OID 78534)
-- Name: ArmourEntity_pkey; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2342 (class 2606 OID 78551)
-- Name: ItemWeaponEntity_PlayerId_ItemModelId_key; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_PlayerId_ItemModelId_key" UNIQUE ("PlayerId", "ItemModelId");


--
-- TOC entry 2344 (class 2606 OID 78549)
-- Name: ItemWeaponEntity_pkey; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2350 (class 2606 OID 45164)
-- Name: PlayerAssetEnity_pkey; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PlayerAssetEnity_pkey" PRIMARY KEY ("AssetId");


SET search_path = "League", pg_catalog;

--
-- TOC entry 2354 (class 2606 OID 45448)
-- Name: LeagueEntity_Info_Abbr_key; Type: CONSTRAINT; Schema: League; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Abbr_key" UNIQUE ("Info_Abbr");


--
-- TOC entry 2356 (class 2606 OID 45446)
-- Name: LeagueEntity_Info_Name_key; Type: CONSTRAINT; Schema: League; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Name_key" UNIQUE ("Info_Name");


--
-- TOC entry 2358 (class 2606 OID 45444)
-- Name: LeagueEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_pkey" PRIMARY KEY ("LeagueId");


--
-- TOC entry 2360 (class 2606 OID 45470)
-- Name: LeagueMemberEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2346 (class 2606 OID 78650)
-- Name: CharacterEntity_PlayerId_ModelId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterEntity_PlayerId_ModelId_key" UNIQUE ("PlayerId", "ItemModelId");


--
-- TOC entry 2348 (class 2606 OID 45055)
-- Name: CharacterItemEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterItemEntity_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2352 (class 2606 OID 45424)
-- Name: FriendEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2338 (class 2606 OID 45048)
-- Name: PlayerEntity_PlayerName_key; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerName_key" UNIQUE ("PlayerName");


--
-- TOC entry 2340 (class 2606 OID 44963)
-- Name: PlayerEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2374 (class 2606 OID 78595)
-- Name: CharacterArmourAssetEntity_ItemId_CharacterId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_ItemId_CharacterId_key" UNIQUE ("ItemId", "CharacterId");


--
-- TOC entry 2376 (class 2606 OID 78567)
-- Name: CharacterArmourAssetEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_pkey" PRIMARY KEY ("AssetId");


--
-- TOC entry 2366 (class 2606 OID 78490)
-- Name: CharacterEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ItemCharacterEntity"
    ADD CONSTRAINT "CharacterEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2378 (class 2606 OID 78597)
-- Name: CharacterWeaponAssetEntity_ItemId_CharacterId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_ItemId_CharacterId_key" UNIQUE ("ItemId", "CharacterId");


--
-- TOC entry 2380 (class 2606 OID 78578)
-- Name: CharacterWeaponAssetEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_pkey" PRIMARY KEY ("AssetId");


--
-- TOC entry 2332 (class 2606 OID 78480)
-- Name: FractionEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "FractionEntity_pkey" PRIMARY KEY ("FractionId");


--
-- TOC entry 2368 (class 2606 OID 78511)
-- Name: ItemAmmoEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ItemAmmoEntity"
    ADD CONSTRAINT "ItemAmmoEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2364 (class 2606 OID 78478)
-- Name: ItemArmourBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ItemArmourEntity"
    ADD CONSTRAINT "ItemArmourBaseEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2362 (class 2606 OID 78468)
-- Name: ItemWeaponBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ItemWeaponEntity"
    ADD CONSTRAINT "ItemWeaponBaseEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2334 (class 2606 OID 44946)
-- Name: ModificatioBaseEntity_ModificationTypeId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_ModificationTypeId_key" UNIQUE ("ModificationTypeId");


--
-- TOC entry 2336 (class 2606 OID 44944)
-- Name: ModificatioBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_pkey" PRIMARY KEY ("ModificationId");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2316 (class 2606 OID 19251)
-- Name: Deposits_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Deposits"
    ADD CONSTRAINT "Deposits_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2318 (class 2606 OID 19253)
-- Name: Roles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2321 (class 2606 OID 19255)
-- Name: UserClaims_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "UserClaims_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2324 (class 2606 OID 19257)
-- Name: UserLogins_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "UserLogins_pkey" PRIMARY KEY ("UserId", "LoginProvider", "ProviderKey");


--
-- TOC entry 2328 (class 2606 OID 19259)
-- Name: UserRoles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "UserRoles_pkey" PRIMARY KEY ("UserId", "RoleId");


--
-- TOC entry 2330 (class 2606 OID 19261)
-- Name: Users_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2319 (class 1259 OID 19277)
-- Name: IX_UserClaims_UserId; Type: INDEX; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE INDEX "IX_UserClaims_UserId" ON "UserClaims" USING btree ("UserId");


--
-- TOC entry 2322 (class 1259 OID 19278)
-- Name: IX_UserLogins_UserId; Type: INDEX; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE INDEX "IX_UserLogins_UserId" ON "UserLogins" USING btree ("UserId");


--
-- TOC entry 2325 (class 1259 OID 19279)
-- Name: IX_UserRoles_RoleId; Type: INDEX; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE INDEX "IX_UserRoles_RoleId" ON "UserRoles" USING btree ("RoleId");


--
-- TOC entry 2326 (class 1259 OID 19280)
-- Name: IX_UserRoles_UserId; Type: INDEX; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE INDEX "IX_UserRoles_UserId" ON "UserRoles" USING btree ("UserId");


SET search_path = "Inventory", pg_catalog;

--
-- TOC entry 2402 (class 2606 OID 78537)
-- Name: ArmourEntity_ItemModelId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_ItemModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemArmourEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2401 (class 2606 OID 78542)
-- Name: ArmourEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2387 (class 2606 OID 45042)
-- Name: InvertoryItemBaseEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "InvertoryItemBaseEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2386 (class 2606 OID 78552)
-- Name: ItemWeaponEntity_ItemModelId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_ItemModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemWeaponEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2392 (class 2606 OID 45165)
-- Name: PlayerAssetEnity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PlayerAssetEnity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "Players"."CharacterEntity"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2391 (class 2606 OID 78557)
-- Name: PreSetEntity_HelmetId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PreSetEntity_HelmetId_fkey" FOREIGN KEY ("HelmetId") REFERENCES "ArmourEntity"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2390 (class 2606 OID 78665)
-- Name: PreSetEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PreSetEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "League", pg_catalog;

--
-- TOC entry 2396 (class 2606 OID 45454)
-- Name: LeagueMemberEntity_LeagueId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_LeagueId_fkey" FOREIGN KEY ("LeagueId") REFERENCES "LeagueEntity"("LeagueId") ON DELETE CASCADE;


--
-- TOC entry 2395 (class 2606 OID 45459)
-- Name: LeagueMemberEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2388 (class 2606 OID 78644)
-- Name: CharacterEntity_ModelId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterEntity_ModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemCharacterEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2389 (class 2606 OID 78639)
-- Name: CharacterEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2393 (class 2606 OID 45431)
-- Name: FriendEntity_FriendId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_FriendId_fkey" FOREIGN KEY ("FriendId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2394 (class 2606 OID 45426)
-- Name: FriendEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2385 (class 2606 OID 78651)
-- Name: PlayerEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES vrs."Users"("Id") ON DELETE CASCADE;


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2404 (class 2606 OID 78568)
-- Name: CharacterArmourAssetEntity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "ItemCharacterEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2403 (class 2606 OID 78589)
-- Name: CharacterArmourAssetEntity_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "ItemArmourEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2406 (class 2606 OID 78579)
-- Name: CharacterWeaponAssetEntity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "ItemCharacterEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2405 (class 2606 OID 78584)
-- Name: CharacterWeaponAssetEntity_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "ItemWeaponEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2400 (class 2606 OID 78611)
-- Name: ItemAmmoEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemAmmoEntity"
    ADD CONSTRAINT "ItemAmmoEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2398 (class 2606 OID 78616)
-- Name: ItemArmourEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemArmourEntity"
    ADD CONSTRAINT "ItemArmourEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2399 (class 2606 OID 78626)
-- Name: ItemCharacterEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemCharacterEntity"
    ADD CONSTRAINT "ItemCharacterEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2397 (class 2606 OID 78606)
-- Name: ItemWeaponEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ItemWeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2381 (class 2606 OID 19456)
-- Name: FK_UserClaims_Users_User_Id; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "FK_UserClaims_Users_User_Id" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2382 (class 2606 OID 19461)
-- Name: FK_UserLogins_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "FK_UserLogins_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2384 (class 2606 OID 19466)
-- Name: FK_UserRoles_Roles_RoleId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Roles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "Roles"("Id") ON DELETE CASCADE;


--
-- TOC entry 2383 (class 2606 OID 19471)
-- Name: FK_UserRoles_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2544 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-03-21 20:07:36

--
-- PostgreSQL database dump complete
--

