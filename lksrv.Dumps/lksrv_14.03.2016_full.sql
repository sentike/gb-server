--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.4.5
-- Started on 2016-03-14 03:22:46

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 19 (class 2615 OID 78532)
-- Name: Inventory; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Inventory";


ALTER SCHEMA "Inventory" OWNER TO postgres;

--
-- TOC entry 18 (class 2615 OID 45439)
-- Name: League; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "League";


ALTER SCHEMA "League" OWNER TO postgres;

--
-- TOC entry 17 (class 2615 OID 44951)
-- Name: Players; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Players";


ALTER SCHEMA "Players" OWNER TO postgres;

--
-- TOC entry 16 (class 2615 OID 44666)
-- Name: Store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Store";


ALTER SCHEMA "Store" OWNER TO postgres;

--
-- TOC entry 14 (class 2615 OID 18804)
-- Name: vrs; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA vrs;


ALTER SCHEMA vrs OWNER TO postgres;

SET search_path = "Inventory", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 285 (class 1259 OID 78522)
-- Name: ArmourEntity; Type: TABLE; Schema: Inventory; Owner: postgres; Tablespace: 
--

CREATE TABLE "ArmourEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "PrimaryAmmoAmount" smallint DEFAULT 0 NOT NULL,
    "SecondaryAmmoAmount" smallint DEFAULT 0 NOT NULL,
    "ItemModelId" smallint NOT NULL
);


ALTER TABLE "ArmourEntity" OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 45159)
-- Name: PreSetEntity; Type: TABLE; Schema: Inventory; Owner: postgres; Tablespace: 
--

CREATE TABLE "PreSetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CharacterId" uuid NOT NULL,
    "HelmetId" uuid,
    "MaskId" uuid,
    "ArmourId" uuid,
    "BackpackId" uuid,
    "GlovesId" uuid,
    "PantsId" uuid,
    "BootsId" uuid,
    "PrimaryWeaponId" uuid,
    "SecondaryWeaponId" uuid,
    "PrimaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "PrimaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "PreSetEntity" OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 44966)
-- Name: WeaponEntity; Type: TABLE; Schema: Inventory; Owner: postgres; Tablespace: 
--

CREATE TABLE "WeaponEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "ItemModelId" smallint NOT NULL
);


ALTER TABLE "WeaponEntity" OWNER TO postgres;

SET search_path = "League", pg_catalog;

--
-- TOC entry 279 (class 1259 OID 45440)
-- Name: LeagueEntity; Type: TABLE; Schema: League; Owner: postgres; Tablespace: 
--

CREATE TABLE "LeagueEntity" (
    "LeagueId" uuid NOT NULL,
    "Cash_Money" smallint NOT NULL,
    "Cash_Donate" smallint NOT NULL,
    "Info_Name" character varying(20) NOT NULL,
    "Info_Abbr" character varying(5) NOT NULL,
    "Info_FoundedDate" timestamp without time zone NOT NULL,
    "Info_AccessType" smallint NOT NULL,
    "Info_JoinPrice" smallint NOT NULL
);


ALTER TABLE "LeagueEntity" OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 45449)
-- Name: LeagueMemberEntity; Type: TABLE; Schema: League; Owner: postgres; Tablespace: 
--

CREATE TABLE "LeagueMemberEntity" (
    "LeagueId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Access" smallint NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "LeagueMemberEntity" OWNER TO postgres;

SET search_path = "Players", pg_catalog;

--
-- TOC entry 276 (class 1259 OID 45051)
-- Name: CharacterEntity; Type: TABLE; Schema: Players; Owner: postgres; Tablespace: 
--

CREATE TABLE "CharacterEntity" (
    "CharacterId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CharacterModelId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL
);


ALTER TABLE "CharacterEntity" OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 45420)
-- Name: FriendEntity; Type: TABLE; Schema: Players; Owner: postgres; Tablespace: 
--

CREATE TABLE "FriendEntity" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FriendId" uuid NOT NULL
);


ALTER TABLE "FriendEntity" OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 44952)
-- Name: PlayerEntity; Type: TABLE; Schema: Players; Owner: postgres; Tablespace: 
--

CREATE TABLE "PlayerEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserId" uuid NOT NULL,
    "PlayerName" character varying(24) NOT NULL,
    "Experience_Level" smallint DEFAULT 0 NOT NULL,
    "Experience_Experience" bigint DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "PremiumEndDate" timestamp without time zone,
    "LastActivitiDate" timestamp without time zone DEFAULT now() NOT NULL,
    "CharacterId" uuid NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "PlayerEntity" OWNER TO postgres;

SET search_path = "Store", pg_catalog;

--
-- TOC entry 286 (class 1259 OID 78562)
-- Name: CharacterArmourAssetEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "CharacterArmourAssetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ItemId" smallint NOT NULL,
    "CharacterId" smallint NOT NULL
);


ALTER TABLE "CharacterArmourAssetEntity" OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 78573)
-- Name: CharacterWeaponAssetEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "CharacterWeaponAssetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ItemId" smallint NOT NULL,
    "CharacterId" smallint NOT NULL
);


ALTER TABLE "CharacterWeaponAssetEntity" OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 44674)
-- Name: FractionEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "FractionEntity" (
    "FractionId" smallint NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" smallint DEFAULT 0 NOT NULL,
    "Discount_Ammo" smallint DEFAULT 0 NOT NULL,
    "Discount_Weapon" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FractionEntity" OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 78501)
-- Name: ItemAmmoEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ItemAmmoEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" smallint NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemAmmoEntity" OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 78469)
-- Name: ItemArmourEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ItemArmourEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" smallint NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemArmourEntity" OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 78481)
-- Name: ItemCharacterEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ItemCharacterEntity" (
    "ModelId" smallint NOT NULL,
    "FractionId" uuid NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" smallint NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemCharacterEntity" OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 78453)
-- Name: ItemWeaponEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ItemWeaponEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" smallint NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "ItemWeaponEntity" OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 44936)
-- Name: ModificationEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModificationEntity" (
    "ModificationId" uuid NOT NULL,
    "ModificationTypeId" smallint DEFAULT 0 NOT NULL,
    "Range_Minimum" smallint DEFAULT 2 NOT NULL,
    "Range_Maximum" smallint DEFAULT 10 NOT NULL,
    "Range_Chance" smallint DEFAULT 50 NOT NULL
);


ALTER TABLE "ModificationEntity" OWNER TO postgres;

SET search_path = vrs, pg_catalog;

--
-- TOC entry 260 (class 1259 OID 19096)
-- Name: Deposits; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "Deposits" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserId" uuid NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "Value" bigint DEFAULT 0 NOT NULL,
    "Currency" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "Deposits" OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 19102)
-- Name: Roles; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "Roles" (
    "Id" uuid NOT NULL,
    "Name" character varying(256) NOT NULL
);


ALTER TABLE "Roles" OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 19105)
-- Name: UserClaims; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "UserClaims" (
    "Id" integer NOT NULL,
    "ClaimType" character varying(256),
    "ClaimValue" character varying(256),
    "UserId" uuid NOT NULL
);


ALTER TABLE "UserClaims" OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 19111)
-- Name: UserClaims_Id_seq; Type: SEQUENCE; Schema: vrs; Owner: postgres
--

CREATE SEQUENCE "UserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "UserClaims_Id_seq" OWNER TO postgres;

--
-- TOC entry 2540 (class 0 OID 0)
-- Dependencies: 263
-- Name: UserClaims_Id_seq; Type: SEQUENCE OWNED BY; Schema: vrs; Owner: postgres
--

ALTER SEQUENCE "UserClaims_Id_seq" OWNED BY "UserClaims"."Id";


--
-- TOC entry 264 (class 1259 OID 19113)
-- Name: UserLogins; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "UserLogins" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "LoginProvider" character varying(128) NOT NULL,
    "ProviderKey" character varying(128) NOT NULL
);


ALTER TABLE "UserLogins" OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 19116)
-- Name: UserRoles; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "UserRoles" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RoleId" uuid NOT NULL
);


ALTER TABLE "UserRoles" OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 19119)
-- Name: Users; Type: TABLE; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE TABLE "Users" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserName" character varying(256) NOT NULL,
    "PasswordHash" character varying(256),
    "SecurityStamp" uuid,
    "Email" character varying(256) DEFAULT NULL::character varying,
    "EmailConfirmed" boolean DEFAULT false NOT NULL,
    "PhoneNumber" character varying(32),
    "PhoneNumberConfirmed" boolean DEFAULT false NOT NULL,
    "TwoFactorEnabled" boolean DEFAULT false NOT NULL,
    "LockoutEndDateUtc" timestamp without time zone,
    "LockoutEnabled" boolean DEFAULT false NOT NULL,
    "AccessFailedCount" smallint DEFAULT 0 NOT NULL,
    "PlayerEntityId" bigint,
    "Balance" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "Users" OWNER TO postgres;

--
-- TOC entry 2243 (class 2604 OID 19149)
-- Name: Id; Type: DEFAULT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"UserClaims_Id_seq"'::regclass);


SET search_path = "Inventory", pg_catalog;

--
-- TOC entry 2533 (class 0 OID 78522)
-- Dependencies: 285
-- Data for Name: ArmourEntity; Type: TABLE DATA; Schema: Inventory; Owner: postgres
--



--
-- TOC entry 2525 (class 0 OID 45159)
-- Dependencies: 277
-- Data for Name: PreSetEntity; Type: TABLE DATA; Schema: Inventory; Owner: postgres
--



--
-- TOC entry 2523 (class 0 OID 44966)
-- Dependencies: 275
-- Data for Name: WeaponEntity; Type: TABLE DATA; Schema: Inventory; Owner: postgres
--



SET search_path = "League", pg_catalog;

--
-- TOC entry 2527 (class 0 OID 45440)
-- Dependencies: 279
-- Data for Name: LeagueEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--

INSERT INTO "LeagueEntity" ("LeagueId", "Cash_Money", "Cash_Donate", "Info_Name", "Info_Abbr", "Info_FoundedDate", "Info_AccessType", "Info_JoinPrice") VALUES ('ff0ec814-a947-4dfb-9fb5-cc3d266d774b', 10, 100, 'VRS PRO', 'VRS', '2016-02-10 23:33:16.507', 1, 1000);


--
-- TOC entry 2528 (class 0 OID 45449)
-- Dependencies: 280
-- Data for Name: LeagueMemberEntity; Type: TABLE DATA; Schema: League; Owner: postgres
--



SET search_path = "Players", pg_catalog;

--
-- TOC entry 2524 (class 0 OID 45051)
-- Dependencies: 276
-- Data for Name: CharacterEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "CharacterEntity" ("CharacterId", "CharacterModelId", "PlayerId") VALUES ('618528d8-54db-44f0-8c3f-6980401376c2', '5f2e429e-98ed-44aa-930c-49a6acc19129', '729cacb8-f7f1-4e01-9a78-4523b3aec19c');


--
-- TOC entry 2526 (class 0 OID 45420)
-- Dependencies: 278
-- Data for Name: FriendEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2522 (class 0 OID 44952)
-- Dependencies: 274
-- Data for Name: PlayerEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "PlayerEntity" ("PlayerId", "UserId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivitiDate", "CharacterId", "LastActivityDate") VALUES ('729cacb8-f7f1-4e01-9a78-4523b3aec19c', '729cacb8-f7f1-4e01-9a78-4523b3aec19c', 'SeNT', 0, 0, 0, 0, 0, '2016-02-11 02:09:34.806', NULL, '2016-02-11 02:09:34.806', '618528d8-54db-44f0-8c3f-6980401376c2', '2016-02-11 02:09:34.806');


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2534 (class 0 OID 78562)
-- Dependencies: 286
-- Data for Name: CharacterArmourAssetEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2535 (class 0 OID 78573)
-- Dependencies: 287
-- Data for Name: CharacterWeaponAssetEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2520 (class 0 OID 44674)
-- Dependencies: 272
-- Data for Name: FractionEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (1, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (2, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (3, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (4, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (0, 0, 0, 0, 0, 0);


--
-- TOC entry 2532 (class 0 OID 78501)
-- Dependencies: 284
-- Data for Name: ItemAmmoEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2530 (class 0 OID 78469)
-- Dependencies: 282
-- Data for Name: ItemArmourEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2531 (class 0 OID 78481)
-- Dependencies: 283
-- Data for Name: ItemCharacterEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2529 (class 0 OID 78453)
-- Dependencies: 281
-- Data for Name: ItemWeaponEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2521 (class 0 OID 44936)
-- Dependencies: 273
-- Data for Name: ModificationEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



SET search_path = vrs, pg_catalog;

--
-- TOC entry 2513 (class 0 OID 19096)
-- Dependencies: 260
-- Data for Name: Deposits; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2514 (class 0 OID 19102)
-- Dependencies: 261
-- Data for Name: Roles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2515 (class 0 OID 19105)
-- Dependencies: 262
-- Data for Name: UserClaims; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "UserClaims" ("Id", "ClaimType", "ClaimValue", "UserId") VALUES (1, 'FaceBook_AccessToken', 'CAAXqNr5nDqUBADiDMnv0uXtEdFr502M7aDgmmVr6L9ubXfLVdqisGsqKl3PxyZA72Y2PLITKl6iVDkwZCN4ZBfFX3rnxp99myqNuU6OhgZCIqX97FYZBRZA7Fit9gIrXrvg0xERrVb8CfhTBazpGbCA0VUFgZB07ZC2XZCrc07ivJPMqZAQZCGNIjDhQeJCOuEWIx61m4JDJEfshgZDZD', 'a12a2b69-c2de-4d3a-aa2c-dfbca7536474');
INSERT INTO "UserClaims" ("Id", "ClaimType", "ClaimValue", "UserId") VALUES (2, 'Vkontakte_AccessToken', '89764d3d10772d3b54b9f2c09fabfbec3d0439e7574671d38db7846e40444da6f065b2e0726a8927f7cdb', '658bf8ca-e295-42da-a044-3cad2a5c833b');


--
-- TOC entry 2541 (class 0 OID 0)
-- Dependencies: 263
-- Name: UserClaims_Id_seq; Type: SEQUENCE SET; Schema: vrs; Owner: postgres
--

SELECT pg_catalog.setval('"UserClaims_Id_seq"', 2, true);


--
-- TOC entry 2517 (class 0 OID 19113)
-- Dependencies: 264
-- Data for Name: UserLogins; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "UserLogins" ("UserId", "LoginProvider", "ProviderKey") VALUES ('658bf8ca-e295-42da-a044-3cad2a5c833b', 'Vkontakte', '42251762');
INSERT INTO "UserLogins" ("UserId", "LoginProvider", "ProviderKey") VALUES ('a12a2b69-c2de-4d3a-aa2c-dfbca7536474', 'Facebook', '692071680934491');
INSERT INTO "UserLogins" ("UserId", "LoginProvider", "ProviderKey") VALUES ('42e5ba91-2123-46ff-9734-f0b17710e557', 'Facebook', '693312480810411');


--
-- TOC entry 2518 (class 0 OID 19116)
-- Dependencies: 265
-- Data for Name: UserRoles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2519 (class 0 OID 19119)
-- Dependencies: 266
-- Data for Name: Users; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('729cacb8-f7f1-4e01-9a78-4523b3aec19c', 'sentike161rus@gmail.com1', 'AOS4yvgWzxhKXY9tYPmWVUOsAFdZvGiMvR+iVPL6VI8gbM+i78264VkIITWggjXKKg==', '60e2f564-20e1-477a-ae8f-6657acb2da90', 'sentike161rus@gmail.com1', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('8394dad0-4a0d-4f05-af96-bff605eebdb2', 'hgfhgfhf@hg.65', 'AMPUb9SZMtkrMCz+XItPTOdg4BTu8k5B67VAuu/Fh1oI37MGxJzelABdXN5BjLCZqQ==', '15a6833b-cd58-40b9-9f84-7a75f1cf3223', 'hgfhgfhf@hg.65', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('a0bc0158-4e9b-47fe-81fb-58564a89834e', 'gfgdfg3@gfd.fg', 'AKRrYHiU6kjafaIEo6vhMaLi6ejSzILkY0SWNNUB8uZ+JyPQLw2OHZK/ivwDj9WaqA==', '3e98a8d5-e82f-4824-b657-81afcf187ec0', 'gfgdfg3@gfd.fg', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('1d4e7376-4252-4515-bfae-418a88388ff7', 'sentike161rus@gmail.com', 'AJbVa4J1+hwsiXqsw1tCkDVoo+7SMcJJJeswqs6AC9xontpfvq3++fEk7HeBQVjFcw==', '8d0d288d-9ef5-4b31-8300-bcb13a5149ec', 'sentike161rus@gmail.com', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('65ac424b-b334-4d3e-8d84-c6c972cd7dc1', 's@s.ru', 'ALyJ8ZEDSDZrCXYmHgdDbI4QuMckdCk6otUaxNQwld2cX9slQkIzN0CW2GaFbj0KFg==', '17bd0e93-a040-4bbf-9cf1-1427a25b76b2', 's@s.ru', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('a12a2b69-c2de-4d3a-aa2c-dfbca7536474', 'sentike161rus000@gmail.com', 'AFAYLeslNkGekMvwSLg/bbvpCZUG8PcreJtnd2cS3JH/BtCs/ZIjKhmtM4Z32oIdvQ==', '78dd151f-a41f-407d-b1d8-334ee243eaa0', 'sentike161rus000@gmail.com', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('658bf8ca-e295-42da-a044-3cad2a5c833b', 'gggsen354235244rus@gmail.com', 'AECgB1yhpb2weGXWZNcX0LK3WK+V2dcP7bN4Oks9euD9m/uYfDK9DZQ8VZOiws/DIQ==', '6fb94831-c74b-46fa-8be5-96b2d041919a', 'gggsen354235244rus@gmail.com', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('42e5ba91-2123-46ff-9734-f0b17710e557', 'vrs.loka@gmail.com', 'AHsLgSlSN2oazx5yAsbYrO+jnWiEeqbgugRXoTLAHNsH4luO/d3epDj4DWvt6aJa4g==', 'c641ecf8-85e0-44f1-b1dd-33f8578c3e08', 'vrs.loka@gmail.com', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('00000000-0000-0000-0000-000000000000', 'sentike@gfd.ru', 'AIpZu/DI2z0Nq28CLX12Rm/EJw9IDxse9jsS/NotXxTPo15mId/Laiu0CseH50hF9w==', 'a5d4914d-4ba8-42c2-8c76-33a1096cdaad', 'sentike@gfd.ru', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('6975a439-bfd8-44ad-8ff6-024babed0c3b', 'sentima@gfd.fd3', 'ANFQV3Pi+rxy99kbi6BvcrT0rxOXlcF1mk0Ou7EAm059wxsXTmR/LDSU7YpLH1Tang==', '86f71c29-9002-4334-8a0a-4c08adb5932a', 'sentima@gfd.fd3', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('3f84d1c7-8f8e-4d52-a2c9-d0ce507484a8', 'sentim3a@gfd.fd3', 'AEluQDGignkG5pcKiGkY5ij0n5hZMLuDz6B2BZScMWFgGfg0zPBYDtjrFYGbiPHhUA==', '90403541-b629-434b-9d18-6d5c4d6074a0', 'sentim3a@gfd.fd3', false, NULL, false, false, NULL, true, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('2ef28325-2625-4fe7-8db3-9728b0349788', 'SeNTike161h643@gfd12.ru', 'ADqm3qNadk2KzRxo7Tij5eMB9kjy+UY1LA+lUckhVCUUMx2bvXtvCbQtW4eGQyapAg==', 'cec7a974-ae40-4caa-a274-313ea530094c', 'SeNTike161h643@gfd12.ru', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('3ed69cb3-5c6f-4b8b-817f-21ad7a934247', 'sentik123456@gmail.com', 'AIbNXeiv8MD1E56WyaTCM3HD8tOae6JhAL9KkzagBiL0l71D3VZdU7DeRGHELJvF5w==', '4eaea342-b461-4924-887d-47d9c9668d04', 'sentik123456@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('f81a5e04-657a-4249-ba44-e26fe02ee342', 'SeNTike', 'AARHR8MbpsjCjbMM3D5eMjToG7BShUjcXSxwixdCE9U9GlwDqSEK47NlJzBEGfywLg==', '8a24d2a0-2181-4df7-8e53-46f82edc897e', 'SeNTike', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('a7754815-c539-4d5c-aa06-d02273fe736b', 'hgfhgfh4hgfh', 'ACJM+c8z362iii/1/jGPC4qV/HeZHig07MdOufYtW3YtVy3qfP/O7cC/FacSoDX3ng==', '4079576d-3179-4f76-b57b-eb1a32afae9a', 'hgfhgfh4hgfh', false, NULL, false, false, NULL, false, 0, NULL, 0);
INSERT INTO "Users" ("Id", "UserName", "PasswordHash", "SecurityStamp", "Email", "EmailConfirmed", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "PlayerEntityId", "Balance") VALUES ('0e83d1ac-7d24-488b-b959-04b25c4eee7a', 'sentike123@gmail.com', 'AHOzhirnWESeGV2lm2iTJaBAyLf8gMs0OrR+p9NWW+itCUuR9i/de8VI9SzmQ13dkA==', 'da710bdb-e4a4-4f3b-86f9-d63b16f760c3', 'sentike123@gmail.com', false, NULL, false, false, NULL, false, 0, NULL, 0);


SET search_path = "Inventory", pg_catalog;

--
-- TOC entry 2374 (class 2606 OID 78536)
-- Name: ArmourEntity_PlayerId_ItemModelId_key; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_PlayerId_ItemModelId_key" UNIQUE ("PlayerId", "ItemModelId");


--
-- TOC entry 2376 (class 2606 OID 78534)
-- Name: ArmourEntity_pkey; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2344 (class 2606 OID 78551)
-- Name: ItemWeaponEntity_PlayerId_ItemModelId_key; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_PlayerId_ItemModelId_key" UNIQUE ("PlayerId", "ItemModelId");


--
-- TOC entry 2346 (class 2606 OID 78549)
-- Name: ItemWeaponEntity_pkey; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2354 (class 2606 OID 45164)
-- Name: PlayerAssetEnity_pkey; Type: CONSTRAINT; Schema: Inventory; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PlayerAssetEnity_pkey" PRIMARY KEY ("AssetId");


SET search_path = "League", pg_catalog;

--
-- TOC entry 2358 (class 2606 OID 45448)
-- Name: LeagueEntity_Info_Abbr_key; Type: CONSTRAINT; Schema: League; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Abbr_key" UNIQUE ("Info_Abbr");


--
-- TOC entry 2360 (class 2606 OID 45446)
-- Name: LeagueEntity_Info_Name_key; Type: CONSTRAINT; Schema: League; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Name_key" UNIQUE ("Info_Name");


--
-- TOC entry 2362 (class 2606 OID 45444)
-- Name: LeagueEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_pkey" PRIMARY KEY ("LeagueId");


--
-- TOC entry 2364 (class 2606 OID 45470)
-- Name: LeagueMemberEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2348 (class 2606 OID 45221)
-- Name: CharacterItemEntity_CharacterId_PlayerId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterItemEntity_CharacterId_PlayerId_key" UNIQUE ("CharacterId", "PlayerId");


--
-- TOC entry 2350 (class 2606 OID 45094)
-- Name: CharacterItemEntity_CharacterModelId_PlayerId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterItemEntity_CharacterModelId_PlayerId_key" UNIQUE ("CharacterModelId", "PlayerId");


--
-- TOC entry 2352 (class 2606 OID 45055)
-- Name: CharacterItemEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterEntity"
    ADD CONSTRAINT "CharacterItemEntity_pkey" PRIMARY KEY ("CharacterId");


--
-- TOC entry 2356 (class 2606 OID 45424)
-- Name: FriendEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2336 (class 2606 OID 45050)
-- Name: PlayerEntity_CharacterId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_CharacterId_key" UNIQUE ("CharacterId");


--
-- TOC entry 2338 (class 2606 OID 45048)
-- Name: PlayerEntity_PlayerName_key; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerName_key" UNIQUE ("PlayerName");


--
-- TOC entry 2340 (class 2606 OID 44965)
-- Name: PlayerEntity_UserId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_UserId_key" UNIQUE ("UserId");


--
-- TOC entry 2342 (class 2606 OID 44963)
-- Name: PlayerEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2378 (class 2606 OID 78595)
-- Name: CharacterArmourAssetEntity_ItemId_CharacterId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_ItemId_CharacterId_key" UNIQUE ("ItemId", "CharacterId");


--
-- TOC entry 2380 (class 2606 OID 78567)
-- Name: CharacterArmourAssetEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_pkey" PRIMARY KEY ("AssetId");


--
-- TOC entry 2370 (class 2606 OID 78490)
-- Name: CharacterEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ItemCharacterEntity"
    ADD CONSTRAINT "CharacterEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2382 (class 2606 OID 78597)
-- Name: CharacterWeaponAssetEntity_ItemId_CharacterId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_ItemId_CharacterId_key" UNIQUE ("ItemId", "CharacterId");


--
-- TOC entry 2384 (class 2606 OID 78578)
-- Name: CharacterWeaponAssetEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_pkey" PRIMARY KEY ("AssetId");


--
-- TOC entry 2330 (class 2606 OID 78480)
-- Name: FractionEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "FractionEntity_pkey" PRIMARY KEY ("FractionId");


--
-- TOC entry 2372 (class 2606 OID 78511)
-- Name: ItemAmmoEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ItemAmmoEntity"
    ADD CONSTRAINT "ItemAmmoEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2368 (class 2606 OID 78478)
-- Name: ItemArmourBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ItemArmourEntity"
    ADD CONSTRAINT "ItemArmourBaseEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2366 (class 2606 OID 78468)
-- Name: ItemWeaponBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ItemWeaponEntity"
    ADD CONSTRAINT "ItemWeaponBaseEntity_pkey" PRIMARY KEY ("ModelId");


--
-- TOC entry 2332 (class 2606 OID 44946)
-- Name: ModificatioBaseEntity_ModificationTypeId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_ModificationTypeId_key" UNIQUE ("ModificationTypeId");


--
-- TOC entry 2334 (class 2606 OID 44944)
-- Name: ModificatioBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_pkey" PRIMARY KEY ("ModificationId");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2314 (class 2606 OID 19251)
-- Name: Deposits_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Deposits"
    ADD CONSTRAINT "Deposits_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2316 (class 2606 OID 19253)
-- Name: Roles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2319 (class 2606 OID 19255)
-- Name: UserClaims_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "UserClaims_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2322 (class 2606 OID 19257)
-- Name: UserLogins_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "UserLogins_pkey" PRIMARY KEY ("UserId", "LoginProvider", "ProviderKey");


--
-- TOC entry 2326 (class 2606 OID 19259)
-- Name: UserRoles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "UserRoles_pkey" PRIMARY KEY ("UserId", "RoleId");


--
-- TOC entry 2328 (class 2606 OID 19261)
-- Name: Users_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2317 (class 1259 OID 19277)
-- Name: IX_UserClaims_UserId; Type: INDEX; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE INDEX "IX_UserClaims_UserId" ON "UserClaims" USING btree ("UserId");


--
-- TOC entry 2320 (class 1259 OID 19278)
-- Name: IX_UserLogins_UserId; Type: INDEX; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE INDEX "IX_UserLogins_UserId" ON "UserLogins" USING btree ("UserId");


--
-- TOC entry 2323 (class 1259 OID 19279)
-- Name: IX_UserRoles_RoleId; Type: INDEX; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE INDEX "IX_UserRoles_RoleId" ON "UserRoles" USING btree ("RoleId");


--
-- TOC entry 2324 (class 1259 OID 19280)
-- Name: IX_UserRoles_UserId; Type: INDEX; Schema: vrs; Owner: postgres; Tablespace: 
--

CREATE INDEX "IX_UserRoles_UserId" ON "UserRoles" USING btree ("UserId");


SET search_path = "Inventory", pg_catalog;

--
-- TOC entry 2399 (class 2606 OID 78537)
-- Name: ArmourEntity_ItemModelId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_ItemModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemArmourEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2398 (class 2606 OID 78542)
-- Name: ArmourEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "ArmourEntity"
    ADD CONSTRAINT "ArmourEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2391 (class 2606 OID 45042)
-- Name: InvertoryItemBaseEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "InvertoryItemBaseEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2390 (class 2606 OID 78552)
-- Name: ItemWeaponEntity_ItemModelId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "WeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_ItemModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemWeaponEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2392 (class 2606 OID 45165)
-- Name: PlayerAssetEnity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PlayerAssetEnity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "Players"."CharacterEntity"("CharacterId") ON DELETE CASCADE;


--
-- TOC entry 2393 (class 2606 OID 78557)
-- Name: PreSetEntity_HelmetId_fkey; Type: FK CONSTRAINT; Schema: Inventory; Owner: postgres
--

ALTER TABLE ONLY "PreSetEntity"
    ADD CONSTRAINT "PreSetEntity_HelmetId_fkey" FOREIGN KEY ("HelmetId") REFERENCES "ArmourEntity"("ItemId") ON DELETE CASCADE;


SET search_path = "League", pg_catalog;

--
-- TOC entry 2397 (class 2606 OID 45454)
-- Name: LeagueMemberEntity_LeagueId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_LeagueId_fkey" FOREIGN KEY ("LeagueId") REFERENCES "LeagueEntity"("LeagueId") ON DELETE CASCADE;


--
-- TOC entry 2396 (class 2606 OID 45459)
-- Name: LeagueMemberEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2394 (class 2606 OID 45431)
-- Name: FriendEntity_FriendId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_FriendId_fkey" FOREIGN KEY ("FriendId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2395 (class 2606 OID 45426)
-- Name: FriendEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2389 (class 2606 OID 45095)
-- Name: PlayerEntity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "CharacterEntity"("CharacterId") ON DELETE CASCADE;


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2401 (class 2606 OID 78568)
-- Name: CharacterArmourAssetEntity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "ItemCharacterEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2400 (class 2606 OID 78589)
-- Name: CharacterArmourAssetEntity_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "ItemArmourEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2403 (class 2606 OID 78579)
-- Name: CharacterWeaponAssetEntity_CharacterId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "ItemCharacterEntity"("ModelId") ON DELETE CASCADE;


--
-- TOC entry 2402 (class 2606 OID 78584)
-- Name: CharacterWeaponAssetEntity_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "ItemWeaponEntity"("ModelId") ON DELETE CASCADE;


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2385 (class 2606 OID 19456)
-- Name: FK_UserClaims_Users_User_Id; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "FK_UserClaims_Users_User_Id" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2386 (class 2606 OID 19461)
-- Name: FK_UserLogins_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "FK_UserLogins_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2388 (class 2606 OID 19466)
-- Name: FK_UserRoles_Roles_RoleId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Roles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "Roles"("Id") ON DELETE CASCADE;


--
-- TOC entry 2387 (class 2606 OID 19471)
-- Name: FK_UserRoles_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


-- Completed on 2016-03-14 03:22:46

--
-- PostgreSQL database dump complete
--

