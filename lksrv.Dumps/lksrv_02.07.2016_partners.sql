--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-07-02 05:43:22

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 19 (class 2615 OID 214686)
-- Name: Promo; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Promo";


ALTER SCHEMA "Promo" OWNER TO postgres;

SET search_path = "Promo", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 251 (class 1259 OID 214698)
-- Name: Codes; Type: TABLE; Schema: Promo; Owner: postgres
--

CREATE TABLE "Codes" (
    "PromoId" uuid NOT NULL,
    "PartnerId" uuid NOT NULL,
    "Name" character varying(64) NOT NULL,
    "Validity" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "Codes" OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 214693)
-- Name: Partners; Type: TABLE; Schema: Promo; Owner: postgres
--

CREATE TABLE "Partners" (
    "PartnerId" uuid NOT NULL,
    "Name" character varying(128) NOT NULL,
    "Login" character varying(32) NOT NULL,
    "Password" character varying(32) NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "AllowDuplicate" boolean DEFAULT false NOT NULL
);


ALTER TABLE "Partners" OWNER TO postgres;

--
-- TOC entry 2215 (class 2606 OID 214702)
-- Name: Codes_pkey; Type: CONSTRAINT; Schema: Promo; Owner: postgres
--

ALTER TABLE ONLY "Codes"
    ADD CONSTRAINT "Codes_pkey" PRIMARY KEY ("PromoId");


--
-- TOC entry 2213 (class 2606 OID 214697)
-- Name: Partners_pkey; Type: CONSTRAINT; Schema: Promo; Owner: postgres
--

ALTER TABLE ONLY "Partners"
    ADD CONSTRAINT "Partners_pkey" PRIMARY KEY ("PartnerId");


--
-- TOC entry 2216 (class 2606 OID 214710)
-- Name: Codes_PartnerId_fkey; Type: FK CONSTRAINT; Schema: Promo; Owner: postgres
--

ALTER TABLE ONLY "Codes"
    ADD CONSTRAINT "Codes_PartnerId_fkey" FOREIGN KEY ("PartnerId") REFERENCES "Partners"("PartnerId") ON DELETE CASCADE;


-- Completed on 2016-07-02 05:43:22

--
-- PostgreSQL database dump complete
--

