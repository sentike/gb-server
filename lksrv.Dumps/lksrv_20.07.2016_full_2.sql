--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-07-20 12:27:19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- TOC entry 2878 (class 1262 OID 12373)
-- Name: postgres; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


ALTER DATABASE postgres OWNER TO postgres;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2879 (class 1262 OID 12373)
-- Dependencies: 2878
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- TOC entry 11 (class 2615 OID 226834)
-- Name: achievements; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA achievements;


ALTER SCHEMA achievements OWNER TO postgres;

--
-- TOC entry 20 (class 2615 OID 227785)
-- Name: dbo; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA dbo;


ALTER SCHEMA dbo OWNER TO postgres;

--
-- TOC entry 12 (class 2615 OID 226835)
-- Name: gamemode; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA gamemode;


ALTER SCHEMA gamemode OWNER TO postgres;

--
-- TOC entry 13 (class 2615 OID 226836)
-- Name: league; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA league;


ALTER SCHEMA league OWNER TO postgres;

--
-- TOC entry 14 (class 2615 OID 226837)
-- Name: matches; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA matches;


ALTER SCHEMA matches OWNER TO postgres;

--
-- TOC entry 15 (class 2615 OID 226838)
-- Name: players; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA players;


ALTER SCHEMA players OWNER TO postgres;

--
-- TOC entry 16 (class 2615 OID 226839)
-- Name: promo; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA promo;


ALTER SCHEMA promo OWNER TO postgres;

--
-- TOC entry 18 (class 2615 OID 226841)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 17 (class 2615 OID 226840)
-- Name: store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA store;


ALTER SCHEMA store OWNER TO postgres;

--
-- TOC entry 19 (class 2615 OID 226842)
-- Name: vrs; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA vrs;


ALTER SCHEMA vrs OWNER TO postgres;

--
-- TOC entry 2 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2880 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 1 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 2881 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 3 (class 3079 OID 226843)
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- TOC entry 2882 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = achievements, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 226923)
-- Name: AchievementContainer; Type: TABLE; Schema: achievements; Owner: postgres
--

CREATE TABLE "AchievementContainer" (
    "AchievementTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "AchievementContainer" OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 226931)
-- Name: AchievementInstance; Type: TABLE; Schema: achievements; Owner: postgres
--

CREATE TABLE "AchievementInstance" (
    "AchievementBonusId" integer NOT NULL,
    "AchievementTypeId" integer DEFAULT 0 NOT NULL,
    "Amount" integer DEFAULT 0 NOT NULL,
    "BonusPackId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "AchievementInstance" OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 226929)
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE; Schema: achievements; Owner: postgres
--

CREATE SEQUENCE "AchievementInstance_AchievementBonusId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AchievementInstance_AchievementBonusId_seq" OWNER TO postgres;

--
-- TOC entry 2883 (class 0 OID 0)
-- Dependencies: 197
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE OWNED BY; Schema: achievements; Owner: postgres
--

ALTER SEQUENCE "AchievementInstance_AchievementBonusId_seq" OWNED BY "AchievementInstance"."AchievementBonusId";


SET search_path = dbo, pg_catalog;

--
-- TOC entry 246 (class 1259 OID 227786)
-- Name: __MigrationHistory; Type: TABLE; Schema: dbo; Owner: postgres
--

CREATE TABLE "__MigrationHistory" (
    "MigrationId" character varying(150) DEFAULT ''::character varying NOT NULL,
    "ContextKey" character varying(300) DEFAULT ''::character varying NOT NULL,
    "Model" bytea DEFAULT '\x'::bytea NOT NULL,
    "ProductVersion" character varying(32) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE "__MigrationHistory" OWNER TO postgres;

SET search_path = gamemode, pg_catalog;

--
-- TOC entry 227 (class 1259 OID 227300)
-- Name: AbstractGameModeEntity; Type: TABLE; Schema: gamemode; Owner: postgres
--

CREATE TABLE "AbstractGameModeEntity" (
    "GameModeId" integer DEFAULT 0 NOT NULL,
    "Level_Min" integer DEFAULT 0 NOT NULL,
    "Level_Max" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "AbstractGameModeEntity" OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 227288)
-- Name: GameMapEntity; Type: TABLE; Schema: gamemode; Owner: postgres
--

CREATE TABLE "GameMapEntity" (
    "InstanceId" integer NOT NULL,
    "GameModeId" integer DEFAULT 0 NOT NULL,
    "MapId" integer DEFAULT 0 NOT NULL,
    "Members_Min" integer DEFAULT 0 NOT NULL,
    "Members_Max" integer DEFAULT 0 NOT NULL,
    "RoundTimeInSeconds" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "GameMapEntity" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 227286)
-- Name: GameMapEntity_InstanceId_seq; Type: SEQUENCE; Schema: gamemode; Owner: postgres
--

CREATE SEQUENCE "GameMapEntity_InstanceId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "GameMapEntity_InstanceId_seq" OWNER TO postgres;

--
-- TOC entry 2884 (class 0 OID 0)
-- Dependencies: 225
-- Name: GameMapEntity_InstanceId_seq; Type: SEQUENCE OWNED BY; Schema: gamemode; Owner: postgres
--

ALTER SEQUENCE "GameMapEntity_InstanceId_seq" OWNED BY "GameMapEntity"."InstanceId";


--
-- TOC entry 232 (class 1259 OID 227350)
-- Name: TeamGameModeEntity; Type: TABLE; Schema: gamemode; Owner: postgres
--

CREATE TABLE "TeamGameModeEntity" (
    "GameModeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "TeamGameModeEntity" OWNER TO postgres;

SET search_path = league, pg_catalog;

--
-- TOC entry 211 (class 1259 OID 227083)
-- Name: LeagueEntity; Type: TABLE; Schema: league; Owner: postgres
--

CREATE TABLE "LeagueEntity" (
    "LeagueId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Info_Name" text,
    "Info_Abbr" text,
    "Info_FoundedDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "Info_AccessType" integer DEFAULT 0 NOT NULL,
    "Info_JoinPrice" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "LeagueEntity" OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 227072)
-- Name: LeagueMemberEntity; Type: TABLE; Schema: league; Owner: postgres
--

CREATE TABLE "LeagueMemberEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "LeagueId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "Access" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "LeagueMemberEntity" OWNER TO postgres;

SET search_path = matches, pg_catalog;

--
-- TOC entry 214 (class 1259 OID 227149)
-- Name: MatchEntity; Type: TABLE; Schema: matches; Owner: postgres
--

CREATE TABLE "MatchEntity" (
    "MatchId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "NodeId" uuid,
    "GameModeTypeId" integer DEFAULT 0 NOT NULL,
    "GameMapTypeId" integer DEFAULT 0 NOT NULL,
    "Created" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "Started" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "Finished" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "NumberOfBots" integer DEFAULT 0 NOT NULL,
    "WinnerTeamId" uuid,
    "RoundTimeInSeconds" integer DEFAULT 0 NOT NULL,
    "State" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "MatchEntity" OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 227164)
-- Name: MatchMember; Type: TABLE; Schema: matches; Owner: postgres
--

CREATE TABLE "MatchMember" (
    "MemberId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "MatchId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "PlayerId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "TeamId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Award_Money" integer DEFAULT 0 NOT NULL,
    "Award_Experience" integer DEFAULT 0 NOT NULL,
    "Award_Reputation" integer DEFAULT 0 NOT NULL,
    "Award_FractionId" smallint DEFAULT 0 NOT NULL,
    "Statistic_Kills" bigint DEFAULT 0 NOT NULL,
    "Statistic_Deads" bigint DEFAULT 0 NOT NULL,
    "Statistic_Assists" bigint DEFAULT 0 NOT NULL,
    "Statistic_TeamKills" bigint DEFAULT 0 NOT NULL,
    "Statistic_Shots" bigint DEFAULT 0 NOT NULL,
    "Statistic_Hits" bigint DEFAULT 0 NOT NULL,
    "Statistic_IncomingDamage" bigint DEFAULT 0 NOT NULL,
    "Statistic_OutgoingDamage" bigint DEFAULT 0 NOT NULL,
    "Squad" integer DEFAULT 0 NOT NULL,
    "State" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "MatchMember" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 227200)
-- Name: MatchTeam; Type: TABLE; Schema: matches; Owner: postgres
--

CREATE TABLE "MatchTeam" (
    "TeamId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "MatchId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL
);


ALTER TABLE "MatchTeam" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 227190)
-- Name: MemberAchievements; Type: TABLE; Schema: matches; Owner: postgres
--

CREATE TABLE "MemberAchievements" (
    "AchievementId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "AchievementTypeId" integer DEFAULT 0 NOT NULL,
    "AchievementCount" integer DEFAULT 0 NOT NULL,
    "MemberId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL
);


ALTER TABLE "MemberAchievements" OWNER TO postgres;

SET search_path = players, pg_catalog;

--
-- TOC entry 200 (class 1259 OID 226953)
-- Name: FriendEntity; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "FriendEntity" (
    "MemberId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "FriendId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "State" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "FriendEntity" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 227035)
-- Name: InstanceOfInstalledAddon; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "InstanceOfInstalledAddon" (
    "ContainerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "AddonId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "AddonSlot" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "InstanceOfInstalledAddon" OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 227385)
-- Name: InstanceOfItemAddon; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "InstanceOfItemAddon" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "InstanceOfItemAddon" OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 227378)
-- Name: InstanceOfItemSkin; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "InstanceOfItemSkin" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "InstanceOfItemSkin" OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 227371)
-- Name: InstanceOfPlayerArmour; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerArmour" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "InstanceOfPlayerArmour" OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 227357)
-- Name: InstanceOfPlayerCharacter; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerCharacter" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "InstanceOfPlayerCharacter" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 226964)
-- Name: InstanceOfPlayerItem; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerItem" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL,
    "PlayerId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_1_value double precision DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_2_value double precision DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_3_value double precision DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_4_value double precision DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_5_value double precision DEFAULT 0 NOT NULL
);


ALTER TABLE "InstanceOfPlayerItem" OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 227392)
-- Name: InstanceOfPlayerKit; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerKit" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "InstanceOfPlayerKit" OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 227364)
-- Name: InstanceOfPlayerWeapon; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerWeapon" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "InstanceOfPlayerWeapon" OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 226985)
-- Name: InstanceOfTargetItem; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "InstanceOfTargetItem" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL,
    "PlayerId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "TargetPlayerItemId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL
);


ALTER TABLE "InstanceOfTargetItem" OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 226911)
-- Name: PlayerAchievement; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "PlayerAchievement" (
    "PlayerAchievementId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Amount" integer DEFAULT 0 NOT NULL,
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "AchievementTypeId" integer DEFAULT 0 NOT NULL,
    "LastAchievementInstanceId" integer
);


ALTER TABLE "PlayerAchievement" OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 226880)
-- Name: PlayerEntity; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "PlayerEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerName" character varying(64),
    "PremiumEndDate" timestamp without time zone,
    "Experience_Level" integer DEFAULT 0 NOT NULL,
    "Experience_Experience" integer DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "WeekBonus_JoinDate" timestamp without time zone DEFAULT now() NOT NULL,
    "WeekBonus_NotifyDate" timestamp without time zone DEFAULT now() NOT NULL,
    "WeekBonus_JoinCount" smallint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL,
    "CurrentFractionId" uuid,
    "FirstPartnerId" uuid,
    "Statistic_Kills" bigint DEFAULT 0 NOT NULL,
    "Statistic_Deads" bigint DEFAULT 0 NOT NULL,
    "Statistic_Assists" bigint DEFAULT 0 NOT NULL,
    "Statistic_TeamKills" bigint DEFAULT 0 NOT NULL,
    "Statistic_Shots" bigint DEFAULT 0 NOT NULL,
    "Statistic_Hits" bigint DEFAULT 0 NOT NULL,
    "Statistic_IncomingDamage" bigint DEFAULT 0 NOT NULL,
    "Statistic_OutgoingDamage" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "PlayerEntity" OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 227046)
-- Name: PlayerProfileEntity; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "PlayerProfileEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text,
    "IsEnabled" boolean DEFAULT false NOT NULL,
    "IsUnlocked" boolean DEFAULT false NOT NULL,
    "PlayerId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "CharacterId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL
);


ALTER TABLE "PlayerProfileEntity" OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 227333)
-- Name: PlayerPromoCode; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "PlayerPromoCode" (
    "PlayerCodeId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "CodeId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "ActivationDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE "PlayerPromoCode" OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 226941)
-- Name: PlayerReputationEntity; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "PlayerReputationEntity" (
    "ReputationId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "CurrentRank" smallint DEFAULT 0 NOT NULL,
    "LastRank" smallint DEFAULT 0 NOT NULL,
    "Reputation" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "PlayerReputationEntity" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 227061)
-- Name: ProfileItemEntity; Type: TABLE; Schema: players; Owner: postgres
--

CREATE TABLE "ProfileItemEntity" (
    "ProfileItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ProfileId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "ItemId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "SlotId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "ProfileItemEntity" OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- TOC entry 219 (class 1259 OID 227225)
-- Name: ClusterInstanceEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "ClusterInstanceEntity" (
    "ClusterId" uuid DEFAULT gen_random_uuid() NOT NULL,
    "MachineName" character varying(64),
    "MachineAddress" character varying(64),
    "RegionTypeId" smallint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "State" integer DEFAULT 0 NOT NULL,
    "LimitActiveNodes" integer DEFAULT 0 NOT NULL,
    "ShoutDownReaspon" integer DEFAULT 0 NOT NULL,
    "UpdateStatus" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "ClusterInstanceEntity" OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 227208)
-- Name: ClusterNodeEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "ClusterNodeEntity" (
    "NodeId" uuid DEFAULT gen_random_uuid() NOT NULL,
    "ClusterId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "Address_Host" text,
    "Address_Port" integer DEFAULT 0 NOT NULL,
    "ShutDownDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "ActiveMatchId" uuid,
    "State" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "ClusterNodeEntity" OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 227308)
-- Name: GameSessionEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "GameSessionEntity" (
    "SessionId" uuid DEFAULT gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "LastMessageAccess" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE "GameSessionEntity" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 227318)
-- Name: GlobalChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "GlobalChatMessage" (
    "MessageId" uuid DEFAULT gen_random_uuid() NOT NULL,
    "SenderId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "EditorId" uuid,
    "Status" integer DEFAULT 0 NOT NULL,
    "LanguageId" integer DEFAULT 0 NOT NULL,
    "Message" text,
    "Date" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE "GlobalChatMessage" OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 227097)
-- Name: PrivateChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "PrivateChatMessage" (
    "MessageId" uuid DEFAULT gen_random_uuid() NOT NULL,
    "MessageDataId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "PlayerId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL
);


ALTER TABLE "PrivateChatMessage" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 227107)
-- Name: PrivateChatMessageData; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "PrivateChatMessageData" (
    "MessageId" uuid DEFAULT gen_random_uuid() NOT NULL,
    "Message" text,
    "Date" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "IsReaded" boolean DEFAULT false NOT NULL,
    "SenderId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "ReceiverId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL
);


ALTER TABLE "PrivateChatMessageData" OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 227944)
-- Name: QueueEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "QueueEntity" (
    "QueueId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "MatchId" uuid,
    "GameModeTypeId" integer DEFAULT 0 NOT NULL,
    "RegionTypeId" smallint DEFAULT 0 NOT NULL,
    "CheckDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "Level_Min" integer DEFAULT 0 NOT NULL,
    "Level_Max" integer DEFAULT 0 NOT NULL,
    "Ready" boolean DEFAULT false NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE "QueueEntity" OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 227956)
-- Name: QueueMember; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "QueueMember" (
    "QueueId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "PlayerId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    "Ready" boolean DEFAULT false NOT NULL,
    "MatchMemberId" uuid
);


ALTER TABLE "QueueMember" OWNER TO postgres;

SET search_path = store, pg_catalog;

--
-- TOC entry 203 (class 1259 OID 226998)
-- Name: AbstractItemInstance; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "AbstractItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL,
    "IsAvalible" boolean DEFAULT false NOT NULL,
    "Level" integer DEFAULT 0 NOT NULL,
    "DefaultSkinId" integer DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer DEFAULT 0 NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL
);


ALTER TABLE "AbstractItemInstance" OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 227014)
-- Name: AddonItemContainer; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "AddonItemContainer" (
    "ContainerId" integer NOT NULL,
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "ModelCategoryTypeId" integer DEFAULT 0 NOT NULL,
    "ItemId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "AddonItemContainer" OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 227012)
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE; Schema: store; Owner: postgres
--

CREATE SEQUENCE "AddonItemContainer_ContainerId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AddonItemContainer_ContainerId_seq" OWNER TO postgres;

--
-- TOC entry 2885 (class 0 OID 0)
-- Dependencies: 204
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE OWNED BY; Schema: store; Owner: postgres
--

ALTER SEQUENCE "AddonItemContainer_ContainerId_seq" OWNED BY "AddonItemContainer"."ContainerId";


--
-- TOC entry 245 (class 1259 OID 227447)
-- Name: AddonItemInstance; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "AddonItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "AddonItemInstance" OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 227423)
-- Name: AmmoItemInstance; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "AmmoItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "AmmoItemInstance" OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 227407)
-- Name: ArmourItemInstance; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "ArmourItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "ArmourItemInstance" OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 227399)
-- Name: CharacterItemInstance; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "CharacterItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "CharacterItemInstance" OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 227026)
-- Name: FractionEntity; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "FractionEntity" (
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" integer DEFAULT 0 NOT NULL,
    "Discount_Ammo" integer DEFAULT 0 NOT NULL,
    "Discount_Weapon" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "FractionEntity" OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 227439)
-- Name: KitItemInstance; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "KitItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "KitItemInstance" OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 227431)
-- Name: SkinItemInstance; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "SkinItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "SkinItemInstance" OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 227415)
-- Name: WeaponItemInstance; Type: TABLE; Schema: store; Owner: postgres
--

CREATE TABLE "WeaponItemInstance" (
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" integer DEFAULT 0 NOT NULL
);


ALTER TABLE "WeaponItemInstance" OWNER TO postgres;

SET search_path = vrs, pg_catalog;

--
-- TOC entry 231 (class 1259 OID 227342)
-- Name: Roles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Roles" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" character varying(256) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE "Roles" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 227257)
-- Name: UserClaims; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserClaims" (
    "Id" integer NOT NULL,
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ClaimType" text,
    "ClaimValue" text
);


ALTER TABLE "UserClaims" OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 227255)
-- Name: UserClaims_Id_seq; Type: SEQUENCE; Schema: vrs; Owner: postgres
--

CREATE SEQUENCE "UserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "UserClaims_Id_seq" OWNER TO postgres;

--
-- TOC entry 2886 (class 0 OID 0)
-- Dependencies: 221
-- Name: UserClaims_Id_seq; Type: SEQUENCE OWNED BY; Schema: vrs; Owner: postgres
--

ALTER SEQUENCE "UserClaims_Id_seq" OWNED BY "UserClaims"."Id";


--
-- TOC entry 223 (class 1259 OID 227268)
-- Name: UserLogins; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserLogins" (
    "LoginProvider" character varying(128) DEFAULT ''::character varying NOT NULL,
    "ProviderKey" character varying(128) DEFAULT ''::character varying NOT NULL,
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "UserLogins" OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 227277)
-- Name: UserRoles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserRoles" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RoleId" uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL
);


ALTER TABLE "UserRoles" OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 227238)
-- Name: Users; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Users" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Balance" bigint DEFAULT 0 NOT NULL,
    "Email" character varying(256),
    "EmailConfirmed" boolean DEFAULT false NOT NULL,
    "PasswordHash" text,
    "SecurityStamp" text,
    "PhoneNumber" text,
    "PhoneNumberConfirmed" boolean DEFAULT false NOT NULL,
    "TwoFactorEnabled" boolean DEFAULT false NOT NULL,
    "LockoutEndDateUtc" timestamp without time zone,
    "LockoutEnabled" boolean DEFAULT false NOT NULL,
    "AccessFailedCount" integer DEFAULT 0 NOT NULL,
    "UserName" character varying(256) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE "Users" OWNER TO postgres;

SET search_path = achievements, pg_catalog;

--
-- TOC entry 2268 (class 2604 OID 226934)
-- Name: AchievementBonusId; Type: DEFAULT; Schema: achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementInstance" ALTER COLUMN "AchievementBonusId" SET DEFAULT nextval('"AchievementInstance_AchievementBonusId_seq"'::regclass);


SET search_path = gamemode, pg_catalog;

--
-- TOC entry 2413 (class 2604 OID 227291)
-- Name: InstanceId; Type: DEFAULT; Schema: gamemode; Owner: postgres
--

ALTER TABLE ONLY "GameMapEntity" ALTER COLUMN "InstanceId" SET DEFAULT nextval('"GameMapEntity_InstanceId_seq"'::regclass);


SET search_path = store, pg_catalog;

--
-- TOC entry 2309 (class 2604 OID 227017)
-- Name: ContainerId; Type: DEFAULT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer" ALTER COLUMN "ContainerId" SET DEFAULT nextval('"AddonItemContainer_ContainerId_seq"'::regclass);


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2406 (class 2604 OID 227260)
-- Name: Id; Type: DEFAULT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"UserClaims_Id_seq"'::regclass);


SET search_path = achievements, pg_catalog;

--
-- TOC entry 2821 (class 0 OID 226923)
-- Dependencies: 196
-- Data for Name: AchievementContainer; Type: TABLE DATA; Schema: achievements; Owner: postgres
--



--
-- TOC entry 2823 (class 0 OID 226931)
-- Dependencies: 198
-- Data for Name: AchievementInstance; Type: TABLE DATA; Schema: achievements; Owner: postgres
--



--
-- TOC entry 2887 (class 0 OID 0)
-- Dependencies: 197
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE SET; Schema: achievements; Owner: postgres
--

SELECT pg_catalog.setval('"AchievementInstance_AchievementBonusId_seq"', 1, false);


SET search_path = dbo, pg_catalog;

--
-- TOC entry 2871 (class 0 OID 227786)
-- Dependencies: 246
-- Data for Name: __MigrationHistory; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190254021_Loka', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3e4509a199f3321ed0645511aadc4119794ceec5b47a9bb4896d95dd55355cd91ecf02ff3837f92ff8281bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdf17fffa75b33e7888f322c9d2978727473f1e1ec4e9325b25e9edcbc35d79f3cf7f3bfcd77ff9cfffe9c5f96af3f5e01f6db9e7b41ca14c8b97877765b9fdf5f8b858dec59ba838da24cb3c2bb29bf268996d8ea35576fcecc71fff7e7c72721c13168784d7c1c18bab5d5a269bb8fa83fc7996a5cb785beea2f545b68ad745f33bf9725d713df83ddac4c5365ac62f0f3f64f7d1d1759c93361fbd4b6ff2a828f3ddb2dce5f1d1eba88caee26d562465967f3b3c385d271169e075bcbe393c88d2342ba39234ffd7cf457c5de6597a7bbd253f44eb4fdfb631297713ad8bb8e9d6af7d716c0f7f7c467b78dc13b6ac96bba2cc36960c4f9e37223b16c99d047fd889b412f766bb8ebfd26e57927d7978b98ebec5f9f9d76d9c2744fd440a62adbf9ead734ac02ba0a6e3ffa7e7f2c3c1fabec81f9adf7fe8e0435046ffef8783b3dd9a2aee651aefca3c5aff7070b9fbb24e96efe36f9fb2fb387d99eed66bb6e5a4ed977946d897df9a867f881fe2f5e141ddb27769f9fcd9e1c1ef842afab28e3b8d1e6b59b0bdf6e1f3471cdf83bc7ef9c9c0ebc531a3118ca26855afb27457f8e8a96332829afe2d4b52323a3ba9d07f7f2206c05ac8bf676572f32d082bdaa6b38c18a296d3ab6fa5898bb5a6cea2e2ee55b48e30638ad30e43f8c341f599377683a8e9224be36f36c89559bcce52463983a0ff9a4aaf2893259936ca2849e31c3b0cead9e5e874b9a47a3f52b16b045e130d22e8f7c97a5d780a3a8e569e2c4e8b8274dc93c9a738da04e8cef55de6db92df125f0eef88e7b3219ecfeb6813dd5a8158e6f57157de664ebc2c06c4e9176210a2655923f95d196f08c4939b64d9780a2eb3839ee7089345491a68619365060fd17ad7cf0f19a93fa05dbf26be654ca5729615a559c2b4f41147339e393fddb0d39b9b57f3aee02dfaab2c5bc7513a803c5f27c5b26eaf9d4c5bba11e59a13c1e67e723ddd6c325f7f33dad2318ee761a115facf0f7174bb8b893c33ac21a9298e78626e366d8a0ca216fadf561c647d476cefe1c145f4f5439cde9677c4b989be1e1ebc49bec6abf69746449fd3842c78a9e1c97766bd7df9920f5ec91b82e755bc0ae2e61267272e8a4f8c4da5ebe1460fec4707f7f9324f065be9fcfb2edec557517a8b5e8c36de5d4578d4930fefcd5d2456c31060405133c838be88cae5dd45bcf912e7a77f45f90a2bcb6b020bf2b195a9c86604990a4b910917f657f176d76ebdf8f07943bd2ac2e5dd8a1b89edcff427fa2994ea7f23bec6e96a95134d5a2f94ced6a4589c5f4429f15dbb9105711c1e09bf557ed6c016f732cbad1c259b21580d9bc2c594d5c32eba277d3e62d97c4726ed3c2d93f29bbc5559fd6cb9c069a4ca72184092e41bf703239babf886eb051dee82188f457209a91d65ddcfb7bbc46434544c4671982ef37893ec36e7a9c29db1b5e195cd94f7ab1d366b9b1d54892df3cd922bddb40318729b80d693cf6d42d7e314dc41fcc10f6471744a269c0782fe200ccf76791ea7a53cbbd5c834cc89495e9497515ea632a64d06a67349ec082bcfd085b0db2804540ced499ae4f87bf490dc568a955cf6bb247e883744aa1f123af95dc5ebaa5c71976cf9115019b18544f026cf3657d95a309762b9c53559c656232b4314fe14e5b77189ef85808bd6629bfaa22053f4082cadef174c62dbbb37d4f0ac0ceaa90b3535f01397d81f65d1ae657c67d4e5dbce637bf22e7d20e2c8f26f74330587378044a11fa9a45e3772715bbd741cc8ef37c93a3674482c6ed092b6b842537a1a5b6dd5ebf6da7c61871444a3d0975c54af30a0bcadc62ef3e481cc3d17645541161b6a7dd5c5ceeea2b229aad695bea8ac2743795b1d31330c5645008942435249bd82e4e2b6fae997bf86b1d41744593c6d71c558d2d3d8eaa9d9e4603c09b3a2201a85a6e4a27a5501e56d75f5b950f4e174bb5d372738b48c5235ba72924eb4852165bc38ee977388451ee38158ef6030b4476a6ed32eff9816b9af0405261e8bc210c7454156a74c97da4d31c6e106be3a2d817ab1a54549d7667d3d4ddf1dfc75a3bd8750b8001888360547a7303048625b6ba390a39b0c0ccc50f2d0f2b0918d9e91b587d3f8813593e19673f09c6958fb39d9e75096794e3619b02bb61639b8e1c2981c78be378c786ece379495068e91001a202ed8ea878c17b45a36334056b5afe809ad8e877ea2668455dd95781b13359165ceea322acb384f09e52a6eacebb4f37108c7a312ca65b4bc370b26ecb41e74a4491e3676687a78d9e29acae75045e435d588ebdbe132d6786aef63164f26218e8d95bbf657517acf8c97935f9c9c697f2ee0193be6d6a5c1e932faa241372d600f13b7d1e13182d9bd609fd1cbf2996ae4f60733b6a3163ed29966c452397a32a1274ab130d156306f30f5e72e2ecaa690f320e191833bc7e069b4e718dc1ff0e05097775b65d9f5c5f54c06d19770835cbe906deb17f383bd3b9000ae7a0775950f5afecd4d01077350b5c9c118b4741e03b09256cfc3e926fa19199eb744d280db2c7e9ac2506d988bfd8b13ded8185e16f8d4f46cb49a9e8f56d34fa3d5f473d09ad4ab9f87689d10c2d3d54a73f2d3565f1bb8be110bf107e0a8c18e545e1b59d2db9f8b1353b85ec7ab4a02856e3f9316382dcb687917b7e61fee33a6bc621bcf4064ddbb1a29f15ab7b4554e0f0b895cd6ab994ade58c2930ebd07eb73c541bb0fabbe11e1e51c88a00fe51a887c9f1c83efc13110b56ecb4e390c6bd9e946a1d2ac0ba4888984a3501a1b03d900864659b3488be8234f82efa44067db4bd5f43ead8b80eebdc1c5f0b2c42e0bb4fa49e3aba8888f2036add1a5a526b4b99de13b369715ad98f56eceac8decbba2758feddec502dba7fe014c5ec7371141c0f57d22ee11530cd19f1d7b39d0ce33fb9e87f2135e54bbaf58d47e3ae55e7dee4f51f861dadb1c4351c9b93395b775cc11f7a585abcb9087aa2822b55e55cecb2795456267072b5a406721cca083d9ebea77b1631cf15807b6218c67c563400bca7995539b77ed3259ebcb05b42ca2f782b544e17c6fa046a5d76d2a8be98ed9d376f2beaa5adc5cb0daf428990571c4a89b07ece071b0c1beb16945899ef6dcf58998f8600038e9539c066d94d8d20a13e544d307eb45d9ce1ec378607da415c80bebbf7a594de596b187f7229a149397e36649369bcc7d0d47a98f4416a3990dbec772d7aac0351e9dabe88f643673e9e0d95d4449dc4e49eb3e762c8e406673e9e9fbc463a381101f090ce6d2af76c9ead6314a7924b2984bd7ea804dee5aabe98f643673e960bf9f56f9053e1792fa830890699853084597e5a3146cb729ea42f7bae739c34e038792617a0e309eea465ae8e5bfdb93a86a15e2c723c0d157d58ceb75c6bb8ee77fee922df3c96f07cf78830b3a09e72831e7ed0c81ea619081cafa65bafe986490ab04f89e853a05696aa83cc4a00681e138a821846ed9614c21f1a742f6b76337cbced63e48c8feb21c67d9e56e35e07fcf5a6035cbee8a512b02393402d7c95e641545ecf4e6ba23f49844e97f070fbbf5ae384f699b56be0794ef8acfe93a5bdefb730a7233a55f9487ba92220d6c7cb01889d4182f46a0504cd0463217df437183b1ae8199fe9b5f802ee98b026155f4e56d4f469117fc8386f441ea27e08b1e5168de8657643859cc8bbe214ed12e78729f373e0da7c9575374b524ecec9f33dd6c3f0f7778298f50d5d9a5bea43c4cf4c5ed4344712669583366ee0b6cf69c863b149acb2ad67a33de653edf6964d326eebc1f9360e98aeaf8eadc086735d57e761ee135b3f354352c80f06c1c493f2af425a54161283ed0db3dae26d77876dadbf99af8771ee3db7f644f3ba6fb41653ba6830cc73a05053386e83fd9ec141384e955dfb2a9b0a33cfd0e362245182307b0a3532a0648440792272440e8c6e95e9757d5bb3d2fef487dde8ed45c68a6ca495eaa6b60db350c1b7e9323d146df644a02ee95b678f899245c1c514457422e0c45e6b5e0bdc62165f1bd8fc5c1b7e2827892ef8aab385af96fc25dc7e9cadbc9be8a9771f210d0fab40cadc66d651f444adde80508106318a2b2b54ab5d4ed7bc7d319fbc616c7f68ca3f1b2504dfc5a26e2b053e62699cd54164ac814606ba53489062cc75bc5690e916da80dfa666d82dc276b190bcac9da50541a0ea6f2b643bc6284ec4a534593d247d50bbe94e4f06b8a7a39fb325fef613cedeab51b3d4e03780ec3ae0a096e97ace42df92fd507f03846fce49091a6bf7ace3ebaa99386d5df5d1e06dec5cbfb60a9bdc324cb615f67569d64d3124e6b3c2b50581a1c96446f74fa925acb0914b77e96d45ac1218c26a6ed827d1d64f26a4f0e1093175014d38d702b4d0e25eee922a735fa9dc9b45e5b42b6d6f6860711c35ccc75451e6d558cfb2fb6d63a8fe9db546f137b5d4679083e6f923429ee0230fa7d47edc1c79b577c427a970ccd499ac6f9a738920e8a0d13054dff4bdbfe2ebd8e9759bab26d87ebe6346bc6d9a2bda1824b48f64951ccd6c4d271a431b06c2d6c51b8b97d096d739962d6415188a651b2ed0bc24d6dbf6b1bda15b26d660f4ba46c6502b8d962396df3a5c2fe7396dda6833ac5f1771afb36c4cc1726fa1460317d1c77a7e8024dba6c66be9473693b84f40d99405264ffe72e320699403fc5e883ccc3ae399ba68a2f2c1807a00c6c18a082d68b09c3a2c8798613775f0c1361b045042b19d5ea4155462b66afcd2ecce441cbe8642c7c87252c16f2dad96207308758df0983e5368324234e17dbc3a5111b3a5b089b13c33b6f88dbcc6970b091e3d9da6c824344675fdd87091d783e0383d24f35145affc1760c04f03b9c7ca820b3197a4da19bc9a48547c885a5ed8400ce5eaa59c309e9cd8e75b55d63f924bf21bd88d2e8b63ffa00184e3508da3d28db4100ed5dd96e0dd552f07f6b9c8b97677fcb8ab2f9ddc34fbedb95afb3bfc29c10d0d30efad28e2225cc9143957ab04c1e085403dd4126cc628723243903098b6bdfd4232d4874ef138432d5de8c683a5465243ba72ce8983618d37256600b894eea84aeb8640e313421ac6350cb38b555644c937528872056ed22227e5a1a1bde80d2cc57def7ce9aaa443b3a506d831d0bcfdfc2ca8652403bd2568aed4c3649ddd0980e72cfc388ebbbac9ef6aee2a8d8f679d6eaa3ca664aec7415150e694a3e6f57a49fb4b33b7eda6e5269f0df9d678e7e16083d65284cac7a6e71b2ab42da74ac5da5655b6b2ab098ca9aba98514ffbd93dd0e8c602264ba0c0e47c13256b8d417cf6f32f012c62550b71536f927c13e0f57c54147f65f9ea37f6f1ca505796afe3e52e27a8256375b31dbcb6cbbb8ccc88bbfaf066bcba82a9e6d35fd99b68596679a0900b1fb2e53d31d6e7e98a4e499fcba53c43211904694efd4af10d0173bc0ab0e3460d96c1fdc18e3f8fc5479428022208a675d11665b2528025e4e4137031dbadf60fd96da2886a2fd6d0165537b52e616c6a532cf88d28b11ed58186ae9cb1f15e071b94254ed84d4975a3ab02c6d6d6a542fa1315ce2ca34654be05c4e78783f57d913f34e5f6c6b5183ec43e9590f736191130a51f7cd2ab6afa47b4de85aeca15a395810980d18acf6418ad6a273f3f242beab91c9b29dac2843daa7c8b32eb67d77ccb943a3f79f6b72013ad3811b0dd1cbb72eb91e90a626aba036098b2990cc230c0c0a2b49d2e580c6029dbaac3abb4b903ebf4cea53e7d8dee09ae8f383e932dc79b7d0aa7b993a11d6b0e6d6f3687bdf04cd410f4a273f7e080bd0a56ffe6f80e64b0ebbdade46a18ea1cf1263a244fb02042e25d5a7531653a5ea86c900ca73c638fb10a339c4b62537650d80ee2610694fc08ca0efe9aeb0d5bc55acb1f9eaa54903a283bc1935e4808064d99d9a0c161c5761f6b7b5a3b2794a63966b4eb6ee302bd4b6ff28854bf5bd2761f29984e3585b607a80e838f219dfa02353de16adef8f311c8263c33f38dfd2541c410d7535f5e11d8d340e46528deaeb32fd1da35869244fddd866d0912c1e47c9510dfd5fe3e8c70b02969057bb8290daff476c789a60e16d6fcececb9ce39c08dd220d4bad1180449ea0b9ea43705fa92921130140f1ef545ae4f15f0455f12d19170615e6a8b48babac9ceac7d1e281cbec06adac0a5678eae374f3db50310a01dd56cef7ae9c6654b2dc076daa45b69135c8698fa1895cadf785a4695b2684a82a7657d01dd691953cace7ced362cdee4e727ef8a37ebe8b6e83a6f9510917974213f6d099297ed33b5dbeb6f44b7ec1e11afa07a3dde5d8f4a492faad39e97873f4acae4cabe4fd6ebc527d2d175477182a078f56d7195dcacfb6a9ee188aed384c087277d8e23bd48d2e4ed2eedc87e42d67897952cd9cf38b2cb8460a097c92fc8ca2ede7624ff1d47f23e4d6e7a59fc0d47f4368fd368d593fd1d2dc29ee6c4800c02f3242e207c1800c2104a303931e084a785d07262800bcf4104cd89013542fd02764e0ce0e1a905089d18302454cd20e9c400259e9207d48901513cad88ab1303b024613306c100afab78bbabcdebe2eadd9b4f3d9d015d02dde20d6941baeac90d0063c8dfc7f1b69a935a5203b26452a97203bacebf6ea9c0e845d2664bb5253400eb225e45eb3ac6d2e2554cefa9de6559490f765b060670490c6ae7a8a737404ca2a73a67e90d3093e87f8bf3aca7febb3c95d793b66622175360fbcce252bef001a7ef2a1681ce580bb0344de030144da3881b72e6b2227bc35039678afee4a0dbd34a093546afe23f770431cd0d7a4b2d37cb0415bf46c1fc8ef4788eda1f5152562e3b4ed5cc75559c963b02b122a391fc1ff1b28cd1ea7ed5a607c3b9669c7ffab3033cce88ea6eb3fc9be3d0170e20786e5343a2ca828845449d13130b079a941e0b016a1e9325da49af128a62f5dfe532c33ae5976d06429c374e1326e3ddf012eb7cb346cd65c26a73398798b0b834e753ae355fc73711e18c05ec1f77491963f1fa6a1d2defd1808dd2d559c400dc8058e2eac6294760406d4570997c8dd71c95694990ac1f18a01b90fb365bafb0c865e1f8379739b6b21dd53eacf72648c5ea88e3380d1c7f8bd79bb87c7582c5e3ab6cf58d296d0224c1e396fc3f436140e56594960553dc00cabaf957cfb098a4cd674a9bcc286d0c53dc04c675f610b3e5070664372d04c164c7ed48e43b0d322fe36cbb8e2f22f4dc5e13fc41fb8bf6eab32f59c95661f2f36879be06d31a9852fcc797e7cfd173fdb72f597efba18a7b8c43694df129cfe8463316ab35d1eb3c4b56e75b1a5e6d1cc812ef210858099fa39ed734006df67e16576f3f6221da92bc415bd096e2e2fd332c48fb76bdfe19ed8e262b22cec50916a54d79b4256dca3fc7c2b329ff131697d7cb288d73a6fda65dbcba7cdf7ed3be5d5dbe6fbf7133b826e83b60da04664796b8e98b1a5af54228c8e8aa591d711ca7196317c93a29a3fcdbeb28bf5f9c1605759df9ed6d831e3806e0f6b80dfd5d56deeed0b305472b6c6e1bc623477a45370bca0fd12e5dde3116de304439168d4d907898b6511b1eb5530e2bc0b491cab3805460da4a1538084a306da4f2d4821a0cc35e205628c2640b042e2a5d188f8996c94da21e08c693a28edce994a8a716e46f3c21ea28c5f31dd34ab02354c9ddb4add131508adc38ad100ed5125c2173e334d3d1834237ce3a3db9687a8cf34f472a88dd745ec4502ae46e3a326238a8046f3a3afaf0f1fde9e222a2a1443ee5bba2df2e319d185584af3f9d5e5f5da30f8b2a9a57711aafd7c9453f639b8e882ab2eb5dfa36fa9f16074335559911efe0efbfa08f8358efc0e9f0876e7d557b8e41ee7050464702cbf95fddb85e9215d2d7e7d8a9bf2efe1376b66f8aff0d3bc5d7e5ff869dceebe227bfa8a66e140aceffdc25db4a67d7ebac7405018d239867f438b338fa2d4a6fa3bcff45fae1bff17586799c312c4cd8ddf0931f4da52bbbce94b7f1f37f7471f4cf9bddfd6a379b88d4ff4047e638f1a14ebdad87f6b3a3e21e3beaea3d3af47661b37f89f597e9f622d631ae3617b13ef0ab2c630a1b268bcb3cd9107753381933de4ca1ef2a653293737bfe75bbce8ae4216616df2667b2a7e917e02637b2a7610cb8411d1fcb3b6e53c0e430d6e5993619b4529767da63318b8bbe1f6ae8b319cbdb674a6e369ce57424b3e5cc74f37dc4557ff507fe947f43babb628db0c15e642bfafc3acbb106e603cd239cab4c8c85e66ae1d6014c7cf47624b29b565f3517f43e7d9d5557a52d9438c5f7b99d30ebebd976d31fcf6ce2a9cfc653a16f5f5f136c56d7c4b0e8ff9015a54c64bce052c4514e1fddb2440693cd113d53cd5eb8a32e3122aa9dc21b7231b0eab0aa36dd5bb3d1f4d5ee3c6256c1a663addd599cd296a2d5bbbbce76255ab19f8bdfb39c296e98f6fe6d8b2df9b9e0db619ab0cf98733843174f77bd63e672c34e8888e1e77073cc263638a7eb755224bb0d168abf25b7777952a077d0afa394f4883896f82b00f93d7bf7cee04c3650bf2eb1e8a5c2f75a450331dd2de1d0da2389d3e4934fd92597442e96cb6c1bafcef22ac42a121015491dd918bdb952d19c7f652e9f1960d1dce06443617f6c8335639102f2a87725b12bc2864525550e7806cbd6d0911503476558905ce6f136ca638ec463baf503b94d707b16fc63ef0dfd91f1f6c680f80ab86c7917ff55113ddc52c21fb7718dca020e47de0854d0c03e78b65de875ac51e1e2a563ad0a6fb606bea20f4674775738c06e7fd57db54bcb64139fe779865ec537cf126a01f09406b5535cadb368d5080ae260c042cba136551003c3ccd2684fd302c3bcd2305037c07436baabf60e6e2add8e73f74b19f9c3715e0163bc702361ec99e4f4215a275ff0975388eb5a9066a09fa75c6fa38d0ae4b85d28213a8add2a9a153ccf690f4e75ae764591e06f869ea7b7645984de1f794d3a56e07746ce68969942e9a3490be8681b71e54debe2f7591e47f8b531c155ca7616b7403e2d8a6c99544069f1cfe6996c728db15916055d128372d0042d3010f6510efa483f10cde1c105415a42232024e537aa64ae4652e7c7f475bc8ecbf8a07ee1469f4615cbea05af2020d2e5957373bb488f6073fbf4507c6bff496ac4557c13e734326844b362d03548929662972ef3245d26db686d294c81cf8121456a6b338ec5fa8fbb06885f5ec75b1a43272d2de516ac655d0304dd9ae4fae29801b71ef3f51b4953cf9a903f2a40d93081c6829ade8c31c756e8213ece88741a1e2ebd1c7fa8b820e2910c9b3a70a2642ddd879096e124c349df22b7d9e3c7a323d9a7093d42500db7c4e1ab2cdd15c30d1794f6314d56309a780835fd601a56e56ed3831226520f853662dd28de95b689c0d8f019ad1ee3432f430ca0fab875dec8d74bcbb731a381b98f180305acd503464bab86b648666fe1f5152b013bc298f24037aa5323831ca5e1bdc17ad3ecb35d4ed9b671745050d791da1a7133beb5b529e16d1a57a1dd154c2b31d8e8db1dd2306b55856996c06062ecd6c18d9a5eb07f1880aba453a396e36e8d5d75857b6a978d1d1ad9261b358a694f4d37274c5bf81d4aba5130fd187c0d6387a6c4f49efa18dde3ac7e214c7fea360f541033d0419896496c306daa10da0c6972b1701d9a1db6911dc360a97bd42f4209761584606f1ee30189064cab2aba705d18610809ce1aed80766b45511e1a3282b73fcebe8aaa7d8e232cd0668a416a38f741e714a3c16e908f7f4ba631fbb5f170b0fb1ce128869fafd180cb8a76afcc3fd8bd7db3ff202ac277a22a3d8b69a0c55cadafc6ad2372410e2c1c3978c141416933c890b5efad9365d7bf598f353ba04cd19529075bf34fe7d1c6d38f3edc84eaf76ea16ed9b1d116ec96fade8385bbb247e20f86cd294b3ea38f0845333433515f760fe7237d777d16d021468b1e1398d6891ca6be58449dd6d3b28c967771bb9357fd86dad235506bae0cc9840e37860cb56b868869a44e3f40acba38d2b0b0523aa64df5d2747e2300316d608827c0ffe399216cfa3839fedde68249edbf1a0b556faa3511d27532b3d0794d663062fc26441bf67ef18eefe35e2ce0f1b079a48b78eec8f45dfa40b8922619cf4ff46473bc9c2a37d26b721ae27aaa528ea32dd17112db839579bba5d0f4a08999b8e8b265a03c2b13b91ae622a5bd6365ac5bb92d25f6717653895d0747f5abb01a4799f79674ba41d0470a657c44e3beac9e0c04bd486185777d75fbbe8440f56e2c90a3343bfb6583dc8de6179355d7d38d806cbe3ea509374d1f738435d835d4064c51c4657060839a45392e0dab99792e16975cb5b4a3fa2c8fe1c22baa53233be8280def8197cec6326e1a5fff749eea60ae2783102e53d8a0db501f00ecf6f33c818deb0f063f359927a671eaf46dced87b2900e450fb14803466b89b02b452390c74c36ea097656a214eb395a216d71e58e926063813b8eb7554468bab7819270f667063a861d71b22b4f3bf3155ef9f5f62d3adf1f06ea3674cab5acab9e1fe9ad6eb887a967654cc73153f16c4439d9a1aef9086316daae96684f5050b403cda169c303008f74337571f046cd4889a23c4a19e6190d490058737a4598bf650b239c11bb7a1a2a71b05e07bbe8982ebd084667b5f374fae49f3c95fffbe8bdb95050ed3063a08d332890da64d15ee1da6911d1a0dd3488dee01a6abec434d1798f8588512cc2a0208c54cd991b64c94ad0320cf94e57a3eccbd1393dc70133b25f7c4ae4944be0d1919b4280bac22b004adcee62aabd83b636beac96856d6a4b57d31af3411dda2ee87f68a1f545889505a6e4ca32ab64b6f508734a20a0961d04049438052210cbf268c05c7661061012915574272d4c31145dba683a5524ea809957208814ca5443c5b31023885a4570b9afe4d0b4f15010450a1ec482055b610806953964d7a37105c4d72c340a5e5e10759937cbc5b321e6c59bd2d9af5a1f1e227825603663ba8d8d5aab2a3431f2a5bb46d2cbb6aa1244c93a83d7988a735b5ec5441fb85593e49c52dfd00e3e249aec0cd4a8603a3be59186557ed0c37adcbc2f76bc4c850a3be2fdae16c0bcfcfddec5aa63292d0da6c00575394d0148ea6288bf9bb996cebff48d234ce691f2c2ca04834901d94aab141db303650d5a43197de0625609ad2d3cee228a95db2319051e1434f663a48b2c7a3a1bea99c445cb3c6328738a5ccdf3042fda8b611d487416a12372c0e34796b9a0940d8e5ec35188c79e9614053910f00615e409e2d9906bed6a7f300dd284675bf0f8c901dc24028f8e9bc46a3beed19fb7d0363900c90d6930d960347aec9c9ba567ec2d498c6750c6d15839cdce3d41aa44d6363bb41057b5483829c4c3718ba81aaec0e9b067a79a36e17cae50c0e4cb54ad02ef0c4c83cdd529d55bf7c2e48f3cfd651b2513bc1707130f8155f72ac7cdc70fba0300f7cc9aae05069b8b542c320c5371e95562a980650b2d960f443769ba4788cd6c5e78bd1a67d668c560547c2282fb40930ca4b65ef308a5a8ee9889cf0aa8922a8abca7215166e72c7086002ec0db1a29b0083b473783359959eaf95ac9b673692b4dc48369293d80430e544327f0bd9c4d57a4bfebae8cf762fa2ad06a36a1a5dc055bef85880d5b415806d552cda62176a5e615535e2c3c0a6250f1415552320ffd68cb698a7cd68967f363bb01862f5b25ea2b35fe11baadebb0d599b5e8db62b6ba3e53d702cdeaeb32f840bf3aaf17c95949909eb7a3208e512850dbe0dd54de6e4e2da351a36715ac134a7a69b112a51c133f46423a072bfa365e0fa33219af7344606e3dcd39e2da8af8e5abcf5a50d8b37686534fce28d69de5c166fb2c4c65dbcc922c1d44fa94642673d78ba4c9f4d03688c8dab789b15d4ea13cbb4dc1565b689d2342b2b0ebf922e9dad73aadde2e56199efe4b70094ed755c76b7c397d92e2d797b57ffafd21c4a769667d92430e99ffa769d2894aca1d2c68af857d412675f9eed7d6f03effe5a384a2e447bbb5a57d5f704640f16c54afe4d9e10281a74c91742f16d639296752e2b59269aa0dda80aea8168e02e27d532f0be264d8ef9d628995b355a4ec70ba9124acb8c693197ff1a468998b71ca945d2bdf53a5e558d520f4828779471ec30b6e43227ebaed28042392231aa0f4c306888b51cd1dbc0b50bde187fd1184128d6258ab18123567d525c1bd07200c189ac19d35906c7bc0eb565c233700547e20d15b2e0ab943154c8c097bb532c31e4be623829bbcc7dc573324c7bca92981ae8257525d3fae2bf810df8604ae20796c27136734573acfd7b603a104f5b4c860fb8c780e0dbdc774034b2396d37726c4ea7ed9a5b1f511979d7eb02036b61475de2297cc77063ce1094f3b65810c157d8c2554c504049136f71310c8916d8cdc04e7c9bec2c5b41e0174a209b8b503ea87866d5023acf9db7c3c6fc393c60c8345e3a47232eb59815ae81b45b8b779d16d70cd23ace8279bb928699338b0771cdce8b0e2156f512896d50ab7059c456f46a89d8b08144af5d176a346155af5e2b036be343549400069c34a3e7e5242d2dcb9134a66fc3d863aaa996a9a37a2eacd494a2bc491230196c9e80ed18a3a415fc01695a68d459a8e2b6057f28ab94ad9ecc24022db51adbd06e8c51dcfaba400c1bb5ea0be1b35d4eb719c52d010390612a24dc40e2e0a0866b5142dbac506749b35b660b7eff4c25663589a9f74a4a3596755b7a56358c8c60ae6e9cad509358f51a67237ce43a8d6590f71cf9bc90805c4d24ea5e1b2821b9823ba71ac19aaa00040becfa7a8b55b0415d0a65409caaa2ea3e2a2820f129f687350254311f497080feea44da7680e469ace0c2910e0449be8e9144ab3a16310b1849a95921a318a8ed2a7bdca35b88e3aa995adecce98aadc005520751f01c8613b950cf68139ab241e20f6ac7c196858374604ec36943519fd237160e7043ed84c827858bea37e3b68781d0b8bfa0a757cb5d3c0235ef64186a524a5cade1a0e2d6231f45e722020cc683c87a0a70ab6f3354cda9261fb3b541509b07be99894efe1ab1b8d636d25ccb2d9ebaab023ab7db4081dc729008836f6ac8358c6cb4a5fcc267771155be79e96da43489c0c4c0286cfe668951e4c6fac613bd94c3dcb42a375068baae25549b0bee668d4eb47afe138a944b0b8f93294f62d36930077d48a9f21598fc6dddc808672a705b7416f9ed3183d6b45517d4424cb36b67c8950e48da26bb3ad76f647e75a6cbe015398d649119d5a51a028397ab9abfba67f01e4cd9bdd593bb26bf7728ff41939cdb4e674e2617932c1a34bcd659a605eb6893679a35c2aa0b8e5a536c93597a70db80c9548c17b9fa8d164606e053ad61c40d3ed39a40d85caa5c949cd5c9750dfd06d3eb6aa56b2b5930a1aebdfec208d6e84cd8e4753575dc78dee72fdb695c0753a65040b056c945b95e63d38bb2bb03c6cbdd56358c265765b64a40a0b8cc965c3f8db92d990e6a2e8be3780242c35e15f7139c0989b8ec8aca4e9ab0e722b809d12626ef53094c9be44fee982acd9f28a8e60180494eaaec7c38b9bbcaa65108423a724943872402a58410beba2157dc604252663403c484cb7ec675cb98ff8ce998e129068e2f2030ccc31167c169736aa965884fc505751b958ccb4e02d6d5a89019d6b435238249006518beaa5451cac106248bf21cbe4076a8c1d108a63032880a4e75a4ec9694ecc8534c528a22d4bce2291b299b8e4146faec3bcabe29f3ef78ca4c99706728d91992c01816090005ce8397094d4b0484209199620633689ae425483942a94e8c5d14929d84959f90a2c46afd164684368b5688c4aabf368b563789cee05d809cc4c1b4f16d48fba0de9456277eb0ebb24d0d636195c78a9c3cc0245453be01759f351907428955936960b0a58a22e63d749506111d9fbfc8a28f8fcf74a979ffadbb14a38f65cf6a007e031e5c50ed5370b3a0a010eddace0941dabd042504541778b59d082e1dd304820f11aeed9d69eab097d73453051cad1a21b7ba20be7b7598e42092aa59994760d397601701a198c99a9b7fc610cbe0e53b5d9065a6abfa780768ce8010f5011a5c274e43a05ee5ec6913e01798df90217e85ee6be33dd8d732da6036448805846c135396eb3832aa2c2b58202c8646b2c838b213c8d474e46e1311d5d065d341bbb74ca7395e87a36cea271d55384ed54c0104e464fa054e0d0856e1271d1ac193f2ea5e9175df5e1c5f2fefe24dd4fcf0e2981459c6db7217adababd845fb8158eb6d92de163d65f3cbc1f5365ad20de07fbe3e3cf8ba59a7c5cbc3bbb2dcfe7a7c5c54ac8ba34db2ccb322bb298f96d9e6385a65c7cf7efcf1efc72727c79b9ac7f19213f80ba1b55d4d64681240095f49d5a4a56f92bc28e90d822f5141d470b6da48c584a896bcf03a61b795a90357cafaa43fd3a8362d2dfd7773db2ebb8f8eaee3fc21ce8fdea5377944a6c9ddb2dce5f111df9c235d15bdb4df1001d0d3d34a163183103d396170bd8cd651de061b9502f39e65ebdd263585eb3571a2ff8678d5bf5b70cbe34db2232a5c1129891c856f20d7b36cb35dc75f05b6e75fc95f499cb691731d75d47301aa5675e943fc10aff99ef48c16cd57a82f2a866c67145cd92236acff88e37b047bb118a889634115786d51f6afb27457f828ab6762d1ff7fcb9254065ec76ad17fb791eaef5999dc7cd3f1654bd870a6ed39a3764ad7e0a64060259d45c59d8f7e28fd2bd225cbd17491a5f137beb794d3a2f9dd4678afb3545249c5abfde02c31b0baabf836a16b253a4bcb5890bfe28d661df4a84c1ec814207396bfe2390bd151c4f902f88ce75d4ddc97515ea6f244247ec37315126eb24c0db938d53c85f4b22c4f43e6d98a273c7eae691899a24c963e83a863d247ffb51800ef93f5bae0fbd3315c341fad06541cad94fc9a8f36fc885b4b68951cbbcf363ce961a7b6df4c011bbed77799baa5cd471b7ebf256a76f5371b6eef52e280132ff275b4a9a241c27cc55236357cdc95b799b106b1949b857d712c38c5a2637e2c79e6c2824974fa514b82e6013118dc6cc8a5011868de75a18063a677f6d99879e00a422880b7b4a71bd999697fb35d928458dc301da1fa1359029f6d676d29faa05887a6985cd74423c3701f39c450083408fce01f020ed3aba88f7339b8aa54d714ed34a5e6825054b50ed268aafb3e0fab10c602569dba8c96f762dbb80fb301676d9a81d88a434eab52fa13d729d5cc48bd246c29453df15fa698fa546b3eb7c55eb352bc8ad27b7009597fb09b3c6566fdaf784ebda4551a98dd403144fa0c37447415a187879e89721d0f2fe11d56ef218704ed893c20da5ff19ce88a085824812ba249a1c607d4b0c1dabb82fefbe3cd7fb1701c942182feab8b03a1ce1785d6534525e8bbfdcd624faa8a242842b9fdd1c2881284dc1241414e87f86de0f101ef696db25572d31c572e4edc36b664b55d305c6db6b74a523bdf29be7d8bba80cd2ec743b4dee9793625026fb673953c9bb9609f0d20d8676308f6f9cc05fb7c00c13e1f43b03fcd5cb03f0d20d89fc610eccf3317eccf0308f6673fc1faba53a1377773b2b877bffdd1ba341f6f58764339383393ddfb449155c8417084d77722b53fe268ebb1e7210aae66f79dc8ce108cd2417c1dc7b94ad06bf9c804281e69ed2806adf659394ab9809fd68d83ecab8872167942df676f2aba0c0e014c45c76bae466200d95ddf27a1444759cd55728ee6558e873d927d0db52be7b61f376f8bf8ae38254b93e4cb5a58beb0bf5b1c71c8f7863597855537b26ea2ddbaa40340eca6f069848321c53dd28c461f71b115dd18a85858ac30a13356ca63a13b68552b1dbc474ad9f59ff67ca9ca0eda6a1ef239d7afe8f778f4cf472b9b4da617a4591946164fbac0e9a2da77f1d50682c9933e50fae896d59e2a41f279d20a4a2bef1383df67d48791c39326509a6897451eaaa8c89f74e1af8b7a07d5736060983c6e7d38aea4a1cc9cc38d39be26979d0d0413a57e5a124939ec87f1f7272b121d78c00216ebf220bbb17b896e3897ef30f8d6a7794760dbc460a41d89d749b1acf701bc76253a36363b13f5f12dbf51d3f059b41fad9ed091059e8a5df5c9ee757875d407b36b3feee1932e2189e4a0c7fd72fa49f7a75c185e63cd05f5b68ef812a2fd716c5b5d557cbdcec48714fdcfb3b1d0eacc62839eb00839c99c416866a4d45151c4d2fbc1ee47bcaee97f7926f52f36fbf6e769f4858c7f71dbbefbd986d7e7749d2def6566fdef539cb1f61b19e278673fcc6654c8890d071a0d624a44978160e6a1d46f4f2a2999ff641342a72654f09bc01a538b2b72697f9b0de2a06c76c360ce9cfa0f013a0c93e1ad4add0ae9c972f72b9e131c79471f6f47f53a73191742ec84f6b799a16d04987902cc165a4e80809761a47799db128cd23455521e162b1cd997a01c166a874209c22f5f728051fdb30da337c4395cc54034b48a1ff7d5aa7dd580f824dddeae5bc97cb48d4675992762e0b08a27f3ed291095b8e1304c20aaa95c3720cbe150ce9b589393f76666a2c4404d20bf52ed7eb6d804ed73612af8b59f865c3ecc0633754ed0b17003a55175c20ecc6844fc809cacae8c49a6c8d6017b575cc5d14a5e00b7bf5a2c1caa00c2d2d2a1fbd5e6957f9d58598eb3d0ff3e9bd100a418196620982bc29c032398a8d4e219594fcd13e436d5fd760afc6f22eeaa9f6609b9415725e68a2c2167bb42991f3caab894d21cd0fe88e7d3a660800e28c56f36e0bd25a28678f25f6cb622e3e5bd3ccd303f0fbc6d00af239acbd52ea0aee07315a5b0efa8d47b928acb64d28245f5b3cdf2e122fa0af2a13f3bafb5f6d590e9f30786b0609a1a10a64b4b3db485a812890a6cdadfa6b6334d661825d3fe9385a5c9636205c4038ff647ab703db9c4a7fbd1e2e9449226c59dc8a8ffd542933bea187dbc7925459de5bfe039f6e94245f1f35f2c660eba39f529d9c4efd2eb7899a5626c60e8fbbcecc8a0ceb7a606ac1db175b7c305d50a6591023e2205906b8f59cb4757b02771fa5794afdc3c0946ad3517bf1dc98a87cb96a42a0944cdd035bd842ad65ecd551f714fcd5575fdabe66abc04e6bef3fc14c2ddc8ef29843bccee7b0fe1aed4c19fbb48f4b3ea9fe6e8198c10f71a599b9dc7a0e7a43e3354867d770ff83e64286536d6b71c5459fa6ab3d76eeb474d89556a0e078527adc0159030edb04e9eadf33a91f2ced6bba28cf36a81ee12e5c1e2b5a4b926843e515c86dd9a689a20dd71ec7fb671e357abbcba35e422d2dfb2a26c18383976bf55611184cbcb94dba2fe6233b15f66b98257fd25f0147eb72b5f677f01dbb2fc17bb2de87dcb9c55d1c5a0a5113e596d43cd3d6a743bd246b157feb6cade4e395a18c53c447c90b4eabd3817311facf975760b60d97d9bf200681fc7b3cdd853b62ed9247503623ab5091a92bf5ab4ee2eab4deb551c15d21b2df92b9ef3e7ed8af492767527b497ff321b0ba4cae21c24ca079f8dd925c487898372f92f5ea7b11a71edfd468e45f7239ecff9264ac45cb6f54f963c884f7493e41bf12844fc66b1911c15c55f59befaadba0dca6d26735f6c2e1f2d7739d55b196db6e20d24ee93452bef3262d6ab131aa191ec07277e0a89c2252cb6d5ffcade444b8262f08994fcd5c21a66cb7b62979a74ce9fcba5600ee5cf0ebc81368bdf6c5f16bc21108d57e08e82f4d9c2ce165012edfed7d9d857c1829dada364b0dd05a82e7fa3ab60338ce5a5158a1cdadf6cd6b5a4c9f26d7de6674b5eff9083acb3bfcf066d54541fb2db241d0962555dfe1053b0519bab5bfa3a227b4856e2c4207cb27a0658d1bc17cf04b90f4301791ec6e92a5b0f97e550aeca1f373097616d0bad535ad965aa97a21329b6b90234e8c52e6d1d085d1ae8d5077b70e2577da65715b7f60296ea6296ed0ec556de27df5aeea3d6e7228efba80db1ffe5ca865180eb951da7012e58eef395a916620e83d42320355fab4f486a7dfbd54a731e72c1ef21071b2cb3b9893c9b207df494d1041023d2304cc2a26c4253d03cd21874caaeaf50196b43a806cd49bd5d5491c98fd5ba9fa7b81549b7bc9b4780505c04e0f3989bed53a1739d7d89d6233c50962a72f12acd3c94e638e0f3d2704f31cf57543822a7fe57bb3319f154427d1ea186717abb03c4c4fe3ee767b8534768da646764fa19d4c077b5789a77231fbd413e03a65ffecb14261e6a957d7b2a53ad389115bfcd068323edeff8efedd8edebf8ed38cba708939c20b41fbbdb4f6291aef6e697eeefa2fd810a9498cd2afa6fd1d35d2fefe24d54f5b6d846cb06ee6f92bc28a90ebf44455c17393c68f73a890cb6b7c59febfe978b284d6ee2a2fc94ddc7e9cbc3bf1ffd7cf4ecf0e0749d44059dd6d63787075f37ebb4f875b92bca6c13a56956bf4d7879785796db5f8f8f8baa8ee268932cf3acc86ecaa365b6398e56d9f1b31f4f9e1f9f9c1cc7abcdb148deb04571f9f1ef2d97a258ad59fc3018e7ec0964d75ebc8fc575d58b163257f18d648f8e051d8be42f94968c36e7e5e16e97907f8bfdfef51df114bebe3cfc5f15d5af07effe63d112fe70f031271af9f5e0c783ff7d78f0fb6ebda627732f0f6fa27521e116aebd46785d7f197f2d593664809bb9e4f126d96d9ac3c68e534210561ff1dab1ebdfc72c98d5fccbc3242d7fb2ee21c38c7d771386e31f717c0f72fd9b3557caea5596ee8a45fffc5a23474b9ebf676572f32d3457dad2b33ed673d5f367d6fcd8b84f1e02e4423e79f0912f50f90a4c5ecbf9723cdbe5399970d8c75996d643e2a0322398015b4d1e97515ea6b225b3e3c4bcb270e81447edd321217a8a6533386a9f66486fd13c602dbd430bc2ab7b8116841bf3f22c08bfe6d559105ef593b320acc4576641988a0fcbd04c593715e123710f7fc09bf9361e93f6e513d6791298c0d60725ce3683a1877730ad3f07bcf862fb826886c4c1ab3df5d4d7ab873994b56c979213dec0a2911e0ae3c6f77766841b141a66640372f5ec6ee51f7af6b7e3c176b8da25781b1385d02824975159c6794ad7f871bd76dbf70113c0025572bb8c96f7c380a5b6537db401f74573cfc305293cb587d19fd662cb3ebcdb22aaf6e7afa2f4de8b0f35b3de4cd8501443f9216fe89a1bccdb82039feab9b11978f0ba60cf404785e7507b4be8557bf3f2678079acb98ad2c84899cd0f8710385383191f2d9d073aba2c70ec20c42c806bba059f50cd4b59626e36cbd9d1d0a213e73dcba986ce265b2537cdb9c4e2645156d7c73d6ca5c0efa1be415e33bc596791c3a29063f92c70139f856fe2f3c04d7c1ebe893f056ee24fe19bf873e026feecd0446b4bfd29ca6f63c65e3fd9e9273b1dc64e8bc8726887ccc2a245d643c1d75d51668e05caea13ae225c6008f701016bc9ea5d714a6c5542085a365fb26c6dbff6f13e6c7c1ddf44bb7559e5f6f6eb926a5988f2d14d673a38fd6405cd72eabd1d51f17957f0277308fda0079029a7336ef86812779ac703473ce47695ef8403a482f60209985ada71ead1b5cd65fee1ad3e5a5435d98ca6667d83cc92410f245d0269dc2052470b358fa1401b6152be671f73ce277b0ec1a9cdf33cd4beac31abf134d6d0ed34aecd7f6ce9b735745e63d6d165b4761315bdaeb32c0f05127dd661e4610f9c6a1871c2d3127a2083fed7e7c21a939bd8c35f6493127bb099769dc42530b6bd44d4d30eb132bad4670646de33d0250246dc30e0c97d76fcfb0cc2b67a6e29f7d4a2b5c98ac39b33531adfc77d79b7cf0b6b597b4be8557bb86baaeddbb9a1f0e18e0c55e65d333260ddd80ed93e7baec75cd767cef564c2a5cbf555ba942ad767ff4748918b3e7386d66a135e46b69819f55957b147e08a978c9833f08ed467fb9ccfcc6a69c538ea00af20c631e31e3a96b3a3ee979e7d0c5020a3d36551f5f0d7fb57bb96686909bdb0caa65cb5acbf271d02afa644a638ac6af3969af1aab9b86f29e72ea7a5a5901bbabdb14710c2aa9c84e137ea4d79472d10e28c8dbdbeb6d5053db79d296b3a9fa7286266420f6f8d8f7bed7382d8a73cf59d18c22d6698d83f1e326222ff78497aa091ac4cc3897448e0e0fd0877041a00b67b87cde37ecb315493cd640809293d7d4e67db749ebeb8eff279fa32eaf379fa72e273797a08894fe169091c96d8073e50f0baf03b26cac4987b71ab7aa009720f5c034770ea61e9746bc67e1ee7d26c7a0c5339bba63733f025832bb34027dc4faf829f5e0563993abf0a0698d68924079d72d49915ad5f207a3e3ef4bdbae0f16c53cb4d0cb761cfcc3dba8221a2813706e4648638adc3290ccdea86e6ccd93a1c68511af308e2840a670f340b155a5ed9ae49fa6c60b617125a4adf1b384caa40f73d6b3e4da0c790e533fff92e8bf621c08d90e4cff602184bed1b80c5c68db41da43e035499330f71410f1e60d6b68f49b357f37988f2e55d941f1e5c445f3fc4e96d79f7f2f0979f6ca52e26db0bc93bd8e6e33e8c225bf8026d92b2ea79993131919e07333e7f5ef811aa4d35877c98e630343dc7649794cec3d16f52d2a947ddb39f7fb11d7662663a8fe3573e219dfbec2c64a17367c4259f0bc2268ca0e4ec721ecc807c721aeb84e9af9848cea37140ea381fcb52f01131bb2160f23c5ac2eae71f0ee8f5e0e4cf1df9f08988833a1fea3114d05201f9d9c63057c3bc776af331593a803599dfbde83e399cfb986693c261b9b8ea1d489a86bce4a8cf9106dfa956e74003cac359b5109728f996a967a49367f6d31cd789b0acc742ad2b54e478da38a4a8b2a30145e1146866954f37e0db16db5eaaca2c6fe6a395a649888634e8ca146808c3ae88d417dac0b3f977d87a1092ef493d2fd56c3db767b9dc6821f8f85efb18e5a41893f70b8753750e26334e55f8d99b5b3b964ff70c2996700257665532cb9b21dddb6b7440daa6a9f76ef093823e9fd19e5c089ff60a759f2cc9b2f696d0774bd96ac70a025b9f5ac96bba99e456bea5c1bbd4251db279df0727f5c1bef13bf33fd6828d9eedb2d4bf1d623ea281d4a7cddf33db0d54a78d1fdabf09367e4cf930a7f3be86f2def131819ae5cbc79b66f8b62fd5a70e3037c48b6b67a934c1489e44d28ba4891af324122a125a078d65f6248e4e1c55749627793023e67d527ed7f2e883a0ec7b00c7ef337029dea1ada686272d3f6e2dd73ed193961fb7966940bf271d3f6e1d5751789f74fca8754c9ccf27153f6e15d7d13c9f94fc98947c5a14d932a9aaed36a5e5947b6c6a3541e9e7e9aada7555e4ea6b3a4a13911fc1052e76eb32a11bd4a4752f0f4fc4ecf22f3ea6afe3755cc607a7cb3a6df959542ca3952c71d2c315a26d1d78154debbff32dfb27a94202d498a6fa4a227aa1969ec12772eaf9cb3c4997c9365aeba424108183c198c2908aa0ab4dfcf23aded203bbb4d40824582bbaca04059904f6e29881a31ea5ea14a82c56c1bdf8fd40ac26c72bdb425db1c78c5f6c0adcfd85b122cd2a1ed2ae86eec7a3a313092b7b054c578ba6c8953a263035697a2704680d3ad6b27e480ab527c01201a0683f7cd7769413050619aad8c863a253d786d1c02826bc5da845396b4c4a897be556c9451e1d16f5e98be78cc3067d4dbe5f6dca9601548f9aa52de11e144e4e8ad525a276009745cd821a27c6169b5379a149b03c67fbc6355b6e11fff9d1d935b5d21475f799a0e782bb3d9d579f706785bb89e7523967e042b3c5cc2c74a1649ddc4a172c300a0e813c886ccba0cfc32c898d094d1598506ed23b6cd35b61d8944152d102f80a93737347803def2e5690d72ea905f7928593f8699c0df510c32f10cc75aeb702037a370fbf8bee3ac026773301b35b0f7aa3dd950e0139adcb5f4538aad72cb3319bfa734e854af7cd66dab5174c2f3b09725509ebb1f89db1dfa0ea1ad83eb9d0930fa197e214ad9e7240b429460d23624e2bb8398e806956735e289e784da744a4f883e930b3e1d313682d3654ecbb45ad46283e2baab1102c124d7d422f27f3ae935b23111c0419c39e9002e9ca810352a8d4a0f875424f00f42293b82b1ad0254c9f19661f91d19d376ca733bb7ec09dd4d6aa855701b7f2cd91e89df3f22ed8c8faee97788ea36c0f1779dc958377e903e14a9a64dc369ee3da6e6ef09f767de708e159dc80e97048da7793ace345f702db60a5d54141b80d5f7529fca632dc52602cc84506411b321e8a42edc1fc03a9b7284bd83db09f0c77756b7b37d6eeb078f63eadd83d1ea7f2d7c7eac34a3d9dbdef2a23b3f905757f26807d7ad490f4305a44553190836c043cb654339ba4f7f45ad77ca6f029af77390d81899dc50f7174bb8beb98ca8d6dac7f3a4f75106ccb4810e43f8c0241b90b408bf8cf83408feb3a46f5358127f480eef9d63df692d9d485999a3e6fdc81cf04f6c8ea39426feae5714ee3bac64c70e8d751192daee2659c3cec1500e19e083e205ce431cdbd701731f5b73a9f1b16ebf8dd4f48fc7e90d8877a9f090e17ca1e0ca1f6d1606968db507074078632098127326d2aa78d9d1334f76f913c171ccec42cee839bd8a470f9f75ddcbab8fb873bb90f5c8ba0cf8f087740f7f60077553eee06704c2c884209388680d32ef7fb2870636ae45aae68155f6610e0b132c04d78b4a827ee545df46dc0c8e0db3b6be73010f6dabed9827b0e868d266359d46dd6de8de90acb9aac7f7de488ea7b8ad12c2d383298d4558e05a566f3180b26c04671bf7f0f80b2b05055f99131a5a97304509dad7745d93fc95afc9ead622dac04024ea1d2b751e0d5d44a5bae6b15ff7d10b889fdc7a8bfa5f1031dd447efdac7831fdbee459b46d47485cac1c0194254cd074953182e570c553903e369cd183b37feae4a791754d10628398033ac11b256e3ef609e3ffba9cf02b5ea2a47860cf5ebf6c999c2af11f6d81ed9ad06e6637ffe48d234ce69c30d56c85295b3b538d32cdb2cd0d86b64161bebed1a4ed58b417d1cb921caddf1e14133b245013a377fd30221a75a5daa37c7bd75fcc84f661c7150959fe6784653f53410dcbb7df2b9d8bde94f0503d53df67d5966c81860e76d66aa297410143a8d8801ac9eade9097244680d7d63d56363b011a1728bd76fd37bc601d4273952b6c60bd3cae922466ca9ecab5f3e1771be385b47c946edaa09c5f9c79fe2b7711ec5f3b556edd735ab2930ccbb4f41021810f8be82877a87a997969e0dea3e64b749bac7a8abdaaf6b5653e091a2aeeedddea10eb528f0c25ee0d931dc5ba4093013742531015a682ff6d844d11f75adaabf3f520355756efef6a9091bf196fc75d19d0a2e2ea2ad0677200d1894432c320a0aab4aa32dd02ce1cba0813884ae6390d092780291efa57fc5a32d2169339af5e3beeea1293a02340b28f38876d3547ddc8349f8ed3afb42b830ef8ace5749990d8242c37e86d414de9ac95f1f1182e4de61aaae553523ececdd93dd27ccd9626ee237ba8cdf497bb1a0fe246ad1a073cf21d7fc7b5a34a0bdf789160db4e06488a3d744840503d557cf76f126cf360bc5dae253b690198cb1d850cfb3407bd80aa0cf83ed924cbe7c3029c7a9f6d1d6104234532d308190d5049cc8609fc346aa5463357018d6f0c89d2e02e550615a4703ef1f71b4edd6befec8add9cd13b64ddb74986d8b7c3f808514365fb49ee69b6c17ceced6ece689d6a66d3ab4b645be1fb4420a9b2f5a6913aeef138c75955360b1686d1919911a287f1006a55d9b6084f69f8746e754997f4cea99372e99ac549ec0ac38cd0d9975a3d4d06cbe7f1fd80434345f70be4fca60f33be135cfc99d364c37b357dfbf9f695dd2d31ce1d967b5e050a701a9084f908311a09e29aa340fabc1e6702f6ee112430373f2444df8d7d9668d8ed4e43176fd2be7db19fd32f974d007dac2b1073e3f81be540bef9122bede1c7146bc4c3e1de281b6b0eca1cf4f882fd5c27ba4883fdd6c32770b2f104f68dfc59670cca58f4f482f55827ba4386fb76c9c705e11ce02e7524bb82795d2c7279c972ac13d529c9395b5b3391768a743b9d81096b7f4ed09e3a5426c8f14e2ac62aa9d4e37dfa5a29c05dce5a670dce5af4f902f95a27b14a0af37c4094d4928e2bc6d0869f69b242f4a9a14e34b54c890a554d771093c9a3f3ca8ff5779f1f77a79176fa29688064e8fbec8f786250c43353291c7bb2e28eb870beb5bc3d0189b846c8cbe194248f9ba2d3009be39fd78d6b5a62f856d4c4f81d2d655bcdd9515020d48910beab5249747b5e74d9e90c164680b5f48d50ebe9449339a831f59419ac2aad6888720f58127b255f249a9b24d7251738b6a1aab16212504cbe6baccf2b8072f58d6d08e6efad70d6ba890a20ded4c821fcb6f689b75e3462c00d72c96c2d958dadcd3b28c084bd36001cb9a3151fd6bbd8e57cdd939aa5d72aa5d45a3e4827a6bc267cd363646ce3f2db7432ea36c825cd4d0002803a7d402a850db84fa5bd702a828aa0986ca51d5a2852ee79202a42e176a2bdfeebe10a79691ba5cd4ba0975ca374433ea82e8a6d4c50dcd81825c494d810a299a0115b56882120b5021441390b8e0c2644a75735fdb4aab1fe37e00728530d529a5cd7dd5568714b032ab90ae6abea4b21940594c6beab8b570fdf5376dc7eb22868ac0e0dc5295602905b298b2484d4b990e54f563eb6ecb615d46310286ec058925dafa1f98d9a57e4d67595913f0c95463534c556d1317cbb2ee26ec8fa9eea698aaee263a9265ddf5db3c53d5752955cd75cc1353c5422407a94ae17b5b59fb48a9ab51288774afc5c7604a075b2ca86c878a00e5d701a104148e1d505231e880922695c84f8365b5c86554f5cb2551b220dee0263bd3e94651cee8e1d6c56d86847938a887026e18402f13a5faa0424a2042854d8b3d61edccbc3593d77ceab2d8a5394362d9b0f64191b1556d416c93daf296ed699f8c18dbd316c4b6a72d8f6e4fff4040d396be10627dda95b56a43b39a3534a229856b056e810cdef935eaa52a85554a55d8e434c1773c65d7092e07ef61280a9bec1870ed4e36654021c5160e50d2d002e81a94d402a810dc02a8a44906d2e5145902521145ffa572a6b5a17461405e194a45e0bae57286baa5735ca96aa9045cb354cc2470f93c4d96b85cc6b06ba8ac9e39e1d11f4a2cb8530d864c7342c1d188c752cc01a3e60c84340a7794a1e2288a5164a894cc312f1a84d8d42746ac205a974216a115fd98e2349f9b557c75c58614ee87a828018d3a095acf6b50c4cd54e4756d2c02ab24584ab12aca9bbacafbee4ce760177ca6c2120f15f9f07d4a99e9c986179dea9095e1653c2ef5c4d7d92ea77711c4d32603ca60aa11baebaf066781b1c7c60bfe0c59252d35c9f0d8820ecc193eda43f02022c20d4135c96313917ce8bce05d4459442612cdbca8398eaf2746c4e13ac7517db05ef1833e7b8b8cb72f55df5593a0aaa8ba43f0397dd519fdd97b78418741537d7fcd0e4e3c8da69baae551dd47d37a67722429e318188586a41c73289a6eff705ce542c309b3bdc3e1204d817448e33f5bf1893fa8e74e5b166661a8efb771e2808acd58d4ca9b4e0b26f08966a96a201ca9e3d0a24b7dd18b5d7341a50614aa1eb428baa9903abd5835314de81fd57463b60808ea29a6aa604af35def76170c75cea481628cc9697a8189573117428053b5e08c941adfd074a25bbb88d883594039aaaba88c9ae422fe2215af8a1ad77e068ac9eca4ea0a6d2d3fe38dd810a2132efd6264c7938c8091a985060f44dc9e8c9e6cf87d996986a87c977ad1de735648cb40a1ee267407bbea9cee6235c001ba48cef0d1de0bf79c47a11a0c13294032249626111178217c71152fe3e4412b2814e1a0434f7b47be3154a8ebeec1c4c66530c10b4d9df8e4518b6cc155809116473142370d5c0d1c87119a714634908c8bafe14524bf25318ac84432a488d44f692a3e889731f622621e6cb0c7ed05241b6559759780872a555f34af4f54f4d0cb139197f61d899f704cc051961d12310ee2751503bdd1bca859aa761ac07286c6b3ef78faa643cf73a6ea78e3f021ba2e9734740050bde615d8040210de0c2de8c32595089465d59d503c6eaa3a227dd30843f3508be5c57f0f251c96eba2b1d1babd100cd970b8994654ecf0601fbfe907125372d00ef90ad85320d4d861ec4a576e60ab8237c99e1dff2349d338a78c717890ca0f38b70c8e00f991f182ab40ef9a0214c3a142fdbe5af24b07154f35ab819ea9a6f480dd9adc7907ba6db3be8148c65adf8c85236ed38e913e729b4fa6181001fee2f61550d346ce8934480820196e3938965084a7d68bf60d3970540d97d49c68c10feaeb632cf19bee6c55f3561e62d614082e9af689bb59344dc9d145c33de587983505828bc6648bb5e58717d35483a979936d96475d7074bcb0efcd215ef5f760576484bcbe17d116168ea6b8f9fc1c7eddce9d9deb9fab735cc1b01115337d2008d7d9498aa2803cfb35d00d7ff8ab0c29c130338788b0179b14f66171be4aca4c233003c590a25246bca811658c6011423ca6d33903c5e3128f105963d104efd11a6ca620cad0ea8c2c64606760b087c8d70e0875f0b4f0234c10ea802a1547447414d7a962c8c4e5ca7966b46ce9a0ea86b91b17f8f2a2b3428749e6ad54e508b9c32754221fba07d4a022088ff3cdfa41b25bab2fe40f9f4c7b42f5f1918e40f5296216395fba2c43a77b565fd40c5e95596de15fb4a8834109ea528676f25155e80cc85a650d9a6e7912757171b3007dc141b05c15163a2bb0525983a61f9ed01c32d1c5405b08850a7338e61f28472e7435608c74bca0c2c23d92d2c65eab4fe45181d41c96adc173b9424bde6113c60eac1c7534ba9a9f39ba9cbd5ac2271c05d4327056d381d5a20ed157f14384dc73182d41b362422365b8b49b438f1245bcc29a9b2902a15bf88b70891b15b13206ca0c39b02a54e11beb13755340467b5584cc2d082862b0d48503ab4111c9b262660a4ee97e32d6b9dc6e26448ee3a23e550b5dd5a8e6ca2f9c8d46412f8e6b465df09aeedb8be33a5868f303f9b3ccf2e8b6dad15d17d5af2f8eaf76847a13d77fbd8e8be4b667f182f04ce32a3051cfb42df32ebdc9da2475428bda22ede7f65a615c46aba88c4ef332b9a12b8b3c5bd233b5f4f6f0e01fd17a478a9c6fbec4ab77e9c75db9dd95a4cbf1e6cb9adb77a649ee74f5bf3896dafce2e396fe5584e802696642ba107f4c5fed92f5aa6bf79b685d084a53b1a091d5dfc6e4f75a970446657cfbade3f47b96221935e2eb92fe7d8a37db3561567c4cafa387d8a56d9f8bf8437c1b2de923d78764457788554ccc8ae0c5fee27512dde6d1a66878f4f4e44f82e1d5e6ebbffc7f1ddbdd7208180400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190440066_AutomaticMigration', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3e4509a199f3321ed0645511aaec4919694ceec5b47a9bb4896d95dd55355cd91ecf02ff3837f92ff8281bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdf17fffa75b33e7888f322c9d2978727473f1e1ec4e9325b25e9edcbc35d79f3cf7f3bfcd77ff9cfffe9c5f96af3f5e01f6db9e7b41ca14c8b97877765b9fdf5f8b858dec59ba838da24cb3c2bb29bf268996d8ea35576fcecc71fff7e7c72721c13168784d7c1c18bab5d5a269bb8fa83fc7996a5cb785beea2f565b68ad745f33bf9725d713df83ddac4c5365ac62f0fdf67f7d1d1759c93361f5da437795494f96e59eef2f8e875544657f1362b9232cbbf1d1e9cae938834f03a5edf1c1e44699a9551499affebe722be2ef32cbdbdde921fa2f5a76fdb9894bb89d645dc74ebd7be38b6873f3ea33d3cee095b56cb5d51661b4b8627cf1b911d8be44e823fec445a897bb35dc75f69b72bc9be3cfcb88ebec5f9f9d76d9c2744fd440a62adbf9ead734ac02ba0a6e3ffa7e7f2c3c1fabec81f9adf7fe8e0435046ffef8783b3dd9a2aee651aefca3c5aff70f071f7659d2cdfc5df3e65f771fa32ddadd76ccb49db3fe619615f7e6b1afe3e7e88d7870775cb2ed2f2f9b3c383df0955f4651d771a3dd6b2607bedc3e78f38be0779fdf29381d78b6346231845d1aa5e65e9aef0d153c7640435fd5b96a464747652a1fffe440c80b5907fcfcae4e65b1056b44d671931442da757df4a13176b4d9d45c5ddab681d61c614a71d86f08783ea336fec0651d36596c6df6c902bb3789da58c720641ff35955e51264b326d945192c6397618d4b3cbd1e97249f57ea462d708bc261a44d0ef92f5baf014741cad3c599c1605e9b827934f71b409d09debbbccb725bf25be1c2e88e7b3219ecfeb6813dd5a8158e6f56157de664ebc2c06c4e9176210a2655923f9a28c3704e2c94db26c3c0597d941cf7384c9a2240db4b0c932838768bdebe7878cd41fd0ae5f13df32a65239cb8ad22c615afa88a319cf9c9f6ed8e9cdcdabb928788bfe2acbd671940e20cfd749b1acdb6b27d3966e44b9e644b0b99f5c4f379bccd7df8cb6748ce379586885fef37d1cddee6222cf0c6b486a8a239e989b4d9b2283a885feb7150759df11db7b7870197d7d1fa7b7e51d716ea2af87076f92aff1aafda511d1e734210b5e6a78f29d596f5fbee48357f286e07915af82b8b9c4d9898be2136353e97ab8d103fbd1c17dfe982783ad74fe7d17efe2ab28bd452f461befae223ceac987f7e62e13ab610830a0a819641c5f46e5f2ee32de7c89f3d3bfa27c8595e5358105f9d8ca546433824c85a5c8840bfbab78bb6bb75e7cf8bca15e15e172b1e24662fb33fd897e0aa5fadf88af71ba5ae54493d60ba5b3352916e797514a7cd76e64411c8747c26f959f35b0c5fd98e5568e92cd10ac864de162caea6117dd933e1fb16cbe2393769e9649f94ddeaaac7eb65ce0345265390c2049f28dfb8191cd557cc3f5820e77418cc722b984d48eb2eee7db5d62321a2a26068789cee6fe032b8f37c96e739e2abc195b135e994c79bbda61afb6d94095d832df2cb9d23d3b8021b707683df7dc2674394eb11dc41d7c4fd646a764be7920e00fc2f06c97e7715aca935b0d4cc39498e445f931cacb5486b4c9be741e891d61e518ba1076fb84808aa12d49931c7f8f1e92db4ab192c77e97c40ff18648f57d42e7beab785d952bee922d3f022a1bb69008dee4d9e62a5b0bd6522cb7b826abd86a646588c29fa2fc362ef1bd1070d11a6c535f14648a1e81a5f5fd82496c7bf7861a9e95413d75a1a6067ede12fba32cdab58cef8cba7cdb796c4f2ed207228e2cff46f7527078034814fa914aea752317b7d54bc781fc7e93ac634387c4e2062d698b2b34a5a7b1d556bd6cafcd17764841340a7dc945f50a03cadb6aec639e3c90b9e7922c2ac85a43adafbad8d95d543645d5bad21795f564286fab236686c1aa08205168482aa957905cdc563ffdead73096fa82288ba72dae184b7a1a5b3d357b1c8c2761561444a3d0945c54af2aa0bcadae3e178a3e9c6eb7ebe600879651aa46574ed289b630a48c17c7fd6a0eb1c6633c10eb0d0c86f648cd6ddad51fd322f785a0c0c4634d18e2b428c8e294e952bb27c638dcc057a725502fb6b428e9daacafa7e9bb83bf6eb4f7100a170003d1a6e0e8140606496c6b6d147274938181194a1e5a1e36b2d133b2f6701a3fb06632dc720e9e330d6b3f27fb1cca32cfc9260376c5d62207375c189303cff78611cfcdf986b2d2c031124003c4055bfd90f18256cb6606c8aaf6153da1d5f1d04fd48cb0aaab126f63a226b2cc597d8cca32ce5342b98a1beb3aed7c1cc2f1a884f2315ade9b0513765a0f3ad2240f1b3b343dbc6c714de573a622f29a6ac4f5ed70196b3cb5f7298b279310a7c6ca5dfbab28bd67c6cbc92f4eceb43f17f0881d73e9d2e074197dd1a09b16b08789dbe8f018c1ec5eb0cfe865f94c3572fb8319db510b1fe94c3362a91c3d99d013a55898682b983798fa7317176553c87990f0c8c19d63f034da730cee0f7870a8cbbbadb2ecfae27a2683e84bb8412edfc7b6f58bf9c1de1d480037bd83baca072dffe6a6808339a8dae4600c5a3a8f015849abe7e17411fd8c0ccf5b2269c06d163f4d61a836ccbdfec5096f6c0c0f0b7c6a7a365a4dcf47abe9a7d16afa39684dead5cf43b44e08e1e96aa539f969abaf0d5cdf8885f80370d460472aaf8d2ce9edcfc589295cafe355258142b79f490b9c9665b4bc8b5bf30ff719535eb18d6720b2ee5d8d9478ad5bda2aa78785442eebd54c256f2ce14987de83f5b9e2a0dd8755df88f0720e44d087720d44be4f8ec1f7e018885ab765a71c86b5ec74a35069d60552c444c251288d8d816c0043a3ac59a445f49127c17752a0b3eda56a7a9fd64540f7dee06278596297055afda2f15554c447109bd6e8d25213dadccef01d9bcb8a56cc7a3767d646f6a268dd63bb67b1c0f6a97ffc92d7f14d4410707d9f887bc41443f467c75e0eb4f3cc3ee7a1fc8407d5ee2b16b59f4eb9579ffb53147e98f636c7505472ee4ce56d1d73c47d69e1ea32e4a12a8a48ad5795f3f2496591d8d9c18a16d0590833e860f6bafa5dec18473cd6816d08e359f118d082725ee5d4e65dbb4cd6fa72012d8be8bd602d5138df1ba851e9759bca62ba63f6b49dbcafaa163717ac363d4a66411c31eae6013b781c6cb06f6c5a51a2a73d777d22263e18004efa14a7411b25b6b4c24439d1f4c17a51b6b3c7301e581f6805f2c2faaf5e5653b965ece1bd8826c5e4e5b85992cd26735fc351ea2391c5686683efb1dcb52a6e8d47e72afa2399cd5c3a7876175112b753d2ba8f1d8b2390d95c7afa2ef1d86820c4470283b9f4ab5db2ba758c521e892ce6d2b53a5e93bbd66afa2399cd5c3ad8efa7557e81cf85a4fe2002641ae61442d165f92805db6d8abad0bdee79ceb0d3c0a164989e038ca7ba91167af9eff624aa5a85f8f10870f45535e37a9df1aee3f99fbb64cb7cf2dbc133dee0824ec2394acc793b43a07a1864a0b27e99ae3f2619e42a01be67a14e419a1a2a0f31a84160380e6a08a15b76185348fca990fdedd8cdb2b3b50f12b2bf2cc75976b95b0df8dfb31658cdb2bb62d48a400e8dc075b2175945113bbdb9ee083d2651fadfc1e3d95d14e7296dd3caf780f2a2f89caeb3e5bd3fa7203753fa4579a82b29d2c0c6078b91488df162040ac5046d2473f13d143718eb1a98e9bff905e892be281056455fdef6641479c13f68481fa47e02bee81185e66d78458693c5bce81be214ed8227f779e3d3709a7c3545574bc2cefe39d3cdf6f3708797f208559d5dea4bcac3445fdc3e44146792863563e6bec066cf69b843a1b9ac42ad37e35de6f39d06366dc2cefb310996ada80eafce8d705653ed67e7115e333b4f55c30208cfc691f4a3425f521a1486e203bddde36a728d67a7bd9daf897fe731befd47f6b463ba1f54b6633ac870ac3350306388fe934d4e3141985ef52d9b0a3bcad3ef602352843172003b3aa5628044741c794202846e9cee757955bddbf3f28ed4e7ed48cd8526aa9ce4a5ba06b65dc3b0e13739126df44da624e05e698b879f49c2c511457425e4c250645e0bde6b1c5216dffb581c7c2b2e882779515cc5d1ca7f13ee3a4e57de4ef655bc8c938780d6a76568356e2bfb2052ea462f408018c31095ad55aaa56edf3b9eced837b638b6671c8d97856ae2d73211879d1237c96ca6b25042a6005b2ba549346039de2a4e73886c436dd0376b13e43e59cb58504ed686a2d2703095b71de2152364579a2a9a8c3eaa5ef0a524875f53d4cbd997f97a0fe36957afdde8711ac0731876554870bb64256fc97fa93e80c731e227878c34fdd573f6d14d9d33acfeeef230f02e5ede07cbec1d26590efb3ab3ea249b95705ae35981c2d2e0b0247aa3d397d45a4ea0b8f5b3a4d60a0e6134316d17eceb2093577b728098bc80a2986e845b69722871cf1639add1ef4ca6f5da12b2b5b6373c8818e662ae2bf268ab62dc7fb1b5d6794cdfa67a9bd8eb32ca43f07993a449711780d1ef3b6a0f3edcbce2f3d1bb24684ed234ce3fc59174506c982868f65fdaf68bf43a5e66e9cab61dae9bd3ac19678bf6860a2e21d92745315b134bc791c6c0b2b5b045e1e6f625b4cd658a590745219a46c9b62f0837b5fdae6d6857c8b6993d2c91b29509e0668be5b4cd970afbcf59769b0eea0cc7df69ecdb10335f98e85380c5f471dc9da20b34d9b299f9524ea5ed10d23764024991fd9fbbc8186402fd14a30f320fbbe66c9a2abeb0601c8032b061800a5a2f260c8b22e7194edc7d314c84c11611ac6454ab075519ad98bd36bb3093072da393b1f01d96b058c86b678b1dc01c627d270c96db0c928c385d6c0f97466ce86c216c4e0cefbc216e33a7c1c1468e676bb3090e119d7d751f2674e0f90c0c4a3fd55068fd07db3110c0ef70f2a182cc66e835856e2693161e211796b61302387ba9660d27a4373bd6d5768de593fc86f4324aa3dbfee8036038d52068f7a06c0701b47765bb35544bc1ffad712e5e9efd2d2bcae6770f3ff96e57becefe0a7342404f3be84b3b8a9430470e55eac1327920500d740799308b1d8e90e40c242cae7d538fb420d1bd4f10ca547b33a2e9509591ec9cb2a063da604ccb59812d243aa913bae29239c4d084b08e412de3d45691314dd6a11c8258b5cb88f869696c78034a335f79df3b6baa12ede840b50d762c3c7f0b2b1b4a01ed485b29b633d9247543633ac83d0f23aeefb27adabb8aa362dbe759ab8f2a9b29b1d3555438a429f9bc5d917ed2ceeef869bb49a5c17f779e39fa5920f494a130b1eab9c5c9ae0a69d3b17695966dada9c0622a6bea62463ded67f740a31b0b982c810293f34d94ac3506f1d9cfbf04b088552dc44dbd49f24d80d7f35151fc95e5abdfd8c72b435d59be8e97bb9ca0968cd5cd76f0da3ede656446dcd58737e3d5154c359ffecade44cb32cb03855c789f2def89b13e4f57744afa5c2ee5190ac9204873ea578a6f0898e355801d376ab00cee0f76fc792c3ea24411104130ad8bb6289395022c21279f808bd96eb5bfcf6e1345547bb186b6a8baa9750963539b62c16f4489f5a80e3474e58c8df73ad8a02c71c26e4aaa1b5d1530b6b62e15d29fa870661935a2f22d203e3f1cacef8bfca129b737aec5f021f6a984bcb7c9888029fde0935e55d33fa2f52e7455ae18ad0c4c008c567c26c368553bf9f9215951cfe5d84cd11626ec51e55b94593fbbe65ba6d4f9c9b3bf059968c58980ede6d8955b8f4c571053d31d00c394cd641086010616a5ed74c162004bd9561d5ea5cd1d58a7772ef5e96b744f707dc4f1996c39deec5338cd9d0ced5873687bb339ec8567a286a0179dbb0707ec55b0fa37c77720835def6d2557c350e78837d1217982051112efd2aa8b29d3f14265836438e5197b8c5598e15c129bb283c276100f33a0e4475076f0d75c6fd82ad65afef054a582d441d9099ef442423068cacc060d0e2bb6fb58dbd3da39a134cd31a35d771b17e822bdc92352fd6e49db7da4603ad514da1ea03a0c3e8674ea0bd4f484ab79e3cf47209bf0cccc37f6970411435c4f7d7945604f039197a178bbcebe446bd7184a12f5771bb625480493f355427c57fbfb30c2c1a6a415ece1a634bcd2db1d279a3a5858f3b3b3e73ae700374a8350eb46631024a92f7892de14e84b4a46c0503c78d417b93e55c0177d494447c28579a92d22e9ea263bb3f679a070f802ab6903979e39bade3cf5d40e40807654b3bdeba51b972db500db69936ea54d701962ea63542a7fe3691955caa229099e96f50574a7654c293bf3b5dbb078939f9f5c146fd6d16dd175de2a2122f3e8427eda12242fdb676ab7d7df886ed93d225e41f57abcbb1e95925e54a73d2f0f7f9494c9957d97acd78b4fa4a3eb8ee20441f1eadbe22ab959f7d53cc3115da709810f4ffa1c477a99a4c9db5dda91fd84acf12e2b59b29f71641f1382815e26bf202bbb7cdb91fc771cc9bb34b9e965f1371cd1db3c4ea3554ff677b4087b9a13033208cc93b880f06100084328c1e4c480139e1642cb89012e3c0711342706d408f50bd839318087a716207462c090503583a4130394784a1e50270644f1b422ae4e0cc09284cd180403bcaee2edae36af8bab8b379f7a3a03ba04bac51bd28274d5931b00c690bf8be36d3527b5a40664c9a452e506749d7fdd5281d18ba4cd966a4b6800d665bc8ad6758ca5c5ab98de53bdcbb2921eecb60c0ce09218d4ce514f6f8098444f75ced21b6026d1ff16e7594ffd77792aaf276dcd442ea6c0f699c5a57ce1034edf552c029db11660699ac061289a461137e4cc6545f686a172ce14fdc941b7a795126a8c5ec57fee08629a1bf4965a6e96092a7e8d82f91de9f11cb53fa2a4ac5c769caa99ebaa382d770462454623f93fe26519a3d5fdaa4d0f8673cd38fff46707789c11d5dd66f937c7a12f1c40f0dca686449505118b883a2726160e34293d1602d43c264bb4935e2514c5eabfcb658675ca3fb6190871de384d988c77c34bacf3cd1a359709abcde51c62c2e2d29c4fb9d67c1ddf44843316b07fdc25658cc5ebab75b4bc4703364a5767110370036289ab1ba71c8101b515c1c7e46bbce6a84c4b8264fdc000dd80dcb7d97a85452e0bc7bfb9ccb195eda8f661bd37412a56471cc769e0f85bbcdec4e5ab132c1e5f65ab6f4c691320091eb7e4ff190a032a3f46695930c50da0ac9b7ff50c8b49da7ca6b4c98cd2c630c54d605c670f315b7e604076d342104c76dc8e44bed320f3639c6dd7f165849edb6b823f687fd15e7df6252bd92a4c7e1e2dcfd7605a03538afff8f2fc397aaefff625cb6fdf57718f7128ad293ee519dd68c662b5267a9d67c9ea7c4bc3ab8d0359e23d04012be173d4f39a06a0cddecfe2eaed072c445b9237680bda525cbe7b860569dfaed73fa3ddd16445c4b938c1a2b4298fb6a44df9e7587836e57fc2e2f27a19a571ceb4dfb48b5797efdb6fdab7abcbf7ed376e06d7047d074c9bc0ecc812377d5143ab5e0805195d35ab238ee33463ec32592765947f7b1de5f78bd3a2a0ae33bfbd6dd003c700dc1eb7a1bfcbcadb1d7ab6e06885cd6dc378e448afe86641f93edaa5cb3bc6c21b8628c7a2b109120fd3366ac3a376ca61059836527916900a4c5ba902074109a68d549e5a508361d80bc40a45986c81c045a50be331d132b949d403c17852d4913b9d12f5d482fc8d27441da578be635a0976842ab99bb6353a064a911ba715c2a15a822b646e9c663a7a50e8c659a727174d8f71fee94805b19bce8b184a85dc4d47460c0795e04d4747ef3fbc3b5d5c463494c8a77c57f4db25a613a38af0f5a7d3ebab6bf4615145f32a4ee3f53ab9ec676cd311514576bd4bdf46ffd3e260a8a62a33e21dfcfd17f47110eb1d381dfed0adaf6acf31c81d0ecae8486039ffab1bd74bb242fafa1c3bf5d7c57fc2cef64df1bf61a7f8bafcdfb0d3795dfce417d5d48d42c1f99fbb645be9ec7a9d95ae20a07104f38c1e671647bf45e96d94f7bf483ffc37bece308f33868509bb1b7ef2a3a97465d799f2367efe8f2e8efe79b3bb5fed661391fa1fe8c81c273ed4a9b7f5d07e7654dc63475dbd4787de2e6cf62fb1fe32dd5ec43ac6d5e622d6077e95654c61c364f1314f36c4dd144ec68c3753e8bb4a99cce4dc9e7fddaeb322798899c5b7c999ec69fa05b8c98dec6918036e50c787f28edb1430398c7579a64d06add4e599f658cce2a2ef871afa6cc6f2f699929b0d67391dc96c3933dd7c1f71d55ffd813fe5df90eeae58236cb017d98a3ebfce72ac81794ff308e72a1363a1b95ab87500131fbd1d89eca6d557cd05bd4f5f67d555690b254ef17d6e27ccfa7ab6ddf4c7339b78eab3f154e8dbd7d7049bd535312cfadf67452913192fb8147194d347b72c91c1647344cf54b317eea84b8c886aa7f0865c0cac3aacaa4df7d66c347db53b8f9855b0e9586b7716a7b4a568f5eeaeb35d8956ece7e2f72c678a1ba6bd7fdb624b7e2ef8769826ec33e61cced0c5d35def98b9dcb0132262f839dc1cb3890dcee97a9d14c96e8385e26fc9ed5d9e14e81df4eb28253d228e25fe0a407ecfdebd3338930dd4af4b2c7aa9f0bd56d1404c774b38b4f648e234f9e45376c925918be532dbc6abb3bc0ab18a044445524736466fae5434e75f99cb670658343738d950d81fda60cd58a4803cea5d49ec8ab061514995039ec1b2357464c5c0511916241ff3781be53147e231ddfa81dc26b83d0bfeb1f786fec8787b63407c055cb6bc8bffaa881e6e29e10fdbb846650187236f042a68601f3cdb2ef43ad6a870f1d2b15685375b035fd10723babb2b1c60b7bfeabedaa565b289cff33c43afe29b6709b500784a83da29aed659b46a0405713060a1e5509b2a8881616669b4a76981615e6918a81b603a1bdd557b0737956ec7b9fba58cfce138af80315eb89130f64c72fa10ad932ff8cb29c4752d4833d0cf53aeb7d1460572dc2e94101dc56e15cd0a9ee7b407a73a57bba248f03743cfd35bb22c42ef8fbc261d2bf03b236734cb4ca1f4d1a40574b48db8f2a675f1bb2c8f23fcda98e02a653b8b5b209f1645b64c2aa0b4f867f34c36b9c6d82c8b822e8941396882161808fb28077da41f88e6f0e092202da1111092f21b55325723a9f343fa3a5ec7657c50bf70a34fa38a65f582571010e9f2cab9b95da447b0b97d7a28beb5ff2435e22abe89731a1934a25931e81a24494bb14b1ff3245d26db686d294c81cf8121456a6b338ec5fa8fbb06885f5ec75b1a43272d2de516ac655d0304dd9ae4fae29801b71ef3f51b4953cf9a903f2a40d93081c6829ade8c31c756e8213ece88741a1e2ebd1c7fa8b820e2910c9b3a70a2642ddd879096e124c349df22b7d9e3c7a323d9a7093d42500db7c4e1ab2cdd15c30d1794f6314d56309a780835fd601a56e56ed3831226520f853662dd28de95b689c0d8f019ad1ee3432f430ca0fab875dec8d74bcbb731a381b98f180305acd503464bab86b648666fe1f5152b013bc298f24037aa5323831ca5e1bdc17ad3ecb35d4ed9b671745050d791da1a7133beb5b529e16d1a57a1dd154c2b31d8e8db1dd2306b55856996c06062ecd6c18d9a5eb07f1880aba453a396e36e8d5d75857b6a978d1d1ad9261b358a694f4d37274c5bf81d4aba5130fd187c0d6387a6c4f49efa18dde3ac7e214c7fea360f541033d0419896496c306daa10da0c6972b1701d9a1db6911dc360a97bd42f4209761584606f1ee30189064cab2aba705d18610809ce1aed80766b45511e1a3282b73fcebe8aaa7d8e232cd0668a416a38f741e714a3c16e908f7f4ba631fbb5f170b0fb1ce128869fafd180cb8a76afcc3fd8bd7db3ff202ac277a22a3d8b69a0c55cadafc6ad2372410e2c1c3978c141416933c890b5efad9365d7bf598f353ba04cd19529075bf34fe7d1c6d38f3edc84eaf76ea16ed9b1d116ec96fade8385bbb247e20f86cd294b3ea38f0845333433515f760fe7237d777d16d021468b1e1398d6891ca6be58449dd6d3b28c967771bb9357fd86dad235506bae0cc9840e37860cb56b868869a44e3f40acba38d2b0b0523aa64df5d2747e2300316d608827c0ffe399216cfa3839fedde68249edbf1a0b556faa3511d27532b3d0794d663062fc26441bf67ef18eefe35e2ce0f1b079a48b78eec8f4227d205c49938ce7277ab2395e4e951be935390d713d5529c7d196e83889edc1cabcdd52687ad0c44c5c74d932509e95895c0d7391d2deb132d6addc9612fb38bba9c4ae83a3fa55588da3cc7b4b3add20e82385323ea2715f564f06825ea4b0c2bbbeba7d5f42a07a3716c8519a9dfdb241ee46f38bc9aaebe94640365f9fd2849ba68f39c21aec1a6a03a628e23238b041cda21c9786d5cc3c178b4bae5ada517d96c770e115d5a9911d749486f7c04b676319378daf7f3a4f7530d793410897296cd06da80f0076fb799ec0c6f507839f9acc13d33875fa3667ecbd140072a87d0a401a33dc4d015aa91c06ba6137d0cb32b510a7d94a518b6b0fac7413039c09dcf53a2aa3c555bc8c930733b831d4b0eb0d11daf9df98aaf7cf2fb1e9d67878b7d133a6552de5dc707f4deb75443d4b3b2ae6b98a1f0be2a14e4d8d7748c39836d57433c2fa8205201e6d0b4e181884fba19bab0f02366a44cd11e250cf30486ac882c31bd2ac457b28d99ce08ddb50d1d38d02f03ddf44c1756842b3bdaf9b27d7a4f9e4af7fdfc5edca028769031d846999c406d3a60af70ed3c80e8d8669a446f700d355f6a1a60b4c7cac42096615018462a6ec485b26cad6019067ca723d1fe6de89496eb8899d927b62d72422df868c0c5a9405561158825667739555ec9db135f564342b6bd2dabe98579a886e51f7437bc50f2aac44282d37a65115dba537a8431a5185843068a0a42140a910865f13c682633388b080948a2b2139eae188a26dd3c1522927d4844a398440a652229ead18019c42d2ab054dffa685a78a0002a8507624902a5b08c0b429cb26bd1b08ae26b961a0d2f2f083ac493ede2d190fb6acde16cdfad078f11341ab01b31d54ec6a55d9d1a10f952dda36965db55012a649d49e3cc4d39a5a76aaa0fdc22c9fa4e2967e8071f12457e06625c38151df2c8cb2ab76869bd665e1fb356264a851df17ed70b685e7e76e762d531949686d3680ab294a680a475394c5fcdd4cb6f57f24691ae7b40f161650241ac80e4ad5d8a06d181ba86ad2984b6f8312304de969677194d42ed918c8a8f0a127331d24d9e3d150df544e22ae596399439c52e66f18a17e54db08eac32035891b16079abc35cd0420ec72f61a0cc6bcf430a0a9c80780302f20cf964c035febd379806e14a3badf0746c80e612014fc745ea351dff68cfdbe8131480648ebc906cb8123d7e4645d2b3f616a4ce33a86b68a414eee716a0dd2a6b1b1dda0823daa41414ea61b0cdd405576874d03bdbc51b70be5720607a65a2568177862649e6ea9ceaa5f3e17a4f967eb28d9a89d60b83818fc8a2f39563e6eb87d509807be645570a834dc5aa16190e21b8f4a2b154c0328d96c30fa3ebb4d523c46ebe2f3c568d33e3346ab8223619417da0418e5a5b27718452dc774444e78d54411d45565b90a0b37b963043001f68658d14d8041da39bc99ac4acfd74ad6cd331b495a6e241bc9496c0298722299bf856ce26abd257f5df667bb97d1568351358d2ee02a5f7c2cc06ada0ac0b62a166db10b35afb0aa1af16160d392078a8aaa11907f6b465bccd36634cb3f9b1d580cb17a592fd1d9aff00d55efdd86ac4daf46db95b5d1f21e38166fd7d917c28579d578be4acacc84753d19847289c206df86ea26737271ed1a0d9b38ad609a53d3cd0895a8e0197ab21150b9dfd13270fd9910cd7b1a238371ee69cf16d457472ddefad286c51bb4321a7ef1c6346f2e8b375962e32ede649160eaa75423a1b31e3c5da6cfa60134c6c655bccd0a6af589655aee8a32db44699a9515875f4997ced639d56ef1f2b0cc77f25b00caf63a2ebbdbe1cb6c9796bcbdabff57690e253bcbb36c1298f44f7dbb4e144ad650696345fc2b6a89b32fcff6beb781777f2d1c2517a2bd5dadabea7b02b2078b6225ff264f08140dbae40ba1f8b63149cb3a97952c134dd06e5405f540347097936a19785f9326c77c6b94ccad1a2da7e3855409a565c6b498cb7f0da344cc5b8ed422e9de7a1dafaa46a90724943bca3876185bf23127ebaed28042392231aa0f4c306888b51cd1dbc0b50bde187fd1184128d6258ab18123567d525c1bd07200c189ac19d35906c7bc0eb565c233700547e20d15b2e0ab943154c8c097bb532c31e4be623829bbcc7dc573324c7bca92981ae8257525d3fae2bf810df8604ae20796c27136734573acfd7b603a104f5b4c860fb8c780e0dbdc774034b2396d37726c4ea7ed9a5b1f511979d7eb02036b61475de2297cc77063ce1094f3b65810c157d8c2554c504049136f71310c8916d8cdc04e7c9bec2c5b41e0174a209b8b503ea87866d5023acf9db7c3c6fc393c60c8345e3a47232eb59815ae81b45b8b779d16d70cd23ace8279bb928699338b0771cdce8b0e2156f512896d50ab7059c456f46a89d8b08144af5d176a346155af5e2b036be37d549400069c34a3e7e5242d2dcb9134a66fc3d863aaa996a9a37a2eacd494a2bc491230196c9e80ed18a3a415fc01695a68d459a8e2b6057f28ab94ad9ecc24022db51adbd06e8c51dcfaba400c1bb5ea0be1b35d4eb719c52d010390612a24dc40e2e0a0866b5142dbac506749b35b660b7eff4c25663589a9f74a4a3596755b7a56358c8c60ae6e9cad509358f51a67237ce43a8d6590f71cf9bc90805c4d24ea5e1b2821b9823ba71ac19aaa00040becfa7a8b55b0415d0a65409caaa2ea3e2a2820f129f687350254311f497080feea44da7680e469ace0c2910e0449be8e9144ab3a16310b1849a95921a318a8ed2a7bdca35b88e3aa995adecce98aadc005520751f01c8613b950cf68139ab241e20f6ac7c196858374604ec36943519fd237160e7043ed84c827858bea37e3b68781d0b8bfa0a757cb5d3c0235ef64186a524a5cade1a0e2d6231f45e722020cc683c87a0a70ab6f3354cda9261fb3b541509b07be99894efe1ab1b8d636d25ccb2d9ebaab023ab7db4081dc729008836f6ac8358c6cb4a5fcc267771155be79e96da43489c0c4c0286cfe668951e4c6fac613bd94c3dcb42a375068baae25549b0bee668d4eb47afe138a944b0b8f93294f62d36930077d48a9f21598fc6dddc808672a705b7416f9ed3183d6b45517d4424cb36b67c8950e48da26bb3ad76f647e75a6cbe015398d649119d5a51a028397ab9abfba67f01e4cd9bdd593bb26bf7728ff41939cdb4e674e2617932c1a34bcd659a605eb6893679a35c2aa0b8e5a536c93597a70db80c9548c17b9fa8d164606e053ad61c40d3ed39a40d85caa5c949cd5c9750dfd06d3eb6aa56b2b5930a1aebdfec208d6e84cd8e4753575dc78dee72fdb695c0753a65040b056c945b95e63d38bb2bb03c6cbdd56358c265765b64a40a0b8cc965c3f8db92d990e6a2e8be3780242c35e15f7139c0989b8ec8aca4e9ab0e722b809d12626ef53094c9be44fee982acd9f28a8e60180494eaaec7c38b9bbcaa65108423a724943872402a58410beba2157dc604252663403c484cb7ec675cb98ff8ce998e129068e2f2030ccc31167c169736aa965884fc505751b958ccb4e02d6d5a89019d6b435238249006518beaa5451cac106248bf21cbe4076a8c1d108a63032880a4e75a4ec9694ecc8534c528a22d4bce2291b299b8e4146faec3bcabe29f3ef78ca4c99706728d91992c01816090005ce8397094d4b0484209199620633689ae425483942a94e8c5d14929d84959f90a2c46afd164684368b5688c4aabf368b563789cee05d809cc4c1b4f16d48fba0de9456277eb0ebb24d0d636195c78a9c3cc0245453be01759f351907428955936960b0a58a22e63d749506111d9fbfc8a28f8fcf74a979ffadbb14a38f65cf6a007e031e5c50ed5370b3a0a010eddace0941dabd042504541778b59d082e1dd304820f11aeed9d69eab097d73453051cad1a21b7ba20be7b7598e42092aa59994760d397601701a198c99a9b7fc610cbe0e53b5d9065a6abfa780768ce8010f5011a5c274e43a05ee5ec6913e01798df90217e85ee6be33dd8d732da6036448805846c135396eb3832aa2c2b58202c8646b2c838b213c8d474e46e1311d5d065d341bbb74ca7395e87a36cea271d55384ed54c0104e464fa054e0d0856e1271d1ac193f2ea5e9175df5e1c5f2fefe24dd4fcf0e2981459c6db7217adababd845fb8158eb6d92de163d65f3cbc1f5365ad20de07fbe3e3cf8ba59a7c5cbc3bbb2dcfe7a7c5c54ac8ba34db2ccb322bb298f96d9e6385a65c7cf7efcf1efc72727c79b9ac7f19213f80ba1b55d4d64681240095f49d5a4a56f92bc28e90d822f5141d470b6da48c584a896bcf03a61b795a90357cafaa43fd3a8362d2dfd7773db2ebb8f8eaee3fc21ce8f2ed29b3c22d3e46e59eef2f8886fce91ae8a5eda6f8800e8e969258b9841889e9c30b85e46eb286f838d4a8179cfb2f56e939ac2f59a38d17f43bceadf2db8e5f126d91115ae8894448ec23790eb59b6d9aee3af02dbf3afe4af244edbc8b98e3aeab90055abbaf43e7e88d77c4f7a468be62bd4171543b6330aae6c111bd67fc4f13d82bd580cd4c4b1a00abcb628fb5759ba2b7c94d533b1e8ffbf65492a03af63b5e8bfdb48f5f7ac4c6ebee9f8b2256c38d3f69c513ba56b705320b092cea2e2ce473f94fe15e992e568baccd2f81bdf5bca69d1fc6e23bcd7592aa9a4e2d57e70961858dd557c9bd0b5129da5652cc85ff146b30e7a54260f640a9039cb5ff19c85e828e27c017cc6f3ae26ee8f515ea6f244247ec37315126eb24c0db938d53c85f4b22c4f43e6d98a273c7eae691899a24c963e83a863d247ffb51800ef92f5bae0fbd3315c341fad06541cad94fc9a8f36fc885b4b68951cbbcf363ce961a7b6df4c011bbed77799baa5cd471b7ebf256a76f5371b6e172971c08917f93ada54d12061be62299b1a3eeccadbcc588358cacdc2be38169c62d1313f963c7361c1243afda82541f380180c6e36e4d2000c34efba50c031d33bfb6ccc3c70052114c05bdad38decccb4bfd92e49422c6e988e50fd892c81cfb6b3b6147d50ac43534cae6ba29161b88f1c6228041a047ef00f0187e955d4c7b91c5c55aa6b8a769a52734128aa5a076934d57d9f8755086301ab4e7d8c96f762dbb80fb301676d9a81d88a434eab52fa13d729d5cc48bd246c29453df15fa698fa546b3eb7c55eb352bc8ad27b7009597fb09b3c6566fdaf784ebda4551a98dd403144fa0c37447415a187879e89721d0f2fe11d56ef218704ed893c20da5ff19ce88a085824812ba249a1c607d4b0c1da4541fffde1e6bf58380eca1041ffd5c58150e78b42eba9a212f4ddfe66b12755451214a1dcfe68614409426e89a020a743fc36f0f880f7b436d92ab9698e2b17276e1b5bb2da2e19ae36db5b25a99def14dfbe455dc06697e3215aeff43c9b128137dbb94a9ecd5cb0cf0610ecb33104fb7ce6827d3e80609f8f21d89f662ed89f0610ec4f6308f6e7990bf6e70104fbb39f607ddda9d09bbb3959dcbbdffe685d9a0f372cbba11c9c99c9ee5da2c82ae42038c2eb3b91da1f71b4f5d8f3100557b3fb4e64670846e920be8ee35c25e8b57c6402148fb476148356fbac1ca55cc04febc641f65544398b3ca1efb337155d068700a6a2e33557233180ecaeef9350a2a3ace62a3947f32ac7c31ec9be86da9573db8f9bb745bc284ec9d224f9b216962fecef16471cf2bd61cd6561d58dac9b68b72ee90010bb297c1ae16048718f34a3d1475c6c4537062a16162b4ce88c95f258e80e5ad54a07ef915276fda73d5faab283b69a877ccef52bfa3d1efdf3d1ca6693e90569568691c5932e70baa8f65d7cb58160f2a40f943eba65b5a74a907c9eb482d2cabbc4e0f719f561e4f0a4099426da6591872a2af2275df8eba2de41f51c1818268f5b1f8e2b692833e770638eafc9656703c144a99f9644520efb61fcfdc98a44071eb080c5ba3cc86eec5ea21bcee53b0cbef569de11d83631186947e275522ceb7d00af5d898e8dcdce447d7ccb6fd4347c16ed47ab27746481a762577db27b1d5e1df5c1ecda8f7bf8a44b482239e871bf9c7ed2fd291786d7587341bdad23be84687f1cdb5657155faf33f12145fff36c2cb43ab3d8a0272c424e3267109a1929755414b1f47eb0fb11af6bfa5f9e49fd8bcdbefd791a7d21e35fdcb6ef7eb6e1f5395d67cb7b9959fffb1467acfd468638ded90fb319157262c38146839812d16520987928f5db934a4ae63fd984d0a90915fc26b0c6d4e28a5cdadf668338289bdd309833a7fe43800ec36478ab52b7427ab2dcfd8ae70447ded1c7db51bdce5cc685103ba1fd6d66681b01669e00b385961320e06518e95de6b604a3344d959487c50a47f625288785daa15082f0cb971c6054ff6cc3e80d710e5731100dade2c77db56a5f35203e49b7b7eb56321f6da3517dcc13317058c593f9f614884adc70182610d554ae1b90e57028e74dacc9c97b33335162a026905fa9763f5b6c82f6b93015fcda4f432e1f6683993a27e858b881d2a83a61076634227e404e5657c6245364eb805d145771b49217c0edaf160b872a80b0b474e87eb579e55f275696e32cf4bfcf6634002946861908e68a30e7c008262ab57846d653f304b94d75bf9d02ff9b88bbeaa759426ed05589b9224bc8d9ae50e6078f2a2ea53407b43fe2f9b42918a0034af19b0d786f89a8219efc179badc878792f4f33cccf036f1bc0eb88e672b50ba82bf85c4529ec3b2af59ea4e23299b46051fd6cb37cb88cbe827ce8cfce6bad7d3564fafc81212c98a60684e9d2520f6d21aa44a2029bf6b7a9ed4c931946c9b4ff646169f2985801f1c0a3fdd12a5c4f2ef1e97eb4783a91a449712732ea7fb5d0e48e3a461f6e5e495167f92f788e7dba5051fcfc178b99836e4e7d4a36f1457a1d2fb3548c0d0c7d9f971d19d4f9d6d480b523b6ee76b8a05aa12c52c047a40072ed316bf9e80af6244eff8af2959b27c1a8b5e6e2b72359f170d992542581a819baa69750c5daabb9ea23eea9b9aaae7fd55c8d97c0dc779e9f42b81bf93d857087d97def21dc953af87317897e56fdd31c3d8311e25e236bb3f318f49cd46786cab0efee01df870ca5ccc6fa96832a4b5f6df6da6dfda829b14acde1a0f0a415b80212a61dd6c9b3755e2752ded97a5794715e2dd05da23c58bc9634d784d0278acbb05b134d13a43b8efdcf366efc6a9557b7865c44fa5b56940d0327c7eeb72a2c82707999725bd45f6c26f68f59aee0557f093c85dfedcad7d95fc0b62cffc56e0b7adf32675574316869844f56db50738f1add8eb451ec95bfadb2b7538e1646310f111f24ad7a2fce45cc076b7e9ddd025876dfa63c00dac7f16c33f694ad4b3649dd80984e6d8286e4af16adbbcb6ad37a154785f4464bfe8ae7fc79bb22bda45ddd09ede5bfccc602a9b2380789f2c167637609f161e2a05cfe8bd769ac465c7bbf9163d1fd88e773be891231976dfd93250fe213dd24f9463c0a11bf596c244745f15796af7eab6e83729bc9dc179bcb47cb5d4ef556469bad780389fb64d1cabb8c98f5ea84466824fbc1899f42a270098b6df5bfb237d192a0187c22257fb5b086d9f29ed8a5269df3e772299843f9b3036fa0cde237db97056f0844e315b8a3207db6b0b3059444bbff7536f655b06067eb28196c7701aacbdfe82ad80c637969852287f6379b752d69b27c5b9ff9d992d73fe420ebecefb3411b15d5fbec364947825855973fc4146cd4e6ea96be8ec81e92953831089fac9e015634efc43341eec350409e8771bacad6c3653994abf2c70dcc6558db42eb94567699eaa5e8448a6dae000d7ab14b5b074297067af5c11e9cf8559fe955c5adbd80a5ba9865bb43b195f7c9b796fba8f5b988e33e6a43ec7fb9b26114e07a65c769800b96fb7c65aa8598c320f50848cdd7ea13925adf7eb5d29c875cf07bc8c106cb6c6e22cf26481f3d653401c488340c93b0289bd014348f34069db2eb2b54c6da10aa4173526f175564f263b5eee7296e45d22defe61120141701f83ce666fb54e85c675fa2f5080f94a58a5cbc4a330fa5390ef8bc34dc53ccf315158ec8a9ffd5ee4c463c95509f47a8619cdeee0031b1bfcff919eed4119a36d919997e0635f05d2d9ee6ddc8476f90cf80e997ff328589875a65df9eca542b4e64c56fb3c1e048fb3bfe7b3b76fb3a7e3bcef229c2242708edc7eef69358a4abbdf9a5fbbb687fa0022566b38afe5bf474d7cbbb781355bd2db6d1b281fb9b242f4aaac32f5111d7450e0fdabd4e2283ed6df1e7baffe5324a939bb8283f65f771faf2f0ef473f1f3d3b3c385d275141a7b5f5cde1c1d7cd3a2d7e5dee8a32db44699ad56f135e1ede95e5f6d7e3e3a2aaa338da24cb3c2bb29bf268996d8ea35576fcecc793e7c72727c7f16a732c92376c515c7efc7bcba528566b163f0cc6397b02d9b517ef62715df5a285cc557c23d9a36341c722f90ba525a3cd7979b8db25e4df62bf7fbd209ec2d79787ffaba2faf5e0e23f162de10f071f72a2915f0f7e3cf8df8707bfefd66b7a32f7f2f0265a17126ee1da6b84d7f53f44f9f22eca0f0fc8b2f37d9cde96772f0f7ff989654c86bc996f1e6f92dda6397e6c799709c15c7de86bc7ae7f31b360d6f72f0f93b4fcc9bacf0c33f6254e188e7fc4f13dc8f56fd65c29ab5759ba2b16fd836c8d1c2d79fe9e95c9cdb7d05c694bcffae8cf55cf9f59f36323417908900b02e5c147be52e52b307975e7cbf16c97e7640a629f6b59da138983cab060066c359d7c8cf232956d9b1d27e6dd8543a7386a9f0e09f1542c9bc151fb34437a9de6016be9655a105edd9bb420dc98b76841f835efd082f0aa1fa1056125be3b0bc2547c6a8666ca3aae08af897b0a04ded5b7f1a1b46fa1b0ee94c004b63e2871b6390d3dbc83693d3ce00d18db174433240e5eeda9a7be5e3dcc31ad65bb949cf006168df4501837bec83323dca0d030231b90ab67772bffd0b3bf1d0fb6c3d5bec1db982884c625f91895659ca774d51fd7abb97d1f30012c5025b78fd1f27e18b0d476aa8f3fe0be8cee79b82085a7f630fad35a6cd987775b44d5fefc5594de7bf1a166d69b091b9c62283fe40d5d7383995c70e0533d4036030f5e17ec19e8a8f01c6a6f09bd6a6fde020d308f3597531a1929f3fbe11002e76e30e3a3a5f3404797178e1d849805704db7e053ac79294bccd666393b1a5a74e2bc8b39d5d0d964abe4a639a9589c2ccaea42b987ad14f83dd477ca6b8637eb2c725814722c9f056ee2b3f04d7c1eb889cfc337f1a7c04dfc297c137f0edcc49f1d9a686da93f45f96dccd8eb273bfd64a7c3d86911590eed905958b4c87a28f8ba2bca5cb240597d0a56840b0ce13e20602d595d14a7c4562584a065f325cbd6f66b1fefc3c6d7f14db45b9755b66fbf2ea99685281fdd74a683d34f56d0bca7dedb11159f8b823f9943e8073d804c599e71c34793cad33c1e38e221b7ab7c271c2039b41748c064d38e538fae6d2ef30f6ff5d1a2aac9663435eb1b64960c7a20e9524ae306913a7ea8790c05da08933240fb98733efd73084e6de6e7a1f6658d798ea7b1866ea7716d46644bbfada1f31ab38e2ea3b59ba8e8759d77792890e8f310230f7be0e4c388139e96d00319f4bfdd2d9af86b697bb182c956ece12fb2698a3dd84cbb4ee2521adb5e22ea698758197dd4e70a46de33d0a50646dc30e0c97d76fcfb9cc2b67a6e29f7d4a2b5e98bc39b335362dfc77d9db7cf146b597b4be8557bb86baaed6bbaa1f0e18e0c552e5e333260ddd80ed93e9faec75cd7e7d2f564c225d0f555ba943cd767ff47489a8b3e7386d66a135e46b69819f57958b147e08ab78d9833f08ed467fb9ccfd56a69c538ea00ef22c631e31e3a96f3a5ee979e7d0c5020a3d3e555f5f0d7fb77bc96686909bdb0ca2661b5acbf271d02afa6d4a638ac6a33999af1aab9b86f29e72ecba5a5901bbabdb14710c2aa2c85e137ea4d99482d10e28c8dbdbeb6d58541b79d296b3a9fa72862ae420f6f8d8f84ed7382d82741f59d18c22d669868401e3262620179497aa091ac4ccc897448e070fe0877041a00b67b87cd737fcb315493cd640809493e7d4e67db049fbeb8ef327cfa32ea337cfa72e2b37b7a08894fea69091c96d8073e5038bbf03b26ca54997b71ab7aa009720f5c034770ea61e9746bc67e1ee7126f7a0c5339dfa63733f025832bb34027dc4faf829f5e0563993abf0a0698d6a925079d72d4b916ad5f207a3e3ef4bdbae0f16c53cb4d0cb761cfcc3dba8221a2813706e4f48638adc3490dcdea86e6ccd93a1c68511a330be2840ae713340b155a5ed9ae49fafc60b617125a4adf1b384cf240f73d6b3e71a0c790e57301fa2e8bf621c08d90f6cff602184bed1b80c5c68db41da43e035499450f71410f1e60d6b68f49bc57f309134e4c4cbf179277b0cdc77d1845b6f005da24e5d9f33263626a3d0f667c46bdf023549b7c0ef930cd61687a8ec92e4d9d87a3df24a9538fba673fff623becc45c751ec7af7c8a3af7d959c84be7ce884b4717844d1841c9f9e63c980119e634d609d35f31b59c47e38064723e96a550c4c834791e2d61f5f30f07f47a70f2e78e7cf844c4419d0ff5180a68a9808c6d6398ab61de3bb5199a2c1dc09accef5e749f2ece7d4cb369e2b05c5cf50ea451435e72d4674d83ef54abb3a201e5e13c5b884b947ccbd433d2c933fb698eeb4458d663a1d6152a72846d1c5254f9d280a2705234b3caa71bf06d8b6d2f55659637f3d14ad3a448431a7465523484615744ea0b6de0d98c3c6c3d08c9f7a49e976ab69edbb35cb6b4107c7caf7d8c72528cc90486c3a93a2b9319a72afceccdad1dcba77b86a44b38812bf32c99e5cd90eeed353a2091d3d47b37f849419fe1684f2e844f7b85ba4f9f64597b4be8bba56cb5630581ad4fb6e435dd4c722bdfd2e07dd4a521b279df07a7f9c1bef13bf33fd6828d9eedb2d4bf1d6286a281d4a7cde833db0d54a78d1fdabf09367e4c1932a7f3be86f2def131819ae5cb879b66f8b62fd5a70e3037c48b6b67a934c1489e44d28ba4891af324122a125a078d65f6248e4e1c55749627793023e65d527ed7f2e883a0ec7b00c7ef337029dea1ada686272d3f6e2dd73ed193961fb7966940bf271d3f6e1d5751789f74fca8754c9ccf27153f6e15d7d13c9f94fc98947c5a14d932a9aaed36a5e5947b6c6a3541e9e7e9aada7555e4ea6b3a4a53931fc1052e77eb32a11bd4a4752f0f4fc47cf32f3ea4afe3755cc607a7cb3a91f959542ca3952c71d2c315a26d1d78154debbff32dfb27a94202d498a6fa4a227aa1969ec1277232fa8f79922e936db4d64949200207833185211541579bf8e575bca5077669a91148b0567495090a3209ecc53103473d4ad5295059ac827bf1fb81584d8e57b685ba628f19bfd814b8fb0b63459a553ca45d0ddd8f4747271256f60a98ae164d912b754c606ad2f44e08d01a74ac657d9f146a4f80250240d17ef8aeed28270a0c3254b191c744a7ae0da381514c78bb508b72d6989412f7caad928b3c3a2cead317cf19870dfa9a7cbfda942d03a81e354b5bc23d289c9c14ab4b44ed002e8b9a05354e8c2d36a7f242936079cef68d6bb6dc22fef3a3b36b6aa529eaee3341cf05777b3aaf3ee1ce0a7713cfa572cec085668b9959e842c93ab9952e5860141c027910d996419f8759121b139a2a30a1dca477d8a6b7c2b02983a4a205f01526e7e68e007bde5dac20af5d520bee250b27f1d3381bea21865f2098eb5c6f0506f46e1e7e17dd75804dee660266b71ef446bb2b1d02725a97bf8a7054af59666336f5e79c0a95ee9bcdb46b2f985e7612e4aa12d663f13b63bf41d535b07d72a1271f422fc5295a3de58068538c1a46c49c5670731c01d3ace6bc503cf19a4e8948f107d36166c3a727d05a6ca8d8778b5a8d507c5654632158249afa845e4ee65d27b74622380832863d2105d2950307a450a941f1eb849e00e84526715734a04b983e33cc3e22a33b6fd84e6776fd803ba9ad550baf026ee59b23d13be7e55db091f5dd2ff11c47d91e2ef2b82b0717e903e14a9a64dc369ee3da6e6ef09f767de708e159dc80e97048da7793ace345f702db60a5d54141b80d5f7529fca632dc52602cc84506411b321e8a42edc1fc03a9b7284bd83db09f0c77756b7b37d6eeb078f63eadd83d1ea7f2d7c7eac34a3d9dbdef2a23b3f905757f26807d7ad490f4305a44553190836c043cb654339ba4f7f45ad77ca6f029af77390d81899dc5f77174bb8beb98ca8d6dac7f3a4f75106ccb4810e43f8c0241b90b408bf8cf83408feb3a46f5358127f480eef9d63df692d9d485999a3e6fdc81cf04f6c8ea39426feae5714ee3bac64c70e8d751192daee2659c3cec1500e19e083e205ce431cdbd701731f5b73a9f1b16ebf8dd4f48fc7e90d8877a9f090e17ca1e0ca1f6d1606968db507074078632098127326d2aa78d9d1334f76f913c171ccec42cee839bd8a470f9f75ddcbab8fb873bb90f5c8ba0cf8f087740f7f60077553eee06704c2c884209388680d32ef7fb2870636ae45aae68155f6610e0b132c04d78b4a827ee545df46dc0c8e0db3b6be73010f6dabed9827b0e868d266359d46dd6de8de90acb9aac7f7de488ea7b8ad12c2d383298d4558e05a566f3180b26c04671bf7f0f80b2b05055f99131a5a97304509dad7745d93fc95afc9ead622dac04024ea1d2b751e0d5d44a5bae6b15ff7d10b889fdc7a8bfa5f1031dd447efdac7831fdbee459b46d47485cac1c0194254cd074953182e570c553903e369cd183b37feae4a791754d10628398033ac11b256e3ef609e3ffba9cf02b5ea2a47860cf5ebf6c999c2af11f6d81ed9ad06e6637ffe48d234ce69c30d56c85295b3b538d32cdb2cd0d86b64161bebed1a4ed58b417d1cb921caddf1e14133b245013a377fd30221a75a5daa37c7bd75fcc84f661c7150959fe6784653f53410dcbb7df2b9d8bde94f0503d53df67d5966c81860e76d66aa297410143a8d8801ac9eade9097244680d7d63d56363b011a1728bd76fd37bc601d4273952b6c60bd3cae922466ca9ecab5f3e1771be385b47c946edaa09c5f9c79fe2b7711ec5f3b556edd735ab2930ccbb4f41021810f8be82877a87a997969e0deade67b749bac7a8abdaaf6b5653e091a2aeeedddea10eb528f0c25ee0d931dc5ba4093013742531015a682ff6d844d11f75adaabf3f520355756efef6a9091bf196fc75d99d0a2e2ea3ad0677200d1894432c320a0aab4aa32dd02ce1cba0813884ae6390d092780291efa57fc5a32d2169339af5e3beeea1293a02340b28f38876d3547ddc8349f8ed3afb42b830ef8ace5749990d8242c37e86d414de9ac95f1f1182e4de61aaae553523ececdd93dd27ccd9626ee237ba8cdf497bb1a0fe246ad1a073cf21d7fc7b5a34a0bdf789160db4e06488a3d744840503d557cf76f126cf360bc5dae253b690198cb1d850cfb3407bd80aa0cf83ed924cbe7c3029c7a9f6d1d6104234532d308190d5049cc8609fc346aa5463357018d6f0c89d2e02e550615a4703ef1f71b4edd6befec8add9cd13b64ddb74986d8b7c3f808514365fb49ee69b6c17ceced6ece689d6a66d3ab4b645be1fb4420a9b2f5a6913aeef138c75955360b1686d1919911a287f1006a55d9b6084f69f8746e754997f4cea99372e99ac549ec0ac38cd0d9975a3d4d06cbe7f1fd80434345f70be4bca60f33be135cfc99d364c37b357dfbf9f695dd2d31ce1d967b5e050a701a9084f908311a09e29aa340fabc1e6702f6ee112430373f2444df8d7d9668d8ed4e43176fd2be7db19fd32f974d007dac2b1073e3f81be540bef9122bede1c7146bc4c3e1de281b6b0eca1cf4f882fd5c27ba4883fdd6c32770b2f104f68dfc59670cca58f4f482f55827ba4386fb76c9c705e11ce02e7524bb82795d2c7279c972ac13d529c9395b5b3391768a743b9d81096b7f4ed09e3a5426c8f14e2ac62aa9d4e37dfa5a29c05dce5a670dce5af4f902f95a27b14a0af37c4094d4928e2bc6d0869f69b242f4a9a14e34b54c890a554d771093c9a3f3ca8ff5779f1f77a79176fa29787dbea571a383dfa22df1b96300cd5c8441eefbaa0ac1f2eac6f0d43636c12b231fa66447c48f9ba2d3009be39fd78d6b5a62f856d4c4f81d2d655bcdd9515020d48910beab5249747b5e74d9e90c164680b5f48d50ebe9449339a831f59419ac2aad6888720f58127b255f249a9b24d7251738b6a1aab16212504cba628b33ceec10b9635b4a39bfe75c31a2aa468433b93e0c7f21bda66ddb8110bc0358ba570369636f7b42c8931884d83052c6bc644f5aff53a5e3567e7a876c9a976158d920beaad099f35dbd81839ffb4dc0eb98cb20972514303a00c9c520ba0426d13d6d5b7ae05505154130c95a3aa450b5dce2505485d2ed4897df78538b58cd4e5a2d64da853be219a51174437a52e6e680e14e44a6a0a5448d10ca8a845139458800a219a80c405172653aa9bfbda56baa13fc6fd00e40a61aa534a9bfbaaad0e29606556215dd57c49653380b298d6d4716be1faeb6fda8ed7450c1581c1b9a52ac1520a643165919a96321da8eac7d6dd96c3ba8c62040cd90b124bb4f53f30b34bfd9aceb2b226e093a9c6a698aada262e9665dd4dd81f53dd4d3155dd4d7424cbbaebb779a6aaeb52aa9aeb9827a68a85480e5295c2f7b6b25bf2f3265bf573a8500ee95e8b8fc1940eb65850d90e1501caaf034209281c3ba0a462d001254d2a919f06cb6a91cba8ea974ba26441bcc14d76a6d38da29cd1c3ad8bdb0c09f370500f05dc30805e264af541859440840a9b167bc2da99796b26aff9d465b14b7386c4b261ed832263abda82d826b5e52ddbd33e1931b6a72d886d4f5b1edd9efe8180a62d7d21c4fab42b6bd58666356b6844530ad70adc0219bcf36bd44b550aab94aab0c96982ef78caae135c0edec3501436d931e0da9d6cca80428a2d1ca0a4a105d03528a9055021b8055049930ca4cb29b204a4228afe4be54c6b43e9c280bc32948ac075cbe50c754be7b852d55209b866a99849e0f2799a2c71b98c61d750593d73c2a33f945870a71a0c99e68482a3118fa5980346cd19086914ee2843c55114a3c8502999635e3408b1a94f8c5841b42e852c422bfa31c5693e37abf8ea8a0d29dcf75151021a7512b49ed7a0889ba9c8ebda58045649b094625594377595f7dd99cec12ef84c85251e2af2e1fb9432d3930d2f3ad5212bc3cb785cea89afb35d4eef2288a74d0694c1542374d75f0dce02638f8d17fc19b24a5a6a92e1b1051d98337cb487e04144841b826a92c72622f9d079c1bb88b2884c249a7951731c5f4f8c88c3758ea3fa60bde2077df616196f5faabeab264155517587e073faaa33fab3f7f0820e83a6fafe9a1d9c781a4d3755cba3ba8fa6f5cee44852c631300a0d4939e65034ddfee1b8ca858613667b87c3419a02e990c67fb6e2137f50cf9db62cccc250df6fe3c401159bb1a895379d164ce013cd52d5403852c7a14597faa217bbe6824a0d28543d6851745321757ab16a629ad03faae9c66c1110d4534c55c194e6bbdeed2e18ea9c4903c51893d3f40213af622e8400a76ac1192935bea1e944b77611b107b380725457511935c945fc452a5e1535aefd0c1493d949d515da5a7ec61bb12144275cfac5c88e27190123530b0d1e88b83d193dd9f0fb32d30c51f92ef5a2bde7ac90968142dd4de80e76d539ddc56a800374919ce1a3bd17ee398f42351826528064482c4d2222f042f8e22a5ec6c983565028c241879ef68e7c63a850d7dd83898dcb6082179a3af1c9a316d982ab00232d8e62846e1ab81a380e2334e38c682019175fc38b487e4b62149189644811a99fd2547c102f63ec45c43cd8608fdb0b4836cab2ea2e010f55aabe685e9fa8e8a19727222fed3b123fe19880a32c3b24621cc4eb2a067aa37951b354ed3480e50c8d67dff1f44d879ee74cd5f1c6e143745d2e69e800a07acd2bb0090420bc195ad0874b2a1128cbaa3ba178dc547544faa61186e6a116cb8bff1e4a382cd74563a3757b2118b2e170338da8d8e1c13e7ed30f24a6e4a01df215b0a740a8b1c3d895aedcc056056f923d3bfe4792a6714e19e3f020951f706e191c01f223e3055781de350528864385fa7db5e4970e2a9e6a56033d534de901bb35b9f30e74db667d03918cb5be190b47dca61d237de4369f4c312002fcc5ed2ba0a68d9c136990104032dc72702ca1084fad17ed1b72e0a81a2ea939d1821fd4d7c758e237ddd9aae6ad3cc4ac29105c34ed1377b3689a92a38b867bca0f316b0a04178dc9166bcb0f2fa6a90653f326db2c8fbae0e87861df9b43bceaefc1aec808797d2fa32d2c1c4d71f3f939fcba9d3b3bd73f57e7b88261232a66fa4010aeb39314450179f66ba01bfef05719528261660e11612f3629ecc3e27c9594994660068a2145a58c785123ca18c12284784ca773068ac7251e21b2c6a209dea335d84c4194a1d51959c8c0cec0600f91af1d10eae069e1479820d401552a8e88e828ae53c59089cb95f3cc68d9d241d50d73372ef0e54567850e93cc5ba9ca1172874fa8443e740fa84145101ee79bf58364b7565fc81f3e99f684eae3231d81ea53c42c72be7459864ef7acbea819bc2ab3dac2bf6851078312d4a50cede4a3aad01990b5ca1a34ddf224eae2e26601fa828360b92a2c745660a5b2064d3f3ca13964a28b81b6100a15e670cc3f508e5ce86ac018e9784185857b24a58dbd569fc8a302a9392c5b83e7728596bcc3268c1d5839ea6874353f7374397bb5844f380aa865e0aca603ab451da2afe28708b9e7305a8266c58446ca706937871e258a78853537530442b7f017e112372a62650c9419726055a8c237d627eaa6808cf6aa08995b1050c460a90b075683229265c5cc149cd2fd64ac73b9dd4c881cc7457daa16baaa51cd955f381b8d825e1cd78cbae035ddb717c775b0d0e607f26799e5d16db5a3bb2eaa5f5f1c5fed08f526aeff7a1d17c96dcfe205e199c65560a29e695be622bdc9da2475428bda22ede7f65a615c46aba88c4ef332b9a12b8b3c5bd233b5f4f6f0e01fd17a478a9c6fbec4ab8bf4c3aedcee4ad2e578f365cded3bd32477bafa5f1c4b6d7ef1614bff2a4274813433215d883fa4af76c97ad5b5fb4db42e04a5a958d0c8ea6f63f27bad4b02a332befdd671fa3d4b918c1af17549ff3ec59bed9a302b3ea4d7d143ecd2b6cf45fc3ebe8d96f491eb43b2a23bc42a266645f0627ff13a896ef36853343c7a7af227c1f06af3f55ffe3f11eaf04919180400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190441545_AutomaticMigration', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3e4509a199f3321ed0645511aadc4119794ceec5b47a9bb4896d95dd55355cd91ecf02ff3837f92ff8281bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdf17fffa75b33e7888f322c9d2978727473f1e1ec4e9325b25e9edcbc35d79f3cf7f3bfcd77ff9cfffe9c5f96af3f5e01f6db9e7b41ca14c8b97877765b9fdf5f8b858dec59ba838da24cb3c2bb29bf268996d8ea35576fcecc71fff7e7c72721c13168784d7c1c18bab5d5a269bb8fa83fc7996a5cb785beea2f545b68ad745f33bf9725d713df83ddac4c5365ac62f0f3f64f7d1d1759c93361fbd4b6ff2a828f3ddb2dce5f1d1eba88caee26d562465967f3b3c385d271169e075bcbe393c88d2342ba39234ffd7cf457c5de6597a7bbd253f44eb4fdfb631297713ad8bb8e9d6af7d716c0f7f7c467b78dc13b6ac96bba2cc36960c4f9e37223b16c99d047fd889b412f766bb8ebfd26e57927d7978b98ebec5f9f9d76d9c2744fd440a62adbf9ead734ac02ba0a6e3ffa7e7f2c3c1fabec81f9adf7fe8e0435046ffef8783b3dd9a2aee651aefca3c5aff7070b9fbb24e96efe36f9fb2fb387d99eed66bb6e5a4ed977946d897df9a867f881fe2f5e141ddb27769f9fcd9e1c1ef842afab28e3b8d1e6b59b0bdf6e1f3471cdf83bc7ef9c9c0ebc531a3118ca26855afb27457f8e8a96332829afe2d4b52323a3ba9d07f7f2206c05ac8bf676572f32d082bdaa6b38c18a296d3ab6fa5898bb5a6cea2e2ee55b48e30638ad30e43f8c341f599377683a8e9224be36f36c89559bcce52463983a0ff9a4aaf2893259936ca2849e31c3b0cead9e5e874b9a47a3f52b16b045e130d22e8f7c97a5d780a3a8e569e2c4e8b8274dc93c9a738da04e8cef55de6db92df125f0eef88e7b3219ecfeb6813dd5a8158e6f57157de664ebc2c06c4e9176210a2655923f95d196f08c4939b64d9780a2eb3839ee7089345491a68619365060fd17ad7cf0f19a93fa05dbf26be654ca5729615a559c2b4f41147339e393fddb0d39b9b57f3aee02dfaab2c5bc7513a803c5f27c5b26eaf9d4c5bba11e59a13c1e67e723ddd6c325f7f33dad2318ee761a115facf0f7174bb8b893c33ac21a9298e78626e366d8a0ca216fadf561c647d476cefe1c145f4f5439cde9677c4b989be1e1ebc49bec6abf69746449fd3842c78a9e1c97766bd7df9920f5ec91b82e755bc0ae2e61267272e8a4f8c4da5ebe1460fec4707f7f9324f065be9fcfb2edec557517a8b5e8c36de5d4578d4930fefcd5d2456c31060405133c838be88cae5dd45bcf912e7a77f45f90a2bcb6b020bf2b195a9c86604990a4b910917f657f176d76ebdf8f07943bd2ac2e5dd8a1b89edcff427fa2994ea7f23bec6e96a95134d5a2f94ced6a4589c5f4429f15dbb9105711c1e09bf557ed6c016f732cbad1c259b21580d9bc2c594d5c32eba277d3e62d97c4726ed3c2d93f29bbc5559fd6cb9c069a4ca72184092e41bf703239babf886eb051dee82188f457209a91d65ddcfb7bbc46434544c4671982ef37893ec36e7a9c29db1b5e195cd94f7ab1d366b9b1d54892df3cd922bddb40318729b80d693cf6d42d7e314dc41fcc10f6471744a269c0782fe200ccf76791ea7a53cbbd5c834cc89495e9497515ea632a64d06a67349ec082bcfd085b0db2804540ced499ae4f87bf490dc568a955cf6bb247e883744aa1f123af95dc5ebaa5c71976cf9115019b18544f026cf3657d95a309762b9c53559c656232b4314fe14e5b77189ef85808bd6629bfaa22053f4082cadef174c62dbbb37d4f0ac0ceaa90b3535f01397d81f65d1ae657c67d4e5dbce637bf22e7d20e2c8f26f74330587378044a11fa9a45e3772715bbd741cc8ef37c93a3674482c6ed092b6b842537a1a5b6dd5ebf6da7c61871444a3d0975c54af30a0bcadc62ef3e481cc3d17645541161b6a7dd5c5ceeea2b229aad695bea8ac2743795b1d31330c5645008942435249bd82e4e2b6fae997bf86b1d41744593c6d71c558d2d3d8eaa9d9e4603c09b3a2201a85a6e4a27a5501e56d75f5b950f4e174bb5d372738b48c5235ba72924eb4852165bc38ee977388451ee38158ef6030b4476a6ed32eff9816b9af0405261e8bc210c7454156a74c97da4d31c6e106be3a2d817ab1a54549d7667d3d4ddf1dfc75a3bd8750b8001888360547a7303048625b6ba390a39b0c0ccc50f2d0f2b0918d9e91b587d3f8813593e19673f09c6958fb39d9e75096794e3619b02bb61639b8e1c2981c78be378c786ece379495068e91001a202ed8ea878c17b45a36334056b5afe809ad8e877ea2668455dd95781b13359165ceea322acb384f09e52a6eacebb4f37108c7a312ca65b4bc370b26ecb41e74a4491e3676687a78d9e29acae75045e435d588ebdbe132d6786aef63164f26218e8d95bbf657517acf8c97935f9c9c697f2ee0193be6d6a5c1e932faa241372d600f13b7d1e13182d9bd609fd1cbf2996ae4f60733b6a3163ed29966c452397a32a1274ab130d156306f30f5e72e2ecaa690f320e191833bc7e069b4e718dc1ff0e05097775b65d9f5c5f54c06d19770835cbe906deb17f383bd3b9000ae7a0775950f5afecd4d01077350b5c9c118b4741e03b09256cfc3e926fa19199eb744d280db2c7e9ac2506d988bfd8b13ded8185e16f8d4f46cb49a9e8f56d34fa3d5f473d09ad4ab9f87689d10c2d3d54a73f2d3565f1bb8be110bf107e0a8c18e545e1b59d2db9f8b1353b85ec7ab4a02856e3f9316382dcb687917b7e61fee33a6bc621bcf4064ddbb1a29f15ab7b4554e0f0b895cd6ab994ade58c2930ebd07eb73c541bb0fabbe11e1e51c88a00fe51a887c9f1c83efc13110b56ecb4e390c6bd9e946a1d2ac0ba4888984a3501a1b03d900864659b3488be8234f82efa44067db4bd5f43ead8b80eebdc1c5f0b2c42e0bb4fa49e3aba8888f2036add1a5a526b4b99de13b369715ad98f56eceac8decbba2758feddec502dba7fe014c5ec7371141c0f57d22ee11530cd19f1d7b39d0ce33fb9e87f2135e54bbaf58d47e3ae55e7dee4f51f861dadb1c4351c9b93395b775cc11f7a585abcb9087aa2822b55e55cecb2795456267072b5a406721cca083d9ebea77b1631cf15807b6218c67c563400bca7995539b77ed3259ebcb05b42ca2f782b544e17c6fa046a5d76d2a8be98ed9d376f2beaa5adc5cb0daf428990571c4a89b07ece071b0c1beb16945899ef6dcf58998f8600038e9539c066d94d8d20a13e544d307eb45d9ce1ec378607da415c80bebbf7a594de596b187f7229a149397e36649369bcc7d0d47a98f4416a3990dbec772d7aac0351e9dabe88f643673e9e0d95d4449dc4e49eb3e762c8e406673e9e9fbc463a381101f090ce6d2af76c9ead6314a7924b2984bd7ea804dee5aabe98f643673e960bf9f56f9053e1792fa830890699853084597e5a3146cb729ea42f7bae739c34e038792617a0e309eea465ae8e5bfdb93a86a15e2c723c0d157d58ceb75c6bb8ee77fee922df3c96f07cf78830b3a09e72831e7ed0c81ea619081cafa65bafe986490ab04f89e853a05696aa83cc4a00681e138a821846ed9614c21f1a742f6b76337cbced63e48c8feb21c67d9e56e35e07fcf5a6035cbee8a512b02393402d7c95e641545ecf4e6ba23f49844e97f070fbbf5ae384f699b56be0794ef8acfe93a5bdefb730a7233a55f9487ba92220d6c7cb01889d4182f46a0504cd0463217df437183b1ae8199fe9b5f802ee98b026155f4e56d4f469117fc8386f441ea27e08b1e5168de8657643859cc8bbe214ed12e78729f373e0da7c9575374b524ecec9f33dd6c3f0f7778298f50d5d9a5bea43c4cf4c5ed4344712669583366ee0b6cf69c863b149acb2ad67a33de653edf6964d326eebc1f9360e98aeaf8eadc086735d57e761ee135b3f354352c80f06c1c493f2af425a54161283ed0db3dae26d77876dadbf99af8771ee3db7f644f3ba6fb41653ba6830cc73a05053386e83fd9ec141384e955dfb2a9b0a33cfd0e362245182307b0a3532a0648440792272440e8c6e95e9757d5bb3d2fef487dde8ed45c68a6ca495eaa6b60db350c1b7e9323d146df644a02ee95b678f899245c1c514457422e0c45e6b5e0bdc62165f1bd8fc5c1b7e2827892ef8aab385af96fc25dc7e9cadbc9be8a9771f210d0fab40cadc66d651f444adde80508106318a2b2b54ab5d4ed7bc7d319fbc616c7f68ca3f1b2504dfc5a26e2b053e62699cd54164ac814606ba53489062cc75bc5690e916da80dfa666d82dc276b190bcac9da50541a0ea6f2b643bc6284ec4a534593d247d50bbe94e4f06b8a7a39fb325fef613cedeab51b3d4e03780ec3ae0a096e97ace42df92fd507f03846fce49091a6bf7ace3ebaa99386d5df5d1e06dec5cbfb60a9bdc324cb615f67569d64d3124e6b3c2b50581a1c96446f74fa925acb0914b77e96d45ac1218c26a6ed827d1d64f26a4f0e1093175014d38d702b4d0e25eee922a735fa9dc9b45e5b42b6d6f6860711c35ccc75451e6d558cfb2fb6d63a8fe9db546f137b5d4679083e6f923429ee0230fa7d47edc1c79b577c427a970ccd499ac6f9a738920e8a0d13054dff4bdbfe2ebd8e9759bab26d87ebe6346bc6d9a2bda1824b48f64951ccd6c4d271a431b06c2d6c51b8b97d096d739962d6415188a651b2ed0bc24d6dbf6b1bda15b26d660f4ba46c6502b8d962396df3a5c2fe7396dda6833ac5f1771afb36c4cc1726fa1460317d1c77a7e8024dba6c66be9473693b84f40d99405264ffe72e320699403fc5e883ccc3ae399ba68a2f2c1807a00c6c18a082d68b09c3a2c8798613775f0c1361b045042b19d5ea4155462b66afcd2ecce441cbe8642c7c87252c16f2dad96207308758df0983e5368324234e17dbc3a5111b3a5b089b13c33b6f88dbcc6970b091e3d9da6c824344675fdd87091d783e0383d24f35145affc1760c04f03b9c7ca820b3197a4da19bc9a48547c885a5ed8400ce5eaa59c309e9cd8e75b55d63f924bf21bd88d2e8b63ffa00184e3508da3d28db4100ed5dd96e0dd552f07f6b9c8b97677fcb8ab2f9ddc34fbedb95afb3bfc29c10d0d30efad28e2225cc9143957ab04c1e085403dd4126cc628723243903098b6bdfd4232d4874ef138432d5de8c683a5465243ba72ce8983618d37256600b894eea84aeb8640e313421ac6350cb38b555644c937528872056ed22227e5a1a1bde80d2cc57def7ce9aaa443b3a506d831d0bcfdfc2ca8652403bd2568aed4c3649ddd0980e72cfc388ebbbac9ef6aee2a8d8f679d6eaa3ca664aec7415150e694a3e6f57a49fb4b33b7eda6e5269f0df9d678e7e16083d65284cac7a6e71b2ab42da74ac5da5655b6b2ab098ca9aba98514ffbd93dd0e8c602264ba0c0e47c13256b8d417cf6f32f012c62550b71536f927c13e0f57c54147f65f9ea37f6f1ca505796afe3e52e27a8256375b31dbcb6cbbb8ccc88bbfaf066bcba82a9e6d35fd99b68596679a0900b1fb2e53d31d6e7e98a4e499fcba53c43211904694efd4af10d0173bc0ab0e3460d96c1fdc18e3f8fc5479428022208a675d11665b2528025e4e4137031dbadf60fd96da2886a2fd6d0165537b52e616c6a532cf88d28b11ed58186ae9cb1f15e071b94254ed84d4975a3ab02c6d6d6a542fa1315ce2ca34654be05c4e78783f57d913f34e5f6c6b5183ec43e9590f736191130a51f7cd2ab6afa47b4de85aeca15a395810980d18acf6418ad6a273f3f242beab91c9b29dac2843daa7c8b32eb67d77ccb943a3f79f6b72013ad3811b0dd1cbb72eb91e90a626aba036098b2990cc230c0c0a2b49d2e580c6029dbaac3abb4b903ebf4cea53e7d8dee09ae8f383e932dc79b7d0aa7b993a11d6b0e6d6f3687bdf04cd410f4a273f7e080bd0a56ffe6f80e64b0ebbdade46a18ea1cf1263a244fb02042e25d5a7531653a5ea86c900ca73c638fb10a339c4b62537650d80ee2610694fc08ca0efe9aeb0d5bc55acb1f9eaa54903a283bc1935e4808064d99d9a0c161c5761f6b7b5a3b2794a63966b4eb6ee302bd4b6ff28854bf5bd2761f29984e3585b607a80e838f219dfa02353de16adef8f311c8263c33f38dfd2541c410d7535f5e11d8d340e46528deaeb32fd1da35869244fddd866d0912c1e47c9510dfd5fe3e8c70b02969057bb8290daff476c789a60e16d6fcececb9ce39c08dd220d4bad1180449ea0b9ea43705fa92921130140f1ef545ae4f15f0455f12d19170615e6a8b48babac9ceac7d1e281cbec06adac0a5678eae374f3db50310a01dd56cef7ae9c6654b2dc076daa45b69135c8698fa1895cadf785a4695b2684a82a7657d01dd691953cace7ced362cdee4e727ef8a37ebe8b6e83a6f9510917974213f6d099297ed33b5dbeb6f44b7ec1e11afa07a3dde5d8f4a492faad39e97873f4acae4cabe4fd6ebc527d2d175477182a078f56d7195dcacfb6a9ee188aed384c087277d8e23bd48d2e4ed2eedc87e42d67897952cd9cf38b2cb8460a097c92fc8ca2ede7624ff1d47f23e4d6e7a59fc0d47f4368fd368d593fd1d2dc29ee6c4800c02f3242e207c1800c2104a303931e084a785d07262800bcf4104cd89013542fd02764e0ce0e1a905089d18302454cd20e9c400259e9207d48901513cad88ab1303b024613306c100afab78bbabcdebe2eadd9b4f3d9d015d02dde20d6941baeac90d0063c8dfc7f1b69a935a5203b26452a97203bacebf6ea9c0e845d2664bb5253400eb225e45eb3ac6d2e2554cefa9de6559490f765b060670490c6ae7a8a737404ca2a73a67e90d3093e87f8bf3aca7febb3c95d793b66622175360fbcce252bef001a7ef2a1681ce580bb0344de030144da3881b72e6b2227bc35039678afee4a0dbd34a093546afe23f770431cd0d7a4b2d37cb0415bf46c1fc8ef4788eda1f5152562e3b4ed5cc75559c963b02b122a391fc1ff1b28cd1ea7ed5a607c3b9669c7ffab3033cce88ea6eb3fc9be3d0170e20786e5343a2ca828845449d13130b079a941e0b016a1e9325da49af128a62f5dfe532c33ae5976d06429c374e1326e3ddf012eb7cb346cd65c26a73398798b0b834e753ae355fc73711e18c05ec1f77491963f1fa6a1d2defd1808dd2d559c400dc8058e2eac6294760406d4570997c8dd71c95694990ac1f18a01b90fb365bafb0c865e1f8379739b6b21dd53eacf72648c5ea88e3380d1c7f8bd79bb87c7582c5e3ab6cf58d296d0224c1e396fc3f436140e56594960553dc00cabaf957cfb098a4cd674a9bcc286d0c53dc04c675f610b3e5070664372d04c164c7ed48e43b0d322fe36cbb8e2f22f4dc5e13fc41fb8bf6eab32f59c95661f2f36879be06d31a9852fcc797e7cfd173fdb72f597efba18a7b8c43694df129cfe8463316ab35d1eb3c4b56e75b1a5e6d1cc812ef210858099fa39ed734006df67e16576f3f6221da92bc415bd096e2e2fd332c48fb76bdfe19ed8e262b22cec50916a54d79b4256dca3fc7c2b329ff131697d7cb288d73a6fda65dbcba7cdf7ed3be5d5dbe6fbf7133b826e83b60da04664796b8e98b1a5af54228c8e8aa591d711ca7196317c93a29a3fcdbeb28bf5f9c1605759df9ed6d831e3806e0f6b80dfd5d56deeed0b305472b6c6e1bc623477a45370bca0fd12e5dde3116de304439168d4d907898b6511b1eb5530e2bc0b491cab3805460da4a1538084a306da4f2d4821a0cc35e205628c2640b042e2a5d188f8996c94da21e08c693a28edce994a8a716e46f3c21ea28c5f31dd34ab02354c9ddb4add131508adc38ad100ed5125c2173e334d3d1834237ce3a3db9687a8cf34f472a88dd745ec4502ae46e3a326238a8046f3a3afaf0f1fde9e222a2a1443ee5bba2df2e319d185584af3f9d5e5f5da30f8b2a9a57711aafd7c9453f639b8e882ab2eb5dfa36fa9f16074335559911efe0efbfa08f8358efc0e9f0876e7d557b8e41ee7050464702cbf95fddb85e9215d2d7e7d8a9bf2efe1376b66f8aff0d3bc5d7e5ff869dceebe227bfa8a66e140aceffdc25db4a67d7ebac7405018d239867f438b338fa2d4a6fa3bcff45fae1bff17586799c312c4cd8ddf0931f4da52bbbce94b7f1f37f7471f4cf9bddfd6a379b88d4ff4047e638f1a14ebdad87f6b3a3e21e3beaea3d3af47661b37f89f597e9f622d631ae3617b13ef0ab2c630a1b268bcb3cd9107753381933de4ca1ef2a653293737bfe75bbce8ae4216616df2667b2a7e917e02637b2a7610cb8411d1fcb3b6e53c0e430d6e5993619b4529767da63318b8bbe1f6ae8b319cbdb674a6e369ce57424b3e5cc74f37dc4557ff507fe947f43babb628db0c15e642bfafc3acbb106e603cd239cab4c8c85e66ae1d6014c7cf47624b29b565f3517f43e7d9d5557a52d9438c5f7b99d30ebebd976d31fcf6ce2a9cfc653a16f5f5f136c56d7c4b0e8ff9015a54c64bce052c4514e1fddb2440693cd113d53cd5eb8a32e3122aa9dc21b7231b0eab0aa36dd5bb3d1f4d5ee3c6256c1a663addd599cd296a2d5bbbbce76255ab19f8bdfb39c296e98f6fe6d8b2df9b9e0db619ab0cf98733843174f77bd63e672c34e8888e1e77073cc263638a7eb755224bb0d168abf25b7777952a077d0afa394f4883896f82b00f93d7bf7cee04c3650bf2eb1e8a5c2f75a450331dd2de1d0da2389d3e4934fd92597442e96cb6c1bafcef22ac42a121015491dd918bdb952d19c7f652e9f1960d1dce06443617f6c8335639102f2a87725b12bc2864525550e7806cbd6d0911503476558905ce6f136ca638ec463baf503b94d707b16fc63ef0dfd91f1f6c680f80ab86c7917ff55113ddc52c21fb7718dca020e47de0854d0c03e78b65de875ac51e1e2a563ad0a6fb606bea20f4674775738c06e7fd57db54bcb64139fe779865ec537cf126a01f09406b5535cadb368d5080ae260c042cba136551003c3ccd2684fd302c3bcd2305037c07436baabf60e6e2add8e73f74b19f9c3715e0163bc702361ec99e4f4215a275ff0975388eb5a9066a09fa75c6fa38d0ae4b85d28213a8add2a9a153ccf690f4e75ae764591e06f869ea7b7645984de1f794d3a56e07746ce68969942e9a3490be8681b71e54debe2f7591e47f8b531c155ca7616b7403e2d8a6c99544069f1cfe6996c728db15916055d128372d0042d3010f6510efa483f10cde1c105415a42232024e537aa64ae4652e7c7f475bc8ecbf8a07ee1469f4615cbea05af2020d2e5957373bb488f6073fbf4507c6bff496ac4557c13e734326844b362d03548929662972ef3245d26db686d294c81cf8121456a6b338ec5fa8fbb06885f5ec75b1a43272d2de516ac655d0304dd9ae4fae29801b71ef3f51b4953cf9a903f2a40d93081c6829ade8c31c756e8213ece88741a1e2ebd1c7fa8b820e2910c9b3a70a2642ddd879096e124c349df22b7d9e3c7a323d9a7093d42500db7c4e1ab2cdd15c30d1794f6314d56309a780835fd601a56e56ed3831226520f853662dd28de95b689c0d8f019ad1ee3432f430ca0fab875dec8d74bcbb731a381b98f180305acd503464bab86b648666fe1f5152b013bc298f24037aa5323831ca5e1bdc17ad3ecb35d4ed9b671745050d791da1a7133beb5b529e16d1a57a1dd154c2b31d8e8db1dd2306b55856996c06062ecd6c18d9a5eb07f1880aba453a396e36e8d5d75857b6a978d1d1ad9261b358a694f4d37274c5bf81d4aba5130fd187c0d6387a6c4f49efa18dde3ac7e214c7fea360f541033d0419896496c306daa10da0c6972b1701d9a1db6911dc360a97bd42f4209761584606f1ee30189064cab2aba705d18610809ce1aed80766b45511e1a3282b73fcebe8aaa7d8e232cd0668a416a38f741e714a3c16e908f7f4ba631fbb5f170b0fb1ce128869fafd180cb8a76afcc3fd8bd7db3ff202ac277a22a3d8b69a0c55cadafc6ad2372410e2c1c3978c141416933c890b5efad9365d7bf598f353ba04cd19529075bf34fe7d1c6d38f3edc84eaf76ea16ed9b1d116ec96fade8385bbb247e20f86cd294b3ea38f0845333433515f760fe7237d777d16d021468b1e1398d6891ca6be58449dd6d3b28c967771bb9357fd86dad235506bae0cc9840e37860cb56b868869a44e3f40acba38d2b0b0523aa64df5d2747e2300316d608827c0ffe399216cfa3839fedde68249edbf1a0b556faa3511d27532b3d0794d663062fc26441bf67ef18eefe35e2ce0f1b079a48b78eec8f45dfa40b8922619cf4ff46473bc9c2a37d26b721ae27aaa528ea32dd17112db839579bba5d0f4a08999b8e8b265a03c2b13b91ae622a5bd6365ac5bb92d25f6717653895d0747f5abb01a4799f79674ba41d0470a657c44e3beac9e0c04bd486185777d75fbbe8440f56e2c90a3343bfb6583dc8de6179355d7d38d806cbe3ea509374d1f738435d835d4064c51c4657060839a45392e0dab99792e16975cb5b4a3fa2c8fe1c22baa53233be8280def8197cec6326e1a5fff749eea60ae2783102e53d8a0db501f00ecf6f33c818deb0f063f359927a671eaf46dced87b2900e450fb14803466b89b02b452390c74c36ea097656a214eb395a216d71e58e926063813b8eb7554468bab7819270f667063a861d71b22b4f3bf3155ef9f5f62d3adf1f06ea3674cab5acab9e1fe9ad6eb887a967654cc73153f16c4439d9a1aef9086316daae96684f5050b403cda169c303008f74337571f046cd4889a23c4a19e6190d490058737a4598bf650b239c11bb7a1a2a71b05e07bbe8982ebd084667b5f374fae49f3c95fffbe8bdb95050ed3063a08d332890da64d15ee1da6911d1a0dd3488dee01a6abec434d1798f8588512cc2a0208c54cd991b64c94ad0320cf94e57a3eccbd1393dc70133b25f7c4ae4944be0d1919b4280bac22b004adcee62aabd83b636beac96856d6a4b57d31af3411dda2ee87f68a1f545889505a6e4ca32ab64b6f508734a20a0961d04049438052210cbf268c05c7661061012915574272d4c31145dba683a5524ea809957208814ca5443c5b31023885a4570b9afe4d0b4f15010450a1ec482055b610806953964d7a37105c4d72c340a5e5e10759937cbc5b321e6c59bd2d9af5a1f1e227825603663ba8d8d5aab2a3431f2a5bb46d2cbb6aa1244c93a83d7988a735b5ec5441fb85593e49c52dfd00e3e249aec0cd4a8603a3be59186557ed0c37adcbc2f76bc4c850a3be2fdae16c0bcfcfddec5aa63292d0da6c00575394d0148ea6288bf9bb996cebff48d234ce691f2c2ca04834901d94aab141db303650d5a43197de0625609ad2d3cee228a95db2319051e1434f663a48b2c7a3a1bea99c445cb3c6328738a5ccdf3042fda8b611d487416a12372c0e34796b9a0940d8e5ec35188c79e9614053910f00615e409e2d9906bed6a7f300dd284675bf0f8c901dc24028f8e9bc46a3beed19fb7d0363900c90d6930d960347aec9c9ba567ec2d498c6750c6d15839cdce3d41aa44d6363bb41057b5483829c4c3718ba81aaec0e9b067a79a36e17cae50c0e4cb54ad02ef0c4c83cdd529d55bf7c2e48f3cfd651b2513bc1707130f8155f72ac7cdc70fba0300f7cc9aae05069b8b542c320c5371e95562a980650b2d960f443769ba4788cd6c5e78bd1a67d668c560547c2282fb40930ca4b65ef308a5a8ee9889cf0aa8922a8abca7215166e72c7086002ec0db1a29b0083b473783359959eaf95ac9b673692b4dc48369293d80430e544327f0bd9c4d57a4bfebae8cf762fa2ad06a36a1a5dc055bef85880d5b415806d552cda62176a5e615535e2c3c0a6250f1415552320ffd68cb698a7cd68967f363bb01862f5b25ea2b35fe11baadebb0d599b5e8db62b6ba3e53d702cdeaeb32f840bf3aaf17c95949909eb7a3208e512850dbe0dd54de6e4e2da351a36715ac134a7a69b112a51c133f46423a072bfa365e0fa33219af7344606e3dcd39e2da8af8e5abcf5a50d8b37686534fce28d69de5c166fb2c4c65dbcc922c1d44fa94642673d78ba4c9f4d03688c8dab789b15d4ea13cbb4dc1565b689d2342b2b0ebf922e9dad73aadde2e56199efe4b70094ed755c76b7c397d92e2d797b57ffafd21c4a769667d92430e99ffa769d2894aca1d2c68af857d412675f9eed7d6f03effe5a384a2e447bbb5a57d5f704640f16c54afe4d9e10281a74c91742f16d639296752e2b59269aa0dda80aea8168e02e27d532f0be264d8ef9d628995b355a4ec70ba9124acb8c693197ff1a468998b71ca945d2bdf53a5e558d520f4828779471ec30b6e43227ebaed28042392231aa0f4c306888b51cd1dbc0b50bde187fd1184128d6258ab18123567d525c1bd07200c189ac19d35906c7bc0eb565c233700547e20d15b2e0ab943154c8c097bb532c31e4be623829bbcc7dc573324c7bca92981ae8257525d3fae2bf810df8604ae20796c27136734573acfd7b603a104f5b4c860fb8c780e0dbdc774034b2396d37726c4ea7ed9a5b1f511979d7eb02036b61475de2297cc77063ce1094f3b65810c157d8c2554c504049136f71310c8916d8cdc04e7c9bec2c5b41e0174a209b8b503ea87866d5023acf9db7c3c6fc393c60c8345e3a47232eb59815ae81b45b8b779d16d70cd23ace8279bb928699338b0771cdce8b0e2156f512896d50ab7059c456f46a89d8b08144af5d176a346155af5e2b036be343549400069c34a3e7e5242d2dcb9134a66fc3d863aaa996a9a37a2eacd494a2bc491230196c9e80ed18a3a415fc01695a68d459a8e2b6057f28ab94ad9ecc24022db51adbd06e8c51dcfaba400c1bb5ea0be1b35d4eb719c52d010390612a24dc40e2e0a0866b5142dbac506749b35b660b7eff4c25663589a9f74a4a3596755b7a56358c8c60ae6e9cad509358f51a67237ce43a8d6590f71cf9bc90805c4d24ea5e1b2821b9823ba71ac19aaa00040becfa7a8b55b0415d0a65409caaa2ea3e2a2820f129f687350254311f497080feea44da7680e469ace0c2910e0449be8e9144ab3a16310b1849a95921a318a8ed2a7bdca35b88e3aa995adecce98aadc005520751f01c8613b950cf68139ab241e20f6ac7c196858374604ec36943519fd237160e7043ed84c827858bea37e3b68781d0b8bfa0a757cb5d3c0235ef64186a524a5cade1a0e2d6231f45e722020cc683c87a0a70ab6f3354cda9261fb3b541509b07be99894efe1ab1b8d636d25ccb2d9ebaab023ab7db4081dc729008836f6ac8358c6cb4a5fcc267771155be79e96da43489c0c4c0286cfe668951e4c6fac613bd94c3dcb42a375068baae25549b0bee668d4eb47afe138a944b0b8f93294f62d36930077d48a9f21598fc6dddc808672a705b7416f9ed3183d6b45517d4424cb36b67c8950e48da26bb3ad76f647e75a6cbe015398d649119d5a51a028397ab9abfba67f01e4cd9bdd593bb26bf7728ff41939cdb4e674e2617932c1a34bcd659a605eb6893679a35c2aa0b8e5a536c93597a70db80c9548c17b9fa8d164606e053ad61c40d3ed39a40d85caa5c949cd5c9750dfd06d3eb6aa56b2b5930a1aebdfec208d6e84cd8e4753575dc78dee72fdb695c0753a65040b056c945b95e63d38bb2bb03c6cbdd56358c265765b64a40a0b8cc965c3f8db92d990e6a2e8be3780242c35e15f7139c0989b8ec8aca4e9ab0e722b809d12626ef53094c9be44fee982acd9f28a8e60180494eaaec7c38b9bbcaa65108423a724943872402a58410beba2157dc604252663403c484cb7ec675cb98ff8ce998e129068e2f2030ccc31167c169736aa965884fc505751b958ccb4e02d6d5a89019d6b435238249006518beaa5451cac106248bf21cbe4076a8c1d108a63032880a4e75a4ec9694ecc8534c528a22d4bce2291b299b8e4146faec3bcabe29f3ef78ca4c99706728d91992c01816090005ce8397094d4b0484209199620633689ae425483942a94e8c5d14929d84959f90a2c46afd164684368b5688c4aabf368b563789cee05d809cc4c1b4f16d48fba0de9456277eb0ebb24d0d636195c78a9c3cc0245453be01759f351907428955936960b0a58a22e63d749506111d9fbfc8a28f8fcf74a979ffadbb14a38f65cf6a007e031e5c50ed5370b3a0a010eddace0941dabd042504541778b59d082e1dd304820f11aeed9d69eab097d73453051cad1a21b7ba20be7b7598e42092aa59994760d397601701a198c99a9b7fc610cbe0e53b5d9065a6abfa780768ce8010f5011a5c274e43a05ee5ec6913e01798df90217e85ee6be33dd8d732da6036448805846c135396eb3832aa2c2b58202c8646b2c838b213c8d474e46e1311d5d065d341bbb74ca7395e87a36cea271d55384ed54c0104e464fa054e0d0856e1271d1ac193f2ea5e9175df5e1c5f2fefe24dd4fcf0e2981459c6db7217adababd845fb8158eb6d92de163d65f3cbc1f5365ad20de07fbe3e3cf8ba59a7c5cbc3bbb2dcfe7a7c5c54ac8ba34db2ccb322bb298f96d9e6385a65c7cf7efcf1efc72727c79b9ac7f19213f80ba1b55d4d64681240095f49d5a4a56f92bc28e90d822f5141d470b6da48c584a896bcf03a61b795a90357cafaa43fd3a8362d2dfd7773db2ebb8f8eaee3fc21ce8fdea5377944a6c9ddb2dce5f111df9c235d15bdb4df1001d0d3d34a163183103d396170bd8cd651de061b9502f39e65ebdd263585eb3571a2ff8678d5bf5b70cbe34db2232a5c1129891c856f20d7b36cb35dc75f05b6e75fc95f499cb691731d75d47301aa5675e943fc10aff99ef48c16cd57a82f2a866c67145cd92236acff88e37b047bb118a889634115786d51f6afb27457f828ab6762d1ff7fcb9254065ec76ad17fb791eaef5999dc7cd3f1654bd870a6ed39a3764ad7e0a64060259d45c59d8f7e28fd2bd225cbd17491a5f137beb794d3a2f9dd4678afb3545249c5abfde02c31b0baabf836a16b253a4bcb5890bfe28d661df4a84c1ec814207396bfe2390bd151c4f902f88ce75d4ddc97515ea6f244247ec37315126eb24c0db938d53c85f4b22c4f43e6d98a273c7eae691899a24c963e83a863d247ffb51800ef93f5bae0fbd3315c341fad06541cad94fc9a8f36fc885b4b68951cbbcf363ce961a7b6df4c011bbed77799baa5cd471b7ebf256a76f5371b6eef52e280132ff275b4a9a241c27cc55236357cdc95b799b106b1949b857d712c38c5a2637e2c79e6c2824974fa514b82e6013118dc6cc8a5011868de75a18063a677f6d99879e00a422880b7b4a71bd999697fb35d928458dc301da1fa1359029f6d676d29faa05887a6985cd74423c3701f39c450083408fce01f020ed3aba88f7339b8aa54d714ed34a5e6825054b50ed268aafb3e0fab10c602569dba8c96f762dbb80fb301676d9a81d88a434eab52fa13d729d5cc48bd246c29453df15fa698fa546b3eb7c55eb352bc8ad27b7009597fb09b3c6566fdaf784ebda4551a98dd403144fa0c37447415a187879e89721d0f2fe11d56ef218704ed893c20da5ff19ce88a085824812ba249a1c607d4b0c1dabb82fefbe3cd7fb1701c942182feab8b03a1ce1785d6534525e8bbfdcd624faa8a242842b9fdd1c2881284dc1241414e87f86de0f101ef696db25572d31c572e4edc36b664b55d305c6db6b74a523bdf29be7d8bba80cd2ec743b4dee9793625026fb673953c9bb9609f0d20d8676308f6f9cc05fb7c00c13e1f43b03fcd5cb03f0d20d89fc610eccf3317eccf0308f6673fc1faba53a1377773b2b877bffdd1ba341f6f58764339383393ddfb449155c8417084d77722b53fe268ebb1e7210aae66f79dc8ce108cd2417c1dc7b94ad06bf9c804281e69ed2806adf659394ab9809fd68d83ecab8872167942df676f2aba0c0e014c45c76bae466200d95ddf27a1444759cd55728ee6558e873d927d0db52be7b61f376f8bf8ae38254b93e4cb5a58beb0bf5b1c71c8f7863597855537b26ea2ddbaa40340eca6f069848321c53dd28c461f71b115dd18a85858ac30a13356ca63a13b68552b1dbc474ad9f59ff67ca9ca0eda6a1ef239d7afe8f778f4cf472b9b4da617a4591946164fbac0e9a2da77f1d50682c9933e50fae896d59e2a41f279d20a4a2bef1383df67d48791c39326509a6897451eaaa8c89f74e1af8b7a07d5736060983c6e7d38aea4a1cc9cc38d39be26979d0d0413a57e5a124939ec87f1f7272b121d78c00216ebf220bbb17b896e3897ef30f8d6a7794760dbc460a41d89d749b1acf701bc76253a36363b13f5f12dbf51d3f059b41fad9ed091059e8a5df5c9ee757875d407b36b3feee1932e2189e4a0c7fd72fa49f7a75c185e63cd05f5b68ef812a2fd716c5b5d557cbdcec48714fdcfb3b1d0eacc62839eb00839c99c416866a4d45151c4d2fbc1ee47bcaee97f7926f52f36fbf6e769f4858c7f71dbbefbd986d7e7749d2def6566fdef539cb1f61b19e278673fcc6654c8890d071a0d624a44978160e6a1d46f4f2a2999ff641342a72654f09bc01a538b2b72697f9b0de2a06c76c360ce9cfa0f013a0c93e1ad4add0ae9c972f72b9e131c79471f6f47f53a73191742ec84f6b799a16d04987902cc165a4e80809761a47799db128cd23455521e162b1cd997a01c166a874209c22f5f728051fdb30da337c4395cc54034b48a1ff7d5aa7dd580f824dddeae5bc97cb48d4675992762e0b08a27f3ed291095b8e1304c20aaa95c3720cbe150ce9b589393f76666a2c4404d20bf52ed7eb6d804ed73612af8b59f865c3ecc0633754ed0b17003a55175c20ecc6844fc809cacae8c49a6c8d6017b575cc5d14a5e00b7bf5a2c1caa00c2d2d2a1fbd5e6957f9d58598eb3d0ff3e9bd100a418196620982bc29c032398a8d4e219594fcd13e436d5fd760afc6f22eeaa9f6609b9415725e68a2c2167bb42991f3caab894d21cd0fe88e7d3a660800e28c56f36e0bd25a28678f25f6cb622e3e5bd3ccd303f0fbc6d00af239acbd52ea0aee07315a5b0efa8d47b928acb64d28245f5b3cdf2e122fa0af2a13f3bafb5f6d590e9f30786b0609a1a10a64b4b3db485a812890a6cdadfa6b6334d661825d3fe9385a5c9636205c4038ff647ab703db9c4a7fbd1e2e9449226c59dc8a8ffd542933bea187dbc7925459de5bfe039f6e94245f1f35f2c660eba39f529d9c4efd2eb7899a5626c60e8fbbcecc8a0ceb7a606ac1db175b7c305d50a6591023e2205906b8f59cb4757b02771fa5794afdc3c0946ad3517bf1dc98a87cb96a42a0944cdd035bd842ad65ecd551f714fcd5575fdabe66abc04e6bef3fc14c2ddc8ef29843bccee7b0fe1aed4c19fbb48f4b3ea9fe6e8198c10f71a599b9dc7a0e7a43e3354867d770ff83e64286536d6b71c5459fa6ab3d76eeb474d89556a0e078527adc0159030edb04e9eadf33a91f2ced6bba28cf36a81ee12e5c1e2b5a4b926843e515c86dd9a689a20dd71ec7fb671e357abbcba35e422d2dfb2a26c18383976bf55611184cbcb94dba2fe6233b15f66b98257fd25f0147eb72b5f677f01dbb2fc17bb2de87dcb9c55d1c5a0a5113e596d43cd3d6a743bd246b157feb6cade4e395a18c53c447c90b4eabd3817311facf975760b60d97d9bf200681fc7b3cdd853b62ed9247503623ab5091a92bf5ab4ee2eab4deb551c15d21b2df92b9ef3e7ed8af492767527b497ff321b0ba4cae21c24ca079f8dd925c487898372f92f5ea7b11a71edfd468e45f7239ecff9264ac45cb6f54f963c884f7493e41bf12844fc66b1911c15c55f59befaadba0dca6d26735f6c2e1f2d7739d55b196db6e20d24ee93452bef3262d6ab131aa191ec07277e0a89c2252cb6d5ffcade444b8262f08994fcd5c21a66cb7b62979a74ce9fcba5600ee5cf0ebc81368bdf6c5f16bc21108d57e08e82f4d9c2ce165012edfed7d9d857c1829dada364b0dd05a82e7fa3ab60338ce5a5158a1cdadf6cd6b5a4c9f26d7de6674b5eff9083acb3bfcf066d54541fb2db241d0962555dfe1053b0519bab5bfa3a227b4856e2c4207cb27a0658d1bc17cf04b90f4301791ec6e92a5b0f97e550aeca1f373097616d0bad535ad965aa97a21329b6b90234e8c52e6d1d085d1ae8d5077b70e2577da65715b7f60296ea6296ed0ec556de27df5aeea3d6e7228efba80db1ffe5ca865180eb951da7012e58eef395a916620e83d42320355fab4f486a7dfbd54a731e72c1ef21071b2cb3b9893c9b207df494d1041023d2304cc2a26c4253d03cd21874caaeaf50196b43a806cd49bd5d5491c98fd5ba9fa7b81549b7bc9b4780505c04e0f3989bed53a1739d7d89d6233c50962a72f12acd3c94e638e0f3d2704f31cf57543822a7fe57bb3319f154427d1ea186717abb03c4c4fe3ee767b8534768da646764fa19d4c077b5789a77231fbd413e03a65ffecb14261e6a957d7b2a53ad389115bfcd068323edeff8efedd8edebf8ed38cba708939c20b41fbbdb4f6291aef6e697eeefa2fd810a9498cd2afa6fd1d35d2fefe24d54f5b6d846cb06ee6f92bc28a90ebf44455c17393c68f73a890cb6b7c59febfe978b284d6ee2a2fc94ddc7e9cbc3bf1ffd7cf4ecf0e0749d44059dd6d63787075f37ebb4f875b92bca6c13a56956bf4d7879785796db5f8f8f8baa8ee268932cf3acc86ecaa365b6398e56d9f1b31f4f9e1f9f9c1cc7abcdb148deb04571f9f1ef2d97a258ad59fc3018e7ec0964d75ebc8fc575d58b163257f18d648f8e051d8be42f94968c36e7e5e16e97907f8bfdfef51df114bebe3cfc5f15d5af07effe63d112fe70f031271af9f5e0c783ff7d78f0fb6ebda627732f0f6fa27521e116aebd46785d7f197f2d593664809bb9e4f126d96d9ac3c68e534210561ff1dab1ebdfc72c98d5fccbc3242d7fb2ee21c38c7d771386e31f717c0f72fd9b3557caea5596ee8a45fffc5a23474b9ebf676572f32d3457dad2b33ed673d5f367d6fcd8b84f1e02e4423e79f0912f50f90a4c5ecbf9723cdbe5399970d8c75996d643e2a0322398015b4d1e97515ea6b225b3e3c4bcb270e81447edd321217a8a6533386a9f66486fd13c602dbd430bc2ab7b8116841bf3f22c08bfe6d559105ef593b320acc4576641988a0fcbd04c593715e123710f7fc09bf9361e93f6e513d6791298c0d60725ce3683a1877730ad3f07bcf862fb826886c4c1ab3df5d4d7ab873994b56c979213dec0a2911e0ae3c6f77766841b141a66640372f5ec6ee51f7af6b7e3c176b8da25781b1385d02824975159c6794ad7f871bd76dbf70113c0025572bb8c96f7c380a5b6537db401f74573cfc305293cb587d19fd662cb3ebcdb22aaf6e7afa2f4de8b0f35b3de4cd8501443f9216fe89a1bccdb82039feab9b11978f0ba60cf404785e7507b4be8557bf3f2678079acb98ad2c84899cd0f8710385383191f2d9d073aba2c70ec20c42c806bba059f50cd4b59626e36cbd9d1d0a213e73dcba986ce265b2537cdb9c4e2645156d7c73d6ca5c0efa1be415e33bc596791c3a29063f92c70139f856fe2f3c04d7c1ebe893f056ee24fe19bf873e026feecd0446b4bfd29ca6f63c65e3fd9e9273b1dc64e8bc8726887ccc2a245d643c1d75d51668e05caea13ae225c6008f701016bc9ea5d714a6c5542085a365fb26c6dbff6f13e6c7c1ddf44bb7559e5f6f6eb926a5988f2d14d673a38fd6405cd72eabd1d51f17957f0277308fda0079029a7336ef86812779ac703473ce47695ef8403a482f60209985ada71ead1b5cd65fee1ad3e5a5435d98ca6667d83cc92410f245d0269dc2052470b358fa1401b6152be671f73ce277b0ec1a9cdf33cd4beac31abf134d6d0ed34aecd7f6ce9b735745e63d6d165b4761315bdaeb32c0f05127dd661e4610f9c6a1871c2d3127a2083fed7e7c21a939bd8c35f6493127bb099769dc42530b6bd44d4d30eb132bad4670646de33d0250246dc30e0c97d76fcfb0cc2b67a6e29f7d4a2b5c98ac39b33531adfc77d79b7cf0b6b597b4be8557bb86baaeddbb9a1f0e18e0c55e65d333260ddd80ed93e7baec75cd767cef564c2a5cbf555ba942ad767ff4748918b3e7386d66a135e46b69819f55957b147e08a978c9833f08ed467fb9ccfcc6a69c538ea00af20c631e31e3a96b3a3ee979e7d0c5020a3d36551f5f0d7fb57bb96686909bdb0caa65cb5acbf271d02afa644a638ac6af3969af1aab9b86f29e72ea7a5a5901bbabdb14710c2aa9c84e137ea4d79472d10e28c8dbdbeb6d5053db79d296b3a9fa7286266420f6f8d8f7bed7382d8a73cf59d18c22d6698d83f1e326222ff78497aa091ac4cc3897448e0e0fd0877041a00b67b87cde37ecb315493cd640809293d7d4e67db749ebeb8eff279fa32eaf379fa72e273797a08894fe169091c96d8073e50f0baf03b26cac4987b71ab7aa009720f5c034770ea61e9746bc67e1ee7d26c7a0c5339bba63733f025832bb34027dc4faf829f5e0563993abf0a0698d68924079d72d49915ad5f207a3e3ef4bdbae0f16c53cb4d0cb761cfcc3dba8221a2813706e4648638adc3290ccdea86e6ccd93a1c68511af308e2840a670f340b155a5ed9ae49fa6c60b617125a4adf1b384caa40f73d6b3e4da0c790e533fff92e8bf621c08d90e4cff602184bed1b80c5c68db41da43e035499330f71410f1e60d6b68f49b357f37988f2e55d941f1e5c445f3fc4e96d79f7f2f0979f6ca52e26db0bc93bd8e6e33e8c225bf8026d92b2ea79993131919e07333e7f5ef811aa4d35877c98e630343dc7649794cec3d16f52d2a947ddb39f7fb11d7662663a8fe3573e219dfbec2c64a17367c4259f0bc2268ca0e4ec721ecc807c721aeb84e9af9848cea37140ea381fcb52f01131bb2160f23c5ac2eae71f0ee8f5e0e4cf1df9f08988833a1fea3114d05201f9d9c63057c3bc776af331593a803599dfbde83e399cfb986693c261b9b8ea1d489a86bce4a8cf9106dfa956e74003cac359b5109728f996a967a49367f6d31cd789b0acc742ad2b54e478da38a4a8b2a30145e1146866954f37e0db16db5eaaca2c6fe6a395a649888634e8ca146808c3ae88d417dac0b3f977d87a1092ef493d2fd56c3db767b9dc6821f8f85efb18e5a41893f70b8753750e26334e55f8d99b5b3b964ff70c2996700257665532cb9b21dddb6b7440daa6a9f76ef093823e9fd19e5c089ff60a759f2cc9b2f696d0774bd96ac70a025b9f5ac96bba99e456bea5c1bbd4251db279df0727f5c1bef13bf33fd6828d9eedb2d4bf1d623ea281d4a7cddf33db0d54a78d1fdabf09367e4cf930a7f3be86f2def131819ae5cbc79b66f8b62fd5a70e3037c48b6b67a934c1489e44d28ba4891af324122a125a078d65f6248e4e1c55749627793023e67d527ed7f2e883a0ec7b00c7ef337029dea1ada686272d3f6e2dd73ed193961fb7966940bf271d3f6e1d5751789f74fca8754c9ccf27153f6e15d7d13c9f94fc98947c5a14d932a9aaed36a5e5947b6c6a3541e9e7e9aada7555e4ea6b3a4a13911fc1052e76eb32a11bd4a4752f0f4fc4ecf22f3ea6afe3755cc607a7cb3a6df959542ca3952c71d2c315a26d1d78154debbff32dfb27a94202d498a6fa4a227aa1969ec12772eaf9cb3c4997c9365aeba424108183c198c2908aa0ab4dfcf23aded203bbb4d40824582bbaca04059904f6e29881a31ea5ea14a82c56c1bdf8fd40ac26c72bdb425db1c78c5f6c0adcfd85b122cd2a1ed2ae86eec7a3a313092b7b054c578ba6c8953a263035697a2704680d3ad6b27e480ab527c01201a0683f7cd7769413050619aad8c863a253d786d1c02826bc5da845396b4c4a897be556c9451e1d16f5e98be78cc3067d4dbe5f6dca9601548f9aa52de11e144e4e8ad525a276009745cd821a27c6169b5379a149b03c67fbc6355b6e11fff9d1d935b5d21475f799a0e782bb3d9d579f706785bb89e7523967e042b3c5cc2c74a1649ddc4a172c300a0e813c886ccba0cfc32c898d094d1598506ed23b6cd35b61d8944152d102f80a93737347803def2e5690d72ea905f7928593f8699c0df510c32f10cc75aeb702037a370fbf8bee3ac026773301b35b0f7aa3dd950e0139adcb5f4538aad72cb3319bfa734e854af7cd66dab5174c2f3b09725509ebb1f89db1dfa0ea1ad83eb9d0930fa197e214ad9e7240b429460d23624e2bb8398e806956735e289e784da744a4f883e930b3e1d313682d3654ecbb45ad46283e2baab1102c124d7d422f27f3ae935b23111c0419c39e9002e9ca810352a8d4a0f875424f00f42293b82b1ad0254c9f19661f91d19d376ca733bb7ec09dd4d6aa855701b7f2cd91e89df3f22ed8c8faee97788ea36c0f1779dc958377e903e14a9a64dc369ee3da6e6ef09f767de708e159dc80e97048da7793ace345f702db60a5d54141b80d5f7529fca632dc52602cc84506411b321e8a42edc1fc03a9b7284bd83db09f0c77756b7b37d6eeb078f63eadd83d1ea7f2d7c7eac34a3d9dbdef2a23b3f905757f26807d7ad490f4305a44553190836c043cb654339ba4f7f45ad77ca6f029af77390d81899dc50f7174bb8beb98ca8d6dac7f3a4f75106ccb4810e43f8c0241b90b408bf8cf83408feb3a46f5358127f480eef9d63df692d9d485999a3e6fdc81cf04f6c8ea39426feae5714ee3bac64c70e8d751192daee2659c3cec1500e19e083e205ce431cdbd701731f5b73a9f1b16ebf8dd4f48fc7e90d8877a9f090e17ca1e0ca1f6d1606968db507074078632098127326d2aa78d9d1334f76f913c171ccec42cee839bd8a470f9f75ddcbab8fb873bb90f5c8ba0cf8f087740f7f60077553eee06704c2c884209388680d32ef7fb2870636ae45aae68155f6610e0b132c04d78b4a827ee545df46dc0c8e0db3b6be73010f6dabed9827b0e868d266359d46dd6de8de90acb9aac7f7de488ea7b8ad12c2d383298d4558e05a566f3180b26c04671bf7f0f80b2b05055f99131a5a97304509dad7745d93fc95afc9ead622dac04024ea1d2b751e0d5d44a5bae6b15ff7d10b889fdc7a8bfa5f1031dd447efdac7831fdbee459b46d47485cac1c0194254cd074953182e570c553903e369cd183b37feae4a791754d10628398033ac11b256e3ef609e3ffba9cf02b5ea2a47860cf5ebf6c999c2af11f6d81ed9ad06e6637ffe48d234ce69c30d56c85295b3b538d32cdb2cd0d86b64161bebed1a4ed58b417d1cb921caddf1e14133b245013a377fd30221a75a5daa37c7bd75fcc84f661c7150959fe6784653f53410dcbb7df2b9d8bde94f0503d53df67d5966c81860e76d66aa297410143a8d8801ac9eade9097244680d7d63d56363b011a1728bd76fd37bc601d4273952b6c60bd3cae922466ca9ecab5f3e1771be385b47c946edaa09c5f9c79fe2b7711ec5f3b556edd735ab2930ccbb4f41021810f8be82877a87a997969e0dea3e64b749bac7a8abdaaf6b5653e091a2aeeedddea10eb528f0c25ee0d931dc5ba4093013742531015a682ff6d844d11f75adaabf3f520355756efef6a9091bf196fc75d19d0a2e2ea2ad0677200d1894432c320a0aab4aa32dd02ce1cba0813884ae6390d092780291efa57fc5a32d2169339af5e3beeea1293a02340b28f38876d3547ddc8349f8ed3afb42b830ef8ace5749990d8242c37e86d414de9ac95f1f1182e4de61aaae553523ececdd93dd27ccd9626ee237ba8cdf497bb1a0fe246ad1a073cf21d7fc7b5a34a0bdf789160db4e06488a3d744840503d557cf76f126cf360bc5dae253b690198cb1d850cfb3407bd80aa0cf83ed924cbe7c3029c7a9f6d1d6104234532d308190d5049cc8609fc346aa5463357018d6f0c89d2e02e550615a4703ef1f71b4edd6befec8add9cd13b64ddb74986d8b7c3f808514365fb49ee69b6c17ceced6ece689d6a66d3ab4b645be1fb4420a9b2f5a6913aeef138c75955360b1686d1919911a287f1006a55d9b6084f69f8746e754997f4cea99372e99ac549ec0ac38cd0d9975a3d4d06cbe7f1fd80434345f70be4fca60f33be135cfc99d364c37b357dfbf9f695dd2d31ce1d967b5e050a701a9084f908311a09e29aa340fabc1e6702f6ee112430373f2444df8d7d9668d8ed4e43176fd2be7db19fd32f974d007dac2b1073e3f81be540bef9122bede1c7146bc4c3e1de281b6b0eca1cf4f882fd5c27ba4883fdd6c32770b2f104f68dfc59670cca58f4f482f55827ba4386fb76c9c705e11ce02e7524bb82795d2c7279c972ac13d529c9395b5b3391768a743b9d81096b7f4ed09e3a5426c8f14e2ac62aa9d4e37dfa5a29c05dce5a670dce5af4f902f95a27b14a0af37c4094d4928e2bc6d0869f69b242f4a9a14e34b54c890a554d771093c9a3f3ca8ff5779f1f77a79176fa29787dbea571a383dfa22df1b96300cd5c8441eefbaa0ac1f2eac6f0d43636c12b231fa66447c48f9ba2d3009be39fd78d6b5a62f856d4c4f81d2d655bcdd9515020d48910beab5249747b5e74d9e90c164680b5f48d50ebe9449339a831f59419ac2aad6888720f58127b255f249a9b24d7251738b6a1aab16212504cba628b33ceec10b9635b4a39bfe75c31a2aa468433b93e0c7f21bda66ddb8110bc0358ba570369636f7b42c8931884d83052c6bc644f5aff53a5e3567e7a876c9a976158d920beaad099f35dbd81839ffb4dc0eb98cb20972514303a00c9c520ba0426d13d6d5b7ae05505154130c95a3aa450b5dce2505485d2ed4897df78538b58cd4e5a2d64da853be219a51174437a52e6e680e14e44a6a0a5448d10ca8a845139458800a219a80c405172653aa9bfbda56baa13fc6fd00e40a61aa534a9bfbaaad0e29606556215dd57c49653380b298d6d4716be1faeb6fda8ed7450c1581c1b9a52ac1520a643165919a96321da8eac7d6dd96c3ba8c62040cd90b124bb4f53f30b34bfd9aceb2b226e093a9c6a698aada262e9665dd4dd81f53dd4d3155dd4d7424cbbaebb779a6aaeb52aa9aeb9827a68a85480e5295c2f7b6b25bf2f3265bf573a8500ee95e8b8fc1940eb65850d90e1501caaf034209281c3ba0a462d001254d2a919f06cb6a91cba8ea974ba26441bcc14d76a6d38da29cd1c3ad8bdb0c09f370500f05dc30805e264af541859440840a9b167bc2da99796b26aff9d465b14b7386c4b261ed832263abda82d826b5e52ddbd33e1931b6a72d886d4f5b1edd9efe8180a62d7d21c4fab42b6bd58666356b6844530ad70adc0219bcf36bd44b550aab94aab0c96982ef78caae135c0edec3501436d931e0da9d6cca80428a2d1ca0a4a105d03528a9055021b8055049930ca4cb29b204a4228afe4be54c6b43e9c280bc32948ac075cbe50c754be7b852d55209b866a99849e0f2799a2c71b98c61d750593d73c2a33f945870a71a0c99e68482a3118fa5980346cd19086914ee2843c55114a3c8502999635e3408b1a94f8c5841b42e852c422bfa31c5693e37abf8ea8a0d29dc0f5151021a7512b49ed7a0889ba9c8ebda58045649b094625594377595f7dd99cec12ef84c85251e2af2e1fb9432d3930d2f3ad5212bc3cb785cea89afb35d4eef2288a74d0694c1542374d75f0dce02638f8d17fc19b24a5a6a92e1b1051d98337cb487e04144841b826a92c72622f9d079c1bb88b2884c249a7951731c5f4f8c88c3758ea3fa60bde2077df616196f5faabeab264155517587e073faaa33fab3f7f0820e83a6fafe9a1d9c781a4d3755cba3ba8fa6f5cee44852c631300a0d4939e65034ddfee1b8ca858613667b87c3419a02e990c67fb6e2137f50cf9db62cccc250df6fe3c401159bb1a895379d164ce013cd52d5403852c7a14597faa217bbe6824a0d28543d6851745321757ab16a629ad03faae9c66c1110d4534c55c194e6bbdeed2e18ea9c4903c51893d3f40213af622e8400a76ac1192935bea1e944b77611b107b380725457511935c945fc452a5e1535aefd0c1493d949d515da5a7ec61bb12144275cfac5c88e27190123530b0d1e88b83d193dd9f0fb32d30c51f92ef5a2bde7ac90968142dd4de80e76d539ddc56a800374919ce1a3bd17ee398f42351826528064482c4d2222f042f8e22a5ec6c983565028c241879ef68e7c63a850d7dd83898dcb6082179a3af1c9a316d982ab00232d8e62846e1ab81a380e2334e38c682019175fc38b487e4b62149189644811a99fd2547c102f63ec45c43cd8608fdb0b4836cab2ea2e010f55aabe685e9fa8e8a19727222fed3b123fe19880a32c3b24621cc4eb2a067aa37951b354ed3480e50c8d67dff1f44d879ee74cd5f1c6e143745d2e69e800a07acd2bb0090420bc195ad0874b2a1128cbaa3ba178dc547544faa61186e6a116cb8bff1e4a382cd74563a3757b2118b2e170338da8d8e1c13e7ed30f24a6e4a01df215b0a740a8b1c3d895aedcc056056f923d3bfe4792a6714e19e3f020951f706e191c01f223e3055781de350528864385fa7db5e4970e2a9e6a56033d534de901bb35b9f30e74db667d03918cb5be190b47dca61d237de4369f4c312002fcc5ed2ba0a68d9c136990104032dc72702ca1084fad17ed1b72e0a81a2ea939d1821fd4d7c758e237ddd9aae6ad3cc4ac29105c34ed1377b3689a92a38b867bca0f316b0a04178dc9166bcb0f2fa6a90653f326db2c8fbae0e87861df9b43bceaefc1aec808797d2fa22d2c1c4d71f3f939fcba9d3b3bd73f57e7b88261232a66fa4010aeb39314450179f66ba01bfef05719528261660e11612f3629ecc3e27c9594994660068a2145a58c785123ca18c12284784ca773068ac7251e21b2c6a209dea335d84c4194a1d51959c8c0cec0600f91af1d10eae069e1479820d401552a8e88e828ae53c59089cb95f3cc68d9d241d50d73372ef0e54567850e93cc5ba9ca1172874fa8443e740fa84145101ee79bf58364b7565fc81f3e99f684eae3231d81ea53c42c72be7459864ef7acbea819bc2ab3dac2bf6851078312d4a50cede4a3aad01990b5ca1a34ddf224eae2e26601fa828360b92a2c745660a5b2064d3f3ca13964a28b81b6100a15e670cc3f508e5ce86ac018e9784185857b24a58dbd569fc8a302a9392c5b83e7728596bcc3268c1d5839ea6874353f7374397bb5844f380aa865e0aca603ab451da2afe28708b9e7305a8266c58446ca706937871e258a78853537530442b7f017e112372a62650c9419726055a8c237d627eaa6808cf6aa08995b1050c460a90b075683229265c5cc149cd2fd64ac73b9dd4c881cc7457daa16baaa51cd955f381b8d825e1cd78cbae035ddb717c775b0d0e607f26799e5d16db5a3bb2eaa5f5f1c5fed08f526aeff7a1d17c96dcfe205e199c65560a29e695be65d7a93b549ea8416b545dacfedb5c2b88c5651199de6657243571679b6a4676ae9ede1c13fa2f58e1439df7c8957efd28fbb72bb2b4997e3cd9735b7ef4c93dce9ea7f712cb5f9c5c72dfdab08d105d2cc847421fe98beda25eb55d7ee37d1ba1094a6624123abbf8dc9efb52e098ccaf8f65bc7e9f72c45326ac4d725fdfb146fb66bc2acf8985e470fb14bdb3e17f187f8365ad247ae0fc98aee10ab989815c18bfdc5eb24bacda34dd1f0e8e9c99f04c3abcdd77ff9ff6e212d1708180400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190442186_AddedPlayerNameLenLimit', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3e4509a199f3321ed0645511aaec4919694ceec5b47a9bb4896d95dd55355cd91ecf02ff3837f92ff8281bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdf17fffa75b33e7888f322c9d2978727473f1e1ec4e9325b25e9edcbc35d79f3cf7f3bfcd77ff9cfffe9c5f96af3f5e01f6db9e7b41ca14c8b97877765b9fdf5f8b858dec59ba838da24cb3c2bb29bf268996d8ea35576fcecc71fff7e7c72721c13168784d7c1c18bab5d5a269bb8fa83fc7996a5cb785beea2f565b68ad745f33bf9725d713df83ddac4c5365ac62f0fdf67f7d1d1759c93361f5da437795494f96e59eef2f8e875544657f1362b9232cbbf1d1e9cae938834f03a5edf1c1e44699a9551499affebe722be2ef32cbdbdde921fa2f5a76fdb9894bb89d645dc74ebd7be38b6873f3ea33d3cee095b56cb5d51661b4b8627cf1b911d8be44e823fec445a897bb35dc75f69b72bc9be3cfcb88ebec5f9f9d76d9c2744fd440a62adbf9ead734ac02ba0a6e3ffa7e7f2c3c1fabec81f9adf7fe8e0435046ffef8783b3dd9a2aee651aefca3c5aff70f071f7659d2cdfc5df3e65f771fa32ddadd76ccb49db3fe619615f7e6b1afe3e7e88d7870775cb2ed2f2f9b3c383df0955f4651d771a3dd6b2607bedc3e78f38be0779fdf29381d78b6346231845d1aa5e65e9aef0d153c7640435fd5b96a464747652a1fffe440c80b5907fcfcae4e65b1056b44d671931442da757df4a13176b4d9d45c5ddab681d61c614a71d86f08783ea336fec0651d36596c6df6c902bb3789da58c720641ff35955e51264b326d945192c6397618d4b3cbd1e97249f57ea462d708bc261a44d0ef92f5baf014741cad3c599c1605e9b827934f71b409d09debbbccb725bf25be1c2e88e7b3219ecfeb6813dd5a8158e6f56157de664ebc2c06c4e9176210a2655923f9a28c3704e2c94db26c3c0597d941cf7384c9a2240db4b0c932838768bdebe7878cd41fd0ae5f13df32a65239cb8ad22c615afa88a319cf9c9f6ed8e9cdcdabb928788bfe2acbd671940e20cfd749b1acdb6b27d3966e44b9e644b0b99f5c4f379bccd7df8cb6748ce379586885fef37d1cddee6222cf0c6b486a8a239e989b4d9b2283a885feb7150759df11db7b7870197d7d1fa7b7e51d716ea2af87076f92aff1aafda511d1e734210b5e6a78f29d596f5fbee48357f286e07915af82b8b9c4d9898be2136353e97ab8d103fbd1c17dfe982783ad74fe7d17efe2ab28bd452f461befae223ceac987f7e62e13ab610830a0a819641c5f46e5f2ee32de7c89f3d3bfa27c8595e5358105f9d8ca546433824c85a5c8840bfbab78bb6bb75e7cf8bca15e15e172b1e24662fb33fd897e0aa5fadf88af71ba5ae54493d60ba5b3352916e797514a7cd76e64411c8747c26f959f35b0c5fd98e5568e92cd10ac864de162caea6117dd933e1fb16cbe2393769e9649f94ddeaaac7eb65ce0345265390c2049f28dfb8191cd557cc3f5820e77418cc722b984d48eb2eee7db5d62321a2a26068789cee6fe032b8f37c96e739e2abc195b135e994c79bbda61afb6d94095d832df2cb9d23d3b8021b707683df7dc2674394eb11dc41d7c4fd646a764be7920e00fc2f06c97e7715aca935b0d4cc39498e445f931cacb5486b4c9be741e891d61e518ba1076fb84808aa12d49931c7f8f1e92db4ab192c77e97c40ff18648f57d42e7beab785d952bee922d3f022a1bb69008dee4d9e62a5b0bd6522cb7b826abd86a646588c29fa2fc362ef1bd1070d11a6c535f14648a1e81a5f5fd82496c7bf7861a9e95413d75a1a6067ede12fba32cdab58cef8cba7cdb796c4f2ed207228e2cff46f7527078034814fa914aea752317b7d54bc781fc7e93ac634387c4e2062d698b2b34a5a7b1d556bd6cafcd17764841340a7dc945f50a03cadb6aec639e3c90b9e7922c2ac85a43adafbad8d95d543645d5bad21795f564286fab236686c1aa08205168482aa957905cdc563ffdead73096fa82288ba72dae184b7a1a5b3d357b1c8c2761561444a3d0945c54af2aa0bcadae3e178a3e9c6eb7ebe600879651aa46574ed289b630a48c17c7fd6a0eb1c6633c10eb0d0c86f648cd6ddad51fd322f785a0c0c4634d18e2b428c8e294e952bb27c638dcc057a725502fb6b428e9daacafa7e9bb83bf6eb4f7100a170003d1a6e0e8140606496c6b6d147274938181194a1e5a1e36b2d133b2f6701a3fb06632dc720e9e330d6b3f27fb1cca32cfc9260376c5d62207375c189303cff78611cfcdf986b2d2c031124003c4055bfd90f18256cb6606c8aaf6153da1d5f1d04fd48cb0aaab126f63a226b2cc597d8cca32ce5342b98a1beb3aed7c1cc2f1a884f2315ade9b0513765a0f3ad2240f1b3b343dbc6c714de573a622f29a6ac4f5ed70196b3cb5f7298b279310a7c6ca5dfbab28bd67c6cbc92f4eceb43f17f0881d73e9d2e074197dd1a09b16b08789dbe8f018c1ec5eb0cfe865f94c3572fb8319db510b1fe94c3362a91c3d99d013a55898682b983798fa7317176553c87990f0c8c19d63f034da730cee0f7870a8cbbbadb2ecfae27a2683e84bb8412edfc7b6f58bf9c1de1d480037bd83baca072dffe6a6808339a8dae4600c5a3a8f015849abe7e17411fd8c0ccf5b2269c06d163f4d61a836ccbdfec5096f6c0c0f0b7c6a7a365a4dcf47abe9a7d16afa39684dead5cf43b44e08e1e96aa539f969abaf0d5cdf8885f80370d460472aaf8d2ce9edcfc589295cafe355258142b79f490b9c9665b4bc8b5bf30ff719535eb18d6720b2ee5d8d9478ad5bda2aa78785442eebd54c256f2ce14987de83f5b9e2a0dd8755df88f0720e44d087720d44be4f8ec1f7e018885ab765a71c86b5ec74a35069d60552c444c251288d8d816c0043a3ac59a445f49127c17752a0b3eda56a7a9fd64540f7dee06278596297055afda2f15554c447109bd6e8d25213dadccef01d9bcb8a56cc7a3767d646f6a268dd63bb67b1c0f6a97ffc92d7f14d4410707d9f887bc41443f467c75e0eb4f3cc3ee7a1fc8407d5ee2b16b59f4eb9579ffb53147e98f636c7505472ee4ce56d1d73c47d69e1ea32e4a12a8a48ad5795f3f2496591d8d9c18a16d0590833e860f6bafa5dec18473cd6816d08e359f118d082725ee5d4e65dbb4cd6fa72012d8be8bd602d5138df1ba851e9759bca62ba63f6b49dbcafaa163717ac363d4a66411c31eae6013b781c6cb06f6c5a51a2a73d777d22263e18004efa14a7411b25b6b4c24439d1f4c17a51b6b3c7301e581f6805f2c2faaf5e5653b965ece1bd8826c5e4e5b85992cd26735fc351ea2391c5686683efb1dcb52a6e8d47e72afa2399cd5c3a7876175112b753d2ba8f1d8b2390d95c7afa2ef1d86820c4470283b9f4ab5db2ba758c521e892ce6d2b53a5e93bbd66afa2399cd5c3ad8efa7557e81cf85a4fe2002641ae61442d165f92805db6d8abad0bdee79ceb0d3c0a164989e038ca7ba91167af9eff624aa5a85f8f10870f45535e37a9df1aee3f99fbb64cb7cf2dbc133dee0824ec2394acc793b43a07a1864a0b27e99ae3f2619e42a01be67a14e419a1a2a0f31a84160380e6a08a15b76185348fca990fdedd8cdb2b3b50f12b2bf2cc75976b95b0df8dfb31658cdb2bb62d48a400e8dc075b2175945113bbdb9ee083d2651fadfc1e3d95d14e7296dd3caf780f2a2f89caeb3e5bd3fa7203753fa4579a82b29d2c0c6078b91488df162040ac5046d2473f13d143718eb1a98e9bff905e892be281056455fdef6641479c13f68481fa47e02bee81185e66d78458693c5bce81be214ed8227f779e3d3709a7c3545574bc2cefe39d3cdf6f3708797f208559d5dea4bcac3445fdc3e44146792863563e6bec066cf69b843a1b9ac42ad37e35de6f39d06366dc2cefb310996ada80eafce8d705653ed67e7115e333b4f55c30208cfc691f4a3425f521a1486e203bddde36a728d67a7bd9daf897fe731befd47f6b463ba1f54b6633ac870ac3350306388fe934d4e3141985ef52d9b0a3bcad3ef602352843172003b3aa5628044741c794202846e9cee757955bddbf3f28ed4e7ed48cd8526aa9ce4a5ba06b65dc3b0e13739126df44da624e05e698b879f49c2c511457425e4c250645e0bde6b1c5216dffb581c7c2b2e882779515cc5d1ca7f13ee3a4e57de4ef655bc8c938780d6a76568356e2bfb2052ea462f408018c31095ad55aaa56edf3b9eced837b638b6671c8d97856ae2d73211879d1237c96ca6b25042a6005b2ba549346039de2a4e73886c436dd0376b13e43e59cb58504ed686a2d2703095b71de2152364579a2a9a8c3eaa5ef0a524875f53d4cbd997f97a0fe36957afdde8711ac0731876554870bb64256fc97fa93e80c731e227878c34fdd573f6d14d9d33acfeeef230f02e5ede07cbec1d26590efb3ab3ea249b95705ae35981c2d2e0b0247aa3d397d45a4ea0b8f5b3a4d60a0e6134316d17eceb2093577b728098bc80a2986e845b69722871cf1639add1ef4ca6f5da12b2b5b6373c8818e662ae2bf268ab62dc7fb1b5d6794cdfa67a9bd8eb32ca43f07993a449711780d1ef3b6a0f3edcbce2f3d1bb24684ed234ce3fc59174506c982868f65fdaf68bf43a5e66e9cab61dae9bd3ac19678bf6860a2e21d92745315b134bc791c6c0b2b5b045e1e6f625b4cd658a590745219a46c9b62f0837b5fdae6d6857c8b6993d2c91b29509e0668be5b4cd970afbcf59769b0eea0cc7df69ecdb10335f98e85380c5f471dc9da20b34d9b299f9524ea5ed10d23764024991fd9fbbc8186402fd14a30f320fbbe66c9a2abeb0601c8032b061800a5a2f260c8b22e7194edc7d314c84c11611ac6454ab075519ad98bd36bb3093072da393b1f01d96b058c86b678b1dc01c627d270c96db0c928c385d6c0f97466ce86c216c4e0cefbc216e33a7c1c1468e676bb3090e119d7d751f2674e0f90c0c4a3fd55068fd07db3110c0ef70f2a182cc66e835856e2693161e211796b61302387ba9660d27a4373bd6d5768de593fc86f4324aa3dbfee8036038d52068f7a06c0701b47765bb35544bc1ffad712e5e9efd2d2bcae6770f3ff96e57becefe0a7342404f3be84b3b8a9430470e55eac1327920500d740799308b1d8e90e40c242cae7d538fb420d1bd4f10ca547b33a2e9509591ec9cb2a063da604ccb59812d243aa913bae29239c4d084b08e412de3d45691314dd6a11c8258b5cb88f869696c78034a335f79df3b6baa12ede840b50d762c3c7f0b2b1b4a01ed485b29b633d9247543633ac83d0f23aeefb27adabb8aa362dbe759ab8f2a9b29b1d3555438a429f9bc5d917ed2ceeef869bb49a5c17f779e39fa5920f494a130b1eab9c5c9ae0a69d3b17695966dada9c0622a6bea62463ded67f740a31b0b982c810293f34d94ac3506f1d9cfbf04b088552dc44dbd49f24d80d7f35151fc95e5abdfd8c72b435d59be8e97bb9ca0968cd5cd76f0da3ede656446dcd58737e3d5154c359ffecade44cb32cb03855c789f2def89b13e4f57744afa5c2ee5190ac9204873ea578a6f0898e355801d376ab00cee0f76fc792c3ea24411104130ad8bb6289395022c21279f808bd96eb5bfcf6e1345547bb186b6a8baa9750963539b62c16f4489f5a80e3474e58c8df73ad8a02c71c26e4aaa1b5d1530b6b62e15d29fa870661935a2f22d203e3f1cacef8bfca129b737aec5f021f6a984bcb7c9888029fde0935e55d33fa2f52e7455ae18ad0c4c008c567c26c368553bf9f9215951cfe5d84cd11626ec51e55b94593fbbe65ba6d4f9c9b3bf059968c58980ede6d8955b8f4c571053d31d00c394cd641086010616a5ed74c162004bd9561d5ea5cd1d58a7772ef5e96b744f707dc4f1996c39deec5338cd9d0ced5873687bb339ec8567a286a0179dbb0707ec55b0fa37c77720835def6d2557c350e78837d1217982051112efd2aa8b29d3f14265836438e5197b8c5598e15c129bb283c276100f33a0e4475076f0d75c6fd82ad65afef054a582d441d9099ef442423068cacc060d0e2bb6fb58dbd3da39a134cd31a35d771b17e822bdc92352fd6e49db7da4603ad514da1ea03a0c3e8674ea0bd4f484ab79e3cf47209bf0cccc37f6970411435c4f7d7945604f039197a178bbcebe446bd7184a12f5771bb625480493f355427c57fbfb30c2c1a6a415ece1a634bcd2db1d279a3a5858f3b3b3e73ae700374a8350eb46631024a92f7892de14e84b4a46c0503c78d417b93e55c0177d494447c28579a92d22e9ea263bb3f679a070f802ab6903979e39bade3cf5d40e40807654b3bdeba51b972db500db69936ea54d701962ea63542a7fe3691955caa229099e96f50574a7654c293bf3b5dbb078939f9f5c146fd6d16dd175de2a2122f3e8427eda12242fdb676ab7d7df886ed93d225e41f57abcbb1e95925e54a73d2f0f7f9494c9957d97acd78b4fa4a3eb8ee20441f1eadbe22ab959f7d53cc3115da709810f4ffa1c477a99a4c9db5dda91fd84acf12e2b59b29f71641f1382815e26bf202bbb7cdb91fc771cc9bb34b9e965f1371cd1db3c4ea3554ff677b4087b9a13033208cc93b880f06100084328c1e4c480139e1642cb89012e3c0711342706d408f50bd839318087a716207462c090503583a4130394784a1e50270644f1b422ae4e0cc09284cd180403bcaee2edae36af8bab8b379f7a3a03ba04bac51bd28274d5931b00c690bf8be36d3527b5a40664c9a452e506749d7fdd5281d18ba4cd966a4b6800d665bc8ad6758ca5c5ab98de53bdcbb2921eecb60c0ce09218d4ce514f6f8098444f75ced21b6026d1ff16e7594ffd77792aaf276dcd442ea6c0f699c5a57ce1034edf552c029db11660699ac061289a461137e4cc6545f686a172ce14fdc941b7a795126a8c5ec57fee08629a1bf4965a6e96092a7e8d82f91de9f11cb53fa2a4ac5c769caa99ebaa382d770462454623f93fe26519a3d5fdaa4d0f8673cd38fff46707789c11d5dd66f937c7a12f1c40f0dca686449505118b883a2726160e34293d1602d43c264bb4935e2514c5eabfcb658675ca3fb6190871de384d988c77c34bacf3cd1a359709abcde51c62c2e2d29c4fb9d67c1ddf44843316b07fdc25658cc5ebab75b4bc4703364a5767110370036289ab1ba71c8101b515c1c7e46bbce6a84c4b8264fdc000dd80dcb7d97a85452e0bc7bfb9ccb195eda8f661bd37412a56471cc769e0f85bbcdec4e5ab132c1e5f65ab6f4c691320091eb7e4ff190a032a3f46695930c50da0ac9b7ff50c8b49da7ca6b4c98cd2c630c54d605c670f315b7e604076d342104c76dc8e44bed320f3639c6dd7f165849edb6b823f687fd15e7df6252bd92a4c7e1e2dcfd7605a03538afff8f2fc397aaefff625cb6fdf57718f7128ad293ee519dd68c662b5267a9d67c9ea7c4bc3ab8d0359e23d04012be173d4f39a06a0cddecfe2eaed072c445b9237680bda525cbe7b860569dfaed73fa3ddd16445c4b938c1a2b4298fb6a44df9e7587836e57fc2e2f27a19a571ceb4dfb48b5797efdb6fdab7abcbf7ed376e06d7047d074c9bc0ecc812377d5143ab5e0805195d35ab238ee33463ec32592765947f7b1de5f78bd3a2a0ae33bfbd6dd003c700dc1eb7a1bfcbcadb1d7ab6e06885cd6dc378e448afe86641f93edaa5cb3bc6c21b8628c7a2b109120fd3366ac3a376ca61059836527916900a4c5ba902074109a68d549e5a508361d80bc40a45986c81c045a50be331d132b949d403c17852d4913b9d12f5d482fc8d27441da578be635a0976842ab99bb6353a064a911ba715c2a15a822b646e9c663a7a50e8c659a727174d8f71fee94805b19bce8b184a85dc4d47460c0795e04d4747ef3fbc3b5d5c463494c8a77c57f4db25a613a38af0f5a7d3ebab6bf4615145f32a4ee3f53ab9ec676cd311514576bd4bdf46ffd3e260a8a62a33e21dfcfd17f47110eb1d381dfed0adaf6acf31c81d0ecae8486039ffab1bd74bb242fafa1c3bf5d7c57fc2cef64df1bf61a7f8bafcdfb0d3795dfce417d5d48d42c1f99fbb645be9ec7a9d95ae20a07104f38c1e671647bf45e96d94f7bf483ffc37bece308f33868509bb1b7ef2a3a97465d799f2367efe8f2e8efe79b3bb5fed661391fa1fe8c81c273ed4a9b7f5d07e7654dc63475dbd4787de2e6cf62fb1fe32dd5ec43ac6d5e622d6077e95654c61c364f1314f36c4dd144ec68c3753e8bb4a99cce4dc9e7fddaeb322798899c5b7c999ec69fa05b8c98dec6918036e50c787f28edb1430398c7579a64d06add4e599f658cce2a2ef871afa6cc6f2f699929b0d67391dc96c3933dd7c1f71d55ffd813fe5df90eeae58236cb017d98a3ebfce72ac81794ff308e72a1363a1b95ab87500131fbd1d89eca6d557cd05bd4f5f67d555690b254ef17d6e27ccfa7ab6ddf4c7339b78eab3f154e8dbd7d7049bd535312cfadf67452913192fb8147194d347b72c91c1647344cf54b317eea84b8c886aa7f0865c0cac3aacaa4df7d66c347db53b8f9855b0e9586b7716a7b4a568f5eeaeb35d8956ece7e2f72c678a1ba6bd7fdb624b7e2ef8769826ec33e61cced0c5d35def98b9dcb0132262f839dc1cb3890dcee97a9d14c96e8385e26fc9ed5d9e14e81df4eb28253d228e25fe0a407ecfdebd3338930dd4af4b2c7aa9f0bd56d1404c774b38b4f648e234f9e45376c925918be532dbc6abb3bc0ab18a044445524736466fae5434e75f99cb670658343738d950d81fda60cd58a4803cea5d49ec8ab061514995039ec1b2357464c5c0511916241ff3781be53147e231ddfa81dc26b83d0bfeb1f786fec8787b63407c055cb6bc8bffaa881e6e29e10fdbb846650187236f042a68601f3cdb2ef43ad6a870f1d2b15685375b035fd10723babb2b1c60b7bfeabedaa565b289cff33c43afe29b6709b500784a83da29aed659b46a0405713060a1e5509b2a8881616669b4a76981615e6918a81b603a1bdd557b0737956ec7b9fba58cfce138af80315eb89130f64c72fa10ad932ff8cb29c4752d4833d0cf53aeb7d1460572dc2e94101dc56e15cd0a9ee7b407a73a57bba248f03743cfd35bb22c42ef8fbc261d2bf03b236734cb4ca1f4d1a40574b48db8f2a675f1bb2c8f23fcda98e02a653b8b5b209f1645b64c2aa0b4f867f34c36b9c6d82c8b822e8941396882161808fb28077da41f88e6f0e092202da1111092f21b55325723a9f343fa3a5ec7657c50bf70a34fa38a65f582571010e9f2cab9b95da447b0b97d7a28beb5ff2435e22abe89731a1934a25931e81a24494bb14b1ff3245d26db686d294c81cf8121456a6b338ec5fa8fbb06885f5ec75b1a43272d2de516ac655d0304dd9ae4fae29801b71ef3f51b4953cf9a903f2a40d93081c6829ade8c31c756e8213ece88741a1e2ebd1c7fa8b820e2910c9b3a70a2642ddd879096e124c349df22b7d9e3c7a323d9a7093d42500db7c4e1ab2cdd15c30d1794f6314d56309a780835fd601a56e56ed3831226520f853662dd28de95b689c0d8f019ad1ee3432f430ca0fab875dec8d74bcbb731a381b98f180305acd503464bab86b648666fe1f5152b013bc298f24037aa5323831ca5e1bdc17ad3ecb35d4ed9b671745050d791da1a7133beb5b529e16d1a57a1dd154c2b31d8e8db1dd2306b55856996c06062ecd6c18d9a5eb07f1880aba453a396e36e8d5d75857b6a978d1d1ad9261b358a694f4d37274c5bf81d4aba5130fd187c0d6387a6c4f49efa18dde3ac7e214c7fea360f541033d0419896496c306daa10da0c6972b1701d9a1db6911dc360a97bd42f4209761584606f1ee30189064cab2aba705d18610809ce1aed80766b45511e1a3282b73fcebe8aaa7d8e232cd0668a416a38f741e714a3c16e908f7f4ba631fbb5f170b0fb1ce128869fafd180cb8a76afcc3fd8bd7db3ff202ac277a22a3d8b69a0c55cadafc6ad2372410e2c1c3978c141416933c890b5efad9365d7bf598f353ba04cd19529075bf34fe7d1c6d38f3edc84eaf76ea16ed9b1d116ec96fade8385bbb247e20f86cd294b3ea38f0845333433515f760fe7237d777d16d021468b1e1398d6891ca6be58449dd6d3b28c967771bb9357fd86dad235506bae0cc9840e37860cb56b868869a44e3f40acba38d2b0b0523aa64df5d2747e2300316d608827c0ffe399216cfa3839fedde68249edbf1a0b556faa3511d27532b3d0794d663062fc26441bf67ef18eefe35e2ce0f1b079a48b78eec8f4227d205c49938ce7277ab2395e4e951be935390d713d5529c7d196e83889edc1cabcdd52687ad0c44c5c74d932509e95895c0d7391d2deb132d6addc9612fb38bba9c4ae83a3fa55588da3cc7b4b3add20e82385323ea2715f564f06825ea4b0c2bbbeba7d5f42a07a3716c8519a9dfdb241ee46f38bc9aaebe94640365f9fd2849ba68f39c21aec1a6a03a628e23238b041cda21c9786d5cc3c178b4bae5ada517d96c770e115d5a9911d749486f7c04b676319378daf7f3a4f7530d793410897296cd06da80f0076fb799ec0c6f507839f9acc13d33875fa3667ecbd140072a87d0a401a33dc4d015aa91c06ba6137d0cb32b510a7d94a518b6b0fac7413039c09dcf53a2aa3c555bc8c930733b831d4b0eb0d11daf9df98aaf7cf2fb1e9d67878b7d133a6552de5dc707f4deb75443d4b3b2ae6b98a1f0be2a14e4d8d7748c39836d57433c2fa8205201e6d0b4e181884fba19bab0f02366a44cd11e250cf30486ac882c31bd2ac457b28d99ce08ddb50d1d38d02f03ddf44c1756842b3bdaf9b27d7a4f9e4af7fdfc5edca028769031d846999c406d3a60af70ed3c80e8d8669a446f700d355f6a1a60b4c7cac42096615018462a6ec485b26cad6019067ca723d1fe6de89496eb8899d927b62d72422df868c0c5a9405561158825667739555ec9db135f564342b6bd2dabe98579a886e51f7437bc50f2aac44282d37a65115dba537a8431a5185843068a0a42140a910865f13c682633388b080948a2b2139eae188a26dd3c1522927d4844a398440a652229ead18019c42d2ab054dffa685a78a0002a8507624902a5b08c0b429cb26bd1b08ae26b961a0d2f2f083ac493ede2d190fb6acde16cdfad078f11341ab01b31d54ec6a55d9d1a10f952dda36965db55012a649d49e3cc4d39a5a76aaa0fdc22c9fa4e2967e8071f12457e06625c38151df2c8cb2ab76869bd665e1fb356264a851df17ed70b685e7e76e762d531949686d3680ab294a680a475394c5fcdd4cb6f57f24691ae7b40f161650241ac80e4ad5d8a06d181ba86ad2984b6f8312304de969677194d42ed918c8a8f0a127331d24d9e3d150df544e22ae596399439c52e66f18a17e54db08eac32035891b16079abc35cd0420ec72f61a0cc6bcf430a0a9c80780302f20cf964c035febd379806e14a3badf0746c80e612014fc745ea351dff68cfdbe8131480648ebc906cb8123d7e4645d2b3f616a4ce33a86b68a414eee716a0dd2a6b1b1dda0823daa41414ea61b0cdd405576874d03bdbc51b70be5720607a65a2568177862649e6ea9ceaa5f3e17a4f967eb28d9a89d60b83818fc8a2f39563e6eb87d509807be645570a834dc5aa16190e21b8f4a2b154c0328d96c30fa3ebb4d523c46ebe2f3c568d33e3346ab8223619417da0418e5a5b27718452dc774444e78d54411d45565b90a0b37b963043001f68658d14d8041da39bc99ac4acfd74ad6cd331b495a6e241bc9496c0298722299bf856ce26abd257f5df667bb97d1568351358d2ee02a5f7c2cc06ada0ac0b62a166db10b35afb0aa1af16160d392078a8aaa11907f6b465bccd36634cb3f9b1d580cb17a592fd1d9aff00d55efdd86ac4daf46db95b5d1f21e38166fd7d917c28579d578be4acacc84753d19847289c206df86ea26737271ed1a0d9b38ad609a53d3cd0895a8e0197ab21150b9dfd13270fd9910cd7b1a238371ee69cf16d457472ddefad286c51bb4321a7ef1c6346f2e8b375962e32ede649160eaa75423a1b31e3c5da6cfa60134c6c655bccd0a6af589655aee8a32db44699a9515875f4997ced639d56ef1f2b0cc77f25b00caf63a2ebbdbe1cb6c9796bcbdabff57690e253bcbb36c1298f44f7dbb4e144ad650696345fc2b6a89b32fcff6beb781777f2d1c2517a2bd5dadabea7b02b2078b6225ff264f08140dbae40ba1f8b63149cb3a97952c134dd06e5405f540347097936a19785f9326c77c6b94ccad1a2da7e3855409a565c6b498cb7f0da344cc5b8ed422e9de7a1dafaa46a90724943bca3876185bf23127ebaed28042392231aa0f4c306888b51cd1dbc0b50bde187fd1184128d6258ab18123567d525c1bd07200c189ac19d35906c7bc0eb565c233700547e20d15b2e0ab943154c8c097bb532c31e4be623829bbcc7dc573324c7bca92981ae8257525d3fae2bf810df8604ae20796c27136734573acfd7b603a104f5b4c860fb8c780e0dbdc774034b2396d37726c4ea7ed9a5b1f511979d7eb02036b61475de2297cc77063ce1094f3b65810c157d8c2554c504049136f71310c8916d8cdc04e7c9bec2c5b41e0174a209b8b503ea87866d5023acf9db7c3c6fc393c60c8345e3a47232eb59815ae81b45b8b779d16d70cd23ace8279bb928699338b0771cdce8b0e2156f512896d50ab7059c456f46a89d8b08144af5d176a346155af5e2b036be37d549400069c34a3e7e5242d2dcb9134a66fc3d863aaa996a9a37a2eacd494a2bc491230196c9e80ed18a3a415fc01695a68d459a8e2b6057f28ab94ad9ecc24022db51adbd06e8c51dcfaba400c1bb5ea0be1b35d4eb719c52d010390612a24dc40e2e0a0866b5142dbac506749b35b660b7eff4c25663589a9f74a4a3596755b7a56358c8c60ae6e9cad509358f51a67237ce43a8d6590f71cf9bc90805c4d24ea5e1b2821b9823ba71ac19aaa00040becfa7a8b55b0415d0a65409caaa2ea3e2a2820f129f687350254311f497080feea44da7680e469ace0c2910e0449be8e9144ab3a16310b1849a95921a318a8ed2a7bdca35b88e3aa995adecce98aadc005520751f01c8613b950cf68139ab241e20f6ac7c196858374604ec36943519fd237160e7043ed84c827858bea37e3b68781d0b8bfa0a757cb5d3c0235ef64186a524a5cade1a0e2d6231f45e722020cc683c87a0a70ab6f3354cda9261fb3b541509b07be99894efe1ab1b8d636d25ccb2d9ebaab023ab7db4081dc729008836f6ac8358c6cb4a5fcc267771155be79e96da43489c0c4c0286cfe668951e4c6fac613bd94c3dcb42a375068baae25549b0bee668d4eb47afe138a944b0b8f93294f62d36930077d48a9f21598fc6dddc808672a705b7416f9ed3183d6b45517d4424cb36b67c8950e48da26bb3ad76f647e75a6cbe015398d649119d5a51a028397ab9abfba67f01e4cd9bdd593bb26bf7728ff41939cdb4e674e2617932c1a34bcd659a605eb6893679a35c2aa0b8e5a536c93597a70db80c9548c17b9fa8d164606e053ad61c40d3ed39a40d85caa5c949cd5c9750dfd06d3eb6aa56b2b5930a1aebdfec208d6e84cd8e4753575dc78dee72fdb695c0753a65040b056c945b95e63d38bb2bb03c6cbdd56358c265765b64a40a0b8cc965c3f8db92d990e6a2e8be3780242c35e15f7139c0989b8ec8aca4e9ab0e722b809d12626ef53094c9be44fee982acd9f28a8e60180494eaaec7c38b9bbcaa65108423a724943872402a58410beba2157dc604252663403c484cb7ec675cb98ff8ce998e129068e2f2030ccc31167c169736aa965884fc505751b958ccb4e02d6d5a89019d6b435238249006518beaa5451cac106248bf21cbe4076a8c1d108a63032880a4e75a4ec9694ecc8534c528a22d4bce2291b299b8e4146faec3bcabe29f3ef78ca4c99706728d91992c01816090005ce8397094d4b0484209199620633689ae425483942a94e8c5d14929d84959f90a2c46afd164684368b5688c4aabf368b563789cee05d809cc4c1b4f16d48fba0de9456277eb0ebb24d0d636195c78a9c3cc0245453be01759f351907428955936960b0a58a22e63d749506111d9fbfc8a28f8fcf74a979ffadbb14a38f65cf6a007e031e5c50ed5370b3a0a010eddace0941dabd042504541778b59d082e1dd304820f11aeed9d69eab097d73453051cad1a21b7ba20be7b7598e42092aa59994760d397601701a198c99a9b7fc610cbe0e53b5d9065a6abfa780768ce8010f5011a5c274e43a05ee5ec6913e01798df90217e85ee6be33dd8d732da6036448805846c135396eb3832aa2c2b58202c8646b2c838b213c8d474e46e1311d5d065d341bbb74ca7395e87a36cea271d55384ed54c0104e464fa054e0d0856e1271d1ac193f2ea5e9175df5e1c5f2fefe24dd4fcf0e2981459c6db7217adababd845fb8158eb6d92de163d65f3cbc1f5365ad20de07fbe3e3cf8ba59a7c5cbc3bbb2dcfe7a7c5c54ac8ba34db2ccb322bb298f96d9e6385a65c7cf7efcf1efc72727c79b9ac7f19213f80ba1b55d4d64681240095f49d5a4a56f92bc28e90d822f5141d470b6da48c584a896bcf03a61b795a90357cafaa43fd3a8362d2dfd7773db2ebb8f8eaee3fc21ce8f2ed29b3c22d3e46e59eef2f8886fce91ae8a5eda6f8800e8e969258b9841889e9c30b85e46eb286f838d4a8179cfb2f56e939ac2f59a38d17f43bceadf2db8e5f126d91115ae8894448ec23790eb59b6d9aee3af02dbf3afe4af244edbc8b98e3aeab90055abbaf43e7e88d77c4f7a468be62bd4171543b6330aae6c111bd67fc4f13d82bd580cd4c4b1a00abcb628fb5759ba2b7c94d533b1e8ffbf65492a03af63b5e8bfdb48f5f7ac4c6ebee9f8b2256c38d3f69c513ba56b705320b092cea2e2ce473f94fe15e992e568baccd2f81bdf5bca69d1fc6e23bcd7592aa9a4e2d57e70961858dd557c9bd0b5129da5652cc85ff146b30e7a54260f640a9039cb5ff19c85e828e27c017cc6f3ae26ee8f515ea6f244247ec37315126eb24c0db938d53c85f4b22c4f43e6d98a273c7eae691899a24c963e83a863d247ffb51800ef92f5bae0fbd3315c341fad06541cad94fc9a8f36fc885b4b68951cbbcf363ce961a7b6df4c011bbed77799baa5cd471b7ebf256a76f5371b6e172971c08917f93ada54d12061be62299b1a3eeccadbcc588358cacdc2be38169c62d1313f963c7361c1243afda82541f380180c6e36e4d2000c34efba50c031d33bfb6ccc3c70052114c05bdad38decccb4bfd92e49422c6e988e50fd892c81cfb6b3b6147d50ac43534cae6ba29161b88f1c6228041a047ef00f0187e955d4c7b91c5c55aa6b8a769a52734128aa5a076934d57d9f8755086301ab4e7d8c96f762dbb80fb301676d9a81d88a434eab52fa13d729d5cc48bd246c29453df15fa698fa546b3eb7c55eb352bc8ad27b7009597fb09b3c6566fdaf784ebda4551a98dd403144fa0c37447415a187879e89721d0f2fe11d56ef218704ed893c20da5ff19ce88a085824812ba249a1c607d4b0c1da4541fffde1e6bf58380eca1041ffd5c58150e78b42eba9a212f4ddfe66b12755451214a1dcfe68614409426e89a020a743fc36f0f880f7b436d92ab9698e2b17276e1b5bb2da2e19ae36db5b25a99def14dfbe455dc06697e3215aeff43c9b128137dbb94a9ecd5cb0cf0610ecb33104fb7ce6827d3e80609f8f21d89f662ed89f0610ec4f6308f6e7990bf6e70104fbb39f607ddda9d09bbb3959dcbbdffe685d9a0f372cbba11c9c99c9ee5da2c82ae42038c2eb3b91da1f71b4f5d8f3100557b3fb4e64670846e920be8ee35c25e8b57c6402148fb476148356fbac1ca55cc04febc641f65544398b3ca1efb337155d068700a6a2e33557233180ecaeef9350a2a3ace62a3947f32ac7c31ec9be86da9573db8f9bb745bc284ec9d224f9b216962fecef16471cf2bd61cd6561d58dac9b68b72ee90010bb297c1ae16048718f34a3d1475c6c4537062a16162b4ce88c95f258e80e5ad54a07ef915276fda73d5faab283b69a877ccef52bfa3d1efdf3d1ca6693e90569568691c5932e70baa8f65d7cb58160f2a40f943eba65b5a74a907c9eb482d2cabbc4e0f719f561e4f0a4099426da6591872a2af2275df8eba2de41f51c1818268f5b1f8e2b692833e770638eafc9656703c144a99f9644520efb61fcfdc98a44071eb080c5ba3cc86eec5ea21bcee53b0cbef569de11d83631186947e275522ceb7d00af5d898e8dcdce447d7ccb6fd4347c16ed47ab27746481a762577db27b1d5e1df5c1ecda8f7bf8a44b482239e871bf9c7ed2fd291786d7587341bdad23be84687f1cdb5657155faf33f12145fff36c2cb43ab3d8a0272c424e3267109a1929755414b1f47eb0fb11af6bfa5f9e49fd8bcdbefd791a7d21e35fdcb6ef7eb6e1f5395d67cb7b9959fffb1467acfd468638ded90fb319157262c38146839812d16520987928f5db934a4ae63fd984d0a90915fc26b0c6d4e28a5cdadf668338289bdd309833a7fe43800ec36478ab52b7427ab2dcfd8ae70447ded1c7db51bdce5cc685103ba1fd6d66681b01669e00b385961320e06518e95de6b604a3344d959487c50a47f625288785daa15082f0cb971c6054ff6cc3e80d710e5731100dade2c77db56a5f35203e49b7b7eb56321f6da3517dcc13317058c593f9f614884adc70182610d554ae1b90e57028e74dacc9c97b33335162a026905fa9763f5b6c82f6b93015fcda4f432e1f6683993a27e858b881d2a83a61076634227e404e5657c6245364eb805d145771b49217c0edaf160b872a80b0b474e87eb579e55f275696e32cf4bfcf6634002946861908e68a30e7c008262ab57846d653f304b94d75bf9d02ff9b88bbeaa759426ed05589b9224bc8d9ae50e6078f2a2ea53407b43fe2f9b42918a0034af19b0d786f89a8219efc179badc878792f4f33cccf036f1bc0eb88e672b50ba82bf85c4529ec3b2af59ea4e23299b46051fd6cb37cb88cbe827ce8cfce6bad7d3564fafc81212c98a60684e9d2520f6d21aa44a2029bf6b7a9ed4c931946c9b4ff646169f2985801f1c0a3fdd12a5c4f2ef1e97eb4783a91a449712732ea7fb5d0e48e3a461f6e5e495167f92f788e7dba5051fcfc178b99836e4e7d4a36f1457a1d2fb3548c0d0c7d9f971d19d4f9d6d480b523b6ee76b8a05aa12c52c047a40072ed316bf9e80af6244eff8af2959b27c1a8b5e6e2b72359f170d992542581a819baa69750c5daabb9ea23eea9b9aaae7fd55c8d97c0dc779e9f42b81bf93d857087d97def21dc953af87317897e56fdd31c3d8311e25e236bb3f318f49cd46786cab0efee01df870ca5ccc6fa96832a4b5f6df6da6dfda829b14acde1a0f0a415b80212a61dd6c9b3755e2752ded97a5794715e2dd05da23c58bc9634d784d0278acbb05b134d13a43b8efdcf366efc6a9557b7865c44fa5b56940d0327c7eeb72a2c82707999725bd45f6c26f68f59aee0557f093c85dfedcad7d95fc0b62cffc56e0b7adf32675574316869844f56db50738f1add8eb451ec95bfadb2b7538e1646310f111f24ad7a2fce45cc076b7e9ddd025876dfa63c00dac7f16c33f694ad4b3649dd80984e6d8286e4af16adbbcb6ad37a154785f4464bfe8ae7fc79bb22bda45ddd09ede5bfccc602a9b2380789f2c167637609f161e2a05cfe8bd769ac465c7bbf9163d1fd88e773be891231976dfd93250fe213dd24f9463c0a11bf596c244745f15796af7eab6e83729bc9dc179bcb47cb5d4ef556469bad780389fb64d1cabb8c98f5ea84466824fbc1899f42a270098b6df5bfb237d192a0187c22257fb5b086d9f29ed8a5269df3e772299843f9b3036fa0cde237db97056f0844e315b8a3207db6b0b3059444bbff7536f655b06067eb28196c7701aacbdfe82ad80c637969852287f6379b752d69b27c5b9ff9d992d73fe420ebecefb3411b15d5fbec364947825855973fc4146cd4e6ea96be8ec81e92953831089fac9e015634efc43341eec350409e8771bacad6c3653994abf2c70dcc6558db42eb94567699eaa5e8448a6dae000d7ab14b5b074297067af5c11e9cf8559fe955c5adbd80a5ba9865bb43b195f7c9b796fba8f5b988e33e6a43ec7fb9b26114e07a65c769800b96fb7c65aa8598c320f50848cdd7ea13925adf7eb5d29c875cf07bc8c106cb6c6e22cf26481f3d653401c488340c93b0289bd014348f34069db2eb2b54c6da10aa4173526f175564f263b5eee7296e45d22defe61120141701f83ce666fb54e85c675fa2f5080f94a58a5cbc4a330fa5390ef8bc34dc53ccf315158ec8a9ffd5ee4c463c95509f47a8619cdeee0031b1bfcff919eed4119a36d919997e0635f05d2d9ee6ddc8476f90cf80e997ff328589875a65df9eca542b4e64c56fb3c1e048fb3bfe7b3b76fb3a7e3bcef229c2242708edc7eef69358a4abbdf9a5fbbb687fa0022566b38afe5bf474d7cbbb781355bd2db6d1b281fb9b242f4aaac32f5111d7450e0fdabd4e2283ed6df1e7baffe5324a939bb8283f65f771faf2f0ef473f1f3d3b3c385d275141a7b5f5cde1c1d7cd3a2d7e5dee8a32db44699ad56f135e1ede95e5f6d7e3e3a2aaa338da24cb3c2bb29bf268996d8ea35576fcecc793e7c72727c7f16a732c92376c515c7efc7bcba528566b163f0cc6397b02d9b517ef62715df5a285cc557c23d9a36341c722f90ba525a3cd7979b8db25e4df62bf7fbd209ec2d79787ffaba2faf5e0e23f162de10f071f72a2915f0f7e3cf8df8707bfefd66b7a32f7f2f0265a17126ee1da6b84d7f53f44f9f22eca0f0fc8b2f37d9cde96772f0f7ff989654c86bc996f1e6f92dda6397e6c799709c15c7de86bc7ae7f31b360d6f72f0f93b4fcc9bacf0c33f6254e188e7fc4f13dc8f56fd65c29ab5759ba2b16fd836c8d1c2d79fe9e95c9cdb7d05c694bcffae8cf55cf9f59f36323417908900b02e5c147be52e52b307975e7cbf16c97e7640a629f6b59da138983cab060066c359d7c8cf232956d9b1d27e6dd8543a7386a9f0e09f1542c9bc151fb34437a9de6016be9655a105edd9bb420dc98b76841f835efd082f0aa1fa1056125be3b0bc2547c6a8666ca3aae08af897b0a04ded5b7f1a1b46fa1b0ee94c004b63e2871b6390d3dbc83693d3ce00d18db174433240e5eeda9a7be5e3dcc31ad65bb949cf006168df4501837bec83323dca0d030231b90ab67772bffd0b3bf1d0fb6c3d5bec1db982884c625f91895659ca774d51fd7abb97d1f30012c5025b78fd1f27e18b0d476aa8f3fe0be8cee79b82085a7f630fad35a6cd987775b44d5fefc5594de7bf1a166d69b091b9c62283fe40d5d7383995c70e0533d4036030f5e17ec19e8a8f01c6a6f09bd6a6fde020d308f3597531a1929f3fbe11002e76e30e3a3a5f3404797178e1d849805704db7e053ac79294bccd666393b1a5a74e2bc8b39d5d0d964abe4a639a9589c2ccaea42b987ad14f83dd477ca6b8637eb2c725814722c9f056ee2b3f04d7c1eb889cfc337f1a7c04dfc297c137f0edcc49f1d9a686da93f45f96dccd8eb273bfd64a7c3d86911590eed905958b4c87a28f8ba2bca5cb240597d0a56840b0ce13e20602d595d14a7c4562584a065f325cbd6f66b1fefc3c6d7f14db45b9755b66fbf2ea99685281fdd74a683d34f56d0bca7dedb11159f8b823f9943e8073d804c599e71c34793cad33c1e38e221b7ab7c271c2039b41748c064d38e538fae6d2ef30f6ff5d1a2aac9663435eb1b64960c7a20e9524ae306913a7ea8790c05da08933240fb98733efd73084e6de6e7a1f6658d798ea7b1866ea7716d46644bbfada1f31ab38e2ea3b59ba8e8759d77792890e8f310230f7be0e4c388139e96d00319f4bfdd2d9af86b697bb182c956ece12fb2698a3dd84cbb4ee2521adb5e22ea698758197dd4e70a46de33d0a50646dc30e0c97d76fcfb9cc2b67a6e29f7d4a2b5e98bc39b335362dfc77d9db7cf146b597b4be8557bb86baaed6bbaa1f0e18e0c552e5e333260ddd80ed93e9faec75cd7e7d2f564c225d0f555ba943cd767ff47489a8b3e7386d66a135e46b69819f57958b147e08ab78d9833f08ed467fb9ccfd56a69c538ea00ef22c631e31e3a96f3a5ee979e7d0c5020a3d3e555f5f0d7fb77bc96686909bdb0ca2661b5acbf271d02afa6d4a638ac6a33999af1aab9b86f29e72ecba5a5901bbabdb14710c2aa2c85e137ea4d99482d10e28c8dbdbeb6d58541b79d296b3a9fa72862ae420f6f8d8f84ed7382d82741f59d18c22d669868401e3262620179497aa091ac4ccc897448e070fe0877041a00b67b87cd737fcb315493cd640809493e7d4e67db049fbeb8ef327cfa32ea337cfa72e2b37b7a08894fea69091c96d8073e5038bbf03b26ca54997b71ab7aa009720f5c034770ea61e9746bc67e1ee7126f7a0c5339dfa63733f025832bb34027dc4faf829f5e0563993abf0a0698d6a925079d72d4b916ad5f207a3e3ef4bdbae0f16c53cb4d0cb761cfcc3dba8221a2813706e4f48638adc3490dcdea86e6ccd93a1c68511a330be2840ae713340b155a5ed9ae49fafc60b617125a4adf1b384cf240f73d6b3e71a0c790e57301fa2e8bf621c08d90f6cff602184bed1b80c5c68db41da43e035499450f71410f1e60d6b68f49bc57f309134e4c4cbf179277b0cdc77d1845b6f005da24e5d9f33263626a3d0f667c46bdf023549b7c0ef930cd61687a8ec92e4d9d87a3df24a9538fba673fff623becc45c751ec7af7c8a3af7d959c84be7ce884b4717844d1841c9f9e63c980119e634d609d35f31b59c47e38064723e96a550c4c834791e2d61f5f30f07f47a70f2e78e7cf844c4419d0ff5180a68a9808c6d6398ab61de3bb5199a2c1dc09accef5e749f2ece7d4cb369e2b05c5cf50ea451435e72d4674d83ef54abb3a201e5e13c5b884b947ccbd433d2c933fb698eeb4458d663a1d6152a72846d1c5254f9d280a2705234b3caa71bf06d8b6d2f55659637f3d14ad3a448431a7465523484615744ea0b6de0d98c3c6c3d08c9f7a49e976ab69edbb35cb6b4107c7caf7d8c72528cc90486c3a93a2b9319a72afceccdad1dcba77b86a44b38812bf32c99e5cd90eeed353a2091d3d47b37f849419fe1684f2e844f7b85ba4f9f64597b4be8bba56cb5630581ad4fb6e435dd4c722bdfd2e07dd4a521b279df07a7f9c1bef13bf33fd6828d9eedb2d4bf1d6286a281d4a7cde833db0d54a78d1fdabf09367e4c1932a7f3be86f2def131819ae5cb879b66f8b62fd5a70e3037c48b6b67a934c1489e44d28ba4891af324122a125a078d65f6248e4e1c55749627793023e65d527ed7f2e883a0ec7b00c7ef337029dea1ada686272d3f6e2dd73ed193961fb7966940bf271d3f6e1d5751789f74fca8754c9ccf27153f6e15d7d13c9f94fc98947c5a14d932a9aaed36a5e5947b6c6a3541e9e7e9aada7555e4ea6b3a4a53931fc1052e77eb32a11bd4a4752f0f4fc47cf32f3ea4afe3755cc607a7cb3a91f959542ca3952c71d2c315a26d1d78154debbff32dfb27a94202d498a6fa4a227aa1969ec1277232fa8f79922e936db4d64949200207833185211541579bf8e575bca5077669a91148b0567495090a3209ecc53103473d4ad5295059ac827bf1fb81584d8e57b685ba628f19bfd814b8fb0b63459a553ca45d0ddd8f4747271256f60a98ae164d912b754c606ad2f44e08d01a74ac657d9f146a4f80250240d17ef8aeed28270a0c3254b191c744a7ae0da381514c78bb508b72d6989412f7caad928b3c3a2cead317cf19870dfa9a7cbfda942d03a81e354b5bc23d289c9c14ab4b44ed002e8b9a05354e8c2d36a7f242936079cef68d6bb6dc22fef3a3b36b6aa529eaee3341cf05777b3aaf3ee1ce0a7713cfa572cec085668b9959e842c93ab9952e5860141c027910d996419f8759121b139a2a30a1dca477d8a6b7c2b02983a4a205f01526e7e68e007bde5dac20af5d520bee250b27f1d3381bea21865f2098eb5c6f0506f46e1e7e17dd75804dee660266b71ef446bb2b1d02725a97bf8a7054af59666336f5e79c0a95ee9bcdb46b2f985e7612e4aa12d663f13b63bf41d535b07d72a1271f422fc5295a3de58068538c1a46c49c5670731c01d3ace6bc503cf19a4e8948f107d36166c3a727d05a6ca8d8778b5a8d507c5654632158249afa845e4ee65d27b74622380832863d2105d2950307a450a941f1eb849e00e84526715734a04b983e33cc3e22a33b6fd84e6776fd803ba9ad550baf026ee59b23d13be7e55db091f5dd2ff11c47d91e2ef2b82b0717e903e14a9a64dc369ee3da6e6ef09f767de708e159dc80e97048da7793ace345f702db60a5d54141b80d5f7529fca632dc52602cc84506411b321e8a42edc1fc03a9b7284bd83db09f0c77756b7b37d6eeb078f63eadd83d1ea7f2d7c7eac34a3d9dbdef2a23b3f905757f26807d7ad490f4305a44553190836c043cb654339ba4f7f45ad77ca6f029af77390d81899dc5f77174bb8beb98ca8d6dac7f3a4f75106ccb4810e43f8c0241b90b408bf8cf83408feb3a46f5358127f480eef9d63df692d9d485999a3e6fdc81cf04f6c8ea39426feae5714ee3bac64c70e8d751192daee2659c3cec1500e19e083e205ce431cdbd701731f5b73a9f1b16ebf8dd4f48fc7e90d8877a9f090e17ca1e0ca1f6d1606968db507074078632098127326d2aa78d9d1334f76f913c171ccec42cee839bd8a470f9f75ddcbab8fb873bb90f5c8ba0cf8f087740f7f60077553eee06704c2c884209388680d32ef7fb2870636ae45aae68155f6610e0b132c04d78b4a827ee545df46dc0c8e0db3b6be73010f6dabed9827b0e868d266359d46dd6de8de90acb9aac7f7de488ea7b8ad12c2d383298d4558e05a566f3180b26c04671bf7f0f80b2b05055f99131a5a97304509dad7745d93fc95afc9ead622dac04024ea1d2b751e0d5d44a5bae6b15ff7d10b889fdc7a8bfa5f1031dd447efdac7831fdbee459b46d47485cac1c0194254cd074953182e570c553903e369cd183b37feae4a791754d10628398033ac11b256e3ef609e3ffba9cf02b5ea2a47860cf5ebf6c999c2af11f6d81ed9ad06e6637ffe48d234ce69c30d56c85295b3b538d32cdb2cd0d86b64161bebed1a4ed58b417d1cb921caddf1e14133b245013a377fd30221a75a5daa37c7bd75fcc84f661c7150959fe6784653f53410dcbb7df2b9d8bde94f0503d53df67d5966c81860e76d66aa297410143a8d8801ac9eade9097244680d7d63d56363b011a1728bd76fd37bc601d4273952b6c60bd3cae922466ca9ecab5f3e1771be385b47c946edaa09c5f9c79fe2b7711ec5f3b556edd735ab2930ccbb4f41021810f8be82877a87a997969e0deade67b749bac7a8abdaaf6b5653e091a2aeeedddea10eb528f0c25ee0d931dc5ba4093013742531015a682ff6d844d11f75adaabf3f520355756efef6a9091bf196fc75d99d0a2e2ea3ad0677200d1894432c320a0aab4aa32dd02ce1cba0813884ae6390d092780291efa57fc5a32d2169339af5e3beeea1293a02340b28f38876d3547ddc8349f8ed3afb42b830ef8ace5749990d8242c37e86d414de9ac95f1f1182e4de61aaae553523ececdd93dd27ccd9626ee237ba8cdf497bb1a0fe246ad1a073cf21d7fc7b5a34a0bdf789160db4e06488a3d744840503d557cf76f126cf360bc5dae253b690198cb1d850cfb3407bd80aa0cf83ed924cbe7c3029c7a9f6d1d6104234532d308190d5049cc8609fc346aa5463357018d6f0c89d2e02e550615a4703ef1f71b4edd6befec8add9cd13b64ddb74986d8b7c3f808514365fb49ee69b6c17ceced6ece689d6a66d3ab4b645be1fb4420a9b2f5a6913aeef138c75955360b1686d1919911a287f1006a55d9b6084f69f8746e754997f4cea99372e99ac549ec0ac38cd0d9975a3d4d06cbe7f1fd80434345f70be4bca60f33be135cfc99d364c37b357dfbf9f695dd2d31ce1d967b5e050a701a9084f908311a09e29aa340fabc1e6702f6ee112430373f2444df8d7d9668d8ed4e43176fd2be7db19fd32f974d007dac2b1073e3f81be540bef9122bede1c7146bc4c3e1de281b6b0eca1cf4f882fd5c27ba4883fdd6c32770b2f104f68dfc59670cca58f4f482f55827ba4386fb76c9c705e11ce02e7524bb82795d2c7279c972ac13d529c9395b5b3391768a743b9d81096b7f4ed09e3a5426c8f14e2ac62aa9d4e37dfa5a29c05dce5a670dce5af4f902f95a27b14a0af37c4094d4928e2bc6d0869f69b242f4a9a14e34b54c890a554d771093c9a3f3ca8ff5779f1f77a79176fa29787dbea571a383dfa22df1b96300cd5c8441eefbaa0ac1f2eac6f0d43636c12b231fa66447c48f9ba2d3009be39fd78d6b5a62f856d4c4f81d2d655bcdd9515020d48910beab5249747b5e74d9e90c164680b5f48d50ebe9449339a831f59419ac2aad6888720f58127b255f249a9b24d7251738b6a1aab16212504cba628b33ceec10b9635b4a39bfe75c31a2aa468433b93e0c7f21bda66ddb8110bc0358ba570369636f7b42c8931884d83052c6bc644f5aff53a5e3567e7a876c9a976158d920beaad099f35dbd81839ffb4dc0eb98cb20972514303a00c9c520ba0426d13d6d5b7ae05505154130c95a3aa450b5dce2505485d2ed4897df78538b58cd4e5a2d64da853be219a51174437a52e6e680e14e44a6a0a5448d10ca8a845139458800a219a80c405172653aa9bfbda56baa13fc6fd00e40a61aa534a9bfbaaad0e29606556215dd57c49653380b298d6d4716be1faeb6fda8ed7450c1581c1b9a52ac1520a643165919a96321da8eac7d6dd96c3ba8c62040cd90b124bb4f53f30b34bfd9aceb2b226e093a9c6a698aada262e9665dd4dd81f53dd4d3155dd4d7424cbbaebb779a6aaeb52aa9aeb9827a68a85480e5295c2f7b6b25bf2f3265bf573a8500ee95e8b8fc1940eb65850d90e1501caaf034209281c3ba0a462d001254d2a919f06cb6a91cba8ea974ba26441bcc14d76a6d38da29cd1c3ad8bdb0c09f370500f05dc30805e264af541859440840a9b167bc2da99796b26aff9d465b14b7386c4b261ed832263abda82d826b5e52ddbd33e1931b6a72d886d4f5b1edd9efe8180a62d7d21c4fab42b6bd58666356b6844530ad70adc0219bcf36bd44b550aab94aab0c96982ef78caae135c0edec3501436d931e0da9d6cca80428a2d1ca0a4a105d03528a9055021b8055049930ca4cb29b204a4228afe4be54c6b43e9c280bc32948ac075cbe50c754be7b852d55209b866a99849e0f2799a2c71b98c61d750593d73c2a33f945870a71a0c99e68482a3118fa5980346cd19086914ee2843c55114a3c8502999635e3408b1a94f8c5841b42e852c422bfa31c5693e37abf8ea8a0d29dcf75151021a7512b49ed7a0889ba9c8ebda58045649b094625594377595f7dd99cec12ef84c85251e2af2e1fb9432d3930d2f3ad5212bc3cb785cea89afb35d4eef2288a74d0694c1542374d75f0dce02638f8d17fc19b24a5a6a92e1b1051d98337cb487e04144841b826a92c72622f9d079c1bb88b2884c249a7951731c5f4f8c88c3758ea3fa60bde2077df616196f5faabeab264155517587e073faaa33fab3f7f0820e83a6fafe9a1d9c781a4d3755cba3ba8fa6f5cee44852c631300a0d4939e65034ddfee1b8ca858613667b87c3419a02e990c67fb6e2137f50cf9db62cccc250df6fe3c401159bb1a895379d164ce013cd52d5403852c7a14597faa217bbe6824a0d28543d6851745321757ab16a629ad03faae9c66c1110d4534c55c194e6bbdeed2e18ea9c4903c51893d3f40213af622e8400a76ac1192935bea1e944b77611b107b380725457511935c945fc452a5e1535aefd0c1493d949d515da5a7ec61bb12144275cfac5c88e27190123530b0d1e88b83d193dd9f0fb32d30c51f92ef5a2bde7ac90968142dd4de80e76d539ddc56a800374919ce1a3bd17ee398f42351826528064482c4d2222f042f8e22a5ec6c983565028c241879ef68e7c63a850d7dd83898dcb6082179a3af1c9a316d982ab00232d8e62846e1ab81a380e2334e38c682019175fc38b487e4b62149189644811a99fd2547c102f63ec45c43cd8608fdb0b4836cab2ea2e010f55aabe685e9fa8e8a19727222fed3b123fe19880a32c3b24621cc4eb2a067aa37951b354ed3480e50c8d67dff1f44d879ee74cd5f1c6e143745d2e69e800a07acd2bb0090420bc195ad0874b2a1128cbaa3ba178dc547544faa61186e6a116cb8bff1e4a382cd74563a3757b2118b2e170338da8d8e1c13e7ed30f24a6e4a01df215b0a740a8b1c3d895aedcc056056f923d3bfe4792a6714e19e3f020951f706e191c01f223e3055781de350528864385fa7db5e4970e2a9e6a56033d534de901bb35b9f30e74db667d03918cb5be190b47dca61d237de4369f4c312002fcc5ed2ba0a68d9c136990104032dc72702ca1084fad17ed1b72e0a81a2ea939d1821fd4d7c758e237ddd9aae6ad3cc4ac29105c34ed1377b3689a92a38b867bca0f316b0a04178dc9166bcb0f2fa6a90653f326db2c8fbae0e87861df9b43bceaefc1aec808797d2fa32d2c1c4d71f3f939fcba9d3b3bd73f57e7b88261232a66fa4010aeb39314450179f66ba01bfef05719528261660e11612f3629ecc3e27c9594994660068a2145a58c785123ca18c12284784ca773068ac7251e21b2c6a209dea335d84c4194a1d51959c8c0cec0600f91af1d10eae069e1479820d401552a8e88e828ae53c59089cb95f3cc68d9d241d50d73372ef0e54567850e93cc5ba9ca1172874fa8443e740fa84145101ee79bf58364b7565fc81f3e99f684eae3231d81ea53c42c72be7459864ef7acbea819bc2ab3dac2bf6851078312d4a50cede4a3aad01990b5ca1a34ddf224eae2e26601fa828360b92a2c745660a5b2064d3f3ca13964a28b81b6100a15e670cc3f508e5ce86ac018e9784185857b24a58dbd569fc8a302a9392c5b83e7728596bcc3268c1d5839ea6874353f7374397bb5844f380aa865e0aca603ab451da2afe28708b9e7305a8266c58446ca706937871e258a78853537530442b7f017e112372a62650c9419726055a8c237d627eaa6808cf6aa08995b1050c460a90b075683229265c5cc149cd2fd64ac73b9dd4c881cc7457daa16baaa51cd955f381b8d825e1cd78cbae035ddb717c775b0d0e607f26799e5d16db5a3bb2eaa5f5f1c5fed08f526aeff7a1d17c96dcfe205e199c65560a29e695be622bdc9da2475428bda22ede7f65a615c46aba88c4ef332b9a12b8b3c5bd233b5f4f6f0e01fd17a478a9c6fbec4ab8bf4c3aedcee4ad2e578f365cded3bd32477bafa5f1c4b6d7ef1614bff2a4274813433215d883fa4af76c97ad5b5fb4db42e04a5a958d0c8ea6f63f27bad4b02a332befdd671fa3d4b918c1af17549ff3ec59bed9a302b3ea4d7d143ecd2b6cf45fc3ebe8d96f491eb43b2a23bc42a266645f0627ff13a896ef36853343c7a7af227c1f06af3f55ffe3f11eaf04919180400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190514228_AddedMatchState', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3e4509a199f3321ed0645511aaec4919694ceec5b47a9bb4896d95dd55355cd91ecf02ff3837f92ff8281bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdf17fffa75b33e7888f322c9d2978727473f1e1ec4e9325b25e9edcbc35d79f3cf7f3bfcd77ff9cfffe9c5f96af3f5e01f6db9e7b41ca14c8b97877765b9fdf5f8b858dec59ba838da24cb3c2bb29bf268996d8ea35576fcecc71fff7e7c72721c13168784d7c1c18bab5d5a269bb8fa83fc7996a5cb785beea2f565b68ad745f33bf9725d713df83ddac4c5365ac62f0fdf67f7d1d1759c93361f5da437795494f96e59eef2f8e875544657f1362b9232cbbf1d1e9cae938834f03a5edf1c1e44699a9551499affebe722be2ef32cbdbdde921fa2f5a76fdb9894bb89d645dc74ebd7be38b6873f3ea33d3cee095b56cb5d51661b4b8627cf1b911d8be44e823fec445a897bb35dc75f69b72bc9be3cfcb88ebec5f9f9d76d9c2744fd440a62adbf9ead734ac02ba0a6e3ffa7e7f2c3c1fabec81f9adf7fe8e0435046ffef8783b3dd9a2aee651aefca3c5aff70f071f7659d2cdfc5df3e65f771fa32ddadd76ccb49db3fe619615f7e6b1afe3e7e88d7870775cb2ed2f2f9b3c383df0955f4651d771a3dd6b2607bedc3e78f38be0779fdf29381d78b6346231845d1aa5e65e9aef0d153c7640435fd5b96a464747652a1fffe440c80b5907fcfcae4e65b1056b44d671931442da757df4a13176b4d9d45c5ddab681d61c614a71d86f08783ea336fec0651d36596c6df6c902bb3789da58c720641ff35955e51264b326d945192c6397618d4b3cbd1e97249f57ea462d708bc261a44d0ef92f5baf014741cad3c599c1605e9b827934f71b409d09debbbccb725bf25be1c2e88e7b3219ecfeb6813dd5a8158e6f56157de664ebc2c06c4e9176210a2655923f9a28c3704e2c94db26c3c0597d941cf7384c9a2240db4b0c932838768bdebe7878cd41fd0ae5f13df32a65239cb8ad22c615afa88a319cf9c9f6ed8e9cdcdabb928788bfe2acbd671940e20cfd749b1acdb6b27d3966e44b9e644b0b99f5c4f379bccd7df8cb6748ce379586885fef37d1cddee6222cf0c6b486a8a239e989b4d9b2283a885feb7150759df11db7b7870197d7d1fa7b7e51d716ea2af87076f92aff1aafda511d1e734210b5e6a78f29d596f5fbee48357f286e07915af82b8b9c4d9898be2136353e97ab8d103fbd1c17dfe982783ad74fe7d17efe2ab28bd452f461befae223ceac987f7e62e13ab610830a0a819641c5f46e5f2ee32de7c89f3d3bfa27c8595e5358105f9d8ca546433824c85a5c8840bfbab78bb6bb75e7cf8bca15e15e172b1e24662fb33fd897e0aa5fadf88af71ba5ae54493d60ba5b3352916e797514a7cd76e64411c8747c26f959f35b0c5fd98e5568e92cd10ac864de162caea6117dd933e1fb16cbe2393769e9649f94ddeaaac7eb65ce0345265390c2049f28dfb8191cd557cc3f5820e77418cc722b984d48eb2eee7db5d62321a2a26068789cee6fe032b8f37c96e739e2abc195b135e994c79bbda61afb6d94095d832df2cb9d23d3b8021b707683df7dc2674394eb11dc41d7c4fd646a764be7920e00fc2f06c97e7715aca935b0d4cc39498e445f931cacb5486b4c9be741e891d61e518ba1076fb84808aa12d49931c7f8f1e92db4ab192c77e97c40ff18648f57d42e7beab785d952bee922d3f022a1bb69008dee4d9e62a5b0bd6522cb7b826abd86a646588c29fa2fc362ef1bd1070d11a6c535f14648a1e81a5f5fd82496c7bf7861a9e95413d75a1a6067ede12fba32cdab58cef8cba7cdb796c4f2ed207228e2cff46f7527078034814fa914aea752317b7d54bc781fc7e93ac634387c4e2062d698b2b34a5a7b1d556bd6cafcd17764841340a7dc945f50a03cadb6aec639e3c90b9e7922c2ac85a43adafbad8d95d543645d5bad21795f564286fab236686c1aa08205168482aa957905cdc563ffdead73096fa82288ba72dae184b7a1a5b3d357b1c8c2761561444a3d0945c54af2aa0bcadae3e178a3e9c6eb7ebe600879651aa46574ed289b630a48c17c7fd6a0eb1c6633c10eb0d0c86f648cd6ddad51fd322f785a0c0c4634d18e2b428c8e294e952bb27c638dcc057a725502fb6b428e9daacafa7e9bb83bf6eb4f7100a170003d1a6e0e8140606496c6b6d147274938181194a1e5a1e36b2d133b2f6701a3fb06632dc720e9e330d6b3f27fb1cca32cfc9260376c5d62207375c189303cff78611cfcdf986b2d2c031124003c4055bfd90f18256cb6606c8aaf6153da1d5f1d04fd48cb0aaab126f63a226b2cc597d8cca32ce5342b98a1beb3aed7c1cc2f1a884f2315ade9b0513765a0f3ad2240f1b3b343dbc6c714de573a622f29a6ac4f5ed70196b3cb5f7298b279310a7c6ca5dfbab28bd67c6cbc92f4eceb43f17f0881d73e9d2e074197dd1a09b16b08789dbe8f018c1ec5eb0cfe865f94c3572fb8319db510b1fe94c3362a91c3d99d013a55898682b983798fa7317176553c87990f0c8c19d63f034da730cee0f7870a8cbbbadb2ecfae27a2683e84bb8412edfc7b6f58bf9c1de1d480037bd83baca072dffe6a6808339a8dae4600c5a3a8f015849abe7e17411fd8c0ccf5b2269c06d163f4d61a836ccbdfec5096f6c0c0f0b7c6a7a365a4dcf47abe9a7d16afa39684dead5cf43b44e08e1e96aa539f969abaf0d5cdf8885f80370d460472aaf8d2ce9edcfc589295cafe355258142b79f490b9c9665b4bc8b5bf30ff719535eb18d6720b2ee5d8d9478ad5bda2aa78785442eebd54c256f2ce14987de83f5b9e2a0dd8755df88f0720e44d087720d44be4f8ec1f7e018885ab765a71c86b5ec74a35069d60552c444c251288d8d816c0043a3ac59a445f49127c17752a0b3eda56a7a9fd64540f7dee06278596297055afda2f15554c447109bd6e8d25213dadccef01d9bcb8a56cc7a3767d646f6a268dd63bb67b1c0f6a97ffc92d7f14d4410707d9f887bc41443f467c75e0eb4f3cc3ee7a1fc8407d5ee2b16b59f4eb9579ffb53147e98f636c7505472ee4ce56d1d73c47d69e1ea32e4a12a8a48ad5795f3f2496591d8d9c18a16d0590833e860f6bafa5dec18473cd6816d08e359f118d082725ee5d4e65dbb4cd6fa72012d8be8bd602d5138df1ba851e9759bca62ba63f6b49dbcafaa163717ac363d4a66411c31eae6013b781c6cb06f6c5a51a2a73d777d22263e18004efa14a7411b25b6b4c24439d1f4c17a51b6b3c7301e581f6805f2c2faaf5e5653b965ece1bd8826c5e4e5b85992cd26735fc351ea2391c5686683efb1dcb52a6e8d47e72afa2399cd5c3a7876175112b753d2ba8f1d8b2390d95c7afa2ef1d86820c4470283b9f4ab5db2ba758c521e892ce6d2b53a5e93bbd66afa2399cd5c3ad8efa7557e81cf85a4fe2002641ae61442d165f92805db6d8abad0bdee79ceb0d3c0a164989e038ca7ba91167af9eff624aa5a85f8f10870f45535e37a9df1aee3f99fbb64cb7cf2dbc133dee0824ec2394acc793b43a07a1864a0b27e99ae3f2619e42a01be67a14e419a1a2a0f31a84160380e6a08a15b76185348fca990fdedd8cdb2b3b50f12b2bf2cc75976b95b0df8dfb31658cdb2bb62d48a400e8dc075b2175945113bbdb9ee083d2651fadfc1e3d95d14e7296dd3caf780f2a2f89caeb3e5bd3fa7203753fa4579a82b29d2c0c6078b91488df162040ac5046d2473f13d143718eb1a98e9bff905e892be281056455fdef6641479c13f68481fa47e02bee81185e66d78458693c5bce81be214ed8227f779e3d3709a7c3545574bc2cefe39d3cdf6f3708797f208559d5dea4bcac3445fdc3e44146792863563e6bec066cf69b843a1b9ac42ad37e35de6f39d06366dc2cefb310996ada80eafce8d705653ed67e7115e333b4f55c30208cfc691f4a3425f521a1486e203bddde36a728d67a7bd9daf897fe731befd47f6b463ba1f54b6633ac870ac3350306388fe934d4e3141985ef52d9b0a3bcad3ef602352843172003b3aa5628044741c794202846e9cee757955bddbf3f28ed4e7ed48cd8526aa9ce4a5ba06b65dc3b0e13739126df44da624e05e698b879f49c2c511457425e4c250645e0bde6b1c5216dffb581c7c2b2e882779515cc5d1ca7f13ee3a4e57de4ef655bc8c938780d6a76568356e2bfb2052ea462f408018c31095ad55aaa56edf3b9eced837b638b6671c8d97856ae2d73211879d1237c96ca6b25042a6005b2ba549346039de2a4e73886c436dd0376b13e43e59cb58504ed686a2d2703095b71de2152364579a2a9a8c3eaa5ef0a524875f53d4cbd997f97a0fe36957afdde8711ac0731876554870bb64256fc97fa93e80c731e227878c34fdd573f6d14d9d33acfeeef230f02e5ede07cbec1d26590efb3ab3ea249b95705ae35981c2d2e0b0247aa3d397d45a4ea0b8f5b3a4d60a0e6134316d17eceb2093577b728098bc80a2986e845b69722871cf1639add1ef4ca6f5da12b2b5b6373c8818e662ae2bf268ab62dc7fb1b5d6794cdfa67a9bd8eb32ca43f07993a4497117a64142e0c50a12545ac8708b221a76d4c07cb879c527b877c9f89ca4699c7f8a23e9e4d930f3d074c2541817e975bcccd2956d3b5c77bbd979812dda5b3eb88464f014c56c6d361d981a8bcdd6c216859bdb97d0369729661d6585681a25dbbe20dcd4f6bbb6a15d21db66f6b044ca5626809b2d96d3365f2aec3f09daed62a853267fa7c174434ca561c2590116d36725e014aea049bf2dce2b5c6e6e87a92a64464a91fd9fbbc818b502fdb6a38f5a0ffbfa6cde2bbeb0601c8032b061800a5aaf4e0cab2ce7194edcce314c84c15625ac6454cb115519ad98bd76cf3093072da393b1f01d96b058c86bab8c1dc01c627d270c96db0cb29638dd940f97976ce8f4236c920def44246e33a7c1c1468e676bb3090e119d7d751f2674e0f90c0c4a3fd55068fd07db3110c0ef70f2a182cc66e835856e2693161e211796b61302387ba9660d27a4375be0d5fe8fe51bff86f4324aa3dbfe2c056038d5206837b56c0701b41966bbd7544bc1fff1722edec6fd2d2bcae6770f3ff96e57becefe0a73e4408f4fe8d33d8a94306718552ec3327920500d74a999308b1dcea4e49d3516d7beb94c5a90e81e3c0865aabd19d174a8ca48764e59d0310f31a6e5acc016129dd4095d71c91c62684258c7a096716aabc89826ebd81041acda6544fcb434a67f686eb2d1545ade17d99aaa443b3a506d839d33cfdfc2ca865240bbdb41c4fb6493d40d8de920f73c8cb8becbea69ef2a8e8a6d9fb8ad3efb6ca6c44e5751e190f7e4f37645fa493bbbe3a7ed263707ffdd79e6e86781d05386c2c4aae71627bb2ae461c7da555ab6b5a6028ba9aca98b19f5b49fdd8b8f6e2c60d20e0a4cce3751b2d618c4673fff12c02256b51037f526c937019ee34745f15796af7e635fc30c7507fa3a5eee72825a325637dbc16bfb7897911971571fde8c575730d57cfa2b7b132dcb2c0f14c3e17db6bc27c6fa3c5dd129e973b9946728248320cda99f3dbe21608e570176dca8c132b83fd8f1e7b1f88812458405c1b42edaa24c9a0bb0849ccd022e66bbd5fe3ebb4d1461f2c51adaa2eaa6d6258c4d6d8a05bf6225d6a33ad0d0953336deeb6083b2c409bb29a96e7455c0d8daba54487fa2c29965188acab780f8fc70b0be2ff287a6dcdeb816c3c7eca712f2de262302a6f4834f7a554dff88d6bbd055b962b4323001305af1990ca355ede4e78764453d976333455b98b047956f5166fd8e9b6f9952e727cffe1664a2152702b69b63576e3d325d414c4d77000c533693411806185894b6d3058b012c655b75789536976a9d1eced4a7afd13dc1f511c767b2e578b34fe1347732b463cda1ed55e9b037a8891a82de9cee5e30b057c1eadf1c1f960c76bdb7955c0d439d23de849be409164448bc4bab2ea6ccef0b950d92329567ec3156618673c994ca0e0adb413ccc80925f55d9c15f73bd61ab586bf9c353955b5207652778d20b09c1a029331b34daacd8ee636d4f6be7a47a58511f33da75b771812ed29b3c22d5ef96b4dd470aa6534da1ed01aac3e06348a7be404d4fb89aa0017c48b309cfcc7c8389491031040ad59757440a351079198ab7ebec4bb4760dca24517fb77160828444395f25c477b5bf0f231c6c4a5ac11e6e4ac32bbddd71a2a9a38f353f3b7bae738e98a33408b56e34064192fa8227e94d81bea464040cc583879191eb534590d1974474245cdc98da2292ae6eb2336b9f078aaf2fb09a3612ea99a3ebcd534fed0004684735dbbb5eba71d9520bb09d36e956da049721a63e46a5f2379e9651a52c9a92e069595f40775ac694b2335fbb0d8b37f9f9c945f1661ddd165de7ad322c328f2ee4a72d4112bd7da6767bfd8de896dd23e21554afc7bbeb5129e94575daf3f2f04749995cd977c97abdf8443abaee284e1014afbe2dae929b755fcd331cd1759a10f8f0a4cf71a497499abcdda51dd94fc81aefb29225fb1947f6312118e865f20bb2b2cbb71dc97fc791bc4b939b5e167fc311bdcde3345af5647f478bb0a739312083c03c890b081f06803084124c4e0c38e16921b49c18e0c27310417362408d50bf809d130378786a014227060c095533483a314089a7e401756240144f2be2eac4002c49d88c4130c0eb2adeee6af3bab8ba78f3a9a733a04ba05bbc212d48573db901600cf9bb38de5673524b6a40964c2a556e40d7f9d72d1518bd48da6ca9b68406605dc6ab685d076d5abc8ae93dd5bb2c2be9c16ecbc0002e8941ed1cf5f4068849f454e72cbd016612fd6f719ef5d47f97a7f27ad2d64ce4624e6d9f595c4a403ee0f45dc522d0196b0196a6091c86a269147143ce5c56646f182ae74cd19f1c747b5a29a1c6e855fce78e20a6b9416fa9e56699a0e2d72898df911ecf51fb234acaca65c7a99ab9ae8ad372472056643492ff235e96315addafda7c6338d78cf34f7f7680c71951dd6d967f731cfac20104cf6d6a48546915b188a8936c62e140b3dc632140cd63b2443be9558652acfebbe46858a7fc639bd210e78dd30ccc7837bcc43adfac517399b0dae4d021262c2e6ffa946bcdd7f14d44386301fbc75d52c658bcbe5a47cb7b3460a37475163100372096b8ba71ca1118505b117c4cbec66b8ecab42448d60f0cd00dc87d9bad5758e4b270fc9bcb1c5bd98e6a1fd67b13a46275c4719c068ebfc5eb4d5cbe3ac1e2f155b6fac694360192e0714bfe9fa130a0f26394960553dc00cabaf957cfb098a4cd674a9bcc286d0c53dc04c675f610b3e5070664372d04c164c7ed48e43b0d323fc6d9761d5f46e8b9bd26f883f617edd5675fb292adc2e4e7d1f27c0da63530a5f88f2fcf9fa3e7fa6f5fb2fcf67d15481987d29ae2539ed18d662c566ba2d77996acceb734bcda389025de4310b0123e473daf6900daecfd2caede7ec042b4257983b6a02dc5e5bb675890f6ed7afd33da1d4d56449c8b132c4a9bf2684bda947f8e856753fe272c2eaf97511ae74cfb4dbb7875f9befda67dbbba7cdf7ee366704dd077c0b409cc8e2c71d31735b4ea855090d155b33ae2384e33c62e93755246f9b7d7517ebf382d0aea3af3dbdb063d700cc0ed711bfabbacbcdda1670b8e56d8dc368c478ef48a6e1694efa35dbabc632cbc6188722c1a9b20f1306da3363c6aa71c56806923956701a9c0b4952a70109460da48e5a905351886bd40ac5084c916085c54ba301e132d939b443d108c27451db9d329514f2dc8df7842d4518ae73ba6956047a892bb695ba363a014b9715a211caa25b842e6c669a6a307856e9c757a72d1f418e79f8e5410bbe9bc88a154c8dd7464c4705009de7474f4fec3bbd3c5654443897cca7745bf5d623a31aa085f7f3abdbeba461f165534afe2345eaf93cb7ec6361d115564d7bbf46df43f2d0e866aaa3223dec1df7f411f07b1de81d3e10fddfaaaf61c83dce1a08c8e0496f3bfba71bd242ba4afcfb1537f5dfc27ec6cdf14ff1b768aafcbff0d3b9dd7c54f7e514ddd28149cffb94bb695ceaed759e90a021a4730cfe8716671f45b94de4679ff8bf4c37fe3eb0cf338635898b0bbe1273f9a4a57769d296fe3e7ffe8e2e89f37bbfbd56e3611a9ff818ecc71e2439d7a5b0fed6747c53d76d4d57b74e8edc266ff12eb2fd3ed45ac635c6d2e627de05759c614364c161ff36443dc4de164cc783385beab94c94cceedf9d7ed3a2b928798597c9b9cc99ea65f809bdcc89e8631e006757c28efb84d0193c3589767da64d04a5d9e698fc52c2efa7ea8a1cfa6406f9f29b9d97096d391cc9633d3cdf71157fdd51ff853fe0de9ee8a35c2067b91ade8f3eb2cc71a98f7343171ae3231169aab855b0730f1d1db91c86e5a7dd55cd0fbf4759a5e95b650e214dfe776c2acaf67db4d7f3cb389a73e1b4f85be7d7d4db0595d13c3a2ff7d56943291f1824b1147397d74cb12194c3647f44c357be18ebac488a8760a6fc8c5c0aac3aada746fcd46d357bbf38859059b8eb57667714a5b8a56efee3adb9568c57e2e7ecf72a6b861dafbb72db6e4e7826f8769c23e63cee10c5d3cddf58e99cb0d3b2122869fc3cd319bd8e09caed74991ec365828fe96dcdee54981de41bf8e52d223e258e2af00e4f7ecdd3b8333d940fdbac4a2970adf6b152d664b74dc4a015280744c275a2677292f912be536b3250e097d024b1c10ced5f767517a0262ef5baaaa9d37244e933b09a5287ba3aab26dbc3acbab50b8c8815b91d411a8d19b6015cdf957e692a061f836376dd990e51fdaa0dad8110df2a8778fb12bf786452555ce401866a0868e4095a3322c1c3fe6f136ca638ec4c32df203b94d1282298dd31f193f2f18105f01972defb2ce504479b794f0876d5ca3b280c3c637021534b00f2b902e443ed6a87071edb15685375b033fa50023efbb2b1c60b7bfeabedaa565b289cff33c43efb634cf476a01f09406b5535cadb368d5080ae260c042cba136551003c3ccd2684fd302c3bcd2305037c07486bdabf6786e2add8e73474f19a1c5715e0163f1702361ec99e4f4215a275ff09788c812a320cd403bbad7db68a302396eb750886263b7dbc10a9ee7b407a76f573bb22cc2dfe03d4f6fc9f215bd8ff59a74acc0ef609dd16c4085d24793363aa26dc49537ed5fbccbf238c2ef61105ca56c67711b19a745912d930a282dfed97ca04d4e38361ba6a04b62500e9ae01206c23e1ac5b73e721e40737870499096d0481549f98d2a99ab91d4f9217d1dafe3323ea85f22d2276cc5b27a692d08887479e5dcdc2e2227d8dc3e8d17dfda7f921a7115dfc4398de01ad1ec25740d92a4a5d8a58f79922e936db4b614a6c0e7c090cab6b519c762fdc75d03c42fafe32d8d75949696720bd6b2ae01826e4d727d71cc805b8ff9fa2daba9674d682615a06c984063414d6fc698632bf4101f67443a0d0f975e8e3f545c10f148864d1de052b296ee4348cb7092e1a46f91dbecf1e3d191ecd3841e21a8865be2f05596ee8ae1860b4afb98262b184d3c849a7e300dab72ece9410913a987421b597014ef4adb44606cf88c568ff1a1972106507d7c416fe4eba5e5db98d1c0dc47f681020beb01a3a555435b24b3b7f0fa8a95801d614c79a01bd5a991418ed2f0de60bd69f6d92ea76cdb784728a8eb486d8db819dfdada94f0368dabd0ee0aa695186cf4ed0e6998b5aac2344b60303176eb20544d2fd83f0cc055d2a951cb71b7c6aebac23db5cbc60e8d6c938d1ac5b4a7a69b13a62dfc0e25dd28987e0cbe86b14353627a4f7d8cee115dbf10a63f759b072a8819e8204ccb2436983655086d86343973b80ecd0edbc88e61b0d4055f10a104bb0a42503e8ff1804403a655155db82e8c308404678d7640bbb5a2280f0d19c1db1f675f45d53ec711166833c520359cfba0738ad16037c8c7bf25d398fdda7838d87d8e7014c3cfd768c06545bb57e61fecdebed97f1015e13b51959ec534d062aed657e3d611b92007168e1cbce0a0a0b41964c8daf7d6c9b2ebdfacc79a1d50a6e8ca9483adf9a7f368e3e9471f6e42f57bb750b7ecd8680b764b7defc1c25dd923f107c3e694259fd14784a2199a99a82fbb87f391bebb3e0be810a3458f094ceb440e535f2ca24eeb695946cbbbb8ddc9ab7e436de91aa83557866442871b4386da3543c43452a71f20565d1c695858291dd3a67a693abf1180983630c413e0fff1cc10367d9c1cff6e73c1a4f65f8d85aa37d59a08e93a9959e8bc263318317e13a20d7bbf78c7f7712f16f078d83cd2453c77647a913e10aea449c6f3133dd91c2fa7ca8df49a9c86b89eaa94e3684b749cc4f66065de6e29343d68625b2ebaac2628cfca44ae86b94869ef5819eb566e4b897d9cdd5462d7c151fd2aacc651e6bd259d6e10f4115d191fd1b82fab2703412f5258e15d5fddbe2f2150bd1b0be428cdce7ed92077a3f9c564d5f57423209baf4f69c24dd3c71c610d760db501531471191cd8a066518e4bc36a669e8bc525572deda83ecb63b8f08aead4c80e3a4ac37be0a5b331a79bc6d73f9da73a98ebc92084cb1436e836d40700bbfd3c4f60e3fa83c14f4de689699c3a7d9b33f65e0a0039d43e05208d19eea600ad540e03ddb01be865995a88d36ca5a8c5b50756ba89d5ce04ee7a1d95d1e22a5ec6c98319dc186ad8f58608edfc6f4cd5fbe797d8746b3cbcdbe819d3aa96726eb8bfa6f53aa29ea51d15f35cc58f05f150a7a6c63ba4614c9b6aba19617dc102108fb605270c0cc2fdd0cdd507011b35a2e60871a86718243564c1e10d69d6a23d946c4ef0c66da8e8e94601f89e6fa2e03a34a1d9ded7cd932677c4bfefe2766581c3b4810ec2b44c62836953857b8769648746c33452a37b80e92a014ad305263e56a104b38a0042315376a42d1365eb00c83365b99e0f73efc42437dcc44ec93db16b12916f4346062dca02ab082c41abb3b9ca2af6ced89a7a329a9535696d5fcc2b4d18b8a8fba1bde20715562294961bd3a88aedd21bd4218da84242183450d210a05408c3af0963c1b1194458404ac595901cf57044d1b6e960a994136a42a51c4220532911cf568c004e21e9d582a67fd3c25345000154283b1248952d0460da946593de0d045793dc30506979f841d6241fef968c075b566f8b667d68bcf889a0d580d90e2a76b5aaece8d087ca166d1bcbae5a2809d3246a4f1ee2694d2d3b55d07e61964f52714b3fc0b878922b70b392e1c0a86f1646d9553bc34debb2f0fd1a3132d4a8ef8b7638dbc2f37337bb96a98c24b4361bc0d514253485a329ca62fe6e26dbfa3f92348d73da070b0b28120d6407a56a6cd0368c0d543569cca5b7410998a6f4b4b3384a6a976c0c6454f8d093990e92ecf168a86f2a2711d7acb1cc214e29f3378c503faa6d04f561909ac40d8b034dde9a66021076397b0d06635e7a18d054e40340981790674ba681aff5e93c40378a51ddef0323648730100a7e3aafd1a86f7bc67edfc0182403a4f56483e5c0916b72b2ae959f3035a6711d435bc52027f738b50669d3d8d86e50c11ed5a02027d30d866ea02abbc3a6815edea8db857239830353ad12b40b3c31324fb75467d52f9f0bd2fcb375946cd44e305c1c0c7ec5971c2b1f37dc3e28cc035fb22a38541a6eadd03048f18d47a5950aa601946c36187d9fdd26291ea375f1f962b4699f19a355c19130ca0b6d028cf252d93b8ca296633a2227bc6aa208eaaab25c85859bdc310298007b43ace826c020ed1cde4c56a5e76b25ebe6998d242d37928de42436014c3991ccdf423671b5de92bf2efbb3ddcb68abc1a89a461770952f3e1660356d05605b158bb6d8859a5758558df830b069c9034545d508c8bf35a32de669339ae59fcd0e2c8658bdac97e8ec57f886aaf76e43d6a657a3edcada68790f1c8bb7ebec0be1c2bc6a3c5f256566c2ba9e0c42b94461836f43759339b9b8768d864d9c5630cda9e966844a54f00c3dd908a8dcef6819b8fe4c88e63d8d91c138f7b4670beaaba3166f7d69c3e20d5a190dbf78639a3797c59b2cb171176fb24830f553aa91d0590f9e2ed367d3001a63e32ade6605b5fac4322d7745996da234cdca8ac3afa44b67eb9c6ab7787958e63bf92d00657b1d97ddedf065b64b4bdeded5ffab3487929de55936094cfaa7be5d270a256ba8b4b122fe15b5c4d997677bdfdbc0bbbf168e920bd1deaed655f53d01d98345b1927f9327048a065df285507cdb98a4659dcb4a9689266837aa827a201ab8cb49b50cbcaf499363be354ae6568d96d3f142aa84d232635acce5bf865122e62d476a91746fbd8e5755a3d40312ca1d651c3b8c2df9989375576940a11c9118d507261834c45a8ee86de0da056f8cbf688c2014eb12c5d8c011ab3e29ae0d683980e044d68ce92c83635e87da32e119b88223f1860a59f055ca182a64e0cbdd299618725f319c945de6bee23919a63d65494c0df492ba92697df1dfc0067c3025f1034be1389bb9a239d6fe3d301d88a72d26c307dc6340f06dee3b201ad99cb61b3936a7d376cdad8fa88cbceb758181b5b0a32ef114be63b8316708ca795b2c88e02b6ce12a2628a0a489b7b81886440bec666027be4d7696ad20f00b2590cd45281f543cb36a019de7cedb6163fe1c1e30641a2f9da311975acc0ad740daadc5bb4e8b6b06691d67c1bc5d49c3cc99c583b866e7458710ab7a89c436a855b82c622b7ab5446cd840a2d7ae0b359ab0aa57af9581b5f13e2a4a00034e9ad1f372929696e5481ad3b761ec31d554cbd4513d17566a4a51de2409980c364fc0768c51d20afe80342d34ea2c5471db823f9455ca564f661281965a8d6d6837c6286e7d5d20868d5af585f0d92ea7db8ce2968001c83015126e20717050c3b528a16d56a8b3a4d92db305bf7fa612b39ac4d47b25a51acbba2d3dab1a4646305737ce56a849ac7a8db3113e729dc632c87b8e7c5e4840ae261275af0d94905cc19d538d604d55008205767dbdc52ad8a02e8532204e5551751f151490f814fbc31a01aa988f2438407f75226d3b40f2345670e1480782245fc748a2551d8b98058ca4d4ac90510cd476953deed12dc471d54c2d6fe674c556e002a98328780ec3895ca867b4094dd920f107b5e360cbc2413a30a7e1b4a1a84fe91b0b07b8a17642e493c245f59b71dbc34068dc5fd0d3abe52e1e819a77320c352925aed6705071eb918fa273110106e341643d05b8d5b719aae654938fd9da20a8cd03dfcc44277f8d585c6b1b69aee5164fdd55019ddb6da0406e394884c13735e41a4636da527ee1b3bb882adfbcf436529a4460626014367fb3c42872637de3895eca616e5a951b28345dd712aacd0577b346275a3dff0945caa585c7c99427b1e93498833ea454f90a4cfeb66e64843315b82d3a8bfcf698416bdaaa0b6a21a6d9b533e44a07246d935d9deb3732bf3ad365f08a9c46b2c88cea520d81c1cb55cd5fdd33780fa6ecdeeac95d93df3b94ffa049ce6da73327938b49160d1a5eeb2cd38275b4c933cd1a61d50547ad29b6c92c3db86dc0642ac68b5cfd460b2303f0a9d630e2069f694d206c2e552e4aceeae4ba867e83e975b5d2b5952c9850d75e7f61046b74266cf2ba9a3a6e3ceff397ed34ae832953282058abe4a25cafb1e945d9dd01e3e56eab1a4693ab325b2520505c664bae9fc6dc964c073597c5713c01a161af8afb09ce84445c764565274dd87311dc84681393f7a904a64df227774c95e64f1454f300c0242755763e9cdc5d65d32804211db9a4a14312815242085fdd902b6e302129339a0162c2653fe3ba65cc7fc674ccf01403c7171018e6e188b3e0b439b5d432c4a7e282ba8d4ac6652701eb6a54c80c6bda9a11c12480320c5f55aa28e560039245790e5f203bd4e068045318194405a73a52764b4a76e429262945116a5ef1948d944dc720237df61d65df94f9773c65a64cb83394ec0c49600c8b048002e7c1cb84a625024290c84c318319344df212a41ca15427c62e0ac94ec2ca4f485162b57e0b23429b452b4462d55f9b45ab9b4467f02e404ee260daf836a47d506f4aab133fd875d9a686b1b0ca63454e1e6012aa29df80bacf9a8c03a1c4aac93430d8524511f31eba4a83888ecf5f64d1c7c767bad4bcffd65d8ad1c7b2673500bf010f2ea8f629b859505088766de78420ed5e821202aa0bbcda4e04978e6902c18708d7f6ce3475d8cb6b9aa9028e568d905b5d10dfbd3a4c721049d5accc23b0e94bb08b8050cc64cdcd3f638865f0f29d2ec832d3557dbc0334674088fa000dae13a72150af72f6b409f00bcc6fc810bf42f7b5f11eec6b196d301b22c40242b68929cb751c195596152c1016432359641cd909646a3a72b789886ae8b2e9a0dd5ba6d31cafc35136f5938e2a1ca76aa600027232fd02a70604abf0930e8de0497975afc8ba6f2f8eaf9777f1266a7e78714c8a2ce36db98bd6d555eca2fd40acf536496f8b9eb2f9e5e07a1b2de906f03f5f1f1e7cddacd3e2e5e15d596e7f3d3e2e2ad6c5d12659e65991dd9447cb6c731cadb2e3673ffef8f7e39393e34dcde378c909fc85d0daae26323409a084afa46ad2d237495e94f406c197a8206a385b6da46242544b5e789db0dbcad4812b657dd29f69549b9696febbb96d97dd4747d771fe10e74717e94d1e916972b72c77797cc437e74857452fed374400f4f4b49245cc20444f4e185c2fa37594b7c146a5c0bc67d97ab7494de17a4d9ce8bf215ef5ef16dcf27893ec880a57444a2247e11bc8f52cdb6cd7f15781edf957f25712a76de45c471df55c80aa555d7a1f3fc46bbe273da345f315ea8b8a21db190557b6880deb3fe2f81ec15e2c066ae25850055e5b94fdab2cdd153ecaea9958f4ffdfb2249581d7b15af4df6da4fa7b562637df747cd912369c697bcea89dd235b829105849675171e7a31f4aff8a74c972345d6669fc8def2de5b4687eb711deeb2c955452f16a3f384b0cacee2abe4de85a89ced23216e4af78a359073d2a93073205c89ce5af78ce42741471be003ee3795713f7c7282f53792212bfe1b90a093759a6865c9c6a9e427a5996a721f36cc5131e3fd7348c4c51264b9f41d431e9a3ff5a0c8077c97a5df0fde9182e9a8f56032a8e564a7ecd471b7ec4ad25b44a8edd671b9ef4b053db6fa6800ddfebbb4cddd2e6a30dbfdf1235bbfa9b0db78b9438e0c48b7c1d6daa6890305fb1944d0d1f76e56d66ac412ce566615f1c0b4eb1e8981f4b9eb9b060129d7ed492a079400c06371b726900069a775d28e098e99d7d36661eb882100ae02dede9467666dadf6c97242116374c47a8fe4496c067db595b8a3e28d6a12926d735d1c830dc470e3114020d023ff88780c3f42aeae35c0eae2ad535453b4da9b9201455ad83349aeabecfc32a84b18055a73e46cb7bb16ddc87d980b336cd406cc521a75529fd89eb946a66a45e12b694a29ef82f534c7daa359fdb62af59295e45e93db884ac3fd84d9e32b3fe573ca75ed22a0dcc6ea018227d861b22ba8ad0c343cf44b98e8797f00eabf7904382f6441e10edaf784e7445042c92c015d1a450e3036ad860eda2a0fffe70f35f2c1c076588a0ffeae240a8f345a1f5545109fa6e7fb3d893aa22098a506e7fb430a20421b7445090d3217e1b787cc07b5a9b6c95dc34c7958b13b78d2d596d970c579bedad92d4ce778a6fdfa22e60b3cbf110ad777a9e4d89c09bed5c25cf662ed8670308f6d918827d3e73c13e1f40b0cfc710ec4f3317ec4f0308f6a73104fbf3cc05fbf30082fdd94fb0beee54e8cddd9c2ceedd6f7fb42ecd871b96dd500ececc64f72e51641572101ce1f59d48ed8f38da7aec798882abd97d27b23304a374105fc771ae12f45a3e32018a475a3b8a41ab7d568e522ee0a775e320fb2aa29c459ed0f7d99b8a2e83430053d1f19aab91184076d7f74928d1515673959ca37995e3618f645f43edcab9edc7cddb225e14a76469927c590bcb17f6778b230ef9deb0e6b2b0ea46d64db45b97740088dd143e8d7030a4b8479ad1e8232eb6a21b03150b8b152674c64a792c7407ad6aa583f74829bbfed39e2f55d9415bcd433ee7fa15fd1e8ffef96865b3c9f482342bc3c8e24917385d54fb2ebeda403079d2074a1fddb2da5325483e4f5a4169e55d62f0fb8cfa307278d2044a13edb2c8431515f9932efc7551efa07a0e0c0c93c7ad0fc79534949973b831c7d7e4b2b38160a2d44f4b222987fd30fefe6445a2030f58c0625d1e6437762fd10de7f21d06dffa34ef086c9b188cb423f13a2996f53e80d7ae44c7c66667a23ebee5376a1a3e8bf6a3d5133ab2c053b1ab3ed9bd0eaf8efa6076edc73d7cd22524911cf4b85f4e3fe9fe940bc36bacb9a0ded6115f42b43f8e6dabab8aafd799f890a2ff7936165a9d596cd013162127993308cd8c943a2a8a587a3fd8fd88d735fd2fcfa4fec566dffe3c8dbe90f12f6edb773fdbf0fa9caeb3e5bdccacff7d8a33d67e23431cefec87d98c0a39b1e140a3414c89e83210cc3c94faed492525f39f6c42e8d4840a7e1358636a71452eed6fb3411c94cd6e18cc9953ff2140876132bc55a95b213d59ee7ec5738223efe8e3eda85e672ee342889dd0fe3633b48d00334f80d942cb0910f0328cf42e735b82519aa64acac3628523fb1294c342ed502841f8e54b0e30aa7fb661f4863887ab18888656f1e3be5ab5af1a109fa4dbdb752b998fb6d1a83ee6891838ace2c97c7b0a44256e380c13886a2ad70dc8723894f326d6e4e4bd999928315013c8af54bb9f2d3641fb5c980a7eeda721970fb3c14c9d13742cdc4069549db003331a113f2027ab2b639229b275c02e8aab385ac90be0f6578b85431540585a3a74bfdabcf2af132bcb7116fadf67331a801423c30c0473459873600413955a3c23eba97982dca6badf4e81ff4dc45df5d32c2137e8aac45c9125e46c5728f383471597529a03da1ff17cda140cd001a5f8cd06bcb744d4104ffe8bcd5664bcbc97a719e6e781b70de0754473b9da05d4157caea214f61d957a4f5271994c5ab0a87eb6593e5c465f413ef467e7b5d6be1a327dfec010164c5303c27469a987b610552251814dfbdbd476a6c90ca364da7fb2b034794cac8078e0d1fe6815ae2797f8743f5a3c9d48d2a4b81319f5bf0e13404889861d75ae3edcbc9222d7f25ff01cfb94a3a20af92f16b30fdde0fa946ce28bf43a5e66a9185f18fa3e2f5b34a803afa9016b8b6c5df67081b94259b5800f5101e4da63d6f2e116ec8d9cfe15e52b376f84516bcdc56f57b3e2e1b2ada94a245133744d51a18ad75773d547ed5373555d21abb91a2f92b9ef5e3f858137f27b0a030fb3fbdec3c02b75f0e72e127db5faa7397a0623c4ce46d666e731e839a9cf1d95a1e3dd83c60f198e998d172e076696bedaecd7dbfa515362959ac341e1492b7005244c3bac9367ebbc4ea4bcb3f5ae28e3bc5ae4bb448ab0787169ae09a14f149761b7379a2648f724fb9f6ddcf8d52aaf6e1eb988f4b7ac281b064e8edd6f556805e10234e5b6a8bfd84cec1fb35cc1abfe12780abfdb95afb3bf80ad5dfe8bdd36f6be65dfaae862d0d2089f86d9389ad6668d63affc6d95bd9d72b4308a7988f82069d57b712e623e58f3ebec16c0b2fb36e521d23e8ee7109bb6ef934d523720a6539ba021f9ab45ebeeb2dab45ec55121bdf392bfe2397fdeae482f695777427bf92fb3b140aa4cd0412285f0199d5dc28498382897ffe2951cab11d7de91e458743fe2f99c6fa244cc875bff64c983f8443749be118f53c46f161bc95151fc95e5abdfaa1ba5dc6632f7c5e602d3729753bd95d1662bde62e23e59b4f22e2366bd3aa1111ac97e70e2a790285cc2625bfdafec4db42428069f59c95f2dac61b6bc2776a94909fdb95c0ae650feecc01b68b3f8cdf675c21b02d17805ee28489f2dec6c0125e2ee7f9d8d7d152cd8d93a4a06db5d80eaf237ba0a36c3585e5aa1c8a1fdcd665d4b9a2cdff8677eb6e4f50f39503bfbfb6cd04645f53ebb4dd2912056d5e50f31051bb5b9baa52f2cb28764254e0cc227aba78415cd3bf14c90fb301490e7619caeb2f5709912e5aafc71037319d6b6d03aa5955da67a6d3a91629b6b44835e0ed3d681d0a5815e7db007278fd5678b55716b2f71a92e77d9ee506ce57df2ade53e6a7d2ee2b88fda10fb5fd06c1805b8a2d9711ae092e63e5f996a21e630483d825af3b5fa84b5d6b75fad34e72117fc2e73b0c1329bdbccb309f4474f194d0031220dc3242cca263405cd438f41a7ecfa0a95b136846ad09cd4db451599fce0adfb798a5b9174cbbb794808c556003e8fb9d93e153ad7d997683dc22367a92217afd2cc43698e033e510df79cf37c45852372ea7fb53b93114f25d4e7116a18a7b73b404cecef737eca3b7594a74d7646a69f410d7c578ba77937f2d11be43360fae5bf4c61e2a156d9b7a732d58a1359f1db6c3038d2fe8effde8eddbe8edf8eb37c8a30c90942fbb1bbfd2416e96a6f7ee9fe2eda1fa84089d9ac2208173dddf5f22ede44556f8b6db46ce0fe26c98b92eaf04b54c47591c38376af93c8607b5bfcb9ee7fb98cd2e4262eca4fd97d9cbe3cfcfbd1cf47cf0e0f4ed74954d0696d7d7378f075b34e8b5f97bba2cc36519a66f5db8497877765b9fdf5f8b8a8ea288e36c932cf8aeca63c5a669be368951d3ffbf1e4f9f1c9c971bcda1c8be40d5b14971fffde72298ad59ac50f8371ce9e4076edc5bb585c57bd68217315df48f6e858d0b148fe4269c968735e1eee7609f9b7d8ef5f2f88a7f0f5e5e1ffaaa87e3db8f88f454bf8c3c1879c68e4d7831f0ffef7e1c1efbbf59a9eccbd3cbc89d685845bb8f61ae175fd0f51bebc8bf2c303b2ec7c1fa7b7e5ddcbc35f7e621993216fe69bc79b64b7698e1f5bde654230571ffadab1eb5fcc2c98f5fdcbc3242d7fb2ee33c38c7d891386e31f717c0f72fd9b3557caea5596ee8a45ffa85b23474b9ebf676572f32d3457dad2b33e8274d5f367d6fcd868521e02e4024979f091af54f90a4c5eddf9723cdbe5399982d8e75a96f644e2a0322c98015b4d271fa3bc4c65db66c7897977e1d0298edaa743424c16cb6670d43ecd905ea779c05a7a99168457f7262d0837e62d5a107ecd3bb420bcea4768415889efce8230159f9aa199b28e2bc26be29e028177f56d7c28ed5b28ac3b253081ad0f4a9c6d5e440fef605a0f0f7803c6f605d10c8983577beaa9af570f734c6bd92e2527bc8145233d14c68d2ff2cc08372834ccc806e4ead9ddca3ff4ec6fc783ed70b56ff036260aa1b14d3e466519e7295df5c7f56a6edf074c000b54c9ed63b4bc1f062cb59deae30fb82fa37b1e2e48e1a93d8cfeb4165bf6e1dd1651b53f7f15a5f75e7ca899f566c206a718ca0f7943d7dc6036181cf8540f90cdc083d7057b063a2a3c87da5b42afda9bb74003cc63cde5944646ca1c813884c0f91fccf868e93cd0d1e59663072166015cd32df8346d5eca1233be59ce8e86169d38ef624e357436d92ab9694e2a16278bb2ba50ee612b057e0ff59df29ae1cd3a8b1c16851ccb67819bf82c7c139f076ee2f3f04dfc2970137f0adfc49f0337f16787265a5bea4f517e1b33f6fac94e3fd9e930765a4496433b6416162db21e0abeee8a321f2d50569fc615e10243b80f08584b5617c529b155092168d97cc9b2b5fddac7fbb0f1757c13edd6659531dcaf4baa6521ca47379de9e0f493153477aaf77644c5e7a2e04fe610fa410f2053a668dcf0d1a403358f078e78c8ed2adf09074830ed05123061b5e3d4a36b9bcbfcc35b7db4a86ab2194dcdfa069925831e48bab4d4b841a48e1f6a1e438136c2a42cd23ee69c4f211d82539b3d7aa87d5963aee469aca1db695c9b55d9d26f6be8bcc6aca3cb68ed262a7a5de76e1e0a24fa5cc6c8c31e388131e284a725f44006fd6f778b26fe5ada5eac60321e7bf88b6caa630f36d3ae93b8b4c8b697887ada2156461ff5f98691f70c74e98511370c78729f1dff3e2fb1ad9e5bca3db5686d0ae4f0e6cc941cf8715fe7edb3cd5ad6de127ad51eee9a6afb9a6e287cb8234395cfd78c0c5837b643b6cfc9eb31d7f5f9783d997049787d952e25e0f5d9ff1112efa2cf9ca1b5da8497912d66467d2e57ec11b8e26d23e60cbc23f5d93ee7f3bd5a5a318e3ac0bb8871ccb8878ee59cabfba5671f0314c8e874b9593dfcf5fe1daf255a5a422facb2895c2debef4987c0ab293d2a0eabda6ca866bc6a2eee5bcab9cb946929e4866e6fec1184b02ad361f88d7a5336530b84386363afaf6d7561d06d67ca9acee7298a98efd0c35be32361fb9c20f689547d2786708b19261a90878c9858405e921e68242b937b221d12389c3fc21d810680edde61f3dcdf720cd56433194242a2509fd3d93649a82feebb2ca1be8cfa2ca1019a6475cf14c00a971fd483119f16d4127a2cb10f00a18078e1f75c94c936f7e25ef64053ec1e38178ee0d4c3d2e9de8dbd27c0a5eef418a672c64e6f66e05b08576681cec89fde153fbd2bc632757e570c30ad93530e3ae5a8b3355abf61f47cbee87bf9c1e3e1a7969b18b0c39e997b7c06434c046f0cc80912715a87d3229ad50dcd99b37538d0a234e626c40915ce4868162ab440b35dd5f419c66caf34b494be777898f483eebbde7cea418f21cb6713f45d58ed43881c2171a0ed153296da37848b8d1b693b487d06a8320f1fe28a1f3cc0ac6d1f93baafe61326209998c02f24ef60db97fb308afcb750e44c7d5e664c4ccee7c18ccfc9177e846ad3d7219fb6390c4dcf31d925baf370f49b3477ea51f7ece75f6c879d98edcee300974f72e73e3b0b99eddc197109ed82b009232839639d073320479dc63a61fa2b26a7f3681c908ecec7b2148a289b26cfa325ac7efee1805e304efedc910f9f8838a8f3a11e43012d1590f36d0c7335cc8ba936c793a5035893f9ddacee13ceb98f6936d11c968babde81446cc86b92fabc6bf0ad6c755e35a03c9ca90b710d936f997a463a79663fcd719d08cb7a2cd4ba42458ed18d438a2ae31a50144eab6656f97403be6db1edb5acccf26e3f5a699a246b4883ae4cab8630ec8a587fa10d3c9bd387ad0721f99ed4f35aced6737b96cbb716828fefc591514e8a31b9c4703855e77532e354859fbdb9f763f9f8cf90b609277065a626b3bc19d2bdbd8807a4829a7aef063f29e87324edc995f2692f61f709982c6b6f097db794ad76ac20b0f5e99abca69b49eef55b1abc8fba4446362f04e14441d8578267fec75ab0d1b35d96fab743cc713490fab4398166bb81eab4f143fb37c1c68f29c7e674ded750de3b3eaa50b37cf970d30cdff6adfbd421ea8678b3ed2c95269cc993487a913471679e44424542eba0d1d09ec4d189a38aeff2240f66c4bc4bcaef5a1e7d18957d0f01f97d863ec53bb4d5d4f0a4e5c7ade5da277ad2f2e3d6320d09f8a4e3c7ade32a8eef938e1fb58e89f3f9a4e2c7ade23a1ee893921f93924f8b225b2655b5dda6b49cb48f4dce2628fd3c5d55bbae8a6c7f4d476972f323b8c0e56e5d2674839ab4eee5e18998b1fec587f475bc8ecbf8e07459a7423f8b8a65b492254e7ab842b4ad03afa269fd77be65ff245548801ad3646149442fd4d233f8444e67ff314fd265b28dd63a290944e060302641a422e86a13bfbc8eb7f4c02e2d350209d68aae3241412681bd3866e0a847a93a892a8b55702f7e3f10abc912cbb65057ec31e3179b44777f61ac48d48a87b4aba1fbf1e8e844c2ca5e01d3d5a229b2ad8e094c4da2df09015a838eb5acef9342ed09b0440028da0fdfb51de5448141862abaf298e8d4b56134308a2973176a51ce1a9352ea5fb95572914787457d02e439e3b0415f9331589bf46500d5a366694bb8078593936275a9ac1dc06551b3a0c689b1c566655e685234cfd9be71cd965bc47f7e74764dad3445dd7d2ee9b9e06e4fe7d527dc59e16ee2b954ce3ab8d06c31330b5d28dd27b7d2050b8c8243209322db32e8f3304b62634a540526949bf40edbf4561836e5a054b400bec2e4dcdc1160cfbb8b15e4b54b6ac1bd64e1247e1a67433dc4f00b04739debadc080decdc3efa2bb0eb0c9dd4cc0ecd683de6877a543404eebf257118eea35cb6ccca6fe9c53a1d27db39976ed0513d44e825c55ca7b2c7e67ec37a8ba06b64f2ef4e443e8a53845aba71c106d9252c38898d30a6e8e23609ad59c178a275ed3291129fe603acc6cf8f4045a8b0d15fb6e51ab118acf8a6a2c048b44539fd0cbe9c0ebf4d848040741c6b027a440c273e080142a35287e9dd01300bdc834f08a067429d76786d9476474e70ddbe9ccae1f7027b5b56ae155c0ad7c73247ae7bcbc0b36b2befb259ee328dbc3451e77e5e0227d205c49938cdbc6735cdbcd0dfed3aeef1c213c8b1b301d0e49fb6e9275bce85e601bacb43a2808b7e1ab2e85df54865b0a8c05b9c8206843c64351a83d987f20f5166509bb07f693e1ae6e6defc6da1d16cfdea715bbc7e354fefa587d58a9a7b3f75d656436bfa0eecf04b04f8f1a921e468ba82a0672908d80c7966a6693f49e5eeb9acf143ee5f52ea72130b1b3f83e8e6e77711d53b9b18df54fe7a90e826d190982fc8751202877016811ff7910e8715dc7a8be26f0841ed03ddfbac75e329bba3053d3e78d3bf099c01e593d47e84dbd3cce695cd798090efd3a2aa3c555bc8c9387bd0220dc13c107848b3ca6b917ee22a6fe56e773c3621dbffb0989df0f12fb50ef33c1e142d98321d43e1a2c0d6d1b0a8eeec0502621f044a64de5b4b17382e6fe2d92e782c39998c57d70139b142effbe8b5b1777ff7027f7816b11f4f911e10ee8de1ee0aecac7dd008e8905512801c71070dae57e1f056e4c8d5ccb15ade2cb0c023c5606b8098f16f5c49daa8bbe0d18197c7b67ed1c06c25edb375b70cfc1b0d1642c8bbacddabb315d615993f5af8f1c517d4f319aa505470693bacab1a0d46c1e63c104d828eef7ef01501616aa2a3f32a634758e00aab3f5ae28fb27598bdfb355ac859540c02954fa360abc9a5a69cb75ade2bf0f0237b1ff18f5b7347ea083fae85dfb78f063dbbd68d3889aae503918384388aaf920690ac3e58aa12a67603cad1963e7c6df5529ef822ada00250770863542d66afc1dccf3673ff559a0565de5c890a17edd3e3953f835c21edb23bbd5c07ceccf1f499ac6396db8c10a59aa72b616679a659b051a7b8dcc6263bd5dc3a97a31a88f233744b93b3e3c6846b62840e7e66f5a20e454ab4bf5e6b8b78e1ff9c98c230eaaf2d31ccf68aa9e06827bb74f3e17bb37fda960a0bac7be2fcb0c1903ecbccd4c35850e8242a7113180d5b3353d418e08ada16fac7a6c0c3622546ef1fa6d7acf3880fa2447cad678615a395dc4882d957df5cbe722ce1767eb28d9a85d35a138fff853fc36cea378bed6aafdba6635058679f72948000302df57f050ef30f5d2d2b341ddfbec3649f7187555fb75cd6a0a3c52d4d5bddb3bd4a116055ed80b3c3b867b8b34016682ae2426400bedc51e9b28faa3ae55f5f7476aa0aacecddf3e356123de92bf2ebb53c1c565b4d5e00ea401837288454641615569b4059a257c19341087d0750c125a124f20f2bdf4af78b425246d46b37edcd73d344547806601651ed16e9aaa8f7b3009bf5d675f0817e65dd1f92a29b3415068d8cf909ac25b33f9eb234290dc3b4cd5b5aa66849dbd7bb2fb84395bcc4dfc4697f13b692f16d49f442d1a74ee39e49a7f4f8b06b4f73ed1a281169c0c71f49a88b060a0faead92edee4d966a1585b7cca16328331161bea7916680f5b01f479b05d92c9970f26e538d53eda1a428866aa052610b29a801319ec73d848956aac060ec31a1eb9d345a01c2a4ceb68e0fd238eb6dddad71fb935bb79c2b6699b0eb36d91ef07b090c2e68bd6d37c93edc2d9d99add3cd1dab44d87d6b6c8f783564861f3452b6dc2f57d82b1ae720a2c16ad2d23235203e50fc2a0b46b138cd0fef3d0e89c2af38f493df3c6259395ca139815a7b921b36e941a9acdf7ef039b8086e60bce7749196c7e27bce639b9d386e966f6eafbf733ad4b7a9a233cfbac161cea342015e109723002d4334595e66135d81ceec52d5c6268604e9ea809ff3adbacd1919a3cc6ae7fe57c3ba35f269f0efa405b38f6c0e727d0976ae13d52c4d79b23ce8897c9a7433cd016963df4f909f1a55a788f14f1a79b4de66ee105e209edbbd8128eb9f4f109e9a54a708f14e7ed968d13ce2bc259e05c6a09f7a452faf884f35225b8478a73b2b27636e702ed7428171bc2f296be3d61bc5488ed91429c554cb5d3e9e6bb5494b380bbdc148ebbfcf509f2a552748f02f4f58638a12909459cb70d21cd7e93e4454993627c890a19b294ea3a2e8147f38707f5ff2a2ffe5e2fefe24df4f2705bfd4a03a7475fe47bc31286a11a99c8e35d1794f5c385f5ad61688c4d423646df8c880f295fb70526c137a71fcfbad6f4a5b08de92950daba8ab7bbb242a001297241bd96e4f2a8f6bcc91332980c6de10ba9dac19732694673f0232b485358d51af110a43ef044b64a3e2955b6492e6a6e514d63d522a48460d9146596c73d78c1b2867674d3bf6e584385146d686712fc587e43dbac1b376201b866b114cec6d2e69e96253106b169b08065cd98a8feb55ec7abe6ec1cd52e39d5aea2517241bd35e1b3661b1b23e79f96db21975136412e6a68009481536a0154a86dc2bafad6b5002a8a6a82a17254b568a1cbb9a400a9cb853ab1efbe10a79691ba5cd4ba0975ca374433ea82e8a6d4c50dcd81825c494d810a299a0115b56882120b5021441390b8e0c2644a75735fdb4a37f4c7b81f805c214c754a69735fb5d52105accc2aa4ab9a2fa96c065016d39a3a6e2d5c7ffd4ddbf1ba88a1223038b75425584a812ca62c52d352a60355fdd8badb725897518c80217b416289b6fe076676a95fd35956d6047c32d5d8145355dbc4c5b2acbb09fb63aabb29a6aabb898e645977fd36cf54755d4a55731df3c454b110c941aa52f8de56764b7ede64ab7e0e15ca21dd6bf13198d2c1160b2adba12240f97540280185630794540c3aa0a44925f2d360592d721955fd7249942c8837b8c9ce74ba5194337ab875719b21611e0eeaa1801b06d0cb44a93ea89012885061d3624f583b336fcde4359fba2c7669ce905836ac7d50646c555b10dba4b6bc657bda2723c6f6b405b1ed69cba3dbd33f10d0b4a52f84589f7665addad0ac660d8d684ae15a815b2083777e8d7aa94a6195521536394df01d4fd97582cbc17b188ac2263b065cbb934d195048b185039434b400ba0625b5002a04b7002a69928174394596805444d17fa99c696d285d189057865211b86eb99ca16ee91c57aa5a2a01d72c1533095c3e4f93252e9731ec1a2aab674e78f487120bee548321d39c507034e2b11473c0a83903218dc21d65a8388a6214192a2573cc8b062136f589112b88d6a5904568453fa638cde766155f5db12185fb3e2a4a40a34e82d6f31a14713315795d1b8bc02a099652ac8af2a6aef2be3bd339d8059fa9b0c443453e7c9f52667ab2e145a73a646578198f4b3df175b6cbe95d04f1b4c98032986a84eefaabc15960ecb1f1823f4356494b4d323cb6a00373868ff6103c88887043504df2d844241f3a2f78175116918944332f6a8ee3eb891171b8ce71541fac57fca0cfde22e3ed4bd577d524a82aaaee107c4e5f75467ff61e5ed061d054df5fb383134fa3e9a66a7954f7d1b4de991c49ca380646a12129c71c8aa6db3f1c57b9d070c26cef70384853201dd2f8cf567ce20feab9d396855918eafb6d9c38a0623316b5f2a6d382097ca259aa1a0847ea38b4e8525ff462d75c50a90185aa072d8a6e2aa44e2f564d4c13fa4735dd982d02827a8aa92a98d27cd7bbdd05439d3369a01863729a5e60e255cc8510e0542d3823a5c637349de8d62e22f66016508eea2a2aa326b988bf48c5aba2c6b59f8162323ba9ba425bcbcf78233684e8844bbf18d9f1242360646aa1c10311b727a3271b7e5f669a212adfa55eb4f79c15d23250a8bb09ddc1ae3aa7bb580d70802e92337cb4f7c23de751a806c3440a900c89a54944045e085f5cc5cb3879d00a0a4538e8d0d3de916f0c15eaba7b30b171194cf04253273e79d4225b701560a4c5518cd04d035703c76184669c110d24e3e26b7811c96f498c2232910c2922f5539a8a0fe2658cbd8898071bec717b01c9465956dd25e0a14ad517cdeb13153df4f244e4a57d47e2271c1370946587448c83785dc5406f342f6a96aa9d06b09ca1f1ec3b9ebee9d0f39ca93ade387c88aecb250d1d0054af79053681008437430bfa7049250265597527148f9baa8e48df34c2d03cd46279f1df430987e5ba686cb46e2f0443361c6ea611153b3cd8c76ffa81c4941cb443be02f61408357618bbd2951bd8aae04db267c7ff48d234ce29631c1ea4f203ce2d8323407e64bce02ad0bba600c570a850bfaf96fcd241c553cd6aa067aa293d60b72677de816edbac6f2092b1d63763e188dbb463a48fdce69329064480bfb87d05d4b49173220d120248865b0e8e2514e1a9f5a27d430e1c55c32535275af083fafa184bfca63b5bd5bc9587983505828ba67de26e164d537274d1704ff921664d81e0a231d9626df9e1c534d5606ade649be551171c1d2fec7b738857fd3dd8151921afef65b48585a3296e3e3f875fb77367e7fae7ea1c57306c44c54c1f08c2757692a22820cf7e0d74c31ffe2a434a30cccc2122ecc526857d589caf9232d308cc4031a4a894112f6a4419235884108fe974ce40f1b8c42344d65834c17bb4069b298832b43a230b19d81918ec21f2b503421d3c2dfc0813843aa04ac511111dc575aa183271b9729e192d5b3aa8ba61eec605bebce8acd06192792b553942eef00995c887ee0135a808c2e37cb37e90ecd6ea0bf9c327d39e507d7ca423507d8a9845ce972ecbd0e99ed51735835765565bf8172dea605082ba94a19d7c54153a03b2565983a65b9e445d5cdc2c405f70102c578585ce0aac54d6a0e9872734874c7431d01642a1c21c8ef907ca910b5d0d18231d2fa8b0708fa4b4b1d7ea137954203587656bf05caed09277d884b1032b471d8daee6678e2e67af96f0094701b50c9cd57460b5a843f455fc1021f71c464bd0ac98d048192eede6d0a34411afb0e6668a40e816fe225ce24645ac8c8132430eac0a55f8c6fa44dd1490d15e1521730b028a182c75e1c06a5044b2ac98998253ba9f8c752eb79b0991e3b8a84fd5425735aab9f20b67a351d08be39a5117bca6fbf6e2b80e16dafc40fe2cb33cbaad7674d745f5eb8be3ab1da1dec4f55fafe322b9ed59bc203cd3b80a4cd4336dcb5ca437599ba44e68515ba4fddc5e2b8ccb681595d1695e2637746591674b7aa696de1e1efc235aef4891f3cd977875917ed895db5d49ba1c6fbeacb97d679ae44e57ff8b63a9cd2f3e6ce95f45882e906626a40bf187f4d52e59afba76bf89d685a034150b1a59fd6d4c7eaf75496054c6b7df3a4ebf6729925123be2ee9dfa778b35d1366c587f43a7a885ddaf6b988dfc7b7d1923e727d4856748758c5c4ac085eec2f5e27d16d1e6d8a86474f4ffe24185e6dbefecbff077fece5a4981a0400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190518465_AddedMatchMemberState', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3e4509a199f3321ed0645511aaec4919694ceec5b47a9bb4896d95dd55355cd91ecf02ff3837f92ff8281bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdf17fffa75b33e7888f322c9d2978727473f1e1ec4e9325b25e9edcbc35d79f3cf7f3bfcd77ff9cfffe9c5f96af3f5e01f6db9e7b41ca14c8b97877765b9fdf5f8b858dec59ba838da24cb3c2bb29bf268996d8ea35576fcecc71fff7e7c72721c13168784d7c1c18bab5d5a269bb8fa83fc7996a5cb785beea2f565b68ad745f33bf9725d713df83ddac4c5365ac62f0fdf67f7d1d1759c93361f5da437795494f96e59eef2f8e875544657f1362b9232cbbf1d1e9cae938834f03a5edf1c1e44699a9551499affebe722be2ef32cbdbdde921fa2f5a76fdb9894bb89d645dc74ebd7be38b6873f3ea33d3cee095b56cb5d51661b4b8627cf1b911d8be44e823fec445a897bb35dc75f69b72bc9be3cfcb88ebec5f9f9d76d9c2744fd440a62adbf9ead734ac02ba0a6e3ffa7e7f2c3c1fabec81f9adf7fe8e0435046ffef8783b3dd9a2aee651aefca3c5aff70f071f7659d2cdfc5df3e65f771fa32ddadd76ccb49db3fe619615f7e6b1afe3e7e88d7870775cb2ed2f2f9b3c383df0955f4651d771a3dd6b2607bedc3e78f38be0779fdf29381d78b6346231845d1aa5e65e9aef0d153c7640435fd5b96a464747652a1fffe440c80b5907fcfcae4e65b1056b44d671931442da757df4a13176b4d9d45c5ddab681d61c614a71d86f08783ea336fec0651d36596c6df6c902bb3789da58c720641ff35955e51264b326d945192c6397618d4b3cbd1e97249f57ea462d708bc261a44d0ef92f5baf014741cad3c599c1605e9b827934f71b409d09debbbccb725bf25be1c2e88e7b3219ecfeb6813dd5a8158e6f56157de664ebc2c06c4e9176210a2655923f9a28c3704e2c94db26c3c0597d941cf7384c9a2240db4b0c932838768bdebe7878cd41fd0ae5f13df32a65239cb8ad22c615afa88a319cf9c9f6ed8e9cdcdabb928788bfe2acbd671940e20cfd749b1acdb6b27d3966e44b9e644b0b99f5c4f379bccd7df8cb6748ce379586885fef37d1cddee6222cf0c6b486a8a239e989b4d9b2283a885feb7150759df11db7b7870197d7d1fa7b7e51d716ea2af87076f92aff1aafda511d1e734210b5e6a78f29d596f5fbee48357f286e07915af82b8b9c4d9898be2136353e97ab8d103fbd1c17dfe982783ad74fe7d17efe2ab28bd452f461befae223ceac987f7e62e13ab610830a0a819641c5f46e5f2ee32de7c89f3d3bfa27c8595e5358105f9d8ca546433824c85a5c8840bfbab78bb6bb75e7cf8bca15e15e172b1e24662fb33fd897e0aa5fadf88af71ba5ae54493d60ba5b3352916e797514a7cd76e64411c8747c26f959f35b0c5fd98e5568e92cd10ac864de162caea6117dd933e1fb16cbe2393769e9649f94ddeaaac7eb65ce0345265390c2049f28dfb8191cd557cc3f5820e77418cc722b984d48eb2eee7db5d62321a2a26068789cee6fe032b8f37c96e739e2abc195b135e994c79bbda61afb6d94095d832df2cb9d23d3b8021b707683df7dc2674394eb11dc41d7c4fd646a764be7920e00fc2f06c97e7715aca935b0d4cc39498e445f931cacb5486b4c9be741e891d61e518ba1076fb84808aa12d49931c7f8f1e92db4ab192c77e97c40ff18648f57d42e7beab785d952bee922d3f022a1bb69008dee4d9e62a5b0bd6522cb7b826abd86a646588c29fa2fc362ef1bd1070d11a6c535f14648a1e81a5f5fd82496c7bf7861a9e95413d75a1a6067ede12fba32cdab58cef8cba7cdb796c4f2ed207228e2cff46f7527078034814fa914aea752317b7d54bc781fc7e93ac634387c4e2062d698b2b34a5a7b1d556bd6cafcd17764841340a7dc945f50a03cadb6aec639e3c90b9e7922c2ac85a43adafbad8d95d543645d5bad21795f564286fab236686c1aa08205168482aa957905cdc563ffdead73096fa82288ba72dae184b7a1a5b3d357b1c8c2761561444a3d0945c54af2aa0bcadae3e178a3e9c6eb7ebe600879651aa46574ed289b630a48c17c7fd6a0eb1c6633c10eb0d0c86f648cd6ddad51fd322f785a0c0c4634d18e2b428c8e294e952bb27c638dcc057a725502fb6b428e9daacafa7e9bb83bf6eb4f7100a170003d1a6e0e8140606496c6b6d147274938181194a1e5a1e36b2d133b2f6701a3fb06632dc720e9e330d6b3f27fb1cca32cfc9260376c5d62207375c189303cff78611cfcdf986b2d2c031124003c4055bfd90f18256cb6606c8aaf6153da1d5f1d04fd48cb0aaab126f63a226b2cc597d8cca32ce5342b98a1beb3aed7c1cc2f1a884f2315ade9b0513765a0f3ad2240f1b3b343dbc6c714de573a622f29a6ac4f5ed70196b3cb5f7298b279310a7c6ca5dfbab28bd67c6cbc92f4eceb43f17f0881d73e9d2e074197dd1a09b16b08789dbe8f018c1ec5eb0cfe865f94c3572fb8319db510b1fe94c3362a91c3d99d013a55898682b983798fa7317176553c87990f0c8c19d63f034da730cee0f7870a8cbbbadb2ecfae27a2683e84bb8412edfc7b6f58bf9c1de1d480037bd83baca072dffe6a6808339a8dae4600c5a3a8f015849abe7e17411fd8c0ccf5b2269c06d163f4d61a836ccbdfec5096f6c0c0f0b7c6a7a365a4dcf47abe9a7d16afa39684dead5cf43b44e08e1e96aa539f969abaf0d5cdf8885f80370d460472aaf8d2ce9edcfc589295cafe355258142b79f490b9c9665b4bc8b5bf30ff719535eb18d6720b2ee5d8d9478ad5bda2aa78785442eebd54c256f2ce14987de83f5b9e2a0dd8755df88f0720e44d087720d44be4f8ec1f7e018885ab765a71c86b5ec74a35069d60552c444c251288d8d816c0043a3ac59a445f49127c17752a0b3eda56a7a9fd64540f7dee06278596297055afda2f15554c447109bd6e8d25213dadccef01d9bcb8a56cc7a3767d646f6a268dd63bb67b1c0f6a97ffc92d7f14d4410707d9f887bc41443f467c75e0eb4f3cc3ee7a1fc8407d5ee2b16b59f4eb9579ffb53147e98f636c7505472ee4ce56d1d73c47d69e1ea32e4a12a8a48ad5795f3f2496591d8d9c18a16d0590833e860f6bafa5dec18473cd6816d08e359f118d082725ee5d4e65dbb4cd6fa72012d8be8bd602d5138df1ba851e9759bca62ba63f6b49dbcafaa163717ac363d4a66411c31eae6013b781c6cb06f6c5a51a2a73d777d22263e18004efa14a7411b25b6b4c24439d1f4c17a51b6b3c7301e581f6805f2c2faaf5e5653b965ece1bd8826c5e4e5b85992cd26735fc351ea2391c5686683efb1dcb52a6e8d47e72afa2399cd5c3a7876175112b753d2ba8f1d8b2390d95c7afa2ef1d86820c4470283b9f4ab5db2ba758c521e892ce6d2b53a5e93bbd66afa2399cd5c3ad8efa7557e81cf85a4fe2002641ae61442d165f92805db6d8abad0bdee79ceb0d3c0a164989e038ca7ba91167af9eff624aa5a85f8f10870f45535e37a9df1aee3f99fbb64cb7cf2dbc133dee0824ec2394acc793b43a07a1864a0b27e99ae3f2619e42a01be67a14e419a1a2a0f31a84160380e6a08a15b76185348fca990fdedd8cdb2b3b50f12b2bf2cc75976b95b0df8dfb31658cdb2bb62d48a400e8dc075b2175945113bbdb9ee083d2651fadfc1e3d95d14e7296dd3caf780f2a2f89caeb3e5bd3fa7203753fa4579a82b29d2c0c6078b91488df162040ac5046d2473f13d143718eb1a98e9bff905e892be281056455fdef6641479c13f68481fa47e02bee81185e66d78458693c5bce81be214ed8227f779e3d3709a7c3545574bc2cefe39d3cdf6f3708797f208559d5dea4bcac3445fdc3e44146792863563e6bec066cf69b843a1b9ac42ad37e35de6f39d06366dc2cefb310996ada80eafce8d705653ed67e7115e333b4f55c30208cfc691f4a3425f521a1486e203bddde36a728d67a7bd9daf897fe731befd47f6b463ba1f54b6633ac870ac3350306388fe934d4e3141985ef52d9b0a3bcad3ef602352843172003b3aa5628044741c794202846e9cee757955bddbf3f28ed4e7ed48cd8526aa9ce4a5ba06b65dc3b0e13739126df44da624e05e698b879f49c2c511457425e4c250645e0bde6b1c5216dffb581c7c2b2e882779515cc5d1ca7f13ee3a4e57de4ef655bc8c938780d6a76568356e2bfb2052ea462f408018c31095ad55aaa56edf3b9eced837b638b6671c8d97856ae2d73211879d1237c96ca6b25042a6005b2ba549346039de2a4e73886c436dd0376b13e43e59cb58504ed686a2d2703095b71de2152364579a2a9a8c3eaa5ef0a524875f53d4cbd997f97a0fe36957afdde8711ac0731876554870bb64256fc97fa93e80c731e227878c34fdd573f6d14d9d33acfeeef230f02e5ede07cbec1d26590efb3ab3ea249b95705ae35981c2d2e0b0247aa3d397d45a4ea0b8f5b3a4d60a0e6134316d17eceb2093577b728098bc80a2986e845b69722871cf1639add1ef4ca6f5da12b2b5b6373c8818e662ae2bf268ab62dc7fb1b5d6794cdfa67a9bd8eb32ca43f07993a4497117a64142e0c50a12545ac8708b221a76d4c07cb879c527b877c9f89ca4699c7f8a23e9e4d930f3d074c2541817e975bcccd2956d3b5c77bbd979812dda5b3eb88464f014c56c6d361d981a8bcdd6c216859bdb97d0369729661d6585681a25dbbe20dcd4f6bbb6a15d21db66f6b044ca5626809b2d96d3365f2aec3f09daed62a853267fa7c174434ca561c2590116d36725e014aea049bf2dce2b5c6e6e87a92a64464a91fd9fbbc818b5c26932ad3bed1bbd9809840f2f1fd8545a7c61c1de0065605b0315b45ef018166ece93a6b84364985b832d7458c9a85638aa325a317b6dc861e6235a462763e13b2c61b190d7ee1b6b1338c4face412cb719244271ba7c1f2ed5d9d0194dd8bc1ddeb94ddc266383cf8e1ccfd666131c223afbea3e4ce8c0f31918947eaaa1d0ba24b66320802be3e4960599cdd0cb14dd4c26ad6542ae556d270470f652cd1a4e486f76d5ab2d25cbb0010de9659446b7fdf10cc070aa41d0ee93d90e02687fcd76fbaa9682ff7be85cbce0fb5b5694cdef1eaef7ddae7c9dfd15e614839ec8d0d7801429618e45aaf48865f240a01ae89e3461163b1c73c9eb0b16d7be0b8c1624ba371442996abb47341daa32929d5316744c6d8c69392bb08544277542575c3287189a10d631a8659cda2a32a6c93adc4410ab7619113f2d8de91f9acb71343b97f7ddb8a62ad18e0e54db6047d7f3b7b0b2a114d0ee76b6f13ed9247543633ac83dcf37aeefb27adabb8aa362dbe782ab8f539b29b1d3555438a452f9bc5d917ed2ceeef869bb49f7c17f779e39fa5920f494a130b1eab9c5c9ae0aa9ddb17695966dada9c0622a6bea62463ded67f788a41b0b984c860293f34d94ac3506f1d9cfbf04b088552dc44dbd49f24d8017fe5151fc95e5abdfd80736435dabbe8e97bb9ca0968cd5cd76f0da3ede656446dcd5e741e3d5154c359ffecade44cb32cb038585789f2def89b13e4f57744afa5c2ee5190ac9204873ea97946f0898e355801d376ab00cee0f76fc792c3ea24411b44130ad8bb6289339032c2127c8808bd96eb5bfcf6e1345e47db186b6a8baa9750963539b62c16f6d89f5a80e3474e58c8df73ad8a02c71c26e4aaa1b5d1530b6b62e15d29fa8706619d9a2f22d203e3f1cacef8bfca129b737aec5f06900a884bcb7c9888029fde0935e55d33fa2f52e7455ae18ad0c4c008c567c26c368553bf9f9215951cfe5d84cd11626ec51e55b94d98e04a1654a9d9f3cfb5b9089569c08d86e8e5db9f5c874053135dd01304cd94c066118606051da4e172c06b0946dd5e155dadcd3757a8b539fbe46f704d7471c9fc996e3cd3e85d3dcc9d08e3587b6b7afc35eca266a087a19bb7b14c15eb4aa7f737cab32d88de15672350c758e7813c19227581021f12eadba9832653054364816569eb1c7588519ce25f92a3b286c07f130034a7ea865077fcdf586ad62ade50f4f55ba4a1d949de0492f240483a6cc6cd000b662bb8fb53dad9d93eaad467dcc68d7ddc605ba486ff28854bf5bd2761f29984e3585b607a80e838f219dfa4e363de16ae210f051d2263c33f38d4f2641c4107b545f5e117cd440e46528deaeb32fd1da35ce9344fddd8696091265e57c9510dfd5fe3e8c70b02969057bb8290daff476c789a60e68d6fcececb9ce39088fd220d4bad1180449ea0b9ea43705fa92921130140f1e9946ae4f1594465f12d19170a1686a8b48babac9ceac7d1e2864bfc06adae0aa678eae374f3db50310a01dd56cef7ae9c6654b2dc076daa45b69135c8698fa1895cadf785a4695b2684a82a7657d01dd691953cace7ced362cdee4e72717c59b75745b749db74adac83cba909fb604c91df799daedf537a25b768f885750bd1eefae47a5a417d569cfcbc31f25657265df25ebf5e213e9e8baa3384150bcfab6b84a6ed67d35cf7044d76942e0c3933ec7915e2669f2769776643f216bbccb4a96ec671cd9c78460a097c92fc8ca2edf7624ff1d47f22e4d6e7a59fc0d47f4368fd368d593fd1d2dc29ee6c4800c02f3242e207c1800c2104a303931e084a785d07262800bcf4104cd89013542fd02764e0ce0e1a905089d18302454cd20e9c400259e9207d48901513cad88ab1303b024613306c100afab78bbabcdebe2eae2cda79ece802e816ef186b4205df5e4068031e4efe2785bcd492da9015932a954b9015de75fb75460f42269b3a5da121a807519afa2751d076af12aa6f754efb2aca407bb2d0303b82406b573d4d31b2026d1539db3f4069849f4bfc579d653ff5d9ecaeb495b33918b69ba7d667129a7f980d37715de4067ac05589a2670188aa651c40d39735991bd61a89c33457f72d0ed69a5841aa357f19f3b8298e606bda5969b65828a5fa3607e477a3c47ed8f28292b971da76ae6ba2a4ecb1d815891d148fe8f7859c66875bf6a5398e15c33ce3ffdd9011e674475b759fecd71e80b07103cb7a92151656ac422a2cedb8985c3e96693612140cd63b2443be955d253acfebb7c6b58a7fc639b2511e78dd3a4ce7837bcc43adfac517399b0da7cd321262c2e15fb946bcdd7f14d44386301fbc75d52c658bcbe5a47cb7b3460a37475163100372096b8ba71ca1118505b117c4cbec66b8ecab42448d60f0cd00dc87d9bad5758e4b270fc9bcb1c5bd98e6a1fd67b13a46275c4719c068ebfc5eb4d5cbe3ac1e2f155b6fac694360192e0714bfe9fa130a0f26394960553dc00cabaf957cfb098a4cd674a9bcc286d0c53dc04c675f610b3e5070664372d04c164c7ed48e43b0d323fc6d9761d5f46e8b9bd26f883f617edd5675fb292adc2e4e7d1f27c0da63530a5f88f2fcf9fa3e7fa6f5fb2fcf67d159b1987d29ae2539ed18d662c566ba2d77996acceb73462db389025de4310b0123e473daf6900daecfd2caede7ec042b4257983b6a02dc5e5bb675890f6ed7afd33da1d4d56449c8b132c4a9bf2684bda947f8e856753fe272c2eaf97511ae74cfb4dbb7875f9befda67dbbba7cdf7ee366704dd077c0b409cc8e2c71d31735b4ea855090d155b33ae2384e33c62e93755246f9b7d7517ebf382d0aea3af3dbdb063d700cc0ed711bfabbacbcdda1670b8e56d8dc368c478ef48a6e1694efa35dbabc632cbc6188722c1a9b20f1306da3363c6aa71c56806923956701a9c0b4952a70109460da48e5a905351886bd40ac5084c916085c54ba301e132d939b443d108c27451db9d329514f2dc8df7842d4518ae73ba6956047a892bb695ba363a014b9715a211caa25b842e6c669a6a307856e9c757a72d1f418e79f8e5410bbe9bc88a154c8dd7464c4705009de7474f4fec3bbd3c5654443897cca7745bf5d623a31aa085f7f3abdbeba461f165534afe2345eaf93cb7ec6361d115564d7bbf46df43f2d0e866aaa3223dec1df7f411f07b1de81d3e10fddfaaaf61c83dce1a08c8e0496f3bfba71bd242ba4afcfb1537f5dfc27ec6cdf14ff1b768aafcbff0d3b9dd7c54f7e514ddd28149cffb94bb695ceaed759e90a021a4730cfe8716671f45b94de4679ff8bf4c37fe3eb0cf338635898b0bbe1273f9a4a57769d296fe3e7ffe8e2e89f37bbfbd56e3611a9ff818ecc71e2439d7a5b0fed6747c53d76d4d57b74e8edc266ff12eb2fd3ed45ac635c6d2e627de05759c614364c161ff36443dc4de164cc783385beab94c94cceedf9d7ed3a2b928798597c9b9cc99ea65f809bdcc89e8631e006757c28efb84d0193c3589767da64d04a5d9e698fc52c2efa7ea8a1cf66556f9f29b9d97096d391cc9633d3cdf71157fdd51ff853fe0de9ee8a35c2067b91ade8f3eb2cc71a98f734d771ae3231169aab855b0730f1d1db91c86e5a7dd55cd0fbf475e65f95b650e214dfe776c2acaf67db4d7f3cb389a73e1b4f85be7d7d4db0595d13c3a2ff7d56943291f1824b1147397d74cb12194c3647f44c357be18ebac488a8760a6fc8c5c0aac3aada746fcd46d357bbf38859059b8eb57667714a5b8a56efee3adb9568c57e2e7ecf72a6b861dafbb72db6e4e7826f8769c23e63cee10c5d3cddf58e99cb0d3b2122869fc3cd319bd8e09caed74991ec365828fe96dcdee54981de41bf8e52d223e258e2af00e4f7ecdd3b8333d940fdbac4a2970adf6b152d266074dc4a015280744c275a26775934912be53659260e097d4e4c1c10ced5f767f17aaa3906d714c3f651eaea3599bbb9f203eb0ac89360a9ac768e97384deed095a2ec8daacab6f1ea2cafc216238d6c4552470b476f585634e75f990b9d0653dbdc8a66c3cb7f6803a063ad2fc8a3dee9c7eeb2342c2aa972c6dce02d347404aa1c956191ff318fb7511e73241e2eac1fc86d12464c699cfec8f839dc80f80ab86c799735a12222bfa5843f6ce31a95051ce2bf11a8a0817d582d76e90cb04685cb4180b52abcd91af8d90b9825c15de100bbfd55f7d52e2d934d7c9ee7197a67ac79ea530b80a734a89de26a9d45ab4650100703165a0eb5a98218186696467b9a1618e6958681ba01a6fb06bb6a3feea6d2ed38f72995d1741ce715306e123712c69e494e1fa275f2057fe18b2c070bd20cb4a37bbd8d362a90e37676858843763b53ace0794e7b70527ab5230b23fc6debf3f4769d14e83dc7d7a463057eb7f18c666e2a943e9ab429156d23aebc69afe95d96c7117ebf89e02a653b8bdb743a2d8a6c99544069f1cfe66e6df2f7b1994b055d128372d004023110f69143bef5510e019ac3834b82b484461549ca6f54c95c8da4ce0fe9eb781d97f141fd6a943e372c96d5ab784140a4cb2be7e676d153c1e6f629d7f8d6fe93d488abf826ce69b4dd88669aa16b90242dc52e7dcc9374996ca3b5a530053e0786b4c3adcd3816eb3fee1a207e791d6f695caab4b4945bb096750d10746b92eb8b6306dc7accd7ef8e4d3d6bc268a90065c3041a0b6a7a33c61c5ba187f83823d26978b8f472fca1e2828847326cea60a492b5741f425a86930c277d8bdc668f1f8f8e649f26f4084135dc1287afb274570c375c50dac73459c168e221d4f4836958950f510f4a98483d14da2890a37857da260263c367b47a8c0fbd0c3180ea63417a235f2f2ddfc68c06e63e0a1314045a0f182dad1ada2299bd85d757ac04ec0863ca03dda84e8d0c729486f706eb4db3cf763965dbc6a642415d476a6bc4cdf8d6d6a684b7695c85765730adc460a36f7748c3ac5515a659028389b15b070c6b7ac1fe6100ae924e8d5a8ebb3576d515eea95d367668649b6cd428a63d35dd9c306de17728e946c1f463f0358c1d9a12d37bea63740f1efb8530fda9db3c5041cc4007615a26b1c1b4a9426833a4c96fc4756876d846760c83a52e50860825d8551002287a8c07241a30adaae8c27561842124386bb403daad15457968c808defe38fb2aaaf6398eb0409b2906a9e1dc079d538c06bb413efe2d99c6ecd7c6c3c1ee7384a3187ebe46032e2bdabd32ff60f7f6cdfe83a808df89aaf42ca6811673b5be1ab78ec80539b070e4e0050705a5cd2043d6beb74e965dff663dd6ec80324557a61c6ccd3f9d471b4f3ffa7013aadfbb85ba65c7465bb05bea7b0f16eeca1e893f1836a72cf98c3e2214cdd0cc447dd93d9c8ff4ddf5594087182d7a4c605a277298fa6211755a4fcb325adec5ed4e5ef51b6a4bd740adb93224133adc1832d4ae1922a6913afd00b1eae248c3c24ae99836d54bd3f98d00c4b481219e00ff8f6786b0e9e3e4f8779b0b26b5ff6a2c54bda9d64448d7c9cc42e73599c188f19b106dd8fbc53bbe8f7bb180c7c3e6912ee2b923d38bf48170254d329e9fe8c9e67839556ea4d7e434c4f554a51c475ba2e324b6072bf3764ba1e941138774d165a0417956267235cc454a7bc7ca58b7725b4aece3eca612bb0e8eea5761358e32ef2de97483a08fbecbf888c67d593d19087a91c20aeffaeaf67d0981eadd5820476976f6cb06b91bcd2f26abaea71b01d97c7d4a136e9a3ee6086bb06ba80d98a288cbe0c006358b725c1a5633f35c2c2eb96a6947f5591ec3855754a74676d0511ade032f9d8d0fde34befee93cd5c15c4f06215ca6b041b7a13e00d8ede779021bd71f0c7e6a324f4ce3d4e9db9cb1f75200c8a1f6290069cc70370568a57218e886dd402fcbd4429c662b452dae3db0d24d5c7d2670d7eba88c1657f1324e1ecce0c650c3ae374468e77f63aade3fbfc4a65be3e1dd46cf9856b59473c3fd35add711f52ceda898e72a7e2c88873a3535de210d63da54d3cd08eb0b168078b42d38616010ee876eae3e08d8a811354788433dc320a9210b0e6f48b316eda1647382376e43454f370ac0f77c1305d7a109cdf6be6e9e34d923fe7d17b72b0b1ca60d7410a665121b4c9b2adc3b4c233b341aa6911add034c332950d8f8588512cc2a0208c54cd991b64c94ad0320cf94e57a3eccbd1393dc70133b25f7c4ae4944be0d1919b4280bac22b004adcee62aabd83b636beac96856d6a4b57d31af34b9e3a2ee87f68a1f545889505a6e4ca32ab64b6f508734a20a0961d04049438052210cbf268c05c7661061012915574272d4c31145dba683a5524ea809957208814ca5443c5b31023885a4570b9afe4d0b4f15010450a1ec482055b610806953964d7a37105c4d72c340a5e5e10759937cbc5b321e6c59bd2d9af5a1f1e227825603663ba8d8d5aab2a3431f2a5bb46d2cbb6aa1244c93a83d7988a735b5ec5441fb85593e49c52dfd00e3e249aec0cd4a8603a3be59186557ed0c37adcbc2f76bc4c850a3be2fdae16c0bcfcfddec5aa63292d0da6c00575394d0148ea6288bf9bb996cebff48d234ce691f2c2ca04834901d94aab141db303650d5a43197de0625609ad2d3cee228a95db2319051e1434f663a48b2c7a3a1bea99c445cb3c6328738a5ccdf3042fda8b611d487416a12372c0e34796b9a0940d8e5ec35188c79e9614053910f00615e409e2d9906bed6a7f300dd284675bf0f8c901dc24028f8e9bc46a3beed19fb7d0363900c90d6930d960347aec9c9ba567ec2d498c6750c6d15839cdce3d41aa44d6363bb41057b5483829c4c3718ba81aaec0e9b067a79a36e17cae50c0e4cb54ad02ef0c4c83cdd529d55bf7c2e48f3cfd651b2513bc1707130f8155f72ac7cdc70fba0300f7cc9aae05069b8b542c320c5371e95562a980650b2d960f47d769ba4788cd6c5e78bd1a67d668c560547c2282fb40930ca4b65ef308a5a8ee9889cf0aa8922a8abca7215166e72c7086002ec0db1a29b0083b473783359959eaf95ac9b673692b4dc48369293d80430e544327f0bd9c4d57a4bfebaeccf762fa3ad06a36a1a5dc055bef85880d5b415806d552cda62176a5e615535e2c3c0a6250f1415552320ffd68cb698a7cd68967f363bb01862f5b25ea2b35fe11baadebb0d599b5e8db62b6ba3e53d702cdeaeb32f840bf3aaf17c95949909eb7a3208e512850dbe0dd54de6e4e2da351a36715ac134a7a69b112a51c133f46423a072bfa365e0fa33219af7344606e3dcd39e2da8af8e5abcf5a50d8b37686534fce28d69de5c166fb2c4c65dbcc922c1d44fa94642673d78ba4c9f4d03688c8dab789b15d4ea13cbb4dc1565b689d2342b2b0ebf922e9dad73aadde2e56199efe4b70094ed755c76b7c397d92e2d797b57ffafd21c4a769667d92430e99ffa769d2894aca1d2c68af857d412675f9eed7d6f03effe5a384a2e447bbb5a57d5f704640f16c54afe4d9e10281a74c91742f16d639296752e2b59269aa0dda80aea8168e02e27d532f0be264d8ef9d628995b355a4ec70ba9124acb8c693197ff1a468998b71ca945d2bdf53a5e558d520f4828779471ec30b6e4634ed65da50185724462541f9860d0106b39a2b7816b17bc31fea2318250ac4b14630347acfaa4b836a0e50082135933a6b30c8e791d6acb8467e00a8ec41b2a64c1572963a890812f77a75862c87dc570527699fb8ae76498f694253135d04bea4aa6f5c57f031bf0c194c40f2c85e36ce68ae658fbf7c074209eb6980c1f708f01c1b7b9ef80686473da6ee4d89c4edb35b73ea232f2aed70506d6c28ebac453f88ee1c69c2128e76db12082afb085ab98a0809226dee26218122db09b819df836d959b682c02f94403617a17c50f1ccaa05749e3b6f878df97378c09069bc748e465c6a312b5c0369b716ef3a2dae19a4759c05f376250d3367160fe29a9d171d42acea2512dba056e1b288ade8d512b16103895ebb2ed468c2aa5ebd5606d6c6fba828010c386946cfcb495a5a9623694cdf86b1c754532d5347f55c58a929457993246032d83c01db3146492bf803d2b4d0a8b350c56d0bfe5056295b3d9949045a6a35b6a1dd18a3b8f5758118366ad517c267bb9c6e338a5b020620c35448b881c4c1410dd7a284b659a1ce9266b7cc16fcfe994acc6a1253ef95946a2cebb6f4ac6a1819c15cdd385ba126b1ea35ce46f8c8751acb20ef39f2792101b99a48d4bd3650427205774e35823555010816d8f5f516ab6083ba14ca80385545d57d545040e253ec0f6b04a8623e92e000fdd589b4ed00c9d358c185231d08927c1d238956752c6216309252b342463150db55f6b847b710c75533b5bc99d3155b810ba40ea2e0390c2772a19ed126346583c41fd48e832d0b07e9c09c86d386a23ea56f2c1ce086da09914f0a17d56fc66d0f03a1717f414faf96bb78046adec930d4a494b85ac341c5ad473e8ace4504188c0791f514e056df66a89a534d3e666b83a0360f7c33139dfc356271ad6da4b9965b3c755705746eb78102b9e5201106dfd4906b18d9684bf985cfee22aa7cf3d2db48691281898151d8fccd12a3c88df58d277a2987b969556ea0d0745d4ba83617dccd1a9d68f5fc27142997161e27539ec4a6d3600efa9052e52b30f9dbba9111ce54e0b6e82cf2db6306ad69ab2ea8859866d7ce902b1d90b44d7675aedfc8fcea4c97c12b721ac92233aa4b3504062f57357f75cfe03d98b27bab27774d7eef50fe832639b79dce9c4c2e2659346878adb34c0bd6d126cf346b8455171cb5a6d826b3f4e0b60193a9182f72f51b2d8c0cc0a75ac3881b7ca63581b0b954b92839ab93eb1afa0da6d7d54ad756b260425d7bfd8511acd199b0c9eb6aeab8f1bccf5fb6d3b80ea64ca18060ad928b72bdc6a6176577078c97bbad6a184daeca6c95804071992db97e1a735b321dd45c16c7f1048486bd2aee2738131271d915959d3461cf457013a24d4cdea7129836c99fdc31559a3f5150cd0300939c54d9f9707277954da3108474e492860e49044a09217c7543aeb8c184a4cc6806880997fd8ceb9631ff19d331c3530c1c5f4060988723ce82d3e6d452cb109f8a0bea362a19979d04acab512133ac696b46049300ca307c55a9a294830d4816e5397c81ec5083a3114c616410159cea48d92d29d991a798a41445a879c5533652361d838cf4d977947d53e6dff1949932e1ce50b2332481312c12000a9c072f139a960808412233c50c66d034c94b907284529d18bb28243b092b3f214589d5fa2d8c086d16ad1089557f6d16ad6e129dc1bb0039898369e3db90f641bd29ad4efc60d7659b1ac6c22a8f1539798049a8a67c03ea3e6b320e8412ab26d3c0604b1545cc7be82a0d223a3e7f91451f1f9fe952f3fe5b7729461fcb9ed500fc063cb8a0daa7e066414121dab59d1382b47b094a08a82ef06a3b115c3aa609041f225cdb3bd3d4612faf69a60a385a35426e75417cf7ea30c9412455b3328fc0a62fc12e024231933537ff8c2196c1cb77ba20cb4c57f5f10ed09c0121ea0334b84e9c8640bdcad9d326c02f30bf2143fc0addd7c67bb0af65b4c16c88100b08d926a62cd77164545956b040580c8d649171642790a9e9c8dd2622aaa1cba683766f994e73bc0e47d9d44f3aaa709caa990208c8c9f40b9c1a10acc24f3a348227e5d5bd22ebbebd38be5edec59ba8f9e1c53129b28cb7e52e5a5757b18bf603b1d6db24bd2d7acae69783eb6db4a41bc0ff7c7d78f075b34e8b97877765b9fdf5f8b8a85817479b6499674576531e2db3cd71b4ca8e9ffdf8e3df8f4f4e8e37358fe32527f017426bbb9ac8d0248012be92aa494bdf247951d21b045fa282a8e16cb5918a09512d79e175c26e2b5307ae94f5497fa6516d5a5afaefe6b65d761f1d5dc7f9439c1f5da4377944a6c9ddb2dce5f111df9c235d15bdb4df1001d0d3d34a163183103d396170bd8cd651de061b9502f39e65ebdd263585eb3571a2ff8678d5bf5b70cbe34db2232a5c1129891c856f20d7b36cb35dc75f05b6e75fc95f499cb691731d75d47301aa5675e97dfc10aff99ef48c16cd57a82f2a866c67145cd92236acff88e37b047bb118a889634115786d51f6afb27457f828ab6762d1ff7fcb9254065ec76ad17fb791eaef5999dc7cd3f1654bd870a6ed39a3764ad7e0a64060259d45c59d8f7e28fd2bd225cbd17499a5f137beb794d3a2f9dd4678afb3545249c5abfde02c31b0baabf836a16b253a4bcb5890bfe28d661df4a84c1ec814207396bfe2390bd151c4f902f88ce75d4ddc1fa3bc4ce58948fc86e72a24dc64991a7271aa790ae965599e86ccb3154f78fc5cd3303245992c7d0651c7a48ffe6b3100de25eb75c1f7a763b8683e5a0da8385a29f9351f6df811b796d02a39769f6d78d2c34e6dbf9902367cafef32754b9b8f36fc7e4bd4ecea6f36dc2e52e280132ff275b4a9a241c27cc55236357cd895b799b106b1949b857d712c38c5a2637e2c79e6c2824974fa514b82e6013118dc6cc8a5011868de75a18063a677f6d99879e00a422880b7b4a71bd999697fb35d928458dc301da1fa1359029f6d676d29faa05887a6985cd74423c3701f39c450083408fce01f020ed3aba88f7339b8aa54d714ed34a5e6825054b50ed268aafb3e0fab10c602569dfa182defc5b6711f6603ceda3403b115879c56a5f427ae53aa99917a49d8528a7ae2bf4c31f5a9d67c6e8bbd66a57815a5f7e012b2fe603779caccfa5ff19c7a49ab3430bb816288f4196e88e82a420f0f3d13e53a1e5ec23bacde430e09da137940b4bfe239d11511b04802574493428d0fa86183b58b82fefbc3cd7fb1701c942182feab8b03a1ce1785d6534525e8bbfdcd624faa8a242842b9fdd1c2881284dc1241414e87f86de0f101ef696db25572d31c572e4edc36b664b55d325c6db6b74a523bdf29be7d8bba80cd2ec743b4dee9793625026fb673953c9bb9609f0d20d8676308f6f9cc05fb7c00c13e1f43b03fcd5cb03f0d20d89fc610eccf3317eccf0308f6673fc1faba53a1377773b2b877bffdd1ba341f6e58764339383393ddbb449155c8417084d77722b53fe268ebb1e7210aae66f79dc8ce108cd2417c1dc7b94ad06bf9c804281e69ed2806adf659394ab9809fd68d83ecab8872167942df676f2aba0c0e014c45c76bae466200d95ddf27a1444759cd55728ee6558e873d927d0db52be7b61f376f8b78519c92a549f2652d2c5fd8df2d8e38e47bc39acbc2aa1b5937d16e5dd201207653f834c2c190e21e6946a38fb8d88a6e0c542c2c5698d0192be5b1d01db4aa950ede23a5ecfa4f7bbe5465076d350ff99ceb57f47b3cfae7a395cd26d30bd2ac0c238b275de07451edbbf86a03c1e4491f287d74cb6a4f9520f93c6905a5957789c1ef33eac3c8e14913284db4cb220f5554e44fbaf0d745bd83ea3930304c1eb73e1c57d25066cee1c61c5f93cbce068289523f2d89a41cf6c3f8fb9315890e3c60018b757990ddd8bd44379ccb77187cebd3bc23b06d6230d28ec4eba458d6fb005ebb121d1b9b9d89faf896dfa869f82cda8f564fe8c8024fc5aefa64f73abc3aea83d9b51ff7f04997904472d0e37e39fda4fb532e0cafb1e6827a5b477c09d1fe38b6adae2abe5e67e2438afee7d958687566b1414f58849c64ce20343352eaa82862e9fd60f7235ed7f4bf3c93fa179b7dfbf334fa42c6bfb86ddffd6cc3eb73bace96f732b3fef729ce58fb8d0c71bcb31f66332ae4c486038d063125a2cb4030f350eab7279594cc7fb209a153132af84d608da9c515b9b4bfcd06715036bb6130674efd87001d86c9f056a56e85f464b9fb15cf098ebca38fb7a37a9db98c0b217642fbdbccd03602cc3c01660b2d2740c0cb30d2bbcc6d0946699a2a290f8b158eec4b500e0bb543a104e1972f39c0a8fed986d11be21cae62201a5ac58ffb6ad5be6a407c926e6fd7ad643eda46a3fa982762e0b08a27f3ed291095b8e1304c20aaa95c3720cbe150ce9b589393f76666a2c4404d20bf52ed7eb6d804ed73612af8b59f865c3ecc0633754ed0b17003a55175c20ecc6844fc809cacae8c49a6c8d601bb28aee268252f80db5f2d160e55006169e9d0fd6af3cabf4eac2cc759e87f9fcd6800528c0c3310cc1561ce81114c546af18caca7e609729bea7e3b05fe371177d54fb384dca0ab1273459690b35da1cc0f1e555c4a690e687fc4f3695330400794e2371bf0de1251433cf92f365b91f1f25e9e66989f07de3680d711cde56a175057f0b98a52d87754ea3d49c5653269c1a2fad966f970197d05f9d09f9dd75afb6ac8f4f9034358304d0d08d3a5a51eda425489440536ed6f53db9926338c9269ffc9c2d2e431b102e28147fba355b89e5ce2d3fd68f174224993e24e64d4ff3a4c0021251a76d4b9fa70f34a8a5ccb7fc173ec538e8a2ae4bf58cc3e7483eb53b2892fd2eb7899a5627c61e8fbbc6cd1a00ebca606ac2db275d9c305e60a65d5023e4405906b8f59cb875bb03772fa5794afdcbc1146ad3517bf5dcd8a87cbb6a62a9144cdd03545852a5e5fcd551fb54fcd557585ace66abc48e6be7bfd1406dec8ef290c3cccee7b0f03afd4c19fbb48f4d5ea9f86f1afa6f72e4688bf8daccdceebd073529f5d2ac3cfbb079e1f32a4331b735c0eee2c7db5d9f3b7f5c5a6c42a35a983c29356e00a4898765847d1d6019e487967eb5d51c679b551e0126dc2e2d5a6b926843e515c86dd22699a20ddb5ec7fb6590aac5679757bc945a4bf6545d93070720e7fabc2330897a829b745fdc5c639f898e50a5ef597c06ec0ddae7c9dfd056c0ff35fecb6c2f72d83574517839646f8f4a89ca376a48d62affc6d95bd9d72b4308a7988f82069d57b712e623e58f3ebec16c0b2fb36e541d43e8ee7101bbfef934d523720a6539ba021f9ab45ebeeb2dab45ec55121bd1593bfe2397fdeae482f695777427bf92fb3b140aa6cd241a28df059a15d428d983828b710c46b3d5623aebd67c9b1e87ec4f339df44899853b7fec99207f1896e927c231ec988df2c36a3a3a2f82bcb57bf55b752b90d69ee8bcd25a8e52ea77a2ba3cd56bc09c57db268e55d46cc7a75ca233492fde0c44f2151b884c5d6fc5fd99b6849500c3ed592bf5a58c36c794fec529356fa73b914cca1fcd98137d066f19bed0b873704a2f10adc51903e5bd8d9024ae6ddff3a1bfb2a58b0b375940cb6bb00d5e56f74156c86b1bcb4429143fb9bcdba9634597e35c0fc6cc9eb1f72b077f6f7d9a08d8aea7d769ba42341acaacb1f620a366a73754b5f69640fc94a9c18844f56cf112b9a77e2b922f7612820cfc3385d65ebe1b22dca55f9e306e632ac6da1754a2bbb4cf5627522c536579106bd60a6ad03a14b03bdfa70104e40abcf38abe2d65e04535d10b3dda1d8cafbe45bcb7dd4fa5cc4711fb521f6bfe4d9300a70cdb3e334c045cf7dbe76d542cc61907a04c6e66bf5098dad6fbf5a69ce432ef87de86083653637a267132c909e329a0062441a864958944d680a9ac722834ed9f5352c636d08d5a039a9b78b2a32f9d15cf7f314372be99677f318118acf007c1e73b37d2a74aeb32fd17a8487d252452e5ea59987d21c077ce61aee49e8f98a0a47e4d4ff6a7726239e4aa8cf23d4304e6f778098d8dfe7fc1c78ea48519bec8c4c3f831af8ae164ff36ee4a337c867c0f4cb7f99c2c443adb26f4f65aa1527b2e2b7d96070a4fd1dffbd1dbb7d1dbf1d67f91461921384f66377fb492cd2d5defcd2fd5db43f508112b35945212e7abaebe55dbc89aade16db68d9c0fd4d921725d5e197a888eb228707ed5e2791c1f6b6f873ddff7219a5c94d5c949fb2fb387d79f8f7a39f8f9e1d1e9cae93a8a0d3dafae6f0e0eb669d16bf2e7745996da234cdeaf70d2f0fefca72fbebf17151d5511c6d92659e15d94d79b4cc36c7d12a3b7ef6e3c9f3e39393e378b53916c91bb6282e3ffebde55214ab358b1f06e39c3d81ecda8b77b1b8ae7ad142e62abe91ecd1b1a06391fc85d292d1e6bc3cdced12f26fb1dfbf5e104fe1ebcbc3ff5551fd7a70f11f8b96f087830f39d1c8af073f1efcefc383df77eb353d997b797813ad0b09b770ed35c2ebfa1fa27c7917e5870764d9f93e4e6fcbbb9787bffcc4322643decc378f37c96ed31c3fb6bccb8460ae3ef4b563d7bfba5930ebfb9787495afe64dd678619fb9a270cc73fe2f81ee4fa376bae94d5ab2cdd158bfe61b8468e963c7fcfcae4e65b68aeb4a5677d14eaaae7cfacf9b111a93c04c805a3f2e0235fa9f21598bcbaf3e578b6cb733205b14fbe2ced89c44165583003b69a4e3e467999cab6cd8e13f3eec2a1531cb54f8784b82e96cde0a87d9a21bd70f380b5f4ba2d08afee5d5b106ecc7bb620fc9ab76c4178d50fd982b012dfae05612a3e574333651d5784d7c43d0502efeadbf850dab75058774a60025b1f9438dbdc8a1edec1b41e1ef0068ced0ba2191207aff6d4535faf1ee698d6b25d4a4e78038b467a288c1b5fe499116e506898910dc8d5b3bb957fe8d9df8e07dbe16adfe06d4c1442e3a37c8cca32ce53baea8febd5dcbe0f980016a892dbc768793f0c586a3bd5c730705f46f73c5c90c2537b18fd692db6ecc3bb2da26a7ffe2a4aefbdf85033ebcd840d7031941ff286aeb9c18c3238f0a91e209b8107af0bf60c7454780eb5b7845eb5376f810698c79acb298d8c947906710881734898f1d1d279a0a3cb4fc70e42cc02b8a65bf0a9debc9425668db39c1d0d2d3a71dec59c6ae86cb25572d39c542c4e166575a1dcc3560afc1eea3be535c39b7516392c0a3996cf0237f159f8263e0fdcc4e7e19bf853e026fe14be893f076ee2cf0e4db4b6d49fa2fc3666ecf5939d7eb2d361ecb4882c8776c82c2c5a643d147cdd15654e5ba0ac3e152cc20586701f10b096ac2e8a5362ab1242d0b2f992656bfbb58ff761e3ebf826daadcb2aebb85f9754cb42948f6e3ad3c1e9272b68fe55efed888acf45c19fcc21f4831e40a66cd3b8e1a349296a1e0f1cf190db55be130e90a4da0b2460d26bc7a947d73697f987b7fa6851d564339a9af50d324b063d9074a9ad7183481d83d43c86026d844999a87dcc399f863a04a73603f550fbb2c67ccbd35843b7d3b83633b3a5dfd6d0798d594797d1da4d54f4baceff3c1448f4f99091873d701264c4094f4be8810cfadfee164dfcb5b4bd58c1644df6f017d974c91e6ca65d2771a9956d2f11f5b443ac8c3eea731623ef19e85214236e18f0e43e3bfe7d6e635b3db7947b6ad1da34cae1cd9929c1f0e3becedb67acb5acbd25f4aa3ddc35d5f635dd50f87047862a27b01919b06e6c876c9fd7d763aeeb73fa7a32e112f9fa2a5d4ae2ebb3ff2324ef459f39436bb5092f235bcc8cfa7cb0d82370c5db46cc197847eab37dcee78cb5b4621c75807711e398710f1dcb795bf74bcf3e062890d1e9f2bb7af8ebfd3b5e4bb4b4845e586593c15ad6df930e8157538a551c56b51955cd78d55cdcb79473976dd352c80dddded823086155b6c4f01bf5a68ca8160871c6c65e5fdbeac2a0dbce94359dcf53143167a287b7c647c2f63941ec93b1fa4e0ce116334c34200f1931b180bc243dd048562608453a2470387f843b020d00dbbdc3e6b9bfe518aac96632848464a33ea7b36da2515fdc7799467d19f599460334c9ea9e2980152ec7a807233eb5a825f458621f004201f1c2efb9281376eec5bdec81a6d83d702e1cc1a987a5d3bd1b7b4f804bffe9314ce5ac9fdeccc0b710aecc029d913fbd2b7e7a578c65eafcae18605a27b8f4180583bd1c41e57bb47e05e9f900d2f7fa84c7d3512d3731e4873d33f7080f86a80ade1890532ce2b40e275634ab1b9a7567ebb2a04569cc6e88132a9cd3d02c54688967bb2eea7394d95e8a68297d6f0131090cddf7cdf9e4853ea697cb47e8bb34db87203b42ea41db4b682cb56f109841e63c6d5a3fe4354155263fc42541788059db3e26f95fcd274c483331056048dec13640f76114f96fc2c8b9febccc9898decf83199fd52ffc08d526c0433e8e73189a9e63b24b95e7b1546812e5a947ddb39f7fb11d7662be3c8f23603e4d9efbec2ce4c67367c4a5c40bc2268ca0e49c771ecc802c771aeb84e9af98decea37140423b1fcb5228e2749a3c8f96b0faf987037a4539f973473e7c22e2a0ce877a0c05b45440d6b831ccd5306faeda2c51960e604de67737bb4f59e73ea6d95475582eae7a0752b9212f5aea33b7c1f7bad599d980f270ae2fc4454ebe65ea19e9e499fd34c775222cebb150eb0a1539ca370e29aa9c6d405138319b59e5d30df8b6c5b617bb32cbd70168a569d2b4210dba32311bc2b02ba2058636f06c5620b61e84e47b52cf8b3d5bcfed592e635b083ebe574f46396bc66423c3e1549d19ca8c53157ef6e6e690e5f34143e2279cc095b99eccf266483d96a913bfdd9193494dbd77839f14f45996f6e452fab4d7b8fb144e96b5b784be5bca563b5610d8fa844f5ed3cd242f032c0dde475d2a249b378670aa21ec3bc333ff632dd8e8d92e4bfddb2166491a487ddaac42b3dd4075daf8a1fd9b60e3c794a5733aef6b28ef1d1f97a859be7cb869866ffb5a7eea207743bcfa76964a1310e54924bd489ac8354f22a122a175d0786a4fe2e8c45145887992073362de25e5772d8f3e10cbbe0791fc3e83a7e21dda6a6a78d2f2e3d672ed133d69f9716b9906157cd2f1e3d6711509f849c78f5ac7c4f97c52f1e356711d51f449c98f49c9a745912d93aada6e535a4efbc7a67713947e9eaeaa5d5745bec0a6a3343dfa115ce072b72e13ba414d5af7f2f044cc79ffe243fa3a5ec7657c70baac93a99f45c5325ac912273d5c21dad68157d1b4fe3bdfb27f922a24408d69bab124a2176ae9193cd1a78cea245d26db68ad939240040e06631a452a82ae36f1cbeb784b0fecd252239060ade82a13146412d88b63068e7a94aad3b0b25805f7e2f703b19a3cb36c0b75c51e337eb16978f717c68a54af7848bb1aba1f8f8e4e24acec15305d2d9a225feb98c0d4a40a9e10a035e858cbfa3e29d49e004b0480a2fdf05ddb514e141864a8e2338f894e5d1b4603a3987477a116e5ac3129250f965b2517797458d4a7509e330e1bf4353987b5696306503d6a96b6847b5038392956970cdb015c16350b6a9c185b6c5ee78526c9f39ced1bd76cb945fce74767d7d44a53d4dd67a39e0beef6745e7dc29d15ee269e4be5bc850bcd1633b3d0851286722b5db0c02838047231b22d833e0fb324362655556042b949efb04d6f856153164b450be02b4ccecd1d01f6bcbb58415ebba416dc4b164ee2a77136d4430cbf4030d7b9de0a0ce8dd3cfc2ebaeb009bdccd04cc6e3de88d76573a04e4b42e7f15e1a85eb3ccc66ceacf39152add379b69d75e30c5ed24c86dc79c98971c8bdf19fb0daaae81ed930b3df9107a294ed1ea2907449be6d43022e6b4829be308986635e785e289d7744a448a3f980e331b3e3d81d66243c5be5bd46a84e2b3a21a0bc122d1d427f47242f13ac13612c1419031ec092990321d3820854a0d8a5f27f404402f3291bca2015dd2f69961f61119dd79c3763ab3eb07dc496dad5a7815702bdf1c89de392fef828dacef7e89e738caf67091c75d39b8481f0857d224e3b6f11cd7767383ffb4eb3b4708cfe2064c8743d2be9b641d2fba17d8062bad0e0ac26df8aa4be13795e1960263412e3208da90f150146a0fe61f48bd4559c2ee81fd64b8ab5bdbbbb17687c5b3f769c5eef13895bf3e561f56eae9ec7d571999cd2fa8fb3301ecd3a386a487d122aa8a811c6423e0b1a59ad924bda7d7bae633854f79bdcb69084cec2cbe8fa3db5d5cc7546e6c63fdd379aa83605b468220ff611408ca5d005ac47f1e047a5cd731aaaf093ca10774cfb7eeb197cca62eccd4f479e30e7c26b04756cf117a532f8f731ad735668243bf8eca6871152fe3e461af0008f744f001e1228f69ee85bb88a9bfd5f9dcb058c7ef7e42e2f783c43ed4fb4c70b850f66008b58f064b43db8682a33b309449083c916953396dec9ca0b97f8be4b9e0702666711fdcc42685cbbfefe2d6c5dd3fdcc97de05a047d7e44b803bab707b8abf2713780636241144ac031049c76b9df47811b5323d77245abf83283008f95016ec2a3453d71a7eaa26f034606dfde593b8781b0d7f6cd16dc73306c3419cba26eb3f66e4c5758d664fdeb234754df538c6669c191c1a4ae722c28359bc7583001368afbfd7b00948585aaca8f8c294d9d2380ea6cbd2bcafe49d6e2f76c156b612510700a95be8d02afa656da725dabf8ef83c04dec3f46fd2d8d1fe8a03e7ad73e1efcd8762fda34a2a62b540e06ce10a26a3e489ac270b962a8ca19184f6bc6d8b9f17755cabba08a3640c9019c618d90b51a7f07f3fcd94f7d16a85557393264a85fb74fce147e8db0c7f6c86e35301ffbf34792a6714e1b6eb04296aa9cadc59966d96681c65e23b3d8586fd770aa5e0ceae3c80d51ee8e0f0f9a912d0ad0b9f99b160839d5ea52bd39eeade3477e32e38883aafc34c7339aaaa781e0deed93cfc5ee4d7f2a18a8eeb1efcb3243c6003b6f33534da183a0d069440c60f56c4d4f9023426be81bab1e1b838d08955bbc7e9bde330ea03ec991b2355e98564e1731624b655ffdf2b988f3c5d93a4a366a574d28ce3ffe14bf8df3289eafb56abfae594d8161de7d0a12c080c0f7153cd43b4cbdb4f46c50f73ebb4dd23d465dd57e5db39a028f147575eff60e75a8458117f602cf8ee1de224d8099a02b8909d0427bb1c7268afea86b55fdfd911aa8aa73f3b74f4dd888b7e4afcbee547071196d35b80369c0a01c6291515058551a6d8166095f060dc421741d838496c413887c2ffd2b1e6d09499bd1ac1ff7750f4dd111a059409947b49ba6eae31e4cc26fd7d917c285795774be4aca6c10141af633a4a6f0d64cfefa881024f70e5375adaa196167ef9eec3e61ce167313bfd165fc4eda8b05f527518b069d7b0eb9e6dfd3a201edbd4fb468a00527431cbd26222c18a8be7ab68b3779b65928d6169fb285cc608cc5867a9e05dac356007d1e6c9764f2e58349394eb58fb68610a2996a810984ac26e04406fb1c3652a51aab81c3b08647ee741128870ad33a1a78ff88a36db7f6f5476ecd6e9eb06ddaa6c36c5be4fb012ca4b0f9a2f534df64bb7076b666374fb4366dd3a1b52df2fda01552d87cd14a9b707d9f60acab9c028b456bcbc888d440f9833028edda0423b4ff3c343aa7cafc6352cfbc71c964a5f20466c5696ec8ac1ba58666f3fdfbc026a0a1f982f35d52069bdf09af794eeeb461ba99bdfafefd4ceb929ee608cf3eab05873a0d484578821c8c00f54c51a579580d36877b710b97181a9893276ac2bfce366b74a4268fb1eb5f39dfcee897c9a7833ed0168e3df0f909f4a55a788f14f1f5e68833e265f2e9100fb485650f7d7e427ca916de2345fce96693b95b78817842fb2eb684632e7d7c427aa912dc23c579bb65e384f38a701638975ac23da9943e3ee1bc5409ee91e29cacac9dcdb9403b1dcac586b0bca56f4f182f15627ba410671553ed74baf92e15e52ce02e3785e32e7f7d827ca914dda3007dbd214e684a4211e76d4348b3df247951d2a4185fa242862ca5ba8e4be0d1fce141fdbfca8bbfd7cbbb7813bd3cdc56bfd2c0e9d117f9deb08461a84626f278d70565fd70617d6b181a6393908dd13723e243cad76d8149f0cde9c7b3ae357d296c637a0a94b6aee2edaeac1068408a5c50af25b93caa3d6ff2840c26435bf842aa76f0a54c9ad11cfcc80ad21456b5463c04a90f3c91ad924f4a956d928b9a5b54d358b508292158364599e5710f5eb0aca11dddf4af1bd65021451bda99043f96dfd036ebc68d5800ae592c85b3b1b4b9a765498c416c1a2c60593326aa7fadd7f1aa393b47b54b4eb5ab68945c506f4df8acd9c6c6c8f9a7e576c865944d908b1a1a0065e0945a00156a9bb0aebe752d808aa29a60a81c552d5ae8722e2940ea72a14eecbb2fc4a965a42e17b56e429df20dd18cba20ba2975714373a020575253a0428a6640452d9aa0c4025408d104242eb8309952dddcd7b6d20dfd31ee07205708539d52dadc576d7548012bb30ae9aae64b2a9b0194c5b4a68e5b0bd75f7fd376bc2e62a8080cce2d55099652208b298bd4b494e940553fb6eeb61cd665142360c85e9058a2adff81995dead774969535019f4c3536c554d53671b12ceb6ec2fe98ea6e8aa9ea6ea22359d65dbfcd33555d9752d55cc73c31552c447290aa14beb795dd929f37d9aa9f43857248f75a7c0ca674b0c582ca76a808507e1d104a40e1d8012515830e28695289fc3458568b5c4655bf5c12250be20d6eb2339d6e14e58c1e6e5ddc66489887837a28e08601f43251aa0f2aa4042254d8b4d813d6cecc5b3379cda72e8b5d9a3324960d6b1f14195bd516c436a92d6fd99ef6c988b13d6d416c7bdaf2e8f6f40f04346de90b21d6a75d59ab3634ab5943239a52b856e016c8e09d5fa35eaa5258a554854d4e137cc753769de072f01e86a2b0c98e01d7ee64530614526ce100250d2d80ae41492d800ac12d804a9a64205d4e9125201551f45f2a675a1b4a1706e495a15404ae5b2e67a85b3ac795aa964ac0354bc54c0297cfd36489cb650cbb86caea99131efda1c4823bd560c83427141c8d782cc51c306ace4048a37047192a8ea21845864ac91cf3a241884d7d62c40aa2752964115ad18f294ef3b959c557576c48e1be8f8a12d0a893a0f5bc0645dc4c455ed7c622b04a82a514aba2bca9abbcefce740e76c1672a2cf150910fdfa794999e6c78d1a90e59195ec6e3524f7c9ded727a17413c6d32a00ca61aa1bbfe6a7016187b6cbce0cf9055d252930c8f2de8c09ce1a33d040f2222dc1054933c3611c987ce0bde4594456422d1cc8b9ae3f87a62441cae731cd507eb153fe8b3b7c878fb52f55d3509aa8aaa3b049fd3579dd19fbd8717741834d5f7d7ece0c4d368baa95a1ed57d34ad77264792328e81516848ca3187a2e9f60fc7552e349c30db3b1c0ed214488734feb3159ff8837aeeb465611686fa7e1b270ea8d88c45adbce9b460029f6896aa06c2913a0e2dbad417bdd83517546a40a1ea418ba29b0aa9d38b5513d384fe514d37668b80a09e62aa0aa634dff56e77c150e74c1a28c6989ca61798781573210438550bce48a9f10d4d27bab58b883d980594a3ba8acaa8492ee22f52f1aaa871ed67a098cc4eaaaed0d6f233de880d213ae1d22f46763cc90818995a68f040c4edc9e8c986df97996688ca77a917ed3d6785b40c14ea6e4277b0abcee92e56031ca08be40c1fedbd70cf7914aac13091022443626912118117c21757f1324e1eb48242110e3af4b477e41b4385baee1e4c6c5c0613bcd0d4894f1eb5c8165c05186971142374d3c0d5c07118a119674403c9b8f81a5e44f25b12a3884c24438a48fd94a6e2837819632f22e6c1067bdc5e40b2519655770978a852f545f3fa44450fbd3c117969df91f809c7041c65d92111e3205e5731d01bcd8b9aa56aa7012c67683cfb8ea76f3af43c67aa8e370e1fa2eb7249430700d56b5e814d2000e1cdd0823e5c5289405956dd09c5e3a6aa23d2378d30340fb5585efcf750c261b92e1a1baddb0bc1900d879b6944c50e0ff6f19b7e20312507ed90af803d05428d1dc6ae74e506b62a7893ecd9f13f92348d73ca188707a9fc8073cbe008901f192fb80af4ae2940311c2ad4efab25bf7450f154b31ae8996a4a0fd8adc99d77a0db36eb1b8864acf5cd5838e236ed18e923b7f9648a0111e02f6e5f01356de49c4883840092e19683630945786abd68df900347d57049cd8916fca0be3ec612bfe9ce56356fe521664d81e0a2699fb89b45d3941c5d34dc537e88595320b8684cb6585b7e78314d35989a37d96679d40547c70bfbde1ce2557f0f764546c8eb7b196d61e1688a9bcfcfe1d7eddcd9b9feb93ac7150c1b5131d30782709d9da4280ac8b35f03ddf087bfca90120c337388087bb149611f16e7aba4cc340233500c292a65c48b1a51c6081621c4633a9d33503c2ef1089135164df01eadc1660aa20cadcec842067606067b887ced8050074f0b3fc204a10ea85271444447719d2a864c5cae9c6746cb960eaa6e98bb71812f2f3a2b749864de4a558e903b7c4225f2a17b400d2a82f038dfac1f24bbb5fa42fef0c9b427541f1fe908549f226691f3a5cb3274ba67f545cde05599d516fe458b3a1894a02e6568271f5585ce80ac55d6a0e99627511717370bd0171c04cb5561a1b3022b953568fae109cd21135d0cb48550a8308763fe8172e4425703c648c70b2a2cdc23296decb5fa441e1548cd61d91a3c972bb4e41d3661ecc0ca5147a3abf999a3cbd9ab257cc251402d0367351d582dea107d153f44c83d87d112342b263452864bbb39f42851c42bacb9992210ba85bf0897b851112b63a0cc9003ab4215beb13e51370564b45745c8dc828022064b5d38b01a14912c2b66a6e094ee27639dcbed6642e4382eea53b5d0558d6aaefcc2d96814f4e2b866d405afe9bebd38ae8385363f903fcb2c8f6eab1ddd7551fdfae2f86a47a83771fdd7ebb8486e7b162f08cf34ae0213f54cdb3217e94dd626a9135ad416693fb7d70ae3325a4565749a97c90d5d59e4d9929ea9a5b78707ff88d63b52e47cf3255e5da41f76e57657922ec79b2f6b6edf9926b9d3d5ffe2586af38b0f5bfa5711a20ba49909e942fc217db54bd6abaedd6fa27521284dc58246567f1b93df6b5d121895f1edb78ed3ef598a64d488af4bfaf729de6cd78459f121bd8e1e6297b67d2ee2f7f16db4a48f5c1f9215dd215631312b8217fb8bd749749b479ba2e1d1d3933f0986579baffff2ff013d180e6f1d1d0400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190543490_AddedPlayerZalupa', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3a428cd8ce74c48bb4191948647e2884b4a6736f6a5a3d85d24cbacaeeaa9aae64876f897f9c13fc97fc185bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdfd7fffa751def3d86591ea5c99bfda38317fb7b61b24c575172f7667f5bdcfef32ffbfffa2ffff93fbd3e5badbfeefda32df78a942b2993fccdfe7d516c7e3d3ccc97f7e13ac80fd6d1324bf3f4b63858a6ebc360951ebe7cf1e26f8747478761c962bfe4b5b7f7fa6a9b14d13aacfe28ff3c499365b829b6417c91aec2386f7e2fbf5c575cf77e0fd661be0996e19bfd8fe94370701d66659b0fce93db2cc88b6cbb2cb65978701a14c155b849f3a848b36ffb7bc77114940dbc0ee3dbfdbd2049d22228cae6fffa250faf8b2c4deeae37e50f41fcf9db262ccbdd06711e36ddfab52f8eede18b97a487873d61cb6ab9cd8b746dc8f0e85523b2439edc4af0fb9d482b71af3771f89574bb92ec9bfdcb38f81666675f37611695ea2fa5c0d7faeb499c11025601351dfb3f3d971ff6e2873c7b6c7effa1834f8932f27f3fec9d6c63a2b83749b82db220fe61ef727b1347cb0fe1b7cfe94398bc49b6714cb7bc6cfb659696ec8b6f4dc33f868f61bcbf57b7ec3c295ebddcdffbbda40a6ee2b0d3e8a19205dd6b173e7f84e103c8ebe71f35bc5e1f521ac1288a54f5364db6b98b9e3a2623a8e9ef699494a3b3930af9f7e7d200180bf9f7b4886ebf796145da74929686a8e5f4f65ba1e362aca99320bf7f1bc401664c31daa1087fd8ab3eb3c66e10355da449f8cd04b9228bd334a1943308faaf89f4f2225a96d346114449986187413dbb1c1c2f9744ef0732768dc06ba24104fd218ae3dc51d061b07264719ce765c71d997c0e83b587ee5cdfa7ae2df92d72e5705e7a3eebd2f3390dd6c19d1188455e9fb6c55d6ac5cb60401cdf94062158163592cf8b705d423cba8d968da760333ba8798e30591465030d6cb2c8e03188b7fdfc9096f57bb4ebd7a56f1912a99ca479a19730297dc0d08c67ce8fd7f4f466e7d59ce7ac457f9ba671182403c8f334ca97757bcd64dad28d28d7ac146ce626d7e3f53a75f537830d19e3781e065a21fffc180677dbb094678a352435c5014bcccca64d9141d442fedb8aa35cdf95b6777fef22f8fa314cee8afbd2b909beeeefbd8bbe86abf69746445f92a85cf012c3936df57abbb9c906afe45d89e755b8f2e2e696ce4e98e79f299b4ad6c38d1ee88f16eef365160db6d2f9b76db80daf82e40ebd186dbcbb8af0a0271fde9bbb888c8621c080a06690717c1114cbfb8b707d1366c77f05d90a2bcbeb1216e5c756a63c9b1164ca2d45265cd85f859b6dbbf5e2c2e71df1aa4a2ee72b6624b63f939fc8275faaffadf4358e57abacd4a4f142e9242e8b85d9459094be6b37b2208ec323e1b7cacf1ad8e25ea69991a3643204ab6193db98b27ad8050f659f0f6836df91493b4b8aa8f8266e55563f1b2e701aa9d21c069064f98df98192cd5578cbf4820c774e8c873cb980d48eb2eee7fb6da4331a32261a8789cce6ee032b0bd7d1767d9648bc1953135e994c71bbda62afb6d94015d852df0cb9923d3b8021b307683cf7dc4564394eb0edc51dfc58ae8d8ecbf9e6b104bf178627db2c0b93429cdc6a606aa6c428cb8bcb202b1211d23afbd27924668495636843d8ed13022a86b6240de5f81f41bcdd048646f7f7e031baabc02178fdf751f818ae4bcd7c8cc8fc7915c655b9fc3edab0a3a8b2830b81e05d96aeafd298b3b87cb9c575b912ae46678a28fc39c8eec202df0b0e5badd1d7f5454226e911585add2f98c4b477ef88f15a69d453176a6a60e73ebe3fd2a25dcbd8cec8cbb79dc7f6e43c792cc59166dfc87e0c0e6f0089443f4249b56ec4e2a67ae93894bfdf4671a8e9105f5ca325657189a6d434a6daaa97feb509c40e298846a22fb1a85a614079538d5d66d163397f5d940b9372bd22d7575dece43e289aa2725da98b8a7ad29437d511354b6155049048342494542b482c6eaa9f7e05ad194b7d4194c55316978c25358da99e9a7d12ca1bd12b0aa291684a2caa561550de54575f72491f8e379bb839042265a4aa51951374a22c0c29e3f561bf2244ac13290fc4781384a23d90739b760549b5c87e31c931715857fa3871f2b2c0a5bad4eeab514e3bf0d56a19d58b2dc90bb2beebeb69fa6ee1af6bed3d84c205c080b729383a898141129b5a1b891ced64a0618692879287896cd48c8c3d9cc60fac990cb79c83e74ccddacfca3efbb2cc73b2c9805d31b5c8de0d17c6e4c0f3bd66c43373bea6ac3070b404d000b1c1563f649ca0d5b29901b2aabd494768753cd4133525aceabac5fbb05453b9cc595d064511664949b90a1beb3aed7cecc3f1a88472192c1ff482f13bad7b1d6982878d1d9a0e5e36bfa6723997e1794d35e2fa76d88c3596daf9a4c691898f9367e9ceff55903c50e3e5e8672b67da9d0b784c8fb9b8a971bab4bea8d74d0bd8c3c46d74388c607a2fd865f4d27ca61ab9fde18ee9a8858f85a619b1448e8e4cc8a954c84db415cc1b4cfdb90df3a229643d4858e4e0ce31581ae53906f3073c38e4e5ed5659667db13d9341f4c5df2017ef749bfac5ec60ef0e2480dbe25e5de5bd967f73dbc0c21c546db230062d9dc300aca4d5f3b0bacc7e520ecfbb52d280dbcc7f9ac250ada9b7018b23d6d8681e27b8d4f472b49a5e8d56d38fa3d5f493d79ae4ab9fc7208e4ac2e3d54a71f2d3565f1bb8be110bfe07e0a8c18c545c1b19d29b9f8b97a6308ec35525815cb59f490a1c1745b0bc0f5bf30ff719535eb28da72132ee5d8d9430562d6da5d3c3422017f5aaa7123796f0a443efc1ba5c7150eec3ca6f443839073ce87db9063cdf67c7e07b700c78ad9bb2930ec35a76aa512835eb1c2962226128a4c646433680a191d6ccd322fac892e03bc9d199f65236bd4feb22a07baf71319c2cb1cd02ad7e15f936c8c303884d6b7449a9096d6e67f80ef565792b66bc9b336b237b9eb7eeb1d9d35a60fbd43d06ca69781b9408b87e88f83d628221f2b3652f07da79a69f04117edca36cfb158bdc4f27dcabcffd290a3b4c7b9ba3292a3877baf2a68e39e2be34777519f250254584d6cbca39f9a4a248ccec60450be8cc8719b4307b5dfd36768c211eebc0d687f1ac780c684119af726af3ae5c262b7d398f9685f75eb096c89fef0dd428f5ba756531ddd17bda56de57558b9d0b569b1e29332f8e1871f3801d3c0636d83736ad28d1d39ebd3e11131f0c002b7df2d3a089125b5a6ea29c68faa0bd28d3d963180fac0fd6027961fd5727ab29dd3276f05e7893a2f372ec2cc97a9ddaafe108f501cf6234b3c1f658ec5a15fbc6a17315fd81c8662e1d3cb90f0889dd2969ddc78ec501c86c2e3dfd10396c3494c4071c83b9f4ab5db2da758c501ef02ce6d2b53ae693bdd66afa0391cd5c3ad8efa7557e81cb85a4fe200264eae71442d265f12805db6d823adfbdee79ceb0d3c0a1a49f9e038ca7ba91e67bf96ff724aa5a85b8f1f070f45535e33a4e59d7f1eccf6db4a13eb9ede0696f704127e10c25e6bc9d22903d0cd25019bf4c571f930c729500df335fa7204d0d9587e8d520501c073584d02d3b8c292cfd299ffdedd8cdb2b3b50fe2b3bf34c75976b95b0db8dfb3e658cdb2bb7cd40a4f0e0dc775b21759791e5abdb9ee081d2651f2dfc163e29de7670969d3caf580f23cff92c4e9f2c19d93979b29fda2dcd795146160e383c508a4da78311c856482d692d9f81e921b8c750dd4f4dffc0274495d1408aba22e6f7a328abce0ef35a40f523f1e5ff4f0427336bc3cc3c9625ef40db18a76c192bbbcf169384dbe9a22ab256e67ff8cea66fb79b8c34b7184cace2ed525c561a22e6e1e228a3149c39a317d5f60b36735dca1d05c46e1da9bf12ef2f94e83a336a1ebdd9878cb78548768674638ada9f6b3f508af999d25b2610184676348fa51a12e290c0a4df181deee3135d9c6b353dece57c4bf7318dfee237bda31dd0f2ad331ed6538d6592ca83144fe4927b89820d4affc964d851de9e9b7b711c9c31839802d9d523e40223a167d4902846e9cee757955bdddf3f28ed4e5ed48cd8524bb9ce4a5ba02b65dc3b0e137191265f44daa24e05e298bfb9f49fcc5114574c5e7c290675e0bde691c1216dffb581c7c2bce8b27799e5f85c1ca7d13ee3a4c56ce4ef655b80ca3478fd6a76568346e2bfbc053aa462f408018c31095a955aaa56ede3b964edb37ba38b6670c8d93856ae2d7521187ad923f896ca6b2505cb601532ba548566038de2a4e73886c436cd0376313643f598b58904ed69aa2c270d095371de2152364579a2a9aac40b25eb0a504875f51d4c9d917f93a0fe36957afdde8b11ac0731876554870b38427efcbff127d008f63f84f16596dfaabe7f4a39b3aef58fddde661e07db87cf0961ddc4fc21dfa7566d5493ab3e1b4c6b30285a1c1a149d446a72fa9b49c4071e36749ad151cc26862daced9d74126aff6e4003179014531ddf0b7d26450629f71725aa3df994ce3b525646b4d6f789462988bb9aec8838d8c71ffc5d45a6721799bea6c62af8b20f3c1e75d9444f9bd9f067181172b48106921c32df268d81203f3e9f66dcaa4b8b7c91a1d2549987d0e03e1e45933f39094c44418e7c975b84c9395693b6c77bbe979812eda5b3eb88460f024c54c6d3619980a8b4dd74217859bdb975036972a661c65a5d4344ab67d41b8a9ed776543bb42a6cdec618994ad4800379b2fa76cbe50d87d1234dbc590a75dfe4e83e9fa984afd84b3022ca6cb4ac02a5c4193c29b9f5798fcde1653d580592dafffdc06daa815569369dd69d7e8c554207c78f940a7d2620b73f6062803db1aa8a0f18247b370b39e34f91d22cddcea6da1434b46b6c29195518ad969430e331f91322a1973df6109f3859c76df689bc020d6750ea2b9cd20118ad5e57b7fa9ce86ce6842e7ed70ce6d6237196b7c76e47836369be01051d957fb6142069ecbc020f4530d85d625311d031e5c192bb7cccb6c865ea6a86632612de373ad6a3a2180b3976cd6b0427ab3ab5e6d2919860d68482f8224b8eb8f670086530d82769fcc741040fb6ba6db57b514dcdf4367fc05dfdfd2bc687e7770bdefb7c569fa979f530c7222435e0312a4f83916a9d22316d16309554ff7a44b66a1c53197b8bea071edbac06841a27a43c195a9b67b78d3212b23d8396941cbd4c69896d3025b0874422754c5057388a1f1611dbd5ac6a9ad22659a8cc34d78b16a1741e9a72521f94371398e64e772be1bd754c5dbd1816a1bece87afe165634941cdaedce363e46eba86e684806b9e3f9c6f57d5a4f7b5761906ffa5c70f5716a332576ba0a728b542a5f36abb29fa4b35b76da6ed27db0dfad678e7e16f03d65484cac7c6eb1b2ab5c6a77ac5d25655b6bcab198ca9ada985147fbd93d22e9c602269321c7e46c1d44b1c220befce9670f16b1aaa574536fa36cede1857f90e77fa5d9ea37fa81cd50d7aaafc3e5362b515b8ed5f566f0da2eefd37246dcd6e741e3d5e54d359fff4adf05cb22cd3c8585f8982e1f4a637d96acc894f4a5588a3314928197e6d42f29df95600e571e76dc88c1d2b83fd8f1e7b0f8082249d006ceb42edaa254e60cb0849820032e66bad5fe31bd8b2491f7f91adaa2f2a6d625b44d6d8a79bfb5c5d7233bd05095d336dee96083b0c409bb29296f745540dbdaba944f7fa2c29961648bcab780f8fcb0173fe4d963536e675c8be1d3001009396f93950226f4834f7a554dff08e2adefaa6c315a19180f18adf84c86d1aaf6f2e7c768453c97433d455bb8648f2adfa2cc7424702d93eafce8e52f5e265a7e22a0bb3976e5c623d316c4c4747bc0306133198461808145493b6db0e8c152b655fb5769734fd7ea2d4e7dfa1a3c94b83e60f84cb61c6ff629ace64e8a76ac39b4bd7dedf75276a906af97b1bb4711f445abfa37cbb72a83dd186e2557c350e58837112c5982452924d6a5951793a60c86ca7ac9c2ca327618ab30c3b9245fa50785e9201e6640890fb5cce0afb8deb091acb5dce1294b57a982b2153cc985046fd014990d1ac0966ff7a1b2a7b57352bdd5a88f19cdbadbb840e7c96d1694d56f97a4dd0712a6534da1ed01aac5e0a348a7be934d4eb89a38046c94b409cfcc5ce3930910d1c41e559797041fd51039198af7717a13c4b6719e04eaef36b48c97282b67aba8f45dcdefc370079b8256b0879bc2f04aeeb68c68ea8066cdcfd69eeb9c83f0480d42ad1b854110a4be60497a53a02e2918014d71ef9169c4fa644169d425111df1178aa6b6886557d7e989b1cf0385ece7584d1b5cf5c4d2f566a9a776003cb4a39aed6d2fddd86ca979d84e9b742b6d82cb10531fa312f96b4fcb8852164d49f0b4ac2fa03a2da34a9999afed9ac69bf8fce43c7f17077779d779a3a48dd4a30bf1698b97dc715f88dd8ebf95baa5f7885805d5ebf1ee7a5452f6a23aed79b3ff42502653f64314c78bcf6547e38ee20841f1f6dbe22aba8dfb6a5ee288ae93a8840f4bfa0a477a1125d1fb6dd291fd88acf13e2d68b29f7064975189815e263f232bbb78df91fc771cc98724baed65f10b8ee87d1626c1aa27fb1b5a843dcd91061925cca33087f0a101084528c0e4488313961642cb91062e2c071e34471ad470f573d839d28087a5e62074a4c110573585a4230d94584a1650471a44b1b43cae8e34c012844d19040dbcaec2cdb636af8babf3779f7b3a0dba38bac5bbb205c9aa27d7008c22ff10869b6a4e6a4935c8124985ca35e83afbba21022317499b2dd5965003ac8b7015c4751ca8c5db90dc53bd4fd3821cecb60c34e01218d4ce514faf8198404f744ed36b6026d0ff1666694ffd37712aaf276dc544cea7e97699c5859ce6034edf55780395b1e660a99bc06128ea461133e4f46579f69aa1724615fdd142b7c795126a8c5e857f6e4bc43437e80db5dc2c1364fc1a05b33bd2e3396a7f045151b9ec385553d755715aee08f88ab446f27f84cb2244abfb6d9bc20ce79a31fee94f16f0382955779766df2c873e7700c1729b1a1255a6462c22eabc9d58381cafd7291602c43c464bb4935e253dc5eabfcbb78675ca2fdb2c89386f9c2475c6bbe105d6f9a68d9acd84d5e69bf6316131a9d8a75c6b9e86b741c9190bd83feea322c4e2f56d1c2c1fd0800d92d54940015c83d8d2d50d13864083da8ae032fa1ac60c956e4910c58f14d035c87d9fc62b2c726938fe6233c756b6a3da8775de04a9581d301ca781e36f61bc0e8bb747583cbe4d57dfa8d23a409678dc94ff4f516850791924454e15d780b26efed54b2c2649f3a9d23a334a1a4315d781314e1f43bafcc080eca6052f98ecb81df07ca741e665986ee2f02240cfed35c11fa4bf68af3ebd490bba0a9d9f47cab335e8d6c084e2df6f5ebd42cff5df6ed2ecee63159b1987d29ae27396928d662c566ba2d32c8d56671b12b16d1cc896de8317b0967c0e7a5ed300b4d9fb595cbdff8485684bf20e6d415b8a8b0f2fb120eddb75fa13da1d8d56a5381747589436e5d196b429ff0a0bcfa6fc8f585c5e2f8324cca8f6eb76f1eaf27dfb75fb7675f9befddacde09aa0ef806e13981e59fca62f6a68d50b212fa3ab6675c0709c668c5d44715404d9b7d3207b581ce739719dd9ed6d8d1e1806e0f6b809fd7d5adc6dd1b30543cb6d6e6bc623437a45360b8a8fc13659de53165e334419168d4d1078e8b6511b1eb5530e2b40b791cab28054a0db4ae538704ad06da4b2d49c1a34c39e23962842670b382e325d688f8996d16d241f08da93a28edcea94a8a7e6e4af3d21ea28f9f31ddd4ab02394c95db7add131908a5c3bad941caa25b844e6da69a6a30785ae9d757a72def468e79f8e9413bbeebc88a294c85d7764447190095e7774f4f1d387e3c5454042897cceb679bf5da23b31aa084f3f1f5f5f5da30f8b2a9ab76112c67174d1cfd8ba23a28aec7a9bbc0ffea7c1c1504d55a4a577f0b79fd1c741b4776075f843b6beaa3d472f773808a3038ee5fcaf6e5c2fcb15d2d757d8a9bf2efe2376b66f8aff829de2ebf2bf60a7f3baf8d1cfb2a91b8582b33fb7d1a6d2d9759c16b620207104b3941c67e607bf05c95d90f5bf083ffc37b64e3f8f33868509bd1b7ef44257bab2eb5479133fff858da37fd6ecee57bbd9a548dd0f74448e131fead4db7a683f3bc81fb0a3aedea3436f1736fb97587f996c2f621de36a7311eb03bf4d53aab066b2b8cca275e96e722763da9b29e45da548a6736ecfbe6ee2348f1e436af1ad73267b9a7e01ae73237b1aca806bd4f1a9b8673605740e635d9e6a93462b7579aa3d06b338effba1863e9d55bd7da66467c3694e07225bc64c37df475cf5577fe04ff9d7657757b411d6d88b74459e5fa719d6c07c24b98e33998931d05c2ddc3a80898bde0e7876d3eaabe682dea7af33ffcab4851227ff3eb713667d3ddb6cfa63994d3cf599782ae4edeb6989cdea9a1816fd1fd3bc1089b4175cf230c8c8a35b9a4863b219a297b2d90b77d4c5474435537843ce07561d56d5ba7b6b269abeda9e05d42a5877acb53d0913d252b47ab7d7e9b6402bf64bfe7b9a51c535d3dedf37d8925f72b61dba09fb843a87d374f178db3b663637ecb888186e0e37c36c6283731cc7511e6dd75828fe16dddd67518ede41bf0e92b247a56389bf02903dd077ef34ce6403f5eb028b5e227ca755349f80d1722b054801d2319d6899dc65d144ae94db64993824f4393171403893df9fc5eba9e6e85d5314db27a9abd372ee66ca0fac2b204f82a1b2da395ee034b94357f0b2d7aa2add84ab93ac0a5b8c34b215491d2d1cbd6159d19c7da52e746a4c6d732b9a0e2fffa90d808eb5be208f7aa71fbbcbd2b0a8a4ca18738db7d0d0955065a8348bfccb2cdc0459c89038b8b06e2037491831a571fa2365e7700de22be0d2e56dd6849288fc8612feb4096b54e67088ff46a09c067661b5d8a533c01a15260701d6aab0666be0672f6096047b8503ec7657dd57dba488d6e15996a5e89db1e6a94f2d009652a37682ab380d568da0200e1a2cb41c6a530531d0cc2c8df6142dd0cc2b0d03790374f70db6d57edc6da5db71ee534aa3e958ce2b60dc2466248c3d931c3f06717483bff0552e07f3b2196847f77a13ac6520c7edec721187cc76a668c1b39c76e0a4f46a5b2e8cf0b7adcf92bb38cad17b8ea765c772fc6ee309c9dc944b7d3461532ad8044c79dd5ed387340b03fc7e5389ab84ee2c6ed3e938cfd3655401a5c53f9dbbb5c9df47672ee574591a94bd26108886b08f1cf2ad8f7208d0ecef5d94488b485491a8f84694ccd458d6f929390de3b008f7ea57a3e4b961beac5ec573022abbbcb26e6e173d156c6e9f728d6ded3f098db80a6fc38c44db0d48a619b206899282efd2651625cb6813c486c2e4f8ec69d20eb736e390afffb06b00ffe534dc90b854496128376f2deb1ac0e95627d7d78714b8d598afdf1deb7ad684d19201ca84093416e4f47a8c59b6420df17146a4d5f0b0e9e5f843c506114f64d8d4c148056b693f84940c27194eea16d9cd1e2f0e0e449fc6f7084135dc10876fd3649b0f375c50dac73459c268e221d4f4836a58950f510d4a98483e14da2890a37857ca260263c365b43a8c0fb50c3180ea63413a235f2d2dd7c68c06e63e0a1314045a0d1825ad1cda3c99b98557572c05ec0863ca01dda84e8d0c7294867706eb4db34fb61961dbc6a642415d456a6ac4f5f856d62685b76e5cf9765730adc460a36fb74fc3ac5415a6591c8389b15b070c6b7a41ffa101ae944e8e5a86bb3176e515eea85dd67668649bacd528a63d35dd9c306de07748e946c1f453f035b41d9a12d33bea63740f1efb8530f9a9db3c90414c4307615a2431c1b4ae426833a4c96fc4746876d846760c83a52e50060f25d855e002283a8c07241a30adaae8fc75618421c4396ba403caad15497968c870defe38fb2ab2f6598e304f9b291aa9e1dc0795538c06bb463eee2d99c6ecd7c6c3c2ee3384a3187eb6460d2e2bda9d32ff60f776cdfe83a8f0df89aaf42ca6811673b5be1ab7ae940b7260e1c8c10b0e124a934186ac7d679d2cb3fecd7aac9901658aae4c39d89a7f5a8f36967ef4e1c655bf730b75c38e8db66037d4f70e2cdca53de27fd06c4e19f2197d44489aa19889fab23b381fa9bbebb280f6315ad498c0b48ee730f5c522e2b41e1745b0bc0fdb9dbcea37d496ae865a71654824b4b831a4a95d3144742375fa0162d4c591868591d2316daa97a6f31b01886903433c01fe9fce0c61d2c7c9f16f37174c6affe558a87a53ad8990ae939e85ca6bd28311e33721dab0f38b777c1f7762018f87cd135dc43347a6e7c963c9b56c92f6fc444d36c7cba962239d26a721aea74ae538da121d27b11d5899b75b0a4d0f9a38a48b2e030dcab3d291cb61ce539a3b56dabaa5db527c1f67379598757054bf0aab7194796f49a71b047df45dca47d4eecbaac940d0f31446785757b7eb4b0854efc602394ab3b35f3688dd687ed1597535dd08c866eb939a70ddf4314758835d436dc0e47958780736a85994e3d2b09a99e76270c955493baacff2142ebca23a35b2838ed2f00e78e9747cf0a6f1f54f67890ae66a3208e1228509ba35f501c06e3fcf13d8b8fe60f0539339621aa74ed7e68cbd9702400eb54f01486386bb29402ba5c34035ec067a592617e2345b297271ed80956ee2ea5381bb4e8322585c85cb307ad4831b430dbbde10a199ff8da97af7fc12936e8d8777133d635ad552ce0df7d7a45e4bd4d3b4a3629ea9f8a9201eead4d47887348c69534d3723ac2f6800e2d1b660848141b81bba99fa2060a346d41c210ef50c83a486cc3bbc21cd1ab48790cd09deb80d1535dd2800dff14d145c872634dbbbba79d2648ff8b76dd8ae2c7098d6d0419816494c30adab70e7308decd06898466a7407304da540a1e363e55230cb082014536547da3291b60e803c5596e9f930f74e7472c34dec84dc11bb3a11b9366464d0a22cb08cc010b42a9b2bad62e78cadae27a359599dd676c5bc92e48e8bba1fca2b7e5061294249b9318d2adf2eb5411dd2884a2484410321f5014a8930dc9a30161c9b418405a4505c0ac9510f47246d9b0e965239a12654c2c10732a512716cc508e0e4925e2d48fa37253c65041040b9b2238154da4200a64d593ae9dd4070d5c90d039596871b6475f2716ec978b0a5f5b668d687da8b9f085a0598cda06256abcc8e0e7da86cd0b6b1ecaa8192304d22f6e4319cd6d4d25305e91766f9241437f403b48b27b1023b2be90f8cea6661945db5d3dfb42e0adfad1123438df8be6887b32d3c3f77b36b99cc48426bb3015c4d5e4253389abc2ce6ef66d2adff234a9230237d30b0803cd1407650a8c6046dc3d8405993c65c7a6b9480694a4f3b8ba3a476c9464146860f3599ee20c91c8f9afaa6721271cd1acb1ce294327fc308f5a3da46901f06c949ecb038d0e4ad682600619bb3576f3066a587014d453e0084590139b6641af81a9fce0374a318d5dd3e304276080321efa7f30a8dbab667ecf70d9441d2405a4d36580e1cb1262beb5af90953631ad731b455f472728f53ab97368d8ded0615f4510d0a7222dd60e806aa323b6c1ae8e58dbc5d2897d33b30e52a41bbc01323f378437456fdf2252f9b7f1207d15aee04c3c5c1e0576cc9b1f271c3ed83c23cb025ab8243a5e1560a0d8314d778544aa9601a40c86683d18fe95d94e0315a179f2f469bf6e9315a151c09a3acd026c0282b959dc3286a39a622b2c2ab228aa0aa2ac35598bfc91d238009b037c48a6e020c92cee1cd64557abe56b26e9ede48927223d948466213c09411c9fc2d641357eb7df9d7457fb67b116c141895d3a802aeb2c5c702aca2ad006cab62c106bb50730aabaa101f06362db9a7a8a80a01b9b766b4c53c6946b3fc33d981c510cb97f5029df90a5f53f5ce6dc89af46ab45d59132def8063f13e4e6f4a2ed4abc6b35554a43aacabc920940b1426f8d6543799938b6bd768d8c46905d39c9a6e46a84405cf50938d80cadd8e9681ebcf8468ded1181994734f7ab620be3a6af1d697d62cdea095d1f08b37aa797359bc89121b77f1268a04533fa11a099df5e0e9327d360d203136aec24d9a13ab5f5aa6e5362fd2759024695171f8b5ecd2499c11ede66ff68b6c2bbe05206cafc3a2bb1dbe4cb749c1dabbfa7fa5e650b0b32ccb268149ffd4b7eb442e650d95d656c4bea21638bbf26cef7b6b78f7d7c2517229b5b7ad75557d8f40f66051ace4df655109458d2ed94228be6d4cd2a2ce6525ca4411b41b55413d1035dcc5a45a1aded7659343b63552e6468d16d3f142aa84d232635acce4bf8651c2e72d476ab1ec5e1c87abaa51f20109e58ed28e1dca965c66e5baabd0a0508c488cea03150c1a622d46f4d670ed823786370a2308c5ba4431d670c4aa4f886b035a0e203891316332cbe098d7a1b6747806aee008bca142067ca532860a69f832778a0586cc570c27699799af784e9a694f5a125303b9a42e655a5ffcd7b0011f4c09fcc05238ce7aae688eb57f0f4c07fc698bcef001f718107c9bfb0e884636a7ed5a8ecde9b45973eb232a2def7a5da061cdeda80b3cb9ef186ed4198274dee60b22f8725bb892090a28a9e3cd2f8621d102bb19d8896f9d9ea42b08fc5c09647311ca07154fad5a40e7b9f376e8983ffb7b1499c24b6768f8a516b5c2d590766bf1aed3fc9a4158c719306f57d230736af1c0afd959d121c42a5f22d10d6a152e8ad8885e2e11133690e895eb4285268cea556b65606d7c0cf202c0809566d4bcaca4a5643992c6d46d187b4c35d5527554cf85a59a9294d749022683cd13b01da395b4843f204d038d5a0b95dfb6600f65a5b25593e944a0a496631bda8dd18a5b5d178861ad565d217cb2cdc83623bf25a001324c85841b48ec1dd4702d5268eb156a2d697acb6cc1ee9fc9c42c27d1f55e4a29c7b26a4bcfa8869111ccd48db3157212a35ee36c848b5ca7b10ce29e239b171290ab8e44de6b0d25245770e75421585d158060815d5f67b17236a84ba10c88535654de470905243ec9feb0428032e623090ed05f9d48db0c902c8d115c18d28120c9d631926865c7227a012329152b641403b95da58f7b540b715c3553cb9b3a5d311538476a210a96c37022e7ea196d42933688ff41ee3898b2b0900ecc69386d48ea93fac6dc01aeaf9d10f1a47051fda6ddf6d0106af717d4f472b9f347a0fa9d0c4d4d5289cb35ec55dc6ae4a3e86c4480c1b817594f016ef96d86aa39d5e4a3b736086afdc0d73351c95f2116dbda469a6b99c553775540e5766b28905b0e02a1f74d0db186918db6905ff8e43e20cad72fbdb5943a11e8186885cdde2cd18a5c5bdf78a2177298eb56e51a0a45d795847273c1dcac518956cd7f42913269e1713265494c3a0de6a0f72955b6029dbfad1a19fe4c056e8bce20bf3d66d0eab6eabc5a886976ed34b9d201499b645767fa8dccaf4e7519bc22a7902c32a3ba508367f03255b357f734de832ebbb77c7257e4f7f6e53f2892739be9cccae46292458386d738cb34671d4df24cd3465876c151698a4d324b0f6e1b30998af12297bfd1c2c8007caa358cb8c1675a13089b49958b92b33cb9aea6df607a5da5744d250b26d435d79f1fc16a9d0993bcaeba8e6bcffbdc653b8deba0cb140a08d628b928d36b6c7a517a77407bb9dba886d1e42acd5609081497d992e9a736b725d541c565711c4f4068d8abe26e82d32111975d51da491df66c043721daf8e47d32812993fc891d93a5f9e305d53c00d0c949969d0f27775bd9340a4148472ca9e99040209510c257d7e48a1b4c48d28c66809870d9cf986e69f39f511dd33cc5c0f10504867938622d38654e2db90cf1a9b8a06ea392719949c0b81a1932fd9ab666445009a034c357962a4a3ad88064518ec317c80e35381ac114461a51c1a98ea4dd12921d398a494851849a571c652364d3d1c8489d7d47da3769fe1d47994913ee0c253b4d1218cd2201a0c079f022a16e898010243253cc60064d91bc04294728d589b68b5cb213bff2e3529418addffc88d064d10a9118f5d764d16a27d119bc0b109338e836be35691fe49bd2f2c40f665d36a9612cacb258119307e884aacb3720efb322e3802fb12a320d0cb65491c4bc87aed220a2e3b31759d4f1f1a92e35efbf559762d4b1ec690dc06fc0bd0baa7d0aae171414a25dd9392e48bb93a0b880ea1cafb613dea5a39b40f021c295bdd34d1de6f29a66aa80a35523e45617c477af0e93ec4552352bfd086cfae2ed222014335971f34f1b6219bc7ca70ab24c75551def00cd1910a23a4083edc4a909d42b9d3d4d02fc02f31b32c42fd77d65bc07f35a461bcc9a08b180904d62ca321d474695a5050b84c55048161947760299ea8edc4d22a26abaac3b687796e934c7eb70944df5a4230bc7299b2980809c54bfc0a901c1caffa4432278125edd2bb2eedbebc3ebe57db80e9a1f5e1f964596e1a6d8067175153b6f3f94d67a132577794fd9fcb277bd09966403f89faff7f7beaee3247fb37f5f149b5f0f0ff38a757eb08e96599aa7b7c5c1325d1f06abf4f0e58b177f3b3c3a3a5cd73c0e978cc05f73aded6a2a87660928ee6b5975d9d277519617e406c14d90976a3859ad85625c544b56789db0dbcae4812b457d929f49549b9696fcbbb96d973e0407d761f6186607e7c96d1694d3e476596cb3f0806dce81aa8a5edaef4a0190d3d34a162185103579c9e07a19c441d6061b1502f39ea4f1769de8c2f5ea38917f43bceadf0db865e13ada962a5c9552e23972df40ae27e97a13875f39b6675fcbbfa2306923e75aeaa8e702542debd2c7f0318cd99ef48c16cd57a82f32867467245ce92226acff08c307047bbe18a889434e15786d11f66fd3649bbb28ab6762d0ffbfa7512202af63b5e8bf9b48f5f7b4886ebfa9f8d2254c3893f69c103ba56a7053c0b3924e82fcde453f84fe6dd925c3d1749126e137b6b784d3a2f9dd4478a76922a8a4e2d57eb0961858dd55781791b51299a5452c885ff146b30e7a54448fe514207216bfe23973d151f8f902f88ce75d4ddc97415624e244c47fc373e5126ed24c35b938e53cb9f4b2344f4de6d98a273c7eae491899bc88962e83a863d247ff3518001fa238ced9fe740c17cd47a30115062b29bfe6a309bfd2ad2d69a51cbbcf263cc961a7b2df540113bed7f7a9bca5cd47137ebf457276f537136ee749e980975ee469b0aea241c27cf95226357cda1677a9b606be945f0bfb1f41bcdd046cdded6f624daf0f39d79a77ef0f05ff9e5b76f14b07d4c2a279860c86481b72810186abb75d6ee098a9970c74e43d701dc215c0dbebe3b5e812b5bf992e6c7c2c91a88e10fdf12c81cfa673bf10c390af43516c36234373abd9c750f03408dce0ef030ed3aba88f9639b8aa64971dcd3425e7825054b59a5268aafb3e0fabe0c702569dba0c960f7cdb980fb301676d9a81088d434eab421215db2955cf48beb06c29793db15fa698fa642b47bb2563b3debc0a920770215a7f309b3c4566fdaf784ebda4651a98dd40d1c40bf537445415a187879a89743700de08b0d803f03924484fc401d1fe8ae744d655c0520b5c574d0a35362c8709d6ce73f2ef4fb7ffc5c07190061afaaf360e843ceb145a4f1515a7eff637839dad2a1e210fe5f64703235a22e4ae1414e474f0df061e1ff0ced83a5d45b7cda1e7e2c86e7b4c54db05c5d56493ac286b673bc5b66f511730d92b790ce2ad9a6753c2f3963d53c9cb990bf6e500827d3986605fcd5cb0af0610ecab3104fbe3cc05fbe30082fd710cc1fe3473c1fe3480607f7213acab3be57b73372b17f7f677485a97e6d32dcd6e28076766b2fb104972135908aee4f59d48ed8f30d838ec79f082abd97d27b2d384b4b4105fc771ae12745a3e52618e475a3bf2a1af5d568e4246e1e775e320fb2abc9c799ed0f7d99b8a2e0f840753d1f19aab91184076d70f912fd1115673959ca57915a36a8f645f7dedcad9edc7cddb229ee7c7e5d224ba89b9e50bfdbbc1118778fb5871e55876afeb36d8c60519007c37b94f231c0c496ea3a62486898dade8c640c5c26085099db1121e0bd541ab5ce9e06d54c2aeffb4e34b557ad056f390cbb97e45bfc3a37f3e5a59af53b520f5cad0b278d6054e17d5be8bab36104c9ef581d247b7ac76540992cfb356505af91069fc3ead3eb41c9e3581d244bb2c72504545feac0b775dd43ba88e0303c3e469ebc372250de5f71c6eccb135d9ec6c209848f5d39208caa13f8cbf3f5991a8c003163058977bd98ddd4974c3198187c1b73a593c02db3a0623ed489c46f9b2de0770da95e8d898ec4cd4c7b7ec464dc367d17e347a88572ef064ecaa4f666fccaba33e985dfb719aad0ec74b8e4c2aca418ffbc52496f64fb930bcc69a0bea6d1dfe2544fbe3d8b6baaaf83a4ef98714fdcfb3b1d0f2fc64839eb07099cdac41a86724d5519e87c2fbc1ee47bcaec97f5926f52f26fbf6674970538e7f7edbbefbd984d797244e970f22b3fef729ce58fb8d0c7ebcd31f66332ac4f488038d063eb1a2cd40d0f390eab7271594cc7e3209c453134af84d608d89c5e5b9b4bfcd0671504ebc6130a74f2088001d86c9f056a56e85f064b9fb15cf098edfa38eda237b9db90c732e0243fbdbccd03602cc1c01660a2d2b40c0cbb0b277a9dd128cd03455121e062b1cd197201c167287420ac29b9b0c6054ff6cc2e85de91cae4220a65ac58ff96ad4be6a407c166e6fd7ada43e9ac6b4bacc223efc58c593faf61cce8adf7018269cd554ae1b902b7128e78dafc9ca7bd3339162a026105fa9763f1b6c82f6193525fcda4f432e1f6683993ab3e858b88192b15a61076634227e404e4657c6045364ea809de75761b01217c0edaf060b872a0cb1b074e87e3579e55fa76716e32cf4bfcf663400894a861908fa8a30e7c0082632b538c6e793f304b94d75bf9d00ff1b8fbbeaa759426ed05589be2243c899ae50e6078f2abaa53007b43fe2f9b4891ca0034afe9b0978ef4a51433cd92f265b91e1f2419c66a89f07de3680d711cde56a1b5057f0b90a12d87794ea3d4af86572d98245f5b3c9f2e122f80af2213ffb0dd3b803864c9d85d0870553d480305d4aeaa12d44958e9463d3fe36b59d69f2cb4899f69f0c2c4d169656803ff0687f340ad793097cba1f0d9e4e444994dff38cfa5f8709202445c39638579f6edf0af16fd92f788e7de2525e85ec1783d9876c707d8ed6e179721d2ed3848f520c7d9f972d1ad48157d480b545a62ebbbfc05cbeac9ac787a80072cd316bf8700bf6468eff0ab2959d3742a9b5e6e2b6ab59f1b0d9d694a5a3a819da26ba90c5ebabb9aaa3f6c9b9caae90d55cb517c9ec77af9f83c96bf93d079387d93d079397e8e0cf6dc0fb6af54fc3f857d37b1723c4df46d666e675a839c9cf2ea5e1e7ed03cf0f19d2998e392e067716be9aecf99bfa6253629598d441e1492ab005244c3baca368ea004fa4bc93789b1761566d14d8449b3078b5a9af09a14f149761b7489a2608772dfb9f4d9602ab5556dd5eb211e96f695e340cac9cc3dfaaf00cdc256ac26d517f31710e2ed34cc2abfee2d90db8df16a7e95fc0f630fbc56c2b7cd7f2805574216869b84f4fca396a47da28f6cadd5699db294b0b2399874a1f24a97acfcf45d407637e9ddd025876dfa63c88dac5f1ec63e3f763b48eea0684646ae334247e3568dd7d5a9bd6ab30c885b762e2573ce72f9b55d94bd2d52dd75ef6cb6c2c902c27b59768236c6e699b50233a0ed22d04fe5a8fd1886bef59322cba1ff17cced641c467e6ad7f32e451fa44b751b6e68f64f86f069bd1419eff9566abdfaa5ba9cc8634f3c5e412d4729b11bd15c17ac3df84623e19b4f23e2dcd7a75cac33592fe60c54f2251b884c1d6fc5fe9bb6059a2187caa257e35b086e9f2a1b44b4d72ea2fc5923387e2670bde409bf96fa62f1cde95100d57e08e82f0d9c0cee6504af0fed7d9d857ce829dc44134d8ee025497bbd195b019c6f2920a790eed6f26ebdab2c9e2ab01ea67435eff1083bdd3bfcf066d44541fd3bb28190962555dee1093b0919bab3bf24a237d8c56fcc4c07d327a8e58d17ce0cf15990f4301791ec6e92a8d87cbb62856e58e1b98cbb0b685d429acec52d98bd58914db5c451af48299b20e842e35f4f2c3413801ad3ae3ac8c5b7b114c7641cc74876223ee936f0cf751eb7311cb7dd486d8fd9267c3c8c335cf8ed300173d77f9da550b318b41ea10189badd52534b6bafd72a5590f39eff7a1bd0d96d9dc889e4db04072caa80388166918267e5136a129681e8b0c3a65d7d7b0b4b5215483e624df2eaac8c44773ddcf53dcac245bdecd6344283e03f079cccdf6a9d019a737413cc24369a1221baf52cf436a8e3d3e73f5f724f46c4584c373ea7f353b93e14f25e4e711721827775b404cf4ef737e0e3c75a4a8757a524e3f831af8ae1647f3aee5a336c827c0f4cb7e99c2c443ad326f4f65aa2527b2fcb7d96070a4fd1df7bd1db37d1db71d67f11461921384f66377fb892fd2d5defcd2fd9db73f10819666b38a429cf774d7cbfb701d54bdcd37c1b281fbbb28cb0ba2c39b200feb22fb7bed5e672983cd5dfe67dcff721124d16d98179fd3873079b3ffb7839f0e5eeeef1dc7519093692dbedddffbba8e93fcd7e5362fd275902469fdbee1cdfe7d516c7e3d3cccab3af28375b4ccd23cbd2d0e96e9fa3058a5872f5f1cbd3a3c3a3a0c57eb439ebc618be2f2e26f2d973c5fc5347e288c33f604b26baf3f84fcbaea750b99abf056b047879c8e79f2d7524b469af3667fbb8dca7ff3fdfef5bcf414bebed9ff5f15d5af7be7ffbe68097fd8fb94951af975efc5deffdedffb7d1bc7e464eecdfe6d10e7026ee1da6b84d7f53f06d9f23ec8f6f7ca65e7c730b92beedfecfffc23cdb81cf27abe59b88eb6ebe6f8b1e55d4425e6ea435f3376fdab9b05b5be7fb31f25c58fc67da698d1af79fc70fc230c1f40aebf187325acdea6c9365ff40fc3157234e4f97b5a44b7df7c73252d3de9a350573d7f69cc8f8e48e52040261895031ff14a95abc0c4d59d2bc7936d96955310fde4cbd09e081c6486053360abe9e432c88a44b46d669ca87717169d62a85d3ac4c575316c0643edd20ce1859b03ac85d76d5e7875efdabc70a3deb379e1d7bc65f3c2ab7ec8e68515ff76cd0b53feb99a03d3ff08e2ed2630982369df17e17831af89c0ebfe266e98f23915d623e398c0060c25bc363da3838331ad93083c23a3fb826886c0c1a93df5ecd9ab873ae9356c979413de46a391ee0be3da477d7a846b14ea6764037275ec6ee5623af6b7e34177b8da7a781f960a2121562e83a208b3846c1c84f58270d7078c070b54c9ed32583e0c0396da4ef56110ec57e23d0f1ba4b0d40e467f5a8b2d2e03ecd661f592e02a481e9cf81033ebcc848e918176650c01f88e2cdbc1a43438f0c9de30eb81072f2d760c74447816b5b7844eb537cf890698c79afb2d8d8ca4a90a710881d350e8f1d1d239a0a34b71470f42cc1abaa65bb0d9e29c94c5279e339c1d352d3ab2de089d6ae8acd35574db1c762c8e16457527ddc15672fc1eeb6be935c3db380d2c96800ccb979e9bf8d27f135f796ee22bff4dfcd173137ff4dfc49f3c37f1278b261a5beacf41761752f6fad94e3fdb693f769a4796453b4416062d321e0aaeee8a342d2e50569d4d16e10243b8f708584356e7f97169aba292a0657393a6b1f9dac7f9bcf234bc0db67151252e77eb926c5988f2d175c74238fda43949e1eabc1d51f139cfd9c33d847ed0034897b01a377c145949f5e381211e72bbca75c201f25c3b8104cc9b6d39f5a8da6633ffb0561f2daa9a6c4653b3ba417ac9a007922a3b366e10c9c398eac790a78d302199b58b39673359fbe0d426b11eec784e97b2791a6b68771ad7267736f4db1a3aa7316be9321abb89925ed729a48702893aa532f2b007cea38c38e169091d9041fedb5dc409bf16a67733a8c4cb0efe229d71d981cdb4eb24263bb3e93da49e768895d1a53aed31f29e812acb31e286014beeb2e3dfa74736d5734bb9a316adcdc4ecdf9ce972143fed1bc17dd25bc3da5b42a7dafddd746d1fe40d850f7b64c8d20aeb9101ebc674c8f6a9811de6ba3e2db023132617b0abd2853cc02efb3f5cfe5ff49933b4569bf03eb3c1cca84e298b3d02973c8fc49c8177a42edbe76cda59432bc6507b785a318e1977d0b198fa75b7f4ec62803c199d2e45ac83bfde3f0536444b4be884553a9fac61fd3de91078d56569c561559994558f57c5dd7f433977093b0d85dcd0ed8c3d821056255cf4bf51af4baa6a80106b6cecf4b5ad2e92bae94c59d3b9bc66e1d32e3a786b6c306d9713c43e9fabebc4e06f31430514729011154ec849d2038d64698e51a44302670440b823d00030dd3b6c2206188ea19a6c264388cb57ea723adbe62a75c57d97acd495519facd443938cee99025861d2943a3062b3931a428f267601201453cfff9e8b34e7e74edccb1e688add01e7c2129c6a585addbb31f704980ca20ec3544c1ceacc0c7c0b61cbccd319f9f3d3e4e7a7c958a61e9f263739321d46c1602f475029238d5f413a3e8074bd3ee1f07454c98d8f1a62cecc3e48842630833306c42c8d38adc3b919f5ea8666ddd9ba2c68516a1324e2840aa745d40b155ae299ae8bfa3467a697225a4ad75b40540e44fb7d7336ffa18be965521aba2ecd76214e0f97bdd0f4121a4ded1a476690394f991910794d50960c107149101e60c6b68fca1f58f3f113158dcf22e893b7b70dd05d1845ee9b3062ba402733c667087460c62606f43f429539f4908fe32c86a6e398ecb2ed392c159a5c7bf251f7f2a79f4d871d9f72cfe10898cdb4673f3b73e9f5ec193159f5bcb0f12328316d9e033320519ec23a61facb67c873681c9013cfc5b2e492509f3acfa325ac7efe618f5c518efedc961f3e97e220ce877c0c79b45440e2b931ccd5306faeda4453860e604de67637bbcf7a673fa6e96c77582eb67a07b2c1212f5aaa93bfc1f7bae5c9dd80f270ba30c4454eb665f219e9e8a5f934c774c22febb1506b0b153150380e29b2b46f405138b79b5ee5d30df8b6c5a617bb52c3d70168a52932bd210dba34b71bc2b04ba205fa36f0746221ba1e84e47b52c78b3d1bc7ed5926e99b0f3eae574f46396bc62434c3e1549e5c4a8f53197e76e6e690e1f3414dee289cc0a5e9a2f4f2a6481d96a913bfdd11f3514dbd77839f14d4899a76e452fab4d7b8fb2c5086b5b784ae5bca463b5610d8fa9c514ed3cd242f030c0ddea52a9b92c91b43385b11f69de189fbb1166cf44c97a5eeede0132d0da43e6562a2d96ea05a6dfc90fe4db0f1a34bf4399df73594f78e8f4bd42c5f3edd36c3b77d2d3f7590bb215e7d5b4ba50988f22c925e244de49a67911091903a483cb5677174e2a822c43ccb831a311fa2e2bb96471f8865d783487e9fc153f10e6d35353c6bf9696bb9f6899eb5fcb4b54c820a3eebf869ebb88a04fcace327ade3d2f97c56f1d356711d51f459c94f49c9c7799e2ea3aada6e535a4cfb47a777e3947e96acaa5d5749bec0a6a324c3fa015ce0621b1711d9a02e5bf766ff689f87c5a7e4348cc322dc3b5ed6f9d84f827c19ac4489973d5c21dad68157d2b4fe3bdbb27f122a2c811a92746351402ed49233f8529f22aaa364196d825825258e081c0cda348a44045d6dfc97d370430eec924221106fade82ae314a413d8eb430a8e6a94cad3b0d25805f7e27703b18a3cb3740b55c59e327eb169787717c69254af7848db1aba170707470256760a98b6164d92af754c602a52054f08d01a74b465fd18e5724f80260240d17ef8aeed28230a0c3264f199c744a7aa0da381914fbabb908b72d6981492078bad128b3c392caa5328cf19870dfa9a9cc3cab43103a81e354b1bc2dd2b9cac14ab4a866d012e839a39354e8c2d3aaff34291e479cef68d69b6d822f6f393b36b72a549eaeeb351cf05773b3aaf3ee3ce087713cfa562dec285628b995ae842094399952e5860141c02b918e996419f8759126b93aa4a3021dda4b7d8a637c2b02e8ba5a405f01526ebe68e007bd65dac20af5c5273ee250d27fed3381bea3e869f2798ab5c6f0906d46e1e7e17dd76804dee660266b71ef45abb2b1c02325a17bff27094af59666336d5e79c1295ee9acd346b2f98e27612e4b6638ecf4b8ec5ef8cfd0659d7c0f689859e7d08b514a768f59403a24d73aa1911735ac1cd71044cb39a7342f1c46b3a2922f91f7487990d9f9e4069b1a162df2d6a15427159518d85609e68ea137a31a1789d601b89602fc818f6841448990e1c9042a506c5af157a3ca01799485ed2802e69fbcc30fb848ceebc613b9dd97503eea4b6562ebc0ab8956f8e44ef9c9777de46d677bfc4b31c653bb8c863ae1c9c278f25d7b249da6de339aeede606ff69d77796109ec50d980e8765fb6ea3385c742fb035565a1e1484d9f09597c26f2ac32d05c682586410b421e3a148d4eecd3f107a8bb284dd03fbc97057b7b67763cd0e8b67efd3f2dd63712a7e7daa3eacd0d3d9fbae22329b5f50f7673cd8a7270d4907a355aa2a0472908d80c7966a6693f48e5eeb9acf143ee5f52eab2130b1b3f8310ceeb6611d53b9b18df54f67890a826d190182ec8751202876016811fb7910e8315dc7a8be2670841ed03dd7bac75e32ebba3053d3e78c3bf099c00e593d4be84dbd3cce485cd7900a0e7d1a14c1e22a5c86d1e34e0110ee09e703c2459ed2dc0b7711537fabf3b961b18edffd8cc4ef07897da8f799e07021edc1106a1f0d969ab60d05477b604893103822d3a472d2d8394173f716c973c1e14ccce22eb8894d0a977fdb86ad8bbb7bb813fbc0b408fafc847007746f077057e5e36e0047c582c8a580a30818ed32bf8f0237aa46a6e59256b16506011e2d03dc84478a3ae24ed645d7068c0cbe9db376160361a7ed9b29b8e760d848329645dd66e5dd98aeb0a8c9fad7278ea8bea718cd92822383495ee558506a368fb160026c14f3fbf70028030b55951f19538a3a4700d549bccd8bfe49d6e2f774152a61c511300a15be8d02afa656d27255abd8ef83c08def3f46fd2d8d1be8a03e3ad73e1efce8762fda34a2ba2b5416064e13a26a3e489ac270d962a8ca19184e6bc6e8b9f17759ca3baf8ad640c9029c7e8d90b11a7f07f3fc994f7d06a89557393264885fb74bce147e8db0c3f6c86c35301ffbf347942461461aaeb14286aa9cadc59966d96680c65e23b3d8586fd770b25e0ceae3880d91ee8e0f0f9a912d0ad0b9f99b160839d5ea52be39eeace3277e32638983aafc34c7338aaaa781e0ceed93cfc5ee4d7f2ae8a9eeb1efcb524346033b6733534da183a0d06a440c60f54c4d8f97234263e86bab1e1b838d08a55bbc6e9bde330ea03ec991b2315ea8564e17316243645ffdf2250fb3c5491c446bb9abc615671f7ff2dfc67914cfd65ab55fd5aca6c030ef3e39096040e0fa0a1eea1da65e527a36a8fb98de45c90ea3ae6abfaa594d81278ababa773b873ad4a2c0097b9e67477f6f9126c08cd795c4046821bdd86113457e54b5aafefe440d54d5b9f9dba7266cc4fbf2af8bee547071116c14b80369c0a01c7c91515058551a6c8066715f060dc4c1751d838496c411886c2fdd2b1e6d09499ad1ac1f77750f4dd211a059409927b49b26ebe30e4cc2efe3f4a6e442bd2b3a5b45453a080a35fb194253586b267e7d4208127b87a9ba56d58cb0b3734f769f31678ab989dfe8527e27e9c582f893a84583ca3d875cf3ef69d180f6de275a34908293218e5c13e1160c445f3ddbc5bb2c5d2f246b8bcfe9426430c662433ecf02eda12b803e0fb64b32f9f241a71cabda475b4370d14c95c004425697e04406fb1c3652a51cab9ec3b0fa47ee741128870ad33a1a78ff08834db7f675476ecd6e9eb06ddaa6c26c5be4fb012ca4b0f9a2f5385ba75b7f76b666374fb4366d53a1b52df2fda01552d87cd14a9a70fd1061acab98028b466bcb488b544ff9833028edda0423b4ff3c343aa7cafca353cfbc714965a5720466c5696ec8ac1b258766f3fdfbc026a0a1f982f34354789bdf4b5ef39cdc49c354337bf5fdfb99d6053dcd119e7d560b06750a90f2f004396801ea98a24af1b01a6c0ef3e2162e313430274fd4847f9dadd7e8484d1e63d7bf72beadd12f924f077da02d0c7be0f333e80bb9f09e28e2ebcd116bc48be4d3211e680bcd1efafc8cf8422ebc278af8e3f53ab5b7f01cf184f69d6f09c35cf8f88cf44226b8278af376cbc60ae715e12c702eb4847952297c7cc6792113dc13c579b9b2b636e71ced7428e71b42f316be3d63bc9088ed89429c564cb5d369e7bb5494b380bbd81486bbf8f519f28554744f02f4f586784953941461d636a46cf6bb28cb0b9214e326c845c812aaebb0001ecdefefd5ff2bbdf87bbdbc0fd7c19bfd4df52b099c1edc88f786050c43355291c7bb2e48eb870bab5b43d1689b846c8cba19011b52be6e0b4c826f4e3f9e55ade94b611bd353a0b475156eb64585400d52c4826a2d89e551ed799745e560d2b4852d246b075b4aa719c5c18fa8204561596bf84390fac013d92af1a454da26b1a8be45358d518b9012826593176916f6e005cb6adad14dffaa610d1592b4a19d49f063f91d69b36adcf005e09af952381b4b9a7b5c14a531087583052cabc744f5af380e57cdd939aa5d62aa5d49a3c4826a6bc266cdd63646cc3f2db6432c236d825854d3002803a7d002a850db84b8fad6b5002a8a6a82a67254b568a18bb9a400a98b853ab16f6f4aa79692ba58d4b80975ca374433ea82e8a6d4c535cd81825c094d810a499a0115356882140b5021441390b860c2640a75335fdb4ad7e4c7b01f804c214c755269335f95d521052ccd2aa4aa9a2d296d065016d39a3a6e2d5c7ffd4dd9f1ba88a6223038b75025584a822caa2c52d342a60359fdd8badb725897918f80217a417c89b6fe476a76a95fd31956d6047cd2d5d8149355dbc4c532acbb09fba3abbb2926abbb898e645877fd364f57755d4a56731df344573117c941a892fbde567657febc4e57fd1cca9543bad7fc6330a983cd1794b6434680f2eb80500212c70e2829197440499d4ac4a7c1a25ac432b2fac592285994dee03a3d51e946524eebe1d6c54d86847e38c887026e18402f1385faa04252204285758b3d6eed4cbd3513d77cf2b2d8a5394562d8b0f64191b6556d416c93daf286ed699f8c68dbd316c4b6a72d8f6e4fff4040d196be10627dda95356a43b39ad534a229856b056e810cdef9d5eaa52a85554a5558e734c1773c45d7092e07ef61480aebec1870ed4e34654021c9160e5052d302e81a94d002a810dc02a8a44e06c2e5145102421149ff8572bab5a17061405c190a45e0bac5729aba85735ca16aa1045cb3504c2770f13c4d94b85846b36b28ad9e3ae1511f4a2c98530d8a4c7142c1d0f0c752d401a3e20ca46c14ee2843c6911723cf502a9943563408b1c94f8c6841b42e85284223fa31c5a93f37abf8aa8a0d29dc8f415e001ab512b49ad7a0889ba9c8ebda68045649b0a4629594d77595f5dda9cec12ef84c85c51f2ab2e1fba43253930d2f3ad9212bc54b7b5cea88af936d46ee22f0a74d1a94c1542374d75d0dd602a38f8d17ec19b24c5a7292e1b1051d98537c9487e05e44841b827292a72622f1d079c1ba88a28874248a7951711c5f4f8c88c37586a3fc60bde2077d7616196b5faabecb264159517987e073faaa33eab377ff82f683a6fafe9a199c581a453765cba3ba8fbaf5cee44892c631d00a0d4939e650d4ddfe61b88a858613667b87c3429a1ce990c67fb6e2e37f90cf9da62cf4c290df6f63c401159bb1a8a5379d1654e013c55255433852c7a14597fca217bde6824a0d2854356851745321757ab12a629a903faae9466f1110d4534c55de94e6badeed2e18aa9c490dc51893d3f402e3af622eb800a772c1692915bea1ee44b77611b107b3807264575129358945dc45ca5f15d5aefd341493d949d915da5a7eda1bb13e44c75dfac5c88e25190123530b0d1e88b83d1935d9f0fb32d30c51f12ef5a2bde72c91968642de4de80e76d539d5c56a800374919ce2a3bc17ee388f42356826528064482c4d2222f042f8e22a5c86d1a3525028c241879ef28e7c63a850d7ddbd898dc96082179a3cf1c99316d982a900232d8662846e6ab86a380e2334ed8ca82119175fc38b487c4ba215918e644811c99fd2547c102f63cc45443dd8a08fdb734836d2b2f22e010f55aabe285e9fc8e8a197273c2fe53b1237e1e880232d3b24622cc46b2b0672a37951b394ed3480e5348da7dff1f44d879ee74cd5f1c6e143745d2ca9e900a07ac52bb00904c0bd195a90874b321148cbca3b2179dc547544f8a61086e2a116cd8bfdee4b3834d74563a3557b2118b2e170338da8e8e1413f7e530f24aae4a01d7215b0a34088b1c3d895aedcc056056f921d3bfe479424614618e3f020941f706e191c01e223e3055381da350528864385fc7db5e0970e2a9e6a56033d5345e901bb35b9f30e74db647d03918cb5be190b47cca61d257de4369f48312002dcc5ed2aa0a68d8c13a991104032dc72702ca1704fad17ed1b72e0a81a2ea938d1821fd4d7c758fc37d5d9aae2ad3cc4ac29e05d34ed1377bd689a92a38b8679ca0f316b0a78178dce162bcb0f2fa6a90653f3265b2f8fbae0e878a1df9b43bceaefdeaec870797d2f820d2c1c4571fdf939fcba9d393b573f5767b88261232a66ea4010b6b39310450179f6aba11bfef0571a528262a60f11612e3621ecc3e26c1515a942601a8a2145258d7851234a1bc1c2877874a7731a8aa7251e2eb2c6a209dea334d8544194a1551959c8c0cec0600f91af1d10eae069e1479820e401552a8e88e828b653c59089cba5f3cc68d9d241d50d7337cef3e5456b850e93cc5baaca1172874fa84436740fa84149101eeb9bf58364b7965fc81f3e99f684ea63231d81ea93c42cb2be7459f84ef72cbfa8e9bd2abddafcbf68910783e2d4250dede4a22adf199095ca1a34ddf224ea62e26601fa828360d92acc775660a9b2064d3f3ca139a4a28b81b6100a156671cc3f508e5ce86ac018e9784185f97b24a58cbd569fc8a302a9592c5bbde7728596bcc3268c1d5839f26874353f7d743973b5f84f380aa865e0aca603ab451ea2afe28708b967315abc66c58446ca706937871e2592788535375d0442bbf017fe12374a62650c9419726055c8c237d627eaba808ce6aaf0995b1050c460a90b075683249265c54c179cd2fe64ac73b9ed4c8818c7457eaae6bbaa51cd955b381b85825e1fd68cbae035ddb7d78775b0d0e687f2cf22cd82bb6a4737ceab5f5f1f5e6d4bea7558ff751ae6d15dcfe275c93309abc0443dd3b6cc79729bb649eab816b545dacfedb5c2b0085641111c6745744b561659ba24676ac9ddfede3f82785b16395bdf84abf3e4d3b6d86c8bb2cbe1fa2666f69d49923b55fdaf0f8536bffeb4217fe53eba5036332abb107e4ade6ea378d5b5fb5d10e79cd2642c4864f5f761f97badcb12464578f7ade3f47b9a201935e2eb92fe7d0ed79bb864967f4aae83c7d0a66d5ff2f06378172cc923d7c768457688654cf48a60c5fefa340aeeb2609d373c7afaf2cf12c3abf5d77ff9ffda03a95eea1d0400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190545426_AutomaticMigration', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3e4509a199f3321ed0645511aaec4919694ceec5b47a9bb4896d95dd55355cd91ecf02ff3837f92ff8281bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdf17fffa75b33e7888f322c9d2978727473f1e1ec4e9325b25e9edcbc35d79f3cf7f3bfcd77ff9cfffe9c5f96af3f5e01f6db9e7b41ca14c8b97877765b9fdf5f8b858dec59ba838da24cb3c2bb29bf268996d8ea35576fcecc71fff7e7c72721c13168784d7c1c18bab5d5a269bb8fa83fc7996a5cb785beea2f565b68ad745f33bf9725d713df83ddac4c5365ac62f0fdf67f7d1d1759c93361f5da437795494f96e59eef2f8e875544657f1362b9232cbbf1d1e9cae938834f03a5edf1c1e44699a9551499affebe722be2ef32cbdbdde921fa2f5a76fdb9894bb89d645dc74ebd7be38b6873f3ea33d3cee095b56cb5d51661b4b8627cf1b911d8be44e823fec445a897bb35dc75f69b72bc9be3cfcb88ebec5f9f9d76d9c2744fd440a62adbf9ead734ac02ba0a6e3ffa7e7f2c3c1fabec81f9adf7fe8e0435046ffef8783b3dd9a2aee651aefca3c5aff70f071f7659d2cdfc5df3e65f771fa32ddadd76ccb49db3fe619615f7e6b1afe3e7e88d7870775cb2ed2f2f9b3c383df0955f4651d771a3dd6b2607bedc3e78f38be0779fdf29381d78b6346231845d1aa5e65e9aef0d153c7640435fd5b96a464747652a1fffe440c80b5907fcfcae4e65b1056b44d671931442da757df4a13176b4d9d45c5ddab681d61c614a71d86f08783ea336fec0651d36596c6df6c902bb3789da58c720641ff35955e51264b326d945192c6397618d4b3cbd1e97249f57ea462d708bc261a44d0ef92f5baf014741cad3c599c1605e9b827934f71b409d09debbbccb725bf25be1c2e88e7b3219ecfeb6813dd5a8158e6f56157de664ebc2c06c4e9176210a2655923f9a28c3704e2c94db26c3c0597d941cf7384c9a2240db4b0c932838768bdebe7878cd41fd0ae5f13df32a65239cb8ad22c615afa88a319cf9c9f6ed8e9cdcdabb928788bfe2acbd671940e20cfd749b1acdb6b27d3966e44b9e644b0b99f5c4f379bccd7df8cb6748ce379586885fef37d1cddee6222cf0c6b486a8a239e989b4d9b2283a885feb7150759df11db7b7870197d7d1fa7b7e51d716ea2af87076f92aff1aafda511d1e734210b5e6a78f29d596f5fbee48357f286e07915af82b8b9c4d9898be2136353e97ab8d103fbd1c17dfe982783ad74fe7d17efe2ab28bd452f461befae223ceac987f7e62e13ab610830a0a819641c5f46e5f2ee32de7c89f3d3bfa27c8595e5358105f9d8ca546433824c85a5c8840bfbab78bb6bb75e7cf8bca15e15e172b1e24662fb33fd897e0aa5fadf88af71ba5ae54493d60ba5b3352916e797514a7cd76e64411c8747c26f959f35b0c5fd98e5568e92cd10ac864de162caea6117dd933e1fb16cbe2393769e9649f94ddeaaac7eb65ce0345265390c2049f28dfb8191cd557cc3f5820e77418cc722b984d48eb2eee7db5d62321a2a26068789cee6fe032b8f37c96e739e2abc195b135e994c79bbda61afb6d94095d832df2cb9d23d3b8021b707683df7dc2674394eb11dc41d7c4fd646a764be7920e00fc2f06c97e7715aca935b0d4cc39498e445f931cacb5486b4c9be741e891d61e518ba1076fb84808aa12d49931c7f8f1e92db4ab192c77e97c40ff18648f57d42e7beab785d952bee922d3f022a1bb69008dee4d9e62a5b0bd6522cb7b826abd86a646588c29fa2fc362ef1bd1070d11a6c535f14648a1e81a5f5fd82496c7bf7861a9e95413d75a1a6067ede12fba32cdab58cef8cba7cdb796c4f2ed207228e2cff46f7527078034814fa914aea752317b7d54bc781fc7e93ac634387c4e2062d698b2b34a5a7b1d556bd6cafcd17764841340a7dc945f50a03cadb6aec639e3c90b9e7922c2ac85a43adafbad8d95d543645d5bad21795f564286fab236686c1aa08205168482aa957905cdc563ffdead73096fa82288ba72dae184b7a1a5b3d357b1c8c2761561444a3d0945c54af2aa0bcadae3e178a3e9c6eb7ebe600879651aa46574ed289b630a48c17c7fd6a0eb1c6633c10eb0d0c86f648cd6ddad51fd322f785a0c0c4634d18e2b428c8e294e952bb27c638dcc057a725502fb6b428e9daacafa7e9bb83bf6eb4f7100a170003d1a6e0e8140606496c6b6d147274938181194a1e5a1e36b2d133b2f6701a3fb06632dc720e9e330d6b3f27fb1cca32cfc9260376c5d62207375c189303cff78611cfcdf986b2d2c031124003c4055bfd90f18256cb6606c8aaf6153da1d5f1d04fd48cb0aaab126f63a226b2cc597d8cca32ce5342b98a1beb3aed7c1cc2f1a884f2315ade9b0513765a0f3ad2240f1b3b343dbc6c714de573a622f29a6ac4f5ed70196b3cb5f7298b279310a7c6ca5dfbab28bd67c6cbc92f4eceb43f17f0881d73e9d2e074197dd1a09b16b08789dbe8f018c1ec5eb0cfe865f94c3572fb8319db510b1fe94c3362a91c3d99d013a55898682b983798fa7317176553c87990f0c8c19d63f034da730cee0f7870a8cbbbadb2ecfae27a2683e84bb8412edfc7b6f58bf9c1de1d480037bd83baca072dffe6a6808339a8dae4600c5a3a8f015849abe7e17411fd8c0ccf5b2269c06d163f4d61a836ccbdfec5096f6c0c0f0b7c6a7a365a4dcf47abe9a7d16afa39684dead5cf43b44e08e1e96aa539f969abaf0d5cdf8885f80370d460472aaf8d2ce9edcfc589295cafe355258142b79f490b9c9665b4bc8b5bf30ff719535eb18d6720b2ee5d8d9478ad5bda2aa78785442eebd54c256f2ce14987de83f5b9e2a0dd8755df88f0720e44d087720d44be4f8ec1f7e018885ab765a71c86b5ec74a35069d60552c444c251288d8d816c0043a3ac59a445f49127c17752a0b3eda56a7a9fd64540f7dee06278596297055afda2f15554c447109bd6e8d25213dadccef01d9bcb8a56cc7a3767d646f6a268dd63bb67b1c0f6a97ffc92d7f14d4410707d9f887bc41443f467c75e0eb4f3cc3ee7a1fc8407d5ee2b16b59f4eb9579ffb53147e98f636c7505472ee4ce56d1d73c47d69e1ea32e4a12a8a48ad5795f3f2496591d8d9c18a16d0590833e860f6bafa5dec18473cd6816d08e359f118d082725ee5d4e65dbb4cd6fa72012d8be8bd602d5138df1ba851e9759bca62ba63f6b49dbcafaa163717ac363d4a66411c31eae6013b781c6cb06f6c5a51a2a73d777d22263e18004efa14a7411b25b6b4c24439d1f4c17a51b6b3c7301e581f6805f2c2faaf5e5653b965ece1bd8826c5e4e5b85992cd26735fc351ea2391c5686683efb1dcb52a6e8d47e72afa2399cd5c3a7876175112b753d2ba8f1d8b2390d95c7afa2ef1d86820c4470283b9f4ab5db2ba758c521e892ce6d2b53a5e93bbd66afa2399cd5c3ad8efa7557e81cf85a4fe2002641ae61442d165f92805db6d8abad0bdee79ceb0d3c0a164989e038ca7ba91167af9eff624aa5a85f8f10870f45535e37a9df1aee3f99fbb64cb7cf2dbc133dee0824ec2394acc793b43a07a1864a0b27e99ae3f2619e42a01be67a14e419a1a2a0f31a84160380e6a08a15b76185348fca990fdedd8cdb2b3b50f12b2bf2cc75976b95b0df8dfb31658cdb2bb62d48a400e8dc075b2175945113bbdb9ee083d2651fadfc1e3d95d14e7296dd3caf780f2a2f89caeb3e5bd3fa7203753fa4579a82b29d2c0c6078b91488df162040ac5046d2473f13d143718eb1a98e9bff905e892be281056455fdef6641479c13f68481fa47e02bee81185e66d78458693c5bce81be214ed8227f779e3d3709a7c3545574bc2cefe39d3cdf6f3708797f208559d5dea4bcac3445fdc3e44146792863563e6bec066cf69b843a1b9ac42ad37e35de6f39d06366dc2cefb310996ada80eafce8d705653ed67e7115e333b4f55c30208cfc691f4a3425f521a1486e203bddde36a728d67a7bd9daf897fe731befd47f6b463ba1f54b6633ac870ac3350306388fe934d4e3141985ef52d9b0a3bcad3ef602352843172003b3aa5628044741c794202846e9cee757955bddbf3f28ed4e7ed48cd8526aa9ce4a5ba06b65dc3b0e13739126df44da624e05e698b879f49c2c511457425e4c250645e0bde6b1c5216dffb581c7c2b2e882779515cc5d1ca7f13ee3a4e57de4ef655bc8c938780d6a76568356e2bfb2052ea462f408018c31095ad55aaa56edf3b9eced837b638b6671c8d97856ae2d73211879d1237c96ca6b25042a6005b2ba549346039de2a4e73886c436dd0376b13e43e59cb58504ed686a2d2703095b71de2152364579a2a9a8c3eaa5ef0a524875f53d4cbd997f97a0fe36957afdde8711ac0731876554870bb64256fc97fa93e80c731e227878c34fdd573f6d14d9d33acfeeef230f02e5ede07cbec1d26590efb3ab3ea249b95705ae35981c2d2e0b0247aa3d397d45a4ea0b8f5b3a4d60a0e6134316d17eceb2093577b728098bc80a2986e845b69722871cf1639add1ef4ca6f5da12b2b5b6373c8818e662ae2bf268ab62dc7fb1b5d6794cdfa67a9bd8eb32ca43f07993a4497117a64142e0c50a12545ac8708b221a76d4c07cb879c527b877c9f89ca4699c7f8a23e9e4d930f3d074c2541817e975bcccd2956d3b5c77bbd979812dda5b3eb88464f014c56c6d361d981a8bcdd6c216859bdb97d0369729661d6585681a25dbbe20dcd4f6bbb6a15d21db66f6b044ca5626809b2d96d3365f2aec3f09daed62a853267fa7c174434ca561c2590116d36725e014aea049bf2dce2b5c6e6e87a92a64464a91fd9fbbc818b5c26932ad3bed1bbd9809840f2f1fd8545a7c61c1de0065605b0315b45ef018166ece93a6b84364985b832d7458c9a85638aa325a317b6dc861e6235a462763e13b2c61b190d7ee1b6b1338c4face412cb719244271ba7c1f2ed5d9d0194dd8bc1ddeb94ddc266383cf8e1ccfd666131c223afbea3e4ce8c0f31918947eaaa1d0ba24b66320802be3e4960599cdd0cb14dd4c26ad6542ae556d270470f652cd1a4e486f76d5ab2d25cbb0010de9659446b7fdf10cc070aa41d0ee93d90e02687fcd76fbaa9682ff7be85cbce0fb5b5694cdef1eaef7ddae7c9dfd15e614839ec8d0d7801429618e45aaf48865f240a01ae89e3461163b1c73c9eb0b16d7be0b8c1624ba371442996abb47341daa32929d5316744c6d8c69392bb08544277542575c3287189a10d631a8659cda2a32a6c93adc4410ab7619113f2d8de91f9acb71343b97f7ddb8a62ad18e0e54db6047d7f3b7b0b2a114d0ee76b6f13ed9247543633ac83dcf37aeefb27adabb8aa362dbe782ab8f539b29b1d3555438a452f9bc5d917ed2ceeef869bb49f7c17f779e39fa5920f494a130b1eab9c5c9ae0aa9ddb17695966dada9c0622a6bea62463ded67f788a41b0b984c860293f34d94ac3506f1d9cfbf04b088552dc44dbd49f24d8017fe5151fc95e5abdfd80736435dabbe8e97bb9ca0968cd5cd76f0da3ede656446dcd5e741e3d5154c359ffecade44cb32cb038585789f2def89b13e4f57744afa5c2ee5190ac9204873ea97946f0898e355801d376ab00cee0f76fc792c3ea24411b44130ad8bb6289339032c2127c8808bd96eb5bfcf6e1345e47db186b6a8baa9750963539b62c16f6d89f5a80e3474e58c8df73ad8a02c71c26e4aaa1b5d1530b6b62e15d29fa8706619d9a2f22d203e3f1cacef8bfca129b737aec5f06900a884bcb7c9888029fde0935e55d33fa2f52e7455ae18ad0c4c008c567c26c368553bf9f9215951cfe5d84cd11626ec51e55b94d98e04a1654a9d9f3cfb5b9089569c08d86e8e5db9f5c874053135dd01304cd94c066118606051da4e172c06b0946dd5e155dadcd3757a8b539fbe46f704d7471c9fc996e3cd3e85d3dcc9d08e3587b6b7afc35eca266a087a19bb7b14c15eb4aa7f737cab32d88de15672350c758e7813c19227581021f12eadba9832653054364816569eb1c7588519ce25f92a3b286c07f130034a7ea865077fcdf586ad62ade50f4f55ba4a1d949de0492f240483a6cc6cd000b662bb8fb53dad9d93eaad467dcc68d7ddc605ba486ff28854bf5bd2761f29984e3585b607a80e838f219dfa4e363de16ae210f051d2263c33f38d4f2641c4107b545f5e117cd440e46528deaeb32fd1da35ce9344fddd8696091265e57c9510dfd5fe3e8c70b02969057bb8290daff476c789a60e68d6fcececb9ce39088fd220d4bad1180449ea0b9ea43705fa92921130140f1e9946ae4f1594465f12d19170a1686a8b48babac9ceac7d1e2864bfc06adae0aa678eae374f3db50310a01dd56cef7ae9c6654b2dc076daa45b69135c8698fa1895cadf785a4695b2684a82a7657d01dd691953cace7ced362cdee4e72717c59b75745b749db74adac83cba909fb604c91df799daedf537a25b768f885750bd1eefae47a5a417d569cfcbc31f25657265df25ebf5e213e9e8baa3384150bcfab6b84a6ed67d35cf7044d76942e0c3933ec7915e2669f2769776643f216bbccb4a96ec671cd9c78460a097c92fc8ca2edf7624ff1d47f22e4d6e7a59fc0d47f4368fd368d593fd1d2dc29ee6c4800c02f3242e207c1800c2104a303931e084a785d07262800bcf4104cd89013542fd02764e0ce0e1a905089d18302454cd20e9c400259e9207d48901513cad88ab1303b024613306c100afab78bbabcdebe2eae2cda79ece802e816ef186b4205df5e4068031e4efe2785bcd492da9015932a954b9015de75fb75460f42269b3a5da121a807519afa2751d076af12aa6f754efb2aca407bb2d0303b82406b573d4d31b2026d1539db3f4069849f4bfc579d653ff5d9ecaeb495b33918b69ba7d667129a7f980d37715de4067ac05589a2670188aa651c40d39735991bd61a89c33457f72d0ed69a5841aa357f19f3b8298e606bda5969b65828a5fa3607e477a3c47ed8f28292b971da76ae6ba2a4ecb1d815891d148fe8f7859c66875bf6a5398e15c33ce3ffdd9011e674475b759fecd71e80b07103cb7a92151656ac422a2cedb8985c3e96693612140cd63b2443be955d253acfebb7c6b58a7fc639b2511e78dd3a4ce7837bcc43adfac517399b0da7cd321262c2e15fb946bcdd7f14d44386301fbc75d52c658bcbe5a47cb7b3460a37475163100372096b8ba71ca1118505b117c4cbec66b8ecab42448d60f0cd00dc87d9bad5758e4b270fc9bcb1c5bd98e6a1fd67b13a46275c4719c068ebfc5eb4d5cbe3ac1e2f155b6fac694360192e0714bfe9fa130a0f26394960553dc00cabaf957cfb098a4cd674a9bcc286d0c53dc04c675f610b3e5070664372d04c164c7ed48e43b0d323fc6d9761d5f46e8b9bd26f883f617edd5675fb292adc2e4e7d1f27c0da63530a5f88f2fcf9fa3e7fa6f5fb2fcf67d159b1987d29ae2539ed18d662c566ba2d77996acceb73462db389025de4310b0123e473daf6900daecfd2caede7ec042b4257983b6a02dc5e5bb675890f6ed7afd33da1d4d56449c8b132c4a9bf2684bda947f8e856753fe272c2eaf97511ae74cfb4dbb7875f9befda67dbbba7cdf7ee366704dd077c0b409cc8e2c71d31735b4ea855090d155b33ae2384e33c62e93755246f9b7d7517ebf382d0aea3af3dbdb063d700cc0ed711bfabbacbcdda1670b8e56d8dc368c478ef48a6e1694efa35dbabc632cbc6188722c1a9b20f1306da3363c6aa71c56806923956701a9c0b4952a70109460da48e5a905351886bd40ac5084c916085c54ba301e132d939b443d108c27451db9d329514f2dc8df7842d4518ae73ba6956047a892bb695ba363a014b9715a211caa25b842e6c669a6a307856e9c757a72d1f418e79f8e5410bbe9bc88a154c8dd7464c4705009de7474f4fec3bbd3c5654443897cca7745bf5d623a31aa085f7f3abdbeba461f165534afe2345eaf93cb7ec6361d115564d7bbf46df43f2d0e866aaa3223dec1df7f411f07b1de81d3e10fddfaaaf61c83dce1a08c8e0496f3bfba71bd242ba4afcfb1537f5dfc27ec6cdf14ff1b768aafcbff0d3b9dd7c54f7e514ddd28149cffb94bb695ceaed759e90a021a4730cfe8716671f45b94de4679ff8bf4c37fe3eb0cf338635898b0bbe1273f9a4a57769d296fe3e7ffe8e2e89f37bbfbd56e3611a9ff818ecc71e2439d7a5b0fed6747c53d76d4d57b74e8edc266ff12eb2fd3ed45ac635c6d2e627de05759c614364c161ff36443dc4de164cc783385beab94c94cceedf9d7ed3a2b928798597c9b9cc99ea65f809bdcc89e8631e006757c28efb84d0193c3589767da64d04a5d9e698fc52c2efa7ea8a1cf66556f9f29b9d97096d391cc9633d3cdf71157fdd51ff853fe0de9ee8a35c2067b91ade8f3eb2cc71a98f734d771ae3231169aab855b0730f1d1db91c86e5a7dd55cd0fbf475e65f95b650e214dfe776c2acaf67db4d7f3cb389a73e1b4f85be7d7d4db0595d13c3a2ff7d56943291f1824b1147397d74cb12194c3647f44c357be18ebac488a8760a6fc8c5c0aac3aada746fcd46d357bbf38859059b8eb57667714a5b8a56efee3adb9568c57e2e7ecf72a6b861dafbb72db6e4e7826f8769c23e63cee10c5d3cddf58e99cb0d3b2122869fc3cd319bd8e09caed74991ec365828fe96dcdee54981de41bf8e52d223e258e2af00e4f7ecdd3b8333d940fdbac4a2970adf6b152d266074dc4a015280744c275a26775934912be53659260e097d4e4c1c10ced5f767f17aaa3906d714c3f651eaea3599bbb9f203eb0ac89360a9ac768e97384deed095a2ec8daacab6f1ea2cafc216238d6c4552470b476f585634e75f990b9d0653dbdc8a66c3cb7f6803a063ad2fc8a3dee9c7eeb2342c2aa972c6dce02d347404aa1c956191ff318fb7511e73241e2eac1fc86d12464c699cfec8f839dc80f80ab86c799735a12222bfa5843f6ce31a95051ce2bf11a8a0817d582d76e90cb04685cb4180b52abcd91af8d90b9825c15de100bbfd55f7d52e2d934d7c9ee7197a67ac79ea530b80a734a89de26a9d45ab4650100703165a0eb5a98218186696467b9a1618e6958681ba01a6fb06bb6a3feea6d2ed38f72995d1741ce715306e123712c69e494e1fa275f2057fe18b2c070bd20cb4a37bbd8d362a90e37676858843763b53ace0794e7b70527ab5230b23fc6debf3f4769d14e83dc7d7a463057eb7f18c666e2a943e9ab429156d23aebc69afe95d96c7117ebf89e02a653b8bdb743a2d8a6c99544069f1cfe66e6df2f7b1994b055d128372d004023110f69143bef5510e019ac3834b82b484461549ca6f54c95c8da4ce0fe9eb781d97f141fd6a943e372c96d5ab784140a4cb2be7e676d153c1e6f629d7f8d6fe93d488abf826ce69b4dd88669aa16b90242dc52e7dcc9374996ca3b5a530053e0786b4c3adcd3816eb3fee1a207e791d6f695caab4b4945bb096750d10746b92eb8b6306dc7accd7ef8e4d3d6bc268a90065c3041a0b6a7a33c61c5ba187f83823d26978b8f472fca1e2828847326cea60a492b5741f425a86930c277d8bdc668f1f8f8e649f26f4084135dc1287afb274570c375c50dac73459c168e221d4f4836958950f510f4a98483d14da2890a37857da260263c367b47a8c0fbd0c3180ea63417a235f2f2ddfc68c06e63e0a1314045a0f182dad1ada2299bd85d757ac04ec0863ca03dda84e8d0c729486f706eb4db3cf763965dbc6a642415d476a6bc4cdf8d6d6a684b7695c85765730adc460a36f7748c3ac5515a659028389b15b070c6b7ac1fe6100ae924e8d5a8ebb3576d515eea95d367668649b6cd428a63d35dd9c306de17728e946c1f463f0358c1d9a12d37bea63740f1efb8530fda9db3c5041cc4007615a26b1c1b4a9426833a4c96fc4756876d846760c83a52e50860825d8551002287a8c07241a30adaae8c27561842124386bb403daad15457968c808defe38fb2aaaf6398eb0409b2906a9e1dc079d538c06bb413efe2d99c6ecd7c6c3c1ee7384a3187ebe46032e2bdabd32ff60f7f6cdfe83a808df89aaf42ca6811673b5be1ab78ec80539b070e4e0050705a5cd2043d6beb74e965dff663dd6ec80324557a61c6ccd3f9d471b4f3ffa7013aadfbb85ba65c7465bb05bea7b0f16eeca1e893f1836a72cf98c3e2214cdd0cc447dd93d9c8ff4ddf5594087182d7a4c605a277298fa6211755a4fcb325adec5ed4e5ef51b6a4bd740adb93224133adc1832d4ae1922a6913afd00b1eae248c3c24ae99836d54bd3f98d00c4b481219e00ff8f6786b0e9e3e4f8779b0b26b5ff6a2c54bda9d64448d7c9cc42e73599c188f19b106dd8fbc53bbe8f7bb180c7c3e6912ee2b923d38bf48170254d329e9fe8c9e67839556ea4d7e434c4f554a51c475ba2e324b6072bf3764ba1e941138774d165a0417956267235cc454a7bc7ca58b7725b4aece3eca612bb0e8eea5761358e32ef2de97483a08fbecbf888c67d593d19087a91c20aeffaeaf67d0981eadd5820476976f6cb06b91bcd2f26abaea71b01d97c7d4a136e9a3ee6086bb06ba80d98a288cbe0c006358b725c1a5633f35c2c2eb96a6947f5591ec3855754a74676d0511ade032f9d8d0fde34befee93cd5c15c4f06215ca6b041b7a13e00d8ede779021bd71f0c7e6a324f4ce3d4e9db9cb1f75200c8a1f6290069cc70370568a57218e886dd402fcbd4429c662b452dae3db0d24d5c7d2670d7eba88c1657f1324e1ecce0c650c3ae374468e77f63aade3fbfc4a65be3e1dd46cf9856b59473c3fd35add711f52ceda898e72a7e2c88873a3535de210d63da54d3cd08eb0b168078b42d38616010ee876eae3e08d8a811354788433dc320a9210b0e6f48b316eda1647382376e43454f370ac0f77c1305d7a109cdf6be6e9e34d923fe7d17b72b0b1ca60d7410a665121b4c9b2adc3b4c233b341aa6911add034c332950d8f8588512cc2a0208c54cd991b64c94ad0320cf94e57a3eccbd1393dc70133b25f7c4ae4944be0d1919b4280bac22b004adcee62aabd83b636beac96856d6a4b57d31af34b9e3a2ee87f68a1f545889505a6e4ca32ab64b6f508734a20a0961d04049438052210cbf268c05c7661061012915574272d4c31145dba683a5524ea809957208814ca5443c5b31023885a4570b9afe4d0b4f15010450a1ec482055b610806953964d7a37105c4d72c340a5e5e10759937cbc5b321e6c59bd2d9af5a1f1e227825603663ba8d8d5aab2a3431f2a5bb46d2cbb6aa1244c93a83d7988a735b5ec5441fb85593e49c52dfd00e3e249aec0cd4a8603a3be59186557ed0c37adcbc2f76bc4c850a3be2fdae16c0bcfcfddec5aa63292d0da6c00575394d0148ea6288bf9bb996cebff48d234ce691f2c2ca04834901d94aab141db303650d5a43197de0625609ad2d3cee228a95db2319051e1434f663a48b2c7a3a1bea99c445cb3c6328738a5ccdf3042fda8b611d487416a12372c0e34796b9a0940d8e5ec35188c79e9614053910f00615e409e2d9906bed6a7f300dd284675bf0f8c901dc24028f8e9bc46a3beed19fb7d0363900c90d6930d960347aec9c9ba567ec2d498c6750c6d15839cdce3d41aa44d6363bb41057b5483829c4c3718ba81aaec0e9b067a79a36e17cae50c0e4cb54ad02ef0c4c83cdd529d55bf7c2e48f3cfd651b2513bc1707130f8155f72ac7cdc70fba0300f7cc9aae05069b8b542c320c5371e95562a980650b2d960f47d769ba4788cd6c5e78bd1a67d668c560547c2282fb40930ca4b65ef308a5a8ee9889cf0aa8922a8abca7215166e72c7086002ec0db1a29b0083b473783359959eaf95ac9b673692b4dc48369293d80430e544327f0bd9c4d57a4bfebaeccf762fa3ad06a36a1a5dc055bef85880d5b415806d552cda62176a5e615535e2c3c0a6250f1415552320ffd68cb698a7cd68967f363bb01862f5b25ea2b35fe11baadebb0d599b5e8db62b6ba3e53d702cdeaeb32f840bf3aaf17c95949909eb7a3208e512850dbe0dd54de6e4e2da351a36715ac134a7a69b112a51c133f46423a072bfa365e0fa33219af7344606e3dcd39e2da8af8e5abcf5a50d8b37686534fce28d69de5c166fb2c4c65dbcc922c1d44fa94642673d78ba4c9f4d03688c8dab789b15d4ea13cbb4dc1565b689d2342b2b0ebf922e9dad73aadde2e56199efe4b70094ed755c76b7c397d92e2d797b57ffafd21c4a769667d92430e99ffa769d2894aca1d2c68af857d412675f9eed7d6f03effe5a384a2e447bbb5a57d5f704640f16c54afe4d9e10281a74c91742f16d639296752e2b59269aa0dda80aea8168e02e27d532f0be264d8ef9d628995b355a4ec70ba9124acb8c693197ff1a468998b71ca945d2bdf53a5e558d520f4828779471ec30b6e4634ed65da50185724462541f9860d0106b39a2b7816b17bc31fea2318250ac4b14630347acfaa4b836a0e50082135933a6b30c8e791d6acb8467e00a8ec41b2a64c1572963a890812f77a75862c87dc570527699fb8ae76498f694253135d04bea4aa6f5c57f031bf0c194c40f2c85e36ce68ae658fbf7c074209eb6980c1f708f01c1b7b9ef80686473da6ee4d89c4edb35b73ea232f2aed70506d6c28ebac453f88ee1c69c2128e76db12082afb085ab98a0809226dee26218122db09b819df836d959b682c02f94403617a17c50f1ccaa05749e3b6f878df97378c09069bc748e465c6a312b5c0369b716ef3a2dae19a4759c05f376250d3367160fe29a9d171d42acea2512dba056e1b288ade8d512b16103895ebb2ed468c2aa5ebd5606d6c6fba828010c386946cfcb495a5a9623694cdf86b1c754532d5347f55c58a929457993246032d83c01db3146492bf803d2b4d0a8b350c56d0bfe5056295b3d9949045a6a35b6a1dd18a3b8f5758118366ad517c267bb9c6e338a5b020620c35448b881c4c1410dd7a284b659a1ce9266b7cc16fcfe994acc6a1253ef95946a2cebb6f4ac6a1819c15cdd385ba126b1ea35ce46f8c8751acb20ef39f2792101b99a48d4bd3650427205774e35823555010816d8f5f516ab6083ba14ca80385545d57d545040e253ec0f6b04a8623e92e000fdd589b4ed00c9d358c185231d08927c1d238956752c6216309252b342463150db55f6b847b710c75533b5bc99d3155b810ba40ea2e0390c2772a19ed126346583c41fd48e832d0b07e9c09c86d386a23ea56f2c1ce086da09914f0a17d56fc66d0f03a1717f414faf96bb78046adec930d4a494b85ac341c5ad473e8ace4504188c0791f514e056df66a89a534d3e666b83a0360f7c33139dfc356271ad6da4b9965b3c755705746eb78102b9e5201106dfd4906b18d9684bf985cfee22aa7cf3d2db48691281898151d8fccd12a3c88df58d277a2987b969556ea0d0745d4ba83617dccd1a9d68f5fc27142997161e27539ec4a6d3600efa9052e52b30f9dbba9111ce54e0b6e82cf2db6306ad69ab2ea8859866d7ce902b1d90b44d7675aedfc8fcea4c97c12b721ac92233aa4b3504062f57357f75cfe03d98b27bab27774d7eef50fe832639b79dce9c4c2e2659346878adb34c0bd6d126cf346b8455171cb5a6d826b3f4e0b60193a9182f72f51b2d8c0cc0a75ac3881b7ca63581b0b954b92839ab93eb1afa0da6d7d54ad756b260425d7bfd8511acd199b0c9eb6aeab8f1bccf5fb6d3b80ea64ca18060ad928b72bdc6a6176577078c97bbad6a184daeca6c95804071992db97e1a735b321dd45c16c7f1048486bd2aee2738131271d915959d3461cf457013a24d4cdea7129836c99fdc31559a3f5150cd0300939c54d9f9707277954da3108474e492860e49044a09217c7543aeb8c184a4cc6806880997fd8ceb9631ff19d331c3530c1c5f4060988723ce82d3e6d452cb109f8a0bea362a19979d04acab512133ac696b46049300ca307c55a9a294830d4816e5397c81ec5083a3114c616410159cea48d92d29d991a798a41445a879c5533652361d838cf4d977947d53e6dff1949932e1ce50b2332481312c12000a9c072f139a960808412233c50c66d034c94b907284529d18bb28243b092b3f214589d5fa2d8c086d16ad1089557f6d16ad6e129dc1bb0039898369e3db90f641bd29ad4efc60d7659b1ac6c22a8f1539798049a8a67c03ea3e6b320e8412ab26d3c0604b1545cc7be82a0d223a3e7f91451f1f9fe952f3fe5b7729461fcb9ed500fc063cb8a0daa7e066414121dab59d1382b47b094a08a82ef06a3b115c3aa609041f225cdb3bd3d4612faf69a60a385a35426e75417cf7ea30c9412455b3328fc0a62fc12e024231933537ff8c2196c1cb77ba20cb4c57f5f10ed09c0121ea0334b84e9c8640bdcad9d326c02f30bf2143fc0addd7c67bb0af65b4c16c88100b08d926a62cd77164545956b040580c8d649171642790a9e9c8dd2622aaa1cba683766f994e73bc0e47d9d44f3aaa709caa990208c8c9f40b9c1a10acc24f3a348227e5d5bd22ebbebd38be5edec59ba8f9e1c53129b28cb7e52e5a5757b18bf603b1d6db24bd2d7acae69783eb6db4a41bc0ff7c7d78f075b34e8b97877765b9fdf5f8b8a85817479b6499674576531e2db3cd71b4ca8e9ffdf8e3df8f4f4e8e37358fe32527f017426bbb9ac8d0248012be92aa494bdf247951d21b045fa282a8e16cb5918a09512d79e175c26e2b5307ae94f5497fa6516d5a5afaefe6b65d761f1d5dc7f9439c1f5da4377944a6c9ddb2dce5f111df9c235d15bdb4df1001d0d3d34a163183103d396170bd8cd651de061b9502f39e65ebdd263585eb3571a2ff8678d5bf5b70cbe34db2232a5c1129891c856f20d7b36cb35dc75f05b6e75fc95f499cb691731d75d47301aa5675e97dfc10aff99ef48c16cd57a82f2a866c67145cd92236acff88e37b047bb118a889634115786d51f6afb27457f828ab6762d1ff7fcb9254065ec76ad17fb791eaef5999dc7cd3f1654bd870a6ed39a3764ad7e0a64060259d45c59d8f7e28fd2bd225cbd17499a5f137beb794d3a2f9dd4678afb3545249c5abfde02c31b0baabf836a16b253a4bcb5890bfe28d661df4a84c1ec814207396bfe2390bd151c4f902f88ce75d4ddc1fa3bc4ce58948fc86e72a24dc64991a7271aa790ae965599e86ccb3154f78fc5cd3303245992c7d0651c7a48ffe6b3100de25eb75c1f7a763b8683e5a0da8385a29f9351f6df811b796d02a39769f6d78d2c34e6dbf9902367cafef32754b9b8f36fc7e4bd4ecea6f36dc2e52e280132ff275b4a9a241c27cc55236357cd895b799b106b1949b857d712c38c5a2637e2c79e6c2824974fa514b82e6013118dc6cc8a5011868de75a18063a677f6d99879e00a422880b7b4a71bd999697fb35d928458dc301da1fa1359029f6d676d29faa05887a6985cd74423c3701f39c450083408fce01f020ed3aba88f7339b8aa54d714ed34a5e6825054b50ed268aafb3e0fab10c602569dfa182defc5b6711f6603ceda3403b115879c56a5f427ae53aa99917a49d8528a7ae2bf4c31f5a9d67c6e8bbd66a57815a5f7e012b2fe603779caccfa5ff19c7a49ab3430bb816288f4196e88e82a420f0f3d13e53a1e5ec23bacde430e09da137940b4bfe239d11511b04802574493428d0fa86183b58b82fefbc3cd7fb1701c942182feab8b03a1ce1785d6534525e8bbfdcd624faa8a242842b9fdd1c2881284dc1241414e87f86de0f101ef696db25572d31c572e4edc36b664b55d325c6db6b74a523bdf29be7d8bba80cd2ec743b4dee9793625026fb673953c9bb9609f0d20d8676308f6f9cc05fb7c00c13e1f43b03fcd5cb03f0d20d89fc610eccf3317eccf0308f6673fc1faba53a1377773b2b877bffdd1ba341f6e58764339383393ddbb449155c8417084d77722b53fe268ebb1e7210aae66f79dc8ce108cd2417c1dc7b94ad06bf9c804281e69ed2806adf659394ab9809fd68d83ecab8872167942df676f2aba0c0e014c45c76bae466200d95ddf27a1444759cd55728ee6558e873d927d0db52be7b61f376f8b78519c92a549f2652d2c5fd8df2d8e38e47bc39acbc2aa1b5937d16e5dd201207653f834c2c190e21e6946a38fb8d88a6e0c542c2c5698d0192be5b1d01db4aa950ede23a5ecfa4f7bbe5465076d350ff99ceb57f47b3cfae7a395cd26d30bd2ac0c238b275de07451edbbf86a03c1e4491f287d74cb6a4f9520f93c6905a5957789c1ef33eac3c8e14913284db4cb220f5554e44fbaf0d745bd83ea3930304c1eb73e1c57d25066cee1c61c5f93cbce068289523f2d89a41cf6c3f8fb9315890e3c60018b757990ddd8bd44379ccb77187cebd3bc23b06d6230d28ec4eba458d6fb005ebb121d1b9b9d89faf896dfa869f82cda8f564fe8c8024fc5aefa64f73abc3aea83d9b51ff7f04997904472d0e37e39fda4fb532e0cafb1e6827a5b477c09d1fe38b6adae2abe5e67e2438afee7d958687566b1414f58849c64ce20343352eaa82862e9fd60f7235ed7f4bf3c93fa179b7dfbf334fa42c6bfb86ddffd6cc3eb73bace96f732b3fef729ce58fb8d0c71bcb31f66332ae4c486038d063125a2cb4030f350eab7279594cc7fb209a153132af84d608da9c515b9b4bfcd06715036bb6130674efd87001d86c9f056a56e85f464b9fb15cf098ebca38fb7a37a9db98c0b217642fbdbccd03602cc3c01660b2d2740c0cb30d2bbcc6d0946699a2a290f8b158eec4b500e0bb543a104e1972f39c0a8fed986d11be21cae62201a5ac58ffb6ad5be6a407c926e6fd7ad643eda46a3fa982762e0b08a27f3ed291095b8e1304c20aaa95c3720cbe150ce9b589393f76666a2c4404d20bf52ed7eb6d804ed73612af8b59f865c3ecc0633754ed0b17003a55175c20ecc6844fc809cacae8c49a6c8d601bb28aee268252f80db5f2d160e55006169e9d0fd6af3cabf4eac2cc759e87f9fcd6800528c0c3310cc1561ce81114c546af18caca7e609729bea7e3b05fe371177d54fb384dca0ab1273459690b35da1cc0f1e555c4a690e687fc4f3695330400794e2371bf0de1251433cf92f365b91f1f25e9e66989f07de3680d711cde56a175057f0b98a52d87754ea3d49c5653269c1a2fad966f970197d05f9d09f9dd75afb6ac8f4f9034358304d0d08d3a5a51eda425489440536ed6f53db9926338c9269ffc9c2d2e431b102e28147fba355b89e5ce2d3fd68f174224993e24e64d4ff3a4c0021251a76d4b9fa70f34a8a5ccb7fc173ec538e8a2ae4bf58cc3e7483eb53b2892fd2eb7899a5627c61e8fbbc6cd1a00ebca606ac2db275d9c305e60a65d5023e4405906b8f59cb875bb03772fa5794afdcbc1146ad3517bf5dcd8a87cbb6a62a9144cdd03545852a5e5fcd551fb54fcd557585ace66abc48e6be7bfd1406dec8ef290c3cccee7b0f03afd4c19fbb48f4d5ea9f86f1afa6f72e4688bf8daccdceebd073529f5d2ac3cfbb079e1f32a4331b735c0eee2c7db5d9f3b7f5c5a6c42a35a983c29356e00a4898765847d1d6019e487967eb5d51c679b551e0126dc2e2d5a6b926843e515c86dd22699a20ddb5ec7fb6590aac5679757bc945a4bf6545d93070720e7fabc2330897a829b745fdc5c639f898e50a5ef597c06ec0ddae7c9dfd056c0ff35fecb6c2f72d83574517839646f8f4a89ca376a48d62affc6d95bd9d72b4308a7988f82069d57b712e623e58f3ebec16c0b2fb36e541d43e8ee7101bbfef934d523720a6539ba021f9ab45ebeeb2dab45ec55121bd1593bfe2397fdeae482f695777427bf92fb3b140aa6cd241a28df059a15d428d983828b710c46b3d5623aebd67c9b1e87ec4f339df44899853b7fec99207f1896e927c231ec988df2c36a3a3a2f82bcb57bf55b752b90d69ee8bcd25a8e52ea77a2ba3cd56bc09c57db268e55d46cc7a75ca233492fde0c44f2151b884c5d6fc5fd99b6849500c3ed592bf5a58c36c794fec529356fa73b914cca1fcd98137d066f19bed0b873704a2f10adc51903e5bd8d9024ae6ddff3a1bfb2a58b0b375940cb6bb00d5e56f74156c86b1bcb4429143fb9bcdba9634597e35c0fc6cc9eb1f72b077f6f7d9a08d8aea7d769ba42341acaacb1f620a366a73754b5f69640fc94a9c18844f56cf112b9a77e2b922f7612820cfc3385d65ebe1b22dca55f9e306e632ac6da1754a2bbb4cf5627522c536579106bd60a6ad03a14b03bdfa70104e40abcf38abe2d65e04535d10b3dda1d8cafbe45bcb7dd4fa5cc4711fb521f6bfe4d9300a70cdb3e334c045cf7dbe76d542cc61907a04c6e66bf5098dad6fbf5a69ce432ef87de86083653637a267132c909e329a0062441a864958944d680a9ac722834ed9f5352c636d08d5a039a9b78b2a32f9d15cf7f314372be99677f318118acf007c1e73b37d2a74aeb32fd17a8487d252452e5ea59987d21c077ce61aee49e8f98a0a47e4d4ff6a7726239e4aa8cf23d4304e6f778098d8dfe7fc1c78ea48519bec8c4c3f831af8ae164ff36ee4a337c867c0f4cb7f99c2c443adb26f4f65aa1527b2e2b7d96070a4fd1dffbd1dbb7d1dbf1d67f91461921384f66377fb492cd2d5defcd2fd5db43f508112b35945212e7abaebe55dbc89aade16db68d9c0fd4d921725d5e197a888eb228707ed5e2791c1f6b6f873ddff7219a5c94d5c949fb2fb387d79f8f7a39f8f9e1d1e9cae93a8a0d3dafae6f0e0eb669d16bf2e7745996da234cdeaf70d2f0fefca72fbebf17151d5511c6d92659e15d94d79b4cc36c7d12a3b7ef6e3c9f3e39393e378b53916c91bb6282e3ffebde55214ab358b1f06e39c3d81ecda8b77b1b8ae7ad142e62abe91ecd1b1a06391fc85d292d1e6bc3cdced12f26fb1dfbf5e104fe1ebcbc3ff5551fd7a70f11f8b96f087830f39d1c8af073f1efcefc383df77eb353d997b797813ad0b09b770ed35c2ebfa1fa27c7917e5870764d9f93e4e6fcbbb9787bffcc4322643decc378f37c96ed31c3fb6bccb8460ae3ef4b563d7bfba5930ebfb9787495afe64dd678619fb9a270cc73fe2f81ee4fa376bae94d5ab2cdd158bfe61b8468e963c7fcfcae4e65b68aeb4a5677d14eaaae7cfacf9b111a93c04c805a3f2e0235fa9f21598bcbaf3e578b6cb733205b14fbe2ced89c44165583003b69a4e3e467999cab6cd8e13f3eec2a1531cb54f8784b82e96cde0a87d9a21bd70f380b5f4ba2d08afee5d5b106ecc7bb620fc9ab76c4178d50fd982b012dfae05612a3e574333651d5784d7c43d0502efeadbf850dab75058774a60025b1f9438dbdc8a1edec1b41e1ef0068ced0ba2191207aff6d4535faf1ee698d6b25d4a4e78038b467a288c1b5fe499116e506898910dc8d5b3bb957fe8d9df8e07dbe16adfe06d4c1442e3a37c8cca32ce53baea8febd5dcbe0f980016a892dbc768793f0c586a3bd5c730705f46f73c5c90c2537b18fd692db6ecc3bb2da26a7ffe2a4aefbdf85033ebcd840d7031941ff286aeb9c18c3238f0a91e209b8107af0bf60c7454780eb5b7845eb5376f810698c79acb298d8c947906710881734898f1d1d279a0a3cb4fc70e42cc02b8a65bf0a9debc9425668db39c1d0d2d3a71dec59c6ae86cb25572d39c542c4e166575a1dcc3560afc1eea3be535c39b7516392c0a3996cf0237f159f8263e0fdcc4e7e19bf853e026fe14be893f076ee2cf0e4db4b6d49fa2fc3666ecf5939d7eb2d361ecb4882c8776c82c2c5a643d147cdd15654e5ba0ac3e152cc20586701f10b096ac2e8a5362ab1242d0b2f992656bfbb58ff761e3ebf826daadcb2aebb85f9754cb42948f6e3ad3c1e9272b68fe55efed888acf45c19fcc21f4831e40a66cd3b8e1a349296a1e0f1cf190db55be130e90a4da0b2460d26bc7a947d73697f987b7fa6851d564339a9af50d324b063d9074a9ad7183481d83d43c86026d844999a87dcc399f863a04a73603f550fbb2c67ccbd35843b7d3b83633b3a5dfd6d0798d594797d1da4d54f4baceff3c1448f4f99091873d701264c4094f4be8810cfadfee164dfcb5b4bd58c1644df6f017d974c91e6ca65d2771a9956d2f11f5b443ac8c3eea731623ef19e85214236e18f0e43e3bfe7d6e635b3db7947b6ad1da34cae1cd9929c1f0e3becedb67acb5acbd25f4aa3ddc35d5f635dd50f87047862a27b01919b06e6c876c9fd7d763aeeb73fa7a32e112f9fa2a5d4ae2ebb3ff2324ef459f39436bb5092f235bcc8cfa7cb0d82370c5db46cc197847eab37dcee78cb5b4621c75807711e398710f1dcb795bf74bcf3e062890d1e9f2bb7af8ebfd3b5e4bb4b4845e586593c15ad6df930e8157538a551c56b51955cd78d55cdcb79473976dd352c80dddded823086155b6c4f01bf5a68ca8160871c6c65e5fdbeac2a0dbce94359dcf53143167a287b7c647c2f63941ec93b1fa4e0ce116334c34200f1931b180bc243dd048562608453a2470387f843b020d00dbbdc3e6b9bfe518aac96632848464a33ea7b36da2515fdc7799467d19f599460334c9ea9e2980152ec7a807233eb5a825f458621f004201f1c2efb9281376eec5bdec81a6d83d702e1cc1a987a5d3bd1b7b4f804bffe9314ce5ac9fdeccc0b710aecc029d913fbd2b7e7a578c65eafcae18605a27b8f4180583bd1c41e57bb47e05e9f900d2f7fa84c7d3512d3731e4873d33f7080f86a80ade1890532ce2b40e275634ab1b9a7567ebb2a04569cc6e88132a9cd3d02c54688967bb2eea7394d95e8a68297d6f0131090cddf7cdf9e4853ea697cb47e8bb34db87203b42ea41db4b682cb56f109841e63c6d5a3fe4354155263fc42541788059db3e26f95fcd274c483331056048dec13640f76114f96fc2c8b9febccc9898decf83199fd52ffc08d526c0433e8e73189a9e63b24b95e7b1546812e5a947ddb39f7fb11d7662be3c8f23603e4d9efbec2ce4c67367c4a5c40bc2268ca0e49c771ecc802c771aeb84e9af98decea37140423b1fcb5228e2749a3c8f96b0faf987037a4539f973473e7c22e2a0ce877a0c05b45440d6b831ccd5306faeda2c51960e604de67737bb4f59e73ea6d95475582eae7a0752b9212f5aea33b7c1f7bad599d980f270ae2fc4454ebe65ea19e9e499fd34c775222cebb150eb0a1539ca370e29aa9c6d405138319b59e5d30df8b6c5b617bb32cbd70168a569d2b4210dba32311bc2b02ba2058636f06c5620b61e84e47b52cf8b3d5bcfed592e635b083ebe574f46396bc66423c3e1549d19ca8c53157ef6e6e690e5f34143e2279cc095b99eccf266483d96a913bfdd9193494dbd77839f14f45996f6e452fab4d7b8fb144e96b5b784be5bca563b5610d8fa844f5ed3cd242f032c0dde475d2a249b378670aa21ec3bc333ff632dd8e8d92e4bfddb2166491a487ddaac42b3dd4075daf8a1fd9b60e3c794a5733aef6b28ef1d1f97a859be7cb869866ffb5a7eea207743bcfa76964a1310e54924bd489ac8354f22a122a175d0786a4fe2e8c45145887992073362de25e5772d8f3e10cbbe0791fc3e83a7e21dda6a6a78d2f2e3d672ed133d69f9716b9906157cd2f1e3d6711509f849c78f5ac7c4f97c52f1e356711d51f449c98f49c9a745912d93aada6e535a4efbc7a67713947e9eaeaa5d5745bec0a6a3343dfa115ce072b72e13ba414d5af7f2f044cc79ffe243fa3a5ec7657c70baac93a99f45c5325ac912273d5c21dad68157d1b4fe3bdfb27f922a24408d69bab124a2176ae9193cd1a78cea245d26db68ad939240040e06631a452a82ae36f1cbeb784b0fecd252239060ade82a13146412d88b63068e7a94aad3b0b25805f7e2f703b19a3cb36c0b75c51e337eb16978f717c68a54af7848bb1aba1f8f8e4e24acec15305d2d9a225feb98c0d4a40a9e10a035e858cbfa3e29d49e004b0480a2fdf05ddb514e141864a8e2338f894e5d1b4603a3987477a116e5ac3129250f965b2517797458d4a7509e330e1bf4353987b5696306503d6a96b6847b5038392956970cdb015c16350b6a9c185b6c5ee78526c9f39ced1bd76cb945fce74767d7d44a53d4dd67a39e0beef6745e7dc29d15ee269e4be5bc850bcd1633b3d0851286722b5db0c02838047231b22d833e0fb324362655556042b949efb04d6f856153164b450be02b4ccecd1d01f6bcbb58415ebba416dc4b164ee2a77136d4430cbf4030d7b9de0a0ce8dd3cfc2ebaeb009bdccd04cc6e3de88d76573a04e4b42e7f15e1a85eb3ccc66ceacf39152add379b69d75e30c5ed24c86dc79c98971c8bdf19fb0daaae81ed930b3df9107a294ed1ea2907449be6d43022e6b4829be308986635e785e289d7744a448a3f980e331b3e3d81d66243c5be5bd46a84e2b3a21a0bc122d1d427f47242f13ac13612c1419031ec092990321d3820854a0d8a5f27f404402f3291bca2015dd2f69961f61119dd79c3763ab3eb07dc496dad5a7815702bdf1c89de392fef828dacef7e89e738caf67091c75d39b8481f0857d224e3b6f11cd7767383ffb4eb3b4708cfe2064c8743d2be9b641d2fba17d8062bad0e0ac26df8aa4be13795e1960263412e3208da90f150146a0fe61f48bd4559c2ee81fd64b8ab5bdbbbb17687c5b3f769c5eef13895bf3e561f56eae9ec7d571999cd2fa8fb3301ecd3a386a487d122aa8a811c6423e0b1a59ad924bda7d7bae633854f79bdcb69084cec2cbe8fa3db5d5cc7546e6c63fdd379aa83605b468220ff611408ca5d005ac47f1e047a5cd731aaaf093ca10774cfb7eeb197cca62eccd4f479e30e7c26b04756cf117a532f8f731ad735668243bf8eca6871152fe3e461af0008f744f001e1228f69ee85bb88a9bfd5f9dcb058c7ef7e42e2f783c43ed4fb4c70b850f66008b58f064b43db8682a33b309449083c916953396dec9ca0b97f8be4b9e0702666711fdcc42685cbbfefe2d6c5dd3fdcc97de05a047d7e44b803bab707b8abf2713780636241144ac031049c76b9df47811b5323d77245abf83283008f95016ec2a3453d71a7eaa26f034606dfde593b8781b0d7f6cd16dc73306c3419cba26eb3f66e4c5758d664fdeb234754df538c6669c191c1a4ae722c28359bc7583001368afbfd7b00948585aaca8f8c294d9d2380ea6cbd2bcafe49d6e2f76c156b612510700a95be8d02afa656da725dabf8ef83c04dec3f46fd2d8d1fe8a03e7ad73e1efcd8762fda34a2a62b540e06ce10a26a3e489ac270b962a8ca19184f6bc6d8b9f17755cabba08a3640c9019c618d90b51a7f07f3fcd94f7d16a85557393264a85fb74fce147e8db0c7f6c86e35301ffbf34792a6714e1b6eb04296aa9cadc59966d96681c65e23b3d8586fd770aa5e0ceae3c80d51ee8e0f0f9a912d0ad0b9f99b160839d5ea52bd39eeade3477e32e38883aafc34c7339aaaa781e0deed93cfc5ee4d7f2a18a8eeb1efcb3243c6003b6f33534da183a0d069440c60f56c4d4f9023426be81bab1e1b838d08955bbc7e9bde330ea03ec991b2355e98564e1731624b655ffdf2b988f3c5d93a4a366a574d28ce3ffe14bf8df3289eafb56abfae594d8161de7d0a12c080c0f7153cd43b4cbdb4f46c50f73ebb4dd23d465dd57e5db39a028f147575eff60e75a8458117f602cf8ee1de224d8099a02b8909d0427bb1c7268afea86b55fdfd911aa8aa73f3b74f4dd888b7e4afcbee547071196d35b80369c0a01c6291515058551a6d8166095f060dc421741d838496c413887c2ffd2b1e6d09499bd1ac1ff7750f4dd111a059409947b49ba6eae31e4cc26fd7d917c285795774be4aca6c10141af633a4a6f0d64cfefa881024f70e5375adaa196167ef9eec3e61ce167313bfd165fc4eda8b05f527518b069d7b0eb9e6dfd3a201edbd4fb468a00527431cbd26222c18a8be7ab68b3779b65928d6169fb285cc608cc5867a9e05dac356007d1e6c9764f2e58349394eb58fb68610a2996a810984ac26e04406fb1c3652a51aab81c3b08647ee741128870ad33a1a78ff88a36db7f6f5476ecd6e9eb06ddaa6c36c5be4fb012ca4b0f9a2f534df64bb7076b666374fb4366dd3a1b52df2fda01552d87cd14a9b707d9f60acab9c028b456bcbc888d440f9833028edda0423b4ff3c343aa7cafc6352cfbc71c964a5f20466c5696ec8ac1ba58666f3fdfbc026a0a1f982f35d52069bdf09af794eeeb461ba99bdfafefd4ceb929ee608cf3eab05873a0d484578821c8c00f54c51a579580d36877b710b97181a9893276ac2bfce366b74a4268fb1eb5f39dfcee897c9a7833ed0168e3df0f909f4a55a788f14f1f5e68833e265f2e9100fb485650f7d7e427ca916de2345fce96693b95b78817842fb2eb684632e7d7c427aa912dc23c579bb65e384f38a701638975ac23da9943e3ee1bc5409ee91e29cacac9dcdb9403b1dcac586b0bca56f4f182f15627ba410671553ed74baf92e15e52ce02e3785e32e7f7d827ca914dda3007dbd214e684a4211e76d4348b3df247951d2a4185fa242862ca5ba8e4be0d1fce141fdbfca8bbfd7cbbb7813bd3cdc56bfd2c0e9d117f9deb08461a84626f278d70565fd70617d6b181a6393908dd13723e243cad76d8149f0cde9c7b3ae357d296c637a0a94b6aee2edaeac1068408a5c50af25b93caa3d6ff2840c26435bf842aa76f0a54c9ad11cfcc80ad21456b5463c04a90f3c91ad924f4a956d928b9a5b54d358b508292158364599e5710f5eb0aca11dddf4af1bd65021451bda99043f96dfd036ebc68d5800ae592c85b3b1b4b9a765498c416c1a2c60593326aa7fadd7f1aa393b47b54b4eb5ab68945c506f4df8acd9c6c6c8f9a7e576c865944d908b1a1a0065e0945a00156a9bb0aebe752d808aa29a60a81c552d5ae8722e2940ea72a14eecbb2fc4a965a42e17b56e429df20dd18cba20ba2975714373a020575253a0428a6640452d9aa0c4025408d104242eb8309952dddcd7b6d20dfd31ee07205708539d52dadc576d7548012bb30ae9aae64b2a9b0194c5b4a68e5b0bd75f7fd376bc2e62a8080cce2d55099652208b298bd4b494e940553fb6eeb61cd665142360c85e9058a2adff81995dead774969535019f4c3536c554d53671b12ceb6ec2fe98ea6e8aa9ea6ea22359d65dbfcd33555d9752d55cc73c31552c447290aa14beb795dd929f37d9aa9f43857248f75a7c0ca674b0c582ca76a808507e1d104a40e1d8012515830e28695289fc3458568b5c4655bf5c12250be20d6eb2339d6e14e58c1e6e5ddc66489887837a28e08601f43251aa0f2aa4042254d8b4d813d6cecc5b3379cda72e8b5d9a3324960d6b1f14195bd516c436a92d6fd99ef6c988b13d6d416c7bdaf2e8f6f40f04346de90b21d6a75d59ab3634ab5943239a52b856e016c8e09d5fa35eaa5258a554854d4e137cc753769de072f01e86a2b0c98e01d7ee64530614526ce100250d2d80ae41492d800ac12d804a9a64205d4e9125201551f45f2a675a1b4a1706e495a15404ae5b2e67a85b3ac795aa964ac0354bc54c0297cfd36489cb650cbb86caea99131efda1c4823bd560c83427141c8d782cc51c306ace4048a37047192a8ea21845864ac91cf3a241884d7d62c40aa2752964115ad18f294ef3b959c557576c48e1be8f8a12d0a893a0f5bc0645dc4c455ed7c622b04a82a514aba2bca9abbcefce740e76c1672a2cf150910fdfa794999e6c78d1a90e59195ec6e3524f7c9ded727a17413c6d32a00ca61aa1bbfe6a7016187b6cbce0cf9055d252930c8f2de8c09ce1a33d040f2222dc1054933c3611c987ce0bde4594456422d1cc8b9ae3f87a62441cae731cd507eb153fe8b3b7c878fb52f55d3509aa8aaa3b049fd3579dd19fbd8717741834d5f7d7ece0c4d368baa95a1ed57d34ad77264792328e81516848ca3187a2e9f60fc7552e349c30db3b1c0ed214488734feb3159ff8837aeeb465611686fa7e1b270ea8d88c45adbce9b460029f6896aa06c2913a0e2dbad417bdd83517546a40a1ea418ba29b0aa9d38b5513d384fe514d37668b80a09e62aa0aa634dff56e77c150e74c1a28c6989ca61798781573210438550bce48a9f10d4d27bab58b883d980594a3ba8acaa8492ee22f52f1aaa871ed67a098cc4eaaaed0d6f233de880d213ae1d22f46763cc90818995a68f040c4edc9e8c986df97996688ca77a917ed3d6785b40c14ea6e4277b0abcee92e56031ca08be40c1fedbd70cf7914aac13091022443626912118117c21757f1324e1eb48242110e3af4b477e41b4385baee1e4c6c5c0613bcd0d4894f1eb5c8165c05186971142374d3c0d5c07118a119674403c9b8f81a5e44f25b12a3884c24438a48fd94a6e2837819632f22e6c1067bdc5e40b2519655770978a852f545f3fa44450fbd3c117969df91f809c7041c65d92111e3205e5731d01bcd8b9aa56aa7012c67683cfb8ea76f3af43c67aa8e370e1fa2eb7249430700d56b5e814d2000e1cdd0823e5c5289405956dd09c5e3a6aa23d2378d30340fb5585efcf750c261b92e1a1baddb0bc1900d879b6944c50e0ff6f19b7e20312507ed90af803d05428d1dc6ae74e506b62a7893ecd9f13f92348d73ca188707a9fc8073cbe008901f192fb80af4ae2940311c2ad4efab25bf7450f154b31ae8996a4a0fd8adc99d77a0db36eb1b8864acf5cd5838e236ed18e923b7f9648a0111e02f6e5f01356de49c4883840092e19683630945786abd68df900347d57049cd8916fca0be3ec612bfe9ce56356fe521664d81e0a2699fb89b45d3941c5d34dc537e88595320b8684cb6585b7e78314d35989a37d96679d40547c70bfbde1ce2557f0f764546c8eb7b196d61e1688a9bcfcfe1d7eddcd9b9feb93ac7150c1b5131d30782709d9da4280ac8b35f03ddf087bfca90120c337388087bb149611f16e7aba4cc340233500c292a65c48b1a51c6081621c4633a9d33503c2ef1089135164df01eadc1660aa20cadcec842067606067b887ced8050074f0b3fc204a10ea85271444447719d2a864c5cae9c6746cb960eaa6e98bb71812f2f3a2b749864de4a558e903b7c4225f2a17b400d2a82f038dfac1f24bbb5fa42fef0c9b427541f1fe908549f226691f3a5cb3274ba67f545cde05599d516fe458b3a1894a02e6568271f5585ce80ac55d6a0e99627511717370bd0171c04cb5561a1b3022b953568fae109cd21135d0cb48550a8308763fe8172e4425703c648c70b2a2cdc23296decb5fa441e1548cd61d91a3c972bb4e41d3661ecc0ca5147a3abf999a3cbd9ab257cc251402d0367351d582dea107d153f44c83d87d112342b263452864bbb39f42851c42bacb9992210ba85bf0897b851112b63a0cc9003ab4215beb13e51370564b45745c8dc828022064b5d38b01a14912c2b66a6e094ee27639dcbed6642e4382eea53b5d0558d6aaefcc2d96814f4e2b866d405afe9bebd38ae8385363f903fcb2c8f6eab1ddd7551fdfae2f86a47a83771fdd7ebb8486e7b162f08cf34ae0213f54cdb3217e94dd626a9135ad416693fb7d70ae3325a4565749a97c90d5d59e4d9929ea9a5b78707ff88d63b52e47cf3255e5da41f76e57657922ec79b2f6b6edf9926b9d3d5ffe2586af38b0f5bfa5711a20ba49909e942fc217db54bd6abaedd6fa27521284dc58246567f1b93df6b5d121895f1edb78ed3ef598a64d488af4bfaf729de6cd78459f121bd8e1e6297b67d2ee2f7f16db4a48f5c1f9215dd215631312b8217fb8bd749749b479ba2e1d1d3933f0986579baffff2ff013d180e6f1d1d0400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190639277_AutomaticMigration', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3e4509a199f3321ed0645511aaec4919694ceec5b47a9bb4896d95dd55355cd91ecf02ff3837f92ff8281bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdf17fffa75b33e7888f322c9d2978727473f1e1ec4e9325b25e9edcbc35d79f3cf7f3bfcd77ff9cfffe9c5f96af3f5e01f6db9e7b41ca14c8b97877765b9fdf5f8b858dec59ba838da24cb3c2bb29bf268996d8ea35576fcecc71fff7e7c72721c13168784d7c1c18bab5d5a269bb8fa83fc7996a5cb785beea2f565b68ad745f33bf9725d713df83ddac4c5365ac62f0fdf67f7d1d1759c93361f5da437795494f96e59eef2f8e875544657f1362b9232cbbf1d1e9cae938834f03a5edf1c1e44699a9551499affebe722be2ef32cbdbdde921fa2f5a76fdb9894bb89d645dc74ebd7be38b6873f3ea33d3cee095b56cb5d51661b4b8627cf1b911d8be44e823fec445a897bb35dc75f69b72bc9be3cfcb88ebec5f9f9d76d9c2744fd440a62adbf9ead734ac02ba0a6e3ffa7e7f2c3c1fabec81f9adf7fe8e0435046ffef8783b3dd9a2aee651aefca3c5aff70f071f7659d2cdfc5df3e65f771fa32ddadd76ccb49db3fe619615f7e6b1afe3e7e88d7870775cb2ed2f2f9b3c383df0955f4651d771a3dd6b2607bedc3e78f38be0779fdf29381d78b6346231845d1aa5e65e9aef0d153c7640435fd5b96a464747652a1fffe440c80b5907fcfcae4e65b1056b44d671931442da757df4a13176b4d9d45c5ddab681d61c614a71d86f08783ea336fec0651d36596c6df6c902bb3789da58c720641ff35955e51264b326d945192c6397618d4b3cbd1e97249f57ea462d708bc261a44d0ef92f5baf014741cad3c599c1605e9b827934f71b409d09debbbccb725bf25be1c2e88e7b3219ecfeb6813dd5a8158e6f56157de664ebc2c06c4e9176210a2655923f9a28c3704e2c94db26c3c0597d941cf7384c9a2240db4b0c932838768bdebe7878cd41fd0ae5f13df32a65239cb8ad22c615afa88a319cf9c9f6ed8e9cdcdabb928788bfe2acbd671940e20cfd749b1acdb6b27d3966e44b9e644b0b99f5c4f379bccd7df8cb6748ce379586885fef37d1cddee6222cf0c6b486a8a239e989b4d9b2283a885feb7150759df11db7b7870197d7d1fa7b7e51d716ea2af87076f92aff1aafda511d1e734210b5e6a78f29d596f5fbee48357f286e07915af82b8b9c4d9898be2136353e97ab8d103fbd1c17dfe982783ad74fe7d17efe2ab28bd452f461befae223ceac987f7e62e13ab610830a0a819641c5f46e5f2ee32de7c89f3d3bfa27c8595e5358105f9d8ca546433824c85a5c8840bfbab78bb6bb75e7cf8bca15e15e172b1e24662fb33fd897e0aa5fadf88af71ba5ae54493d60ba5b3352916e797514a7cd76e64411c8747c26f959f35b0c5fd98e5568e92cd10ac864de162caea6117dd933e1fb16cbe2393769e9649f94ddeaaac7eb65ce0345265390c2049f28dfb8191cd557cc3f5820e77418cc722b984d48eb2eee7db5d62321a2a26068789cee6fe032b8f37c96e739e2abc195b135e994c79bbda61afb6d94095d832df2cb9d23d3b8021b707683df7dc2674394eb11dc41d7c4fd646a764be7920e00fc2f06c97e7715aca935b0d4cc39498e445f931cacb5486b4c9be741e891d61e518ba1076fb84808aa12d49931c7f8f1e92db4ab192c77e97c40ff18648f57d42e7beab785d952bee922d3f022a1bb69008dee4d9e62a5b0bd6522cb7b826abd86a646588c29fa2fc362ef1bd1070d11a6c535f14648a1e81a5f5fd82496c7bf7861a9e95413d75a1a6067ede12fba32cdab58cef8cba7cdb796c4f2ed207228e2cff46f7527078034814fa914aea752317b7d54bc781fc7e93ac634387c4e2062d698b2b34a5a7b1d556bd6cafcd17764841340a7dc945f50a03cadb6aec639e3c90b9e7922c2ac85a43adafbad8d95d543645d5bad21795f564286fab236686c1aa08205168482aa957905cdc563ffdead73096fa82288ba72dae184b7a1a5b3d357b1c8c2761561444a3d0945c54af2aa0bcadae3e178a3e9c6eb7ebe600879651aa46574ed289b630a48c17c7fd6a0eb1c6633c10eb0d0c86f648cd6ddad51fd322f785a0c0c4634d18e2b428c8e294e952bb27c638dcc057a725502fb6b428e9daacafa7e9bb83bf6eb4f7100a170003d1a6e0e8140606496c6b6d147274938181194a1e5a1e36b2d133b2f6701a3fb06632dc720e9e330d6b3f27fb1cca32cfc9260376c5d62207375c189303cff78611cfcdf986b2d2c031124003c4055bfd90f18256cb6606c8aaf6153da1d5f1d04fd48cb0aaab126f63a226b2cc597d8cca32ce5342b98a1beb3aed7c1cc2f1a884f2315ade9b0513765a0f3ad2240f1b3b343dbc6c714de573a622f29a6ac4f5ed70196b3cb5f7298b279310a7c6ca5dfbab28bd67c6cbc92f4eceb43f17f0881d73e9d2e074197dd1a09b16b08789dbe8f018c1ec5eb0cfe865f94c3572fb8319db510b1fe94c3362a91c3d99d013a55898682b983798fa7317176553c87990f0c8c19d63f034da730cee0f7870a8cbbbadb2ecfae27a2683e84bb8412edfc7b6f58bf9c1de1d480037bd83baca072dffe6a6808339a8dae4600c5a3a8f015849abe7e17411fd8c0ccf5b2269c06d163f4d61a836ccbdfec5096f6c0c0f0b7c6a7a365a4dcf47abe9a7d16afa39684dead5cf43b44e08e1e96aa539f969abaf0d5cdf8885f80370d460472aaf8d2ce9edcfc589295cafe355258142b79f490b9c9665b4bc8b5bf30ff719535eb18d6720b2ee5d8d9478ad5bda2aa78785442eebd54c256f2ce14987de83f5b9e2a0dd8755df88f0720e44d087720d44be4f8ec1f7e018885ab765a71c86b5ec74a35069d60552c444c251288d8d816c0043a3ac59a445f49127c17752a0b3eda56a7a9fd64540f7dee06278596297055afda2f15554c447109bd6e8d25213dadccef01d9bcb8a56cc7a3767d646f6a268dd63bb67b1c0f6a97ffc92d7f14d4410707d9f887bc41443f467c75e0eb4f3cc3ee7a1fc8407d5ee2b16b59f4eb9579ffb53147e98f636c7505472ee4ce56d1d73c47d69e1ea32e4a12a8a48ad5795f3f2496591d8d9c18a16d0590833e860f6bafa5dec18473cd6816d08e359f118d082725ee5d4e65dbb4cd6fa72012d8be8bd602d5138df1ba851e9759bca62ba63f6b49dbcafaa163717ac363d4a66411c31eae6013b781c6cb06f6c5a51a2a73d777d22263e18004efa14a7411b25b6b4c24439d1f4c17a51b6b3c7301e581f6805f2c2faaf5e5653b965ece1bd8826c5e4e5b85992cd26735fc351ea2391c5686683efb1dcb52a6e8d47e72afa2399cd5c3a7876175112b753d2ba8f1d8b2390d95c7afa2ef1d86820c4470283b9f4ab5db2ba758c521e892ce6d2b53a5e93bbd66afa2399cd5c3ad8efa7557e81cf85a4fe2002641ae61442d165f92805db6d8abad0bdee79ceb0d3c0a164989e038ca7ba91167af9eff624aa5a85f8f10870f45535e37a9df1aee3f99fbb64cb7cf2dbc133dee0824ec2394acc793b43a07a1864a0b27e99ae3f2619e42a01be67a14e419a1a2a0f31a84160380e6a08a15b76185348fca990fdedd8cdb2b3b50f12b2bf2cc75976b95b0df8dfb31658cdb2bb62d48a400e8dc075b2175945113bbdb9ee083d2651fadfc1e3d95d14e7296dd3caf780f2a2f89caeb3e5bd3fa7203753fa4579a82b29d2c0c6078b91488df162040ac5046d2473f13d143718eb1a98e9bff905e892be281056455fdef6641479c13f68481fa47e02bee81185e66d78458693c5bce81be214ed8227f779e3d3709a7c3545574bc2cefe39d3cdf6f3708797f208559d5dea4bcac3445fdc3e44146792863563e6bec066cf69b843a1b9ac42ad37e35de6f39d06366dc2cefb310996ada80eafce8d705653ed67e7115e333b4f55c30208cfc691f4a3425f521a1486e203bddde36a728d67a7bd9daf897fe731befd47f6b463ba1f54b6633ac870ac3350306388fe934d4e3141985ef52d9b0a3bcad3ef602352843172003b3aa5628044741c794202846e9cee757955bddbf3f28ed4e7ed48cd8526aa9ce4a5ba06b65dc3b0e13739126df44da624e05e698b879f49c2c511457425e4c250645e0bde6b1c5216dffb581c7c2b2e882779515cc5d1ca7f13ee3a4e57de4ef655bc8c938780d6a76568356e2bfb2052ea462f408018c31095ad55aaa56edf3b9eced837b638b6671c8d97856ae2d73211879d1237c96ca6b25042a6005b2ba549346039de2a4e73886c436dd0376b13e43e59cb58504ed686a2d2703095b71de2152364579a2a9a8c3eaa5ef0a524875f53d4cbd997f97a0fe36957afdde8711ac0731876554870bb64256fc97fa93e80c731e227878c34fdd573f6d14d9d33acfeeef230f02e5ede07cbec1d26590efb3ab3ea249b95705ae35981c2d2e0b0247aa3d397d45a4ea0b8f5b3a4d60a0e6134316d17eceb2093577b728098bc80a2986e845b69722871cf1639add1ef4ca6f5da12b2b5b6373c8818e662ae2bf268ab62dc7fb1b5d6794cdfa67a9bd8eb32ca43f07993a4497117a64142e0c50a12545ac8708b221a76d4c07cb879c527b877c9f89ca4699c7f8a23e9e4d930f3d074c2541817e975bcccd2956d3b5c77bbd979812dda5b3eb88464f014c56c6d361d981a8bcdd6c216859bdb97d0369729661d6585681a25dbbe20dcd4f6bbb6a15d21db66f6b044ca5626809b2d96d3365f2aec3f09daed62a853267fa7c174434ca561c2590116d36725e014aea049bf2dce2b5c6e6e87a92a64464a91fd9fbbc818b5c26932ad3bed1bbd9809840f2f1fd8545a7c61c1de0065605b0315b45ef018166ece93a6b84364985b832d7458c9a85638aa325a317b6dc861e6235a462763e13b2c61b190d7ee1b6b1338c4face412cb719244271ba7c1f2ed5d9d0194dd8bc1ddeb94ddc266383cf8e1ccfd666131c223afbea3e4ce8c0f31918947eaaa1d0ba24b66320802be3e4960599cdd0cb14dd4c26ad6542ae556d270470f652cd1a4e486f76d5ab2d25cbb0010de9659446b7fdf10cc070aa41d0ee93d90e02687fcd76fbaa9682ff7be85cbce0fb5b5694cdef1eaef7ddae7c9dfd15e614839ec8d0d7801429618e45aaf48865f240a01ae89e3461163b1c73c9eb0b16d7be0b8c1624ba371442996abb47341daa32929d5316744c6d8c69392bb08544277542575c3287189a10d631a8659cda2a32a6c93adc4410ab7619113f2d8de91f9acb71343b97f7ddb8a62ad18e0e54db6047d7f3b7b0b2a114d0ee76b6f13ed9247543633ac83dcf37aeefb27adabb8aa362dbe782ab8f539b29b1d3555438a452f9bc5d917ed2ceeef869bb49f7c17f779e39fa5920f494a130b1eab9c5c9ae0aa9ddb17695966dada9c0622a6bea62463ded67f788a41b0b984c860293f34d94ac3506f1d9cfbf04b088552dc44dbd49f24d8017fe5151fc95e5abdfd80736435dabbe8e97bb9ca0968cd5cd76f0da3ede656446dcd5e741e3d5154c359ffecade44cb32cb038585789f2def89b13e4f57744afa5c2ee5190ac9204873ea97946f0898e355801d376ab00cee0f76fc792c3ea24411b44130ad8bb6289339032c2127c8808bd96eb5bfcf6e1345e47db186b6a8baa9750963539b62c16f6d89f5a80e3474e58c8df73ad8a02c71c26e4aaa1b5d1530b6b62e15d29fa8706619d9a2f22d203e3f1cacef8bfca129b737aec5f06900a884bcb7c9888029fde0935e55d33fa2f52e7455ae18ad0c4c008c567c26c368553bf9f9215951cfe5d84cd11626ec51e55b94d98e04a1654a9d9f3cfb5b9089569c08d86e8e5db9f5c874053135dd01304cd94c066118606051da4e172c06b0946dd5e155dadcd3757a8b539fbe46f704d7471c9fc996e3cd3e85d3dcc9d08e3587b6b7afc35eca266a087a19bb7b14c15eb4aa7f737cab32d88de15672350c758e7813c19227581021f12eadba9832653054364816569eb1c7588519ce25f92a3b286c07f130034a7ea865077fcdf586ad62ade50f4f55ba4a1d949de0492f240483a6cc6cd000b662bb8fb53dad9d93eaad467dcc68d7ddc605ba486ff28854bf5bd2761f29984e3585b607a80e838f210d7e27db7ca0d5841de083a24d7844e61b8e4c428421d4a8bebc22d6a881c8cb2ebc5d675fa2b56b582789fabb8d241324a8caf92a21aeaafdf517e11c53d20af62c531a5ee9ed8e134d1dbfacf9d9d9519d73cc1da541a875a3310892d4173c496f0af425252360281e3c108d5c9f2a068dbe24a223e122cfd416917475939d59bb3850847e81d5b4b154cf1c3d6d9e7aea375801da51cdf6ae776c5c76d002ec9e4dba7336c1dd87a94f4da9fc8d876354298ba6247838d617d01d8e31a5ecccd76ec3e24d7e6d7251bc5947b745d779ab1c8dcc1b0bf9254b9054719fa9dd5e7f23ba65b7847805d5cbefee36544a7a511deebc3cfc51522657f65db25e2f3e918eae3b8a1304c5ab6f8babe466dd57f30c47749d26043e3ce9731ce96592266f776947f613b2c6bbac64c97ec6917d4c08067a99fc82acecf26d47f2df7124efd2e4a697c5df70446ff3388d563dd9dfd122ec694e0cc820304fe202c28701200ca1049313034e785a082d2706b8f01c44d09c185023d42f60e7c4001e9e5a80d089014342d50c924e0c50e22979409d1810c5d38ab83a31004b123663100cf0ba8ab7bbdabc2eae2ede7ceae90ce812e8166f480bd2554f6e001843fe2e8eb7d59cd4921a9025934a951bd075fe754b0546ef8d363ba82da1015897f12a5ad7619f16af627a2df52ecb4a7a8edb3230804b62503b473dbd0162123dd5394b6f809944ff5b9c673df5dfe5a9bc9eb43513b99895db67169752980f387d57d10c74c65a80a5690287a1681a45dc90339715d91b86ca3953f42707dd9e564aa8317a15ffb92388692ecc5b6ab95926a8f8350ae637a0c773d4fe8892b272d971aa666ea7e2b4dc118815198de4ff8897658c56f7ab366319ce35e3fcd39f1de0714654779be5df1c87be70dec0739b1a125562462c22ea349d58389c6e36191602d43c264bb4935ee538c5eabf4baf8675ca3fb6491171de38cde18c77c34bacf3cd1a359709ab4d2f1d62c2e232af4fb9d67c1ddf44843316b07fdc25658cc5ebab75b4bc4703364a5767110370036289ab1ba71c8101b515c1c7e46bbce6a84c4b8264fdc000dd80dcb7d97a85452e0bc7bfb9ccb195eda8f661bd37412a56471cc769e0f85bbcdec4e5ab132c1e5f65ab6f4c691320091eb7e4ff190a032a3f46695930c50da0ac9b7ff50c8b49da7ca6b4c98cd2c630c54d605c670f315b7e604076d342104c76dc8e44bed320f3639c6dd7f165849edb6b823f687fd15e7df6252bd92a4c7e1e2dcfd7605a03538afff8f2fc397aaefff625cb6fdf57a1987128ad293ee519dd68c662b5267a9d67c9ea7c4b03b48d0359e23d04012be173d4f39a06a0cddecfe2eaed072c445b9237680bda525cbe7b860569dfaed73fa3ddd16445c4b938c1a2b4298fb6a44df9e7587836e57fc2e2f27a19a571ceb4dfb48b5797efdb6fdab7abcbf7ed376e06d7047d074c9bc0ecc812377d5143ab5e0805195d35ab238ee33463ec32592765947f7b1de5f78bd3a2a0ae33bfbd6dd003c700dc1eb7a1bfcbcadb1d7ab6e06885cd6dc378e448afe86641f93edaa5cb3bc6c21b8628c7a2b109120fd3366ac3a376ca61059836527916900a4c5ba902074109a68d549e5a508361d80bc40a45986c81c045a50be331d132b949d403c17852d4913b9d12f5d482fc8d27441da578be635a0976842ab99bb6353a064a911ba715c2a15a822b646e9c663a7a50e8c659a727174d8f71fee94805b19bce8b184a85dc4d47460c0795e04d4747ef3fbc3b5d5c463472c8a77c57f4db25a613a38af0f5a7d3ebab6bf4615145f32a4ee3f53ab9ec676cd311514576bd4bdf46ffd3e260a8a62a33e21dfcfd17f47110eb1d381dfed0adaf6acf31c81d0ecae8486039ffab1bd74bb242fafa1c3bf5d7c57fc2cef64df1bf61a7f8bafcdfb0d3795dfce417d5d48d42c1f99fbb645be9ec7a9d95ae20a06103f38c1e671647bf45e96d94f7bf483ffc37bece306f31868509bb1b7ef2a3a97465d799f2367efe8f2e8efe79b3bb5fed661391fa1fe8c81c273ed4a9b7f5d07e7654dc63475dbd4787de2e6cf62fb1fe32dd5ec43ac6d5e622d6077e95654c61c364f1314f36c4dd144ec68c3753e8334a99cce4dc9e7fddaeb322798899c5b7c999ec69fa05b8c98dec6918036e50c787f28edb1430398c7579a64d06add4e599f658cce2a2ef871afa6c12f5f699929b0d67391dc96c3933dd7c1f71d55ffd813fe5df90eeae58236cb017d98abeb6ce72ac81794f531be72a1363a1b95ab875bc121fbd1d89eca6d557cd05bd4f5f27fa55690b254ef1396e27ccfa7ab6ddf4c7339b78eab3f154e853d7d7049bd535312cfadf67452913192fb8147194d337b62c91c1647344cf54b317eea84b0c806aa7f0865c8ca33aacaa4df7d66c347db53b8f9855b0e9586b7716a7b4a568f5eeaeb35d8956ece7e2f72c678a1ba6bd7fdb624b7e2ef8769826ec33e61cced0c5d35def98b9dcb0130260f839dc1cb3890dcee97a9d14c96e8385e26fc9ed5d9e14e81df4eb28253d228e25fe0a407ecfdebd3338930dd4af4b2c7aa9f0bd56d162be45c7ad1420e347c774a26572973413b9526e7363e290d0a7c0c401e15c7d7f16afa79a63704d316c1fa5ae5e93b99b2b3fb0ae80b40896ca6ae77889d3e40e5d29cadea8aa6c1bafcef22a4a31d2c85624757070f486654573fe95b9d06930b5cdad68369afc8736de39d6fa823cea9d7eec2e4bc3a2922a67cc0dde424347a0ca511916f91ff3781be53147e2e1c2fa81dc263fc494c6e98f8c9fc30d88af80cb967759132a02f05b4af8c336ae5159c011fd1b810a1ad887d56297bd006b54b8940358abc29bad819fbd804911dc150eb0db5f755fedd232d9c4e7799ea177c69aa73eb500784a83da29aed659b46a0405713060a1e5509b2a8881616669b4a76981615e6918a81b60ba6fb0abf6e36e2add8e739f52194dc7715e01e326712361ec99e4f4215a275ff017bec872b020cd403bbad7db68a302396e6757883864b733c50a9ee7b40727a5573bb230c2dfb63e4f6fd74981de737c4d3a56e0771bcf68a2a642e9a3499b52d136e2ca9bf69ade65791ce1f79b08ae52b6b3b84da7d3a2c89649059416ff6caad6265d1f9ba854d0253128074d201003611f39e45b1fd410a0393cb824484b685491a4fc4695ccd548eafc90be8ed771191fd4af46e973c36259bd8a170444babc726e6e172c156c6e9f618d6fed3f498db88a6fe29c06d78d686219ba0649d252ecd2c73c4997c9365a5b0a53e07360c832dcda8c63b1fee3ae01e297d7f196c6a54a4b4bb9056b59d70041b726b9be3866c0adc77cfdeed8d4b3268c960a50364ca0b1a0a63763ccb1157a888f33229d86874b2fc71f2a2e887824c3a60e462a594bf721a46538c970d2b7c86df6f8f1e848f669428f1054c32d71f82a4b77c570c305a57d4c93158c261e424d3f988655e90ff5a08489d443a18d02398a77a56d2230367c46abc7f8d0cb1003a83e16a437f2f5d2f26dcc6860eea3304141a0f580d1d2aaa12d92d95b787dc54ac08e30a63cd08dead4c8204769786fb0de34fb6c9753b66d6c2a14d475a4b646dc8c6f6d6d4a789bc655687705d34a0c36fa768734cc5a55619a25309818bb75c0b0a617ec1f06e02ae9d4a8e5b85b63575de19eda65638746b6c9468d62da53d3cd09d3167e87926e144c3f065fc3d8a12931bda73e46f7e0b15f08d39fbacd0315c40c7410a665121b4c9b2a8436439a74465c8766876d64c73058ea02658850825d052180a2c77840a201d3aa8a2e5c1746184282b3463ba0dd5a519487868ce0ed8fb3afa26a9fe3080bb49962901ace7dd039c568b01be4e3df9269cc7e6d3c1cec3e47388ae1e76b34e0b2a2dd2bf30f766fdfec3f888af09da84acf621a683157ebab71eb885c90030b470e5e705050da0c3264ed7beb64d9f56fd663cd0e28537465cac1d6fcd379b4f1f4a30f37a1fabd5ba85b766cb405bba5bef760e1aeec91f8836173ca92cfe82342d10ccd4cd497ddc3f948df5d9f057488d1a2c704a6752287a92f1651a7f5b42ca3e55ddceee455bfa1b6740dd49a2b4332a1c38d2143ed9a21621aa9d30f10ab2e8e342cac948e6953bd349ddf08404c1b18e209f0ff7866089b3e4e8e7fb7b96052fbafc642d59b6a4d84749dcc2c745e93198c18bf09d186bd5fbce3fbb8170b783c6c1ee9229e3b32bd481f0857d224e3f9899e6c8e9753e5467a4d4e435c4f55ca71b4253a4e627bb0326fb7149a1e347148175d061a9467652257c35ca4b477ac8c752bb7a5c43ece6e2ab1ebe0a87e1556e328f3de924e3708fae8bb8c8f68dc97d59381a01729acf0aeaf6edf9710a8de8d0572946667bf6c90bbd1fc62b2ea7aba1190cdd7a734e1a6e9638eb006bb86da80298ab80c0e6c50b328c7a5613533cfc5e292ab9676549fe5315c7845756a64071da5e13df0d2d9f8e04de3eb9fce531dccf56410c2650a1b741bea0380dd7e9e27b071fdc1e0a726f3c4344e9dbecd197b2f05801c6a9f0290c60c775380562a87816ed80df4b24c2dc469b652d4e2da032bddc4d5670277bd8eca6871152fe3e4c10c6e0c35ec7a438476fe37a6eafdf34b6cba351ede6df48c69554b3937dc5fd37a1d51cfd28e8a79aee2c78278a85353e31dd230a64d35dd8cb0be60018847db82130606e17ee8e6ea83808d1a51738438d4330c921ab2e0f086346bd11e4a362778e33654f474a3007ccf3751701d9ad06cefebe649933de2df7771bbb2c061da4007615a26b1c1b4a9c2bdc334b243a3611aa9d13dc0349302858d8f5528c1ac228050cc941d69cb44d93a00f24c59aee7c3dc3b31c90d37b153724fec9a44e4db9091418bb2c02a024bd0ea6caeb28abd33b6a69e8c66654d5adb17f34a933b2eea7e68aff841859508a5e5c634aa62bbf406754823aa9010060d9434042815c2f06bc258706c0611169052712524473d1c51b46d3a582ae5849a50298710c8544ac4b315238053487ab5a0e9dfb4f05411400015ca8e0452650b01983665d9a47703c1d524370c545a1e7e9035c9c7bb25e3c196d5dba2591f1a2f7e22683560b6838a5dad2a3b3af4a1b245dbc6b2ab164ac23489da9387785a53cb4e15b45f98e59354dcd20f302e9ee40adcac643830ea9b855176d5ce70d3ba2c7cbf468c0c35eafba21dceb6f0fcdccdae652a2309adcd06703545094de1688ab298bf9bc9b6fe8f244de39cf6c1c2028a4403d941a91a1bb40d6303554d1a73e96d5002a6293ded2c8e92da251b0319153ef464a683247b3c1aea9bca49c4356b2c738853cafc0d23d48f6a1b417d18a42671c3e24093b7a69900845dce5e83c198971e063415f90010e605e4d99269e06b7d3a0fd08d6254f7fbc008d9210c84829fce6b34eadb9eb1df373006c900693dd9603970e49a9cac6be5274c8d695cc7d05631c8c93d4ead41da3436b61b54b0473528c8c97483a11ba8caeeb069a09737ea76a15ccee0c054ab04ed024f8cccd32dd559f5cbe78234ff6c1d251bb5130c1707835ff125c7cac70db70f0af3c097ac0a0e95865b2b340c527ce35169a5826900259b0d46df67b7498ac7685d7cbe186dda67c6685570248cf2429b00a3bc54f60ea3a8e5988ec809af9a2882baaa2c5761e126778c0026c0de102bba0930483b87379355e9f95ac9ba79662349cb8d642339894d00534e24f3b7904d5cadb7e4afcbfe6cf732da6a30aaa6d1055ce58b8f05584d5b01d856c5a22d76a1e6155655233e0c6c5af24051513502f26fcd688b79da8c66f967b3038b21562feb253afb15bea16a1757601efb57b88e8db6316ba3e83df02ddeaeb32f840bf3b0f17c95949909ee7a3208e812850dc40dd54de6e7e2da351a36715ac134a7a69b112a51f133f46423a072bf0366e0fa33219af7344c06e3dfd39e2da8bb8e5abff5a50deb37687134fcfa8d69de5cd66fb2c4c65dbfc922c1d44fa94642673d78ba649f4d0368988dab789b15d4ea13cbb4dc1565b689d2342b2b0ebf922e9dad73aadde2e56199efe4e70094ed755c7617c497d92e2d797b57ffafd21c4a769667d9e430e95ffb769d2894aca1d2c68af887d412675f9eed956f03effe66384a2e447bbb5a57d5f704640f16c54afe4d9e10281a74c91742f16dc39296753a2b59269ab8dda80aea8168e02ee7d532f0be264d8ef9d628995b355acec80ba912cacc8c693197021b468998ba1ca945d2bdf53a5e558d520f48287d9471ec30b6e4634ed65da50185725062541f9878d0106b39a8b7816b17bf31fea2318250b84b14630347acfaa4d036a0e500e2135933a6b30c8e791d6dcb8467e0168ec41b2a64c1572963a890812f77ad5862c87dc570527699fb8ae76498f694253135d07bea4aa6f5dd7f031bf0cd94c40f2c85e36ce68ae658fbf7c074201eb8980c1f709501c1b7b9f280686473e06ee4d81c50db35b73ea532f2aed70506d6c2a6bac453f88ee1c61c2328e76db12082afb08bab98a0809226dee26218122db09b819df836d959b682c02f94403617a17c50f1ccaa05749e3b6f870dfb7378c09069bc748e465c6a312b5c0369b716ef3a2dae19a4759c05f376250d3367160fe29a9d171d42acea2512dba056e1b288ade8d512b16103895ebb2ed468c2aa5ebd5606d6c6fba828010c386946cfcb495a5a9623694cdf86b1c754532d5347f56258a929457993246032d83c01db3146492bf803d2b4d0a8b350c56d0bfe5c56295b3d9949045a6a35b6a1dd18a3b8f5758118366ad517c267bb9c6e338a5b020620c35448b881c4c1410dd7a284b659a1ce9266b7cc16fcfe994acc6a1253ef95946a2cebb6f4ac6a1819c15cdd385ba126b1ea35ce46f8c8751acb20ef39f2a92101b99a48d4bd3650427205774e35823555010816d8f5f516ab6083ba2cca80385545d57d545040e253ec0f6b04a8623e92e000fdd5b9b4ed00c9d358c185231d08927c1d238956752c6216309252b342463150db55f6b847b710c75533b5bc99d3155b810ba40ea2e0390c2772a19ed126346583c41fd48e832d0b07e9c09c86d386a23ea56f2c1ce086da09914f0a17d56fc66d0f03a1717f414faf96bb78046adec930d4a494b85ac341c5ad473e8ace4504188c0791f514e056df66a89a534d3e666b83a0360f7c33139dfc356271ad6da4b9965b3c755705746eb78102b9e5201106dfd4906b18d9684b2986cfee22aa7cf3d2db48691281898151d8fccd12a3c88df58d277a298db969556ea0d0745d4ba83617dccd1a9d68f5fc27142997191e27539ec4a6d3601afa9052e52b30f9dbba9111ce54e0b6e82c52dc6306ad69ab2ea8859866d7ce902e1d90b44d8275aedfc814eb4c97c12b721ac92293aa4b3504062f57357f75cfe03d98127cab27774d8aef50fe83263fb79dce9c4c2e265f346878ad134d0bd6d126d5346b8455171cb5a6d826b9f4e0b60193ac182f72f51b2d8c0cc0a75ac3881b7ca63581b0b96cb92839abf3eb1afa0d66d8d54ad756b2604e5d7bfd8511acd199b049ed6aeab8f1bccf5fb6d3b80ea664a18060adf28b72bdc666186577078c97bbad6a184daeca8495804071c92db97e1ad35b321dd45c16c7f1048486bd2aee27381312710916959d3461cf457013a24dccdfa7129836cf9fdc3155a63f5150cd0300939c5409fa707277954da3108474e492860e49044a09217c7543bab8c184a44c6a06880997008deb9631051ad331c3530c1c5f4060988723ce82d3a6d552cb109f8d0bea362a1f979d04acab512133ac696b46049303ca307c55d9a294830dc817e5397c81045183a311cc626410159ced48d92d29df91a798a42c45a879c5533652421d838cf40978947d53a6e0f1949932e7ce50b233e481312c12000a9c072f139a960808412293c50c66d034f94b907284b29d18bb28e43b092b3f214b89d5fa2d8c086d16ad1089557f6d16ad6e129dc1bb00398f8369e3db90f941bd29adcefd60d7659b1ac6c22a8f15397f8049a8a69403ea3e6b920e8412ab26d9c0604b1545d87be82a0d22403e7f91451f229fe952f3fe5b7729461fce9ed500fc063cb8a0daa7e066414151dab59d13e2b47b094a88a92ef06a3b115c3aa609041f255cdb3bd3d4612faf69a60a386035426e75417cf7ea48c9412455b3328fc0a62fc12e024261933537ff8c5196c1cb77ba38cb4c57f5f10ed09c0121ea0334b84e9c8658bdcad9d326c62f30bf21a3fc0addd7c67bb0af65b4c16c88100b08d926a62cd77164545956b040580c8d649171642790a9e9c8dd2622aaa1cba683766f994e73bc0e47d9d44f3aaa709caa990208c8c9f40b9c1a10acc24f3a348227e5d5bd22ebbebd38be5edec59ba8f9e1c53129b28cb7e52e5a5757b18bf603b1d6db24bd2d7acae69783eb6db4a41bc0ff7c7d78f075b34e8b97877765b9fdf5f8b8a85817479b6499674576531e2db3cd71b4ca8e9ffdf8e3df8f4f4e8e37358fe32527f017426bbb9ac8d0248012be92aa494bdf247951d21b045fa282a8e16cb5918a09512d79e175c26e2b5307ae94f5497fa6516d5a5afaefe6b65d761f1d5dc7f9439c1f5da4377944a6c9ddb2dce5f111df9c235d15bdb4df1001d0d3d34a163183103d396170bd8cd651de061b9502f39e65ebdd263585eb3571a2ff8678d5bf5b70cbe34db2232a5c1129891c856f20d7b36cb35dc75f05b6e75fc95f499cb691731d75d47301aa5675e97dfc10aff99ef48c16cd57a82f2a866c67145cd92236acff88e37b047bb118a889634115786d51f6afb27457f828ab6762d1ff7fcb9254065ec76ad17fb791eaef5999dc7cd3f1654bd870a6ed39a3764ad7e0a64060259d45c59d8f7e28fd2bd225cbd17499a5f137beb794d3a2f9dd4678afb3545249c5abfde02c31b0baabf836a16b253a4bcb5890bfe28d661df4a84c1ec814207396bfe2390bd151c4f902f88ce75d4ddc1fa3bc4ce58948fc86e72ae4dc64991ad271aa790a1966599e86e4b3154f78fc5cd3303245992c7d0651c7a48ffe6b3100de25eb75c1f7a763b8683e5a0da8385a29f9351f6df811b796d02a39769f6d78d2c34e6dbf9902367cafef32754b9b8f36fc7e4bd4ecea6f36dc2e52e280132ff275b4a9a241c27cc55236357cd895b799b106b1949b857d712c38c5a2637e2c79e6c2824974fa514b82e6013118dc6cc8a5011868de75a18063a677f6d99879e00a422880b7b4a71bd999697fb35d928458dc301da1fa1359029f6d676d29faa05887a6985cd74423c3701f39c450083408fce01f020ed3aba88f7339b8aa54d714ed34a5e6825054b50ed268aafb3e0fab10c602569dfa182defc5b6711f6603ceda3403b115879c56a5f427ae53aa99917a49d8528a7ae2bf4c31f5a9d67c6e8bbd66a57815a5f7e012b2fe603779caccfa5ff19c7a49ab3430bb816288f4196e88e82a420f0f3d13e53a1e5ec23bacde430e09da137940b4bfe239d11511b04802574493428d0fa86183b58b82fefbc3cd7fb1701c942182feab8b03a1ce1785d6534525e8bbfdcd624faa8a242842b9fdd1c2881284dc1241414e87f86de0f101ef696db25572d31c572e4edc36b664b55d325c6db6b74a523bdf29be7d8bba80cd2ec743b4dee9793625026fb673953c9bb9609f0d20d8676308f6f9cc05fb7c00c13e1f43b03fcd5cb03f0d20d89fc610eccf3317eccf0308f6673fc1faba53a1377773b2b877bffdd1ba341f6e58764339383393ddbb449155c8417084d77722b53fe268ebb1e7210aae66f79dc8ce108cd2417c1dc7b94ad06bf9c804281e69ed2806adf659394ab9809fd68d83ecab8872167942df676f2aba0c0e014c45c76bae466200d95ddf27a1444759cd55728ee6558e873d927d0db52be7b61f376f8b78519c92a549f2652d2c5fd8df2d8e38e47bc39acbc2aa1b5937d16e5dd201207653f834c2c190e21e6946a38fb8d88a6e0c542c2c5698d0192be5b1d01db4aa950ede23a5ecfa4f7bbe5465076d350ff99ceb57f47b3cfae7a395cd26d30bd2ac0c238b275de07451edbbf86a03c1e4491f287d74cb6a4f9520f93c6905a5957789c1ef33eac3c8e14913284db4cb220f5554e44fbaf0d745bd83ea3930304c1eb73e1c57d25066cee1c61c5f93cbce068289523f2d89a41cf6c3f8fb9315890e3c60018b757990ddd8bd44379ccb77187cebd3bc23b06d6230d28ec4eba458d6fb005ebb121d1b9b9d89faf896dfa869f82cda8f564fe8c8024fc5aefa64f73abc3aea83d9b51ff7f04997904472d0e37e39fda4fb532e0cafb1e6827a5b477c09d1fe38b6adae2abe5e67e2438afee7d958687566b1414f58849c64ce20343352eaa82862e9fd60f7235ed7f4bf3c93fa179b7dfbf334fa42c6bfb86ddffd6cc3eb73bace96f732b3fef729ce58fb8d0c71bcb31f66332ae4c486038d063125a2cb4030f350eab7279594cc7fb209a153132af84d608da9c515b9b4bfcd06715036bb6130674efd87001d86c9f056a56e85f464b9fb15cf098ebca38fb7a37a9db98c0b217642fbdbccd03602cc3c01660b2d2740c0cb30d2bbcc6d0946699a2a290f8b158eec4b500e0bb543a104e1972f39c0a8fed986d11be21cae62201a5ac58ffb6ad5be6a407c926e6fd7ad643eda46a3fa982762e0b08a27f3ed291095b8e1304c20aaa95c3720cbe150ce9b589393f76666a2c4404d20bf52ed7eb6d804ed73612af8b59f865c3ecc0633754ed0b17003a55175c20ecc6844fc809cacae8c49a6c8d601bb28aee268252f80db5f2d160e55006169e9d0fd6af3cabf4eac2cc759e87f9fcd6800528c0c3310cc1561ce81114c546af18caca7e609729bea7e3b05fe371177d54fb384dca0ab1273459690b35da1cc0f1e555c4a690e687fc4f3695330400794e2371bf0de1251433cf92f365b91f1f25e9e66989f07de3680d711cde56a175057f0b98a52d87754ea3d49c5653269c1a2fad966f970197d05f9d09f9dd75afb6ac8f4f9034358304d0d08d3a5a51eda425489440536ed6f53db9926338c9269ffc9c2d2e431b102e28147fba355b89e5ce2d3fd68f174224993e24e64d4ff3a4c0021251a76d4b9fa70f34a8a5ccb7fc173ec538e8a2ae4bf58cc3e7483eb53b2892fd2eb7899a5627c61e8fbbc6cd1a00ebca606ac2db275d9c305e60a65d5023e4405906b8f59cb875bb03772fa5794afdcbc1146ad3517bf5dcd8a87cbb6a62a9144cdd03545852a5e5fcd551fb54fcd557585ace66abc48e6be7bfd1406dec8ef290c3cccee7b0f03afd4c19fbb48f4d5ea9f86f1afa6f72e4688bf8daccdceebd073529f5d2ac3cfbb079e1f32a4331b735c0eee2c7db5d9f3b7f5c5a6c42a35a983c29356e00a4898765847d1d6019e487967eb5d51c679b551e0126dc2e2d5a6b926843e515c86dd22699a20ddb5ec7fb6590aac5679757bc945a4bf6545d93070720e7fabc2330897a829b745fdc5c639f898e50a5ef597c06ec0ddae7c9dfd056c0ff35fecb6c2f72d83574517839646f8f4a89ca376a48d62affc6d95bd9d72b4308a7988f82069d57b712e623e58f3ebec16c0b2fb36e541d43e8ee7101bbfef934d523720a6539ba021f9ab45ebeeb2dab45ec55121bd1593bfe2397fdeae482f695777427bf92fb3b140aa6cd241a28df059a15d428d983828b710c46b3d5623aebd67c9b1e87ec4f339df44899853b7fec99207f1896e927c231ec988df2c36a3a3a2f82bcb57bf55b752b90d69ee8bcd25a8e52ea77a2ba3cd56bc09c57db268e55d46cc7a75ca233492fde0c44f2151b884c5d6fc5fd99b6849500c3ed592bf5a58c36c794fec529356fa73b914cca1fcd98137d066f19bed0b873704a2f10adc51903e5bd8d9024ae6ddff3a1bfb2a58b0b375940cb6bb00d5e56f74156c86b1bcb4429143fb9bcdba9634597e35c0fc6cc9eb1f72b077f6f7d9a08d8aea7d769ba42341acaacb1f620a366a73754b5f69640fc94a9c18844f56cf112b9a77e2b922f7612820cfc3385d65ebe1b22dca55f9e306e632ac6da1754a2bbb4cf5627522c536579106bd60a6ad03a14b03bdfa70104e40abcf38abe2d65e04535d10b3dda1d8cafbe45bcb7dd4fa5cc4711fb521f6bfe4d9300a70cdb3e334c045cf7dbe76d542cc61907a04c6e66bf5098dad6fbf5a69ce432ef87de86083653637a267132c909e329a0062441a864958944d680a9ac722834ed9f5352c636d08d5a039a9b78b2a32f9d15cf7f314372be99677f318118acf007c1e73b37d2a74aeb32fd17a8487d252452e5ea59987d21c077ce61aee49e8f98a0a47e4d4ff6a7726239e4aa8cf23d4304e6f778098d8dfe7fc1c78ea48519bec8c4c3f831af8ae164ff36ee4a337c867c0f4cb7f99c2c443adb26f4f65aa1527b2e2b7d96070a4fd1dffbd1dbb7d1dbf1d67f91461921384f66377fb492cd2d5defcd2fd5db43f508112b35945212e7abaebe55dbc89aade16db68d9c0fd4d921725d5e197a888eb228707ed5e2791c1f6b6f873ddff7219a5c94d5c949fb2fb387d79f8f7a39f8f9e1d1e9cae93a8a0d3dafae6f0e0eb669d16bf2e7745996da234cdeaf70d2f0fefca72fbebf17151d5511c6d92659e15d94d79b4cc36c7d12a3b7ef6e3c9f3e39393e378b53916c91bb6282e3ffebde55214ab358b1f06e39c3d81ecda8b77b1b8ae7ad142e62abe91ecd1b1a06391fc85d292d1e6bc3cdced12f26fb1dfbf5e104fe1ebcbc3ff5551fd7a70f11f8b96f087830f39d1c8af073f1efcefc383df77eb353d997b797813ad0b09b770ed35c2ebfa1fa27c7917e5870764d9f93e4e6fcbbb9787bffcc4322643decc378f37c96ed31c3fb6bccb8460ae3ef4b563d7bfba5930ebfb9787495afe64dd678619fb9a270cc73fe2f81ee4fa376bae94d5ab2cdd158bfe61b8468e963c7fcfcae4e65b68aeb4a5677d14eaaae7cfacf9b111a93c04c805a3f2e0235fa9f21598bcbaf3e578b6cb733205b14fbe2ced89c44165583003b69a4e3e467999cab6cd8e13f3eec2a1531cb54f8784b82e96cde0a87d9a21bd70f380b5f4ba2d08afee5d5b106ecc7bb620fc9ab76c4178d50fd982b012dfae05612a3e574333651d5784d7c43d0502efeadbf850dab75058774a60025b1f9438dbdc8a1edec1b41e1ef0068ced0ba2191207aff6d4535faf1ee698d6b25d4a4e78038b467a288c1b5fe499116e506898910dc8d5b3bb957fe8d9df8e07dbe16adfe06d4c1442e3a37c8cca32ce53baea8febd5dcbe0f980016a892dbc768793f0c586a3bd5c730705f46f73c5c90c2537b18fd692db6ecc3bb2da26a7ffe2a4aefbdf85033ebcd840d7031941ff286aeb9c18c3238f0a91e209b8107af0bf60c7454780eb5b7845eb5376f810698c79acb298d8c947906710881734898f1d1d279a0a3cb4fc70e42cc02b8a65bf0a9debc9425668db39c1d0d2d3a71dec59c6ae86cb25572d39c542c4e166575a1dcc3560afc1eea3be535c39b7516392c0a3996cf0237f159f8263e0fdcc4e7e19bf853e026fe14be893f076ee2cf0e4db4b6d49fa2fc3666ecf5939d7eb2d361ecb4882c8776c82c2c5a643d147cdd15654e5ba0ac3e152cc20586701f10b096ac2e8a5362ab1242d0b2f992656bfbb58ff761e3ebf826daadcb2aebb85f9754cb42948f6e3ad3c1e9272b68fe55efed888acf45c19fcc21f4831e40a66cd3b8e1a349296a1e0f1cf190db55be130e90a4da0b2460d26bc7a947d73697f987b7fa6851d564339a9af50d324b063d9074a9ad7183481d83d43c86026d844999a87dcc399f863a04a73603f550fbb2c67ccbd35843b7d3b83633b3a5dfd6d0798d594797d1da4d54f4baceff3c1448f4f99091873d701264c4094f4be8810cfadfee164dfcb5b4bd58c1644df6f017d974c91e6ca65d2771a9956d2f11f5b443ac8c3eea731623ef19e85214236e18f0e43e3bfe7d6e635b3db7947b6ad1da34cae1cd9929c1f0e3becedb67acb5acbd25f4aa3ddc35d5f635dd50f87047862a27b01919b06e6c876c9fd7d763aeeb73fa7a32e112f9fa2a5d4ae2ebb3ff2324ef459f39436bb5092f235bcc8cfa7cb0d82370c5db46cc197847eab37dcee78cb5b4621c75807711e398710f1dcb795bf74bcf3e062890d1e9f2bb7af8ebfd3b5e4bb4b4845e586593c15ad6df930e8157538a551c56b51955cd78d55cdcb79473976dd352c80dddded823086155b6c4f01bf5a68ca8160871c6c65e5fdbeac2a0dbce94359dcf53143167a287b7c647c2f63941ec93b1fa4e0ce116334c34200f1931b180bc243dd048562608453a2470387f843b020d00dbbdc3e6b9bfe518aac96632848464a33ea7b36da2515fdc7799467d19f599460334c9ea9e2980152ec7a807233eb5a825f458621f004201f1c2efb9281376eec5bdec81a6d83d702e1cc1a987a5d3bd1b7b4f804bffe9314ce5ac9fdeccc0b710aecc029d913fbd2b7e7a578c65eafcae18605a27b8f4180583bd1c41e57bb47e05e9f900d2f7fa84c7d3512d3731e4873d33f7080f86a80ade1890532ce2b40e275634ab1b9a7567ebb2a04569cc6e88132a9cd3d02c54688967bb2eea7394d95e8a68297d6f0131090cddf7cdf9e4853ea697cb47e8bb34db87203b42ea41db4b682cb56f109841e63c6d5a3fe4354155263fc42541788059db3e26f95fcd274c483331056048dec13640f76114f96fc2c8b9febccc9898decf83199fd52ffc08d526c0433e8e73189a9e63b24b95e7b1546812e5a947ddb39f7fb11d7662be3c8f23603e4d9efbec2ce4c67367c4a5c40bc2268ca0e49c771ecc802c771aeb84e9af98decea37140423b1fcb5228e2749a3c8f96b0faf987037a4539f973473e7c22e2a0ce877a0c05b45440d6b831ccd5306faeda2c51960e604de67737bb4f59e73ea6d95475582eae7a0752b9212f5aea33b7c1f7bad599d980f270ae2fc4454ebe65ea19e9e499fd34c775222cebb150eb0a1539ca370e29aa9c6d405138319b59e5d30df8b6c5b617bb32cbd70168a569d2b4210dba32311bc2b02ba2058636f06c5620b61e84e47b52cf8b3d5bcfed592e635b083ebe574f46396bc66423c3e1549d19ca8c53157ef6e6e690e5f34143e2279cc095b99eccf266483d96a9839db6a3161e722ea9a9b76ef073823ec9d29edc499ff616779fc1c9b2f696d07747d96ac30a025b9fefc96bb699e46180a5bdfba8cb8464f3c410ce34847d6678e67faa05db3cdb55a97f3bc4244903a94f9b5468b6fba74efb3eb47f13ecfb9892744ee77c0de5bce3c31235ab970f37cdf06d1fcb4f1de36e8847dfce5269e2a13c89a4174913b8e649245424b40e1a4eed491c9d38aa00314ff26046ccbba4fcaee5d1c761d9f71892df67ec54bc435b4d0d4f5a7edc5aae7da2272d3f6e2dd398824f3a7edc3aae02013fe9f851eb98389f4f2a7edc2aae038a3e29f93129f9b428b2655255db6d4acb59ffd8ec6e82d2cfd355b5ebaa4817d8749466473f820b5ceed6654237a849eb5e1e9e8829ef5f7c485fc7ebb88c0f4e97752ef5b3a858462b59e2a4872b44db3af02a9ad67fe75bf64f528504a831cd369644f43e2d3d8227fa94519da4cb641bad75521288c0c160cca24845d0d5267e791d6fe9815d5a6a0412ac155d6582824c027b71ccc0518f5275165616abe05efc7e20569366966da1aed863c62f360beffec25891e9150f695743f7e3d1d1898495bd02a6ab4553a46b1d13989a4cc11302b4061d6b59df2785da1360890050b41fbe6b3bca8902830c5578e631d1a96bc368601473ee2ed4a29c3526a5dcc172abe4228f0e8bfa0cca73c66183be26e5b0366bcc00aa47cdd296700f0a2727c5ea72613b80cba266418d13638b4debbcd0e4789eb37de39a2db788fffce8ec9a5a698abafb64d473c1dd9eceab4fb8b3c2ddc473a99cb670a1d9626616ba50be506ea50b16180587402a46b665d0e76196c4c69caa0a4c2837e91db6e9ad306c4a62a968017c85c9b9b923c09e77172bc86b97d4827bc9c249fc34ce867a88e11708e63ad75b8101bd9b87df45771d6093bb9980d9ad07bdd1ee4a87809cd6e5af221cd56b96d9984dfd39a742a5fb6633edda0b66b89d04b9ed9813d39263f13b63bf41d535b07d72a1271f422fc5295a3de58068b39c1a46c49c5670731c01d3ace6bc503cf19a4e8948f107d36166c3a727d05a6ca8d8778b5a8d507c5654632158249afa845ece275ee7d74622380832863d210532a60307a450a941f1eb849e00e845e6915734a0cbd93e33cc3e22a33b6fd84e6776fd803ba9ad550baf026ee59b23d13be7e55db091f5dd2ff11c47d91e2ef2b82b0717e903e14a9a64dc369ee3da6e6ef09f767de708e159dc80e97048da7793ace345f702db60a5d54141b80d5f7529fca632dc52602cc84506411b321e8a42edc1fc03a9b7284bd83db09f0c77756b7b37d6eeb078f63eadd83d1ea7f2d7c7eac34a3d9dbdef2a23b3f905757f26807d7ad490f4305a44553190826c043cb654339ba4f7f45ad77ca6f029af77390d81899dc5f77174bb8beb90ca8d6dac7f3a4f75106ccb4810e43f8c0241b90b408bf8cf83408feb3a46f5358127f480eef9d63df692d9d485999a3e6fdc81cf04f6c8ea39426feae5714ee3bac64c70e8d751192daee2659c3cec1500e19e083e205ce431cdbd701731f5b73a9f1b16ebf8dd4f48fc7e90d8877a9f090e17ca1e0ca1f6d1606968db507074078632098127326d2aa78d9d1334f76f913c171ccec42cee839bd86470f9f75ddcbab8fb873bb90f5c8ba0cf8f087740f7f60077553aee06704c2c884209388680d32ef7fb2870636ae45aae68155f6610e0b132c04d78b4a827ee545df46dc0c8e0db3b6be73010f6dabed9827b0e868d266359d46dd6de8de90acb9aac7f7de488ea7b8ad12c2d383298d4558e05a566f3180b26c04671bf7f0f80b2b05055f99131a5a97304509dad7745d93fc95afc9ead622dac04024ea1d2b751e0d5d44a5bae6b15ff7d10b889fdc7a8bfa5f1031dd447efdac7831fdbee459b45d47485cac1c0194254cd074953182e570c553903e369cd183b37feae4a791754d10628398033ac11b256e3ef609e3ffba9cf02b5ea2a47860cf5ebf6c999c2af11f6d81ed9ad06e6637ffe48d234ce69c30d56c85295b3b538d32cdb2cd0d86b64161bebed1a4ed58b417d1cb921caddf1e14133b245013a377fd30221a75a5daa37c7bd75fcc84f661c7150959fe6784653f53410dcbb7df2b9d8bde94f0503d53df67d5966c81860e76d66aa297410143a8d8801ac9eade9097244680d7d63d56363b011a1728bd76fd37bc601d4273952b6c60bd3cae922466ca9ecab5f3e1771be385b47c946edaa09c5f9c79fe2b7711ec5f3b556edd735ab2930ccbb4f41021810f8be82877a87a997969e0deade67b749bac7a8abdaaf6b5653e091a2aeeedddea10eb528f0c25ee0d931dc5ba4093013742531015a682ff6d844d11f75adaabf3f520355756efef6a9091bf196fc75d99d0a2e2ea3ad0677200d1894432c320a0aab4aa32dd02ce1cba0813884ae6390d092780291efa57fc5a32d2169339af5e3e07b68436f60487d015a069479441b6aaa3eeec13cfc769d7d215c98a745e7aba4cc8603a2da69939ac21b34f9eb234290dc3b4cd5b5aa66849dbd7bb5fb84395bcc4dfc4c97713d692f16d4a544ad1b741e3ae49d7f4feb06b4033fd1ba81169c0c71f4a688b066a0faead92edee4d966a1585e7cca16328331d61bea7916680f5b01f479b08d92c9571026e538d53eda32420868aa052610b59a801319ef73d860956aac068ec41a1eb9d305a11c2a52eb68e0fd238eb6ddf2d71fb935bb79c2b6699b0eb36d91ef07b090c2e68bd6d37c93edc2d9d99add3cd1dab44d87d6b6c8f783564861f3452b6dc2f57d82b1ae72162c16ad2d23235203a510c2a0b46b138cd0fef3d0e89c2af98f493df3c6259398ca139815a7b921b36e941a9acdf7ef039b8086e60bce7749196c7e27bce639b9d386e966f6eafbf733ad4b7a9a233cfbc4161cea342015e109723002d4334b95e66d35d81ceed12d5c6268604e9eab09ff40dbacd1919a3cc6ae7fe57c3ba35f269f0efa405b38f6c0e727d0976ae13d52c4d79b23ce8897c9a7433cd016963df4f909f1a55a788f14f1a79b4de66ee105e209edbbd8128eb9f4f109e9a54a708f14e7ed968d13ce2bc259e05c6a09f7aa52faf884f35225b8478a73b2b27636e702ed7428171bc2f296be3d61bc5488ed91429c554cb5d3e9e6bb5494b380bbdc148ebbfcf509f2a552748f02f4f58638a12909459cb70d21cd7e93e44549f3627c890a19b294ea3a2e8177f38707f5ff2a2ffe5e2fefe24df4f2705bfd4a63a7475fe47bc31286a11a99e0e35d1794f5c385f5ad61688c4d423646df8c888f2a5fb70526c137a71fcfbad6f4a5b08de92950daba8ab7bbb242a001297241bd96e4f2a8f6bcc91332980c6de10ba9dac19732694673f0232b485358d51af110a43ef044b64a3e2955b6492e6a6e514d63d522a48460d9146596c73d78c1b2867674d3bf6e584385146d686712fc587e43dbac1b376201b866b114cec6d2e69e96253106b169b08065cd98a8feb55ec7abe6ec1cd52e39dbaea2517241bd35e113671b1b23a7a096db21975136412e6a68009484536a0154a86dc2bafad6b5002a8a6a82a17254b568a1cbe9a400a9cb853ab1efbe10a79691ba5cd4ba0975d6374433ea82e8a6d4c50dcd81e25c494d810a299a0115b56882120b5021441390b8e022654a75735fdb4a37f4c7b81f805c214c754a69735fb5d52105ac4c2ca4ab9a2fa96c065016d39a3a742d5c7ffd4ddbf1ba88a122303eb75425584a812ca62c52d352b20355fdd8badb725897510c82217b416289b6fe076676a95fd35956d6c47c32d5d8145355db84c6b2acbb89fc63aabb29a6aabb0990645977fd36cf54755d4a55731df6c454b110cc41aa52f8de56764b7ede64ab7e0e15ca21dd6bf13198d2c1160b2adba12240f97540280185630794540c3aa0a44925f2d360592d721955fd7249942c8837b8c9ce74ba5194337ab875719b21611e0eeaa1801b06d0cb44a93ea89012885061d3624f583b336fcde4359fba2c7669ce905836ac7d50646c555b10dba4b6bc657bda2723c6f6b405b1ed69cba3dbd33f10d0b4a52f84589f7665addad0ac660d8d684ae15a815b2083777e8d7aa94a6195521536394df01d4fd97582cbc17b188ac2263b065cbb934d195048b185039434b400ba0625b5002a04b7002a69928174394596805444d17fa99c696d285d189057865211b86eb99ca16ee91c57aa5a2a01d72c1533095c3e4f93252e9731ec1a2aab674e78f487120bee548321d39c507034e2b11473c0a83903218dc21d65a8388a6214192a2573cc8b062136f589112b88d6a5904568453fa638cde766155f5db12185fb3e2a4a40a34e82d6f31a14713315795d1b8bc02a0f9652ac8af2a6aef2be3bd339d8059fa9b0c443453e829f52667ab2e145a73a646578198f4b3df175b6cbe95d04f1b4c98032986a84eefaabc15960ecb1f1823f4356494b4d323cb6a00373868ff6103c88887043504df2d844241f3a2f78175116918944332f6a8ee3eb891171b8ce71541fac57fca0cfde22e3ed4bd577d524a82aaaee107c4e5f75467ff61e5ed061d054df5fb383134fa3e9a66a7954f7d1b4de991c49ca380646a12129c71c8aa6db3f1c57b9d070c26cef70384853201dd2f8cf567ce20feab9d396855918eafb6d9c38a0623316b5f2a6d382097ca259aa1a0847ea38b4e8525ff462d75c50a90185aa072d8a6e2aa44e2f564d4c13fa4735dd982d02827a8aa92a98d27cd7bbdd05439d3369a01863729a5e60e255cc8510e0542d3823a5c637349de8d62e22f66016508eea2a2aa326b988bf48c5aba2c6b59f8162323ba9ba425bcbcf78233684e8844bbf18d9f1242360646aa1c10311b727a3271b7e5f669a212adfa55eb4f79c15d23250a8bb09ddc1ae3aa7bb580d70802e92337cb4f7c23de751a806c3440a900c89a54944045e085f5cc5cb3879d00a0a4538e8d0d3de916f0c15eaba7b30b171194cf04253273e79d4225b701560a4c5518cd04d035703c76184669c110d24e3e26b7811c96f498c2232910c2922f5539a8a0fe2658cbd8898071bec717b01c9465956dd25e0a14ad517cdeb13153df4f244e4a57d47e2271c1370946587448c83785dc5406f342f6a96aa9d06b09ca1f1ec3b9ebee9d0f39ca93ade387c88aecb250d1d0054af79053681008437430bfa7049250265597527148f9baa8e48df34c2d03cd46279f1df430987e5ba686cb46e2f0443361c6ea611153b3cd8c76ffa81c4941cb443be02f61408357618bbd2951bd8aae04db267c7ff48d234ce29631c1ea4f203ce2d8323407e64bce02ad0bba600c570a850bfaf96fcd241c553cd6aa067aa293d60b72677de816edbac6f2092b1d63763e188dbb463a48fdce69329064480bfb87d05d4b49173220d120248865b0e8e2514e1a9f5a27d430e1c55c32535275af083fafa184bfca63b5bd5bc9587983505828ba67de26e164d537274d1704ff921664d81e0a231d9626df9e1c534d5606ade649be551171c1d2fec7b738857fd3dd8151921afef65b48585a3296e3e3f875fb77367e7fae7ea1c57306c44c54c1f08c2757692a22820cf7e0d74c31ffe2a434a30cccc2122ecc526857d589caf9232d308cc4031a4a894112f6a4419235884108fe974ce40f1b8c42344d65834c17bb4069b298832b43a230b19d81918ec21f2b503421d3c2dfc0813843aa04ac511111dc575aa183271b9729e192d5b3aa8ba61eec605bebce8acd06192792b553942eef00995c887ee0135a808c2e37cb37e90ecd6ea0bf9c327d39e507d7ca423507d8a9845ce972ecbd0e99ed51735835765565bf8172dea605082ba94a19d7c54153a03b2565983a65b9e445d5cdc2c405f70102c578585ce0aac54d6a0e9872734874c7431d01642a1c21c8ef907ca910b5d0d18231d2fa8b0708fa4b4b1d7ea137954203587656bf05caed09277d884b1032b471d8daee6678e2e67af96f0094701b50c9cd57460b5a843f455fc1021f71c464bd0ac98d048192eede6d0a34411afb0e6668a40e816fe225ce24645ac8c8132430eac0a55f8c6fa44dd1490d15e1521730b028a182c75e1c06a5044b2ac98998253ba9f8c752eb79b0991e3b8a84fd5425735aab9f20b67a351d08be39a5117bca6fbf6e2b80e16dafc40fe2cb33cbaad7674d745f5eb8be3ab1da1dec4f55fafe322b9ed59bc203cd3b80a4cd4336dcb5ca437599ba44e68515ba4fddc5e2b8ccb681595d1695e2637746591674b7aa696de1e1efc235aef4891f3cd977875917ed895db5d49ba1c6fbeacb97d679ae44e57ff8b63a9cd2f3e6ce95f45882e906626a40bf187f4d52e59afba76bf89d685a034150b1a59fd6d4c7eaf75496054c6b7df3a4ebf6729925123be2ee9dfa778b35d1366c587f43a7a885ddaf6b988dfc7b7d1923e727d4856748758c5c4ac085eec2f5e27d16d1e6d8a86474f4ffe24185e6dbefecbff07dbb62573111d0400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607190644444_AutomaticMigration', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7d5b731cb992debb23fc1f187cb2d7b3e4509a199f3321ed0645511aaec4919694ceec5b47a9bb4896d95dd55355cd91ecf02ff3837f92ff8281bae292001297ba34c5d8d83962173201647e48246e99ffeffffcdf17fffa75b33e7888f322c9d2978727473f1e1ec4e9325b25e9edcbc35d79f3cf7f3bfcd77ff9cfffe9c5f96af3f5e01f6db9e7b41ca14c8b97877765b9fdf5f8b858dec59ba838da24cb3c2bb29bf268996d8ea35576fcecc71fff7e7c72721c13168784d7c1c18bab5d5a269bb8fa83fc7996a5cb785beea2f565b68ad745f33bf9725d713df83ddac4c5365ac62f0fdf67f7d1d1759c93361f5da437795494f96e59eef2f8e875544657f1362b9232cbbf1d1e9cae938834f03a5edf1c1e44699a9551499affebe722be2ef32cbdbdde921fa2f5a76fdb9894bb89d645dc74ebd7be38b6873f3ea33d3cee095b56cb5d51661b4b8627cf1b911d8be44e823fec445a897bb35dc75f69b72bc9be3cfcb88ebec5f9f9d76d9c2744fd440a62adbf9ead734ac02ba0a6e3ffa7e7f2c3c1fabec81f9adf7fe8e0435046ffef8783b3dd9a2aee651aefca3c5aff70f071f7659d2cdfc5df3e65f771fa32ddadd76ccb49db3fe619615f7e6b1afe3e7e88d7870775cb2ed2f2f9b3c383df0955f4651d771a3dd6b2607bedc3e78f38be0779fdf29381d78b6346231845d1aa5e65e9aef0d153c7640435fd5b96a464747652a1fffe440c80b5907fcfcae4e65b1056b44d671931442da757df4a13176b4d9d45c5ddab681d61c614a71d86f08783ea336fec0651d36596c6df6c902bb3789da58c720641ff35955e51264b326d945192c6397618d4b3cbd1e97249f57ea462d708bc261a44d0ef92f5baf014741cad3c599c1605e9b827934f71b409d09debbbccb725bf25be1c2e88e7b3219ecfeb6813dd5a8158e6f56157de664ebc2c06c4e9176210a2655923f9a28c3704e2c94db26c3c0597d941cf7384c9a2240db4b0c932838768bdebe7878cd41fd0ae5f13df32a65239cb8ad22c615afa88a319cf9c9f6ed8e9cdcdabb928788bfe2acbd671940e20cfd749b1acdb6b27d3966e44b9e644b0b99f5c4f379bccd7df8cb6748ce379586885fef37d1cddee6222cf0c6b486a8a239e989b4d9b2283a885feb7150759df11db7b7870197d7d1fa7b7e51d716ea2af87076f92aff1aafda511d1e734210b5e6a78f29d596f5fbee48357f286e07915af82b8b9c4d9898be2136353e97ab8d103fbd1c17dfe982783ad74fe7d17efe2ab28bd452f461befae223ceac987f7e62e13ab610830a0a819641c5f46e5f2ee32de7c89f3d3bfa27c8595e5358105f9d8ca546433824c85a5c8840bfbab78bb6bb75e7cf8bca15e15e172b1e24662fb33fd897e0aa5fadf88af71ba5ae54493d60ba5b3352916e797514a7cd76e64411c8747c26f959f35b0c5fd98e5568e92cd10ac864de162caea6117dd933e1fb16cbe2393769e9649f94ddeaaac7eb65ce0345265390c2049f28dfb8191cd557cc3f5820e77418cc722b984d48eb2eee7db5d62321a2a26068789cee6fe032b8f37c96e739e2abc195b135e994c79bbda61afb6d94095d832df2cb9d23d3b8021b707683df7dc2674394eb11dc41d7c4fd646a764be7920e00fc2f06c97e7715aca935b0d4cc39498e445f931cacb5486b4c9be741e891d61e518ba1076fb84808aa12d49931c7f8f1e92db4ab192c77e97c40ff18648f57d42e7beab785d952bee922d3f022a1bb69008dee4d9e62a5b0bd6522cb7b826abd86a646588c29fa2fc362ef1bd1070d11a6c535f14648a1e81a5f5fd82496c7bf7861a9e95413d75a1a6067ede12fba32cdab58cef8cba7cdb796c4f2ed207228e2cff46f7527078034814fa914aea752317b7d54bc781fc7e93ac634387c4e2062d698b2b34a5a7b1d556bd6cafcd17764841340a7dc945f50a03cadb6aec639e3c90b9e7922c2ac85a43adafbad8d95d543645d5bad21795f564286fab236686c1aa08205168482aa957905cdc563ffdead73096fa82288ba72dae184b7a1a5b3d357b1c8c2761561444a3d0945c54af2aa0bcadae3e178a3e9c6eb7ebe600879651aa46574ed289b630a48c17c7fd6a0eb1c6633c10eb0d0c86f648cd6ddad51fd322f785a0c0c4634d18e2b428c8e294e952bb27c638dcc057a725502fb6b428e9daacafa7e9bb83bf6eb4f7100a170003d1a6e0e8140606496c6b6d147274938181194a1e5a1e36b2d133b2f6701a3fb06632dc720e9e330d6b3f27fb1cca32cfc9260376c5d62207375c189303cff78611cfcdf986b2d2c031124003c4055bfd90f18256cb6606c8aaf6153da1d5f1d04fd48cb0aaab126f63a226b2cc597d8cca32ce5342b98a1beb3aed7c1cc2f1a884f2315ade9b0513765a0f3ad2240f1b3b343dbc6c714de573a622f29a6ac4f5ed70196b3cb5f7298b279310a7c6ca5dfbab28bd67c6cbc92f4eceb43f17f0881d73e9d2e074197dd1a09b16b08789dbe8f018c1ec5eb0cfe865f94c3572fb8319db510b1fe94c3362a91c3d99d013a55898682b983798fa7317176553c87990f0c8c19d63f034da730cee0f7870a8cbbbadb2ecfae27a2683e84bb8412edfc7b6f58bf9c1de1d480037bd83baca072dffe6a6808339a8dae4600c5a3a8f015849abe7e17411fd8c0ccf5b2269c06d163f4d61a836ccbdfec5096f6c0c0f0b7c6a7a365a4dcf47abe9a7d16afa39684dead5cf43b44e08e1e96aa539f969abaf0d5cdf8885f80370d460472aaf8d2ce9edcfc589295cafe355258142b79f490b9c9665b4bc8b5bf30ff719535eb18d6720b2ee5d8d9478ad5bda2aa78785442eebd54c256f2ce14987de83f5b9e2a0dd8755df88f0720e44d087720d44be4f8ec1f7e018885ab765a71c86b5ec74a35069d60552c444c251288d8d816c0043a3ac59a445f49127c17752a0b3eda56a7a9fd64540f7dee06278596297055afda2f15554c447109bd6e8d25213dadccef01d9bcb8a56cc7a3767d646f6a268dd63bb67b1c0f6a97ffc92d7f14d4410707d9f887bc41443f467c75e0eb4f3cc3ee7a1fc8407d5ee2b16b59f4eb9579ffb53147e98f636c7505472ee4ce56d1d73c47d69e1ea32e4a12a8a48ad5795f3f2496591d8d9c18a16d0590833e860f6bafa5dec18473cd6816d08e359f118d082725ee5d4e65dbb4cd6fa72012d8be8bd602d5138df1ba851e9759bca62ba63f6b49dbcafaa163717ac363d4a66411c31eae6013b781c6cb06f6c5a51a2a73d777d22263e18004efa14a7411b25b6b4c24439d1f4c17a51b6b3c7301e581f6805f2c2faaf5e5653b965ece1bd8826c5e4e5b85992cd26735fc351ea2391c5686683efb1dcb52a6e8d47e72afa2399cd5c3a7876175112b753d2ba8f1d8b2390d95c7afa2ef1d86820c4470283b9f4ab5db2ba758c521e892ce6d2b53a5e93bbd66afa2399cd5c3ad8efa7557e81cf85a4fe2002641ae61442d165f92805db6d8abad0bdee79ceb0d3c0a164989e038ca7ba91167af9eff624aa5a85f8f10870f45535e37a9df1aee3f99fbb64cb7cf2dbc133dee0824ec2394acc793b43a07a1864a0b27e99ae3f2619e42a01be67a14e419a1a2a0f31a84160380e6a08a15b76185348fca990fdedd8cdb2b3b50f12b2bf2cc75976b95b0df8dfb31658cdb2bb62d48a400e8dc075b2175945113bbdb9ee083d2651fadfc1e3d95d14e7296dd3caf780f2a2f89caeb3e5bd3fa7203753fa4579a82b29d2c0c6078b91488df162040ac5046d2473f13d143718eb1a98e9bff905e892be281056455fdef6641479c13f68481fa47e02bee81185e66d78458693c5bce81be214ed8227f779e3d3709a7c3545574bc2cefe39d3cdf6f3708797f208559d5dea4bcac3445fdc3e44146792863563e6bec066cf69b843a1b9ac42ad37e35de6f39d06366dc2cefb310996ada80eafce8d705653ed67e7115e333b4f55c30208cfc691f4a3425f521a1486e203bddde36a728d67a7bd9daf897fe731befd47f6b463ba1f54b6633ac870ac3350306388fe934d4e3141985ef52d9b0a3bcad3ef602352843172003b3aa5628044741c794202846e9cee757955bddbf3f28ed4e7ed48cd8526aa9ce4a5ba06b65dc3b0e13739126df44da624e05e698b879f49c2c511457425e4c250645e0bde6b1c5216dffb581c7c2b2e882779515cc5d1ca7f13ee3a4e57de4ef655bc8c938780d6a76568356e2bfb2052ea462f408018c31095ad55aaa56edf3b9eced837b638b6671c8d97856ae2d73211879d1237c96ca6b25042a6005b2ba549346039de2a4e73886c436dd0376b13e43e59cb58504ed686a2d2703095b71de2152364579a2a9a8c3eaa5ef0a524875f53d4cbd997f97a0fe36957afdde8711ac0731876554870bb64256fc97fa93e80c731e227878c34fdd573f6d14d9d33acfeeef230f02e5ede07cbec1d26590efb3ab3ea249b95705ae35981c2d2e0b0247aa3d397d45a4ea0b8f5b3a4d60a0e6134316d17eceb2093577b728098bc80a2986e845b69722871cf1639add1ef4ca6f5da12b2b5b6373c8818e662ae2bf268ab62dc7fb1b5d6794cdfa67a9bd8eb32ca43f07993a4497117a64142e0c50a12545ac8708b221a76d4c07cb879c527b877c9f89ca4699c7f8a23e9e4d930f3d074c2541817e975bcccd2956d3b5c77bbd979812dda5b3eb88464f014c56c6d361d981a8bcdd6c216859bdb97d0369729661d6585681a25dbbe20dcd4f6bbb6a15d21db66f6b044ca5626809b2d96d3365f2aec3f09daed62a853267fa7c174434ca561c2590116d36725e014aea049bf2dce2b5c6e6e87a92a64464a91fd9fbbc818b5c26932ad3bed1bbd9809840f2f1fd8545a7c61c1de0065605b0315b45ef018166ece93a6b84364985b832d7458c9a85638aa325a317b6dc861e6235a462763e13b2c61b190d7ee1b6b1338c4face412cb719244271ba7c1f2ed5d9d0194dd8bc1ddeb94ddc266383cf8e1ccfd666131c223afbea3e4ce8c0f31918947eaaa1d0ba24b66320802be3e4960599cdd0cb14dd4c26ad6542ae556d270470f652cd1a4e486f76d5ab2d25cbb0010de9659446b7fdf10cc070aa41d0ee93d90e02687fcd76fbaa9682ff7be85cbce0fb5b5694cdef1eaef7ddae7c9dfd15e614839ec8d0d7801429618e45aaf48865f240a01ae89e3461163b1c73c9eb0b16d7be0b8c1624ba371442996abb47341daa32929d5316744c6d8c69392bb08544277542575c3287189a10d631a8659cda2a32a6c93adc4410ab7619113f2d8de91f9acb71343b97f7ddb8a62ad18e0e54db6047d7f3b7b0b2a114d0ee76b6f13ed9247543633ac83dcf37aeefb27adabb8aa362dbe782ab8f539b29b1d3555438a452f9bc5d917ed2ceeef869bb49f7c17f779e39fa5920f494a130b1eab9c5c9ae0aa9ddb17695966dada9c0622a6bea62463ded67f788a41b0b984c860293f34d94ac3506f1d9cfbf04b088552dc44dbd49f24d8017fe5151fc95e5abdfd80736435dabbe8e97bb9ca0968cd5cd76f0da3ede656446dcd5e741e3d5154c359ffecade44cb32cb038585789f2def89b13e4f57744afa5c2ee5190ac9204873ea97946f0898e355801d376ab00cee0f76fc792c3ea24411b44130ad8bb6289339032c2127c8808bd96eb5bfcf6e1345e47db186b6a8baa9750963539b62c16f6d89f5a80e3474e58c8df73ad8a02c71c26e4aaa1b5d1530b6b62e15d29fa8706619d9a2f22d203e3f1cacef8bfca129b737aec5f06900a884bcb7c9888029fde0935e55d33fa2f52e7455ae18ad0c4c008c567c26c368553bf9f9215951cfe5d84cd11626ec51e55b94d98e04a1654a9d9f3cfb5b9089569c08d86e8e5db9f5c874053135dd01304cd94c066118606051da4e172c06b0946dd5e155dadcd3757a8b539fbe46f704d7471c9fc996e3cd3e85d3dcc9d08e3587b6b7afc35eca266a087a19bb7b14c15eb4aa7f737cab32d88de15672350c758e7813c19227581021f12eadba9832653054364816569eb1c7588519ce25f92a3b286c07f130034a7ea865077fcdf586ad62ade50f4f55ba4a1d949de0492f240483a6cc6cd000b662bb8fb53dad9d93eaad467dcc68d7ddc605ba486ff28854bf5bd2761f29984e3585b607a80e838f219dfa4e363de16ae210f051d2263c33f38d4f2641c4107b545f5e117cd440e46528deaeb32fd1da35ce9344fddd8696091265e57c9510dfd5fe3e8c70b02969057bb8290daff476c789a60e68d6fcececb9ce39088fd220d4bad1180449ea0b9ea43705fa92921130140f1e9946ae4f1594465f12d19170a1686a8b48babac9ceac7d1e2864bfc06adae0aa678eae374f3db50310a01dd56cef7ae9c6654b2dc076daa45b69135c8698fa1895cadf785a4695b2684a82a7657d01dd691953cace7ced362cdee4e72717c59b75745b749db74adac83cba909fb604c91df799daedf537a25b768f885750bd1eefae47a5a417d569cfcbc31f25657265df25ebf5e213e9e8baa3384150bcfab6b84a6ed67d35cf7044d76942e0c3933ec7915e2669f2769776643f216bbccb4a96ec671cd9c78460a097c92fc8ca2edf7624ff1d47f22e4d6e7a59fc0d47f4368fd368d593fd1d2dc29ee6c4800c02f3242e207c1800c2104a303931e084a785d07262800bcf4104cd89013542fd02764e0ce0e1a905089d18302454cd20e9c400259e9207d48901513cad88ab1303b024613306c100afab78bbabcdebe2eae2cda79ece802e816ef186b4205df5e4068031e4efe2785bcd492da9015932a954b9015de75fb75460f42269b3a5da121a807519afa2751d076af12aa6f754efb2aca407bb2d0303b82406b573d4d31b2026d1539db3f4069849f4bfc579d653ff5d9ecaeb495b33918b69ba7d667129a7f980d37715de4067ac05589a2670188aa651c40d39735991bd61a89c33457f72d0ed69a5841aa357f19f3b8298e606bda5969b65828a5fa3607e477a3c47ed8f28292b971da76ae6ba2a4ecb1d815891d148fe8f7859c66875bf6a5398e15c33ce3ffdd9011e674475b759fecd71e80b07103cb7a92151656ac422a2cedb8985c3e96693612140cd63b2443be955d253acfebb7c6b58a7fc639b2511e78dd3a4ce7837bcc43adfac517399b0da7cd321262c2e15fb946bcdd7f14d44386301fbc75d52c658bcbe5a47cb7b3460a37475163100372096b8ba71ca1118505b117c4cbec66b8ecab42448d60f0cd00dc87d9bad5758e4b270fc9bcb1c5bd98e6a1fd67b13a46275c4719c068ebfc5eb4d5cbe3ac1e2f155b6fac694360192e0714bfe9fa130a0f26394960553dc00cabaf957cfb098a4cd674a9bcc286d0c53dc04c675f610b3e5070664372d04c164c7ed48e43b0d323fc6d9761d5f46e8b9bd26f883f617edd5675fb292adc2e4e7d1f27c0da63530a5f88f2fcf9fa3e7fa6f5fb2fcf67d159b1987d29ae2539ed18d662c566ba2d77996acceb73462db389025de4310b0123e473daf6900daecfd2caede7ec042b4257983b6a02dc5e5bb675890f6ed7afd33da1d4d56449c8b132c4a9bf2684bda947f8e856753fe272c2eaf97511ae74cfb4dbb7875f9befda67dbbba7cdf7ee366704dd077c0b409cc8e2c71d31735b4ea855090d155b33ae2384e33c62e93755246f9b7d7517ebf382d0aea3af3dbdb063d700cc0ed711bfabbacbcdda1670b8e56d8dc368c478ef48a6e1694efa35dbabc632cbc6188722c1a9b20f1306da3363c6aa71c56806923956701a9c0b4952a70109460da48e5a905351886bd40ac5084c916085c54ba301e132d939b443d108c27451db9d329514f2dc8df7842d4518ae73ba6956047a892bb695ba363a014b9715a211caa25b842e6c669a6a307856e9c757a72d1f418e79f8e5410bbe9bc88a154c8dd7464c4705009de7474f4fec3bbd3c5654443897cca7745bf5d623a31aa085f7f3abdbeba461f165534afe2345eaf93cb7ec6361d115564d7bbf46df43f2d0e866aaa3223dec1df7f411f07b1de81d3e10fddfaaaf61c83dce1a08c8e0496f3bfba71bd242ba4afcfb1537f5dfc27ec6cdf14ff1b768aafcbff0d3b9dd7c54f7e514ddd28149cffb94bb695ceaed759e90a021a4730cfe8716671f45b94de4679ff8bf4c37fe3eb0cf338635898b0bbe1273f9a4a57769d296fe3e7ffe8e2e89f37bbfbd56e3611a9ff818ecc71e2439d7a5b0fed6747c53d76d4d57b74e8edc266ff12eb2fd3ed45ac635c6d2e627de05759c614364c161ff36443dc4de164cc783385beab94c94cceedf9d7ed3a2b928798597c9b9cc99ea65f809bdcc89e8631e006757c28efb84d0193c3589767da64d04a5d9e698fc52c2efa7ea8a1cf66556f9f29b9d97096d391cc9633d3cdf71157fdd51ff853fe0de9ee8a35c2067b91ade8f3eb2cc71a98f734d771ae3231169aab855b0730f1d1db91c86e5a7dd55cd0fbf475e65f95b650e214dfe776c2acaf67db4d7f3cb389a73e1b4f85be7d7d4db0595d13c3a2ff7d56943291f1824b1147397d74cb12194c3647f44c357be18ebac488a8760a6fc8c5c0aac3aada746fcd46d357bbf38859059b8eb57667714a5b8a56efee3adb9568c57e2e7ecf72a6b861dafbb72db6e4e7826f8769c23e63cee10c5d3cddf58e99cb0d3b2122869fc3cd319bd8e09caed74991ec365828fe96dcdee54981de41bf8e52d223e258e2af00e4f7ecdd3b8333d940fdbac4a2970adf6b152d266074dc4a015280744c275a26775934912be53659260e097d4e4c1c10ced5f767f17aaa3906d714c3f651eaea3599bbb9f203eb0ac89360a9ac768e97384deed095a2ec8daacab6f1ea2cafc216238d6c4552470b476f585634e75f990b9d0653dbdc8a66c3cb7f6803a063ad2fc8a3dee9c7eeb2342c2aa972c6dce02d347404aa1c956191ff318fb7511e73241e2eac1fc86d12464c699cfec8f839dc80f80ab86c799735a12222bfa5843f6ce31a95051ce2bf11a8a0817d582d76e90cb04685cb4180b52abcd91af8d90b9825c15de100bbfd55f7d52e2d934d7c9ee7197a67ac79ea530b80a734a89de26a9d45ab4650100703165a0eb5a98218186696467b9a1618e6958681ba01a6fb06bb6a3feea6d2ed38f72995d1741ce715306e123712c69e494e1fa275f2057fe18b2c070bd20cb4a37bbd8d362a90e37676858843763b53ace0794e7b70527ab5230b23fc6debf3f4769d14e83dc7d7a463057eb7f18c666e2a943e9ab429156d23aebc69afe95d96c7117ebf89e02a653b8bdb743a2d8a6c99544069f1cfe66e6df2f7b1994b055d128372d004023110f69143bef5510e019ac3834b82b484461549ca6f54c95c8da4ce0fe9eb781d97f141fd6a943e372c96d5ab784140a4cb2be7e676d153c1e6f629d7f8d6fe93d488abf826ce69b4dd88669aa16b90242dc52e7dcc9374996ca3b5a530053e0786b4c3adcd3816eb3fee1a207e791d6f695caab4b4945bb096750d10746b92eb8b6306dc7accd7ef8e4d3d6bc268a90065c3041a0b6a7a33c61c5ba187f83823d26978b8f472fca1e2828847326cea60a492b5741f425a86930c277d8bdc668f1f8f8e649f26f4084135dc1287afb274570c375c50dac73459c168e221d4f4836958950f510f4a98483d14da2890a37857da260263c367b47a8c0fbd0c3180ea63417a235f2f2ddfc68c06e63e0a1314045a0f182dad1ada2299bd85d757ac04ec0863ca03dda84e8d0c729486f706eb4db3cf763965dbc6a642415d476a6bc4cdf8d6d6a684b7695c85765730adc460a36f7748c3ac5515a659028389b15b070c6b7ac1fe6100ae924e8d5a8ebb3576d515eea95d367668649b6cd428a63d35dd9c306de17728e946c1f463f0358c1d9a12d37bea63740f1efb8530fda9db3c5041cc4007615a26b1c1b4a9426833a4c96fc4756876d846760c83a52e50860825d8551002287a8c07241a30adaae8c27561842124386bb403daad15457968c808defe38fb2aaaf6398eb0409b2906a9e1dc079d538c06bb413efe2d99c6ecd7c6c3c1ee7384a3187ebe46032e2bdabd32ff60f7f6cdfe83a808df89aaf42ca6811673b5be1ab78ec80539b070e4e0050705a5cd2043d6beb74e965dff663dd6ec80324557a61c6ccd3f9d471b4f3ffa7013aadfbb85ba65c7465bb05bea7b0f16eeca1e893f1836a72cf98c3e2214cdd0cc447dd93d9c8ff4ddf5594087182d7a4c605a277298fa6211755a4fcb325adec5ed4e5ef51b6a4bd740adb93224133adc1832d4ae1922a6913afd00b1eae248c3c24ae99836d54bd3f98d00c4b481219e00ff8f6786b0e9e3e4f8779b0b26b5ff6a2c54bda9d64448d7c9cc42e73599c188f19b106dd8fbc53bbe8f7bb180c7c3e6912ee2b923d38bf48170254d329e9fe8c9e67839556ea4d7e434c4f554a51c475ba2e324b6072bf3764ba1e941138774d165a0417956267235cc454a7bc7ca58b7725b4aece3eca612bb0e8eea5761358e32ef2de97483a08fbecbf888c67d593d19087a91c20aeffaeaf67d0981eadd5820476976f6cb06b91bcd2f26abaea71b01d97c7d4a136e9a3ee6086bb06ba80d98a288cbe0c006358b725c1a5633f35c2c2eb96a6947f5591ec3855754a74676d0511ade032f9d8d0fde34befee93cd5c15c4f06215ca6b041b7a13e00d8ede779021bd71f0c7e6a324f4ce3d4e9db9cb1f75200c8a1f6290069cc70370568a57218e886dd402fcbd4429c662b452dae3db0d24d5c7d2670d7eba88c1657f1324e1ecce0c650c3ae374468e77f63aade3fbfc4a65be3e1dd46cf9856b59473c3fd35add711f52ceda898e72a7e2c88873a3535de210d63da54d3cd08eb0b168078b42d38616010ee876eae3e08d8a811354788433dc320a9210b0e6f48b316eda1647382376e43454f370ac0f77c1305d7a109cdf6be6e9e34d923fe7d17b72b0b1ca60d7410a665121b4c9b2adc3b4c233b341aa6911add034c332950d8f8588512cc2a0208c54cd991b64c94ad0320cf94e57a3eccbd1393dc70133b25f7c4ae4944be0d1919b4280bac22b004adcee62aabd83b636beac96856d6a4b57d31af34b9e3a2ee87f68a1f545889505a6e4ca32ab64b6f508734a20a0961d04049438052210cbf268c05c7661061012915574272d4c31145dba683a5524ea809957208814ca5443c5b31023885a4570b9afe4d0b4f15010450a1ec482055b610806953964d7a37105c4d72c340a5e5e10759937cbc5b321e6c59bd2d9af5a1f1e227825603663ba8d8d5aab2a3431f2a5bb46d2cbb6aa1244c93a83d7988a735b5ec5441fb85593e49c52dfd00e3e249aec0cd4a8603a3be59186557ed0c37adcbc2f76bc4c850a3be2fdae16c0bcfcfddec5aa63292d0da6c00575394d0148ea6288bf9bb996cebff48d234ce691f2c2ca04834901d94aab141db303650d5a43197de0625609ad2d3cee228a95db2319051e1434f663a48b2c7a3a1bea99c445cb3c6328738a5ccdf3042fda8b611d487416a12372c0e34796b9a0940d8e5ec35188c79e9614053910f00615e409e2d9906bed6a7f300dd284675bf0f8c901dc24028f8e9bc46a3beed19fb7d0363900c90d6930d960347aec9c9ba567ec2d498c6750c6d15839cdce3d41aa44d6363bb41057b5483829c4c3718ba81aaec0e9b067a79a36e17cae50c0e4cb54ad02ef0c4c83cdd529d55bf7c2e48f3cfd651b2513bc1707130f8155f72ac7cdc70fba0300f7cc9aae05069b8b542c320c5371e95562a980650b2d960f47d769ba4788cd6c5e78bd1a67d668c560547c2282fb40930ca4b65ef308a5a8ee9889cf0aa8922a8abca7215166e72c7086002ec0db1a29b0083b473783359959eaf95ac9b673692b4dc48369293d80430e544327f0bd9c4d57a4bfebaeccf762fa3ad06a36a1a5dc055bef85880d5b415806d552cda62176a5e615535e2c3c0a6250f1415552320ffd68cb698a7cd68967f363bb01862f5b25ea2b35fe11baadebb0d599b5e8db62b6ba3e53d702cdeaeb32f840bf3aaf17c95949909eb7a3208e512850dbe0dd54de6e4e2da351a36715ac134a7a69b112a51c133f46423a072bfa365e0fa33219af7344606e3dcd39e2da8af8e5abcf5a50d8b37686534fce28d69de5c166fb2c4c65dbcc922c1d44fa94642673d78ba4c9f4d03688c8dab789b15d4ea13cbb4dc1565b689d2342b2b0ebf922e9dad73aadde2e56199efe4b70094ed755c76b7c397d92e2d797b57ffafd21c4a769667d92430e99ffa769d2894aca1d2c68af857d412675f9eed7d6f03effe5a384a2e447bbb5a57d5f704640f16c54afe4d9e10281a74c91742f16d639296752e2b59269aa0dda80aea8168e02e27d532f0be264d8ef9d628995b355a4ec70ba9124acb8c693197ff1a468998b71ca945d2bdf53a5e558d520f4828779471ec30b6e4634ed65da50185724462541f9860d0106b39a2b7816b17bc31fea2318250ac4b14630347acfaa4b836a0e50082135933a6b30c8e791d6acb8467e00a8ec41b2a64c1572963a890812f77a75862c87dc570527699fb8ae76498f694253135d04bea4aa6f5c57f031bf0c194c40f2c85e36ce68ae658fbf7c074209eb6980c1f708f01c1b7b9ef80686473da6ee4d89c4edb35b73ea232f2aed70506d6c28ebac453f88ee1c69c2128e76db12082afb085ab98a0809226dee26218122db09b819df836d959b682c02f94403617a17c50f1ccaa05749e3b6f878df97378c09069bc748e465c6a312b5c0369b716ef3a2dae19a4759c05f376250d3367160fe29a9d171d42acea2512dba056e1b288ade8d512b16103895ebb2ed468c2aa5ebd5606d6c6fba828010c386946cfcb495a5a9623694cdf86b1c754532d5347f55c58a929457993246032d83c01db3146492bf803d2b4d0a8b350c56d0bfe5056295b3d9949045a6a35b6a1dd18a3b8f5758118366ad517c267bb9c6e338a5b020620c35448b881c4c1410dd7a284b659a1ce9266b7cc16fcfe994acc6a1253ef95946a2cebb6f4ac6a1819c15cdd385ba126b1ea35ce46f8c8751acb20ef39f2792101b99a48d4bd3650427205774e35823555010816d8f5f516ab6083ba14ca80385545d57d545040e253ec0f6b04a8623e92e000fdd589b4ed00c9d358c185231d08927c1d238956752c6216309252b342463150db55f6b847b710c75533b5bc99d3155b810ba40ea2e0390c2772a19ed126346583c41fd48e832d0b07e9c09c86d386a23ea56f2c1ce086da09914f0a17d56fc66d0f03a1717f414faf96bb78046adec930d4a494b85ac341c5ad473e8ace4504188c0791f514e056df66a89a534d3e666b83a0360f7c33139dfc356271ad6da4b9965b3c755705746eb78102b9e5201106dfd4906b18d9684bf985cfee22aa7cf3d2db48691281898151d8fccd12a3c88df58d277a2987b969556ea0d0745d4ba83617dccd1a9d68f5fc27142997161e27539ec4a6d3600efa9052e52b30f9dbba9111ce54e0b6e82cf2db6306ad69ab2ea8859866d7ce902b1d90b44d7675aedfc8fcea4c97c12b721ac92233aa4b3504062f57357f75cfe03d98b27bab27774d7eef50fe832639b79dce9c4c2e2659346878adb34c0bd6d126cf346b8455171cb5a6d826b3f4e0b60193a9182f72f51b2d8c0cc0a75ac3881b7ca63581b0b954b92839ab93eb1afa0da6d7d54ad756b260425d7bfd8511acd199b0c9eb6aeab8f1bccf5fb6d3b80ea64ca18060ad928b72bdc6a6176577078c97bbad6a184daeca6c95804071992db97e1a735b321dd45c16c7f1048486bd2aee2738131271d915959d3461cf457013a24d4cdea7129836c99fdc31559a3f5150cd0300939c54d9f9707277954da3108474e492860e49044a09217c7543aeb8c184a4cc6806880997fd8ceb9631ff19d331c3530c1c5f4060988723ce82d3e6d452cb109f8a0bea362a19979d04acab512133ac696b46049300ca307c55a9a294830d4816e5397c81ec5083a3114c616410159cea48d92d29d991a798a41445a879c5533652361d838cf4d977947d53e6dff1949932e1ce50b2332481312c12000a9c072f139a960808412233c50c66d034c94b907284529d18bb28243b092b3f214589d5fa2d8c086d16ad1089557f6d16ad6e129dc1bb0039898369e3db90f641bd29ad4efc60d7659b1ac6c22a8f1539798049a8a67c03ea3e6b320e8412ab26d3c0604b1545cc7be82a0d223a3e7f91451f1f9fe952f3fe5b7729461fcb9ed500fc063cb8a0daa7e066414121dab59d1382b47b094a08a82ef06a3b115c3aa609041f225cdb3bd3d4612faf69a60a385a35426e75417cf7ea30c9412455b3328fc0a62fc12e024231933537ff8c2196c1cb77ba20cb4c57f5f10ed09c0121ea0334b84e9c8640bdcad9d326c02f30bf2143fc0addd7c67bb0af65b4c16c88100b08d926a62cd77164545956b040580c8d649171642790a9e9c8dd2622aaa1cba683766f994e73bc0e47d9d44f3aaa709caa990208c8c9f40b9c1a10acc24f3a348227e5d5bd22ebbebd38be5edec59ba8f9e1c53129b28cb7e52e5a5757b18bf603b1d6db24bd2d7acae69783eb6db4a41bc0ff7c7d78f075b34e8b97877765b9fdf5f8b8a85817479b6499674576531e2db3cd71b4ca8e9ffdf8e3df8f4f4e8e37358fe32527f017426bbb9ac8d0248012be92aa494bdf247951d21b045fa282a8e16cb5918a09512d79e175c26e2b5307ae94f5497fa6516d5a5afaefe6b65d761f1d5dc7f9439c1f5da4377944a6c9ddb2dce5f111df9c235d15bdb4df1001d0d3d34a163183103d396170bd8cd651de061b9502f39e65ebdd263585eb3571a2ff8678d5bf5b70cbe34db2232a5c1129891c856f20d7b36cb35dc75f05b6e75fc95f499cb691731d75d47301aa5675e97dfc10aff99ef48c16cd57a82f2a866c67145cd92236acff88e37b047bb118a889634115786d51f6afb27457f828ab6762d1ff7fcb9254065ec76ad17fb791eaef5999dc7cd3f1654bd870a6ed39a3764ad7e0a64060259d45c59d8f7e28fd2bd225cbd17499a5f137beb794d3a2f9dd4678afb3545249c5abfde02c31b0baabf836a16b253a4bcb5890bfe28d661df4a84c1ec814207396bfe2390bd151c4f902f88ce75d4ddc1fa3bc4ce58948fc86e72a24dc64991a7271aa790ae965599e86ccb3154f78fc5cd3303245992c7d0651c7a48ffe6b3100de25eb75c1f7a763b8683e5a0da8385a29f9351f6df811b796d02a39769f6d78d2c34e6dbf9902367cafef32754b9b8f36fc7e4bd4ecea6f36dc2e52e280132ff275b4a9a241c27cc55236357cd895b799b106b1949b857d712c38c5a2637e2c79e6c2824974fa514b82e6013118dc6cc8a5011868de75a18063a677f6d99879e00a422880b7b4a71bd999697fb35d928458dc301da1fa1359029f6d676d29faa05887a6985cd74423c3701f39c450083408fce01f020ed3aba88f7339b8aa54d714ed34a5e6825054b50ed268aafb3e0fab10c602569dfa182defc5b6711f6603ceda3403b115879c56a5f427ae53aa99917a49d8528a7ae2bf4c31f5a9d67c6e8bbd66a57815a5f7e012b2fe603779caccfa5ff19c7a49ab3430bb816288f4196e88e82a420f0f3d13e53a1e5ec23bacde430e09da137940b4bfe239d11511b04802574493428d0fa86183b58b82fefbc3cd7fb1701c942182feab8b03a1ce1785d6534525e8bbfdcd624faa8a242842b9fdd1c2881284dc1241414e87f86de0f101ef696db25572d31c572e4edc36b664b55d325c6db6b74a523bdf29be7d8bba80cd2ec743b4dee9793625026fb673953c9bb9609f0d20d8676308f6f9cc05fb7c00c13e1f43b03fcd5cb03f0d20d89fc610eccf3317eccf0308f6673fc1faba53a1377773b2b877bffdd1ba341f6e58764339383393ddbb449155c8417084d77722b53fe268ebb1e7210aae66f79dc8ce108cd2417c1dc7b94ad06bf9c804281e69ed2806adf659394ab9809fd68d83ecab8872167942df676f2aba0c0e014c45c76bae466200d95ddf27a1444759cd55728ee6558e873d927d0db52be7b61f376f8b78519c92a549f2652d2c5fd8df2d8e38e47bc39acbc2aa1b5937d16e5dd201207653f834c2c190e21e6946a38fb8d88a6e0c542c2c5698d0192be5b1d01db4aa950ede23a5ecfa4f7bbe5465076d350ff99ceb57f47b3cfae7a395cd26d30bd2ac0c238b275de07451edbbf86a03c1e4491f287d74cb6a4f9520f93c6905a5957789c1ef33eac3c8e14913284db4cb220f5554e44fbaf0d745bd83ea3930304c1eb73e1c57d25066cee1c61c5f93cbce068289523f2d89a41cf6c3f8fb9315890e3c60018b757990ddd8bd44379ccb77187cebd3bc23b06d6230d28ec4eba458d6fb005ebb121d1b9b9d89faf896dfa869f82cda8f564fe8c8024fc5aefa64f73abc3aea83d9b51ff7f04997904472d0e37e39fda4fb532e0cafb1e6827a5b477c09d1fe38b6adae2abe5e67e2438afee7d958687566b1414f58849c64ce20343352eaa82862e9fd60f7235ed7f4bf3c93fa179b7dfbf334fa42c6bfb86ddffd6cc3eb73bace96f732b3fef729ce58fb8d0c71bcb31f66332ae4c486038d063125a2cb4030f350eab7279594cc7fb209a153132af84d608da9c515b9b4bfcd06715036bb6130674efd87001d86c9f056a56e85f464b9fb15cf098ebca38fb7a37a9db98c0b217642fbdbccd03602cc3c01660b2d2740c0cb30d2bbcc6d0946699a2a290f8b158eec4b500e0bb543a104e1972f39c0a8fed986d11be21cae62201a5ac58ffb6ad5be6a407c926e6fd7ad643eda46a3fa982762e0b08a27f3ed291095b8e1304c20aaa95c3720cbe150ce9b589393f76666a2c4404d20bf52ed7eb6d804ed73612af8b59f865c3ecc0633754ed0b17003a55175c20ecc6844fc809cacae8c49a6c8d601bb28aee268252f80db5f2d160e55006169e9d0fd6af3cabf4eac2cc759e87f9fcd6800528c0c3310cc1561ce81114c546af18caca7e609729bea7e3b05fe371177d54fb384dca0ab1273459690b35da1cc0f1e555c4a690e687fc4f3695330400794e2371bf0de1251433cf92f365b91f1f25e9e66989f07de3680d711cde56a175057f0b98a52d87754ea3d49c5653269c1a2fad966f970197d05f9d09f9dd75afb6ac8f4f9034358304d0d08d3a5a51eda425489440536ed6f53db9926338c9269ffc9c2d2e431b102e28147fba355b89e5ce2d3fd68f174224993e24e64d4ff3a4c0021251a76d4b9fa70f34a8a5ccb7fc173ec538e8a2ae4bf58cc3e7483eb53b2892fd2eb7899a5627c61e8fbbc6cd1a00ebca606ac2db275d9c305e60a65d5023e4405906b8f59cb875bb03772fa5794afdcbc1146ad3517bf5dcd8a87cbb6a62a9144cdd03545852a5e5fcd551fb54fcd557585ace66abc48e6be7bfd1406dec8ef290c3cccee7b0f03afd4c19fbb48f4d5ea9f86f1afa6f72e4688bf8daccdceebd073529f5d2ac3cfbb079e1f32a4331b735c0eee2c7db5d9f3b7f5c5a6c42a35a983c29356e00a4898765847d1d6019e487967eb5d51c679b551e0126dc2e2d5a6b926843e515c86dd22699a20ddb5ec7fb6590aac5679757bc945a4bf6545d93070720e7fabc2330897a829b745fdc5c639f898e50a5ef597c06ec0ddae7c9dfd056c0ff35fecb6c2f72d83574517839646f8f4a89ca376a48d62affc6d95bd9d72b4308a7988f82069d57b712e623e58f3ebec16c0b2fb36e541d43e8ee7101bbfef934d523720a6539ba021f9ab45ebeeb2dab45ec55121bd1593bfe2397fdeae482f695777427bf92fb3b140aa6cd241a28df059a15d428d983828b710c46b3d5623aebd67c9b1e87ec4f339df44899853b7fec99207f1896e927c231ec988df2c36a3a3a2f82bcb57bf55b752b90d69ee8bcd25a8e52ea77a2ba3cd56bc09c57db268e55d46cc7a75ca233492fde0c44f2151b884c5d6fc5fd99b6849500c3ed592bf5a58c36c794fec529356fa73b914cca1fcd98137d066f19bed0b873704a2f10adc51903e5bd8d9024ae6ddff3a1bfb2a58b0b375940cb6bb00d5e56f74156c86b1bcb4429143fb9bcdba9634597e35c0fc6cc9eb1f72b077f6f7d9a08d8aea7d769ba42341acaacb1f620a366a73754b5f69640fc94a9c18844f56cf112b9a77e2b922f7612820cfc3385d65ebe1b22dca55f9e306e632ac6da1754a2bbb4cf5627522c536579106bd60a6ad03a14b03bdfa70104e40abcf38abe2d65e04535d10b3dda1d8cafbe45bcb7dd4fa5cc4711fb521f6bfe4d9300a70cdb3e334c045cf7dbe76d542cc61907a04c6e66bf5098dad6fbf5a69ce432ef87de86083653637a267132c909e329a0062441a864958944d680a9ac722834ed9f5352c636d08d5a039a9b78b2a32f9d15cf7f314372be99677f318118acf007c1e73b37d2a74aeb32fd17a8487d252452e5ea59987d21c077ce61aee49e8f98a0a47e4d4ff6a7726239e4aa8cf23d4304e6f778098d8dfe7fc1c78ea48519bec8c4c3f831af8ae164ff36ee4a337c867c0f4cb7f99c2c443adb26f4f65aa1527b2e2b7d96070a4fd1dffbd1dbb7d1dbf1d67f91461921384f66377fb492cd2d5defcd2fd5db43f508112b35945212e7abaebe55dbc89aade16db68d9c0fd4d921725d5e197a888eb228707ed5e2791c1f6b6f873ddff7219a5c94d5c949fb2fb387d79f8f7a39f8f9e1d1e9cae93a8a0d3dafae6f0e0eb669d16bf2e7745996da234cdeaf70d2f0fefca72fbebf17151d5511c6d92659e15d94d79b4cc36c7d12a3b7ef6e3c9f3e39393e378b53916c91bb6282e3ffebde55214ab358b1f06e39c3d81ecda8b77b1b8ae7ad142e62abe91ecd1b1a06391fc85d292d1e6bc3cdced12f26fb1dfbf5e104fe1ebcbc3ff5551fd7a70f11f8b96f087830f39d1c8af073f1efcefc383df77eb353d997b797813ad0b09b770ed35c2ebfa1fa27c7917e5870764d9f93e4e6fcbbb9787bffcc4322643decc378f37c96ed31c3fb6bccb8460ae3ef4b563d7bfba5930ebfb9787495afe64dd678619fb9a270cc73fe2f81ee4fa376bae94d5ab2cdd158bfe61b8468e963c7fcfcae4e65b68aeb4a5677d14eaaae7cfacf9b111a93c04c805a3f2e0235fa9f21598bcbaf3e578b6cb733205b14fbe2ced89c44165583003b69a4e3e467999cab6cd8e13f3eec2a1531cb54f8784b82e96cde0a87d9a21bd70f380b5f4ba2d08afee5d5b106ecc7bb620fc9ab76c4178d50fd982b012dfae05612a3e574333651d5784d7c43d0502efeadbf850dab75058774a60025b1f9438dbdc8a1edec1b41e1ef0068ced0ba2191207aff6d4535faf1ee698d6b25d4a4e78038b467a288c1b5fe499116e506898910dc8d5b3bb957fe8d9df8e07dbe16adfe06d4c1442e3a37c8cca32ce53baea8febd5dcbe0f980016a892dbc768793f0c586a3bd5c730705f46f73c5c90c2537b18fd692db6ecc3bb2da26a7ffe2a4aefbdf85033ebcd840d7031941ff286aeb9c18c3238f0a91e209b8107af0bf60c7454780eb5b7845eb5376f810698c79acb298d8c947906710881734898f1d1d279a0a3cb4fc70e42cc02b8a65bf0a9debc9425668db39c1d0d2d3a71dec59c6ae86cb25572d39c542c4e166575a1dcc3560afc1eea3be535c39b7516392c0a3996cf0237f159f8263e0fdcc4e7e19bf853e026fe14be893f076ee2cf0e4db4b6d49fa2fc3666ecf5939d7eb2d361ecb4882c8776c82c2c5a643d147cdd15654e5ba0ac3e152cc20586701f10b096ac2e8a5362ab1242d0b2f992656bfbb58ff761e3ebf826daadcb2aebb85f9754cb42948f6e3ad3c1e9272b68fe55efed888acf45c19fcc21f4831e40a66cd3b8e1a349296a1e0f1cf190db55be130e90a4da0b2460d26bc7a947d73697f987b7fa6851d564339a9af50d324b063d9074a9ad7183481d83d43c86026d844999a87dcc399f863a04a73603f550fbb2c67ccbd35843b7d3b83633b3a5dfd6d0798d594797d1da4d54f4baceff3c1448f4f99091873d701264c4094f4be8810cfadfee164dfcb5b4bd58c1644df6f017d974c91e6ca65d2771a9956d2f11f5b443ac8c3eea731623ef19e85214236e18f0e43e3bfe7d6e635b3db7947b6ad1da34cae1cd9929c1f0e3becedb67acb5acbd25f4aa3ddc35d5f635dd50f87047862a27b01919b06e6c876c9fd7d763aeeb73fa7a32e112f9fa2a5d4ae2ebb3ff2324ef459f39436bb5092f235bcc8cfa7cb0d82370c5db46cc197847eab37dcee78cb5b4621c75807711e398710f1dcb795bf74bcf3e062890d1e9f2bb7af8ebfd3b5e4bb4b4845e586593c15ad6df930e8157538a551c56b51955cd78d55cdcb79473976dd352c80dddded823086155b6c4f01bf5a68ca8160871c6c65e5fdbeac2a0dbce94359dcf53143167a287b7c647c2f63941ec93b1fa4e0ce116334c34200f1931b180bc243dd048562608453a2470387f843b020d00dbbdc3e6b9bfe518aac96632848464a33ea7b36da2515fdc7799467d19f599460334c9ea9e2980152ec7a807233eb5a825f458621f004201f1c2efb9281376eec5bdec81a6d83d702e1cc1a987a5d3bd1b7b4f804bffe9314ce5ac9fdeccc0b710aecc029d913fbd2b7e7a578c65eafcae18605a27b8f4180583bd1c41e57bb47e05e9f900d2f7fa84c7d3512d3731e4873d33f7080f86a80ade1890532ce2b40e275634ab1b9a7567ebb2a04569cc6e88132a9cd3d02c54688967bb2eea7394d95e8a68297d6f0131090cddf7cdf9e4853ea697cb47e8bb34db87203b42ea41db4b682cb56f109841e63c6d5a3fe4354155263fc42541788059db3e26f95fcd274c483331056048dec13640f76114f96fc2c8b9febccc9898decf83199fd52ffc08d526c0433e8e73189a9e63b24b95e7b1546812e5a947ddb39f7fb11d7662be3c8f23603e4d9efbec2ce4c67367c4a5c40bc2268ca0e49c771ecc802c771aeb84e9af98decea37140423b1fcb5228e2749a3c8f96b0faf987037a4539f973473e7c22e2a0ce877a0c05b45440d6b831ccd5306faeda2c51960e604de67737bb4f59e73ea6d95475582eae7a0752b9212f5aea33b7c1f7bad599d980f270ae2fc4454ebe65ea19e9e499fd34c775222cebb150eb0a1539ca370e29aa9c6d405138319b59e5d30df8b6c5b617bb32cbd70168a569d2b4210dba32311bc2b02ba2058636f06c5620b61e84e47b52cf8b3d5bcfed592e635b083ebe574f46396bc66423c3e1549d19ca8c53157ef6e6e690e5f34143e2279cc095b99eccf266483d96a913bfdd9193494dbd77839f14f45996f6e452fab4d7b8fb144e96b5b784be5bca563b5610d8fa844f5ed3cd242f032c0dde475d2a249b378670aa21ec3bc333ff632dd8e8d92e4bfddb2166491a487ddaac42b3dd4075daf8a1fd9b60e3c794a5733aef6b28ef1d1f97a859be7cb869866ffb5a7eea207743bcfa76964a1310e54924bd489ac8354f22a122a175d0786a4fe2e8c45145887992073362de25e5772d8f3e10cbbe0791fc3e83a7e21dda6a6a78d2f2e3d672ed133d69f9716b9906157cd2f1e3d6711509f849c78f5ac7c4f97c52f1e356711d51f449c98f49c9a745912d93aada6e535a4efbc7a67713947e9eaeaa5d5745bec0a6a3343dfa115ce072b72e13ba414d5af7f2f044cc79ffe243fa3a5ec7657c70baac93a99f45c5325ac912273d5c21dad68157d1b4fe3bdfb27f922a24408d69bab124a2176ae9193cd1a78cea245d26db68ad939240040e06631a452a82ae36f1cbeb784b0fecd252239060ade82a13146412d88b63068e7a94aad3b0b25805f7e2f703b19a3cb36c0b75c51e337eb16978f717c68a54af7848bb1aba1f8f8e4e24acec15305d2d9a225feb98c0d4a40a9e10a035e858cbfa3e29d49e004b0480a2fdf05ddb514e141864a8e2338f894e5d1b4603a3987477a116e5ac3129250f965b2517797458d4a7509e330e1bf4353987b5696306503d6a96b6847b5038392956970cdb015c16350b6a9c185b6c5ee78526c9f39ced1bd76cb945fce74767d7d44a53d4dd67a39e0beef6745e7dc29d15ee269e4be5bc850bcd1633b3d0851286722b5db0c02838047231b22d833e0fb324362655556042b949efb04d6f856153164b450be02b4ccecd1d01f6bcbb58415ebba416dc4b164ee2a77136d4430cbf4030d7b9de0a0ce8dd3cfc2ebaeb009bdccd04cc6e3de88d76573a04e4b42e7f15e1a85eb3ccc66ceacf39152add379b69d75e30c5ed24c86dc79c98971c8bdf19fb0daaae81ed930b3df9107a294ed1ea2907449be6d43022e6b4829be308986635e785e289d7744a448a3f980e331b3e3d81d66243c5be5bd46a84e2b3a21a0bc122d1d427f47242f13ac13612c1419031ec092990321d3820854a0d8a5f27f404402f3291bca2015dd2f69961f61119dd79c3763ab3eb07dc496dad5a7815702bdf1c89de392fef828dacef7e89e738caf67091c75d39b8481f0857d224e3b6f11cd7767383ffb4eb3b4708cfe2064c8743d2be9b641d2fba17d8062bad0e0ac26df8aa4be13795e1960263412e3208da90f150146a0fe61f48bd4559c2ee81fd64b8ab5bdbbbb17687c5b3f769c5eef13895bf3e561f56eae9ec7d571999cd2fa8fb3301ecd3a386a487d122aa8a811c6423e0b1a59ad924bda7d7bae633854f79bdcb69084cec2cbe8fa3db5d5cc7546e6c63fdd379aa83605b468220ff611408ca5d005ac47f1e047a5cd731aaaf093ca10774cfb7eeb197cca62eccd4f479e30e7c26b04756cf117a532f8f731ad735668243bf8eca6871152fe3e461af0008f744f001e1228f69ee85bb88a9bfd5f9dcb058c7ef7e42e2f783c43ed4fb4c70b850f66008b58f064b43db8682a33b309449083c916953396dec9ca0b97f8be4b9e0702666711fdcc42685cbbfefe2d6c5dd3fdcc97de05a047d7e44b803bab707b8abf2713780636241144ac031049c76b9df47811b5323d77245abf83283008f95016ec2a3453d71a7eaa26f034606dfde593b8781b0d7f6cd16dc73306c3419cba26eb3f66e4c5758d664fdeb234754df538c6669c191c1a4ae722c28359bc7583001368afbfd7b00948585aaca8f8c294d9d2380ea6cbd2bcafe49d6e2f76c156b612510700a95be8d02afa656da725dabf8ef83c04dec3f46fd2d8d1fe8a03e7ad73e1efcd8762fda34a2a62b540e06ce10a26a3e489ac270b962a8ca19184f6bc6d8b9f17755cabba08a3640c9019c618d90b51a7f07f3fcd94f7d16a85557393264a85fb74fce147e8db0c7f6c86e35301ffbf34792a6714e1b6eb04296aa9cadc59966d96681c65e23b3d8586fd770aa5e0ceae3c80d51ee8e0f0f9a912d0ad0b9f99b160839d5ea52bd39eeade3477e32e38883aafc34c7339aaaa781e0deed93cfc5ee4d7f2a18a8eeb1efcb3243c6003b6f33534da183a0d069440c60f56c4d4f9023426be81bab1e1b838d08955bbc7e9bde330ea03ec991b2355e98564e1731624b655ffdf2b988f3c5d93a4a366a574d28ce3ffe14bf8df3289eafb56abfae594d8161de7d0a12c080c0f7153cd43b4cbdb4f46c50f73ebb4dd23d465dd57e5db39a028f147575eff60e75a8458117f602cf8ee1de224d8099a02b8909d0427bb1c7268afea86b55fdfd911aa8aa73f3b74f4dd888b7e4afcbee547071196d35b80369c0a01c6291515058551a6d8166095f060dc421741d838496c413887c2ffd2b1e6d09499bd1ac1ff7750f4dd111a059409947b49ba6eae31e4cc26fd7d917c285795774be4aca6c10141af633a4a6f0d64cfefa881024f70e5375adaa196167ef9eec3e61ce167313bfd165fc4eda8b05f527518b069d7b0eb9e6dfd3a201edbd4fb468a00527431cbd26222c18a8be7ab68b3779b65928d6169fb285cc608cc5867a9e05dac356007d1e6c9764f2e58349394eb58fb68610a2996a810984ac26e04406fb1c3652a51aab81c3b08647ee741128870ad33a1a78ff88a36db7f6f5476ecd6e9eb06ddaa6c36c5be4fb012ca4b0f9a2f534df64bb7076b666374fb4366dd3a1b52df2fda01552d87cd14a9b707d9f60acab9c028b456bcbc888d440f9833028edda0423b4ff3c343aa7cafc6352cfbc71c964a5f20466c5696ec8ac1ba58666f3fdfbc026a0a1f982f35d52069bdf09af794eeeb461ba99bdfafefd4ceb929ee608cf3eab05873a0d484578821c8c00f54c51a579580d36877b710b97181a9893276ac2bfce366b74a4268fb1eb5f39dfcee897c9a7833ed0168e3df0f909f4a55a788f14f1f5e68833e265f2e9100fb485650f7d7e427ca916de2345fce96693b95b78817842fb2eb684632e7d7c427aa912dc23c579bb65e384f38a701638975ac23da9943e3ee1bc5409ee91e29cacac9dcdb9403b1dcac586b0bca56f4f182f15627ba410671553ed74baf92e15e52ce02e3785e32e7f7d827ca914dda3007dbd214e684a4211e76d4348b3df247951d2a4185fa242862ca5ba8e4be0d1fce141fdbfca8bbfd7cbbb7813bd3cdc56bfd2c0e9d117f9deb08461a84626f278d70565fd70617d6b181a6393908dd13723e243cad76d8149f0cde9c7b3ae357d296c637a0a94b6aee2edaeac1068408a5c50af25b93caa3d6ff2840c26435bf842aa76f0a54c9ad11cfcc80ad21456b5463c04a90f3c91ad924f4a956d928b9a5b54d358b508292158364599e5710f5eb0aca11dddf4af1bd65021451bda99043f96dfd036ebc68d5800ae592c85b3b1b4b9a765498c416c1a2c60593326aa7fadd7f1aa393b47b54b4eb5ab68945c506f4df8acd9c6c6c8f9a7e576c865944d908b1a1a0065e0945a00156a9bb0aebe752d808aa29a60a81c552d5ae8722e2940ea72a14eecbb2fc4a965a42e17b56e429df20dd18cba20ba2975714373a020575253a0428a6640452d9aa0c4025408d104242eb8309952dddcd7b6d20dfd31ee07205708539d52dadc576d7548012bb30ae9aae64b2a9b0194c5b4a68e5b0bd75f7fd376bc2e62a8080cce2d55099652208b298bd4b494e940553fb6eeb61cd665142360c85e9058a2adff81995dead774969535019f4c3536c554d53671b12ceb6ec2fe98ea6e8aa9ea6ea22359d65dbfcd33555d9752d55cc73c31552c447290aa14beb795dd929f37d9aa9f43857248f75a7c0ca674b0c582ca76a808507e1d104a40e1d8012515830e28695289fc3458568b5c4655bf5c12250be20d6eb2339d6e14e58c1e6e5ddc66489887837a28e08601f43251aa0f2aa4042254d8b4d813d6cecc5b3379cda72e8b5d9a3324960d6b1f14195bd516c436a92d6fd99ef6c988b13d6d416c7bdaf2e8f6f40f04346de90b21d6a75d59ab3634ab5943239a52b856e016c8e09d5fa35eaa5258a554854d4e137cc753769de072f01e86a2b0c98e01d7ee64530614526ce100250d2d80ae41492d800ac12d804a9a64205d4e9125201551f45f2a675a1b4a1706e495a15404ae5b2e67a85b3ac795aa964ac0354bc54c0297cfd36489cb650cbb86caea99131efda1c4823bd560c83427141c8d782cc51c306ace4048a37047192a8ea21845864ac91cf3a241884d7d62c40aa2752964115ad18f294ef3b959c557576c48e1be8f8a12d0a893a0f5bc0645dc4c455ed7c622b04a82a514aba2bca9abbcefce740e76c1672a2cf150910fdfa794999e6c78d1a90e59195ec6e3524f7c9ded727a17413c6d32a00ca61aa1bbfe6a7016187b6cbce0cf9055d252930c8f2de8c09ce1a33d040f2222dc1054933c3611c987ce0bde4594456422d1cc8b9ae3f87a62441cae731cd507eb153fe8b3b7c878fb52f55d3509aa8aaa3b049fd3579dd19fbd8717741834d5f7d7ece0c4d368baa95a1ed57d34ad77264792328e81516848ca3187a2e9f60fc7552e349c30db3b1c0ed214488734feb3159ff8837aeeb465611686fa7e1b270ea8d88c45adbce9b460029f6896aa06c2913a0e2dbad417bdd83517546a40a1ea418ba29b0aa9d38b5513d384fe514d37668b80a09e62aa0aa634dff56e77c150e74c1a28c6989ca61798781573210438550bce48a9f10d4d27bab58b883d980594a3ba8acaa8492ee22f52f1aaa871ed67a098cc4eaaaed0d6f233de880d213ae1d22f46763cc90818995a68f040c4edc9e8c986df97996688ca77a917ed3d6785b40c14ea6e4277b0abcee92e56031ca08be40c1fedbd70cf7914aac13091022443626912118117c21757f1324e1eb48242110e3af4b477e41b4385baee1e4c6c5c0613bcd0d4894f1eb5c8165c05186971142374d3c0d5c07118a119674403c9b8f81a5e44f25b12a3884c24438a48fd94a6e2837819632f22e6c1067bdc5e40b2519655770978a852f545f3fa44450fbd3c117969df91f809c7041c65d92111e3205e5731d01bcd8b9aa56aa7012c67683cfb8ea76f3af43c67aa8e370e1fa2eb7249430700d56b5e814d2000e1cdd0823e5c5289405956dd09c5e3a6aa23d2378d30340fb5585efcf750c261b92e1a1baddb0bc1900d879b6944c50e0ff6f19b7e20312507ed90af803d05428d1dc6ae74e506b62a7893ecd9f13f92348d73ca188707a9fc8073cbe008901f192fb80af4ae2940311c2ad4efab25bf7450f154b31ae8996a4a0fd8adc99d77a0db36eb1b8864acf5cd5838e236ed18e923b7f9648a0111e02f6e5f01356de49c4883840092e19683630945786abd68df900347d57049cd8916fca0be3ec612bfe9ce56356fe521664d81e0a2699fb89b45d3941c5d34dc537e88595320b8684cb6585b7e78314d35989a37d96679d40547c70bfbde1ce2557f0f764546c8eb7b196d61e1688a9bcfcfe1d7eddcd9b9feb93ac7150c1b5131d30782709d9da4280ac8b35f03ddf087bfca90120c337388087bb149611f16e7aba4cc340233500c292a65c48b1a51c6081621c4633a9d33503c2ef1089135164df01eadc1660aa20cadcec842067606067b887ced8050074f0b3fc204a10ea85271444447719d2a864c5cae9c6746cb960eaa6e98bb71812f2f3a2b749864de4a558e903b7c4225f2a17b400d2a82f038dfac1f24bbb5fa42fef0c9b427541f1fe908549f226691f3a5cb3274ba67f545cde05599d516fe458b3a1894a02e6568271f5585ce80ac55d6a0e99627511717370bd0171c04cb5561a1b3022b953568fae109cd21135d0cb48550a8308763fe8172e4425703c648c70b2a2cdc23296decb5fa441e1548cd61d91a3c972bb4e41d3661ecc0ca5147a3abf999a3cbd9ab257cc251402d0367351d582dea107d153f44c83d87d112342b263452864bbb39f42851c42bacb9992210ba85bf0897b851112b63a0cc9003ab4215beb13e51370564b45745c8dc828022064b5d38b01a14912c2b66a6e094ee27639dcbed6642e4382eea53b5d0558d6aaefcc2d96814f4e2b866d405afe9bebd38ae8385363f903fcb2c8f6eab1ddd7551fdfae2f86a47a83771fdd7ebb8486e7b162f08cf34ae0213f54cdb3217e94dd626a9135ad416693fb7d70ae3325a4565749a97c90d5d59e4d9929ea9a5b78707ff88d63b52e47cf3255e5da41f76e57657922ec79b2f6b6edf9926b9d3d5ffe2586af38b0f5bfa5711a20ba49909e942fc217db54bd6abaedd6fa27521284dc58246567f1b93df6b5d121895f1edb78ed3ef598a64d488af4bfaf729de6cd78459f121bd8e1e6297b67d2ee2f7f16db4a48f5c1f9215dd215631312b8217fb8bd749749b479ba2e1d1d3933f0986579baffff2ff013d180e6f1d1d0400', '6.1.3-40302');
INSERT INTO "__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") VALUES ('201607200916461_RefactoryQueu', 'Loka.Server.Migrations.Configuration', '\x1f8b0800000000000400ed7ddb721c4793debd23fc0e085cd96b2d4090922c29c8dd000190c29210b100f96bef261a330da08d9eee51770f44dae127f3851fc9afe0ae3ed621ab2aebd08701111bab9f98aeccaacafc2a2beb94f9fffecfff7dfdaf5fd7f1de6398e5519abcd93f3a78b1bf1726cb741525776ff6b7c5ed3fffb2ffaffff29fffd3ebb3d5faebde3fda72af48b99232c9dfecdf17c5e6b7c3c37c791fae83fc601d2db3344f6f8b8365ba3e0c56e9e1cb172f7e3d3c3a3a0c4b16fb25afbdbdd757dba488d661f547f9e7499a2cc34db10de28b7415c679f37bf9e5bae2baf747b00ef34db00cdfec7f4c1f8283eb302bdb7c709edc66415e64db65b1cdc283d3a008aec24d9a47459a7ddbdf3b8ea3a06ce07518dfeeef05499216415136ffb72f79785d64697277bd297f08e2cfdf366159ee3688f3b0e9d66f7d716c0f5fbc243d3cec095b56cb6d5ea46b438647af1a911df2e45682dfef445a897bbd89c3afa4db9564dfec5fc6c1b7303bfbba09b3a8547f2905bed6df4ee28c10b00aa8e9d8ffe9b9fcb0173fe4d963f3fb0f1d7c4a9491fffb61ef641b13c5bd49c26d9105f10f7b97db9b385a7e08bf7d4e1fc2e44db28d63bae565db2fb3b4645f7c6b1afe317c0ce3fdbdba65e749f1eae5fede1f25557013879d460f952ce85ebbf0f9330c1f405e3fffa8e1f5fa90d2084651a4aab769b2cd5df4d43119414dff964649393a3ba9907f7f2e0d80b190ff488be8f69b1756a44d276969885a4e6fbf153a2ec69a3a09f2fbb7411c60c614a31d8af087bdea336bec0651d3459a84df4c902bb2384d134a3983a0ff9a482f2fa265396d14419484197618d4b3cbc1f17249f47e2063d708bc261a44d01fa238ce1d051d062b4716c7795e76dc91c9e730587be8cef57deada92df23570ee7a5e7b32e3d9fd3601ddc198158e4f5695bdca556bc0c06c4f14d691082655123f9bc08d725c4a3db68d9780a36b3839ae70893455136d0c0268b0c1e8378dbcf0f6959bf47bb7e5dfa962191ca499a177a0993d2070ccd78e6fc784d4f6f765ecd79ce5af4b7691a874132803c4fa37c59b7d74ca62ddd8872cd4ac1666e723d5eaf53577f33d890318ee761a015f2cf8f6170b70d4b79a6584352531cb0c4cc6cda1419442de4bfad38caf55d697bf7f72e82af1fc3e4aeb82f9d9be0ebfedebbe86bb86a7f6944f42589ca052f313cd956afb79b9b6cf04ade95785e852b2f6e6ee9ec8479fe99b2a9643ddce881fe68e13e5f66d1602b9d7fdf86dbf02a48eed08bd1c6bbab080f7af2e1bdb98bc86818020c086a0619c71741b1bcbf08d7376176fc7790adb0b2bc2e61517e6c65cab31941a6dc5264c285fd55b8d9b65b2f2e7cde11afaae472be624662fb33f9897cf2a5fadf4b5fe378b5ca4a4d1a2f944ee2b258985d0449e9bb76230be2383c127eaffcac812dee659a19394a2643b01a36b98d29ab875df050f6f98066f31d99b4b3a4888a6fe25665f5b3e102a7912acd61004996df981f28d95c85b74c2fc870e7c478c8930b48ed28eb7ebedf463aa32163a27198c86cee3eb0b2701d6dd76789c49b3135e195c914b7ab2df66a9b0d54812df5cd902bd9b30318327b80c673cf5d4496e304db5edcc18fe5dae8b89c6f1e4bf07b6178b2cdb23029c4c9ad06a6664a8cb2bcb80cb2221121add92f6a77ed0081431b84ba5efd113c4677959805fff93e0a1fc375d9c78f119989aec2b82a97df471b168f9545590804efb2747d95c69cede2cb2daecb356585f31451f87390dd8505be179c965af3a9eb8b844cd223b0b4ba5f308969efde1133b0d2a8a72ed4d4c0ce227c7fa445bb96b19d91976f3b8fedc979f2588a23cdbe919d0d1cde0012897e84926add88c54df5d271287fbf8de250d321beb8464bcae2124da9694cb5552fa26bcf0c3ba4201a89bec4a26a8501e54d3556aee81fcb99e0a274f14bcf5faeafbad8c97d503445e5ba521715f5a4296faaa36a2340a11ceabbb40fb23242e3a505ad5a5d6b52deeafabbbad54019b8d55041d356f72b65cd48ef0ba2ecb1b2b864a4ab694c7bf6259728e278b3899ba3115246da115539a107cac250d35f1ff6eb24c4ea89f2268cb70628da0339b769d755548bec97581c1387d5968f73182fcb3eaa4bed6e13e53c035fad1617bdd892bc20ab9ebe9ea6ef16beb7767a8550b80018f0b6054727997891c4a6b3b0448e7632d03043c943c9c344366a46c6de4ae3d3d54c865b9ac1338c661d67659f7d59e639d964c0ae985a64ef860b6372e0f95e33e299395f535618385a026880d860ab1f324ed06ad9cc0059d58e9d23b43a1eea899a12567509e17d58aaa95cb2ac2e83a208b3a4a45c858d759d763ef6e1785442b90c960f7ac1f89dd6bd8e34c1c3c60e4d072f9b5f81b89c56f0bca61a717d3b6cc61a4bed7c7ee1c8c4c779ac743ffc2a481ea8f172f4b39533edce053cbcc65c67d4385d5a5fd4eb121ff63071db020e2398ded77519bd349fa9466ebdbf63336a7bcae9472c91a32313723a1472136d05f306537f6dc3bc680a590f121639b8330996467926c1fc010f0e7979bb5596595f6ccf57107df137c8c59bcea67e313bd8bbc305e00eb5575779afe5df9cc15b9883aa4d16c6a0a573188095b47a1e5657bc4fcae179574a1a709bf94f5318aa3575637e71c41a1bcd957d979a5e8e56d3abd16afa71b49a7ef25a937cf5f318c4514978bc5a29ce49daea6b03d73762c1ff001c3598918a6b23437af333eed214c671b8aa2490abf6334981e3a20896f7616bfee13e63ca4bb6f13444c6bdab9112c6aaa5ad747a5808e4a25ef554e2c6129e74e83d5897eb0aca7d58f9ed0627e78007bd2fd780e7fbec187c0f8e01af755376d26158cb4e350aa5669d23454c240c85d4d868c8063034d29a795a441f59127c27393ad35ecaa6f7695d0474ef352e869325b659a0d56f05df06797800b1698d2e2935a1cded0cdfa1be2c6fc58c7773666d64cff3d63d367b700a6c9fba4706390d6f831201d70f11bf474c30447eb6ece5403bcff44319c28f7baa6cbf6291fbe9847bf5b93f456187696f73344505e74e57ded43147dc7de6ae21431eaaa488d07a5939279f541489991dac68019df930831666afabdfc68e31c4631dd8fa309e158f012d28e3554e6dde95cb64a52fe7d1b2f0de0bd612f9f3bd811aa55eb7ae2ca63b7a4fdbcafbaa6ab173c16ad32365e6c511236e1eb083c7c006fb5ea615257adab3d72762e2830160a54f7e1a3451624bcb4d94134d1fb417653a7b0ce381f5214c202facffea6435a55bc60ede0b6f52745e8e9d2559af53fb351ca13ee0598c6636d81e8b5dab22c23874aea23f10d9cca58327f70121b13b25adfbd8b1380099cda5a71f22878d8692f8806330977eb54b56bb8e11ca039ec55cba564742b2d75a4d7f20b2994b07fbfdb4ca2f70b990d41f44804cfd9c4248ba2c1ea560bb4d50e7bbd73dcf19761a3894f4d37380f15437d27c2fffed9e4455ab10371e1e8ebeaa665cc729eb3a9efdb58d36d427b71d3ced0d2ee8249ca1c49cb75304b287411a2ae357e6ea639241ae12e07be6eb14a4a9a1f210bd1a048ae3a08610ba65873185a53fe5b3bf1dbb5976b6f6417cf697e638cb2e77ab01f77bd61cab5976978f40e1c9a1e1b84ef6222bcf43ab37d71da1c3244afe3b78a4b8f3fc2c216d5ab91e509ee75f92385d3eb873f27233a55f94fbba92220c6c7ce01781541bfb85a3904cd05a321bdf437283b1ae819afe9b5f802ea98b022152d4e54d4f469117fcbd86e741eac7e38b1e5e68ce8697673859cc8bbe2156d12e587297373e0da7c9575364b5c4edec9f51dd6c3f0f7778298e50d9d9a5baa4384cd4c5cdc33d3126695833a6ef0b6cf6ac863b1466cb28887933de453edf69c8d026a0bb1b136f7980eac0e54044f3264875f3d97a84d7ccce12d9b00042ad3124fda85097140685a6f8406ff7989a6c63d3296fe72b62d9398c6ff7913ded98ee0795e998f6321cebdc0ed41822ffa4d33e4c1000577ecba6c28ef4f4dbdb88e4618c1cc0964e291fec101da1bd2401c2304ef7babcaadeee797947eaf276a4e64252404ef2525d01dbae61d8509a0c89329226551270af94c5fdcf24fe628222bae27361c833af05ef340e098bef7d2c0ebe15e7c5933ccfafc260e5be09771d262b6727fb2a5c86d1a347ebd332341ab7957de02955a31720408c6188cad42ad55237ef1d4ba7ed1b5d1cdb3386c6c94231218c6d73214debe8564db0b1481da18b3d226954ccd20bbc2fff4b64085c81e73f596474e82f98d257ebeb9c3bf5779be73ff7e1f2c15b66dc79269b601e75f581b09bdc3ac69a0856df8ccdbedcd123285358223a363853168e334e15112c8eac9cf1c3853aa79036c2386380e000e3749354f1c5e972decf299c62b9ab84eccfdb64346f9f8b6d5a6bde195463ff12b2c4a6a7bca518e662cc2bf2602363dc7f31b5e55948dea739dbcbeb22c87cf0791725517eefa7415cf0b50a12445ac8906b3c1ab6c4ac7cba7dcba68fb6c9a71a2549989174d666e8ba22c93a8930ce93eb7099262bd376d8ee78d1969f2edadb3ab88460e924c54c6713323015c699ae852e0a37b72fa16c2e55cc38d242a969946cfb827053dbefca8676854c9bd9c312295b91006e365f4ed97ca1b0fb24d86637714d48fa9d06d4f43195fa096903584c17b7deeac97293dc969f5798ccb7165395cf0c733cfbbfb681f6e5bad5645a77da358229150c1b5e2050752dd8c29cbd01cac0b6062a68bca4d1acc2ac274d7e51a3995bbdad6968c9c8d634b2324a316bd734bac953d76852462563ee3b2c61be90d3711b6d1318c43a27c5a6b8cd201982d5055c7fe98e86ce6a40c7ee77ce6f6037196b7c76e47836369be01051d957fb6142069ecbc020f4530d85d625311d031e5c192bb7cccb6c865ea6a86632612de373ad6a3a2180b3976cd6b0427ab3e75e6d29193e1d6e482f8224b8eb0f5c0086530d82769fcc741040fb6ba6db57b514dcdf4466fc25bfdfd3bc687e7770bdefb7c569fab79f338ef967e8ae9885168760e2fa82c6b5eb02a30589ea1e3557a6daeee14d87ac8c60e7a4054d6d5d33dd615a4e0b6c21d0099d501517cc2186c68775f46a19a7b68a9469327e72eec5aa5d04a59f96849ab76a24438ff3fd98a62ade8e0e54db6007dbf3b7b0a2a1e4d06e77b6f1315a4775434332c81dcf37aeefd37adabb0a837cd3e783aa8354355362a7ab20b748a7f065b32afb493abb65a7ed26e43ffbdd7ae6e86701df5386c4c4cae716fbfb3c66dbdfe27d9e6937bf277e8ce2ed5610bcfa1fffde0832d53b5316be88416f0980571a8072deef023ba5a757b5da69d392b9c6e2f7ca8baacdd0d5182bbbc1a585c7da0e52b635201c8ba9cc878de1701ceedd03946e0ec56441e4989cad83285638522f7ffad9832755d5522e6f6fa36ced213a4090e77fa7d9ea77fa71ce5057b2afc3e5362b515bcef1ebcde0b55ddea7a527bdadcde178757953cde7bfd377c1b248334f21253ea6cb87d2c93b4b56c495fd522c45cf16c9c04b73ea5798ef4a30872b0f3bf5c46069964dd8f1e7b069114492800f9c695db445a9ac1b600931b9065ccc74b6fb98de4592a8fd7c0d6d517953eb12daa636c5bcfb137c3d329f42554edb7827df82b0c409bb29296f745540dbdaba944f7fa2c29961548ccab780f8fcb0173fe4d963536e675c8be153081009396faf970226f4834f7a554dff08e2adefaa6c315a19180f18adf84c86d1aaf6f2e7c768453c97433d455bb8648f2adfa2cc7424702d93eafce8e52f5e265a7e22a0bb3976e5c623d316c4c4747bc0306133198461808145493b6db0e8c152b655fb576973bfdfea555ebdf5123c94b83e60f84cb61c6ff637ade64e8a76ac39b47db5e1f73147a906af8f38bad755f405cdfa37cb076b83bd34682557c350e58837d12f5982452924d6a5951793a61b86ca7ac9e0ca327618ab30c3b9246ea50785e9201e664089af35cde0afd832df48d65aeef094a5ba5441d90a9ee4229337688acc060d7ecbb7fb50d9d3da39a9de78d5d713ccbadbb840e7c96d1694d56f97a4dd0712a6534da1edc50b8bc147914efd96839c8c37310cd8086b139eb5bbc6361320a2895baa2e2f095caa21723214efe3f426886d634409d4df6d581a2f115ace5651e9bb9adfa3e32e44085ac15e8a10865772b765445307436b7eb6f65ce71cc0476a106add280c8220f5054bd29b027549c108688a7b8f6a23d6270b68a32e89e888bf3036b5452cbbba4e4f8c7d1e28dc3fc76adabb302796ae374b3db503e0a11dd56c6f7b59cf664bcdc376daa45b69135c8698fa1895c85f7b5a4694b2684a82a7657d01d5691955cacc7c6dd734dec4676be7f9bb38b8cbbbce1b257ca41e6b894fe2bce49dfb42ec76fcadd42dbd47c42aa85e8f77d72a93b217d569cf9bfd17823299b21fa2385e7c2e3b1a771447088ab7df1657d16ddc57f31247749d44257c58d25738d28b2889de6f938eec47648df7694193fd8423bb8c4a0cf432f91959d9c5fb8ee4bfe3483e24d16d2f8b5f7044efb33009563dd9af6811f634471a6494308fc21cc287062014a10093230d4e585a082d471ab8b01c78d01c6950c3d5cf61e748031e969a83d09106435cd514928e3450622959401d6910c5d2f2b83ad2004b1036651034f0ba0a37dbdabc2eaecedf7deee934e8e2e816efca1624ab9e5c03308afc43186eaa39a925d5204b24152ad7a0ebeceb86088c5c406fb6545b420db02ec25510d7377d176f4372bffd3e4d0b72b0db32d0804b60503b473dbd0662023dd1394daf819940ff7b98a53df5afe2545e4fda8a899c4ff1ed328b0bf9d0079cbeabb0282a63cdc1523781c350d48d2266c8e9cbf2ec3543e58c2afaa3856e8f2b25d418bd0affda9688695ede186ab95926c8f8350a6677a4c773d4fe0ca2a272d971aaa6aeabe2b4dc11f015698de4ff0897458856f7db36fd19ce3563fcd39f2ce07152aaee2ecdbe590e7dee0082e5363524aa2c8f5844d4393fb170385eaf532c0488798c966827bd4a988ad57f97ab0deb945fb6191671de3849088d77c30bacf34d1b359b09abcd55ed63c262d2b84fb9d63c0d6f83923316b07fde474588c5ebdb38583ea0011b24ab938002b806b1a5ab1b260c8106b515c165f4358c192add92208a1f29a06b90fb3e8d5758e4d270fcc5668ead6c47b50febbc0952b13a60384e03c7dfc3781d166f8fb0787c9baebe51a575802cf1b829ff9fa2d0a0f232488a9c2aae0165ddfcab97584c92e653a575669434862aae03639c3e8674f98101d94d0b5e30d9713be0f94e83cccb30ddc4e145809edb6b823f497fd15e7d7a931674153a3f8f94676bd0ad8109c57fdcbc7a859eebbfdda4d9ddc7eaa12d0ea535c5e72c251bcd58acd644a7591aadce3624d2e338902dbd072f602df91cf4bca60168b3f7b3b87aff090bd196e41dda82b614171f5e6241dab7ebf427b43b1aad4a712e8eb0286dcaa32d6953fe15169e4df91fb1b8bc5e06499851edd7ede2d5e5fbf6ebf6edeaf27dfbb59bc13541df01dd26303db2f84d5fd4d0aa17425e4657cdea80e138cd18bb88e2a808b26fa741f6b038ce73e23ab3dbdb1a3d300cc0ed7113fafbb4b8dba2670b8696dbdcd68c4786f48a6c16141f836db2bca72cbc6688322c1a9b20f0d06da3363c6aa71c56806e23956501a940b795ca71e094a0db4865a9393568863d472c5184ce16705c64bad01e132da3db483e10b427451db9d529514fcdc95f7b42d451f2e73bba9560472893bb6e5ba3632015b9765a2939544b7089ccb5d34c470f0a5d3bebf4e4bce9d1ce3f1d292776dd7911452991bbeec888e22013bceee8e8e3a70fc78b8b808420fa9c6df37ebb44776254119e7e3ebebeba461f1655346fc3248ce3e8a29fb175474415d9f536791ffc4f8383a19aaa484befe0d79fd1c741b4776075f843b6beaa3d472f773808a3038ee5fcaf6e5c2fcb15d2d757d8a9bf2efe2376b66f8aff829de2ebf2bf60a7f3baf8d1cfb2a91b8582b3bfb6d1a6d2d9759c16b62020f147b3941c67e607bf07c95d90f5bf083ffc37b64e3f8f33868509bd1b7ef44257bab2eb5479133fff858da37fd6ecee57bbd9a548dd0f74448e131fead4db7a683f3bc81fb0a3aedea3436f1736fb97587f996c2f621de36a7311eb03bf4d53aab066b2b8cca275e96e722763da9b29e45da548a6736ecfbe6ee2348f1e436af1ad73267b9a7e01ae73237b1aca806bd4f1a9b8673605740e635d9e6a93462b7579aa3d06b338effba1863e9d91bd7da66467c3694e07225bc64c37df475cf5577fe04ff9d7657757b411d6d88b74459e5fa719d6c07c24799233998931d05c2ddc3a80898bde0e7876d3eaabe682dea7afb306cbb4851227ff3eb713667d3ddb6cfa63994d3cf599782ae4edeb6989cdea9a1816fd1fd3bc1089b4175cf230c8c8a35b9a4863b219a297b2d90b77d4c5475236537843ce07641e56d5ba7b6b269abeda9e05d42a5877acb53d0913d252b47ab7d7e9b6402bf64bfe479a51c535d3debf6db025bfe46c3b7413f609750ea7e9e2f1b677cc6c6ed8711131dc1c6e86d9c406e7388ea33cdaaeb150fc3dbabbcfa21cbd837e1d24658f4ac7127f05207ba0efde699cc906ead70516bd44f84eab683e71abe5560a903aa8633ad132b9cbbe8b5c29b749767148e873e9e2807026bf3f8bd753cdd1bba628b64f5257a7e5dccd941f5857407e154365b573bcc0697287aee065af5555ba0957275915b61869642b923acb007ac3b2a239fb4a5de8d498dae656349d96e2539b38016b7d411ef54e3f7697a56151499531e61a6fa1a12ba1ca506916f99759b809b29021717061dd406e9268664ae3f467cacee11ac457c0a5cbdbac0925993c0c25fc6913d6a8cce1d4208d40390decc26ab14b8382352a4cee12ac5561cdd6c0cf5ec0ec2af60a07d8edaebaafb64911adc3b32c4bd13b63cd539f5a002ca546ed0457711aac1a41411c34586839d4a60a62a099591aed295aa099571a06f206e8ee1b6cabfdb8db4ab7e3dca79446d3b19c57c0b849cc48187b26397e0ce2e8067fe1ab5c0ee66533d08eeef52658cb408edbd9e5220e99ed4cd1826739edc049e9d5b65c18e16f5b9f25777194a3f71c4fcb8ee5f8ddc61392f12d97fa68c2a654b00998f2babda60f691606f8fda6125709dd59dca6d3719ea7cba8024a8b7f3ae77393f793ce78cce9b234287b4d20100d611f39e45b1fe510a0d9dfbb28911691a82251f18d2899a9b1acf353721ac66111eed5af46c973c37c59bd8ae70454767965dddc2e7a2ad8dc3e5523dbda7f121a7115de861989b61b904c33640d122505dfa5cb2c4a96d126880d85c9f1d9d3a42b6f6dc6215fff61d700fecb69b82171a992c2506ede5ad63580d3ad4eaeaf0f2970ab315fbf3bd6f5ac09a32503940913682cc8e9f518b36c851ae2e38c48abe161d3cbf1878a0d229ec8b0a983910ad6d27e0829194e329cd42db29b3d5e1c1c883e8def11826ab8210edfa6c9361f6eb8a0b48f69b284d1c443a8e907d5b02a8faa1a9430917c28b4512047f1ae944d04c686cb6875181f6a196200254bcf6a817cb5b45c1b331a98fb284c501068356094b47268f364e6165e5db114b0238c290774a33a3532c8511ade19ac37cd3ed966846d1b9b0a057515a9a911d7e35b599b14deba71e5db5dc1b412838dbedd3e0db352559866710c26c66e1d30ace905fd8706b8523a396a19eec6d89557b8a37659dba1916db256a398f6d47473c2b481df21a51b05d34fc1d7d076684a4cefa88fd13d78ec17c2e4a76ef34006310d1d846991c404d3ba0aa1cd9026bf11d3a1d9611bd9310c96ba40193c946057810ba0e8301e9068c0b4aaa2f3d785118610e7ac910e28b75624e5a121c379fbe3ecabc8da6739c23c6da668a486731f544e311aec1af9b8b7641ab35f1b0f0bbbcf108e62f8d91a35b8ac6877cafc83dddb35fb0fa2c27f27aad2b398065accd5fa6adcba522ec8818523072f3848284d0619b2f69d75b2ccfa37ebb166069429ba32e5606bfe693dda58fad1871b57fdce2dd40d3b36da82dd50df3bb07097f688ff41b33965c867f4112169866226eacbeee07ca4eeaecb02dac768516302d33a9ec3d4178b88d37a5c14c1f23e6c77f2aadf505bba1a6ac5952191d0e2c690a676c510d18dd4e907885117471a16464ac7b4a95e9ace6f0420a60d0cf104f87f3a3384491f27c7bfdd5c30a9fd9763a1ea4db52642ba4e7a162aaf490f468cdf8468c3ce2fdef17ddc89053c1e364f7411cf1c999e278f25d7b249daf31335d91c2fa78a8d749a9c86b89e2a95e3684b749cc4766065de6e29343d68e2902eba0c3428cf4a472e87394f69ee5869eb966e4bf17d9cdd5462d6c151fd2aacc651e6bd259d6e10f4d177291f51bb2fab260341cf5318e15d5dddae2f2150bd1b0be428cdce7ed92076a3f94567d5d57423209bad4f6ac275d3c71c610d760db50193e761e11dd8a066518e4bc36a669e8bc1255725eda83ecb53b8f08aead4c80e3a4ac33be0a5d3f1c19bc6d73f9d252a98abc920848b1426e8d6d40700bbfd3c4f60e3fa83c14f4de688699c3a5d9b33f65e0a0039d43e05208d19eea600ad940e03d5b01be865995c88d36ca5c8c5b50356ba89ab4f05ee3a0d8a6071152ec3e8510f6e0c35ec7a438466fe37a6eaddf34b4cba351ede4df48c69554b3937dc5f937a2d514fd38e8a79a6e2a78278a85353e31dd230a64d35dd8cb0bea0018847db82110606e16ee866ea83808d1a51738438d4330c921a32eff086346bd01e42362778e33654d474a3007cc73751701d9ad06cefeae609952e828e25944bc12c2380504c951d6979296d1d0079aa2cd3f361cee87572c3194142ee885d9d885c1b3232685116584660085a95cd9556b173c656d793d1acac4e6bbb625e4922bc45dd0fe57528a8b014a1a4dc9846956f97daa00e69442512c2a08190fa00a544186e4d180b8ecd20c20252282e85e4a81bc992b64d074ba99c50132ae1e00399528938b662047072098216245596129e320208a05cd991402a6d2100d3a62c9d206c20b8eae486814acbc30db23af938b7643cd8d27a5b3439f9b497e410b40a309b41c5ac56991d1dfa00cea06d63d9550325619a44ecc96338ada9a5a70ad22fccf249286ee80768174f62057656d21f18d5cdc228bb6aa7bf695d14be5b2346861af17dd10e675b787eee66d732999184d66603b89abc84a670347959ccdfcda45bff6794246146fa60600179a281eca0508d09da86b181b2268db9f4d62801d3949e763210fefb366c6ff62d68acc88021290f418f2a6a023d590d5339829af68c65eb34829fbfb9ab3ad06ca2d23b214a1c88e5a54833df2997d560b675e31969f2f6b89e8798414d2e7934d4263e9ca1bb813a9c911118420e0404a2969d3b9fd1f5c4f558c40ab03b7a3e4377819e383128a2a78661ac235d030053c54c3f2f9402fdc060a322f3885140618ecd18d953c49b5380c0d057c49853a896dd34a78a9e8c6b4e158a9b3f568f3744afd52f5ff272c09dc441b4965f25828b831176d8926325fd85db07bd25674b560587caf5ab141a0623ae416f9452c1348090cd06a31fd3bb28c163b42e3e5f8c36edd363b42a38124659a14d8051562a3b8751d4bcaf22b2c2ab225499aa2a4307c0df3a1e238009b037844f31010649e7f066b22a3d5f2b59374f6f2449b9916c2423b10960ca8864fe16b209def3befceba23f14bd08360a8cca6954511dd9e2630156d15600b655b16083ddf5778adda8101f06362db9a7d08b0a01b9b766b4d7fea419cd951293a53e8658feee5fa0338fd5a2a97ae7f6024c7a35dabe80899677c0b1781fa7372517eae9d4d92a2a521dd6d56410ca050a137c6baa9bccc9c5b56b346ce2b482694e4d372354a25ee8abc94640e56e3fc9c7f5674234efe8437ccab9273d5b105f1db578eb4b6b166fd0ca68f8c51bd5bcb92cde44898dbb78134582a99f508d84ce7af074e9049b069087fc57e126cd89d52f2dd3729b17e93a4892b4a838fc5676e924ce8876f337fb45b6152fd113b6d761d15dab5ea6dba460ed5dfdbf527328d85996659325a17f23db752297b2864a6b2b629f1f0b9c5d79b617a535bcfbfbd428b994dadbd6baaabe47207bb02856f26cd274096fb6108a6f1bf8b0a813e688325144064655500f440d7731738f86f775d9e4906d8d94b951a3c59c9f902aa1dcaf98163349766194f0c991915a2cbb17c7e1aa6a947c4042096ab46387b2259759b9ee2a342814c39ea2fa40459c85588b6183355cbb0871e18dc2084201f5508c351cb1ea138267809603888062cc98cc3238e6753c1f4d05cc75098129f355c389b9422b7062be6238b5d78c604eed573c27cd84242d89a981dcbb9632adefb26bd8806f80047e60291c673d573447e616180c19a47a6a1f1e30f9fc898aceb8017715107c9b3b0d88463627ea5a8ecd09b45973eb63282defdaf7d7b0e676cd059edc770c37ea9c403a37f305117cb96d5ac9240494d4f1e617bc9068811d0bece4b64e4fd215348cb812c8e622940f2a9e5a99800e729feb9cfa717f8f225378e20c0dbf9ca256b11ad26ebddd759a5f17086b3503e6ed6a19664e2d10f875392b3a8458e5cb20ba41adc245111bd1cb2562c20612bd72eda7d08451bd6aad0cac8d8f415e0018b0d28c9a9795b4942c47d298ba0d638fa9a65aaa8eea2dad545392f23a49c064b07902b65cb49296f007a469a0516ba1f25b13ecc1ab54b66a329d0894d4726c433b2e5a71abeb0231acd5aa2b844fb619d94ae497fd1a20c35448b881c4de410dd72285b65ea1d692a6b7c516ec1e994ccc72125defa594722cabb6ed8c6a1819c14cdd385b212731ea35ce46b8c8751acb20ee2bb209e600b9ea48e4bdd65042720577471582d555010816d8d975162b6783ba5cac80386545e57d945040e293ec012b0428633e92e000fdd51979cd00c9d218c185211d08926c1d23895676f4a117309252b142463190db55fa4847b510c75533b5bca91314538173a416a260390c2772ae9ed126346983f81fe48e83290b0be9c09c86d386a43ea96fcc1dd2fada09114f0317d56fda6d0f0da1767f414d2f973b7fcca9dfc9d0d42495b85cc35ec5ad463e8ace4604188c7b91f514e096df58a89a534d3e7a6b83a0d60f7c3d1395fc1562b1ad6da4b996593c0949efb59b1b220572cb4120f4bea921d630b2d1d6e553970b574ba913013695bb54d8eced11adc8b1f9ec4710bd3aed3b24728344f16cd771a9e2852e33b76754a2c5e5869f40a46cc671944c1549ca359d86d3947b942a9c97dc6664f83315b82d3a8344d99841abdbaaf36a21a6d9b5d3245d06246d92a699e937325133d565f01a9c42b2c8d4cc420d9ec1cb54cd5ecfd3780fba34c1f2c95d9128d897ffa0c8f26ba6332b938bc93a0b1a5ee374b59c753449584b1b61d92546a5293649513bb86dc0a43cc58b5cfe0e0b2303f039d630e2069f624d206c26e7264acef22c9d9a7e83793a95d235952c9899d35c7f7e04ab75264c1244ea3aae3def7397ed34ae8334e52020515c7a42a69fda0485540715d7a3713c01a1612f47bb094e07455c8a3c692775e0b311dc8468e333b0c904a6ccd426764c96ab8d175473e55d2727598a359cdc6d65d32804211db1a4a64302815442089f5293f06b302149d3520162c2a5b062baa54d6245754cf3f800c7171018e6a984b5e0948991e432c4e75382ba8dcaa8642601e36a64c8f46bda9a114165f1d10c5f59be1fe9600332fe380e5f20c5cfe06804f3d0684405e7ab91764bc858e3282621cf0c6a5e71948d9012452323750a1569dfa449541c6526cd9a3294ec64993c00b1a1927e303dd3a5fda03aa5787d886239b8b59225a290094a97b342ec95226b052f2884ebaa4b533198c3214da0a09114d2cdd7a65b7094d5346ebe2c90bf4666d298ffd2ee4151ff1d250685f9c78d6c477b8542175416675e50e8323459d3a04b12d81cba4b810881cede64500741a7bad63c0056dd8a50072ca7c5043f02f62ea8f62db05e50501c6e65e7b848dc4e82e2a26673bcda4e78978e6efce1e3402b7ba71b87e6f29ac7286c9e03ebe55617c4778f89c5eb262926acae6204367df176130c0a8cabb8faa58da30bdebe5245d2a5baaa7ef08ee60c0851fd42dff61c56138d557a186b12c515382e45c671e5baaf7cf06f5ecb68835913061410b249e050a6e3c8d0a1b46081b8080ac92283854e2053dd99ab49d84b4d977527adce329de67c150ea5a89e74643117653305107591ea1738352058f99f74489846c2ab7b46d47d7b7d78bdbc0fd741f3c3ebc3b2c832dc14db20aeeee2e6ed87d25a6fa2e42eef299b5ff6ae37c192ecacfef3f5feded7759ce46ff6ef8b62f3dbe1615eb1ce0fd6d1324bf3f4b63858a6ebc360951ebe7cf1e2d7c3a3a3c375cde370c908fc35d7daaea672689680e2be9655972d7d176579418e906f82bc54c3c96a2d14e34217b2c2eb84dd56268f4e28ea93fc4cc29ab4b4e4dfcd75abf42138b80eb3c7303b384f6eb3a09c26b7cb629b85076c730e5455f4d27e570a801c4b56b2082984a8c94b06d7cb200eb236a2a4107df5248db7eb44179355c789fc1be255ff6ec02d0bd7d1b654e1aa9412cf91fb06723d49d79b38fccab13dfb5afe1585491b1ed552473d17a06a59973e868f61ccf6a467b468be427d9131a43b23e14a173161fd67183e20d8f3c5404d1c72aac06b8bb07f9b26dbdc45593d1383feff5b1a2522f03a568bfebb8954ff488be8f69b8a2f5dc2843369cf09b153aa0637053c2be924c8ef5df443e8df965d321c4d1769127e637b4b382d9adf4d84779a26824a2a5eed076b8981d55d857711592b91595ac482f8156f34eba83745f4584e012267f12b9e33171e839f2f80cf78ded5c47d196445224e44fc3703537f4de273e445b4740167c7a40f9d6a00ac0f511ce76c873a868be6a31150c36025e5d77c34e157ba8b25ad9463f7d98427399d53f69b2a60c2f7fa3e95b7b4f968c2eff748ceaefe66c2ed3c291ddbd23b3b0dd655983d982f5fcaa4864fdbe22ed5d6c097b2b35caf0f39679377780f058f975b88f0ce34cad56e5e668251a38674b9c128ddb60e388e99da89a68391819e3957006f6b8fd7a293d0fe66eaeafb5834501d21fae359029f4d674321ac1b5f87a29858d74423437381d6c750f03408dce0ef030ed3aba80f2038b8aa64f7eacc3425e7825054b5be5068aafb3e0fabe0c702569dba0c960f7cdb980fb301676d9a81a075434eab42ee08db2955cf48bed46a29793db15fa698fa646b29bb4554b302bb0a92077069567f309b3c4566fdaf784ebda4651a98dd40d18450f437445415a187879a8974ffa4bac1c483afff759a21417a220e88f6573c27b222021649e08a6852a8b1910a4cb0769e937f7fbafd2f068e8334f6ca7fb57120e4c976d07aaaa8387db7bfe1b55d8768e3a1dcfe6860444b84dc9582829c0efedbc0e303ded35aa7abe8b639065c1cd96d6c896abba0b89a6c6f1565ed6ca7d8f62dea0226bb1c8f41bc55f36c4a78dec4662a793973c1be1c40b02fc710ecab990bf6d500827d3586607f9cb9607f1c40b03f8e21d89f662ed89f0610ec4f6e827575a77c6fee66e5e2defe5645ebd27cbaa5d90de5e0cc4c761f2249ba160bc195bcbe13a9fd19061b873d0f5e7035bbef44769a287f16e2eb38ce55824ecb472af2eb486b473e1ab0cbca5148a4fabc6e1c645f859733cf13fa3e7b53d185c6f7602a3a5e73351203c8eefa21f2253ac26aae92b334af62a0e191ecabaf5d39bbfdb8795bc4f3fcb85c9a443731b77ca17f3738e210efe32a2ee1ca6e64dd06dbb8200380ef26f769848321c9fdcc9484cbb0b115dd18a85818ac30a13356c263a13a68952b1dbc9f49d8f59f767ca94a0fda6a1e7239d7afe87778f4cf472beb75aa16a45e195a16cfbac0e9a2da7771d50682c9b33e50fae896d58e2a41f279d60a4a2b1f228ddfa7d58796c3b326509a6897450eaaa8c89f75e1ae8b7a07d5716060983c6d7d58aea4a19487c38d39b6269b9d0d0413a97e5a124139f487f1f7272b121578c00206eb722fbbb13b896e3849ea30f856e7cf46605bc760a41d89d3285fd6fb004ebb121d1b939d89faf896dda869f82cda8f464fe8ca059e8c5df5c9ecd57575d407b36b3feee0932e2e3bdfa0c7fd625e3ffba75c185e63cd05f5b60eff12a2fd716c5b5d557c1da7fc438afee7d9586879caa6414f58b8644fd620d43392ea28cf43e1fd60f7235ed7e4bf2c93fa17937dfbb324b829c73fbf6ddffd6cc2eb4b12a7cb079159fffb1467acfd46063fdee90fb3191562c6b88146039f6bce6620e87948f5db930a4a663f9984a6a90925fc26b0c6c4e2f25cdadf668338284dd83098d3e75443800ec36478ab52b74278b2dcfd8ae70447b451c7b191bdce5c8639173ba1fd6d66681b01668e003385961520e06558d9bbd46e0946689a2a090f83158ee84b100e0bb9432105e1cd4d0630aa7f3661f4ae740e57211065ace2c77c356a5f35203e0bb7b7eb56521f4da33c5d66111f90abe2497d7b0ef0c46f380c13e0692ad70d481f3794f3c6d764e5bde9994831501388af54bb9f0d3641fb2483127eeda721970fb3c14c9d6c712cdc40f929adb003331a113f2027a32b6382293275c0cef3ab3058890be0f657838543159857583a74bf9abcf2af33d68a7116fadf67331ad4b9437c8c00450d08d82ba9651aa88878f1773f1ae09ca4c411464cfb239e4f1b081e3acee1bf99e0ec2e4a138827fbc564e3265c3e888392fa798a459697508eb03fd75c72b586f55590c073b8145151c22f57ca162caa9f4ddcb88be02bc887fc6cedf34a5016acbef1f0aa7e9a8d0d53a721f361c31435206c98927a68db536520e4d8b4bf4d6dc19acc1752a6fd27031b9685a515e0379edb1f8dc2a664029fee47832bec5112e5f73ca3fed76102b948d1b0259b789f6edf0a1144d92f788e7dae425e85ec1783798d6c347c8ed6e179721d2ed3848ff30a7d9f972d9266faf3668be01ab0b648462df7f77d0548f265d53c3e0804906b8e59c30734b03772fc7790adecbc113a5f7bc5c56d77a9e261b3bd240b945f33b40dc12f8b9b567355474f9373955de5a9b96a2ff4d8ef223e87e3d6f27b0ec70db3fbdec3714b75f0d736e07db5faa761fcabe9bd8b11e220236b33f33ad49ce46748d230e0f601c0870cad4bc77e1683ec0a5f4df65e4d7db129b15a27281f109ea4025b40c2b4c33a8aa60ef044ca3b89b7791166d54681cdab7f83d773fa9a10fa447119768ba4698270e7adffd96429b05a65d52d121b91fe9ee645c3c0ca39fcbd7a26cf5d6625dc16f51713e7e032cd24bcea2f9edd80fb6d719afe0d6c3cb35fcc36d9772d43514517829686fbf4a49ca376a48d62afdc6d95b99db2b4309279a8f44192aaf7fc5c447d30e6d7d92d8065f76dca23ae5d1ccf3e367e3f46eba86e4048a6364e43e25783d6dda7b569bd0a835c78b3237ec573feb25995bd245ddd72ed65bfccc602556782836efe2a6ac01ea69b6efefadb6af57a2c2f5907b19f4cecc2ec8f3965b998bdc41461732adb0414d171906e50f197778cf4d6dea66458743fe2f99cad8388cf485bff64c8a3f4b86fa36ccd1ff8f1df0c8e3a823cff3bcd56bf57774f9931c87c31b9eab4dc66446f45b0def0f79d984f06adbc4f4ba7a13a43e41a497fb0e22791285cc2e0e0e7eff45db0ac12dc030fb2c4af06736dba7c2867bd2629f39762c94db6e2670bde409bf96fa6ef18de95100d57e07e95f0d96016cfa154d8fdafb3b1af9c053b898368b0bd2ba82e77a32b61338ce52515f21cdadf4c764dca268b6f03a89f0d79fd430ce94eff3e1bb411517d4cefa26424885575b9434cc2466eaeeec85b8cf4315af11303f7c9e8d16145f3813fb5663e0c05e47918a7ab341e2ea7a258953b6e602ec3da1652a7b06f90cadea54ea4d8e6a2dba0d71795752074a9a1971f3dc36966d5796565dcda6b86b2eb87a66bc88db876dc18eed2d70b4dcb5dfa86d8fd0a71c3c8c325e28ed300d78877f9525f0b318b41ea10fe9aadd52500b6bafd72a5590f39efb7edbd0d96d9dcb79f4d48407286ad0388166918267e5136a129b82e17c3d2d0615e43e7686b43a806cd49be5d5491894fe3ba9fa7b8b74b0e549a2787501406e0f398473953a1334e6f827884e7d04245365ea59e87d41c7b7ccceaefe1e7d98a0887e7d4ff6a76e2c79f79c94fbbe4304eeeb68098e8dfe7fce877ea7850ebf4a49c7e0635f05d2d8ee65dcb476d904f80e997fd328589875a65de9eca544bcefbf96fb3c1e048fb3bee7b3b66fb3a6e3bcee229c2242708edc7ee6e1d5fa4abbdf9a5fb3b6f7f20022dcd66156b38efe9ae97f7e13aa87a9b6f826503f777519617448737411ed645f6f7dabdce52069bbbfcafb8ffe52248a2db302f3ea70f61f266ffd7839f0e5eeeef1dc7519093692dbedddffbba8e93fcb7e5362fd275902469fd7ae6cdfe7d516c7e3b3cccab3af28375b4ccd23cbd2d0e96e9fa3058a5872f5f1cbd3a3c3a3a0c57eb439ebc618be2f2e2d7964b9eaf621a3f14c6197b02d9b5d71f427e5df5ba85cc55782bd8a3434ec73cf96ba92523cd79b3bfdd46e5bff97eff765e7a0a5fdfecffaf8aeab7bdf3ff58b4843fec7dca4a8dfcb6f762ef7fefeffdb18d637232f766ff36887301b770ed35c2ebfa1f836c791f64fb7be5b2f36398dc15f76ff67ffe91665c0e793ddf2c5c47db7573fcd8f22ea21273f5a1af19bbfe4dd7825adfbfd98f92e247e33e53cce8b7627e38fe19860f20d75f8cb912566fd3649b2ffa80060a391af2fc232da2db6fbeb992969ef4b1a6ab9ebf34e647c79d7210201372ca818f7861cf5560e2eace95e3c936cbca29887e5068684f040e32c38219b0d5747219644522da36334ec28b4107450aaf05bdf0eade097ae146bd0ff4c2af791be88557fd30d00b2bfe2da017a6fcf33f3453da5543f809ccd32af0ed8389d7a07c5b8675203826f0784389b3cd19e8301f4eebd3006feae8be209a2170706a4f6dec7bf550079386ed9272c29b6a34d27d615cfbc2518f708d42fd8c6c40ae8eddad3c22c7fe763ce80e572be5f761a910126fe632288a304bc83a37acd72fbb3e603c58a04a6e97c1f26118b0d476aa8f0961bf70ec79d82085a57630fad35a6cd16bb55b36d41eec55903c38f12166d699091d3064283fe41d596582995270e093bd61d003afa7dc61d011e159d4de123ad5debcad1a601e6bae63f489e75da6333837821e1f2d9d033ababc6bf42044a8a7a15bb029cc9c94c56743339c1d352d3ab2deb79b6ae8acd35574dbeccd2f8e16457585dac15672fc1eeb5bd435c3db380d2c16850ccb979e9bf8d27f135f796ee22bff4dfcd173137ff4dfc49f3c37f1278b261a5beacf41761752f6fad94e3fdb693f769a4796453b4416062d321e0aaeee8a34572b50569de214e10243b8f708584356e7f97169aba292a0657393a6b1f9dac7f978ed34bc0db6715165d376eb926c5988f2d175a71838fda439c92beabc1d51f139cfd9b328847ed00348974519377c14a932f5e381211e72bbca75c201922f3b81044ce66c39f5a8da6633ffb0561f2daa9a6c4653b3ba417ac9a007922a65336e10c963baeac790a78d3021c3b28b3967d32bfbe0d466561e6a5f569b47781a6b68771ad7661c36f4db1a3aa7316be9321abb89925ed7798d8702893acf2ff2b0074eee8b38e169091d9041fedbdd1b09bf16a6172ba86cc00efe229d06d881cdb4eb242665b0e9b5999e768895d1a53a172ff29e812af52ee286014beeb2e3dfe7ec35d5734bb9a316ad4d0fecdf9ce912e73eed0bac7d2656c3da5b42a7dafd5dcc6cdf8f0d850f7b64c872ddea9101ebc674c8f6f96a1de6ba3e57ad23132641adabd285e4b42efb3f5c525af49933b4569bf0faadc1cca8ce738a3d0297bce6c39c8177a42edbe76c2e54432bc6507b7809308e1977d0b1988f74b7f4ec62803c199d2e6fa983bfdebf5c35444b4be884553ac9a961fd3de91078952612c5815412a6540fd18ed0501a0d9ddbf6671be7dbd474d5742eaf0cf8a4800ed3271bead9e548a7cf63ea3a52fd7997fedf9b50214e1ca44e053871d25d1543d7ffa98a34a72672ce8123e023661c6848996e0f356f980d47654d369341c9e5e77439806b7373bae2be4bcee9caa84fcee9a14946570901ac3069391d18b1d9380da14713bb00108af2e57f592dcd71b913576f079ab467b7d2106bb704a71a9656572bcc7d0b2663a6c3301513653a3303afbbdb32f3740cfafc74f4f9e92896a9f5d35180699d13d261140cf638009522d1f8a19be31b37d7137287d7814a6e7c1c037366f0548ddad6ab0987d8a3906425c4691dce45a8573734ebced665418b529b10102754380da05ea8d012cf745dd4a7f5323df76e295d2f7a5039ffecb746d97c7f2ea69749e1e7ba34db85c8215cb63ed37b4634b5cbaa6db0394f99090f79134c96fc0e710f0c1e60c6b68fca9757f3f113a789cf9ae793b7b72dd55d1845ee9b30627a3c2733c667c47360c626c2f33f42a519e39ef6d595c90f6decdd539ada69ab6ea89d7b65a238e4933a0b40399af92ea59cc3eab349282737e42f7ffad9544d7c5e39878363369d9cbdc3c7e590b367c4a48ef3c2c68fa0c4dc700ecc806c708a090fd35f3e0d9c43e380c46f2e93552e8967a9b36b2d61f5f30f7be46273f4d7b6fcf0b91407316df231e4d15201d9d5c63057c3bcd46ab329194e2f3599db8dee3eb59bfd98a653ba61b9d8ea1d487986bc9ea9ce7006df0697673003cac339b110d73fd996c967a4a397e6d31cd309bfacc742ad2d54c468d838a4c8729b0145e104667a954f37e0db169b5e074b0ddf14a095a648678634e8d2046608c32e8931e8dbc0d3d973e87a1092ef491d17321bc71d7f26b3990f3eaeb79946b9be80c9da85c3a93c83921ea732fceccc6534c347879a044938814b7322e9e54d913a2c53277ef123265d9a7a3b103f29a8b311edc855f6692f7ff7a98e0c6b6f095d4f298c364121b0f589919ca69b49de13181abc4b55ca2093ed5d38250f768bf7c4fda414367aa6cb52f776f0d98406529f32fbce6c3750ad367e48ff26d8f8d165b39ccefb1aca7bc747336a962f9f6e9be1dbbeb19f3a34de106fc5ada5d284517916492f9226decdb3488848481d240adbb3383a715471659ee5418d980f51f15dcba30fdfb2eba127bfcf90ab7887b69a1a9eb5fcb4b55cfb44cf5a7eda5a26a1089f75fcb4755cc50f7ed6f193d671e97c3eabf869abb88e43faace4a7a4e4e33c4f9751556db7292d260ba493c2714a3f4b56d5aeab24cb60d3519246fc002e70b18d8b886c5097ad7bb37f24e4b0ff949c867158847bc7cb3ae9f849902f839528f1b2872b44db3af04a9ad67f675bf64f428525504392a42c0ac8855a72061f8989e32fb32859469b205649892302078336f9221141571bffe534dc9003bba45008c45b2bbaca3805e904f6fa9082a31aa5f2e4ad3456c1bdf8dd40ac223b2ddd4255b1a78c5f6cf2dedd85b124412c1ed2b686eec5c1c19180959d02a6ad459364791d13988a04c31302b4061d6d593f46b9dc13a0890050b41fbe6b3bca8802830cd9d3b831d1a96ac36860e453f52ee4a29c35268594c362abc4224f0e8beac4cb73c66183be2653b132d9cc00aa47cdd28670f70a272bc5aa52685b80cba0664e8d13638bce06bd50a4869eb37d639a2db688fdfce4ec9a5c6992bafb1cd673c1dd8eceabcfb833c2ddc473a998ed70a1d862a616ba509a5166a50b16180587400647ba65d0e76196c4da54ac124c4837e92db6e98d30accb7d2969017c85c9bab923c09e75172bc82b97d49c7b49c389ff34ce86ba8fe1e709e62ad75b8201b59b87df45b71d6093bb9980d9ad07bdd6ee0a87808cd6c5af3c1ce56b96d9984df539a744a5bb6633cdda0b26c69d04b9ed98e3b39963f13b63bf41d635b07d62a1671f422dc5295a3de5806893a36a46c49c5670731c01d3ace69c503cf19a4e8a48fe07dd6166c3a727505a6ca8d8778b5a85505c5654632198279afa845e4c435ea7e54622d80b32863d210512ad0307a450a941f16b851e0fe845a69f9734a04bf53e33cc3e21a33b6fd84e6776dd803ba9ad950baf026ee59b23d13be7e59db791f5dd2ff12c47d90e2ef2982b07e7c963c9b56c9276db788e6bbbb9c17fdaf59d2584677103a6c361d9bedb280e17dd0b6c8d9596070561367ce5a5f09bca704b81b1201619046dc8782812b57bf30f84dea22c61f7c07e32dcd5adeddd58b3c3e2d9fbb47cf7589c8a5f9faa0f2bf474f6beab88cce617d4fd190ff6e94943d2c16895aa0a81b47623e0b1a59ad924bda3d7bae633854f79bdcb6a084cec2c7e0c83bb362950631beb9fce121504db320204d90fa34050ec02d022f6f320d063ba8e517d4de0083da07bae758fbd64d67561a6a6cf1977e033811db27a96d09b7a799c91b8ae21151cfa342882c555b80ca3c79d0220dc13ce07848b3ca5b917ee22a6fe56e773c3621dbffb1989df0f12fb50ef33c1e142da8321d43e1a2c356d1b0a8ef6c09026217044a649e5a4b17382e6ee2d92e782c39998c55d7013a9dcab746c855c0a388a80d12ef3fb2870a36a645a2e69155b6610e0d132c0199e3663ae03ee645d746dc0c8e0db396b67311076dabe99827b0e868d24c558d46d56de51e80a8b9aac7f7de288ea7b8ad12c29383298e4558e05a566130f0b26c04631bf7f0f8032b05055f99131a5a87304509dc4dbbce89fc62cfe4857a112561c01a350e1db28f06a6a252d57b58afd3e08dcf8fe63d4dfd2b8810eeaa373ede3c18f6ef7a24de7a8bbca6261e034a182e683a4290c972d86aadc6de1b4668c9e1bff90a51ef3aa680d942cc0e9d70819abf10f30df9af9d467805a7995234386f875bbe44ce1d7083b6c8fcc5603f3b13f7f46491266a4e11a2b64a8cad95a9c69966d0668ec35321940fe7d1bb6b75516d2e60feadc502d603832bf3f19e341f76afee6a36a6db303295d3fbaada831e8003832bf3f9ddd6aba5b68784cbc4d4d8364e7b6a92df0b5eb17b64c3136f14e350d2fa9f5749b4fe6832d4fb6cb7496a9ca8f0c2b459d23fb3dbb69b4cc20fe248c961f448ff14c7e43045ffdf2252fedd6491c446bf9a501ae38fbe28dff36ce4b60b6d6aafdaa6635058679ecc64900a37ed7a7bf50ef30f592d2b341ddc7f42e4a76187555fb55cd6a0a3c51d4d5bddb39d4a1665327ece99e921b4ed2fea6c60930e375369e002da4173b6ca2c88faa56d5df9fa881aa3a377ffbd4bc957f5ffe75d11dc12d2e828d0277200d1889802f320a0aab4a830dd02ceecba0d107b8ae6390d092380291eda57bc5a3bd7524cd68ee11eceae253d211a05940992774e153d6c71d9884dfc7e94dc9857a4c71b68a8a7410146af6f785a6b0d64cfcfa841024f60e5375adaa196167e7de293e63ce1473133f4ca4fc4ed28b05f127518b06957b0eb9e6dfd3a201edbd4fb468200527431cb993c12d1888be7ab68b7759ba5e48d6169fd385c8608cc5867c9e05da4357007d1e6c9764f2e5834e3956b58fb686e042382a8109c4e92dc1898c70386c783e39563dc79ef48fdce9c2ee0d159b7234f0fe19069b6eedeb8edc9add3c61dbb44d85d9b6c8f703584861f345eb71b64eb7feec6ccd6e9e686ddaa6426b5be4fb412ba4b0f9a29534e1fa21c2585731ef0f8dd6969116a99e92a66050dab5094668ff7968744e95ee44a79e79e3924ac5e308cc8ad3dc9059374a0ecde6fbf7814d4043f305e787a8f036bf97bce639b99386a966f6eafbf733ad0b7a9a233cfb50fe0cea1420e5e10972d002d4312f8fe21533d81ce6792b5c6268604e9e9d06ff145aafd1919a3cc6ae7fe57c5ba35f249f0efa405b18f6c0e767d01772e13d51c4d79b23d68817c9a7433cd0169a3df4f919f1855c784f14f1c7eb756a6fe139e209ed3bdf1286b9f0f119e9854c704f14e7ed968d15ce2bc259e05c6809cd5cfcf88cf34226b8278af372656d6dce39dae950ce3784e62d7c7bc6782111db138538ad983a89b895ef5251ce02ee625318eee2d767c81752d13d09d0d71be2254d51528459db90b2d9efa22c2f4826809b2017214ba8aec382d95f6f6f93d5ff2bbdf87bbdbc0fd7c19bfd4df52b89521edc88f786050c43355261bebb2e48eb870bab5b43d1689b846c8cba19011bbfbd6e0b4c826f4e3f9e55ade94b611bd353a0b475156eb64585400d52c4826a2d89e551ed799745e560d2b4852d246b075b4aa719c5c18fa8204561596bf84390fac013d92af1a454da26b1a8be45358d518b9012826593176916f6e005cb6adad14dffaa610d1592b4a19d49f063f91d69b36adcf005e09af952381b4b9a7b5c14a531087583052cabc744f5af380e57cdd939aa5d627e5149a3c4826a6bc2a60ad636464cba2bb6432c236d825854d30028eda0d002a850db84b8fad6b5002a8a6a82a67254b568a18b097400a98b853ab16f6f4aa79692ba58d4b809759e2b4433ea82e8a6d4c535cd61e201096d60be4a2a66ca686a6342400ab5315fdbdad6e4c7b0073a5308535d1b450baeaefdaaacae2d84af8e4d95a3aa9a2d296d065016d39a3a182b5c7ffd4dd9f1ba88a62230e2b45025584a0229aa2c52d342f87e59fdd8badb72c8fa99786df030e291060d2324d084b816a26fc397686b7da4e68cfa8d9c61654d18275d8d4d3159b54db42bc3ba9b603ebaba9b62b2ba9b98478675d72fee7455d7a56435d7914c741573f119842ab9ef6d6577e5cfeb74d5cf8c5c39a4d3cc3ff192bacd7c41693b6404286f0d08102071d7809292a10694d4a9447cf02baa452c23ab5f2c899245e9e3add313956e24e5b47e6b5ddc6448e887837c28e08601f4de50a80f2a2405225458b784e356c4d40b327125272f8b5d705324860d6b9f09695bd516c436a92d6fd89ef62188b63d6d416c7bdaf2e8f6f4d7fe156de90b21569d5d59a336346b544d239a52b856e096bde04d5ead5eaa5258a55485752e1a7c735374d4e072f0ce84a4b0ce8e0197e944530614926ccc0025352d802e37092d800ac12d804aea64205c391125201491f45f28a7a95bbc0620d42d1681eb16cb69ea164e6785aa851270cd42319dc0c5533251e26219cd5ea0b47aeadc467dd4b060ce2a2832c5b90343c31f3651c7868a938db251b8030a19475e8c3c43a9640e59d120c4263f07a205d1ba14a2088de8c714a7fe34ace2ab2a36a4703f06790168d44ad06a5e83226ea622af6ba31158e591928a55525ed755d677a73a07bbe03315167f54c806e593ca4c4d36bce86447a7142fed21a823be4eb619b961c09f21695006538dd05d7735580b8c3e0c5eb027c33269c94986c716740c4ef1511e6d7b11116e08ca499e9a88c4a3e405eb228a22d29128e645c5217b3d31228ecc198ef2e3f28a1ff4d95964ac7da9fa2e9b046545e51d824fdfabcea84fd4fd0bda0f9aea5b696670626914dd942d8fea3eead63b9323491a9d402b3424e598435177a787e12a161a4e98edcd0c0b6972a4431affd98a8fff413e779ab2d00b437e6b8d1107546cc6a296de5f5a50e14c144b550de1481d87165df2eb5bf49a0b2a35a050d5a045d14d85d4e9c5aa885442fea8a61bbd4540504f315579539aeb7ab7bb36a872263514634c4ed30b8cbf60b9e0c296ca05a7a554f886ba13ddda45c41ecc02ca915d30a5d424167117297f0154bbf6d3504c66276517636bf969efb9fa101d779517233b9664048c4c2d347820e2f664d464c3efcb4c3344c51bd28bf6f6b2445a1a0a7937a19bd555e754d7a5010ed0f5708a8ff2b6b7e33c0ad5a09948019221b1348988c06bde8bab7019468f4a41a108071d7aca9bef8da1425d62f72636262f095e68f274264f5a640ba6028cb4188a11baa9e1aae1388cd0b433a286645c7c0d2f22eaed037d969c43b2919695770978f351f545f19043460f3de2e079299f64b80947071c69d9211163215e5b3190ebba8b9aa56c190d96d3349e7e12d3371d7ae93255c71b6f06d175b1a4a60380ea150faa261000f7fc6641de00c944202d2bef84e49d50d511e19b42188a374f342ff6bb2fe1d05c17cd4306d5421f43361c6ea611153d3ce87764ea8144951cb443ae02761408317618bbd2951bd8aae04db263c7ff8c9224cc08631c1e84f203ce2d8323807aa8bb60388b3290151d0e07c03be48a5ef5bad852048dcbc4cc5b1211004587f33c8137a4bd08bc4db074bf741ea6b4ec901ee6f8626030a696025dd42b98a71241bb7b8d4102547670240c6e13b817bc8bf6693270020a97541c94c0efb4ebd311fe9beac84ef1041b62d614f02e9af6e5b45e344dc9d145c3bc1087983505bc8b46378094e58717d3581799f96e364f7df5f2a80b8e8e17fa1933c4abfeeeede6059704f622d8c0c25114d71fcbc28fa6992359f52b68862b188da062a68e2f607bfa233cce471e296ae8863f5394462aa098e9230f988b4dccb65ea77f970b4c4331a4a8a481146a44690323f8108feed0c7247dfdce8b07ce9bae36d8544194a1551959c8c0cec0600f91dc1b10eae039c4479820e4713a2a8e88a01bb653c59059aea5f3cc68a9b541d50d73e5caf39d386b850e93f959aaca11124d4fa84436220ca841496c17eb0bdb83a44296dff31e3ef3f284ea6303e880ea9384c2b1becb57f8ce0d2cbfffe7bd2abddafc3f9490c718e2d4258d18e4a22adfe97295ca1a3437ef24ea62c23101fa82632bd92acc770a59a9b206cd553ba139a4825681b6108a406571c03e504255e8507e8cdcada0c2fcbdbd5186f4aacfc251f1b92c96adde137f424bde61b38b0eac1c7990b39a9f3e6899b95afc67a704d432700acc81d5228ffc56f1434472b3182d5e5328422365b81c8d438f124918bc9a9b2eb09d5d54057f59fe242118064a2338b02a6451012b6eda387fe6aaf099880e50c46079ee065683244062c54c17f3d0fe64ac73b9ed4c88181e447eaae6bbaa51cd955b941485825e1fd68cba9828ddb7d787750ccae687f2cf22cd82bb6a4737ceab5f5f1f5e6d4bea7558ff751ae6d15dcfe275c93309ab78373dd3b6cc79729bb619cdb816b545dacfed85beb0085641111c6745744b561659ba24676ac9ddfede3f82785b16395bdf84abf3e4d3b6d86c8bb2cbe1fa2666f69d49463455fdaf0f8536bffeb4217fe53eba5036332abb107e4ade6ea378d5b5fb5d10e79cd2642c48c0eef761f97badcb12464578f7ade3f4479a201935e2eb32c47d0ed79bb864967f4aae83c7d0a66d5ff2f06378172cc9dbc9c768457688654cf48a60c5fefa340aeeb2609d373c7afaf2cf12c3abf5d77ff9ff4133d23985100400', '6.1.3-40302');


SET search_path = gamemode, pg_catalog;

--
-- TOC entry 2852 (class 0 OID 227300)
-- Dependencies: 227
-- Data for Name: AbstractGameModeEntity; Type: TABLE DATA; Schema: gamemode; Owner: postgres
--

INSERT INTO "AbstractGameModeEntity" ("GameModeId", "Level_Min", "Level_Max") VALUES (1, 0, 0);
INSERT INTO "AbstractGameModeEntity" ("GameModeId", "Level_Min", "Level_Max") VALUES (2, 0, 0);
INSERT INTO "AbstractGameModeEntity" ("GameModeId", "Level_Min", "Level_Max") VALUES (4, 0, 0);
INSERT INTO "AbstractGameModeEntity" ("GameModeId", "Level_Min", "Level_Max") VALUES (8, 0, 0);


--
-- TOC entry 2851 (class 0 OID 227288)
-- Dependencies: 226
-- Data for Name: GameMapEntity; Type: TABLE DATA; Schema: gamemode; Owner: postgres
--

INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (1, 1, 2, 16, 0, 600);
INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (2, 1, 3, 16, 0, 600);
INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (3, 2, 2, 16, 0, 600);
INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (4, 2, 3, 16, 0, 600);
INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (5, 4, 2, 16, 0, 600);
INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (6, 4, 3, 16, 0, 600);
INSERT INTO "GameMapEntity" ("InstanceId", "GameModeId", "MapId", "Members_Min", "Members_Max", "RoundTimeInSeconds") VALUES (7, 8, 5, 16, 0, 600);


--
-- TOC entry 2888 (class 0 OID 0)
-- Dependencies: 225
-- Name: GameMapEntity_InstanceId_seq; Type: SEQUENCE SET; Schema: gamemode; Owner: postgres
--

SELECT pg_catalog.setval('"GameMapEntity_InstanceId_seq"', 7, true);


--
-- TOC entry 2857 (class 0 OID 227350)
-- Dependencies: 232
-- Data for Name: TeamGameModeEntity; Type: TABLE DATA; Schema: gamemode; Owner: postgres
--

INSERT INTO "TeamGameModeEntity" ("GameModeId") VALUES (1);
INSERT INTO "TeamGameModeEntity" ("GameModeId") VALUES (2);
INSERT INTO "TeamGameModeEntity" ("GameModeId") VALUES (4);
INSERT INTO "TeamGameModeEntity" ("GameModeId") VALUES (8);


SET search_path = league, pg_catalog;

--
-- TOC entry 2836 (class 0 OID 227083)
-- Dependencies: 211
-- Data for Name: LeagueEntity; Type: TABLE DATA; Schema: league; Owner: postgres
--



--
-- TOC entry 2835 (class 0 OID 227072)
-- Dependencies: 210
-- Data for Name: LeagueMemberEntity; Type: TABLE DATA; Schema: league; Owner: postgres
--



SET search_path = matches, pg_catalog;

--
-- TOC entry 2839 (class 0 OID 227149)
-- Dependencies: 214
-- Data for Name: MatchEntity; Type: TABLE DATA; Schema: matches; Owner: postgres
--

INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('d6ea8547-d8f2-45d6-aba8-cff22ed03f10', '7fe1702a-aa5a-4520-8d8b-57977840eeba', 1, 2, '2016-07-19 12:01:28.104611', '2016-07-19 12:01:32.245092', '0001-01-01 00:00:00', 15, NULL, 600, 2);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('a3d3504a-1c73-45bb-b10b-33d236bd6d00', '8e4d65eb-7ae5-4506-8f5e-6a9958d7a3ad', 1, 2, '2016-07-19 12:57:19.245991', '2016-07-19 12:57:23.11619', '0001-01-01 00:00:00', 15, NULL, 600, 2);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('75be812b-de2a-4b0b-8886-931d89856559', '45aa77e6-9c1e-4d99-b4b7-aaef2ca45142', 1, 3, '2016-07-20 10:32:24.012251', '2016-07-20 10:32:54.97819', '0001-01-01 00:00:00', 15, NULL, 600, 2);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('77a97bdf-12ba-4563-9e76-a350773bc356', '91fd5279-ea2a-43d3-9945-ddd1e67b8182', 1, 3, '2016-07-20 10:58:59.520053', '2016-07-20 11:00:28.314556', '0001-01-01 00:00:00', 15, NULL, 600, 2);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('35a7ddb7-22d1-42f1-9c2f-b96b526285b7', '761b03f3-90d0-40d8-ab8f-4420d71ee251', 1, 2, '2016-07-20 11:25:37.17335', '0001-01-01 00:00:00', '0001-01-01 00:00:00', 15, NULL, 600, 0);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('241ed45e-fb74-496f-a6d1-a92fdc5daf16', '761b03f3-90d0-40d8-ab8f-4420d71ee251', 1, 3, '2016-07-20 11:25:24.960936', '2016-07-20 11:26:03.669691', '0001-01-01 00:00:00', 15, NULL, 600, 2);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('bc339326-3286-42f2-ae77-0744a0f2a7b2', 'fc5fd786-94eb-414a-ad21-9799b99dea35', 1, 2, '2016-07-20 11:28:40.271412', '2016-07-20 11:32:53.902131', '0001-01-01 00:00:00', 15, NULL, 600, 2);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('71f89982-fed6-49d7-a99a-5a37a8e3fd19', 'a74c644f-fbd0-4625-a01f-9b53dc57d632', 1, 3, '2016-07-20 11:34:05.870255', '0001-01-01 00:00:00', '0001-01-01 00:00:00', 15, NULL, 600, 2);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('dcb4376f-6a31-410b-9514-1774269ddcc2', '81379b9d-87e1-4e18-87a2-805251520d5c', 1, 3, '2016-07-20 11:35:02.573312', '2016-07-20 11:35:57.780695', '0001-01-01 00:00:00', 15, NULL, 600, 2);
INSERT INTO "MatchEntity" ("MatchId", "NodeId", "GameModeTypeId", "GameMapTypeId", "Created", "Started", "Finished", "NumberOfBots", "WinnerTeamId", "RoundTimeInSeconds", "State") VALUES ('d7502a24-c17c-4904-96ea-9a2f15dccd99', '65fced91-770c-4395-9249-187f96a540bd', 1, 2, '2016-07-20 11:59:08.430404', '2016-07-20 12:13:20.613008', '0001-01-01 00:00:00', 15, NULL, 600, 2);


--
-- TOC entry 2840 (class 0 OID 227164)
-- Dependencies: 215
-- Data for Name: MatchMember; Type: TABLE DATA; Schema: matches; Owner: postgres
--

INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('5ac81d00-f24b-4a5f-958f-7cac89364827', 'd6ea8547-d8f2-45d6-aba8-cff22ed03f10', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 'ae9f71b5-5fa0-457e-9b70-1d34c4767d2a', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('540daf6f-b824-4695-bf85-8f8c32fb7a8c', 'a3d3504a-1c73-45bb-b10b-33d236bd6d00', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '71d46bf9-9440-4597-b755-d9b52514338d', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('8ec2d3b3-0040-4e16-8884-cb443267a71f', '75be812b-de2a-4b0b-8886-931d89856559', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '3157d758-fbda-4410-a54a-98dfbce38a85', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('03dcf6cf-e697-4a18-9233-ab8b5e28d11e', '77a97bdf-12ba-4563-9e76-a350773bc356', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 'ec6591c5-da75-4fd7-ac74-31d96fb9a55c', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('1aaea6df-c96f-4c9c-9616-ac6798cf446a', '35a7ddb7-22d1-42f1-9c2f-b96b526285b7', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '6333a521-47e5-40f2-95af-d3f6d2ae6b0a', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('8bef7fab-0627-42d7-9ca1-b332b4f4e281', '241ed45e-fb74-496f-a6d1-a92fdc5daf16', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '43ab2b00-9831-4750-a76e-ccde2f3e585c', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('6420829f-6142-463d-bc8f-c2a6a66c5f05', 'bc339326-3286-42f2-ae77-0744a0f2a7b2', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '5dbb793b-fe4b-4111-822b-f45ba271a7af', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('0a5accc8-997d-458b-8ad9-7990deab0dc4', '71f89982-fed6-49d7-a99a-5a37a8e3fd19', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '25d06281-d4d2-431e-8850-c1d2aabfec87', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('4a949abe-3f75-4911-a9e3-ea5512e0585e', 'dcb4376f-6a31-410b-9514-1774269ddcc2', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '31f29312-7b92-49e8-a1fb-2698db931106', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "MatchMember" ("MemberId", "MatchId", "PlayerId", "TeamId", "Level", "Award_Money", "Award_Experience", "Award_Reputation", "Award_FractionId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage", "Squad", "State") VALUES ('7a26ba64-5954-4370-be35-b7cc6f3afe5c', 'd7502a24-c17c-4904-96ea-9a2f15dccd99', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '0a9e7edc-f2b3-4c29-b07a-474af7960f09', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);


--
-- TOC entry 2842 (class 0 OID 227200)
-- Dependencies: 217
-- Data for Name: MatchTeam; Type: TABLE DATA; Schema: matches; Owner: postgres
--

INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('48a0f3ff-48c3-4b35-ae8a-1bd77349a39f', 'd6ea8547-d8f2-45d6-aba8-cff22ed03f10');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('ae9f71b5-5fa0-457e-9b70-1d34c4767d2a', 'd6ea8547-d8f2-45d6-aba8-cff22ed03f10');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('71d46bf9-9440-4597-b755-d9b52514338d', 'a3d3504a-1c73-45bb-b10b-33d236bd6d00');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('e7807ffc-1f92-4151-9028-5ef8a204b0a2', 'a3d3504a-1c73-45bb-b10b-33d236bd6d00');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('3157d758-fbda-4410-a54a-98dfbce38a85', '75be812b-de2a-4b0b-8886-931d89856559');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('36772f32-6ffd-4a55-9e0d-2797ee4ee781', '75be812b-de2a-4b0b-8886-931d89856559');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('ec6591c5-da75-4fd7-ac74-31d96fb9a55c', '77a97bdf-12ba-4563-9e76-a350773bc356');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('f95b9d4b-7293-42d5-8e87-784e0d4b08ea', '77a97bdf-12ba-4563-9e76-a350773bc356');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('4b19072e-fdb1-43ae-92a2-6711418dff39', '35a7ddb7-22d1-42f1-9c2f-b96b526285b7');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('6333a521-47e5-40f2-95af-d3f6d2ae6b0a', '35a7ddb7-22d1-42f1-9c2f-b96b526285b7');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('43ab2b00-9831-4750-a76e-ccde2f3e585c', '241ed45e-fb74-496f-a6d1-a92fdc5daf16');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('f4264648-a0d8-47d3-91ef-6a639f2ba43a', '241ed45e-fb74-496f-a6d1-a92fdc5daf16');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('1d7bf528-8855-4083-be9b-96ae6f8c2dee', 'bc339326-3286-42f2-ae77-0744a0f2a7b2');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('5dbb793b-fe4b-4111-822b-f45ba271a7af', 'bc339326-3286-42f2-ae77-0744a0f2a7b2');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('25d06281-d4d2-431e-8850-c1d2aabfec87', '71f89982-fed6-49d7-a99a-5a37a8e3fd19');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('372a0aa0-9a78-4e52-90e4-37e24bad69c4', '71f89982-fed6-49d7-a99a-5a37a8e3fd19');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('31f29312-7b92-49e8-a1fb-2698db931106', 'dcb4376f-6a31-410b-9514-1774269ddcc2');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('45caf685-57a8-474b-9b82-ac00d39f1398', 'dcb4376f-6a31-410b-9514-1774269ddcc2');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('0a9e7edc-f2b3-4c29-b07a-474af7960f09', 'd7502a24-c17c-4904-96ea-9a2f15dccd99');
INSERT INTO "MatchTeam" ("TeamId", "MatchId") VALUES ('9cd983de-eb67-41fa-b1fa-d803915038e3', 'd7502a24-c17c-4904-96ea-9a2f15dccd99');


--
-- TOC entry 2841 (class 0 OID 227190)
-- Dependencies: 216
-- Data for Name: MemberAchievements; Type: TABLE DATA; Schema: matches; Owner: postgres
--



SET search_path = players, pg_catalog;

--
-- TOC entry 2825 (class 0 OID 226953)
-- Dependencies: 200
-- Data for Name: FriendEntity; Type: TABLE DATA; Schema: players; Owner: postgres
--



--
-- TOC entry 2832 (class 0 OID 227035)
-- Dependencies: 207
-- Data for Name: InstanceOfInstalledAddon; Type: TABLE DATA; Schema: players; Owner: postgres
--



--
-- TOC entry 2862 (class 0 OID 227385)
-- Dependencies: 237
-- Data for Name: InstanceOfItemAddon; Type: TABLE DATA; Schema: players; Owner: postgres
--



--
-- TOC entry 2861 (class 0 OID 227378)
-- Dependencies: 236
-- Data for Name: InstanceOfItemSkin; Type: TABLE DATA; Schema: players; Owner: postgres
--

INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('1a731078-7d4f-445c-8716-5be57fa83b8a');
INSERT INTO "InstanceOfItemSkin" ("ItemId") VALUES ('787b9880-a67b-4f68-955c-988522a3e3cd');


--
-- TOC entry 2860 (class 0 OID 227371)
-- Dependencies: 235
-- Data for Name: InstanceOfPlayerArmour; Type: TABLE DATA; Schema: players; Owner: postgres
--



--
-- TOC entry 2858 (class 0 OID 227357)
-- Dependencies: 233
-- Data for Name: InstanceOfPlayerCharacter; Type: TABLE DATA; Schema: players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerCharacter" ("ItemId") VALUES ('2d445da0-03e0-4eeb-bb2a-35fcd47856c7');


--
-- TOC entry 2826 (class 0 OID 226964)
-- Dependencies: 201
-- Data for Name: InstanceOfPlayerItem; Type: TABLE DATA; Schema: players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerItem" ("ItemId", "ModelId", "CategoryTypeId", "PlayerId", modification_1_type, modification_1_value, modification_2_type, modification_2_value, modification_3_type, modification_3_value, modification_4_type, modification_4_value, modification_5_type, modification_5_value) VALUES ('2d445da0-03e0-4eeb-bb2a-35fcd47856c7', 0, 5, '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "ModelId", "CategoryTypeId", "PlayerId", modification_1_type, modification_1_value, modification_2_type, modification_2_value, modification_3_type, modification_3_value, modification_4_type, modification_4_value, modification_5_type, modification_5_value) VALUES ('56e6a899-96f0-4865-9608-257a26947ac0', 0, 0, '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "InstanceOfPlayerItem" ("ItemId", "ModelId", "CategoryTypeId", "PlayerId", modification_1_type, modification_1_value, modification_2_type, modification_2_value, modification_3_type, modification_3_value, modification_4_type, modification_4_value, modification_5_type, modification_5_value) VALUES ('7034d186-7a49-4bfc-9495-6a24881c1082', 24, 0, '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);


--
-- TOC entry 2863 (class 0 OID 227392)
-- Dependencies: 238
-- Data for Name: InstanceOfPlayerKit; Type: TABLE DATA; Schema: players; Owner: postgres
--



--
-- TOC entry 2859 (class 0 OID 227364)
-- Dependencies: 234
-- Data for Name: InstanceOfPlayerWeapon; Type: TABLE DATA; Schema: players; Owner: postgres
--

INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('56e6a899-96f0-4865-9608-257a26947ac0');
INSERT INTO "InstanceOfPlayerWeapon" ("ItemId") VALUES ('7034d186-7a49-4bfc-9495-6a24881c1082');


--
-- TOC entry 2827 (class 0 OID 226985)
-- Dependencies: 202
-- Data for Name: InstanceOfTargetItem; Type: TABLE DATA; Schema: players; Owner: postgres
--

INSERT INTO "InstanceOfTargetItem" ("ItemId", "ModelId", "CategoryTypeId", "PlayerId", "TargetPlayerItemId") VALUES ('1a731078-7d4f-445c-8716-5be57fa83b8a', 0, 7, '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '7034d186-7a49-4bfc-9495-6a24881c1082');
INSERT INTO "InstanceOfTargetItem" ("ItemId", "ModelId", "CategoryTypeId", "PlayerId", "TargetPlayerItemId") VALUES ('787b9880-a67b-4f68-955c-988522a3e3cd', 0, 7, '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '56e6a899-96f0-4865-9608-257a26947ac0');


--
-- TOC entry 2820 (class 0 OID 226911)
-- Dependencies: 195
-- Data for Name: PlayerAchievement; Type: TABLE DATA; Schema: players; Owner: postgres
--



--
-- TOC entry 2819 (class 0 OID 226880)
-- Dependencies: 194
-- Data for Name: PlayerEntity; Type: TABLE DATA; Schema: players; Owner: postgres
--

INSERT INTO "PlayerEntity" ("PlayerId", "PlayerName", "PremiumEndDate", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "WeekBonus_JoinDate", "WeekBonus_NotifyDate", "WeekBonus_JoinCount", "Cash_Money", "Cash_Donate", "RegistrationDate", "LastActivityDate", "CurrentFractionId", "FirstPartnerId", "Statistic_Kills", "Statistic_Deads", "Statistic_Assists", "Statistic_TeamKills", "Statistic_Shots", "Statistic_Hits", "Statistic_IncomingDamage", "Statistic_OutgoingDamage") VALUES ('5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 'SeNtike', '2016-07-22 09:49:13.306075', 0, 0, 0, '2016-07-20 12:13:50.657766', '2016-07-20 07:44:27.22671', 0, 1500, 0, '2016-07-19 09:49:13.306075', '2016-07-19 09:49:13.306075', 'f99106b6-1085-4351-bb4a-1cb6ee38598a', NULL, 0, 0, 0, 0, 0, 0, 0, 0);


--
-- TOC entry 2833 (class 0 OID 227046)
-- Dependencies: 208
-- Data for Name: PlayerProfileEntity; Type: TABLE DATA; Schema: players; Owner: postgres
--

INSERT INTO "PlayerProfileEntity" ("AssetId", "Name", "IsEnabled", "IsUnlocked", "PlayerId", "CharacterId") VALUES ('ca8683ad-1652-45f2-bd3c-34f7cd666f15', NULL, false, false, '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '2d445da0-03e0-4eeb-bb2a-35fcd47856c7');


--
-- TOC entry 2855 (class 0 OID 227333)
-- Dependencies: 230
-- Data for Name: PlayerPromoCode; Type: TABLE DATA; Schema: players; Owner: postgres
--



--
-- TOC entry 2824 (class 0 OID 226941)
-- Dependencies: 199
-- Data for Name: PlayerReputationEntity; Type: TABLE DATA; Schema: players; Owner: postgres
--

INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('21f5ad0a-248f-4762-8861-1656dbbd17be', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 0, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('c36916c9-9bb5-4dbb-87fe-897eb0f93cae', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 1, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('c418bc2c-3bf7-4882-825f-20833598dcc9', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 3, 0, 0, 0);
INSERT INTO "PlayerReputationEntity" ("ReputationId", "PlayerId", "FractionId", "CurrentRank", "LastRank", "Reputation") VALUES ('f99106b6-1085-4351-bb4a-1cb6ee38598a', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 2, 1, 1, 0);


--
-- TOC entry 2834 (class 0 OID 227061)
-- Dependencies: 209
-- Data for Name: ProfileItemEntity; Type: TABLE DATA; Schema: players; Owner: postgres
--

INSERT INTO "ProfileItemEntity" ("ProfileItemId", "ProfileId", "ItemId", "SlotId") VALUES ('88022129-18e7-4c94-857d-8de991057365', 'ca8683ad-1652-45f2-bd3c-34f7cd666f15', '56e6a899-96f0-4865-9608-257a26947ac0', 9);
INSERT INTO "ProfileItemEntity" ("ProfileItemId", "ProfileId", "ItemId", "SlotId") VALUES ('f239210a-f97e-48f8-ab18-0110aa967c9a', 'ca8683ad-1652-45f2-bd3c-34f7cd666f15', '7034d186-7a49-4bfc-9495-6a24881c1082', 10);


SET search_path = public, pg_catalog;

--
-- TOC entry 2844 (class 0 OID 227225)
-- Dependencies: 219
-- Data for Name: ClusterInstanceEntity; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('f03a3deb-f25f-47d4-93de-d44f3eb0a210', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-19 09:32:11.245083', '2016-07-19 09:32:11.245083', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('53ebe1b1-998d-42b0-b169-ddf70596008b', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-19 10:05:46.869971', '2016-07-19 10:05:46.869971', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('f41e9731-b4e6-4871-9660-72304546b446', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-19 10:41:47.894474', '2016-07-19 10:41:47.894474', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('e54d0045-3cd0-4dc4-a182-76667eaa5911', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-19 10:42:01.955313', '2016-07-19 10:42:01.955313', 0, 30, 0, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('a4eee8d8-1d6c-4a94-b28e-4eab91fe371d', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-19 12:00:56.491499', '2016-07-19 12:00:56.491499', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('26298c5a-6d9f-44ec-ba75-d5c7a0efbe3f', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-19 12:45:49.378574', '2016-07-19 12:45:49.378574', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('054ccb63-5359-494b-8cf5-26235f20b48a', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-19 12:51:52.608501', '2016-07-19 12:51:52.608501', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('1e075ccb-634c-4805-aa55-7c23dcc1d1f4', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-20 10:31:54.556472', '2016-07-20 10:31:54.556472', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('f108d25b-b972-4ed9-8704-0488af9f6e04', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-20 10:51:59.766383', '2016-07-20 10:51:59.766383', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('5782541c-96ff-460e-83ae-c1e7098aad82', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-20 11:17:58.274467', '2016-07-20 11:17:58.274467', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('fe43d1e2-13fb-4276-bed0-f6533be23cad', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-20 11:28:10.144932', '2016-07-20 11:28:10.144932', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('bc8b7f27-c16d-463b-b80a-8589bc46f202', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-20 11:33:35.233159', '2016-07-20 11:33:35.233159', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('084f6a2e-37e4-4a94-bc10-5797438ad0f0', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-20 11:34:31.905336', '2016-07-20 11:34:31.905336', 0, 30, 1, 0);
INSERT INTO "ClusterInstanceEntity" ("ClusterId", "MachineName", "MachineAddress", "RegionTypeId", "RegistrationDate", "LastActivityDate", "State", "LimitActiveNodes", "ShoutDownReaspon", "UpdateStatus") VALUES ('b1d7b58b-ec04-4820-a736-13b68a44f6ab', 'DESKTOP-LLB1F78', '127.0.0.1', 2, '2016-07-20 11:58:40.247036', '2016-07-20 11:58:40.247036', 0, 30, 1, 0);


--
-- TOC entry 2843 (class 0 OID 227208)
-- Dependencies: 218
-- Data for Name: ClusterNodeEntity; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('91fd5279-ea2a-43d3-9945-ddd1e67b8182', 'f108d25b-b972-4ed9-8704-0488af9f6e04', '127.0.0.1', 7777, '2016-07-20 11:12:09.377748', '2016-07-20 10:52:32.454586', '2016-07-20 11:12:06.957858', '77a97bdf-12ba-4563-9e76-a350773bc356', 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('a74c644f-fbd0-4625-a01f-9b53dc57d632', 'bc8b7f27-c16d-463b-b80a-8589bc46f202', '127.0.0.1', 7777, '2016-07-20 11:34:27.870594', '2016-07-20 11:34:02.345876', '2016-07-20 11:34:07.41641', '71f89982-fed6-49d7-a99a-5a37a8e3fd19', 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('bcc01c92-7b84-4299-acc9-1fdfbd52adc4', '26298c5a-6d9f-44ec-ba75-d5c7a0efbe3f', '127.0.0.1', 7777, '2016-07-19 12:49:05.951803', '2016-07-19 12:46:16.311679', '2016-07-19 12:49:03.867239', NULL, 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('45aa77e6-9c1e-4d99-b4b7-aaef2ca45142', '1e075ccb-634c-4805-aa55-7c23dcc1d1f4', '127.0.0.1', 7777, '2016-07-20 10:42:38.007686', '2016-07-20 10:32:23.705815', '2016-07-20 10:42:36.713617', '75be812b-de2a-4b0b-8886-931d89856559', 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('7fe1702a-aa5a-4520-8d8b-57977840eeba', 'a4eee8d8-1d6c-4a94-b28e-4eab91fe371d', '127.0.0.1', 7777, '2016-07-19 12:04:50.594587', '2016-07-19 12:01:24.533273', '2016-07-19 12:04:49.175358', 'd6ea8547-d8f2-45d6-aba8-cff22ed03f10', 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('65fced91-770c-4395-9249-187f96a540bd', 'b1d7b58b-ec04-4820-a736-13b68a44f6ab', '127.0.0.1', 7777, '2016-07-20 12:13:50.797767', '2016-07-20 11:59:07.285273', '2016-07-20 12:13:50.243643', 'd7502a24-c17c-4904-96ea-9a2f15dccd99', 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('fc5fd786-94eb-414a-ad21-9799b99dea35', 'fe43d1e2-13fb-4276-bed0-f6533be23cad', '127.0.0.1', 7777, '2016-07-20 11:32:54.940941', '2016-07-20 11:28:37.713703', '2016-07-20 11:32:53.898644', 'bc339326-3286-42f2-ae77-0744a0f2a7b2', 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('761b03f3-90d0-40d8-ab8f-4420d71ee251', '5782541c-96ff-460e-83ae-c1e7098aad82', '127.0.0.1', 7777, '2016-07-20 11:27:26.138956', '2016-07-20 11:18:26.128985', '2016-07-20 11:27:25.402888', '241ed45e-fb74-496f-a6d1-a92fdc5daf16', 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('8e4d65eb-7ae5-4506-8f5e-6a9958d7a3ad', '054ccb63-5359-494b-8cf5-26235f20b48a', '127.0.0.1', 7777, '2016-07-19 12:59:07.881515', '2016-07-19 12:54:10.626931', '2016-07-19 12:59:05.122678', 'a3d3504a-1c73-45bb-b10b-33d236bd6d00', 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('35f586c4-6a80-4fca-b2ee-c609cee4cac7', 'f03a3deb-f25f-47d4-93de-d44f3eb0a210', '127.0.0.1', 7777, '2016-07-19 09:51:57.109759', '2016-07-19 09:32:41.544169', '2016-07-19 09:51:54.117252', NULL, 3);
INSERT INTO "ClusterNodeEntity" ("NodeId", "ClusterId", "Address_Host", "Address_Port", "ShutDownDate", "RegistrationDate", "LastActivityDate", "ActiveMatchId", "State") VALUES ('81379b9d-87e1-4e18-87a2-805251520d5c', '084f6a2e-37e4-4a94-bc10-5797438ad0f0', '127.0.0.1', 7777, '2016-07-20 11:37:01.613453', '2016-07-20 11:35:00.222399', '2016-07-20 11:36:59.742607', 'dcb4376f-6a31-410b-9514-1774269ddcc2', 3);


--
-- TOC entry 2853 (class 0 OID 227308)
-- Dependencies: 228
-- Data for Name: GameSessionEntity; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "GameSessionEntity" ("SessionId", "PlayerId", "LastMessageAccess", "LastActivityDate") VALUES ('66ee92de-50ea-4f1c-8063-aafbed8afdba', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', '2016-07-20 12:14:05.758483', '2016-07-20 12:14:25.756482');


--
-- TOC entry 2854 (class 0 OID 227318)
-- Dependencies: 229
-- Data for Name: GlobalChatMessage; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2837 (class 0 OID 227097)
-- Dependencies: 212
-- Data for Name: PrivateChatMessage; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2838 (class 0 OID 227107)
-- Dependencies: 213
-- Data for Name: PrivateChatMessageData; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2872 (class 0 OID 227944)
-- Dependencies: 247
-- Data for Name: QueueEntity; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2873 (class 0 OID 227956)
-- Dependencies: 248
-- Data for Name: QueueMember; Type: TABLE DATA; Schema: public; Owner: postgres
--



SET search_path = store, pg_catalog;

--
-- TOC entry 2828 (class 0 OID 226998)
-- Dependencies: 203
-- Data for Name: AbstractItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (9, 0, false, 1, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (10, 0, false, 1, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (11, 0, false, 1, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 0, false, 0, 0, 1, 5000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 0, false, 0, 0, 2, 3500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 0, false, 0, 0, 2, 4500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 0, false, 0, 0, 2, 7000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 0, false, 0, 0, 0, 175000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 0, false, 0, 0, 0, 23543, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (8, 0, false, 0, 0, 1, 14477, true);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 7, true, 1, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (18, 0, true, 3, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (20, 0, true, 4, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (23, 0, true, 6, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (22, 0, true, 7, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (19, 0, true, 3, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (21, 0, true, 9, 0, 0, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 1, true, 2, 0, 0, 50700, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 1, true, 1, 0, 0, 58700, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 1, true, 3, 0, 0, 50000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 1, true, 4, 0, 0, 15000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 0, true, 0, 0, 2, 5000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (24, 0, true, 0, 0, 2, 1000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (26, 0, true, 1, 0, 2, 2600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (27, 0, true, 2, 0, 2, 8600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (25, 0, true, 2, 0, 2, 11200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (12, 0, true, 0, 0, 0, 6800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (15, 0, true, 0, 0, 0, 2600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (13, 0, true, 1, 0, 0, 9800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (14, 0, true, 1, 0, 0, 4200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (16, 0, true, 2, 0, 0, 18600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (17, 0, true, 2, 0, 0, 22100, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (28, 0, true, 1, 0, 2, 5400, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 1, true, 1, 0, 2, 2600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 1, true, 1, 0, 2, 4200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 1, true, 1, 0, 2, 3200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (8, 1, true, 1, 0, 2, 1100, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (4, 1, true, 2, 0, 2, 1800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 4, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 4, true, 1, 0, 2, 100, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 4, true, 2, 0, 2, 500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 4, true, 1, 0, 2, 400, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (4, 4, true, 2, 0, 2, 850, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 4, true, 1, 0, 2, 250, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 4, true, 0, 0, 2, 100, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 4, true, 1, 0, 2, 400, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (8, 4, true, 2, 0, 2, 450, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (9, 4, true, 3, 0, 2, 600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (10, 4, true, 3, 0, 2, 800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (11, 4, true, 3, 0, 2, 900, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (12, 4, true, 2, 0, 2, 200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (13, 4, true, 2, 0, 2, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (14, 4, true, 2, 0, 2, 400, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (15, 4, true, 3, 0, 2, 600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (16, 4, true, 3, 0, 2, 800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (17, 4, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (18, 4, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (19, 4, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 7, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 7, true, 1, 0, 0, 500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 7, true, 1, 0, 0, 1500, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (4, 7, true, 1, 0, 0, 1600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 7, true, 1, 0, 0, 2000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (8, 7, true, 1, 0, 0, 100000, true);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (29, 0, true, 3, 0, 2, 11200, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 7, true, 1, 0, 0, 10000, true);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 7, true, 1, 0, 0, 100000, true);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 5, true, 0, 0, 0, 0, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 5, true, 10, 0, 0, 50000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (2, 5, true, 20, 0, 0, 80000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (3, 5, true, 20, 0, 0, 80000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (4, 5, true, 40, 0, 0, 150000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (5, 5, true, 30, 0, 0, 110000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (6, 5, true, 38, 0, 0, 140000, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (0, 9, true, 1, 0, 2, 800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (1, 9, true, 1, 0, 0, 900, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (20, 4, true, 2, 0, 2, 800, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (21, 4, true, 2, 0, 2, 600, false);
INSERT INTO "AbstractItemInstance" ("ModelId", "CategoryTypeId", "IsAvalible", "Level", "DefaultSkinId", "FractionId", "Cost_Amount", "Cost_IsDonate") VALUES (7, 5, true, 10, 0, 0, 45600, false);


--
-- TOC entry 2830 (class 0 OID 227014)
-- Dependencies: 205
-- Data for Name: AddonItemContainer; Type: TABLE DATA; Schema: store; Owner: postgres
--



--
-- TOC entry 2889 (class 0 OID 0)
-- Dependencies: 204
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE SET; Schema: store; Owner: postgres
--

SELECT pg_catalog.setval('"AddonItemContainer_ContainerId_seq"', 1, false);


--
-- TOC entry 2870 (class 0 OID 227447)
-- Dependencies: 245
-- Data for Name: AddonItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (20, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (21, 4);


--
-- TOC entry 2867 (class 0 OID 227423)
-- Dependencies: 242
-- Data for Name: AmmoItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--



--
-- TOC entry 2865 (class 0 OID 227407)
-- Dependencies: 240
-- Data for Name: ArmourItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 1);


--
-- TOC entry 2864 (class 0 OID 227399)
-- Dependencies: 239
-- Data for Name: CharacterItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 5);


--
-- TOC entry 2831 (class 0 OID 227026)
-- Dependencies: 206
-- Data for Name: FractionEntity; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (1, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (2, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (3, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (4, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (0, 0, 0, 0);


--
-- TOC entry 2869 (class 0 OID 227439)
-- Dependencies: 244
-- Data for Name: KitItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "KitItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 9);
INSERT INTO "KitItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 9);


--
-- TOC entry 2868 (class 0 OID 227431)
-- Dependencies: 243
-- Data for Name: SkinItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 7);


--
-- TOC entry 2866 (class 0 OID 227415)
-- Dependencies: 241
-- Data for Name: WeaponItemInstance; Type: TABLE DATA; Schema: store; Owner: postgres
--

INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (20, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (21, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (22, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (23, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (24, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (25, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (26, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (27, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (28, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (29, 0);


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2856 (class 0 OID 227342)
-- Dependencies: 231
-- Data for Name: Roles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2847 (class 0 OID 227257)
-- Dependencies: 222
-- Data for Name: UserClaims; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2890 (class 0 OID 0)
-- Dependencies: 221
-- Name: UserClaims_Id_seq; Type: SEQUENCE SET; Schema: vrs; Owner: postgres
--

SELECT pg_catalog.setval('"UserClaims_Id_seq"', 1, false);


--
-- TOC entry 2848 (class 0 OID 227268)
-- Dependencies: 223
-- Data for Name: UserLogins; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "UserLogins" ("LoginProvider", "ProviderKey", "UserId") VALUES ('Steam', 'http://steamcommunity.com/openid/id/76561198310348310', '5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a');


--
-- TOC entry 2849 (class 0 OID 227277)
-- Dependencies: 224
-- Data for Name: UserRoles; Type: TABLE DATA; Schema: vrs; Owner: postgres
--



--
-- TOC entry 2845 (class 0 OID 227238)
-- Dependencies: 220
-- Data for Name: Users; Type: TABLE DATA; Schema: vrs; Owner: postgres
--

INSERT INTO "Users" ("Id", "Balance", "Email", "EmailConfirmed", "PasswordHash", "SecurityStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEndDateUtc", "LockoutEnabled", "AccessFailedCount", "UserName") VALUES ('5030e1a2-1e14-4968-b8ce-e1d3c41b3b2a', 0, NULL, false, NULL, '93749cb1-6857-45d9-9ae8-00db21fe95cd', NULL, false, false, NULL, false, 0, '76561198310348310');


SET search_path = achievements, pg_catalog;

--
-- TOC entry 2483 (class 2606 OID 226928)
-- Name: PK_achievements.AchievementContainer; Type: CONSTRAINT; Schema: achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementContainer"
    ADD CONSTRAINT "PK_achievements.AchievementContainer" PRIMARY KEY ("AchievementTypeId");


--
-- TOC entry 2486 (class 2606 OID 226939)
-- Name: PK_achievements.AchievementInstance; Type: CONSTRAINT; Schema: achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementInstance"
    ADD CONSTRAINT "PK_achievements.AchievementInstance" PRIMARY KEY ("AchievementBonusId");


SET search_path = dbo, pg_catalog;

--
-- TOC entry 2632 (class 2606 OID 227797)
-- Name: PK_dbo.__MigrationHistory; Type: CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY "__MigrationHistory"
    ADD CONSTRAINT "PK_dbo.__MigrationHistory" PRIMARY KEY ("MigrationId", "ContextKey");


SET search_path = gamemode, pg_catalog;

--
-- TOC entry 2576 (class 2606 OID 227307)
-- Name: PK_gamemode.AbstractGameModeEntity; Type: CONSTRAINT; Schema: gamemode; Owner: postgres
--

ALTER TABLE ONLY "AbstractGameModeEntity"
    ADD CONSTRAINT "PK_gamemode.AbstractGameModeEntity" PRIMARY KEY ("GameModeId");


--
-- TOC entry 2574 (class 2606 OID 227298)
-- Name: PK_gamemode.GameMapEntity; Type: CONSTRAINT; Schema: gamemode; Owner: postgres
--

ALTER TABLE ONLY "GameMapEntity"
    ADD CONSTRAINT "PK_gamemode.GameMapEntity" PRIMARY KEY ("InstanceId");


--
-- TOC entry 2590 (class 2606 OID 227355)
-- Name: PK_gamemode.TeamGameModeEntity; Type: CONSTRAINT; Schema: gamemode; Owner: postgres
--

ALTER TABLE ONLY "TeamGameModeEntity"
    ADD CONSTRAINT "PK_gamemode.TeamGameModeEntity" PRIMARY KEY ("GameModeId");


SET search_path = league, pg_catalog;

--
-- TOC entry 2529 (class 2606 OID 227096)
-- Name: PK_league.LeagueEntity; Type: CONSTRAINT; Schema: league; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "PK_league.LeagueEntity" PRIMARY KEY ("LeagueId");


--
-- TOC entry 2527 (class 2606 OID 227080)
-- Name: PK_league.LeagueMemberEntity; Type: CONSTRAINT; Schema: league; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "PK_league.LeagueMemberEntity" PRIMARY KEY ("PlayerId");


SET search_path = matches, pg_catalog;

--
-- TOC entry 2541 (class 2606 OID 227161)
-- Name: PK_matches.MatchEntity; Type: CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "PK_matches.MatchEntity" PRIMARY KEY ("MatchId");


--
-- TOC entry 2546 (class 2606 OID 227186)
-- Name: PK_matches.MatchMember; Type: CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "PK_matches.MatchMember" PRIMARY KEY ("MemberId");


--
-- TOC entry 2552 (class 2606 OID 227206)
-- Name: PK_matches.MatchTeam; Type: CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "PK_matches.MatchTeam" PRIMARY KEY ("TeamId");


--
-- TOC entry 2549 (class 2606 OID 227198)
-- Name: PK_matches.MemberAchievements; Type: CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "PK_matches.MemberAchievements" PRIMARY KEY ("AchievementId");


SET search_path = players, pg_catalog;

--
-- TOC entry 2493 (class 2606 OID 226961)
-- Name: PK_players.FriendEntity; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "PK_players.FriendEntity" PRIMARY KEY ("MemberId");


--
-- TOC entry 2515 (class 2606 OID 227043)
-- Name: PK_players.InstanceOfInstalledAddon; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "PK_players.InstanceOfInstalledAddon" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2606 (class 2606 OID 227390)
-- Name: PK_players.InstanceOfItemAddon; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "PK_players.InstanceOfItemAddon" PRIMARY KEY ("ItemId");


--
-- TOC entry 2603 (class 2606 OID 227383)
-- Name: PK_players.InstanceOfItemSkin; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "PK_players.InstanceOfItemSkin" PRIMARY KEY ("ItemId");


--
-- TOC entry 2600 (class 2606 OID 227376)
-- Name: PK_players.InstanceOfPlayerArmour; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "PK_players.InstanceOfPlayerArmour" PRIMARY KEY ("ItemId");


--
-- TOC entry 2594 (class 2606 OID 227362)
-- Name: PK_players.InstanceOfPlayerCharacter; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "PK_players.InstanceOfPlayerCharacter" PRIMARY KEY ("ItemId");


--
-- TOC entry 2497 (class 2606 OID 226982)
-- Name: PK_players.InstanceOfPlayerItem; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "PK_players.InstanceOfPlayerItem" PRIMARY KEY ("ItemId");


--
-- TOC entry 2609 (class 2606 OID 227397)
-- Name: PK_players.InstanceOfPlayerKit; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerKit"
    ADD CONSTRAINT "PK_players.InstanceOfPlayerKit" PRIMARY KEY ("ItemId");


--
-- TOC entry 2597 (class 2606 OID 227369)
-- Name: PK_players.InstanceOfPlayerWeapon; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "PK_players.InstanceOfPlayerWeapon" PRIMARY KEY ("ItemId");


--
-- TOC entry 2502 (class 2606 OID 226994)
-- Name: PK_players.InstanceOfTargetItem; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "PK_players.InstanceOfTargetItem" PRIMARY KEY ("ItemId");


--
-- TOC entry 2478 (class 2606 OID 226919)
-- Name: PK_players.PlayerAchievement; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PK_players.PlayerAchievement" PRIMARY KEY ("PlayerAchievementId");


--
-- TOC entry 2474 (class 2606 OID 226906)
-- Name: PK_players.PlayerEntity; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PK_players.PlayerEntity" PRIMARY KEY ("PlayerId");


--
-- TOC entry 2517 (class 2606 OID 227058)
-- Name: PK_players.PlayerProfileEntity; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PK_players.PlayerProfileEntity" PRIMARY KEY ("AssetId");


--
-- TOC entry 2585 (class 2606 OID 227341)
-- Name: PK_players.PlayerPromoCode; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerPromoCode"
    ADD CONSTRAINT "PK_players.PlayerPromoCode" PRIMARY KEY ("PlayerCodeId");


--
-- TOC entry 2488 (class 2606 OID 226951)
-- Name: PK_players.PlayerReputationEntity; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PK_players.PlayerReputationEntity" PRIMARY KEY ("ReputationId");


--
-- TOC entry 2521 (class 2606 OID 227069)
-- Name: PK_players.ProfileItemEntity; Type: CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "ProfileItemEntity"
    ADD CONSTRAINT "PK_players.ProfileItemEntity" PRIMARY KEY ("ProfileItemId");


SET search_path = public, pg_catalog;

--
-- TOC entry 2558 (class 2606 OID 227237)
-- Name: PK_public.ClusterInstanceEntity; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterInstanceEntity"
    ADD CONSTRAINT "PK_public.ClusterInstanceEntity" PRIMARY KEY ("ClusterId");


--
-- TOC entry 2556 (class 2606 OID 227222)
-- Name: PK_public.ClusterNodeEntity; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterNodeEntity"
    ADD CONSTRAINT "PK_public.ClusterNodeEntity" PRIMARY KEY ("NodeId");


--
-- TOC entry 2579 (class 2606 OID 227316)
-- Name: PK_public.GameSessionEntity; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GameSessionEntity"
    ADD CONSTRAINT "PK_public.GameSessionEntity" PRIMARY KEY ("SessionId");


--
-- TOC entry 2583 (class 2606 OID 227330)
-- Name: PK_public.GlobalChatMessage; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "PK_public.GlobalChatMessage" PRIMARY KEY ("MessageId");


--
-- TOC entry 2531 (class 2606 OID 227104)
-- Name: PK_public.PrivateChatMessage; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PK_public.PrivateChatMessage" PRIMARY KEY ("MessageId");


--
-- TOC entry 2535 (class 2606 OID 227119)
-- Name: PK_public.PrivateChatMessageData; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PK_public.PrivateChatMessageData" PRIMARY KEY ("MessageId");


--
-- TOC entry 2634 (class 2606 OID 228038)
-- Name: QueueEntity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "QueueEntity"
    ADD CONSTRAINT "QueueEntity_pkey" PRIMARY KEY ("QueueId");


--
-- TOC entry 2639 (class 2606 OID 228036)
-- Name: QueueMember_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "QueueMember"
    ADD CONSTRAINT "QueueMember_pkey" PRIMARY KEY ("PlayerId");


SET search_path = store, pg_catalog;

--
-- TOC entry 2505 (class 2606 OID 227010)
-- Name: PK_store.AbstractItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "PK_store.AbstractItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2509 (class 2606 OID 227023)
-- Name: PK_store.AddonItemContainer; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "PK_store.AddonItemContainer" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2630 (class 2606 OID 227453)
-- Name: PK_store.AddonItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "PK_store.AddonItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2621 (class 2606 OID 227429)
-- Name: PK_store.AmmoItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "PK_store.AmmoItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2615 (class 2606 OID 227413)
-- Name: PK_store.ArmourItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "PK_store.ArmourItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2612 (class 2606 OID 227405)
-- Name: PK_store.CharacterItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "PK_store.CharacterItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2511 (class 2606 OID 227034)
-- Name: PK_store.FractionEntity; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "PK_store.FractionEntity" PRIMARY KEY ("FractionId");


--
-- TOC entry 2627 (class 2606 OID 227445)
-- Name: PK_store.KitItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "PK_store.KitItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2623 (class 2606 OID 227437)
-- Name: PK_store.SkinItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "PK_store.SkinItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2617 (class 2606 OID 227421)
-- Name: PK_store.WeaponItemInstance; Type: CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "PK_store.WeaponItemInstance" PRIMARY KEY ("ModelId", "CategoryTypeId");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2587 (class 2606 OID 227348)
-- Name: PK_vrs.Roles; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Roles"
    ADD CONSTRAINT "PK_vrs.Roles" PRIMARY KEY ("Id");


--
-- TOC entry 2563 (class 2606 OID 227266)
-- Name: PK_vrs.UserClaims; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "PK_vrs.UserClaims" PRIMARY KEY ("Id");


--
-- TOC entry 2566 (class 2606 OID 227275)
-- Name: PK_vrs.UserLogins; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "PK_vrs.UserLogins" PRIMARY KEY ("LoginProvider", "ProviderKey", "UserId");


--
-- TOC entry 2569 (class 2606 OID 227283)
-- Name: PK_vrs.UserRoles; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "PK_vrs.UserRoles" PRIMARY KEY ("UserId", "RoleId");


--
-- TOC entry 2560 (class 2606 OID 227253)
-- Name: PK_vrs.Users; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "PK_vrs.Users" PRIMARY KEY ("Id");


SET search_path = achievements, pg_catalog;

--
-- TOC entry 2484 (class 1259 OID 226940)
-- Name: AchievementInstance_IX_AchievementTypeId; Type: INDEX; Schema: achievements; Owner: postgres
--

CREATE INDEX "AchievementInstance_IX_AchievementTypeId" ON "AchievementInstance" USING btree ("AchievementTypeId");


SET search_path = gamemode, pg_catalog;

--
-- TOC entry 2572 (class 1259 OID 227299)
-- Name: GameMapEntity_IX_GameModeId; Type: INDEX; Schema: gamemode; Owner: postgres
--

CREATE INDEX "GameMapEntity_IX_GameModeId" ON "GameMapEntity" USING btree ("GameModeId");


--
-- TOC entry 2591 (class 1259 OID 227356)
-- Name: TeamGameModeEntity_IX_GameModeId; Type: INDEX; Schema: gamemode; Owner: postgres
--

CREATE INDEX "TeamGameModeEntity_IX_GameModeId" ON "TeamGameModeEntity" USING btree ("GameModeId");


SET search_path = league, pg_catalog;

--
-- TOC entry 2524 (class 1259 OID 227082)
-- Name: LeagueMemberEntity_IX_LeagueId; Type: INDEX; Schema: league; Owner: postgres
--

CREATE INDEX "LeagueMemberEntity_IX_LeagueId" ON "LeagueMemberEntity" USING btree ("LeagueId");


--
-- TOC entry 2525 (class 1259 OID 227081)
-- Name: LeagueMemberEntity_IX_PlayerId; Type: INDEX; Schema: league; Owner: postgres
--

CREATE INDEX "LeagueMemberEntity_IX_PlayerId" ON "LeagueMemberEntity" USING btree ("PlayerId");


SET search_path = matches, pg_catalog;

--
-- TOC entry 2538 (class 1259 OID 227162)
-- Name: MatchEntity_IX_NodeId; Type: INDEX; Schema: matches; Owner: postgres
--

CREATE INDEX "MatchEntity_IX_NodeId" ON "MatchEntity" USING btree ("NodeId");


--
-- TOC entry 2539 (class 1259 OID 227163)
-- Name: MatchEntity_IX_WinnerTeamId; Type: INDEX; Schema: matches; Owner: postgres
--

CREATE INDEX "MatchEntity_IX_WinnerTeamId" ON "MatchEntity" USING btree ("WinnerTeamId");


--
-- TOC entry 2542 (class 1259 OID 227187)
-- Name: MatchMember_IX_MatchId; Type: INDEX; Schema: matches; Owner: postgres
--

CREATE INDEX "MatchMember_IX_MatchId" ON "MatchMember" USING btree ("MatchId");


--
-- TOC entry 2543 (class 1259 OID 227188)
-- Name: MatchMember_IX_PlayerId; Type: INDEX; Schema: matches; Owner: postgres
--

CREATE INDEX "MatchMember_IX_PlayerId" ON "MatchMember" USING btree ("PlayerId");


--
-- TOC entry 2544 (class 1259 OID 227189)
-- Name: MatchMember_IX_TeamId; Type: INDEX; Schema: matches; Owner: postgres
--

CREATE INDEX "MatchMember_IX_TeamId" ON "MatchMember" USING btree ("TeamId");


--
-- TOC entry 2550 (class 1259 OID 227207)
-- Name: MatchTeam_IX_MatchId; Type: INDEX; Schema: matches; Owner: postgres
--

CREATE INDEX "MatchTeam_IX_MatchId" ON "MatchTeam" USING btree ("MatchId");


--
-- TOC entry 2547 (class 1259 OID 227199)
-- Name: MemberAchievements_IX_MemberId; Type: INDEX; Schema: matches; Owner: postgres
--

CREATE INDEX "MemberAchievements_IX_MemberId" ON "MemberAchievements" USING btree ("MemberId");


SET search_path = players, pg_catalog;

--
-- TOC entry 2490 (class 1259 OID 226963)
-- Name: FriendEntity_IX_FriendId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "FriendEntity_IX_FriendId" ON "FriendEntity" USING btree ("FriendId");


--
-- TOC entry 2491 (class 1259 OID 226962)
-- Name: FriendEntity_IX_PlayerId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "FriendEntity_IX_PlayerId" ON "FriendEntity" USING btree ("PlayerId");


--
-- TOC entry 2512 (class 1259 OID 227044)
-- Name: InstanceOfInstalledAddon_IX_AddonId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfInstalledAddon_IX_AddonId" ON "InstanceOfInstalledAddon" USING btree ("AddonId");


--
-- TOC entry 2513 (class 1259 OID 227045)
-- Name: InstanceOfInstalledAddon_IX_ItemId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfInstalledAddon_IX_ItemId" ON "InstanceOfInstalledAddon" USING btree ("ItemId");


--
-- TOC entry 2604 (class 1259 OID 227391)
-- Name: InstanceOfItemAddon_IX_ItemId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfItemAddon_IX_ItemId" ON "InstanceOfItemAddon" USING btree ("ItemId");


--
-- TOC entry 2601 (class 1259 OID 227384)
-- Name: InstanceOfItemSkin_IX_ItemId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfItemSkin_IX_ItemId" ON "InstanceOfItemSkin" USING btree ("ItemId");


--
-- TOC entry 2598 (class 1259 OID 227377)
-- Name: InstanceOfPlayerArmour_IX_ItemId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfPlayerArmour_IX_ItemId" ON "InstanceOfPlayerArmour" USING btree ("ItemId");


--
-- TOC entry 2592 (class 1259 OID 227363)
-- Name: InstanceOfPlayerCharacter_IX_ItemId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfPlayerCharacter_IX_ItemId" ON "InstanceOfPlayerCharacter" USING btree ("ItemId");


--
-- TOC entry 2494 (class 1259 OID 226983)
-- Name: InstanceOfPlayerItem_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfPlayerItem_IX_ModelId_CategoryTypeId" ON "InstanceOfPlayerItem" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2495 (class 1259 OID 226984)
-- Name: InstanceOfPlayerItem_IX_PlayerId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfPlayerItem_IX_PlayerId" ON "InstanceOfPlayerItem" USING btree ("PlayerId");


--
-- TOC entry 2607 (class 1259 OID 227398)
-- Name: InstanceOfPlayerKit_IX_ItemId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfPlayerKit_IX_ItemId" ON "InstanceOfPlayerKit" USING btree ("ItemId");


--
-- TOC entry 2595 (class 1259 OID 227370)
-- Name: InstanceOfPlayerWeapon_IX_ItemId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfPlayerWeapon_IX_ItemId" ON "InstanceOfPlayerWeapon" USING btree ("ItemId");


--
-- TOC entry 2498 (class 1259 OID 226995)
-- Name: InstanceOfTargetItem_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfTargetItem_IX_ModelId_CategoryTypeId" ON "InstanceOfTargetItem" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2499 (class 1259 OID 226996)
-- Name: InstanceOfTargetItem_IX_PlayerId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfTargetItem_IX_PlayerId" ON "InstanceOfTargetItem" USING btree ("PlayerId");


--
-- TOC entry 2500 (class 1259 OID 226997)
-- Name: InstanceOfTargetItem_IX_TargetPlayerItemId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "InstanceOfTargetItem_IX_TargetPlayerItemId" ON "InstanceOfTargetItem" USING btree ("TargetPlayerItemId");


--
-- TOC entry 2479 (class 1259 OID 226921)
-- Name: PlayerAchievement_IX_AchievementTypeId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "PlayerAchievement_IX_AchievementTypeId" ON "PlayerAchievement" USING btree ("AchievementTypeId");


--
-- TOC entry 2480 (class 1259 OID 226922)
-- Name: PlayerAchievement_IX_LastAchievementInstanceId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "PlayerAchievement_IX_LastAchievementInstanceId" ON "PlayerAchievement" USING btree ("LastAchievementInstanceId");


--
-- TOC entry 2481 (class 1259 OID 226920)
-- Name: PlayerAchievement_IX_PlayerId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "PlayerAchievement_IX_PlayerId" ON "PlayerAchievement" USING btree ("PlayerId");


--
-- TOC entry 2475 (class 1259 OID 226908)
-- Name: PlayerEntity_IX_CurrentFractionId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "PlayerEntity_IX_CurrentFractionId" ON "PlayerEntity" USING btree ("CurrentFractionId");


--
-- TOC entry 2476 (class 1259 OID 226907)
-- Name: PlayerEntity_IX_PlayerId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "PlayerEntity_IX_PlayerId" ON "PlayerEntity" USING btree ("PlayerId");


--
-- TOC entry 2518 (class 1259 OID 227060)
-- Name: PlayerProfileEntity_IX_CharacterId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "PlayerProfileEntity_IX_CharacterId" ON "PlayerProfileEntity" USING btree ("CharacterId");


--
-- TOC entry 2519 (class 1259 OID 227059)
-- Name: PlayerProfileEntity_IX_PlayerId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "PlayerProfileEntity_IX_PlayerId" ON "PlayerProfileEntity" USING btree ("PlayerId");


--
-- TOC entry 2489 (class 1259 OID 226952)
-- Name: PlayerReputationEntity_IX_PlayerId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "PlayerReputationEntity_IX_PlayerId" ON "PlayerReputationEntity" USING btree ("PlayerId");


--
-- TOC entry 2522 (class 1259 OID 227071)
-- Name: ProfileItemEntity_IX_ItemId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "ProfileItemEntity_IX_ItemId" ON "ProfileItemEntity" USING btree ("ItemId");


--
-- TOC entry 2523 (class 1259 OID 227070)
-- Name: ProfileItemEntity_IX_ProfileId; Type: INDEX; Schema: players; Owner: postgres
--

CREATE INDEX "ProfileItemEntity_IX_ProfileId" ON "ProfileItemEntity" USING btree ("ProfileId");


SET search_path = public, pg_catalog;

--
-- TOC entry 2553 (class 1259 OID 227224)
-- Name: ClusterNodeEntity_IX_ActiveMatchId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "ClusterNodeEntity_IX_ActiveMatchId" ON "ClusterNodeEntity" USING btree ("ActiveMatchId");


--
-- TOC entry 2554 (class 1259 OID 227223)
-- Name: ClusterNodeEntity_IX_ClusterId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "ClusterNodeEntity_IX_ClusterId" ON "ClusterNodeEntity" USING btree ("ClusterId");


--
-- TOC entry 2577 (class 1259 OID 227858)
-- Name: GameSessionEntity_IX_PlayerId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "GameSessionEntity_IX_PlayerId" ON "GameSessionEntity" USING btree ("PlayerId");


--
-- TOC entry 2580 (class 1259 OID 227332)
-- Name: GlobalChatMessage_IX_EditorId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "GlobalChatMessage_IX_EditorId" ON "GlobalChatMessage" USING btree ("EditorId");


--
-- TOC entry 2581 (class 1259 OID 227331)
-- Name: GlobalChatMessage_IX_SenderId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "GlobalChatMessage_IX_SenderId" ON "GlobalChatMessage" USING btree ("SenderId");


--
-- TOC entry 2536 (class 1259 OID 227121)
-- Name: PrivateChatMessageData_IX_ReceiverId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "PrivateChatMessageData_IX_ReceiverId" ON "PrivateChatMessageData" USING btree ("ReceiverId");


--
-- TOC entry 2537 (class 1259 OID 227120)
-- Name: PrivateChatMessageData_IX_SenderId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "PrivateChatMessageData_IX_SenderId" ON "PrivateChatMessageData" USING btree ("SenderId");


--
-- TOC entry 2532 (class 1259 OID 227105)
-- Name: PrivateChatMessage_IX_MessageDataId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "PrivateChatMessage_IX_MessageDataId" ON "PrivateChatMessage" USING btree ("MessageDataId");


--
-- TOC entry 2533 (class 1259 OID 227106)
-- Name: PrivateChatMessage_IX_PlayerId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "PrivateChatMessage_IX_PlayerId" ON "PrivateChatMessage" USING btree ("PlayerId");


--
-- TOC entry 2637 (class 1259 OID 228005)
-- Name: QueueMember_IX_MatchMemberId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "QueueMember_IX_MatchMemberId" ON "QueueMember" USING btree ("MatchMemberId");


--
-- TOC entry 2635 (class 1259 OID 227967)
-- Name: SessionQueueEntity_IX_MatchId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "SessionQueueEntity_IX_MatchId" ON "QueueEntity" USING btree ("MatchId");


--
-- TOC entry 2636 (class 1259 OID 227968)
-- Name: SessionQueueEntity_IX_PlayerId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "SessionQueueEntity_IX_PlayerId" ON "QueueEntity" USING btree ("QueueId");


--
-- TOC entry 2640 (class 1259 OID 227969)
-- Name: SessionQueueMember_IX_PlayerId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "SessionQueueMember_IX_PlayerId" ON "QueueMember" USING btree ("PlayerId");


--
-- TOC entry 2641 (class 1259 OID 227970)
-- Name: SessionQueueMember_IX_QueueId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "SessionQueueMember_IX_QueueId" ON "QueueMember" USING btree ("QueueId");


SET search_path = store, pg_catalog;

--
-- TOC entry 2503 (class 1259 OID 227011)
-- Name: AbstractItemInstance_IX_FractionId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "AbstractItemInstance_IX_FractionId" ON "AbstractItemInstance" USING btree ("FractionId");


--
-- TOC entry 2506 (class 1259 OID 227025)
-- Name: AddonItemContainer_IX_ItemId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "AddonItemContainer_IX_ItemId_CategoryTypeId" ON "AddonItemContainer" USING btree ("ItemId", "CategoryTypeId");


--
-- TOC entry 2507 (class 1259 OID 227024)
-- Name: AddonItemContainer_IX_ModelId_ModelCategoryTypeId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "AddonItemContainer_IX_ModelId_ModelCategoryTypeId" ON "AddonItemContainer" USING btree ("ModelId", "ModelCategoryTypeId");


--
-- TOC entry 2628 (class 1259 OID 227454)
-- Name: AddonItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "AddonItemInstance_IX_ModelId_CategoryTypeId" ON "AddonItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2619 (class 1259 OID 227430)
-- Name: AmmoItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "AmmoItemInstance_IX_ModelId_CategoryTypeId" ON "AmmoItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2613 (class 1259 OID 227414)
-- Name: ArmourItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "ArmourItemInstance_IX_ModelId_CategoryTypeId" ON "ArmourItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2610 (class 1259 OID 227406)
-- Name: CharacterItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "CharacterItemInstance_IX_ModelId_CategoryTypeId" ON "CharacterItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2625 (class 1259 OID 227446)
-- Name: KitItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "KitItemInstance_IX_ModelId_CategoryTypeId" ON "KitItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2624 (class 1259 OID 227438)
-- Name: SkinItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "SkinItemInstance_IX_ModelId_CategoryTypeId" ON "SkinItemInstance" USING btree ("ModelId", "CategoryTypeId");


--
-- TOC entry 2618 (class 1259 OID 227422)
-- Name: WeaponItemInstance_IX_ModelId_CategoryTypeId; Type: INDEX; Schema: store; Owner: postgres
--

CREATE INDEX "WeaponItemInstance_IX_ModelId_CategoryTypeId" ON "WeaponItemInstance" USING btree ("ModelId", "CategoryTypeId");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2588 (class 1259 OID 227349)
-- Name: Roles_RoleNameIndex; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE UNIQUE INDEX "Roles_RoleNameIndex" ON "Roles" USING btree ("Name");


--
-- TOC entry 2564 (class 1259 OID 227267)
-- Name: UserClaims_IX_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "UserClaims_IX_UserId" ON "UserClaims" USING btree ("UserId");


--
-- TOC entry 2567 (class 1259 OID 227276)
-- Name: UserLogins_IX_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "UserLogins_IX_UserId" ON "UserLogins" USING btree ("UserId");


--
-- TOC entry 2570 (class 1259 OID 227285)
-- Name: UserRoles_IX_RoleId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "UserRoles_IX_RoleId" ON "UserRoles" USING btree ("RoleId");


--
-- TOC entry 2571 (class 1259 OID 227284)
-- Name: UserRoles_IX_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "UserRoles_IX_UserId" ON "UserRoles" USING btree ("UserId");


--
-- TOC entry 2561 (class 1259 OID 227254)
-- Name: Users_UserNameIndex; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE UNIQUE INDEX "Users_UserNameIndex" ON "Users" USING btree ("UserName");


SET search_path = achievements, pg_catalog;

--
-- TOC entry 2647 (class 2606 OID 227490)
-- Name: FK_achievements.AchievementInstance_achievements.AchievementCon; Type: FK CONSTRAINT; Schema: achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementInstance"
    ADD CONSTRAINT "FK_achievements.AchievementInstance_achievements.AchievementCon" FOREIGN KEY ("AchievementTypeId") REFERENCES "AchievementContainer"("AchievementTypeId") ON DELETE CASCADE;


SET search_path = gamemode, pg_catalog;

--
-- TOC entry 2684 (class 2606 OID 227695)
-- Name: FK_gamemode.GameMapEntity_gamemode.AbstractGameModeEntity_GameM; Type: FK CONSTRAINT; Schema: gamemode; Owner: postgres
--

ALTER TABLE ONLY "GameMapEntity"
    ADD CONSTRAINT "FK_gamemode.GameMapEntity_gamemode.AbstractGameModeEntity_GameM" FOREIGN KEY ("GameModeId") REFERENCES "AbstractGameModeEntity"("GameModeId") ON DELETE CASCADE;


--
-- TOC entry 2687 (class 2606 OID 227715)
-- Name: FK_gamemode.TeamGameModeEntity_gamemode.AbstractGameModeEntity_; Type: FK CONSTRAINT; Schema: gamemode; Owner: postgres
--

ALTER TABLE ONLY "TeamGameModeEntity"
    ADD CONSTRAINT "FK_gamemode.TeamGameModeEntity_gamemode.AbstractGameModeEntity_" FOREIGN KEY ("GameModeId") REFERENCES "AbstractGameModeEntity"("GameModeId");


SET search_path = league, pg_catalog;

--
-- TOC entry 2665 (class 2606 OID 227580)
-- Name: FK_league.LeagueMemberEntity_league.LeagueEntity_LeagueId; Type: FK CONSTRAINT; Schema: league; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "FK_league.LeagueMemberEntity_league.LeagueEntity_LeagueId" FOREIGN KEY ("LeagueId") REFERENCES "LeagueEntity"("LeagueId") ON DELETE CASCADE;


--
-- TOC entry 2666 (class 2606 OID 227585)
-- Name: FK_league.LeagueMemberEntity_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: league; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "FK_league.LeagueMemberEntity_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES players."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = matches, pg_catalog;

--
-- TOC entry 2672 (class 2606 OID 227635)
-- Name: FK_matches.MatchEntity_matches.MatchTeam_WinnerTeamId; Type: FK CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "FK_matches.MatchEntity_matches.MatchTeam_WinnerTeamId" FOREIGN KEY ("WinnerTeamId") REFERENCES "MatchTeam"("TeamId");


--
-- TOC entry 2671 (class 2606 OID 227630)
-- Name: FK_matches.MatchEntity_public.ClusterNodeEntity_NodeId; Type: FK CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "FK_matches.MatchEntity_public.ClusterNodeEntity_NodeId" FOREIGN KEY ("NodeId") REFERENCES public."ClusterNodeEntity"("NodeId");


--
-- TOC entry 2675 (class 2606 OID 227650)
-- Name: FK_matches.MatchMember_matches.MatchEntity_MatchId; Type: FK CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "FK_matches.MatchMember_matches.MatchEntity_MatchId" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2674 (class 2606 OID 227645)
-- Name: FK_matches.MatchMember_matches.MatchTeam_TeamId; Type: FK CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "FK_matches.MatchMember_matches.MatchTeam_TeamId" FOREIGN KEY ("TeamId") REFERENCES "MatchTeam"("TeamId") ON DELETE CASCADE;


--
-- TOC entry 2673 (class 2606 OID 227640)
-- Name: FK_matches.MatchMember_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "FK_matches.MatchMember_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES players."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2677 (class 2606 OID 227660)
-- Name: FK_matches.MatchTeam_matches.MatchEntity_MatchId; Type: FK CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "FK_matches.MatchTeam_matches.MatchEntity_MatchId" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2676 (class 2606 OID 227655)
-- Name: FK_matches.MemberAchievements_matches.MatchMember_MemberId; Type: FK CONSTRAINT; Schema: matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "FK_matches.MemberAchievements_matches.MatchMember_MemberId" FOREIGN KEY ("MemberId") REFERENCES "MatchMember"("MemberId") ON DELETE CASCADE;


SET search_path = players, pg_catalog;

--
-- TOC entry 2649 (class 2606 OID 227500)
-- Name: FK_players.FriendEntity_players.PlayerEntity_FriendId; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FK_players.FriendEntity_players.PlayerEntity_FriendId" FOREIGN KEY ("FriendId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2650 (class 2606 OID 227505)
-- Name: FK_players.FriendEntity_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FK_players.FriendEntity_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2660 (class 2606 OID 227555)
-- Name: FK_players.InstanceOfInstalledAddon_players.InstanceOfPlayerIte; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "FK_players.InstanceOfInstalledAddon_players.InstanceOfPlayerIte" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2659 (class 2606 OID 227550)
-- Name: FK_players.InstanceOfInstalledAddon_players.InstanceOfTargetIte; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "FK_players.InstanceOfInstalledAddon_players.InstanceOfTargetIte" FOREIGN KEY ("AddonId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2692 (class 2606 OID 227740)
-- Name: FK_players.InstanceOfItemAddon_players.InstanceOfTargetItem_Ite; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "FK_players.InstanceOfItemAddon_players.InstanceOfTargetItem_Ite" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId");


--
-- TOC entry 2691 (class 2606 OID 227735)
-- Name: FK_players.InstanceOfItemSkin_players.InstanceOfTargetItem_Item; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "FK_players.InstanceOfItemSkin_players.InstanceOfTargetItem_Item" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId");


--
-- TOC entry 2690 (class 2606 OID 227730)
-- Name: FK_players.InstanceOfPlayerArmour_players.InstanceOfPlayerItem_; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "FK_players.InstanceOfPlayerArmour_players.InstanceOfPlayerItem_" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId");


--
-- TOC entry 2688 (class 2606 OID 227720)
-- Name: FK_players.InstanceOfPlayerCharacter_players.InstanceOfPlayerIt; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "FK_players.InstanceOfPlayerCharacter_players.InstanceOfPlayerIt" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId");


--
-- TOC entry 2652 (class 2606 OID 227515)
-- Name: FK_players.InstanceOfPlayerItem_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "FK_players.InstanceOfPlayerItem_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2651 (class 2606 OID 227510)
-- Name: FK_players.InstanceOfPlayerItem_store.AbstractItemInstance_Mode; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "FK_players.InstanceOfPlayerItem_store.AbstractItemInstance_Mode" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES store."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2693 (class 2606 OID 227745)
-- Name: FK_players.InstanceOfPlayerKit_players.InstanceOfPlayerItem_Ite; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerKit"
    ADD CONSTRAINT "FK_players.InstanceOfPlayerKit_players.InstanceOfPlayerItem_Ite" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId");


--
-- TOC entry 2689 (class 2606 OID 227725)
-- Name: FK_players.InstanceOfPlayerWeapon_players.InstanceOfPlayerItem_; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "FK_players.InstanceOfPlayerWeapon_players.InstanceOfPlayerItem_" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId");


--
-- TOC entry 2655 (class 2606 OID 227530)
-- Name: FK_players.InstanceOfTargetItem_players.InstanceOfPlayerItem_Ta; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "FK_players.InstanceOfTargetItem_players.InstanceOfPlayerItem_Ta" FOREIGN KEY ("TargetPlayerItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2654 (class 2606 OID 227525)
-- Name: FK_players.InstanceOfTargetItem_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "FK_players.InstanceOfTargetItem_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2653 (class 2606 OID 227520)
-- Name: FK_players.InstanceOfTargetItem_store.AbstractItemInstance_Mode; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "FK_players.InstanceOfTargetItem_store.AbstractItemInstance_Mode" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES store."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2644 (class 2606 OID 227475)
-- Name: FK_players.PlayerAchievement_achievements.AchievementContainer_; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "FK_players.PlayerAchievement_achievements.AchievementContainer_" FOREIGN KEY ("AchievementTypeId") REFERENCES achievements."AchievementContainer"("AchievementTypeId") ON DELETE CASCADE;


--
-- TOC entry 2645 (class 2606 OID 227480)
-- Name: FK_players.PlayerAchievement_achievements.AchievementInstance_L; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "FK_players.PlayerAchievement_achievements.AchievementInstance_L" FOREIGN KEY ("LastAchievementInstanceId") REFERENCES achievements."AchievementInstance"("AchievementBonusId");


--
-- TOC entry 2646 (class 2606 OID 227485)
-- Name: FK_players.PlayerAchievement_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "FK_players.PlayerAchievement_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2642 (class 2606 OID 227455)
-- Name: FK_players.PlayerEntity_players.PlayerReputationEntity_CurrentF; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "FK_players.PlayerEntity_players.PlayerReputationEntity_CurrentF" FOREIGN KEY ("CurrentFractionId") REFERENCES "PlayerReputationEntity"("ReputationId");


--
-- TOC entry 2643 (class 2606 OID 227470)
-- Name: FK_players.PlayerEntity_vrs.Users_PlayerId; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "FK_players.PlayerEntity_vrs.Users_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES vrs."Users"("Id");


--
-- TOC entry 2661 (class 2606 OID 227560)
-- Name: FK_players.PlayerProfileEntity_players.InstanceOfPlayerCharacte; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "FK_players.PlayerProfileEntity_players.InstanceOfPlayerCharacte" FOREIGN KEY ("CharacterId") REFERENCES "InstanceOfPlayerCharacter"("ItemId");


--
-- TOC entry 2662 (class 2606 OID 227565)
-- Name: FK_players.PlayerProfileEntity_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "FK_players.PlayerProfileEntity_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2648 (class 2606 OID 227495)
-- Name: FK_players.PlayerReputationEntity_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "FK_players.PlayerReputationEntity_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2663 (class 2606 OID 227570)
-- Name: FK_players.ProfileItemEntity_players.InstanceOfPlayerItem_ItemI; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "ProfileItemEntity"
    ADD CONSTRAINT "FK_players.ProfileItemEntity_players.InstanceOfPlayerItem_ItemI" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2664 (class 2606 OID 227575)
-- Name: FK_players.ProfileItemEntity_players.PlayerProfileEntity_Profil; Type: FK CONSTRAINT; Schema: players; Owner: postgres
--

ALTER TABLE ONLY "ProfileItemEntity"
    ADD CONSTRAINT "FK_players.ProfileItemEntity_players.PlayerProfileEntity_Profil" FOREIGN KEY ("ProfileId") REFERENCES "PlayerProfileEntity"("AssetId") ON DELETE CASCADE;


SET search_path = public, pg_catalog;

--
-- TOC entry 2679 (class 2606 OID 227670)
-- Name: FK_public.ClusterNodeEntity_matches.MatchEntity_ActiveMatchId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterNodeEntity"
    ADD CONSTRAINT "FK_public.ClusterNodeEntity_matches.MatchEntity_ActiveMatchId" FOREIGN KEY ("ActiveMatchId") REFERENCES matches."MatchEntity"("MatchId");


--
-- TOC entry 2678 (class 2606 OID 227665)
-- Name: FK_public.ClusterNodeEntity_public.ClusterInstanceEntity_Cluste; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "ClusterNodeEntity"
    ADD CONSTRAINT "FK_public.ClusterNodeEntity_public.ClusterInstanceEntity_Cluste" FOREIGN KEY ("ClusterId") REFERENCES "ClusterInstanceEntity"("ClusterId") ON DELETE CASCADE;


--
-- TOC entry 2685 (class 2606 OID 227705)
-- Name: FK_public.GlobalChatMessage_players.PlayerEntity_EditorId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "FK_public.GlobalChatMessage_players.PlayerEntity_EditorId" FOREIGN KEY ("EditorId") REFERENCES players."PlayerEntity"("PlayerId");


--
-- TOC entry 2686 (class 2606 OID 227710)
-- Name: FK_public.GlobalChatMessage_players.PlayerEntity_SenderId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "FK_public.GlobalChatMessage_players.PlayerEntity_SenderId" FOREIGN KEY ("SenderId") REFERENCES players."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2669 (class 2606 OID 227600)
-- Name: FK_public.PrivateChatMessageData_players.PlayerEntity_ReceiverI; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "FK_public.PrivateChatMessageData_players.PlayerEntity_ReceiverI" FOREIGN KEY ("ReceiverId") REFERENCES players."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2670 (class 2606 OID 227605)
-- Name: FK_public.PrivateChatMessageData_players.PlayerEntity_SenderId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "FK_public.PrivateChatMessageData_players.PlayerEntity_SenderId" FOREIGN KEY ("SenderId") REFERENCES players."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2668 (class 2606 OID 227595)
-- Name: FK_public.PrivateChatMessage_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "FK_public.PrivateChatMessage_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES players."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2667 (class 2606 OID 227590)
-- Name: FK_public.PrivateChatMessage_public.PrivateChatMessageData_Mess; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "FK_public.PrivateChatMessage_public.PrivateChatMessageData_Mess" FOREIGN KEY ("MessageDataId") REFERENCES "PrivateChatMessageData"("MessageId") ON DELETE CASCADE;


--
-- TOC entry 2703 (class 2606 OID 228006)
-- Name: FK_public.QueueMember_matches.MatchMember_MatchMemberId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "QueueMember"
    ADD CONSTRAINT "FK_public.QueueMember_matches.MatchMember_MatchMemberId" FOREIGN KEY ("MatchMemberId") REFERENCES matches."MatchMember"("MemberId");


--
-- TOC entry 2702 (class 2606 OID 227971)
-- Name: FK_public.SessionQueueEntity_matches.MatchEntity_MatchId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "QueueEntity"
    ADD CONSTRAINT "FK_public.SessionQueueEntity_matches.MatchEntity_MatchId" FOREIGN KEY ("MatchId") REFERENCES matches."MatchEntity"("MatchId");


--
-- TOC entry 2701 (class 2606 OID 227976)
-- Name: FK_public.SessionQueueEntity_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "QueueEntity"
    ADD CONSTRAINT "FK_public.SessionQueueEntity_players.PlayerEntity_PlayerId" FOREIGN KEY ("QueueId") REFERENCES players."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2704 (class 2606 OID 227981)
-- Name: FK_public.SessionQueueMember_players.PlayerEntity_PlayerId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "QueueMember"
    ADD CONSTRAINT "FK_public.SessionQueueMember_players.PlayerEntity_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES players."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = store, pg_catalog;

--
-- TOC entry 2656 (class 2606 OID 227535)
-- Name: FK_store.AbstractItemInstance_store.FractionEntity_FractionId; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "FK_store.AbstractItemInstance_store.FractionEntity_FractionId" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2657 (class 2606 OID 227540)
-- Name: FK_store.AddonItemContainer_store.AbstractItemInstance_ItemId_C; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "FK_store.AddonItemContainer_store.AbstractItemInstance_ItemId_C" FOREIGN KEY ("ItemId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2658 (class 2606 OID 227545)
-- Name: FK_store.AddonItemContainer_store.AddonItemInstance_ModelId_Mod; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "FK_store.AddonItemContainer_store.AddonItemInstance_ModelId_Mod" FOREIGN KEY ("ModelId", "ModelCategoryTypeId") REFERENCES "AddonItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2700 (class 2606 OID 227780)
-- Name: FK_store.AddonItemInstance_store.AbstractItemInstance_ModelId_C; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "FK_store.AddonItemInstance_store.AbstractItemInstance_ModelId_C" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2697 (class 2606 OID 227765)
-- Name: FK_store.AmmoItemInstance_store.AbstractItemInstance_ModelId_Ca; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "FK_store.AmmoItemInstance_store.AbstractItemInstance_ModelId_Ca" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2695 (class 2606 OID 227755)
-- Name: FK_store.ArmourItemInstance_store.AbstractItemInstance_ModelId_; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "FK_store.ArmourItemInstance_store.AbstractItemInstance_ModelId_" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2694 (class 2606 OID 227750)
-- Name: FK_store.CharacterItemInstance_store.AbstractItemInstance_Model; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "FK_store.CharacterItemInstance_store.AbstractItemInstance_Model" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2699 (class 2606 OID 227775)
-- Name: FK_store.KitItemInstance_store.AbstractItemInstance_ModelId_Cat; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "FK_store.KitItemInstance_store.AbstractItemInstance_ModelId_Cat" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2698 (class 2606 OID 227770)
-- Name: FK_store.SkinItemInstance_store.AbstractItemInstance_ModelId_Ca; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "FK_store.SkinItemInstance_store.AbstractItemInstance_ModelId_Ca" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


--
-- TOC entry 2696 (class 2606 OID 227760)
-- Name: FK_store.WeaponItemInstance_store.AbstractItemInstance_ModelId_; Type: FK CONSTRAINT; Schema: store; Owner: postgres
--

ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "FK_store.WeaponItemInstance_store.AbstractItemInstance_ModelId_" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2680 (class 2606 OID 227675)
-- Name: FK_vrs.UserClaims_vrs.Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "FK_vrs.UserClaims_vrs.Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2681 (class 2606 OID 227680)
-- Name: FK_vrs.UserLogins_vrs.Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "FK_vrs.UserLogins_vrs.Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2683 (class 2606 OID 227690)
-- Name: FK_vrs.UserRoles_vrs.Roles_RoleId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_vrs.UserRoles_vrs.Roles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "Roles"("Id") ON DELETE CASCADE;


--
-- TOC entry 2682 (class 2606 OID 227685)
-- Name: FK_vrs.UserRoles_vrs.Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_vrs.UserRoles_vrs.Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


-- Completed on 2016-07-20 12:27:19

--
-- PostgreSQL database dump complete
--

