--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-05-31 21:55:01

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 11 (class 2615 OID 46237)
-- Name: Store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Store";


ALTER SCHEMA "Store" OWNER TO postgres;

SET search_path = "Store", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 215 (class 1259 OID 46339)
-- Name: AbstractItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AbstractItemInstance" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 1 NOT NULL,
    "IsAvalible" boolean DEFAULT true NOT NULL,
    "DefaultSkinId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "AbstractItemInstance" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 46351)
-- Name: AmmoItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AmmoItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AmmoItemInstance" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 46354)
-- Name: ArmourItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ArmourItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "ArmourItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 1))
);


ALTER TABLE "ArmourItemInstance" OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 46358)
-- Name: CharacterItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "CharacterItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "CharacterItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 5))
);


ALTER TABLE "CharacterItemInstance" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 46362)
-- Name: FractionEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "FractionEntity" (
    "FractionId" smallint NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" smallint DEFAULT 0 NOT NULL,
    "Discount_Ammo" smallint DEFAULT 0 NOT NULL,
    "Discount_Weapon" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FractionEntity" OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 46370)
-- Name: ModificationEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ModificationEntity" (
    "ModificationId" uuid NOT NULL,
    "ModificationTypeId" smallint DEFAULT 0 NOT NULL,
    "Range_Minimum" smallint DEFAULT 2 NOT NULL,
    "Range_Maximum" smallint DEFAULT 10 NOT NULL,
    "Range_Chance" smallint DEFAULT 50 NOT NULL
);


ALTER TABLE "ModificationEntity" OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 46377)
-- Name: SkinItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "SkinItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "SkinItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 7))
);


ALTER TABLE "SkinItemInstance" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 46381)
-- Name: WeaponItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "WeaponItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "WeaponItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 0))
);


ALTER TABLE "WeaponItemInstance" OWNER TO postgres;

--
-- TOC entry 2234 (class 0 OID 46339)
-- Dependencies: 215
-- Data for Name: AbstractItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 5000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 15000, false, 0, 0, 0, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 0, 50000, false, 0, 0, 0, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 0, 50700, false, 0, 0, 0, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 0, 58700, false, 0, 0, 0, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 0, 5000, false, 0, 0, 0, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 10, 10000, true, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (9, 1, 1000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (10, 1, 1000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (11, 1, 1000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (28, 1, 1000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 0, 5000, false, 0, 0, 0, 1, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 0, 3500, false, 0, 0, 0, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 0, 4500, false, 0, 0, 0, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 0, 7000, false, 0, 0, 0, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 0, 175000, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 0, 23543, false, 0, 0, 0, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 0, 14477, true, 0, 0, 0, 1, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (12, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (13, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (14, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (15, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (16, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (17, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (18, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (19, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (20, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (21, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (22, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (23, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (24, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (25, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (26, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (27, 1, 1000, false, 0, 0, 0, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 1, 1000, false, 0, 0, 0, 0, 7, true, 0);


--
-- TOC entry 2235 (class 0 OID 46351)
-- Dependencies: 216
-- Data for Name: AmmoItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2236 (class 0 OID 46354)
-- Dependencies: 217
-- Data for Name: ArmourItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 1);


--
-- TOC entry 2237 (class 0 OID 46358)
-- Dependencies: 218
-- Data for Name: CharacterItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 5);


--
-- TOC entry 2238 (class 0 OID 46362)
-- Dependencies: 219
-- Data for Name: FractionEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (1, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (2, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (3, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (4, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (0, 0, 0, 0, 0, 0);


--
-- TOC entry 2239 (class 0 OID 46370)
-- Dependencies: 220
-- Data for Name: ModificationEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2240 (class 0 OID 46377)
-- Dependencies: 221
-- Data for Name: SkinItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 7);


--
-- TOC entry 2241 (class 0 OID 46381)
-- Dependencies: 222
-- Data for Name: WeaponItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (20, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (21, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (22, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (23, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (24, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (25, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (26, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (27, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (28, 0);


--
-- TOC entry 2107 (class 2606 OID 46477)
-- Name: AbstractItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "AbstractItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2109 (class 2606 OID 46479)
-- Name: AmmoItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "AmmoItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2111 (class 2606 OID 46481)
-- Name: ArmourItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2113 (class 2606 OID 46483)
-- Name: CharacterItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "CharacterItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2115 (class 2606 OID 46485)
-- Name: FractionEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "FractionEntity_pkey" PRIMARY KEY ("FractionId");


--
-- TOC entry 2117 (class 2606 OID 46487)
-- Name: ModificatioBaseEntity_ModificationTypeId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_ModificationTypeId_key" UNIQUE ("ModificationTypeId");


--
-- TOC entry 2119 (class 2606 OID 46489)
-- Name: ModificatioBaseEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_pkey" PRIMARY KEY ("ModificationId");


--
-- TOC entry 2121 (class 2606 OID 46491)
-- Name: SkinItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "SkinItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2123 (class 2606 OID 46493)
-- Name: WeaponItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "WeaponItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2124 (class 2606 OID 46620)
-- Name: ArmourItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


-- Completed on 2016-05-31 21:55:01

--
-- PostgreSQL database dump complete
--

