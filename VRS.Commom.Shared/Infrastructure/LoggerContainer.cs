﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web;
using NLog;

namespace VRS.Infrastructure
{
    public static class LoggerContainer
    {
        public static Logger GlobalLogger   { get; } = LogManager.GetCurrentClassLogger();
        public static Logger BuildingLogger { get; } = LogManager.GetLogger("BuildingLogger");
        public static Logger ClusterLogger { get; }  = LogManager.GetLogger("ClusterLogger");
        public static Logger SessionLogger { get; }  = LogManager.GetLogger("SessionLogger");
        public static Logger PaymentLogger { get; }  = LogManager.GetLogger("PaymentLogger");
        public static Logger AccountLogger { get; }  = LogManager.GetLogger("AccountLogger");
        public static Logger TutorialLogger { get; } = LogManager.GetLogger("TutorialLogger");
        public static Logger MatchMakingLogger { get; } = LogManager.GetLogger("MatchMakingLogger");

        public static Logger StoreLogger { get; }    = LogManager.GetLogger("StoreLogger");
        public static Logger DataBaseLogger { get; } = LogManager.GetLogger("DataBaseLogger");
        public static void WriteExceptionMessage(this Logger logger, string description, DbEntityValidationException exception)
        {
            var sb = new StringBuilder(2048);
            sb.AppendLine("Db Entity Validation Exception Exception was:");
            sb.AppendLine(exception.ToString());

            var innerEx = exception.InnerException;
            while (innerEx != null)
            {
                sb.AppendLine("Inner Exception : " + innerEx.Message);
                innerEx = innerEx.InnerException;
            }

            if (exception.EntityValidationErrors != null)
            {
                sb.AppendLine("Validation Errors : ");
                foreach (var valErr in exception.EntityValidationErrors)
                {
                    sb.AppendLine(
                        valErr.ValidationErrors.Select(data => data.ErrorMessage)
                            .Aggregate((current, next) => current + "; /n" + next));
                }
            }

            DataBaseLogger.Error($"{description} | exception: {sb}");
            logger.Error($"{description} | exception: {sb}");
        }

        public static void WriteExceptionMessage(this Logger logger, string description, Exception exception)
        {
            if (exception == null)
            {
                logger.Error($"{description}");
            }
            else
            {
                var dbx = exception as DbEntityValidationException;
                if (dbx == null)
                {
                    var sb = new StringBuilder(2048);
                    sb.AppendLine(exception.ToString());

                    var innerEx = exception.InnerException;
                    while (innerEx != null)
                    {
                        sb.AppendLine("Inner Exception : " + innerEx.Message);
                        innerEx = innerEx.InnerException;
                    }

                    logger.Error($"{description} | exception: {sb}");
                }
                else
                {
                    WriteExceptionMessage(logger, description, dbx);
                }
            }
        }
    }
}