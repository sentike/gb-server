﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Match.GameMode
{
    public static class GameModeFlagHeler
    {
        public static bool HasFlag(this GameModeFlags gameModeFlagTypeId, GameModeTypeId gameModeTypeId)
        {
            return gameModeFlagTypeId.HasFlag(gameModeTypeId.AsFlag());
        }

        public static GameModeFlags AsFlag(this GameModeTypeId value)
        {
            return (GameModeFlags)(1 << (int)value);
        }

        //public static GameModeTypeId AsValue(this GameModeFlags flag)
        //{
        //    return (GameModeTypeId)(1 >> (int)flag);
        //}
    }

    public enum GameModeTypeId
    {
        None,

        [Display(Name = "Коммандный бой")]
        TeamDeadMatch,

        [Display(Name = "Последний герой")]
        LostDeadMatch,

        [Display(Name = "Исследование")]
        ResearchMatch,

    }

    [Flags]
    public enum GameModeFlags
    {
        None,

        [Display(Name = "Коммандный бой")]
        TeamDeadMatch = 1 << GameModeTypeId.TeamDeadMatch, // 2

        [Display(Name = "Последний герой")]
        LostDeadMatch = 1 << GameModeTypeId.LostDeadMatch, // 4

        [Display(Name = "Исследование")]
        ResearchMatch = 1 << GameModeTypeId.ResearchMatch, // 8

        All = TeamDeadMatch | LostDeadMatch | ResearchMatch 
    }
}