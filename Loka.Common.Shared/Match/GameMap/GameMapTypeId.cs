﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Match.GameMap
{
    public enum GameMapTypeId
    {
        None,
        Hangar = 1,
        x1_1 = 2,
        HangarOutside = 3,
        MilitaryBase = 4,
        Coastline = 5,
        Lobby = 6,
        CoastlineCity = 7,
        End
    };

    public enum GameMapFlags
    {
        None = 0,
        Hangar = 1 << GameMapTypeId.Hangar,
        x1_1 = 1 << GameMapTypeId.x1_1,
        HangarOutside = 1 << GameMapTypeId.HangarOutside,
        MilitaryBase = 1 << GameMapTypeId.MilitaryBase,
        Coastline = 1 << GameMapTypeId.Coastline,
        CoastlineCity = 1 << GameMapTypeId.CoastlineCity,

        Lobby = 1 << GameMapTypeId.Lobby,

        End = 1 << GameMapTypeId.End,
    }
}