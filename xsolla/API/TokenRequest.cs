﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmazedSaint.Elastic.Lib;
using Newtonsoft.Json.Linq;

namespace xsolla
{
    public class TokenRequest
    {
        private dynamic data = new JObject();

        private TokenRequest()
        {
            data.user = new JObject();
            data.user.id = new JObject();
            data.user.email = new JObject();
            data.user.name = new JObject();


            data.settings = new JObject();
            //data.custom_parameters = new JObject();
            data.purchase = new JObject();

            data.purchase = new JObject();
            data.purchase.checkout = new JObject();
        }

        public TokenRequest(long projectId, string userId) : this()
        {
            data.user.id.value = userId;
            data.settings.project_id = projectId;
        }

        public TokenRequest setUserEmail(string value)
        {
            data.user.email.value = value;
            return this;
        }

        public TokenRequest setUserName(string value)
        {
            data.user.name.value = value;
            return this;
        }

        public TokenRequest setCurrency(string value)
        {
            data.settings.currency = value;
            return this;
        }

        public TokenRequest setCustomParameters(dynamic value)
        {
            data.custom_parameters = value;
            return this;
        }

        public TokenRequest setExternalPaymentId(string value)
        {
            data.settings.external_id = value;
            return this;
        }

        public TokenRequest setSandboxMode(bool isSandbox = true)
        {
            if (isSandbox)
            {
                data.settings.mode = "sandbox";
            }
            else
                data.settings.mode = null;
            return this;
        }

        public TokenRequest setPurchase(long amount, string currency)
        {
            data.purchase.checkout.amount = amount;
            data.purchase.checkout.currency = currency;

            return this;
        }

        public dynamic toArray() => this.data;
    }
}
