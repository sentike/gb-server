﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AmazedSaint.Elastic.Lib;
using Newtonsoft.Json.Linq;
using static System.String;

namespace xsolla
{
    public class XsollaClient
    {
        protected long MerchantId;

        private HttpClient Client;


        public struct FXsollaConfig
        {
            public FXsollaConfig(int merchantId, string apiKey)
            {
                Sandbox = false;
                ProjectId = 0;
                ProjectKey = string.Empty;
                MerchantId = merchantId;
                ApiKey = apiKey;
            }

            public bool Sandbox;

            public int MerchantId;
            public string ApiKey;

            public long ProjectId;
            public string ProjectKey;
        }

        public void Factory(FXsollaConfig config)
        {
            MerchantId = config.MerchantId;
            Client = new HttpClient();
            Client.BaseAddress = new Uri(Settings.BaseUrl);
            Client.DefaultRequestHeaders.Add("Authorization", $"Basic {Convert.ToBase64String(Encoding.ASCII.GetBytes($"{config.MerchantId}:{config.ApiKey}"))}");
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            Client.DefaultRequestHeaders.Add("User-Agent", Settings.Version);
        }

        public dynamic createCommonPaymentUIToken(long projectId, string userId, bool sandboxMode = false)
        {
            var tokenRequest = new TokenRequest(projectId, userId);
            tokenRequest.setSandboxMode(sandboxMode);

            return createPaymentUITokenFromRequest(tokenRequest);
        }

        public async Task<string> createPaymentUITokenFromRequest(TokenRequest tokenRequest)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, $"/merchant/merchants/{MerchantId}/token");
            request.Content = new StringContent(tokenRequest.toArray().ToString(), Encoding.UTF8,"application/json");
            var parsedResponse = await Client.SendAsync(request); ;
            var json = JObject.Parse(await parsedResponse.Content.ReadAsStringAsync());

            return json["token"].ToString();
        }


    }
}
