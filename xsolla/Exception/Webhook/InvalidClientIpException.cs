﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace xsolla.Exception
{
    public class InvalidClientIpException : XsollaWebhookException
    {
        public InvalidClientIpException(string message, System.Exception innerException = null) 
            : base(message, innerException)
        {
        }

        public override string ErrorCode => "SERVER_ERROR";
        public override HttpStatusCode HttpStatusCode => HttpStatusCode.InternalServerError;
    }
}
