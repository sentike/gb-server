﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace xsolla.Exception
{
    public abstract class XsollaException : System.Exception
    {
        protected XsollaException(string message, System.Exception innerException = null) : base(message, innerException)
        {
            
        }

        public virtual string ErrorCode { get; }

        public virtual HttpStatusCode HttpStatusCode { get; }
    }
}
