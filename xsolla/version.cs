﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xsolla
{
    public static class Settings
    {
        public static xsolla.XsollaClient.FXsollaConfig XsollaConfig;
        public const string BaseUrl = "https://api.xsolla.com";

        public const string Version = "xsolla-api-client/1.0";
    }
}
