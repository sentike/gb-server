﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace xsolla.Webhook
{
    public class WebhookServerEventArgs : EventArgs
    {
        public Message Message;
    }

    public delegate void WebhookServerHandler(object sender, WebhookServerEventArgs e);


    public class WebhookServer
    {
        protected WebhookAuthenticator webhookAuthenticator;

        protected event WebhookServerHandler webhookCallback;


        public static WebhookServer create(WebhookServerHandler handler, string projectSecretKey)
        {
            return new WebhookServer(handler, new WebhookAuthenticator(projectSecretKey));
        }

        public WebhookServer(WebhookServerHandler handler, WebhookAuthenticator webhookAuthenticator)
        {
            this.webhookAuthenticator = webhookAuthenticator;
            this.webhookCallback = handler;
        }

        public void start(WebhookRequest webhookRequest = null, bool authenticateClientIp = true)
        {
            var response = this.getSymfonyResponse(webhookRequest, authenticateClientIp);

            foreach (var v in response.Headers)
            {
                HttpContext.Current.Response.Headers.Add(v.Key, v.Value.First());
            }
        }

        public HttpResponseMessage getSymfonyResponse(WebhookRequest webhookRequest = null,
            bool authenticateClientIp = true)
        {
            try
            {
                if(webhookRequest == null)
                    webhookRequest = WebhookRequest.fromGlobals();

                webhookAuthenticator.authenticate(webhookRequest, authenticateClientIp);
                var message = Message.fromArray(webhookRequest.toArray());
                webhookCallback(this, new WebhookServerEventArgs() {Message = message });

                var webhookResponse = new WebhookResponse();
                return webhookResponse.symfonyResponse;
            }
            catch (System.Exception exception)
            {
                return WebhookResponse.fromException(exception).symfonyResponse;
            }
        }



    }
}
