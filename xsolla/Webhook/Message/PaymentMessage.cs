﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace xsolla.Webhook
{
    public class PaymentMessage : Message
    {
        public PaymentMessage(object request) : base(request) { }

        public dynamic Purchase => request.purchase;
        public dynamic Transaction => request.transaction;
        public long PaymentId => Transaction.id;
        public string ExternalPaymentId => Transaction.external_id ?? null;
        public dynamic PaymentDetails => request.payment_details;
        public dynamic CustomParameters => request.custom_parameters ?? new  JObject();
        public bool DryRun => Transaction.dry_run ?? false;
    }
}
