﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace xsolla.Webhook
{
    public class CreateSubscriptionMessage : CancelSubscriptionMessage
    {
        public CreateSubscriptionMessage(object request) : base(request) { }

        public dynamic Coupon => request.coupon ?? new JObject();
    }
}
