﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace xsolla.Webhook
{
    class RefundMessage : PaymentMessage
    {
        public RefundMessage(object request) : base(request) { }

        public dynamic RefundDetails => request.refund_details;


    }
}
