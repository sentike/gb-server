﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace xsolla.Webhook
{
    public class UserBalanceMessage : Message
    {
        protected const string IN_GAME_PURCHASE = "inGamePurchase";
        protected const string COUPON = "coupon";
        protected const string INTERNAL = "internal";
        protected const string CANCELLATION = "cancellation";

        public UserBalanceMessage(object request) : base(request) { }

        public dynamic VirtualCurrencyBalance => request.virtual_currency_balance ?? new JObject();
        public string OperationType => request.operation_type;
        public long OperationId => request.id_operation;
        public dynamic Coupon => request.coupon ?? new JObject();
        public string ItemsOperationType => request.items_operation_type;

    }
}
