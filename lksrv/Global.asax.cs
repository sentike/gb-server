﻿using System;
using System.Linq;
using System.Threading;
using Loka.Server.Infrastructure.DataRepository;


namespace Loka.Server
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        //public static readonly Mutex MatchMakingMutex = new Mutex();
        //public static readonly Thread MatchMakingThread = new Thread(StoreRepository.OnMatchMakingProcess);
        public static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            Logger.Info("Application Start");
            try
            {
                using (var db = new DataRepository())
                {
                    var last24Hour = DateTime.UtcNow.AddHours(-24);
                    var invites = db.SquadInviteEntities.RemoveRange(db.SquadInviteEntities.Where(i => i.SubmittedDate <= last24Hour)).LongCount();
                    var sessions = db.GameSessionEntity.RemoveRange(db.GameSessionEntity.Where(i => i.LastActivityDate <= last24Hour)).LongCount();

                    Logger.Info($"Deleted {invites} squad invites");
                    Logger.Info($"Deleted {sessions} game sessions");

                    db.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        protected void Application_End()
        {
            Logger.Info("Application End");
            //try
            //{
            //    MatchMakingThread.Abort();
            //}
            //catch (Exception exception)
            //{
            //    Logger.Error(exception);
            //    throw;
            //}
            
            //using (var db = new DataRepository())
            //{
            //    db.GameSessionEntity.RemoveRange(db.GameSessionEntity.AsNoTracking());
            //    db.SaveChanges();
            //}
        }
    }
}
