﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using Loka.Server.Infrastructure.DataRepository;

using Loka.Server.Infrastructure;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Quartz;
using Quartz.Impl;
using VRS.Infrastructure;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Loka.Server.NinjectWebCommon), "Launch")]

namespace Loka.Server
{
    using System;
    using System.Web;
    using Loka.Server.Models.Session;
    using Loka.Server.Service;
    using Ninject;
    using Ninject.Web.Common;

    //[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Loka.Server.App_Start.NinjectWebCommon), "Stop")]

    public static class NinjectWebCommon
    {
        public static readonly Bootstrapper Bootstrapper = new Bootstrapper();
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            Bootstrapper.Initialize(CreateKernel);
            Task.Run(async () =>
            {
                try
                {
                    //===============================================================
#if !DEVELOPMENT_BUILD
                    await PlayerRatingBuffer.Instance.CkeckUpdate(true);
                    await PlayerEloRatingBuffer.Instance.CkeckUpdate(true);
#endif
                    GooglePlayPayments.UpdateMarketCosts();

                    //===============================================================
                    var scheduler = await StdSchedulerFactory.GetDefaultScheduler();
                    if (scheduler == null)
                    {
                        LoggerContainer.ClusterLogger.Error($"[NinjectWebCommon][Start]scheduler was nullptr");
                    }
                    else
                    {
                        //===============================================================
                        ITrigger making = TriggerBuilder.Create() // создаем триггер
                            .WithIdentity("MatchMaking", "MatchMakingGroup")
                            .StartNow()
                            .WithSimpleSchedule(x => x
                                .WithIntervalInSeconds(15)
                                .RepeatForever())
                            .Build(); // создаем триггер



                        //===============================================================
                        ITrigger monitor = TriggerBuilder.Create() // создаем триггер
                            .WithIdentity("MatchMonitorJob", "MatchMonitorGroup")
                            .StartNow()
                            .WithSimpleSchedule(x => x
                            .WithIntervalInSeconds(5)
                            .RepeatForever())
                            .Build(); // создаем триггер

                        //===============================================================
                        ITrigger updater = TriggerBuilder.Create() // создаем триггер
                            .WithIdentity("BuildUpdaterJob", "BuildUpdateGroup")
                            .StartNow()
                            .WithSimpleSchedule(x => x
                                .WithIntervalInSeconds(5)
                                .RepeatForever())
                            .Build(); // создаем триггер

                        //===============================================================
                        ITrigger rating = TriggerBuilder.Create() // создаем триггер
                            .WithIdentity("PlayerRatingUpdateJob", "PlayerRatingUpdateGroup")
                            .StartNow()
                            .WithSimpleSchedule(x => x
                                .WithIntervalInMinutes(2)
                                .RepeatForever())
                            .Build(); // создаем триггер

                        

                        await scheduler.Clear();
                        await scheduler.ScheduleJobs(new Dictionary<IJobDetail, IReadOnlyCollection<ITrigger>>()
                        {
                            {JobBuilder.Create<PlayerRatingUpdateJob>().WithIdentity("PlayerRatingUpdateJob").Build(), new HashSet<ITrigger> { rating } },
                            {JobBuilder.Create<MatchMonitorJob>().WithIdentity("MatchMonitorJob").Build(), new HashSet<ITrigger> { monitor } },
                            {JobBuilder.Create<MatchMakingJob>().WithIdentity("MatchMakingJob").Build(), new HashSet<ITrigger> { making } },
                            {JobBuilder.Create<ServerUpdateJob>().WithIdentity("BuildUpdaterJob").Build(), new HashSet<ITrigger> { updater } }
                        }, true);

                        await scheduler.Start();
                    }
                }
                catch (Exception e)
                {
                    LoggerContainer.ClusterLogger.WriteExceptionMessage("[NinjectWebCommon][Start]", e);
                }
            }).Wait();

        }

        public static void Launch()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
            Task.Run(async () =>
            {
                var scheduler = await StdSchedulerFactory.GetDefaultScheduler();
                if (scheduler != null)
                {
                    await scheduler.Shutdown(false);
                }
            });
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        internal static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {

                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<AuthorizationModule>();
                
                RegisterServices(kernel);

                return kernel;
            }
            catch (Exception exception)
            {
                logger.Error($"Ninject Error: {exception.Message}");
                kernel.Dispose();
                throw;
            }
        }
        //
        public static void RegisterNinject(HttpConfiguration configuration)
        {
           // IDependencyResolver qq =

            var dependencyResolver = new Ninject.Web.WebApi.NinjectDependencyResolver(Bootstrapper.Kernel);
            //------------------------------------------------------------------------
            //GlobalHost.DependencyResolver = new Microsoft.AspNet.SignalR.Ninject.NinjectDependencyResolver(kernel);
            //DependencyResolver.SetResolver(dependencyResolver);
            configuration.DependencyResolver = dependencyResolver;
        }

       

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            DAL.RegisterServices(kernel);
            logger.Info("Ninject: register services complete");

            //var bResolved = kernel.Get<IDAL>() != null;
            //logger.Info($"Ninject: IDAL is resolved: {bResolved}");


            // Binding services here
        }
    }
}