﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Infrastructure;

namespace Loka.Server.Infrastructure
{
    public class AbstractControllerException<TExceptionResponseCode> : Exception
        where TExceptionResponseCode : struct, IConvertible 
    {
        public readonly TExceptionResponseCode ResponseCode;

        public AbstractControllerException(TExceptionResponseCode responseCode)
        {
            ResponseCode = responseCode;
        }
    }

    public class AbstractControllerException<TExceptionResponseCode, TExceptionResponseValue> : AbstractControllerException<TExceptionResponseCode>
    where TExceptionResponseCode : struct, IConvertible
    {
        public readonly FReqestOneParam<TExceptionResponseValue> Value;

        public AbstractControllerException(TExceptionResponseCode responseCode, FReqestOneParam<TExceptionResponseValue> message)
            : base(responseCode)
        {
            Value = message;
        }
    }

}