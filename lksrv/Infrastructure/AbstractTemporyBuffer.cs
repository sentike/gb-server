﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Loka.Infrastructure;

namespace Loka.Server.Infrastructure
{
    public abstract class AbstractTemporyBuffer<TObject, TResultObject, TDataKey> 
        where TObject : class
    {
        public int UpdateIntevalInSeconds { get; protected set; }
        public DateTime UpdateDate { get; protected set; }
        public TObject Object { get; protected set; }

        protected AbstractTemporyBuffer(int updateIntevalInSeconds)
        {
            UpdateIntevalInSeconds = updateIntevalInSeconds;
        }

        public abstract Task<TResultObject> GetData(TDataKey InKey);

        public virtual async Task CkeckUpdate(bool InForceUpdate = false)
        {
            if (Object == null || InForceUpdate || UpdateDate.ToElapsedSeconds() > UpdateIntevalInSeconds)
            {
                UpdateDate = DateTime.UtcNow;
                await Update();
            }
        }

        protected abstract Task Update();
    }
}