﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace Loka.Server.Infrastructure
{
    [Flags]
    public enum MinidumpType
    {
        MiniDumpNormal = 0x00000000,
        MiniDumpWithDataSegs = 0x00000001,
        MiniDumpWithFullMemory = 0x00000002,
        MiniDumpWithHandleData = 0x00000004,
        MiniDumpFilterMemory = 0x00000008,
        MiniDumpScanMemory = 0x00000010,
        MiniDumpWithUnloadedModules = 0x00000020,
        MiniDumpWithIndirectlyReferencedMemory = 0x00000040,
        MiniDumpFilterModulePaths = 0x00000080,
        MiniDumpWithProcessThreadData = 0x00000100,
        MiniDumpWithPrivateReadWriteMemory = 0x00000200,
        MiniDumpWithoutOptionalData = 0x00000400,
        MiniDumpWithFullMemoryInfo = 0x00000800,
        MiniDumpWithThreadInfo = 0x00001000,
        MiniDumpWithCodeSegs = 0x00002000
    }

    public class MiniDumpDemo
    {
        [DllImport("dbghelp.dll")]
        static extern bool MiniDumpWriteDump(
            IntPtr hProcess,
            Int32 processId,
            IntPtr hFile,
            MinidumpType dumpType,
            IntPtr exceptionParam,
            IntPtr userStreamParam,
            IntPtr callackParam);

        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static void MiniDumpToFile(string fileToDump, MinidumpType type = MinidumpType.MiniDumpNormal)
        {
            try
            {
                FileStream fsToDump = File.Exists(fileToDump) ? File.Open(fileToDump, FileMode.Append) : File.Create(fileToDump);
                Debug.Assert(fsToDump.SafeFileHandle != null, "SafeFileHandle was null");

                Process thisProcess = Process.GetCurrentProcess();
                MiniDumpWriteDump(thisProcess.Handle, thisProcess.Id,
                    fsToDump.SafeFileHandle.DangerousGetHandle(), type,
                    IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
                fsToDump.Close();

            }
            catch (Exception e)
            {
                Logger.Error($"#MiniDumpToFile: {fileToDump} | {type} | e: {e}");
            }
        }
    };
}