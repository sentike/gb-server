﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SteamSharp
{
    /// <summary>
    /// Information about the application you can get the owner of the application
    /// </summary>
    public class App
    {
        /// <summary>
        /// Unique ID of the Application (synonymous with AppID).
        /// </summary>
        [JsonProperty("appid")]
        public int ApplicationId { get; set; }

        [JsonProperty("ownsapp")]
        public bool IsApplicationOwned { get; set; }

        [JsonProperty("permanent")]
        public bool IsApplicationPermanent { get; set; }

        [JsonProperty("timestamp")]
        public DateTime OwnedTimestamp { get; set; }

        [JsonProperty("ownersteamid")]
        public long OwnerSteamId { get; set; }
    }

    /// <summary>
    /// A container for a response, which contains the container application
    /// </summary>
    public class GetOwnedGamesOwnershipResponseContainer
    {
        /// <summary>
        /// Information about the specified Steam user's games library.
        /// </summary>
        [JsonProperty("appownership")]
        public AppContainer Response { get; set; }
    }

    /// <summary>
    /// The container for the application, which contains an array of application property
    /// </summary>
    public class AppContainer
    {
        [JsonProperty("apps")]
        public AppCollectionContainer Apps { get; set; }
    }

    /// <summary>
    /// The container for the application, which contains an array of application property
    /// </summary>
    public class AppCollectionContainer
    {
        [JsonProperty("app")]
        public List<App> Application { get; set; }
        public int Count => Application.Count;
    }

    //public class AppOwnership : SteamInterface
    //{
    //    public async static Task<AppContainer> GetOwnedApplicationsAsync(SteamClient client, string steamID)
    //    {
    //        //client.IsAuthorizedCall(new Type[] {
	//		//	typeof( Authenticators.APIKeyAuthenticator ) // Executes in API context (public)
	//		//});
    //
    //        SteamRequest request = new SteamRequest(SteamAPIInterface.ISteamUser, "GetPublisherAppOwnership", SteamMethodVersion.v0001);
    //        request.AddParameter("steamid", steamID, ParameterType.QueryString);
    //
    //        return VerifyAndDeserialize<GetOwnedGamesOwnershipResponseContainer>((await client.ExecuteAsync(request))).Response;
    //
    //    }
    //}
}
