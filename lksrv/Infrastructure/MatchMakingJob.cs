using System;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using Loka.Common.Cluster;
using Loka.Common.Match.GameMode;
using Loka.Infrastructure;
using VRS.Infrastructure;

namespace Loka.Server.Infrastructure
{
    public class MatchMakingJob : IJob
    {
        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public static GameModeTypeId LastGameMode { get; set; } = GameModeTypeId.None;
        public static DateTime? LastDeleteFinishedMatches { get; set; }

        public Task Execute(IJobExecutionContext context)
        {
            LoggerContainer.MatchMakingLogger.Info("============ OnMatchMakingProcess v4.5 =====================\n");

            //if (Environment.MachineName.Equals("WIN-MasterSrv", StringComparison.OrdinalIgnoreCase) == false)
            //{
            //    Logger.Info($"OnMatchMakingProcess abort, MachineName: {Environment.MachineName} != WIN-MasterSrv\n");
            //    return Task.CompletedTask;
            //}

            try
            {
                if (LastDeleteFinishedMatches == null || LastDeleteFinishedMatches.Value.ToLocalElapsedSeconds() > 30 * 60)
                {               
                    using (var db = new DataRepository.DataRepository())
                    {
                        //=========================================
                        var now = DateTime.UtcNow.AddDays(-3);
                        var finished = db.MatchEntity.Where(p => p.Created <= now || p.Finished <= now).ToArray();

                        //=========================================
                        db.MatchEntity.RemoveRange(finished);

                        //=========================================
                        var counted = db.SaveChanges();

                        //=========================================
                        if (finished.Any())
                        {
                            LoggerContainer.MatchMakingLogger.Info($"[OnMatchMakingProcess][Delete finished matches <= {now}] | Array: {finished.Length} | Counted: {counted} | LastDeleteFinishedMatches: {LastDeleteFinishedMatches}");
                        }
                    }

                    //=========================================
                    LastDeleteFinishedMatches = DateTime.UtcNow;
                }
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"OnMatchMakingProcess[0]", e);
            }

            try
            {
                using (var db = new DataRepository.DataRepository())
                {
                    //var gm = db.GameModeEntity.Shuffle().FirstOrDefault(p => p.GameModeId != LastGameMode);
                    //if (gm != null)
                    //{
                    //    try
                    //    {
                    //        LastGameMode = gm.GameModeId;
                    //        gm.Tick();
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"OnMatchMakingProcess[{gm.GameModeFlag}][0]", e);
                    //    }
                    //
                    //}

                    var gms = db.GameModeEntity/*.Where(p => p.GameModeId != LastGameMode)*/.Shuffle().ToArray();
                    foreach (var mode in gms)
                    {
                        try
                        {
                            mode.Tick();
                        }
                        catch (Exception e)
                        {
                            LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"OnMatchMakingProcess[{mode.GameModeFlag}][1]", e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"OnMatchMakingProcess[2]", e);
            }

            return Task.CompletedTask;
        }
    }
}