﻿using Loka.Server.Models.Session;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using VRS.Infrastructure;

namespace Loka.Server.Infrastructure
{
    public class PlayerRatingUpdateJob : IJob
    {
#if !DEVELOPMENT_BUILD
        public async Task Execute(IJobExecutionContext context)
        {
            //==================================================================
            try
            {
                var StartDate = DateTime.UtcNow;
                await PlayerRatingBuffer.Instance.CkeckUpdate(true);
                var EndDate = DateTime.UtcNow;

                LoggerContainer.StoreLogger.Warn($"[PlayerRatingUpdateJob] Elapsed: {EndDate - StartDate}");
            }
            catch (Exception e)
            {
                LoggerContainer.DataBaseLogger.WriteExceptionMessage("PlayerRatingUpdateJob", e);
            }

            //==================================================================
            try
            {
                var StartDate = DateTime.UtcNow;
                await PlayerEloRatingBuffer.Instance.CkeckUpdate(true);
                var EndDate = DateTime.UtcNow;

                LoggerContainer.StoreLogger.Warn($"[PlayerEloRatingBuffer] Elapsed: {EndDate - StartDate}");
            }
            catch (Exception e)
            {
                LoggerContainer.DataBaseLogger.WriteExceptionMessage("PlayerEloRatingBuffer", e);
            }
        }
#else
        public Task Execute(IJobExecutionContext context)
        {
            return Task.CompletedTask;
        }
#endif
    }
}