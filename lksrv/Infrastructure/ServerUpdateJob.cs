﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using CliWrap;
using CliWrap.Models;
using Loka.Common.Match.Match;
using Loka.Common.Version;
using Loka.Infrastructure;
using Quartz;
using SharpSvn;
using SharpSvn.Implementation;
using VRS.Infrastructure;

namespace Loka.Server.Infrastructure
{
    public class ServerUpdateJob : IJob
    {
        //======================================
        public static string ServerRepositoryLocation { get; } = HostingEnvironment.MapPath(@"~/Server/LokaGameServer");
        public static string ServerRepositoryUrl { get; } = "http://136.243.2.83:8099/svn/GunsOfBoomGameServer/branches";
        public static NetworkCredential NetworkCredential { get; } = new NetworkCredential("buildserver", "buildserver");

        /// <summary>
        ///   Удаляет заданный каталог и, при наличии соответствующей инструкции, все подкаталоги и файлы в нем.
        /// </summary>
        /// <param name="path">
        ///   Имя каталога, который необходимо удалить.
        /// </param>
        /// <exception cref="T:System.IO.IOException">
        ///   Файл с тем же именем и расположении, заданном <paramref name="path" />, уже существует.
        /// 
        ///   -или-
        /// 
        ///   Каталог, заданный параметром <paramref name="path" />, доступен только для чтения, или <paramref name="recursive" /> имеет значение <see langword="false" /> и <paramref name="path" /> не является пустым каталогом.
        /// 
        ///   -или-
        /// 
        ///   Каталог является текущим рабочим каталогом приложения.
        /// 
        ///   -или-
        /// 
        ///   Каталог содержит файл только для чтения.
        /// 
        ///   -или-
        /// 
        ///   Каталог используется другим процессом.
        /// </exception>
        /// <exception cref="T:System.UnauthorizedAccessException">
        ///   У вызывающего объекта отсутствует необходимое разрешение.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        ///   <paramref name="path" /> представляет собой строку нулевой длины, содержащую только пробелы или один или несколько недопустимых символов.
        ///    Вы можете запросить недопустимые символы с помощью метода <see cref="M:System.IO.Path.GetInvalidPathChars" />.
        /// </exception>
        /// <exception cref="T:System.ArgumentNullException">
        ///   Свойство <paramref name="path" /> имеет значение <see langword="null" />.
        /// </exception>
        /// <exception cref="T:System.IO.PathTooLongException">
        ///   Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе.
        ///    Например, для платформ на основе Windows длина пути не должна превышать 248 знаков, а имен файлов — 260 знаков.
        /// </exception>
        /// <exception cref="T:System.IO.DirectoryNotFoundException">
        ///   Параметр <paramref name="path" /> не существует или не найден.
        /// 
        ///   -или-
        /// 
        ///   Указан недопустимый путь (например, ведущий на несопоставленный диск).
        /// </exception>
        public void DeleteDirectory(string path)
        {
            if (File.Exists(path) || Directory.Exists(path))
            {
                File.SetAttributes(path, FileAttributes.Normal);

                string[] files = Directory.GetFiles(path);
                string[] dirs = Directory.GetDirectories(path);

                foreach (string file in files)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }

                foreach (string dir in dirs)
                {
                    DeleteDirectory(dir);
                }

                Directory.Delete(path, false);
            }
        }

        //======================================
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                //======================================
                try
                {
                    using (var db = new DataRepository.DataRepository())
                    {
                        //=====================================================
                        var DeprecatedVersions = await db.ServerVersionEntities.Where(p =>
                            p.Flags.HasFlag(ServerVersionFlags.Published) == false &&
                            p.Flags.HasFlag(ServerVersionFlags.Deleted) == false).ToArrayAsync();
                    
                        //=====================================================
                        foreach (var deprecated in DeprecatedVersions)
                        {
                            await DeleteDeprecatedBranch(db, deprecated);
                        }
                    }
                }
                catch (Exception e)
                {
                    LoggerContainer.ClusterLogger.WriteExceptionMessage($"[ServerUpdateJob][DeleteDeprecatedBranch][1]", e);
                }

                //======================================
                try
                {
                    using (var db = new DataRepository.DataRepository())
                    {
                        //=====================================================
                        var server = Path.Combine(ServerRepositoryLocation, "Server_");

                        //=====================================================
                        var folders = Directory.GetDirectories(ServerRepositoryLocation).Where(p => p.Contains("Server_")).ToArray();
                        var branches = folders.Select(p => p.Replace(server, String.Empty)).ToArray();

                        LoggerContainer.ClusterLogger.Debug($"[ServerUpdateJob][DeleteDeprecatedBranch]{Environment.NewLine}[ids: {string.Join(", ", branches)}]");


                        //  10, 11, 12, 13, 14, 15
                        var ids = branches.Select(long.Parse).OrderBy(p => p).ToArray();

                        //=====================================================
                        //  10, 15
                        var published = await db.ServerVersionEntities.Where(p => p.Flags.HasFlag(ServerVersionFlags.Published)).Select(p => p.EntityId).OrderBy(p => p).ToArrayAsync();

                        //=====================================================
                        //  11, 12, 13, 14
                        var delete = ids.Except(published).OrderBy(p => p).ToArray();

                        //=====================================================
                        LoggerContainer.ClusterLogger.Debug($"[ServerUpdateJob][DeleteDeprecatedBranch]{Environment.NewLine}[ids: {string.Join(", ", published)}][pub: {string.Join(", ", published)}]{Environment.NewLine}[del: {string.Join(", ", delete)}]");

                        //=====================================================
                        List<Task> deletes = new List<Task>(128);

                        //=====================================================
                        foreach (var version in delete)
                        {
                            //deletes.Add(Task.Run(() =>
                            {
                                try
                                {
                                    SvnDeleteBranch(ServerRepositoryLocation, version);
                                }
                                catch (Exception e)
                                {
                                    LoggerContainer.ClusterLogger.WriteExceptionMessage(
                                        $"Failed delete local branch {version}", e);
                                }
                            }
                            //));
                        }

                        //=====================================================
                        //Task.WaitAll(deletes.ToArray());

                    }
                }
                catch (Exception e)
                {
                    LoggerContainer.ClusterLogger.WriteExceptionMessage($"[ServerUpdateJob][DeleteDeprecatedBranch][2]", e);
                }

                //======================================
                using (var db = new DataRepository.DataRepository())
                {
                    //=====================================================
                    var UpdateRequiredVersions = await db.ServerVersionEntities.AsNoTracking().Where(p =>
                        p.Flags.HasFlag(ServerVersionFlags.Published) &&
                        p.Flags.HasFlag(ServerVersionFlags.Updated) == false).Select(p => p.EntityId).ToArrayAsync();

                    //=====================================================
                    foreach (var id in UpdateRequiredVersions)
                    {
                        try
                        {
                            using (var db2 = new DataRepository.DataRepository())
                            {
                                var entity = await db2.ServerVersionEntities.SingleOrDefaultAsync(p => p.EntityId == id);
                                await DownloadOrUpdate(db2, entity);
                            }
                        }
                        catch (Exception e)
                        {
                            LoggerContainer.ClusterLogger.WriteExceptionMessage($"DownloadOrUpdate[{id}]", e);
                        }
                    }
                }

                //======================================
                //try
                //{
                //    SvnUpdate(ServerRepositoryLocation);
                //}
                //catch (Exception e)
                //{
                //    LoggerContainer.ClusterLogger.WriteExceptionMessage($"[ServerUpdateJob][SvnUpdate]", e);
                //}

                //======================================
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage("ServerUpdateJob", e);
            }

            //======================================
        }

        //======================================
        private async Task DownloadOrUpdate(DataRepository.DataRepository db, ServerVersionEntity entity)
        {
            //===============================
            if (entity.HasUpdating && entity.LastUpdateDate.ToElapsedSeconds() < 180)
            {
                return;
            }

            //===============================
            entity.HasUpdating = true;
            entity.LastUpdateDate = DateTime.UtcNow;
            await db.SaveChangesAsync();

            //===============================
            var data = await db.MatchEntity.AsNoTracking().Where(p => p.Options.TargetServerVersion == entity.EntityId).ToArrayAsync();
            var ActiveMatches = data.Where(p => p.IsCompleteState == false).ToArray();

            //===============================
            List<MatchEntity> DeletedMatches = new List<MatchEntity>(ActiveMatches.Length);

            //===============================
            foreach (var match in ActiveMatches)
            {
                var ServerProcess = Process.GetProcessById(match.ProcessId);

                //====================================
                if (ServerProcess == null)
                {
                    DeletedMatches.Add(match);
                }

                //====================================
                if (ServerProcess.HasExited)
                {
                    DeletedMatches.Add(match);
                }

                if (ServerProcess.ProcessName.Contains("ShooterGameServer") == false)
                {
                    DeletedMatches.Add(match);
                }
            }

            //===============================
            var bAllowUpdate = ActiveMatches.Length == DeletedMatches.Count;

            //===============================
            LoggerContainer.ClusterLogger.Error($"[DownloadOrUpdate][{entity.EntityId}][bAllowUpdate: {bAllowUpdate} | ActiveMatches: {ActiveMatches.Length} | DeletedMatches: {DeletedMatches.Count}]");

            //===============================
            if (bAllowUpdate)
            {
                var isOk = false;
                var TargetServerLocation = Path.Combine(ServerRepositoryLocation, $"Server_{entity.EntityId}");

                //-------------------------------
                //  Получаем информацию о ветке
                isOk = SvnCheckOut(ServerRepositoryLocation, entity.EntityId);

                //  Обновляем содержимое ветки
                if (SvnUpdate(TargetServerLocation))
                {
                    isOk = true;
                }

                //  Обновляем содержимое ветки еще раз, если произошла ошибка
                if (SvnUpdate(TargetServerLocation))
                {
                    isOk = true;
                }

                //-------------------------------
                //  Выполняем обновление
                if (isOk)
                {
                    entity.LastUpdateDate = DateTime.UtcNow;
                    entity.HasUpdated = true;
                    entity.HasUpdating = false;
                    await db.SaveChangesAsync();
                }
            }
        }

        private bool SvnCheckOut(string InWorkingDirectory, long InVersion)
        {
            //-----------------------------------------
            Directory.CreateDirectory(InWorkingDirectory);

            //-----------------------------------------
            LoggerContainer.ClusterLogger.Error(new string('=', 48));
            LoggerContainer.ClusterLogger.Error($"- Begin svn CheckOut");
            LoggerContainer.ClusterLogger.Error($"- InWorkingDirectory: {InWorkingDirectory}");
            try
            {
                using (SvnClient client = new SvnClient()
                {
                    Authentication = { DefaultCredentials = NetworkCredential }
                })
                {
                    try
                    {
                        client.CleanUp(InWorkingDirectory);
                    }
                    catch { }

                    var Suffix = $"Server_{InVersion}";
                    var BranchUrl = $"{ServerRepositoryUrl}/{Suffix}";
                    var BranchPath = Path.Combine(InWorkingDirectory, Suffix);

                    if (client.CheckOut(SvnUriTarget.FromString(BranchUrl), BranchPath))
                    {
                        LoggerContainer.ClusterLogger.Error($"- Svn CheckOut success");
                        return true;
                    }
                    else
                    {
                        LoggerContainer.ClusterLogger.Error($"- Svn CheckOut failed");
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage($"- Failed CheckOut update. InWorkingDirectory: {InWorkingDirectory} ", e);
                return false;
            }
        }

        private bool SvnUpdate(string InWorkingDirectory)
        {             
            //-----------------------------------------
            Directory.CreateDirectory(InWorkingDirectory);

            //-----------------------------------------
            LoggerContainer.ClusterLogger.Warn(new string('=', 48));
            LoggerContainer.ClusterLogger.Warn($"- Begin svn update");
            LoggerContainer.ClusterLogger.Warn($"- InWorkingDirectory: {InWorkingDirectory}");
            try
            {
                using (SvnClient client = new SvnClient()
                {
                    Authentication = {DefaultCredentials = NetworkCredential}
                })
                {
                    client.CleanUp(InWorkingDirectory);
                    if (client.Update(InWorkingDirectory))
                    {
                        LoggerContainer.ClusterLogger.Debug($"- Svn update success");
                        return true;
                    }
                    else
                    {
                        LoggerContainer.ClusterLogger.Error($"- Svn update failed");
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage($"- Failed svn update. InWorkingDirectory: {InWorkingDirectory} ", e);
                return false;
            }
        }

        private void SvnDeleteBranch(string InWorkingDirectory, long InVersion)
        {
            var BranchUrl = $"{ServerRepositoryUrl}/Server_{InVersion}";
            var directory = Path.Combine(ServerRepositoryLocation, $"Server_{InVersion}");

            //-----------------------------------------
            LoggerContainer.ClusterLogger.Warn(new string('=', 48));
            LoggerContainer.ClusterLogger.Warn($"- Begin svn delete");
            LoggerContainer.ClusterLogger.Warn($"- InWorkingDirectory: {InWorkingDirectory}");
            LoggerContainer.ClusterLogger.Warn($"- Deleting Branch URL: {BranchUrl}");
            LoggerContainer.ClusterLogger.Warn($"- Deleting Branch DIR: {directory}");

            //======================================
            Directory.CreateDirectory(InWorkingDirectory);

            //======================================
            try
            {
                //-----------------------------------------
                using (SvnClient client = new SvnClient()
                {
                    Authentication = { DefaultCredentials = NetworkCredential }
                })
                {
                    client.CleanUp(InWorkingDirectory);
                    if (client.RemoteDelete(new Uri(BranchUrl), new SvnDeleteArgs()
                    {
                        LogMessage = $"Automation remote delete deprecated branch {BranchUrl} at {DateTime.UtcNow}",
                        ThrowOnError = true,
                        Force = true,
                        KeepLocal = false
                    }))
                    {
                        LoggerContainer.ClusterLogger.Debug($"- Svn remote delete success");
                    }
                    else
                    {
                        LoggerContainer.ClusterLogger.Error($"- Svn remote delete failed");
                    }

                    if (client.Delete(directory, new SvnDeleteArgs()
                    {
                        LogMessage = $"Automation local delete deprecated branch {BranchUrl} at {DateTime.UtcNow}",
                        ThrowOnError = true,
                        KeepLocal = false,
                        Force = true
                    }))
                    {
                        LoggerContainer.ClusterLogger.Debug($"- Svn local delete success");
                    }
                    else
                    {
                        LoggerContainer.ClusterLogger.Error($"- Svn local delete failed");
                    }
                }

                DeleteDirectory(directory);
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage($"- Failed svn delete [{BranchUrl}]", e);
            }

        }

        //======================================
        private async Task DeleteDeprecatedBranch(DataRepository.DataRepository db, ServerVersionEntity entity)
        {
            try
            {
                //===============================
                if (entity.HasUpdating && entity.LastUpdateDate.ToElapsedSeconds() < 180)
                {
                    return;
                }

                //===============================
                entity.HasUpdating = true;
                entity.LastUpdateDate = DateTime.UtcNow;
                await db.SaveChangesAsync();

                //===============================
                SvnUpdate(ServerRepositoryLocation);
                SvnDeleteBranch(ServerRepositoryLocation, entity.EntityId);

                //===============================
                entity.DeleteDate = DateTime.UtcNow;
                entity.HasDeleted = true;

                //===============================
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage($"DeleteDeprecatedBranch[{entity.EntityId}]", e);
            }
        }

    }
}