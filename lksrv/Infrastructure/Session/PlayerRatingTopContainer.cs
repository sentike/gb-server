﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Award;
using Loka.Server.Player.Models;
using VRS.Infrastructure.Environment;

namespace Loka.Server.Infrastructure.Session
{
    public struct PlayerRatingTopView
    {
        public IReadOnlyCollection<PlayerRatingTopContainer> PlayerRating { get; set; }
        public IReadOnlyCollection<PlayerRatingTopContainer> PlayerEloRating { get; set; }

    }

    public struct PlayerRatingTopContainer 
        : IWithEloRatingScore
        , IWithIsoCountry
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public long Score { get; set; }
        public int Level { get; set; }
        public int Position { get; set; }
        public short EloRatingScore { get; set; }
        public EIsoCountry Country { get; set; }
        public bool IsOnline { get; set; }
    }
}
