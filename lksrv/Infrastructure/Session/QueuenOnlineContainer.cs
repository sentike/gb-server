﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.GameMode;
using Loka.Infrastructure;

namespace Loka.Server.Infrastructure.Session
{
    public struct QueuenOnlineContainer
    {
        public readonly GameModeTypeId GameModeTypeId;
        public readonly long Count;

        public QueuenOnlineContainer(GameModeTypeId gameModeTypeId, long count)
        {
            GameModeTypeId = gameModeTypeId;
            Count = count;
        }
    }
}
