﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Server.Infrastructure.Session
{
    public enum RespondSquadInviteExceptionCode
    {
        InviteNotFound = 700,
        InviteAreRecived,
        NotEnouthMoney,
        SenderInSquad,
        SenderNotEnouthMoney,
        ErrorOnDataBase,

        YouAreInSquad,

    }

    public class RespondSquadInviteException : AbstractControllerException<RespondSquadInviteExceptionCode>
    {
        public RespondSquadInviteException(RespondSquadInviteExceptionCode responseCode) : base(responseCode)
        {
        }
    }


    public class RespondSquadInviteExceptionWithValue : AbstractControllerException<RespondSquadInviteExceptionCode, string>
    {
        public RespondSquadInviteExceptionWithValue(RespondSquadInviteExceptionCode responseCode, string value) : base(responseCode, value)
        {
        }
    }

}
