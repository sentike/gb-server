﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Loka.Server.Controllers.IdentityController.Operation
{
    public abstract class Authentication
    {
        public class Request
        {
            public string Login { get; set; }

            public string Password { get; set; }

            public bool IsAuthorization { get; set; }

            public override string ToString()
            {
                if (IsAuthorization)
                {
                    return $"Authorization | Login: {Login} | {Password}";
                }
                return $"Registration | Login: {Login} | {Password}";
            }
        }

        public struct Response
        {
            public Response(Guid token, IEnumerable<string> error)
            {
                Token = token;
                Error = error.Select(s => s.Replace("\"", "")).ToList();
            }

            public Guid Token;
            public IEnumerable<string> Error;
        }

    }
}
