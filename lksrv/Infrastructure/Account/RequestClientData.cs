﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Loka.Common.Achievement;
using Loka.Common.Award;
using Loka.Common.Player;
using Loka.Common.Player.Instance;

using Loka.Common.Player.Statistic;

using Loka.Common.Store;


using Loka.Common.Tutorial;
using Loka.Infrastructure;
using Loka.Player;

using Loka.Server.Player.Models;
using Loka.Server.Session;
using Newtonsoft.Json;
using VRS.Infrastructure.Environment;

namespace Loka.Server.Controllers.IdentityController.Operation
{
    public struct PlayerCash
    {
        public GameCurrency Currency { get; set; }
        public long Amount { get; set; }

        public PlayerCash(PlayerCashEntity entity)
        {
            Currency = entity.Currency;
            Amount = entity.Amount;
        }
    }

    public class PlayerTutorialView
    {
        public PlayerTutorialView(PlayerTutorialEntity entity)
        {
            Id = entity.ProgressId;
            TutorialId = entity.TutorialId;
            StepId = entity.StepEntity.StepId;
            TutorialComplete = entity.Complete;
        }

        public Guid Id { get; set; }
        public long TutorialId { get; set; }
        public long StepId { get; set; }
        public bool TutorialComplete { get; set; }
    }

    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public abstract class RequestClientData
    {
        public struct ExperienceContainer
        {
            public ExperienceContainer(PlayerExperience experience)
            {
                Level = experience.Level;
                LastLevel = experience.LastLevel;
                Experience = experience.Experience;
                NextExperience = experience.NextExperience;
                IsNameConfirmed = experience.IsNameConfirmed;
                IsLevelUpNotified = experience.IsLevelUpNotified;
                ChangeNameTickets = experience.ChangeNameTickets;
                SkillPoints = 0;
            }

            public int Level { get; set; }
            public int LastLevel { get; set; }
            public long Experience { get; set; }
            public long NextExperience { get; set; }
            public int SkillPoints { get; set; }
            public bool IsNameConfirmed { get; set; }
            public bool IsLevelUpNotified { get; set; }
            public int ChangeNameTickets { get; set; }

            

        }


        public struct RaitingContainer
        {
            public int Position { get; set; }
            public long Experience { get; set; }
            public long ConductedTime { get; set; }
        }

        public struct EveryDayPrimeTime
        {
            public EveryDayPrimeTime(PlayerExperience experience)
            {
                if(experience.IsAllowUsePrimeMatch())
                {
                    NextAvalibleDate = null;
                    AvalibleMatches = experience.PrimeMatchesCount;
                }
                else
                {
                    AvalibleMatches = null;
                    NextAvalibleDate = experience.PrimeMatchesNextDate.ToUnixTime();
                }
            }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int? AvalibleMatches { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public long? NextAvalibleDate { get; set; }
        }

        public struct PlayerEveryDayModel
        {
            public short? JoinCount { get; set; }
            public bool? IsNotified { get; set; }

            public PlayerEveryDayModel(PlayerEveryDayAwardEntity entity)
            {
                JoinCount = entity?.JoinCount;
                IsNotified = entity?.State != PlayerEveryDayAwardState.RequirClientNotify;
            }

        }


        public struct Response 
            : IWithEloRatingScore
            , IWithIsoCountry
        {
            public Response(PlayerEntity entity)
            {
                PlayerId = entity.PlayerId;
                Login = entity.PlayerName;
                PlayerRole = entity.Role;
                Country = entity.Country;

                Accounts = entity.AccountEntities.Select(p => p.Service).ToArray();

                EveryDayPrimeTime = new EveryDayPrimeTime(entity.Experience);
                EveryDayAward = new PlayerEveryDayModel(entity.SafeEveryDayAwardEntity);
                Experience = new ExperienceContainer(entity.Experience);
                Cash = entity.CashEntities.Select(p => new PlayerCash(p)).ToArray();
                NumberOfMatches = entity.AchievementEntities
                                      .SingleOrDefault(p => p.AchievementId == AchievementModelId.MatchesTotal)
                                      ?.Amount ?? -1;

                IsSquadLeader = entity.IsSquadLeader;
                PremiumEndDate = entity.PremiumEndDate?.ToUnixTime() ?? 0;
                EloRatingScore = entity.EloRatingScore;

                TutorialId = entity.CurrentTutorialId;
                Tutorials = entity.PlayerTutorialEntities.Select(p => new PlayerTutorialView(p)).ToArray();
                Raiting = new RaitingContainer
                {
                    //Position = SessionRepository.PlayerRating(entity.PlayerId),
                    Experience = entity.Experience.WeekExperience,
                    ConductedTime = entity.Experience.WeekActivityTime,
                };
            }

            public Guid? TutorialId { get; set; }
            public PlayerRole PlayerRole { get; set; }
            public long PremiumEndDate { get; set; }
            public long NumberOfMatches { get; set; }
            public short EloRatingScore { get; set; }

            public EAccountService[] Accounts { get; set; }

            public PlayerEveryDayModel EveryDayAward { get; set; }
            public EveryDayPrimeTime EveryDayPrimeTime { get; set; }
            public RaitingContainer Raiting { get; set; }
            public Guid PlayerId { get; set; }
            public string Login { get; set; }
            public ExperienceContainer Experience { get; set; }
            public PlayerCash[] Cash { get; set; }
            public IEnumerable<PlayerTutorialView> Tutorials { get; set; }
            public bool IsSquadLeader { get; set; }
            public EIsoCountry Country { get; set; }
        }
    }
}
