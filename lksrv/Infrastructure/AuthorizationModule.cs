﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Loka.Common.Cluster;
using Loka.Server.Player;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Player.Infrastructure;

namespace Loka.Server.Infrastructure
{ 
    public class AuthorizationModule : IHttpModule
    {
        public const string RealmName = "VRS_LokaGame";

        public static readonly Encoding Encoding = Encoding.GetEncoding("iso-8859-1");
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly PlayerRepository PlayerRepository;

        public AuthorizationModule(PlayerRepository playerRepository)
        {
            PlayerRepository = playerRepository;
        }

        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += ContextOnAuthenticateRequest;
            context.EndRequest += ContextOnEndRequest;
        }

        private static void ContextOnEndRequest(object sender, EventArgs eventArgs)
        {
            if(HttpContext.Current.Response.StatusCode >= 900)
            {
                HttpContext.Current.Response.Headers.Add("WWW-Authenticate", $"Basic realm=\"{RealmName}\"");
            }
        }

        private void ClientAuthorizationHandler(Guid id, Guid token)
        {
            var principal = PlayerRepository.GetClientPrincipal(null, token);
            if(principal == null)
            {
                logger.Error($"[ClientAuthorizationHandler][{token}] | principal was null");
                HttpContext.Current.Response.StatusCode = 900;
            }
            else
            {
                SetPrincipal(principal);
            }
        }

        private void NodeAuthorizationHandler(Guid id, Guid token)
        {
            var db = new DataRepository.DataRepository();
            var match = db.MatchEntity.SingleOrDefault(n => n.MatchId == token);
            if (match == null)
            {
                using (db)
                {
                    HttpContext.Current.Response.StatusCode = 900;
                }
            }
            else
            {
                SetPrincipal(new ClusterNodePrincipal(db, match));
            }
        }

        private void ContextOnAuthenticateRequest(object sender, EventArgs eventArgs)
        {
            string authorizationHeader = null;
            try
            {
                var request = HttpContext.Current.Request;
                authorizationHeader = request.Headers["Authorization"];

                //logger.Error($"ContextOnAuthenticateRequest: {authorizationHeader}");

                if ( authorizationHeader != null)
                {
                    var authorizationValue = AuthenticationHeaderValue.Parse(authorizationHeader);
                    if (authorizationValue?.Parameter != null && authorizationValue?.Scheme != null)
                    {
                        var authorizationScheme = authorizationValue.Scheme;
                        string authorizationCredentials = Encoding.GetString(Convert.FromBase64String(authorizationValue.Parameter));
                        if (string.IsNullOrWhiteSpace(authorizationCredentials) == false)
                        {
                            Guid token;
                            if (Guid.TryParse(authorizationCredentials, out token))
                            {
                                if (authorizationScheme.Equals("client", StringComparison.OrdinalIgnoreCase))
                                {
                                    ClientAuthorizationHandler(token, token);
                                }
                                else if (authorizationScheme.Equals("server", StringComparison.OrdinalIgnoreCase))
                                {
                                    NodeAuthorizationHandler(token, token);
                                }
                                else HttpContext.Current.Response.StatusCode = 901;
                            }
                            else HttpContext.Current.Response.StatusCode = 901;
                        }
                        else HttpContext.Current.Response.StatusCode = 902;
                    }
                    else HttpContext.Current.Response.StatusCode = 903;
                }
            }
            catch (FormatException exception)
            {
                HttpContext.Current.Response.StatusCode = 904;
                logger.Error($"[ClientAuthorizationHandler][FormatException][{authorizationHeader}]:{exception}");
            }
            catch (ArgumentNullException exception)
            {
                logger.Error($"[ClientAuthorizationHandler][ArgumentNullException][{authorizationHeader}]:{exception}");
                HttpContext.Current.Response.StatusCode = 905;
            }
            catch (Exception exception)
            {
                logger.Error($"[ClientAuthorizationHandler][Exception][{authorizationHeader}]{exception}");
                HttpContext.Current.Response.StatusCode = 906;
            }
        }

        public void Dispose()
        {
        }

        public static void SetPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }


    }
}
