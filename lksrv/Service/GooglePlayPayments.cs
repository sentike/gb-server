﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Hosting;
using Google.Apis.AndroidPublisher.v2.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Loka.Common.Payments;
using Loka.Common.Player.Discount;
using Loka.Server.Infrastructure.DataRepository;
using VRS.Infrastructure;

namespace Loka.Server.Service
{
    public static class GooglePlayPayments
    {
        public static string PackageName { get; } = "com.VRSPro.LOKA_AW";
        public static string DiscountSuff { get; } = "_d_";

        public static string PriceMicrosAsPrice(this string InPriceMicros)
        {
            try
            {
                return InPriceMicros.Substring(0, InPriceMicros.Length - 4);
            }
            catch (Exception e)
            {
                LoggerContainer.StoreLogger.WriteExceptionMessage($"[PriceMicrosAsPrice][PriceMicros: {InPriceMicros}]", e);
                return String.Empty;
            }
        }

        public static void ProcessProducts(InAppProduct[] InProducts, EProductDiscountCoupone InDiscount)
        {
            //==================================
            using (var db = new DataRepository())
            {
                //==================================
                var GameProducts = db.MarketProductEntities.ToArray();

                //==================================
                foreach (var Product in InProducts)
                {
                    var GameProduct = GameProducts.SingleOrDefault(p => p.ProductName == Product.Sku);
                    if (GameProduct != null)
                    {
                        //------------------------------------------
                        GameProduct.AdminComment = Product.Listings.FirstOrDefault().Value?.Title;
                        GameProduct.LastUpdateDate = DateTime.UtcNow;

                        //------------------------------------------
                        foreach (var Price in Product.Prices)
                        {
                            ProductCostContainer Cost = null;
                            var PriceMicrosAsPrice = Price.Value.PriceMicros.PriceMicrosAsPrice();
                            if (ProductCostContainer.Parse(Price.Key, Price.Value.Currency, PriceMicrosAsPrice, out Cost))
                            {
                                GameProduct.AddOrUpdatePrice(Cost, Product.Sku, InDiscount);
                            }
                        }
                    }
                }

                //==================================
                var Updates = db.SaveChanges();

                //==================================
                LoggerContainer.StoreLogger.Info($"UpdateMarketCosts, updated: {Updates}");
            }
        }

        public static EProductDiscountCoupone GetDiscountGroup(string InSku)
        {
            if (InSku.Contains($"{DiscountSuff}70"))
            {
                return EProductDiscountCoupone.Percent_70;
            }

            if (InSku.Contains($"{DiscountSuff}50"))
            {
                return EProductDiscountCoupone.Percent_50;
            }

            if (InSku.Contains($"{DiscountSuff}30"))
            {
                return EProductDiscountCoupone.Percent_30;
            }

            if (InSku.Contains($"{DiscountSuff}15"))
            {
                return EProductDiscountCoupone.Percent_15;
            }

            if (InSku.Contains($"{DiscountSuff}5"))
            {
                return EProductDiscountCoupone.Percent_5;
            }

            return EProductDiscountCoupone.None;
        }

        private static Regex RegexDiscountInProductSKU { get; } = new Regex($"{DiscountSuff}([0-9]*)", RegexOptions.Compiled | RegexOptions.Singleline);

        public static string RemoveDiscountInProductSKU2(string InSku)
        {
            if (InSku.Contains(DiscountSuff))
            {
                //==============================================
                var Length = InSku.Length;
                var Begin = 0;
                var Count = 0;

                //==============================================
                try
                {
                    //==============================================
                    Begin = InSku.IndexOf(DiscountSuff);
                    if (Begin >= 0)
                    {
                        Count = Length - Begin;

                        //==============================================
                        var NewSku = InSku.Remove(Begin, Count);

                        //==============================================
                        return NewSku;
                    }
                    return InSku;
                }
                catch (Exception e)
                {
                    LoggerContainer.StoreLogger.WriteExceptionMessage($"[RemoveDiscountInProductSKU][SKU: {InSku}][Begin: {Begin}][Length: {Length}][CountToDelete: {Count}]", e);
                    return InSku;
                }
            }
            return InSku;
        }

        public static string RemoveDiscountInProductSKU(string InSku)
        {
            try
            {
                var NewSku = RegexDiscountInProductSKU.Replace(InSku, string.Empty);
                var NewSku2 = RemoveDiscountInProductSKU2(NewSku);
                return NewSku2;
            }
            catch 
            {
                var NewSku2 = RemoveDiscountInProductSKU2(InSku);
                return NewSku2;
            }
        }

        public static InAppProduct WithRemovedDiscountInProductSKU(InAppProduct InProduct)
        {
            //==============================================
            InProduct.Sku = RemoveDiscountInProductSKU(InProduct.Sku);

            //==============================================
            return InProduct;
        }

        public static void UpdateMarketCosts()
        {
            try
            {
                //==================================
                var RawMarketProducts = GetProductCosts();

                //==================================
                var RawDiscountedProducts = RawMarketProducts.Where(p => p.Sku.Contains(DiscountSuff) == true).ToArray();
                var RawNonDiscountedProducts = RawMarketProducts.Where(p => p.Sku.Contains(DiscountSuff) == false).ToArray();

                //==================================
                ProcessProducts(RawNonDiscountedProducts, EProductDiscountCoupone.None);

                //==================================
                var ProductsGroupedByDiscount = RawDiscountedProducts.GroupBy(p => GetDiscountGroup(p.Sku), p => p);

                //==================================
                foreach (var ProductGroup in ProductsGroupedByDiscount)
                {
                    var ProductArray = ProductGroup.Select(p => WithRemovedDiscountInProductSKU(p)).ToArray();
                    ProcessProducts(ProductArray, ProductGroup.Key);
                }
            }
            catch (Exception e)
            {
                LoggerContainer.StoreLogger.WriteExceptionMessage($"UpdateMarketCosts", e);
            }
        }

        public static InAppProduct[] GetProductCosts()
        {
            String serviceAccountEmail = "masterserverpurchases@loka-aw-17856225.iam.gserviceaccount.com";

            var certificate = new X509Certificate2(File.ReadAllBytes(HostingEnvironment.MapPath("~/App_Data/Loka AW-3ed494c017b2.p12")), "notasecret", X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(serviceAccountEmail)
                {
                    Scopes = new[] { Google.Apis.AndroidPublisher.v2.AndroidPublisherService.Scope.Androidpublisher }
                }.FromCertificate(certificate));

            //=========================================
            var client = new Google.Apis.AndroidPublisher.v2.AndroidPublisherService(new BaseClientService.Initializer
            {
                ApiKey = "AIzaSyBRU_LD0bELyhrX-m8rHAwXzFTj75toaak",
                ApplicationName = "Loka AW",
                HttpClientInitializer = credential,
            });

            return client.Inappproducts.List(PackageName).Execute().Inappproduct.ToArray();
        }

        public static ProductPurchase GetProductPurchase(string inProductId, string inPurchaseToken)
        {
            String serviceAccountEmail = "masterserverpurchases@loka-aw-17856225.iam.gserviceaccount.com";
            

            var certificate = new X509Certificate2(File.ReadAllBytes(HostingEnvironment.MapPath("~/App_Data/Loka AW-3ed494c017b2.p12")), "notasecret", X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(serviceAccountEmail)
                {
                    Scopes = new[] { Google.Apis.AndroidPublisher.v2.AndroidPublisherService.Scope.Androidpublisher }
                }.FromCertificate(certificate));

            //=========================================
            var client = new Google.Apis.AndroidPublisher.v2.AndroidPublisherService(new BaseClientService.Initializer
            {
                ApiKey = "AIzaSyBRU_LD0bELyhrX-m8rHAwXzFTj75toaak",
                ApplicationName = "Loka AW",
                HttpClientInitializer = credential,
            });

            return client.Purchases.Products.Get(PackageName, inProductId, inPurchaseToken).Execute();
        }
    }
}
