﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Loka.Server.Infrastructure;
using RestSharp;
using VRS.Infrastructure;

namespace Loka.Server.Service
{
    public static class NotifyService
    {
        public static bool SendNotify(this object o, string url)
        {
            try
            {
                Task.Run(() =>
                {
                    try
                    {
                        var client = new RestClient(AbstractApiController.ServiceUrl(url));
                        var request = new RestRequest(Method.POST);

                        request.AddJsonBody(o);
                        client.Execute(request);
                    }
                    catch (Exception e)
                    {
                        LoggerContainer.AccountLogger.WriteExceptionMessage($"[NotifyService][0][SendNotify][{url}]", e);
                    }
                });
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[NotifyService][1][SendNotify][{url}]", e);
                return false;
            }

            return true;
        }

    }
}