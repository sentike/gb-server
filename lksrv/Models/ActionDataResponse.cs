﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;

namespace Loka.Server.Models
{
    //public class ActionDataResponse<TData> : NegotiatedContentResult<TData>
    //    where TData : class
    //{
    //    //====================================
    //    public TData Content { get; set; }
    //
    //    //====================================
    //    public ActionDataResponse(ApiController controller, int code, TData data)
    //        : base((HttpStatusCode)code, data, controller)
    //    {
    //        Content = data;
    //    }
    //}

    public class ActionDataException<TData> : Exception
    {
        //====================================
        public int StatusCode { get; set; }
        public TData Content { get; set; }

        //====================================
        public string ContentString => JsonConvert.SerializeObject(Content);

        //====================================
        public ActionDataException(string exception, int code, TData data)
            : base(exception)
        {
            Content = data;
            StatusCode = code;
        }

        //====================================
        public ActionDataException(int code, TData data)
        {
            Content = data;
            StatusCode = code;
        }
    }

}