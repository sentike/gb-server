﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Player.Models;
using Microsoft.Ajax.Utilities;
using VRS.Infrastructure;
using WebGrease.Css.Extensions;
using Z.EntityFramework.Plus;

namespace Loka.Server.Models.Session
{
    public class PlayerRatingBuffer : AbstractTemporyBuffer<PlayerRatingTopContainer[], PlayerRatingTopContainer[], Guid?>
    {
        public static readonly PlayerRatingBuffer Instance = new PlayerRatingBuffer(50 * 2);

        public PlayerRatingBuffer(int updateIntevalInSeconds)
            : base(updateIntevalInSeconds)
        {

        }

        public override async Task<PlayerRatingTopContainer[]> GetData(Guid? InKey)
        {
            try
            {
                if (InKey != null && InKey.HasValue && (Object?.Any(p => p.Id == InKey) ?? true) == false)
                {
                    using (var db = new DataRepository())
                    {
                        //=================================
                        var Player = await db.AccountPlayerEntity.SingleOrDefaultAsync(p => p.PlayerId == InKey);
                        if (Player != null)
                        {
                            var OrderedPlayers = db.AccountPlayerEntity.OrderBy(p => p.RatingPosition).AsNoTracking();

                            //=================================
                            var CurrentRatingPosition = Player.RatingPosition;
                            var PrevRatingPosition = Player.RatingPosition - 5;
                            var NextRatingPosition = Player.RatingPosition + 5;

                            //=================================
                            var BeforPlayerTask = OrderedPlayers.Where(p => p.RatingPosition >= PrevRatingPosition && p.RatingPosition < CurrentRatingPosition).Take(5).ToArrayAsync();
                            var AfterPlayerTask = OrderedPlayers.Where(p => p.RatingPosition <= NextRatingPosition && p.RatingPosition > CurrentRatingPosition).Take(5).ToArrayAsync();

                            var BeforPlayer = await BeforPlayerTask;
                            var AfterPlayer = await AfterPlayerTask;

                            //=================================
                            var PlayerEntities = BeforPlayer.Concat(new[] { Player }).Concat(AfterPlayer).ToArray();
                            var PlayerRatings = PlayerEntities.Select(p => new PlayerRatingTopContainer
                            {
                                Id = p.PlayerId,
                                Name = p.PlayerName,
                                Country = p.Country,
                                Position = p.RatingPosition,
                                Score = p.Experience.WeekExperience,
                                EloRatingScore = p.EloRatingScore,
                                Level = p.Experience.Level,
                                IsOnline = p.IsOnline
                            });

                            //=================================
                            //LoggerContainer.DataBaseLogger.Error($"[PlayerEloRatingBuffer][{InKey}][PrevEloRatingPosition: {PrevEloRatingPosition}][BeforPlayer: {string.Join(",", BeforPlayer.Select(p => $"{p.PlayerName} | {p.EloRatingPosition}"))}] {Environment.NewLine}");
                            //LoggerContainer.DataBaseLogger.Error($"[PlayerEloRatingBuffer][{InKey}][NextEloRatingPosition: {NextEloRatingPosition}][AfterPlayer: {string.Join(",", AfterPlayer.Select(p => $"{p.PlayerName} | {p.EloRatingPosition}"))}] {Environment.NewLine}");
                            //LoggerContainer.DataBaseLogger.Error($"[PlayerEloRatingBuffer][{InKey}]PlayerRatings:  {string.Join($" {Environment.NewLine} > ", PlayerRatings.Select(p => $"{p.Name} | {p.Position}"))}");


                            //=================================
                            var RatingResult = Object.Concat(PlayerRatings).DistinctBy(p => p.Id).ToArray();
                            return RatingResult;
                        }
                    }
                }
            }
            catch(Exception e)
            {
                LoggerContainer.DataBaseLogger.WriteExceptionMessage($"[PlayerRatingBuffer][{InKey}][Object.Length: {Object?.Length}]", e);
            }
            return Object;
        }

        private async Task UpdateProcess(Guid[] InPlayerRatingListIds, Guid[] InPlayerListIds)
        {
            using (var db = new DataRepository())
            {
                try
                {
                    //-------------------------------------------
                    db.Configuration.AutoDetectChangesEnabled = false;

                    //=========================================
                    var num = 128;
                    var steps = InPlayerListIds.Length / num;

                    //=========================================
                    for (var itter = 0; itter <= steps; itter++)
                    {
                        var sb = new StringBuilder(num * 256);
                        var ids = InPlayerListIds.Skip(num * itter).Take(num).ToArray();
                        foreach (var id in ids)
                        {
                            sb.AppendLine($"UPDATE players.\"PlayerEntity\" SET \"RatingPosition\" = { Array.IndexOf(InPlayerRatingListIds, id) + 1} WHERE \"PlayerId\" = '{id}';");
                        }

                        var query = sb.ToString();
                        if (string.IsNullOrWhiteSpace(query) == false)
                        {
                            await db.Database.ExecuteSqlCommandAsync(query);
                        }
                    }
                    
                    var changes = await db.SaveChangesAsync();
                    LoggerContainer.StoreLogger.Warn($"[PlayerRatingBuffer][UpdateIds] PlayerListIds: {InPlayerListIds.Length} of {InPlayerRatingListIds.Length}");
                }
                catch (Exception e)
                {
                    LoggerContainer.DataBaseLogger.WriteExceptionMessage("[PlayerEloRatingBuffer][1]", e);
                }
                finally
                {
                    db.Configuration.AutoDetectChangesEnabled = true;
                }
            }
        }

        protected override async Task Update()
        {
            //return;
            //===============================
            Guid[] PlayerListIds;

            //===============================
            var LoadIdsStartDate = DateTime.UtcNow;

            //===============================
            //  Получаем отстортированные ид всех игроков в порядке возрастания их очков
            using (var db = new DataRepository())
            {
                //-------------------------------------------
                db.Configuration.LazyLoadingEnabled = true;
                db.Configuration.AutoDetectChangesEnabled = false;
                db.Configuration.ValidateOnSaveEnabled = false;

                PlayerListIds = await db.AccountPlayerEntity.AsNoTracking().OrderByDescending(p => p.Experience.WeekExperience).Select(p => p.PlayerId).ToArrayAsync();
            }

            var LoadIdsEndDate = DateTime.UtcNow;

            //===============================
            LoggerContainer.StoreLogger.Warn($"[LoadIds] Elapsed: {LoadIdsEndDate - LoadIdsStartDate} | PlayerListIds: {PlayerListIds.Length}");

            //===============================
            //  Обновляем позицию всех игроков

            var UpdateIdsStartDate = DateTime.UtcNow;
            try
            {
                //=========================================
                var num = 1024;
                var steps = PlayerListIds.Length / num;

                //=========================================
                List<Task> tasks = new List<Task>(steps);

                //=========================================
                for (var i = 0; i <= steps; i++)
                {
                    var itter = i;
                    tasks.Add(Task.Run(() =>
                    {
                        var ids = PlayerListIds.Skip(num * itter).Take(num).ToArray();
                        return UpdateProcess(PlayerListIds, ids);
                    }));
                }

                //=========================================
                Task.WaitAll(tasks.ToArray());
            }
            catch (Exception e)
            {
                LoggerContainer.DataBaseLogger.WriteExceptionMessage("PlayerRatingBuffer[0]", e);
            }

            var UpdateIdsEndDate = DateTime.UtcNow;

            //===============================
            LoggerContainer.StoreLogger.Warn($"[UpdateIds] Elapsed: {UpdateIdsEndDate - UpdateIdsStartDate}");

            //===============================
            var CacheIdsStartDate = DateTime.UtcNow;


            //===============================
            using (var db = new DataRepository())
            {
                //-----------------------------------------------
                var data = await db.AccountPlayerEntity.AsNoTracking().Where(p => p.RatingPosition >= 1 && p.RatingPosition <= 90 && p.Experience.WeekExperience > 0).OrderBy(p => p.RatingPosition).Take(90).ToArrayAsync();
                Object = data.Select
                (
                    p => new PlayerRatingTopContainer
                    {
                        Id = p.PlayerId,
                        Name = p.PlayerName,
                        Country = p.Country,
                        Position = p.RatingPosition,
                        Score = p.Experience.WeekExperience,
                        EloRatingScore = p.EloRatingScore,
                        Level = p.Experience.Level,
                        IsOnline = p.IsOnline
                    }
                )
                .ToArray();
            }

            //===============================
            var CacheIdsEndDate = DateTime.UtcNow;
            PlayerEntity.LastRatingPosition = PlayerListIds.Length;

            //===============================
            LoggerContainer.StoreLogger.Warn($"[CacheIds] Elapsed: {CacheIdsEndDate - CacheIdsStartDate} | LastRatingPosition: {PlayerEntity.LastRatingPosition}");


        }
    }
}