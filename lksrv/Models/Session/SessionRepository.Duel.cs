﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using Loka.Common.Match.GameMode;
//using Loka.Server.Infrastructure.DataRepository;
//using Loka.Server.Models.Queue;
//using Loka.Server.Player.Models;
//using Ninject;
//using Loka.Infrastructure;
//using Loka.Server.Infrastructure.Queue;
//using Loka.Server.Infrastructure.Session;
//using Loka.Server.Models.Session;
//using Loka.Server.Player.Infrastructure;
//using WebGrease.Css.Extensions;

//namespace Loka.Server.Session
//{
//    public partial class SessionRepository 
//    {
//        public SendSquadDuelInviteResponse SendDuelInvite(PlayerEntity sender, SendSquadDuelInviteRequest options)
//        {
//            using (var db = new DataRepository())
//            {
//                if (sender.QueueEntity.QueueId == options.TargetId)
//                {
//                    throw new SendSquadInviteException(SendSquadInviteExceptionCode.InviteYourself);
//                }

//                if (sender.QueueEntity.InSquad && sender.QueueEntity.IsLeader == false)
//                {
//                    throw new SendSquadInviteException(SendSquadInviteExceptionCode.YouAreNotLeader);
//                }

//                if (sender.QueueEntity.InSquad || sender.QueueEntity.Members.Count > 1)
//                {
//                    throw new SendSquadInviteException(SendSquadInviteExceptionCode.YouAreInSquad);
//                }

//                var lastFiveSeconds = DateTime.UtcNow.AddSeconds(-6);
//                if (db.SquadInviteEntities.Any(s => s.SquadId == sender.QueueEntity.QueueId && s.SubmittedDate >= lastFiveSeconds))
//                {
//                    throw new SendSquadInviteException(SendSquadInviteExceptionCode.InviteTimeoutSent);
//                }

//                var member = db.AccountPlayerEntity.FirstOrDefault(m => m.PlayerId == options.TargetId);

//                //  Player Not Found
//                if (member == null)
//                {
//                    throw new SendSquadInviteException(SendSquadInviteExceptionCode.PlayerNotFound);
//                }

//                if (member.IsOnline == false && member.LastActivityDate.ToElapsedSeconds() >= 300)
//                {
//                    throw new SendSquadInviteExceptionWithValue(SendSquadInviteExceptionCode.PlayerNotOnline, member.PlayerName);
//                }

//                // Invite already sent in the duel
//                if (db.SquadInviteEntities.Any(s => s.PlayerId == options.TargetId && s.State == SquadInviteState.Submitted && s.SquadId == sender.QueueEntity.QueueId))
//                {
//                    throw new SendSquadInviteExceptionWithValue(SendSquadInviteExceptionCode.InviteAlreadySent, member.PlayerName);
//                }

//                //  Player In Squad
//                if (member.QueueEntity != null && member.QueueEntity.InSquad && member.QueueEntity.Ready)
//                {
//                    throw new SendSquadInviteExceptionWithValue(SendSquadInviteExceptionCode.PlayerInSquad, member.PlayerName);
//                }

//                if (member.Cash.IsPayAllow(options.Bet) == false)
//                {
//                    throw new SendSquadInviteExceptionWithValue(SendSquadInviteExceptionCode.PlayerNotEnouthMoney, member.PlayerName);
//                }

//                db.SquadInviteEntities.Add(SquadDuelInviteEntity.Factory(sender.QueueEntity.QueueId, options));
//                db.SaveChanges();
//                return new SendSquadDuelInviteResponse(member.PlayerName, options);
//            }
//        }

//        public object RespondDuelInvite(ClientPrincipal connection, RespondSqaudInviteContainer respond)
//        {
//            var target = connection.PlayerInstance;
//            var inviteInstance = target.IncomingSquadInvites.FirstOrDefault(s => s.InviteId == respond.InviteId);
//            if (inviteInstance == null)
//            {
//                throw new RespondSquadInviteException(RespondSquadInviteExceptionCode.InviteNotFound);
//            }

//            if (inviteInstance.Type == SquadInviteType.Squad) return RespondSquadInvite(connection, respond);

//            var invite = inviteInstance as SquadDuelInviteEntity;
//            if (invite == null)
//            {
//                throw new RespondSquadInviteException(RespondSquadInviteExceptionCode.InviteNotFound);
//            }

//            if (invite.State != SquadInviteState.Submitted)
//            {
//                throw new RespondSquadInviteExceptionWithValue(RespondSquadInviteExceptionCode.InviteAreRecived, invite.PlayerEntity.PlayerName);
//            }

//            if (target.Cash.IsPayAllow(invite.Bet) == false)
//            {
//                invite.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
//                throw new RespondSquadInviteException(RespondSquadInviteExceptionCode.NotEnouthMoney);
//            }

//            var duel = invite.SquadEntity.PartyEntity;
//            if (duel.Members.Count > 1)
//            {
//                invite.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
//                throw new RespondSquadInviteException(RespondSquadInviteExceptionCode.SenderInSquad);
//            }

//            if (respond.IsApproved)
//            {
//                invite.State = SquadInviteState.Received ^ SquadInviteState.Approved;
//                if (target.QueueEntity == null)
//                {
//                    target.QueueEntity = new QueueEntity(target.PlayerId, new SearchSessionOptions());
//                }

//                foreach (var inv in target.IncomingSquadInvites.Where(s => s.State == SquadInviteState.Submitted))
//                {
//                    inv.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
//                }

//                //  Нельзя принять приглашение если уже в отряде
//                if (target.QueueEntity.InSquad)
//                {
//                    throw new RespondSquadInviteExceptionWithValue(RespondSquadInviteExceptionCode.SenderInSquad, invite.PlayerEntity.PlayerName);
//                }

//                target.QueueEntity.PartyId = invite.SquadId;

//                //target.QueueEntity.GameModeTypeId = GameModeTypeId.DuelMatch;
//                target.QueueEntity.Ready = true;
//                target.QueueEntity.DuelEntity = QueueDuelEntity.Factory(target.QueueEntity.DuelEntity, target.QueueEntity.QueueId, invite);


//                //duel.PartyEntity.GameModeTypeId = GameModeTypeId.DuelMatch;
//                duel.PartyEntity.Ready = true;
//                duel.DuelEntity = QueueDuelEntity.Factory(duel.DuelEntity, duel.QueueId, invite);
//                connection.Db.SaveChanges();
//                connection.Db.GameModeEntity.FirstOrDefault(g => g.GameModeId == GameModeTypeId.DuelMatch)?.Tick();;
//            }
//            else
//            {
//                invite.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
//            }
//            return new object();
//        }
//    }
//}