﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Player.Models;
using Microsoft.Ajax.Utilities;
using VRS.Infrastructure;
using WebGrease.Css.Extensions;
using Z.EntityFramework.Plus;

namespace Loka.Server.Models.Session
{
    public class PlayerEloRatingBuffer : AbstractTemporyBuffer<PlayerRatingTopContainer[], PlayerRatingTopContainer[], Guid?>
    {
        public static readonly PlayerEloRatingBuffer Instance = new PlayerEloRatingBuffer(50 * 2);

        public PlayerEloRatingBuffer(int updateIntevalInSeconds)
            : base(updateIntevalInSeconds)
        {

        }

        public override async Task<PlayerRatingTopContainer[]> GetData(Guid? InKey)
        {
            try
            {
                if (InKey != null && InKey.HasValue && (Object?.Any(p => p.Id == InKey) ?? true) == false)
                {
                    using (var db = new DataRepository())
                    {
                        //=================================
                        var Player = await db.AccountPlayerEntity.SingleOrDefaultAsync(p => p.PlayerId == InKey);
                        if (Player != null)
                        {
                            var OrderedPlayers = db.AccountPlayerEntity.OrderBy(p => p.EloRatingPosition).AsNoTracking();

                            //=================================
                            var CurrentEloRatingPosition = Player.EloRatingPosition;
                            var PrevEloRatingPosition = Player.EloRatingPosition - 5;
                            var NextEloRatingPosition = Player.EloRatingPosition + 5;

                            //=================================
                            var BeforPlayerTask = OrderedPlayers.Where(p => p.EloRatingPosition >= PrevEloRatingPosition && p.EloRatingPosition < CurrentEloRatingPosition).Take(5).ToArrayAsync();
                            var AfterPlayerTask = OrderedPlayers.Where(p => p.EloRatingPosition <= NextEloRatingPosition && p.EloRatingPosition > CurrentEloRatingPosition).Take(5).ToArrayAsync();

                            //=================================
                            var BeforPlayer = await BeforPlayerTask;
                            var AfterPlayer = await AfterPlayerTask;

                            //=================================
                            var PlayerEntities = BeforPlayer.Concat(new[] { Player }).Concat(AfterPlayer).ToArray();
                            var PlayerRatings = PlayerEntities.Select(p => new PlayerRatingTopContainer
                            {
                                Id = p.PlayerId,
                                Name = p.PlayerName,
                                Country = p.Country,
                                Position = p.EloRatingPosition,
                                Score = p.Experience.WeekExperience,
                                EloRatingScore = p.EloRatingScore,
                                Level = p.Experience.Level,
                                IsOnline = p.IsOnline
                            });

                            //=================================
                            //LoggerContainer.DataBaseLogger.Error($"[PlayerEloRatingBuffer][{InKey}][PrevEloRatingPosition: {PrevEloRatingPosition}][BeforPlayer: {string.Join(",", BeforPlayer.Select(p => $"{p.PlayerName} | {p.EloRatingPosition}"))}] {Environment.NewLine}");
                            //LoggerContainer.DataBaseLogger.Error($"[PlayerEloRatingBuffer][{InKey}][NextEloRatingPosition: {NextEloRatingPosition}][AfterPlayer: {string.Join(",", AfterPlayer.Select(p => $"{p.PlayerName} | {p.EloRatingPosition}"))}] {Environment.NewLine}");
                            //LoggerContainer.DataBaseLogger.Error($"[PlayerEloRatingBuffer][{InKey}]PlayerRatings:  {string.Join($" {Environment.NewLine} > ", PlayerRatings.Select(p => $"{p.Name} | {p.Position}"))}");


                            //=================================
                            var RatingResult = Object.Concat(PlayerRatings).DistinctBy(p => p.Id).ToArray();
                            return RatingResult;
                        }
                    }
                }
            }
            catch(Exception e)
            {
                LoggerContainer.DataBaseLogger.WriteExceptionMessage($"[PlayerEloRatingBuffer][{InKey}][Object.Length: {Object?.Length}]", e);
            }
            return Object;
        }

        private async Task UpdateProcess(Guid[] InPlayerRatingListIds,  Guid[] InPlayerListIds)
        {
            using (var db = new DataRepository())
            {
                try
                {
                    //-------------------------------------------
                    db.Configuration.AutoDetectChangesEnabled = false;

                    //=========================================
                    var num = 128;
                    var steps = InPlayerListIds.Length / num;

                    //=========================================
                    for (var itter = 0; itter <= steps; itter++)
                    {
                        var sb = new StringBuilder(num * 256);
                        var ids = InPlayerListIds.Skip(num * itter).Take(num).ToArray();
                        foreach (var id in ids)
                        {
                            sb.AppendLine($"UPDATE players.\"PlayerEntity\" SET \"EloRatingPosition\" = { Array.IndexOf(InPlayerRatingListIds, id) + 1} WHERE \"PlayerId\" = '{id}';");
                        }

                        var query = sb.ToString();
                        if (string.IsNullOrWhiteSpace(query) == false)
                        {
                            await db.Database.ExecuteSqlCommandAsync(query);
                        }
                    }

                    var changes = await db.SaveChangesAsync();
                    LoggerContainer.StoreLogger.Warn($"[PlayerEloRatingBuffer][UpdateIds] PlayerListIds: {InPlayerListIds.Length} of {InPlayerRatingListIds.Length}");
                }
                catch (Exception e)
                {
                    LoggerContainer.DataBaseLogger.WriteExceptionMessage("[PlayerEloRatingBuffer][1]", e);
                }
                finally
                {
                    db.Configuration.AutoDetectChangesEnabled = true;
                }
            }
        }

        protected override async Task Update()
        {
            //return;
            //===============================
            Guid[] PlayerListIds;

            //===============================
            var LoadIdsStartDate = DateTime.UtcNow;

            //===============================
            //  Получаем отстортированные ид всех игроков в порядке возрастания их очков
            using (var db = new DataRepository())
            {
                //-------------------------------------------
                db.Configuration.LazyLoadingEnabled = true;
                db.Configuration.AutoDetectChangesEnabled = false;
                db.Configuration.ValidateOnSaveEnabled = false;

                PlayerListIds = await db.AccountPlayerEntity.AsNoTracking().OrderByDescending(p => new { p.EloRatingScore, p.Experience.Level }).Select(p => p.PlayerId).ToArrayAsync();
            }

            var LoadIdsEndDate = DateTime.UtcNow;

            //===============================
            LoggerContainer.StoreLogger.Warn($"[PlayerEloRatingBuffer][LoadIds] Elapsed: {LoadIdsEndDate - LoadIdsStartDate} | PlayerListIds: {PlayerListIds.Length}");
            LoggerContainer.StoreLogger.Warn($"[PlayerEloRatingBuffer][LoadIds]: {string.Join("|", PlayerListIds.Take(30).ToArray())}");

            //===============================
            //  Обновляем позицию всех игроков
            var UpdateIdsStartDate = DateTime.UtcNow;
            try
            {
                //=========================================
                var num = 1024;
                var steps = PlayerListIds.Length / num;

                //=========================================
                List<Task> tasks = new List<Task>(steps);

                //=========================================
                for (var i = 0; i <= steps; i++)
                {
                    var itter = i;
                    tasks.Add(Task.Run(() =>
                    {
                        var ids = PlayerListIds.Skip(num * itter).Take(num).ToArray();
                        return UpdateProcess(PlayerListIds, ids);
                    }));
                }

                //=========================================
                Task.WaitAll(tasks.ToArray());
            }
            catch (Exception e)
            {
                LoggerContainer.DataBaseLogger.WriteExceptionMessage("[PlayerEloRatingBuffer][0]", e);
            }

            //var UpdateIdsStartDate = DateTime.UtcNow;
            //try
            //{
            //
            //
            //    using (var db = new DataRepository())
            //    {
            //        try
            //        {
            //            //-------------------------------------------
            //            db.Configuration.AutoDetectChangesEnabled = false;
            //
            //            //=======================================
            //            foreach (var PlayerId in PlayerListIds)
            //            {
            //                await db.Database.ExecuteSqlCommandAsync($"UPDATE players.\"PlayerEntity\" SET \"EloRatingPosition\" = { Array.IndexOf(PlayerListIds, PlayerId) + 1} WHERE \"PlayerId\" = '{PlayerId}'");
            //            }
            //
            //            var changes = await db.SaveChangesAsync();
            //            LoggerContainer.StoreLogger.Warn($"[PlayerEloRatingBuffer][UpdateIds] PlayerListIds: {PlayerListIds.Length} / {changes}");
            //        }
            //        catch (Exception e)
            //        {
            //            LoggerContainer.DataBaseLogger.WriteExceptionMessage("[PlayerEloRatingBuffer][1]", e);
            //        }
            //        finally
            //        {
            //            db.Configuration.AutoDetectChangesEnabled = true;
            //        }
            //    }
            //}
            //catch (Exception e)
            //{
            //    LoggerContainer.DataBaseLogger.WriteExceptionMessage("[PlayerEloRatingBuffer][0]", e);
            //}

            var UpdateIdsEndDate = DateTime.UtcNow;

            //===============================
            LoggerContainer.StoreLogger.Warn($"[PlayerEloRatingBuffer][UpdateIds] Elapsed: {UpdateIdsEndDate - UpdateIdsStartDate}");

            //===============================
            var CacheIdsStartDate = DateTime.UtcNow;


            //===============================
            try
            {
                using (var db = new DataRepository())
                {
                    //-----------------------------------------------
                    var data = await db.AccountPlayerEntity.AsNoTracking().Where(p => p.EloRatingPosition >= 1 && p.EloRatingPosition <= 30 && p.EloRatingScore > 0).OrderBy(p => p.EloRatingPosition).Take(30).ToArrayAsync();
                    Object = data.Select
                        (
                            p => new PlayerRatingTopContainer
                            {
                                Id = p.PlayerId,
                                Name = p.PlayerName,
                                Country = p.Country,
                                Position = p.EloRatingPosition,
                                Score = p.Experience.WeekExperience,
                                EloRatingScore = p.EloRatingScore,
                                Level = p.Experience.Level,
                                IsOnline = p.IsOnline
                            }
                        )
                        .ToArray();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            //===============================
            var CacheIdsEndDate = DateTime.UtcNow;

            //===============================
            LoggerContainer.StoreLogger.Warn($"[PlayerEloRatingBuffer][CacheIds] Elapsed: {CacheIdsEndDate - CacheIdsStartDate}");


        }
    }
}