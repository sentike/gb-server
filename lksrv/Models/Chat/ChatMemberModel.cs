﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Chat.Chanel;
using Loka.Server.Player.Models;

namespace Loka.Server.Models.Chat
{
    public class ChatMemberModel
    {
        public ChatMemberModel(ChatChanelMemberEntity entity)
        {
            MemberId = entity.MemberId;

            ChanelId = entity.ChanelId;
            PlayerId = entity.PlayerId;
            PlayerName = entity.PlayerEntity.PlayerName;
            PlayerStatus = entity.PlayerEntity.Status();
        }

        public Guid MemberId { get; set; }
        public Guid ChanelId { get; set; }

        public Guid PlayerId { get; set; }
        public string PlayerName { get; set; }
        public PlayerFriendStatus PlayerStatus { get; set; }
    }
}