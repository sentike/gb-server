﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Chat.Chanel;

namespace Loka.Server.Models.Chat
{
    /// <summary>
    /// Базовая информация о чате
    /// </summary>
    public class ChatChanelModel
    {
        public ChatChanelModel()
        {
            
        }

        public ChatChanelModel(ChatChanelEntity entity)
        {
            ChanelId = entity.ChanelId;
            ChanelName = entity.ChanelName;

            GroupId = entity.GroupId;
            ChanelGroup = entity.ChanelGroup;
        }

        //====================================================
        public Guid ChanelId { get; set; }
        public string ChanelName { get; set; }

        //====================================================
        public Guid? GroupId { get; set; }
        public ChatChanelGroup ChanelGroup { get; set; }
    }
}