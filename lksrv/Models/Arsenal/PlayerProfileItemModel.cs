﻿using Loka.Common.Player.Profile.Item;
using Loka.Common.Player.Profile.Slot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loka.Server.Models.Arsenal
{
    public class PlayerProfileItemModel
    {
        public Guid ItemId { get; set; }
        public Guid EntityId { get; set; }
        public PlayerProfileInstanceSlotId SetSlotId { get; set; }

        public override string ToString()
        {
            return $"[PlayerProfileItemModel][ItemId: {ItemId}][SlotId: {SetSlotId}]";
        }

        public PlayerProfileItemModel() { }
        public PlayerProfileItemModel(PlayerProfileItemEntity slot)
        {
            ItemId = slot.ItemId;
            EntityId = slot.ProfileItemId;
            SetSlotId = slot.SlotId;
        }

    }
}