﻿using Loka.Common.Player.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Player.Discount;

namespace Loka.Server.Models.Arsenal
{
    public class PlayerInventoryItemUnlock
    {
        public int ModelId { get; set; }
        public long Amount { get; set; }
        public PlayerProfileInstanceSlotId SetSlotId { get; set; }

        public override string ToString()
        {
            return $"[PlayerInventoryItemUnlock][ModelId: {ModelId}][Amount: {Amount}][SetSlotId: {SetSlotId}]";
        }
    }

    public class PlayerInventoryItemUnlocked : PlayerInventoryItemModel
    {
        public EProductDiscountCoupone DiscountCoupone { get; set; }
        public long AmountRequest { get; set; }
        public long AmountBought { get; set; }

        public PlayerInventoryItemUnlocked() { }
        public PlayerInventoryItemUnlocked(PlayerInventoryItemEntity entity, long request, long bought, EProductDiscountCoupone? InDiscountCoupone)
            : base(entity)
        {
            DiscountCoupone = InDiscountCoupone ?? EProductDiscountCoupone.None;
            AmountRequest = request;
            AmountBought = bought;
        }
    }
}