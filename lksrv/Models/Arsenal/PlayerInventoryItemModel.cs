﻿using Loka.Common.Player.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loka.Server.Models.Arsenal
{
    public class PlayerInventoryItemModel
    {
        public Guid EntityId { get; set; }
        public int ModelId { get; set; }
        public int Level { get; set; }
        public long Amount { get; set; }

        public PlayerInventoryItemModel() { }
        public PlayerInventoryItemModel(PlayerInventoryItemEntity entity)
        {
            EntityId = entity.ItemId;
            ModelId = entity.ModelId;
            Level = entity.Level;
            Amount = entity.Amount;
        }
    }
}