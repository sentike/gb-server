﻿using Loka.Common.Player.Profile.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Server.Player.Models;

namespace Loka.Server.Models.Arsenal
{
    public class PlayerProfileModel
    {
        public Guid ProfileId { get; set; }
        public Guid? DefaultItemId { get; set; }
        public List<PlayerProfileItemModel> Items { get; set; } = new List<PlayerProfileItemModel>();

        public PlayerProfileModel()
        {

        }
        public PlayerProfileModel(PlayerEntity profile)
        {
            ProfileId = profile.PlayerId;
            DefaultItemId = profile.DefaultItemId;
            Items = profile.Items.Select(p => new PlayerProfileItemModel(p)).ToList();
        }
    }
}