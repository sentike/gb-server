﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

using Loka.Infrastructure;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using VRS.Infrastructure;
using System.Data.Entity;
using System.Threading.Tasks;
using Loka.Common.Player.Instance;

namespace Loka.Server.Session
{
    public enum CheckGameVersionResponse
    {
        None,
        UpdateNotExist,
        UpdateCheckException,
        UpdateAvailable,
        UpdateRequired,
        InvalidGameVersion,
        CurrentGameVersionNotFound,
    }

    public class NotifyController : AbstractApiController
    {
        [AllowAnonymous, HttpGet]
        //public IHttpActionResult GetAvailablePlayersNotifyList(Guid? Id)
        public async Task<IHttpActionResult> GetAvailableRatingNotifications(Guid? Id)
        {
            //--------------------------------------------------------
            Logger.Error($"[GetAvailableRatingNotifications][{Id}]");
            //return Json(new int[]{ rnd.Next(1, 5000)});

            //--------------------------------------------------------
            using (var db = new DataRepository())
            {
                //-----------------------------------------
                var PlayerEntity = await db.AccountPlayerEntity.SingleOrDefaultAsync(p => p.PlayerId == Id);
                if (PlayerEntity == null)
                {
                    return Json(new int[0]);
                }

                //-----------------------------------------
                if (PlayerEntity.Notifications.IsAllowNotificationBroadcast == false)
                {
                    return Json(new int[0]);
                }

                //-----------------------------------------
                if (PlayerEntity.Notifications.IsAllowRatingNotify == false)
                {
                    return Json(new int[0]);
                }

                //-----------------------------------------
                //  Если игрок потерял позицию  
                //  1500 > 1000
                //  1500 - 1000 = 500
                if (PlayerEntity.EloRatingPosition > PlayerEntity.NotifiedEloRatingPosition)
                {
                    //-----------------------------------------
                    var avg = PlayerEntity.EloRatingPosition - PlayerEntity.NotifiedEloRatingPosition;
                    PlayerEntity.NotifiedEloRatingPosition = PlayerEntity.EloRatingPosition;
                    PlayerEntity.Notifications.UpdateRatingNextNotifyDate();

                    //-----------------------------------------
                    await db.SaveChangesAsync();

                    //-----------------------------------------
                    return Json(new [] { avg });
                }
            }

            //--------------------------------------------------------
            return Json(new int[0]);
        }

        [AllowAnonymous, HttpGet]
        public async Task<IHttpActionResult> GetNotifyListForCheckGameVersion(Guid? Id, string Version = null)
        {
            Logger.Error($"[GetNotifyListForCheckGameVersion][{Id}][{Version}]");
            
            //--------------------------------------------------------
            CheckGameVersionResponse Response = CheckGameVersionResponse.None;

            //============================================
            if (string.IsNullOrWhiteSpace(Version))
            {
                Response = CheckGameVersionResponse.InvalidGameVersion;
            }
            else
            {
                try
                {
                    //====================================
                    using (var db = new DataRepository())
                    {
                        long LongVersion = 0;
                        if (long.TryParse(Version, out LongVersion))
                        {
                            var PlayerEntity = await db.AccountPlayerEntity.SingleOrDefaultAsync(p => p.PlayerId == Id);
                            if (PlayerEntity == null)
                            {
                                //  Что бы не флудить уведомлениями каждые 20м
                                Response = CheckGameVersionResponse.UpdateNotExist;
                            }
                            else if(PlayerEntity.Notifications.IsAllowUpdateExistNotify && PlayerEntity.Notifications.IsAllowNotificationBroadcast)
                            {
                                //==================================================
                                var TargetGameVesrion = db.GameVersionEntities.AsNoTracking().SingleOrDefault(p => p.EntityId == LongVersion);

                                //==================================================
                                if (TargetGameVesrion == null)
                                {
                                    Response = CheckGameVersionResponse.CurrentGameVersionNotFound;
                                }
                                else
                                {
                                    //  Доступные опубликованные версии
                                    var AvalibleGameVersionEntities = db.GameVersionEntities.Where(p =>
                                        p.DeployTarget == TargetGameVesrion.DeployTarget &&
                                        p.EntityId > TargetGameVesrion.EntityId &&
                                        p.HasPublished).ToArray();

                                    //  Если есть доступные версии с учетом времени публикации
                                    if (AvalibleGameVersionEntities.Any(p => p.HasAvalibleByDeliveryTime))
                                    {
                                        PlayerEntity.Notifications.AddUpdateExistNotify();

                                        //  Если текущая версия еще опубликована, то доступно новое обновление
                                        if (TargetGameVesrion.HasPublished)
                                        {
                                            Response = CheckGameVersionResponse.UpdateAvailable;
                                        }

                                        //  Если текущая версия уже удалена, то требуется обновление
                                        else
                                        {
                                            Response = CheckGameVersionResponse.UpdateRequired;
                                        }
                                    }
                                    else
                                    {
                                        Response = CheckGameVersionResponse.UpdateNotExist;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Response = CheckGameVersionResponse.InvalidGameVersion;
                        }
                        
                    }
                }
                catch (Exception e)
                {
                    Response = CheckGameVersionResponse.UpdateCheckException;
                    LoggerContainer.AccountLogger.WriteExceptionMessage($"[GetNotifyListForCheckGameVersion][{Id}][{Version}][Exception]", e);
                }
            }
            //============================================
            return Json(new [] { Response });
        }

        [AllowAnonymous, HttpGet]
        public async Task<IHttpActionResult> GetNotifyListForPlayerReturn(Guid? Id)
        {
            //--------------------------------------------------------
            PlayerActivityNotificationType Notification = PlayerActivityNotificationType.None;

            //====================================
            LoggerContainer.AccountLogger.Warn($"[GetNotifyListForPlayerReturn][{Id}][0]");
            try
            {
                //====================================
                using (var db = new DataRepository())
                {
                    //--------------------------------------------------------
                    var PlayerEntity = await db.AccountPlayerEntity.SingleOrDefaultAsync(p => p.PlayerId == Id);

                    //--------------------------------------------------------
                    LoggerContainer.AccountLogger.Warn($"[GetNotifyListForPlayerReturn][{Id}][{PlayerEntity?.PlayerName}][IsAllowNotificationBroadcast: {PlayerEntity?.Notifications?.IsAllowNotificationBroadcast}]");

                    if (PlayerEntity != null && PlayerEntity.Notifications.IsAllowNotificationBroadcast)
                    {
                        //--------------------------------------------------------
                        if (PlayerEntity.IsLowActivity)
                        {
                            PlayerEntity.Notifications.AddLowActivityNotify();
                            Notification = PlayerActivityNotificationType.LowActivity;
                        }

                        //--------------------------------------------------------
                        else if(PlayerEntity.Notifications.IsAllowAfterLevelUpNotify)
                        {
                            PlayerEntity.Notifications.AddAfterLevelUpNotify();
                            Notification = PlayerActivityNotificationType.AfterLevelUp;
                        }

                        //--------------------------------------------------------
                        else if (PlayerEntity.Notifications.IsAllowAfterMatchNotif)
                        {
                            PlayerEntity.Notifications.AddAfterMatchNotify(false);
                            Notification = PlayerActivityNotificationType.AfterMatch;
                        }

                        //--------------------------------------------------------
                        else if(PlayerEntity.Notifications.IsAllowBeforeLevelUpNotify)
                        {
                            PlayerEntity.Notifications.AddBeforeLevelUpNotify();
                            Notification = PlayerActivityNotificationType.BeforeLevelUp;
                        }

                        //--------------------------------------------------------
                        LoggerContainer.AccountLogger.Warn($"[GetNotifyListForPlayerReturn][{Id}][{PlayerEntity.PlayerName}][Notification: {Notification} / {(int)Notification}]");

                        //--------------------------------------------
                        try
                        {
                            await db.SaveChangesAsync();
                        }
                        catch (Exception e)
                        {
                            LoggerContainer.AccountLogger.WriteExceptionMessage($"[GetNotifyListForPlayerReturn][{Id}][Exception][1]", e);
                            Notification = PlayerActivityNotificationType.None;
                        }
                    }
                }
            }
            catch(Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[GetNotifyListForPlayerReturn][{Id}][Exception][0]", e);
                Notification = PlayerActivityNotificationType.None;
            }
            return Json(new []{ Notification });
        }

        [AllowAnonymous, HttpGet]
        public async Task<IHttpActionResult> GetNotifyListForPlayerCases(Guid? Id)
        {
            try
            {
                LoggerContainer.AccountLogger.Warn($"[GetAvalibleCaseIds][{Id}][0]");

                //====================================
                using (var db = new DataRepository())
                {
                    //--------------------------------------------
                    var PlayerEntity = await db.AccountPlayerEntity.AsNoTracking().SingleOrDefaultAsync(p => p.PlayerId == Id);

                    //--------------------------------------------
                    LoggerContainer.AccountLogger.Warn($"[GetNotifyListForPlayerCases][{Id}][{PlayerEntity?.PlayerName}][IsAllowNotificationBroadcast: {PlayerEntity?.Notifications?.IsAllowNotificationBroadcast}]");

                    //--------------------------------------------
                    if (PlayerEntity != null && PlayerEntity.Notifications.IsAllowNotificationBroadcast)
                    { 
                        //--------------------------------------------
                        var PlayerCases = db.PlayerCaseEntities.Include(p => p.CaseEntity).Where(p => p.PlayerId == Id);
                        var AvalibleCases = PlayerCases.Where(p => DateTime.UtcNow >= p.NextNotifyDate).ToArray();
                        var TargetPlayerCase = AvalibleCases.FirstOrDefault(p => p.CaseEntity.IsEveryDayCase && p.IsAllowUseByTime);

                        //--------------------------------------------
                        if (TargetPlayerCase?.CaseEntity?.NextNotifyDelay != null)
                        {
                            TargetPlayerCase.NextNotifyDate = DateTime.UtcNow.AddNightSafeTime(TargetPlayerCase.CaseEntity.NextNotifyDelay.Value);
                        }
                        else if (TargetPlayerCase != null)
                        {
                            TargetPlayerCase.NextNotifyDate = DateTime.UtcNow.AddNightSafeTime(TimeSpan.FromHours(3));
                        }

                        //--------------------------------------------
                        LoggerContainer.AccountLogger.Warn($"[GetAvalibleCaseIds][{Id}][{PlayerEntity?.PlayerName}][PlayerCases: {PlayerCases.Count()} |AvalibleCases: {AvalibleCases.Length}][TargetPlayerCase: {TargetPlayerCase?.EntityId} / {TargetPlayerCase?.CaseId}]");
                        
                        //--------------------------------------------
                        try
                        {
                            //===================================================
                            if (TargetPlayerCase != null)
                            {
                                PlayerEntity.Notifications.UpdateLastNotifyDate();
                            }

                            //===================================================
                            db.SaveChanges();

                            //===================================================
                            if (TargetPlayerCase != null)
                            {
                                return Json(new int[] { TargetPlayerCase.CaseId });
                            }
                        }
                        catch (Exception e)
                        {
                            LoggerContainer.AccountLogger.WriteExceptionMessage($"[GetAvalibleCaseIds][{Id}][{PlayerEntity?.PlayerName}][Exception][1]", e);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[GetAvalibleCaseIds][{Id}][0]", e);
            }
            return Json(new int[0]);
        }
    }
}
