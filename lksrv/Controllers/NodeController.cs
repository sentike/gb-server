﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using Loka.Common.Award;
using Loka.Common.Cluster;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.GameMode.PvP;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Common.Store.Abstract;


using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Controllers.IdentityController.Operation;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models.Queue;
using RestSharp;
using VRS.Infrastructure;
using VRS.Infrastructure.Environment;

namespace Loka.Server.Controllers
{
    public class MatchHeartBeatView
    {
        public short TimeLeftInSeconds { get; set; }
        public bool IsAllowJoinNewPlayers { get; set; }
    }

    public partial class NodeController : ApiController
    {

        public class NodeSessionMember 
            : IWithEloRatingScore
            , IWithIsoCountry
        {
            public Guid PlayerId { get; set; }
            public Guid MemberId { get; set; }
            public Guid TokenId { get; set; }
            public Guid TeamId { get; set; }
            public Guid SquadId { get; set; }
            public int TargetLevel { get; set; }
            public int TargetWeaponLevel { get; set; }
            public int TargetArmourLevel { get; set; }
            public int TargetWeaponUpgrade { get; set; }
            

            public int WinRate { get; set; }

            public string PlayerName { get; set; }
            public short EloRatingScore { get; set; }
            public EIsoCountry Country { get; set; }

            public NodeSessionMember(MatchMemberEntity m)
            {
                //======================
                PlayerId = m.PlayerId;
                MemberId = m.MemberId;
                TeamId = m.TeamId;
                TokenId = m.SessionToken;
                TargetLevel = m.TargetWeaponLevel;
                TargetWeaponLevel = m.TargetWeaponLevel;
                TargetArmourLevel = m.TargetArmourLevel;
                TargetWeaponUpgrade = m.TargetWeaponUpgrade;
                EloRatingScore = m.EloRatingScore;
                WinRate = m.WinRate;

                //======================
                var p = m.PlayerEntity;
                if (p != null)
                {
                    PlayerName = p.PlayerName;
                    Country = m.PlayerEntity.Country;
                    SquadId = p.QueueEntity.PartyId;
                }

            }
        }

        public class NodeSessionMatch
        {
            public Guid NodeId { get; set; }
            public Guid MatchId { get; set; }
            public ProcessCreateTargetLevel TargetLevel { get; set; }
            public SearchSessionOptions Options { get; set; }

            public Guid[] TeamList { get; set; }
            public NodeSessionMember[] Members { get; set; }

            public NodeSessionMatch(MatchEntity match)
            {
                NodeId = match.MatchId;
                MatchId = match.MatchId;

                Options = match.Options;
                Options.Difficulty = 3.5;
                TargetLevel = match.TargetLevel;
                TeamList = match.TeamList.Select(m => m.TeamId).ToArray();
                Members = match.MemberList.Select(m => new NodeSessionMember(m)).ToArray();
            }
        }


        [AllowAnonymous, HttpGet]
        public IHttpActionResult CreateMaps()
        {
            using (var db = new DataRepository())
            {
                var gms = db.GameModeEntity.AddRange(new List<GameModeEntity>
                {
                    new LastHeroGameModeEntity(10),

                    new TeamGameModeEntity(GameModeTypeId.TeamDeadMatch, 50),
                    new TeamGameModeEntity(GameModeTypeId.ResearchMatch, 50),
                }).ToArray();

                foreach (var gm in gms)
                {
                    gm.AddMaps();
                }
                return Ok(db.SaveChanges());
            }
        }
        


        [Authorize, HttpGet]
        public IHttpActionResult CheckAvalibleMatch()
        {
            if (CurrentMatchEntity == null)
            {
                LoggerContainer.ClusterLogger.Warn($"[CheckAvalibleMatch][0] {CurrentMatchId} | CurrentMatchEntity was null");
                return Conflict();
            }


            LoggerContainer.ClusterLogger.Trace($"[CheckAvalibleMatch][1] {CurrentMatchId} | State: {CurrentMatchEntity.State} | LastActivityDate: {CurrentMatchEntity.LastActivityDate}");
            CurrentMatchEntity.LastActivityDate = DateTime.UtcNow;
            return Json(new NodeSessionMatch(CurrentMatchEntity));
        }



        [Authorize, HttpGet]
        public IHttpActionResult GameNodeHeartBeat()
        {
            CurrentMatchEntity.LastActivityDate = DateTime.UtcNow;
            return Ok();
        }

        [Authorize, HttpPost]
        public IHttpActionResult StartMatch(FReqestOneParam<short> port)
        {
            try
            {
                //LoggerContainer.ClusterLogger.Trace($"[StartMatch][1] {CurrentMatchId} | State: {CurrentMatchEntity.State} | {CurrentMatchEntity.GameMapTypeId} - {CurrentMatchEntity.GameModeTypeId} on address: {CurrentMatchEntity.MachineAddress}");
                CurrentMatchEntity.OnStartNode(port);
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage($"[StartMatch][2] {CurrentMatchId}", e);
                throw;
            }
            return Ok();
        }

        [Authorize, HttpPost]
        public IHttpActionResult CheckAvalibleMember(MatchHeartBeatView view)
        {          
            if (CurrentMatchEntity == null)
            {
                return Conflict();
            }

            CurrentMatchEntity.IsAllowJoinNewPlayers = view.IsAllowJoinNewPlayers;
            CurrentMatchEntity.TimeLeftInSeconds = view.TimeLeftInSeconds;

            var members = CurrentMatchEntity.MemberList.Where(m => m.State == MatchMemberState.Created).Select(m => new NodeSessionMember(m)).ToArray();
            LoggerContainer.ClusterLogger.Trace($"CheckAvalibleMember: {members.Length}");
            return Json(members);
        }

        [Flags]
        public enum MemberJoinResponseStatus
        {
            None = 0,
            Allow = 1,
            DisAllow = 2,
            Confirmed = 4,
        }

        public struct ConfirmMemberContainer
        {
            public Guid MemberId { get; set; }
            public MemberJoinResponseStatus Status { get; set; }
        }

        public struct ConfirmMemberRequest
        {
            public List<ConfirmMemberContainer> Members { get; set; }
        }


        [Authorize, HttpPost]
        public IHttpActionResult ConfirmMemberJoin(ConfirmMemberRequest request)
        {
            LoggerContainer.ClusterLogger.Trace($"ConfirmMemberJoin: {request.Members.Count}");
            foreach (var response in request.Members)
            {
                var member = CurrentMatchEntity.MemberList.SingleOrDefault(m => m.MemberId == response.MemberId);
	            if (member == null)
	            {
		            member = ViewByNode.Db.MatchMember.SingleOrDefault(m => m.MemberId == response.MemberId);
		            if (member == null)
		            {
			            LoggerContainer.ClusterLogger.Error($"Join: {response.MemberId} | {response.Status} -- not found");
		            }
		            else
		            {
						LoggerContainer.ClusterLogger.Error($"Join: {member.PlayerEntity.PlayerName} | {response.Status} [{member.State}] -- not found");
					}
				}
				else
                {
                    if (response.Status.HasFlag(MemberJoinResponseStatus.Allow))
                    {
                        member.State = MatchMemberState.Started;
                    }
                    else
                    {
                        member.State = MatchMemberState.Finished;
                    }

                    LoggerContainer.ClusterLogger.Error($"Join: {member.PlayerEntity.PlayerName} | {response.Status} [{member.State}] -- was found");
                }
            }
            return Json(request.Members);
        }


        [Authorize, HttpPost]
        public IHttpActionResult LeaveSession(FReqestOneParam<Guid> memberId)
        {
            var session = ViewByNode.SessionInstance;
            if (session == null)
            {
                return BadRequest("session was nullptr");
            }

            var member = session.MemberList.SingleOrDefault(p => p.MemberId == memberId);
            if (member == null)
            {
                return BadRequest("member was nullptr");
            }

	        member.State = MatchMemberState.Deserted;
			member.PlayerEntity.QueueEntity.Ready = QueueState.None;
	        ViewByNode.Db.SaveChanges();

			return Ok(memberId);
        }

        [AllowAnonymous, HttpGet]
        public string GetRemoteAddress()
        {
            return HttpContext.Current.Request.UserHostAddress;
        }

        [Authorize, HttpPost]
        public IHttpActionResult CompleteMatch(MatchResultView result)
        {
            try
            {
                if (CurrentMatchEntity == null)
                {
                    LoggerContainer.ClusterLogger.Warn($"[CompleteMatch][0] [{CurrentMatchId}] | CurrentMatchEntity was null");
                    return Conflict();
                }

                LoggerContainer.ClusterLogger.Trace($"[CompleteMatch][0] [{CurrentMatchId}] | {CurrentMatchEntity.State} | Created: {CurrentMatchEntity.Created} | Elapsed: {CurrentMatchEntity.Created.ToElapsedSeconds()}");

                CurrentMatchEntity.Finished = DateTime.UtcNow;
                CurrentMatchEntity.State = MatchGameState.Finished;
                ViewByNode.Db.SaveChanges();

                try
                {
                    CurrentMatchEntity.GameModeEntity(ViewByNode.Db).Finish(ViewByNode.SessionInstance, result, ViewByNode.Db);
                    ViewByNode.Db.SaveChanges();
                }
                catch (Exception e)
                {
                    LoggerContainer.ClusterLogger.WriteExceptionMessage($"[CompleteMatch][0] [{CurrentMatchId}]", e);
                    throw;
                }

                ViewByNode.Db.SaveChanges();
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage($"[CompleteMatch][1] [{CurrentMatchId}]", e);
                throw;
            }



            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            ViewByNode?.Dispose();
            base.Dispose(disposing);
        }

        public Guid CurrentMatchId => CurrentMatchEntity?.MatchId ?? Guid.Empty;
        public MatchEntity CurrentMatchEntity => ViewByNode?.SessionInstance;
        public ClusterNodePrincipal ViewByNode => User as ClusterNodePrincipal;
        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
    }
}
