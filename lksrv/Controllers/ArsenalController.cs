﻿using Loka.Server.Infrastructure;
using System.Web.Http;
using System.Linq;
using Loka.Common.Player.Inventory;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Loka.Common.Payments;
using Loka.Common.Store.Abstract;
using Loka.Server.Models.Arsenal;
using Loka.Common.Player.Profile.Profile;
using Loka.Common.Player.Profile.Item;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Store;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Service;
using VRS.Infrastructure;
using System.Web.Hosting;
using Loka.Common.Cases;
using SteamSharp;
using Loka.Common.Match.GameMode;
using System.Collections.Generic;
using System.Net;
using Loka.Common.Player.Discount;
using Loka.Server.Player.Models;

namespace Loka.Server.Controllers.HangarController
{

    public class MarketDeliveryRequest
    {
        public string ProductId { get; set; }
        public string PurchaseToken { get; set; }
        public string DataSignature { get; set; }
        public string Reciept { get; set; }

        public override string ToString()
        {
            return $"[ProductId: {ProductId}]{Environment.NewLine}[PurchaseToken: {PurchaseToken}]{Environment.NewLine}[DataSignature: {DataSignature}]{Environment.NewLine}[Reciept: {Reciept}]";
        }

    }

    public class MarketDeliveryResponse
    {
        public string OrderId { get; set; }
        public long Amount { get; set; }
        public string ProductId { get; set; }
    }


    public class ArsenalController : AbstractApiController
    {
        //[AllowAnonymous, HttpGet]
        //public IHttpActionResult OnMarketDeliveryTest()
        //{
        //    var path = HostingEnvironment.MapPath("~/Content/LokaAW-3ed494c017b2.p12");
        //    var exist = File.Exists(path);
        //    if (exist)
        //    {
        //        X509Certificate2 cert = null;
        //        var raw = File.ReadAllBytes(path);
        //
        //        try
        //        {
        //            var certificate = new X509Certificate2();
        //            certificate.Import(raw, "notasecret", X509KeyStorageFlags.Exportable);
        //
        //            return Ok(new
        //            {
        //                path = HostingEnvironment.MapPath("~/Content/LokaAW-3ed494c017b2.p12"),
        //                exist = File.Exists(HostingEnvironment.MapPath("~/Content/LokaAW-3ed494c017b2.p12")),
        //                raw = raw,
        //                hash = certificate.GetCertHashString()
        //            });
        //        }
        //        catch (Exception e)
        //        {
        //            return Ok(new
        //            {
        //                path = HostingEnvironment.MapPath("~/Content/LokaAW-3ed494c017b2.p12"),
        //                exist = File.Exists(HostingEnvironment.MapPath("~/Content/LokaAW-3ed494c017b2.p12")),
        //                raw = raw,
        //                Exception = e.ToString()
        //            });
        //        }
        //    }
        //    return Ok(new
        //    {
        //        path = HostingEnvironment.MapPath("~/Content/LokaAW-3ed494c017b2.p12"),
        //        exist = File.Exists(HostingEnvironment.MapPath("~/Content/LokaAW-3ed494c017b2.p12"))
        //    });
        //
        //    //return OnMarketDelivery(new MarketDeliveryRequest
        //    //{
        //    //    ProductId = "crystals_50",
        //    //    PurchaseToken = "gmggbogldgbiboihfoddlnbm.AO-J1OxxsI5C12jqSnqtl4am9R_afEIBSRLYcSTEuFhwVzbzSA_vuCZJcF6v0phHBazCf82-1lNPw3IYd0crEyMBfFJBTCa0vU6D32Tp41Jq3BBDpejYoAw",
        //    //});
        //}

        [Authorize, HttpPost]
        public IHttpActionResult OnMarketExchange(FReqestOneParam<string> inProductId)
        {
            //=========================================
            LoggerContainer.StoreLogger.Info($"[OnMarketExchange][{PlayerAccountEntity.PlayerName}][ProductId: {inProductId}]");

            //=========================================
            var product = DataRepository.MarketProductEntities.SingleOrDefault(p => p.ProductName == inProductId);
            if (product == null)
            {
                return BadRequest($"Product[{inProductId}] not found");
            }

            //=========================================
            LoggerContainer.StoreLogger.Info($"[OnMarketExchange][{PlayerAccountEntity.PlayerName}][ProductId: {inProductId}][type: {product.ProductType}][Cost: {product.Cost}][IsAllowPay: {PlayerAccountEntity.IsAllowPay(GameCurrency.Donate, product.Cost)}]");

            //=========================================
            if (product.ProductType == MarketProductType.Money && product.Cost > 0)
            {
                //=========================================
                var ItemCost = new StoreItemCost(product.Cost, GameCurrency.Donate);

                //=========================================
                var TargetProductId = product.TargetProductId ?? 0;
                if (TargetProductId != 0)
                {
                    var DicountCoupon = PlayerAccountEntity.GetDiscountCoupone(TargetProductId);
                    if (DicountCoupon?.HasAvalible ?? false)
                    {
                        ItemCost = DicountCoupon.ApplyDiscount(ItemCost);
                    }
                }

                //=========================================
                if (PlayerAccountEntity.Pay(ItemCost))
                {
                    //=========================================
                    PlayerAccountEntity.Give(GameCurrency.Money, product.Amount);
                    
                    //=========================================
                    return Json(new MarketDeliveryResponse
                    {
                        OrderId = string.Empty,
                        Amount = product.Amount,
                        ProductId = product.ProductName
                    });
                }

                return Content(HttpStatusCode.PaymentRequired, ItemCost);

            }

            //=========================================
            return Conflict();
        }

        [Authorize, HttpPost]
        public IHttpActionResult OnMarketDelivery(MarketDeliveryRequest request)
        {
            try
            {
                //=========================================
                LoggerContainer.StoreLogger.Info($"[OnMarketDelivery][{UserName}][request][ProductId: {request.ProductId}][PurchaseToken: {request.PurchaseToken}]");

                //=========================================
                var purchase = GooglePlayPayments.GetProductPurchase(request.ProductId, request.PurchaseToken);

                //=========================================
                LoggerContainer.StoreLogger.Info($"[OnMarketDelivery][{UserName}][order][OrderId: {purchase.OrderId}][Content]:{Environment.NewLine}PurchaseState: {purchase.PurchaseState}");

                //=========================================
                if (purchase.PurchaseState == 0)
                {
                    //=========================================
                    var SafeProductId = GooglePlayPayments.RemoveDiscountInProductSKU(request.ProductId);

                    //=========================================
                    var product = DataRepository.MarketProductEntities.SingleOrDefault(p => p.ProductName == SafeProductId || p.ProductName == request.ProductId);
                    if (product == null)
                    {
                        LoggerContainer.StoreLogger.Info($"[OnMarketDelivery][{UserName}][Product {request.ProductId} / [{SafeProductId}] Not Found][order][OrderId: {purchase.OrderId}][Content]:{Environment.NewLine}PurchaseState: {purchase.PurchaseState}");
                        return BadRequest($"Product[{request.ProductId}] not found");
                    }

                    //=========================================
                    if (DataRepository.PlayerPaymentEntities.Any(p =>
                        p.OrderId == purchase.OrderId || p.TokenId == request.PurchaseToken))
                    {
                        LoggerContainer.StoreLogger.Info($"[OnMarketDelivery][{UserName}][order Exist #1][OrderId: {purchase.OrderId}][Content]:{Environment.NewLine}PurchaseState: {purchase.PurchaseState}");
                        return Conflict();
                    }

                    //=========================================
                    PlayerAccountEntity.PaymentEntities.Add(new PlayerPaymentEntity(product.ProductId, purchase.OrderId, request.PurchaseToken));

                    //=========================================
                    var TargetProductId = product.TargetProductId ?? 0;
                    if (TargetProductId != 0)
                    {
                        var DicountCoupon = PlayerAccountEntity.GetDiscountCoupone(TargetProductId);
                        if (DicountCoupon?.HasAvalible ?? false)
                        {
                            DicountCoupon.UseDiscountCoupone();
                        }
                    }

                    //=========================================
                    switch (product.ProductType)
                    {
                        case MarketProductType.Money:
                            PlayerAccountEntity.Give(GameCurrency.Money, product.Amount);
                            break;
                        case MarketProductType.Donate:
                            PlayerAccountEntity.Give(GameCurrency.Donate, product.Amount);
                            break;
                        case MarketProductType.Booster:
                            PlayerAccountEntity.GivePremiumAccount((int)product.Amount);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(product.ProductType), product.ProductType, "Invalid type of product");
                    }

                    //=========================================
                    return Json(new MarketDeliveryResponse
                    {
                        OrderId = purchase.OrderId,
                        Amount = product.Amount,
                        ProductId = product.ProductName
                    });
                }

                //=========================================
                LoggerContainer.StoreLogger.Info($"[OnMarketDelivery][{UserName}][order Exist #2][OrderId: {purchase.OrderId}][Content]:{Environment.NewLine}PurchaseState: {purchase.PurchaseState}");

                //=========================================
                return Conflict();
            }
            catch (Exception e)
            {
                LoggerContainer.StoreLogger.WriteExceptionMessage($"[OnMarketDelivery][{UserName}][request][{request}]", e);
                throw;
            }
        }

        [Authorize, HttpPost]
        public IHttpActionResult UnlockItem(PlayerInventoryItemUnlock request)
        {
            //================================================
            LoggerContainer.StoreLogger.Info($"[UnlockItem][{PlayerAccountEntity.PlayerName}]{request}");

            //=========================================
            var instance = DataRepository.StoreItemInstance.SingleOrDefault(p => p.ModelId == request.ModelId);
            if (instance == null)
            {
                return NotFound();
            }

            //=========================================
            var ItemCost = instance.Cost;

            //=========================================
            var DicountCoupon = PlayerAccountEntity.GetDiscountCoupone(request.ModelId);
            if(DicountCoupon?.HasAvalible ?? false)
            {
                ItemCost = DicountCoupon.ApplyDiscount(ItemCost);
            }

            //=========================================
            if (PlayerAccountEntity.IsAllowPay(ItemCost))
            {
                PlayerInventoryItemEntity item = null;
                if (instance.Give(PlayerAccountEntity, 1, out item))
                {
                    PlayerAccountEntity.Pay(ItemCost);
                    PlayerAccountEntity.Equip(item, request.SetSlotId);
                    return Json(new PlayerInventoryItemUnlocked(item, request.Amount, instance.SafeAmount, DicountCoupon?.Coupone));
                }
                //  Ошибка, если нельзя покупать несколько одинаковых предметов
                return Conflict();
            }
            else
            {
                return Content(HttpStatusCode.PaymentRequired, new FReqestOneParam<int>(request.ModelId));
            }
        }

        public class FPlayerCaseModel
        {
            public Guid EntityId { get; set; }
            public long Amount { get; set; }
            public int CaseId { get; set; }

            public long LastActiveDate { get; set; }

            public FPlayerCaseModel() { }
            public FPlayerCaseModel(PlayerCaseEntity entity)
            {
                EntityId = entity.EntityId;
                LastActiveDate = entity.LastActiveDate.ToUnixTime();
                CaseId = entity.CaseId;
                Amount = entity.Amount;
            }

        }

        public class FDropedCaseModel : FPlayerCaseModel
        {
            public FDropedCaseModel() { }
            public FDropedCaseModel(PlayerCaseEntity entity) : base(entity)
            {

            }

            public CaseDropItemModel[] Items { get; set; }
        }

        public struct FPlayerDiscountModel
        {
            public Guid EntityId { get; set; }
            public int ModelId { get; set; }

            public EProductDiscountCoupone Coupone { get; set; }
            public long EndDiscountDate { get; set; }

            public FPlayerDiscountModel(PlayerDiscountEntity entity)
            {
                EntityId = entity.EntityId;
                ModelId = entity.ModelId;
                Coupone = entity.Coupone;
                EndDiscountDate = entity.EndDiscountDate.Value.ToUnixTime();
            }

        }

        [Authorize, HttpGet]
        public IHttpActionResult GetPlayerCases()
        {
            var result = PlayerAccountEntity.CaseEntities.Select(p => new FPlayerCaseModel(p)).ToArray();
            return Json(result);
        }

        [Authorize, HttpGet]
        public IHttpActionResult GetPlayerDiscounts()
        {
            //================================================
            var AvalibleDiscounts = PlayerAccountEntity.DiscountEntities.Where(p => p.HasAvalible).ToArray();
            var PurchasedProducts = AvalibleDiscounts.Where(p => PlayerAccountEntity.InventoryItemList.Any(i => i.ModelId == p.ModelId)).ToArray();

            //================================================
            foreach(var p in PurchasedProducts)
            {
                p.NextAvalibleDate = null;
            }

            //================================================
            var result = PlayerAccountEntity.DiscountEntities.Where(p => p.HasAvalible).ToArray().Select(p => new FPlayerDiscountModel(p)).ToArray();
            return Json(result);
        }



        [Authorize, HttpPost]
        public IHttpActionResult RequestBonusCase(FReqestOneParam<Guid> id)
        {
            //================================================
            var PlayerCase = PlayerAccountEntity.CaseEntities.SingleOrDefault(p => p.EntityId == id);

            //================================================
            LoggerContainer.AccountLogger.Error($"[RequestBonusCase][{UserName}][{id}][PlayerCase: CaseId: {PlayerCase?.CaseId} | Amount: {PlayerCase?.Amount} / CaseEntity:  {PlayerCase?.CaseEntity != null} / IsAllowGetBonusCase: {PlayerCase?.CaseEntity?.IsAllowGetBonusCase}]");

            //================================================
            if (PlayerCase == null)
            {
                return NotFound();
            }

            //================================================
            if (PlayerCase.CaseEntity == null)
            {
                return Conflict();
            }

            //================================================
            if (PlayerCase.CaseEntity.IsAllowGetBonusCase == false)
            {
                return BadRequest();
            }

            //================================================
            var award = PlayerCase.CaseEntity.GiveAwardToPlayer(PlayerAccountEntity);

            //================================================
            var result = new FDropedCaseModel(PlayerCase)
            {
                Items = award.Select(p => new CaseDropItemModel(p.ItemModelId, p.Amount)).ToArray()
            };

            //================================================
            return Json(result);
        }

        [Authorize, HttpPost]
        public IHttpActionResult TakeCase(FReqestOneParam<Guid> id)
        {
            //================================================
            var PlayerCase = PlayerAccountEntity.CaseEntities.SingleOrDefault(p => p.EntityId == id);

            //================================================
            LoggerContainer.AccountLogger.Error($"[TakeCase][{UserName}][{id}][PlayerCase: CaseId: {PlayerCase?.CaseId} | Amount: {PlayerCase?.Amount}]");

            //================================================
            if (PlayerCase == null)
            {
                return NotFound();
            }

            //================================================
            if (PlayerCase.CaseEntity == null)
            {
                return Conflict();
            }

            //================================================
            LoggerContainer.AccountLogger.Error($"[TakeCase][{UserName}][IsEveryDayCase: {PlayerCase.CaseEntity.IsEveryDayCase}][IsAllowUse: {PlayerCase.IsAllowUse}][LastActiveDate: {PlayerCase.LastActiveDate}]");

            //================================================
            if (PlayerCase.CaseEntity.IsEveryDayCase && PlayerCase.Amount < 1)
            {
                PlayerCase.SetAmount(1);
            }

            //================================================
            if (PlayerCase.IsAllowUse)
            {
                //------------------------------
                var award = PlayerCase.CaseEntity.GiveAwardToPlayer(PlayerAccountEntity);
                PlayerCase.LastActiveDate = DateTime.UtcNow;
                PlayerCase.Use();

                //------------------------------
                var result = new FDropedCaseModel(PlayerCase)
                {
                    Items = award.Select(p => new CaseDropItemModel(p.ItemModelId, p.Amount)).ToArray()
                };

                //------------------------------
                return Json(result);
            }

            //================================================
            return BadRequest();
        }

        //  Покупка по количеству
        //[Authorize, HttpPost]
        //public IHttpActionResult UnlockItem(PlayerInventoryItemUnlock request)
        //{         
        //    //================================================
        //    LoggerContainer.StoreLogger.Info($"[UnlockItem][{PlayerAccountEntity.PlayerName}]{request}");
        //
        //    //=========================================
        //    var instance = DataRepository.StoreItemInstance.SingleOrDefault(p => p.ModelId == request.ModelId);
        //    if(instance == null)
        //    {
        //        return NotFound();
        //    }
        //
        //    //=========================================
        //    if(request.Amount < 1)
        //    {
        //        request.Amount = 1;
        //    }
        //    else if (request.Amount > 10000)
        //    {
        //        request.Amount = 10000;
        //    }
        //
        //    //=========================================
        //    var item = PlayerAccountEntity.InventoryItemList.SingleOrDefault(p => p.ModelId == request.ModelId);
        //
        //    //=========================================
        //    var bought = instance.Bought(PlayerAccountEntity, request.Amount);
        //    var cost = instance.Cost * bought;
        //
        //    //=========================================
        //    if (item == null)
        //    {
        //        //=========================================
        //        //  Покупаем предмет в первый раз
        //        if (PlayerAccountEntity.Pay(cost))
        //        {
        //            PlayerAccountEntity.InventoryItemList.Add(item = new PlayerInventoryItemEntity(PlayerAccountEntity.PlayerId, instance, bought));
        //            PlayerAccountEntity.Equip(item, request.SetSlotId);
        //        }
        //        else
        //        {
        //            return BadRequest("Not enouth money");
        //        }
        //    }
        //    else if (instance.Flags.HasFlag(ItemInstanceFlags.AllowSome))
        //    {
        //        if (PlayerAccountEntity.Pay(cost))
        //        {
        //            //  Покупаем предмет, если разрешено покупать несколько
        //            item.Amount += bought;
        //        }
        //        else
        //        {
        //            return BadRequest("Not enouth money");
        //        }
        //    }
        //    else
        //    {
        //        //  Ошибка, если нельзя покупать несколько одинаковых предметов
        //        return Conflict();
        //    }
        //
        //    return Json(new PlayerInventoryItemUnlocked(item, request.Amount, bought));
        //}

        [Authorize, HttpPost]
        public IHttpActionResult EquipItem(PlayerProfileItemModel request)
        {
            //================================================
            LoggerContainer.StoreLogger.Info($"[EquipItem][{PlayerAccountEntity.PlayerName}]{request}");

            //================================================
            var item = PlayerAccountEntity.InventoryItemList.SingleOrDefault(p => p.ItemId == request.ItemId);
            if (item == null)
            {
                return NotFound();
            }

            //================================================
            PlayerAccountEntity.Equip(item, request.SetSlotId);

            //================================================
            return Json(request);
        }

        public class FUsePlayerItemRequest
        {
            public Guid EntityId { get; set; }
            public int Amount { get; set; }

            public override string ToString()
            {
                return $"[FUsePlayerItemRequest][EntityId: {EntityId}][Amount: {Amount}]";
            }
        }

        [Authorize, HttpPost]
        public IHttpActionResult UsePlayerItem(FUsePlayerItemRequest request)
        {
            //================================================
            LoggerContainer.StoreLogger.Info($"[UsePlayerItem][{PlayerAccountEntity.PlayerName}]{request}");

            //================================================
            var item = PlayerAccountEntity.InventoryItemList.SingleOrDefault(p => p.ItemId == request.EntityId);
            if (item == null)
            {
                return NotFound();
            }

            //================================================
            item.Use(request.Amount);

            //================================================

            var response = new PlayerInventoryItemModel(item);

            //================================================
            return Json(response);
        }


        public class UpgradeItemResponse
        {
            public Guid EntityId { get; set; }
            public int Level { get; set; }

            public override string ToString()
            {
                return $"EntityId: {EntityId} | Level: {Level}";
            }
        }



        [Authorize, HttpPost]
        public IHttpActionResult UpgradeItem(UpgradeItemResponse InItemId)
        {
            //================================================
            LoggerContainer.StoreLogger.Info($"[UpgradeItem][{PlayerAccountEntity.PlayerName}]{InItemId}");

            //================================================
            var item = PlayerAccountEntity.InventoryItemList.SingleOrDefault(p => p.ItemId == InItemId.EntityId);
            if (item == null)
            {
                return NotFound();
            }

            //================================================
            var from = item.Level;
            var UpgradeStatus = item.Upgrade(InItemId.Level);

            LoggerContainer.StoreLogger.Info($"[UpgradeItem][{PlayerAccountEntity.PlayerName}][{InItemId}][{from} -> {item.Level} level][UpgradeStatus: {UpgradeStatus}]");

            if (UpgradeStatus == PlayerInventoryItemEntity.UpgradeStatus.Successfully)
            {
                return Json(new UpgradeItemResponse
                {
                    EntityId = item.ItemId,
                    Level = item.Level
                });
            }
            else if(UpgradeStatus == PlayerInventoryItemEntity.UpgradeStatus.MaximumLevelReached)
            {
                return BadRequest();
            }
            else if (UpgradeStatus == PlayerInventoryItemEntity.UpgradeStatus.RequestedInvalidLevel)
            {
                return Conflict();
            }
            //================================================  
            return Content(HttpStatusCode.PaymentRequired, new FReqestOneParam<Guid>(InItemId.EntityId));
        }


        [Authorize, HttpGet]
        public IHttpActionResult GetPlayerProfile()
        {
            return Json(new[] { new PlayerProfileModel(PlayerAccountEntity) });
        }

    }
}
