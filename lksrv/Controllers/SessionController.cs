﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Loka.Common.Achievement;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;

using Loka.Infrastructure;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models.Session;
using Ninject;
using VRS.Infrastructure;
using WebApi.OutputCache.V2;
using System.Data.Entity;
using System.Threading.Tasks;
using Loka.Common.Award;
using Loka.Common.Store;
using VRS.Infrastructure.Environment;

namespace Loka.Server.Session
{
    public class SessionController : AbstractApiController
    {
        public struct LobbyMatchMemberResult 
            : IWithEloRatingScore
            , IWithPreviewEloRatingScore
            , IWithIsoCountry
        {
            public Guid MemberId { get; set; }

            public Guid PlayerId { get; set; }
            public Guid TeamId { get; set; }
            public string PlayerName { get; set; }
            public short EloRatingScore { get; set; }
            public short EloRatingScorePreview { get; set; }

            public List<PlayerGameAchievement> Achievements { get; set; }
            public bool HasUsedPremiumAccount { get; set; }
            public MatchMemberHistoryState State { get; set; }
            public EIsoCountry Country { get; set; }

            public LobbyMatchMemberResult(MatchMemberHistoryEntity entity)
            {

                MemberId = entity.MemberId;
                PlayerId = entity.PlayerId;
                TeamId = entity.TeamId;
                State = entity.State;
                EloRatingScore = entity.EloRatingScore;
                EloRatingScorePreview = entity.EloRatingScorePreview;
                PlayerName = entity.PlayerEntity.PlayerName;
                Country = entity.PlayerEntity.Country;
                HasUsedPremiumAccount = entity.HasUsedPremiumAccount;
                Achievements = entity.AchievementEntities.Select(p => new PlayerGameAchievement(p.AchievementId, p.Amount)).ToList();
            }

            public LobbyMatchMemberResult(MatchMemberBotHistoryEntity entity, Random random)
            {

                MemberId = Guid.Empty;
                PlayerId = Guid.Empty;
                TeamId = entity.TeamId;
                HasUsedPremiumAccount = false;
                State = MatchMemberHistoryState.None;
                PlayerName = entity.PlayerName;
                EloRatingScore = Convert.ToInt16(random.Next(1000, 1200));
                EloRatingScorePreview = Convert.ToInt16(random.Next(900, 1200));
                Country = (EIsoCountry) random.Next((int)EIsoCountry.AU, (int)EIsoCountry.JP);

                Achievements = new List<PlayerGameAchievement>()
                {
                    new PlayerGameAchievement(AchievementModelId.Kills, entity.Kills),
                    new PlayerGameAchievement(AchievementModelId.Deads, entity.Deads),
                    new PlayerGameAchievement(AchievementModelId.Score, entity.Score),
                };
            }

        }

        public struct LobbyMatchResultDetailed
        {
            private LobbyMatchResultDetailed(MatchHistoryEntity matchEntity)
            {
                var rnd = new Random();

                //Achievements = new List<PlayerGameAchievement>();
                Members = matchEntity.MemberList.Select(p => new LobbyMatchMemberResult(p)).ToList();
                Members.AddRange(matchEntity.BotList.Select(p => new LobbyMatchMemberResult(p, rnd)).ToList());
                Scores = matchEntity.TeamList.ToDictionary(p => p.TeamId, p => p.Score);

                MatchId = matchEntity.MatchId;
                Options = matchEntity.Options;
                OwnerTeamId = Guid.Empty;
                IsWin = false;
            }

            public LobbyMatchResultDetailed(MatchMemberHistoryEntity m) : this(m.MatchEntity)
            {
                OwnerTeamId = m.TeamId;
                IsWin = m.TeamId == m.MatchEntity.WinnerTeamId;
                //Achievements = m.AchievementEntities.Select(p => new PlayerGameAchievement(p.AchievementId, p.Amount)).ToList();
            }

            public Guid MatchId { get; set; }
            public Guid OwnerTeamId { get; set; }
            public SearchSessionOptions Options { get; set; }
            public List<LobbyMatchMemberResult> Members { get; set; }
            public Dictionary<Guid, short> Scores { get; set; }

            public bool IsWin { get; set; }
        };

        public class MatchUpgradeResponse
        {
            public long Money { get; set; }
            public long Experience { get; set; }
        }

        [Authorize, HttpPost]
        public IHttpActionResult ApplyMatchAdsBonus(FReqestOneParam<Guid?> InMatchMemberId)
        {            
            //================================
            LoggerContainer.SessionLogger.Info($"[ApplyMatchAdsBonus][0][{InMatchMemberId}][PlayerId: {UserId} / {UserName}]");

            //================================
            if (InMatchMemberId == null || InMatchMemberId.Value == null || InMatchMemberId == Guid.Empty)
            {
                return NotFound();
            }

            //================================
            var MemberEntity = User.Db.MatchMemberHistoryEntities.SingleOrDefault(p => p.MemberId == InMatchMemberId && p.PlayerId == PlayerAccountEntity.PlayerId);
            if (MemberEntity == null)
            {
                return NotFound();
            }

            //================================
            LoggerContainer.SessionLogger.Info($"[ApplyMatchAdsBonus][1][{InMatchMemberId}][PlayerId: {UserId} / {UserName}][HasUsedAdsBooster: {MemberEntity.HasUsedAdsBooster} / {MemberEntity.State}]");

            //================================
            if (MemberEntity.HasUsedAdsBooster)
            {
                return BadRequest();
            }

            //================================
            MemberEntity.State |= MatchMemberHistoryState.HasUsedAdsBooster;

            //================================
            var MoneyBefore = PlayerAccountEntity.TakeCash(GameCurrency.Money).Amount;

            //================================
            var Money = MemberEntity.AchievementEntities.SingleOrDefault(p => p.AchievementId == AchievementModelId.Money)?.Amount ?? 0;
            var Experience = MemberEntity.AchievementEntities.SingleOrDefault(p => p.AchievementId == AchievementModelId.Experience)?.Amount ?? 0;

            //================================
            PlayerAccountEntity.Give(Common.Store.GameCurrency.Money, Money);
            PlayerAccountEntity.Experience.Experience += Experience;

            //================================
            var MoneyAfter = PlayerAccountEntity.TakeCash(GameCurrency.Money).Amount;

            LoggerContainer.SessionLogger.Info($"[ApplyMatchAdsBonus][2][{InMatchMemberId}][PlayerId: {UserId} / {UserName}][Money: {Money} / Experience: {Experience}] | [{MoneyBefore} + {Money} = {MoneyAfter}");

            //================================
            DataRepository.SaveChanges();

            //================================
            return Json(new MatchUpgradeResponse
            {
                Money = Money,
                Experience = Experience
            });
        }

        [Authorize, HttpPost]
        public IHttpActionResult ApplyMatchPremiumAccountBonus(FReqestOneParam<Guid?> InMatchMemberId)
        {
            return MatchUpgrade(InMatchMemberId);
        }


        [Authorize, HttpPost]
        public IHttpActionResult MatchUpgrade(FReqestOneParam<Guid?> InMatchMemberId)
        {
            try
            {
                //================================
                LoggerContainer.SessionLogger.Info($"[MatchUpgrade][0][{InMatchMemberId}][PlayerId: {UserId} / {UserName}][HasPremiumAccount: {PlayerAccountEntity.HasPremiumAccount} / {PlayerAccountEntity.PremiumEndDate}]");

                //================================
                if (InMatchMemberId == null || InMatchMemberId.Value == null || InMatchMemberId == Guid.Empty)
                {
                    return NotFound();
                }

                //================================
                var MemberEntity = User.Db.MatchMemberHistoryEntities.SingleOrDefault(p => p.MemberId == InMatchMemberId && p.PlayerId == PlayerAccountEntity.PlayerId);
                if (MemberEntity == null)
                {
                    return NotFound();
                }

                //================================
                if (PlayerAccountEntity.HasPremiumAccount == false)
                {
                    return Conflict();
                }

                //================================
                LoggerContainer.SessionLogger.Info($"[MatchUpgrade][1][{InMatchMemberId}][PlayerId: {UserId} / {UserName}][HasPremiumAccount: {PlayerAccountEntity.HasPremiumAccount}][MemberEntity: {MemberEntity.HasUsedPremiumAccount}]");

                //================================
                if (MemberEntity.HasUsedPremiumAccount)
                {
                    return BadRequest();
                }

                //================================
                MemberEntity.State |= MatchMemberHistoryState.HasUsedPremiumAccount;

                //================================
                var Money = MemberEntity.AchievementEntities.SingleOrDefault(p => p.AchievementId == AchievementModelId.Money)?.Amount ?? 0;
                var Experience = MemberEntity.AchievementEntities.SingleOrDefault(p => p.AchievementId == AchievementModelId.Experience)?.Amount ?? 0;

                //================================
                LoggerContainer.SessionLogger.Info($"[MatchUpgrade][2][{InMatchMemberId}][PlayerId: {UserId} / {UserName}][Money: {Money} / Experience: {Experience}]");

                //================================
                PlayerAccountEntity.Give(Common.Store.GameCurrency.Money, Money);
                PlayerAccountEntity.Experience.Experience += Experience;

                //================================
                DataRepository.SaveChanges();

                //================================
                return Json(new MatchUpgradeResponse
                {
                    Money = Money,
                    Experience = Experience
                });
            }
            catch(Exception e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage($"[MatchUpgrade][{InMatchMemberId}][PlayerId: {UserId} / {UserName}]", e);
                throw;
            }
        }


        [Authorize, HttpGet]
        public async Task<IHttpActionResult> GetMatchResults()
        {
            try
            {
                //==============================================
                var lastMatchMembers = await User.Db.MatchMemberHistoryEntities.Where
                (
                    m =>    m.PlayerId == PlayerAccountEntity.PlayerId &&
                            m.State.HasFlag(MatchMemberHistoryState.Deserted) == false &&
                            m.State.HasFlag(MatchMemberHistoryState.HasNotified) == false
                ).OrderByDescending(m => m.MatchEntity.Finished).Take(10).ToArrayAsync();

                //==============================================
                foreach (var member in lastMatchMembers)
                {
                    member.State |= MatchMemberHistoryState.HasNotified;
                }

                return Json(lastMatchMembers.Select(m => new LobbyMatchResultDetailed(m)).ToArray());
            }
            catch (Exception e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage($"[GetMatchResults][1]: {PlayerAccountEntity.PlayerName}", e);
                throw;
            }
        }

        

        [AllowAnonymous, HttpGet]
        public HttpResponseMessage GetPlayerRetation()
        {
            using (var db = new DataRepository())
            {              
                //====================================
                var sb = new StringBuilder(4096);

                //====================================
                var NumOfDays = 180;

                //====================================
                var NowDate = DateTime.UtcNow;
                var BeginDate = new DateTime(NowDate.Year, NowDate.Month, NowDate.Day, 0, 0, 0, 0, System.DateTimeKind.Utc).AddDays(-NumOfDays);
                var EndDate = new DateTime(NowDate.Year, NowDate.Month, NowDate.Day, 23, 59, 59, 0, System.DateTimeKind.Utc).AddDays(1);

                //====================================
                List<KeyValuePair<int, Guid[]>> PlayersPerDays = new List<KeyValuePair<int, Guid[]>>(NumOfDays);

                //====================================
                var MatchMemberByDates = db.MatchMemberHistoryEntities.AsNoTracking().Where(p => p.MatchEntity.Created >= BeginDate && p.MatchEntity.Created <= EndDate).Select(p => new { p.PlayerId, p.MatchEntity.Created }).OrderBy(p => p.Created).AsEnumerable().GroupBy(p => p.Created.DayOfYear, p => p.PlayerId).ToArray();
                foreach(var Date in MatchMemberByDates)
                {
                    PlayersPerDays.Add(new KeyValuePair<int, Guid[]>(Date.Key, Date.Distinct().ToArray()));
                }

                //====================================
                var LastDay = 0;

                foreach (var TodayPair in PlayersPerDays)
                {
                    //---------------------------------------------
                    var YesterdayPair = PlayersPerDays.FirstOrDefault(p => p.Key > LastDay && p.Key < TodayPair.Key);

                    //---------------------------------------------
                    LastDay = YesterdayPair.Key;

                    //---------------------------------------------
                    var PlayersToday = TodayPair.Value;
                    var PlayersYesterday = YesterdayPair.Value;

                    //---------------------------------------------
                    //  Считаем количество игроков, которые вернулись
                    if ((PlayersToday?.Any() ?? false) && (PlayersYesterday?.Any() ?? false))
                    {



                        //---------------------------------------------
                        int ReturnedYesterday = PlayersYesterday.Length;
                        int ReturnedToday = 0;

                        foreach (var today in PlayersToday)
                        {
                            if (PlayersYesterday.Contains(today))
                            {
                                ReturnedToday++;
                            }
                        }

                        //------------------------------------------
                        var Yesterday = new DateTime(DateTime.UtcNow.Year, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddDays(YesterdayPair.Key);
                        var Today = new DateTime(DateTime.UtcNow.Year, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddDays(TodayPair.Key);

                        //---------------------------------------------
                        sb.AppendLine($"{Yesterday:dd.MM.yyyy} | joined {ReturnedYesterday} players > returned {ReturnedToday} players on {Today:dd.MM.yyyy} | {Convert.ToSingle(ReturnedToday) / Convert.ToSingle(ReturnedYesterday) * 100.0f :0.##}%");
                    }
                }

                //====================================
                var sbb = Encoding.ASCII.GetBytes(sb.ToString());

                //====================================
                var ms = new MemoryStream(sbb);

                HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
                httpResponseMessage.Content = new StreamContent(ms);
                httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                httpResponseMessage.Content.Headers.ContentDisposition.FileName = "PlayerRetation.txt";
                httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

                return httpResponseMessage;
            }
        }

        [AllowAnonymous, HttpGet]
        public async Task<IHttpActionResult> GetPlayerRatingTopAnonymous(Guid? id = null)
        {
            try
            {
                await PlayerRatingBuffer.Instance.CkeckUpdate();
                await PlayerEloRatingBuffer.Instance.CkeckUpdate();

                return Json(new PlayerRatingTopView
                {
                    PlayerRating = await PlayerRatingBuffer.Instance.GetData(id),
                    PlayerEloRating = await PlayerEloRatingBuffer.Instance.GetData(id),
                });
            }
            catch (Exception exception)
            {
                Logger.WriteExceptionMessage("GetPlayerRatingTopAnonymous", exception);
                return Ok();
            }
            // return Json(DataRepository.AccountPlayerEntity.AsNoTracking().Where(p => p.Experience.WeekExperience >= 0 && p.Experience.Experience >= 0).OrderByDescending(p => p.Experience.WeekExperience).Take(60).ToArray().Select(p => new PlayerRatingTopContainer(p)).ToArray());
        }

        [Authorize, HttpGet]
        public async Task<IHttpActionResult> GetPlayerRatingTop()
        {
            try
            {
                return Json(new PlayerRatingTopView
                {
                    PlayerRating = await PlayerRatingBuffer.Instance.GetData(UserId),
                    PlayerEloRating = await PlayerEloRatingBuffer.Instance.GetData(UserId),
                });
            }
            catch (Exception exception)
            {
                Logger.WriteExceptionMessage("GetPlayerRatingTop", exception);
                return Ok();
            }
            // return Json(DataRepository.AccountPlayerEntity.AsNoTracking().Where(p => p.Experience.WeekExperience >= 0 && p.Experience.Experience >= 0).OrderByDescending(p => p.Experience.WeekExperience).Take(60).ToArray().Select(p => new PlayerRatingTopContainer(p)).ToArray());
        }
    }
}
