﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Loka.Common.Award;
using Loka.Common.Infrastructure;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Match;
using Loka.Common.Player.Instance;
using Loka.Common.Player.Profile.Item;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Player.Statistic;
using Loka.Common.Version;
using Loka.Infrastructure;



using Loka.Server.Controllers.IdentityController.Operation;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Queue;
using Loka.Server.Player.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using RestSharp;
using SteamSharp;
using VRS.Infrastructure;
using VRS.Infrastructure.Environment;
using VRS.Infrastructure.IpAddress;

namespace Loka.Server.Controllers.IdentityController
{
    public partial class IdentityController : AbstractApiController
    {
        #region Steam
        //        [AllowAnonymous, HttpPost]
        //        public async Task<IHttpActionResult> OnSteamAuthentication(FReqestOneParam<string> steamId)
        //        {
        //            return new StatusCodeResult(HttpStatusCode.PaymentRequired, this);
        //#pragma warning disable 162
        //            try
        //            {
        //                using (var db = new ApplicationDbContext())
        //                {
        //                    var steamProviderKey = $"http://steamcommunity.com/openid/id/{steamId.Value}";

        //                    SteamSharp.SteamClient c = new SteamSharp.SteamClient
        //                    {
        //                        Authenticator =
        //                            SteamSharp.Authenticators.APIKeyAuthenticator.ForProtectedResource(
        //                                "178F53C45855C4341AAA4DD2D4BA4507")
        //                    };

        //                    var taskOfGetsteamUserLogin = db.UserLogins.AsNoTracking().FirstOrDefaultAsync(
        //                        u => u.LoginProvider == "Steam" && u.ProviderKey == steamProviderKey);

        //                    var taskOfGetownedGames = SteamSharp.AppOwnership.GetOwnedApplicationsAsync(c, steamId);

        //                    var ownedGames = (await taskOfGetownedGames).Apps.Application ?? new List<App>();
        //                    var lokaGame = ownedGames.FirstOrDefault(g => g.ApplicationId == 488720);

        //                    Logger.Info($"Attemp login by {steamId.Value}, owned: {lokaGame != null}");

        //                    if (lokaGame == null)
        //                    {
        //                        return new StatusCodeResult(HttpStatusCode.PaymentRequired, this);
        //                    }

        //                    //==================================================================
        //                    var steamUserLogin = await taskOfGetsteamUserLogin;
        //                    if (steamUserLogin == null)
        //                    {
        //                        var id = Guid.NewGuid();
        //                        var result = UserManager.Create(new ApplicationUser()
        //                        {
        //                            Id = id,
        //                            UserName = steamId,
        //                            Country = UserCountry,
        //                            Logins =
        //                            {
        //                                new ApplicationUserLogin
        //                                {
        //                                    LoginProvider = "Steam",
        //                                    ProviderKey = steamProviderKey,
        //                                    UserId = id,
        //                                }
        //                            }
        //                        });

        //                        if (result.Succeeded)
        //                        {
        //                            try
        //                            {
        //                                using (var mark = new MarketingDbContext())
        //                                {
        //                                    mark.RegistrationEntities.Add(new RegistrationEntity(id, UserCountry));
        //                                }
        //                            }
        //                            catch (Exception exception)
        //                            {
        //                                Logger.Error($"RegistrationEntities: {id} | {exception}");
        //                            }
        //                            return OnIdentification(steamId, true, db);
        //                        }
        //                        else
        //                        {
        //                            Logger.Error($"Failed identity {steamId.Value}: {string.Join(",", result.Errors)}");

        //                        }
        //                        return Json(new Authentication.Response(Guid.Empty, result.Errors));
        //                    }
        //                    else
        //                    {
        //                        var userAccount = db.Users.AsNoTracking().First(u => u.Id == steamUserLogin.UserId);
        //                        return OnIdentification(userAccount.UserName, true, db);
        //                    }
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                Logger.Error(e);
        //                return Json(new Authentication.Response(Guid.Empty, new List<string> { e.ToString() }));
        //            }
        //#pragma warning restore 162
        //        }
        #endregion

        //=====================================================================================================
        [AllowAnonymous, HttpPost]
        public IHttpActionResult PublishNewGameVersion(GameVersionPublishModel model)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    //=========================================
                    if (model.DeprecatePreviewVersions)
                    {
                        var TargetVersions = db.GameVersionEntities.Where(p => p.DeployTarget == model.DeployTarget && p.HasPublished).ToArray();
                        foreach (var version in TargetVersions)
                        {
                            version.HasPublished = false;
                        }
                    }

                    //=========================================
                    var DeprecatedNum = db.SaveChanges();

                    //=========================================
                    var entity = new GameVersionEntity
                    {
                        EntityId = model.StoreVersionId,
                        DeployTarget = model.DeployTarget,
                        ServerVersionId = model.ServerVersionId,
                        AdminComment = model.AdminComment,
                        CreatedDate = DateTime.UtcNow,
                        HasPublished = true
                    };

                    //==========================
                    db.GameVersionEntities.Add(entity);

                    //==========================
                    var changes = db.SaveChanges();
        
                    //==========================
                    return Ok($"Version[{model.DeployTarget}]: {model.StoreVersionId} | DeprecatedNum: {DeprecatedNum} | changes: {changes} | DeprecatePreviewVersions: {model.DeprecatePreviewVersions}");
                }
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[PublishNewGameVersion][Version[{model.DeployTarget}]: {model.StoreVersionId}] | DeprecatePreviewVersions: {model.DeprecatePreviewVersions}", e);
                throw;
            }
        }

        //=====================================================================================================
        [AllowAnonymous, HttpPost]
        public IHttpActionResult PublishNewServerVersion(ServerVersionPublishModel model)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    //=========================================
                    LoggerContainer.AccountLogger.Error($"[PublishNewServerVersion][ServerVersionId: {model.ServerVersionId} / {model.DeployTarget}][DeprecatePreviewVersions: {model.DeprecatePreviewVersions}]");

                    //=========================================
                    if (model.DeprecatePreviewVersions)
                    {
                        //--------------------------------------------------------------
                        var TargetVersions = db.ServerVersionEntities.Where(p => p.DeployTarget == model.DeployTarget && p.Flags.HasFlag(ServerVersionFlags.Published) && p.EntityId != model.ServerVersionId).ToArray();
                        var TargetVersionIds = TargetVersions.Select(p => p.EntityId).ToArray();

                        //--------------------------------------------------------------
                        LoggerContainer.AccountLogger.Error($"[PublishNewServerVersion][ServerVersionId: {model.ServerVersionId} / {model.DeployTarget}][DeprecatePreviewVersions: {string.Join(", ", TargetVersionIds)}]");

                        //--------------------------------------------------------------
                        foreach (var version in TargetVersions)
                        {
                            version.HasPublished = false;
                        }
                    }

                    //=========================================
                    var DeprecatedNum = db.SaveChanges();

                    //=========================================
                    var entity = db.ServerVersionEntities.SingleOrDefault(p => p.EntityId == model.ServerVersionId) ?? db.ServerVersionEntities.Add(new ServerVersionEntity
                    {
                        EntityId = model.ServerVersionId,
                        DeployTarget = model.DeployTarget,
                        AdminComment = model.AdminComment.Substring(0, Math.Min(128, model.AdminComment.Length)),
                        CreatedDate = DateTime.UtcNow,
                        HasPublished = true,
                    });

                    //=========================================
                    LoggerContainer.AccountLogger.Error($"[PublishNewServerVersion][ServerVersionId: {model.ServerVersionId} / {model.DeployTarget}][HasUpdated: {entity.HasUpdated} > false][CreatedDate: {entity.CreatedDate}]");

                    //=========================================
                    entity.HasUpdated = false;
                    

                    //==========================
                    var changes = db.SaveChanges();

                    //==========================
                    return Ok($"Version[{model.DeployTarget}]: {model.ServerVersionId} | DeprecatedNum: {DeprecatedNum} | changes: {changes} | DeprecatePreviewVersions: {model.DeprecatePreviewVersions}");
                }
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[PublishNewServerVersion][Version[{model.DeployTarget}]: {model.ServerVersionId}] | DeprecatePreviewVersions: {model.DeprecatePreviewVersions}", e);
                throw;
            }
        }

        [NonAction]
        private IHttpActionResult TryIsLatestVersion(GameVersionModel model)
        {
            using (var db = new DataRepository())
            {
                //=====================================
                //  Версии игры подходящии для сервера и ветки
                var TargetVersions = db.GameVersionEntities.Where(p => p.DeployTarget == model.DeployTarget && p.ServerVersionId == model.ServerVersionId).ToArray();

                //=====================================
                //  Получаем информацию о текущей версии
                var TargetVersion = TargetVersions.SingleOrDefault(p => p.EntityId == model.StoreVersionId)
                                    ?? TargetVersions.FirstOrDefault(p => model.StoreVersionId >= p.EntityId && p.HasPublished);

                //=====================================
                LoggerContainer.AccountLogger.Warn($"[IsLatestVersion][DeployTarget: {model.DeployTarget}][ServerVersionId: {model.ServerVersionId}][StoreVersionId: {model.StoreVersionId}][TargetVersion: {TargetVersion != null} | HasPublished: {TargetVersion?.HasPublished}]");

                //=====================================
                //  Если не удалось получить информацию о версии или версия не была опубликована
                //  То игроку необходимо обновить игру
                if (TargetVersion == null || TargetVersion?.HasPublished == false)
                {
                    //----------------------------------------
                    var ActualVersion = db.GameVersionEntities.FirstOrDefault(p => p.DeployTarget == model.DeployTarget && p.HasPublished);
                    if (ActualVersion == null)
                    {
                        return BadRequest();
                    }

                    //----------------------------------------
                    var ActualModel = new GameVersionModel
                    {
                        ServerVersionId = 0,
                        StoreVersionId = ActualVersion.EntityId,
                        DeployTarget = ActualVersion.DeployTarget
                    };

                    //----------------------------------------
                    return Content(HttpStatusCode.Conflict, ActualModel);
                }

                //=====================================
                var ActualModel2 = new GameVersionModel
                {
                    ServerVersionId = TargetVersion.ServerVersionId,
                    StoreVersionId = TargetVersion.EntityId,
                    DeployTarget = TargetVersion.DeployTarget
                };
                return Json(ActualModel2);
            }
        }

        [AllowAnonymous, HttpPost]
        public IHttpActionResult IsLatestVersion(GameVersionModel model)
        {
            //===========================================
            Exception LastException = null;

            //===========================================
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    return TryIsLatestVersion(model);
                }
                catch(Exception e)
                {
                    LastException = e;
                    LoggerContainer.AccountLogger.WriteExceptionMessage($"[IsLatestVersion][StoreVersionId: {model?.StoreVersionId} | ServerVersionId: {model?.ServerVersionId}]", e);
                }
            }

            //===========================================
            throw LastException;
        }

        #region CreateGameAccount

        [Authorize, HttpGet]
        public IHttpActionResult IsExistGameAccount()
        {
            if (CheckIsExistGameAccount())
            {
                return Ok();
            }
            return NotFound();
        }

        private bool CheckIsExistGameAccount()
        {
            try
            {
                var exist = PlayerRepository.IsExistIndex(UserId);
                LoggerContainer.AccountLogger.Trace($"[CheckIsExistGameAccount][{UserId}][Exist: {exist}]");
                return exist;
            }
            catch (Exception exception)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[CheckIsExistGameAccount][{UserId}]", exception);
                throw;
            }
        }
        #endregion

        //=====================================================================================================

        #region Authentication


        public enum LoginFromRemoteStatus
        {
            LoginNotFound,
            LoginWasExist,

            InvalidLogin,
            InvalidPassword,

            Successfully,

            //-----------------
            BadRequest,
            IsNotAllowed,
            IsLockedOut,
            RequiresTwoFactor,

            //-----------------
            SessionExist,

            //-----------------
            GameNotPurchased,
            GameNotAvailable,
        }

        public class LoginFromRemoteResponse
        {
            public Guid? AccountId { get; set; }
            public string AccountName { get; set; }
            public string[] Roles { get; set; }

            public LoginFromRemoteStatus Status { get; set; }
            public LoginFromRemoteResponse() { }

            public LoginFromRemoteResponse(Guid token, LoginFromRemoteStatus status, string name = null)
            {
                AccountName = name;
                AccountId = token;
                Status = status;
            }

            public LoginFromRemoteResponse(LoginFromRemoteStatus status)
            {
                Status = status;
            }

        }
        [AllowAnonymous, HttpPost]
        public IHttpActionResult OnAuthenticationWeb(Authentication.Request credential)
        {
            LoggerContainer.AccountLogger.Info($"OnAuthentication: {credential}");
            try
            {

                //----------------------------------------------------------
                var client = new RestClient(ServiceUrl("Account/LoginFromRemote"));
                var request = new RestRequest(Method.POST);

                //----------------------------------------------------------
                request.AddJsonBody(credential);
                var rest = client.Execute(request);

                //----------------------------------------------------------
                var response = JsonConvert.DeserializeObject<LoginFromRemoteResponse>(rest.Content);

                //----------------------------------------------------------
                if (response.AccountId == null || response.Status != LoginFromRemoteStatus.Successfully)
                {
                    return Ok(new LoginFromRemoteResponse(response.Status));
                }

                //----------------------------------------------------------
                return Ok(OnIdentification(credential, response));

            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"OnAuthentication: {credential}", e);
                throw;
            }
        }


        [AllowAnonymous, HttpPost]
        public IHttpActionResult OnAuthentication(Authentication.Request credential)
        {
            LoggerContainer.AccountLogger.Info($"OnAuthentication: {credential}");
            try
            {
                using (var db = new DataRepository())
                {
                    var PlayerNameUp = credential.Login.ToLowerInvariant();
                    var PlayerAccount = db.AccountPlayerEntity.SingleOrDefault(p => p.PlayerNameNormilized == PlayerNameUp);
                    return Ok(OnIdentification(credential, new LoginFromRemoteResponse()
                    {
                        AccountId = PlayerAccount.PlayerId,
                        AccountName = PlayerAccount.PlayerName,
                        Roles = new string[0],
                        Status = LoginFromRemoteStatus.Successfully,
                    }));
                }

                //----------------------------------------------------------
                //var client = new RestClient(ServiceUrl("Account/LoginFromRemote"));
                //var request = new RestRequest(Method.POST);
                //
                ////----------------------------------------------------------
                //request.AddJsonBody(credential);
                //var rest = client.Execute(request);
                //
                ////----------------------------------------------------------
                //var response = JsonConvert.DeserializeObject<LoginFromRemoteResponse>(rest.Content);
                //
                ////----------------------------------------------------------
                //if (response.AccountId == null || response.Status != LoginFromRemoteStatus.Successfully)
                //{
                //    return Ok(new LoginFromRemoteResponse(response.Status));
                //}
                //
                ////----------------------------------------------------------
                //return Ok(OnIdentification(credential, response));

            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"OnAuthentication: {credential}", e);
                throw;
            }
        }

        [AllowAnonymous, HttpPost]
        public LoginFromRemoteResponse OnGooglePlayAuthenticationV3(Authentication.Request credential)
        {
            //=======================================
            Guid PlayerGameAccountId;

            //=======================================
            PlayerRole role = PlayerRole.None;

            //=======================================
            var LoginId = credential.Login;
            var LoginHash = credential.Password;

            //=======================================

            //=======================================
            //  Получаем ид игрока по его GoogleId
            if (PlayerRepository.TryGetPlayerAccountIdByGoogleToken(LoginId, out PlayerGameAccountId) == false)
            { 
                //====================================================================
                //  Если не удалось найти игрока по его Email адресу и Google аккаунту
                //  То создаем новый аккаунт с случайным именем

                //====================================================================
                var guidBA = PlayerGameAccountId.ToByteArray();

                //====================================================================
                var result = PlayerRepository.CreateGameAccount($"user_{DateTime.UtcNow.Millisecond}{guidBA[0]}{CMath.ToUnixTime(DateTime.UtcNow)}{guidBA[2]}", PlayerGameAccountId, new string[0], out role, LoginId);
                if (result == false)
                {
                    LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName not created]");
                }
            }

            //=======================================
            using (var gdb = new DataRepository())
            {
                return TrySessionStart(gdb, PlayerGameAccountId);
            }
        }

        [AllowAnonymous, HttpPost]
        public LoginFromRemoteResponse OnGooglePlayAuthenticationV2(Authentication.Request credential)
        {
            //=======================================
            Guid PlayerGameAccountId;

            //=======================================
            PlayerRole role = PlayerRole.None;

            //=======================================
            var PlayerEmail = credential.Login;
            var PlayerGoogleId = credential.Password;

            //=======================================
            //  Получаем ид игрока по его GoogleId
            if (PlayerRepository.TryGetPlayerAccountIdByGoogleToken(PlayerGoogleId, out PlayerGameAccountId))
            {
                //  Привязываем Email адрес игрока к Google аккаунту
                PlayerRepository.AttachGoogleAccountToEmailAddress(PlayerGameAccountId, PlayerEmail);
            }
            else
            {
                //  Если не удалось найти игрока по его GoogleId, то ищем его по Email адресу
                if (PlayerRepository.TryGetPlayerAccountIdByEmailAddress(PlayerEmail, out PlayerGameAccountId))
                {
                    //  Привязываем Email адрес игрока к Google аккаунту
                    PlayerRepository.AttachEmailToGoogleAccount(PlayerGameAccountId, PlayerGoogleId);
                }
                else
                {
                    //====================================================================
                    //  Если не удалось найти игрока по его Email адресу и Google аккаунту
                    //  То создаем новый аккаунт с случайным именем

                    //====================================================================
                    //  PlayerGameAccountId был сгенерирован в TryGetPlayerAccountIdByGoogleToken / TryGetPlayerAccountIdByEmailAddress
                    var guidBA = PlayerGameAccountId.ToByteArray();

                    //====================================================================
                    var result = PlayerRepository.CreateGameAccount($"user_{DateTime.UtcNow.Millisecond}{guidBA[0]}{CMath.ToUnixTime(DateTime.UtcNow)}{guidBA[2]}", PlayerGameAccountId, new string[0], out role, PlayerGoogleId, PlayerEmail);
                    if (result == false)
                    {
                        LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName not created]");
                    }
                }
            }

            //=======================================
            using (var gdb = new DataRepository())
            {
                return TrySessionStart(gdb, PlayerGameAccountId);
            }
        }

        [AllowAnonymous, HttpPost]
        public LoginFromRemoteResponse OnGooglePlayAuthentication(Authentication.Request credential)
        {
            //=======================================
            Guid accountId;

            //=======================================
            PlayerRole role = PlayerRole.None;

            //=======================================
            var id = credential.Password;
            var login = credential.Login;

            //=======================================
            if (PlayerRepository.TryGetPlayerAccountIdByGoogleToken(id, out accountId) == false)
            {
                if (PlayerRepository.IsExistLogin(login))
                {
                    var guidBA = accountId.ToByteArray();
                    var result = PlayerRepository.CreateGameAccount($"user_{DateTime.UtcNow.Millisecond}{guidBA[0]}{CMath.ToUnixTime(DateTime.UtcNow)}{guidBA[2]}", accountId, new string[0], out role, id);
                    if (result == false)
                    {
                        LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName not created]");
                    }
                }
                else
                { 
                    var result = PlayerRepository.CreateGameAccount(login, accountId, new string[0], out role, id);
                    if (result == false)
                    {
                        LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName not created]");
                    }
                }
            }

            //=======================================
            using (var gdb = new DataRepository())
            {
                return TrySessionStart(gdb, accountId);
            }
        }

        [NonAction]
        private LoginFromRemoteResponse TrySessionStart(DataRepository gdb, Guid accountId)
        {
            //=======================================
            var sessions = gdb.GameSessionEntity.Where(p => p.PlayerId == accountId).ToArray();
            var session = sessions.Where(p => p.SessionAttach == PlayerSessionAttach.Client).OrderByDescending(p => p.LastActivityDate).FirstOrDefault();

            //=======================================
            if (session != null)
            {
                var elapsed = session.LastActivityDate.ToSecondsTime();

                //----------------------------------------------------------
                //  Сбрасывание статуса готовности, если давно не был в игре
                if (elapsed > 60)
                {
                    var q = gdb.QueueEntity.SingleOrDefault(p => p.QueueId == accountId);
                    if (q != null)
                    {
                        q.Ready = QueueState.None;
                    }
                }

                //----------------------------------------------------------
                if (elapsed > 10)
                {
                    LoggerContainer.AccountLogger.Info($"OnIdentification: {accountId} | SessionExist | elapsed: {elapsed}");
                    return new LoginFromRemoteResponse(LoginFromRemoteStatus.SessionExist);
                }
            }

            //=======================================
            {
                var servers = sessions.Where(p => p.SessionAttach == PlayerSessionAttach.Server && p.LastActivityDate.ElapsedMore(TimeSpan.FromHours(2))).ToArray();
                gdb.GameSessionEntity.RemoveRange(servers);
            }

            //=======================================
            {
                var clients = sessions.Where(p => p.SessionAttach == PlayerSessionAttach.Client && p.LastActivityDate.ElapsedMore(TimeSpan.FromMinutes(10))).ToArray();
                gdb.GameSessionEntity.RemoveRange(clients);
            }

            //=======================================

            var sessionId = PlayerRepository.SessionBegin(accountId);

            //========================================================================
            return new LoginFromRemoteResponse(sessionId, LoginFromRemoteStatus.Successfully);
        }

        [NonAction]
        private LoginFromRemoteResponse OnIdentification(Authentication.Request credential, LoginFromRemoteResponse response)
        {
            //=======================================
            if (response.AccountId == null)
            {
                throw new ArgumentNullException(nameof(response.AccountId));
            }

            //=======================================
            var accountId = response.AccountId.Value;

            //=======================================
            try
            {
                using (var gdb = new DataRepository())
                {
                    PlayerRole role = PlayerRole.None;
                    //=======================================
                    if (string.IsNullOrWhiteSpace(response.AccountName) == false)
                    {
                        if (PlayerRepository.IsExistIndex(accountId) == false)
                        {
                            if (response.AccountName.IsEmailAddress() == false && PlayerRepository.IsExistLogin(response.AccountName) == false)
                            {
                                var result = PlayerRepository.CreateGameAccount(response.AccountName, accountId, response.Roles, out role);
                                if(result == false)
                                {
                                    LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName not created]");
                                }
                            }
                            else
                            {
                                LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName was exist]");
                            }
                        }
                        else
                        {
                            PlayerRepository.UpdateUserRoles(accountId, response.Roles, out role);
                        }
                    }
                    else
                    {
                        LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName was null]");
                    }

                    //=======================================
                    LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][Role: {role} / {string.Join(",",  response.Roles)} | IsAdministrator: {role.HasFlag(PlayerRole.Administrator)}]");
                    
                    //if (role.HasFlag(PlayerRole.Administrator) == false && role.HasFlag(PlayerRole.BetaTester) == false)
                    //{
                    //    return new LoginFromRemoteResponse(LoginFromRemoteStatus.GameNotPurchased);
                    //}

                    //=======================================
                    return TrySessionStart(gdb, accountId);
                }
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"OnIdentification: {accountId}", e);
                throw;
            }
        }
        #endregion

        public class PlayerStatisticItemModel
        {
            public PlayerProfileInstanceSlotId SlotId { get; set; }
            public int ModelId { get; set; }
            public int Level { get; set; }

            public PlayerStatisticItemModel() { }
            public PlayerStatisticItemModel(PlayerProfileItemEntity entity)
            {
                ModelId = entity.InventoryItemEntity?.ModelId ?? -1;
                Level = entity.InventoryItemEntity?.Level ?? -1;
                SlotId = entity.SlotId;
            }
        }

        public class PlayerStatisticModel 
            : IWithEloRatingScore
            , IWithIsoCountry
        {
            public Guid EntityId { get; set; }
            public int Level { get; set; }
            public string PlayerName { get; set; }
            public long RegistrationDate { get; set; }
            public long LastActivityDate { get; set; }
            public long PremiumEndDate { get; set; }

            public PlayerFriendStatus PlayerStatus { get; set; }

            public int DefaulItemModelId { get; set; }
            public PlayerExperience Experience { get; set; }

            public List<PlayerStatisticItemModel> Items { get; set; } = new List<PlayerStatisticItemModel>();
            public List<PlayerGameAchievement> Achievements { get; set; } = new List<PlayerGameAchievement>();
            public short EloRatingScore { get; set; }
            public EIsoCountry Country { get; set; }

            public PlayerStatisticModel() { }
            public PlayerStatisticModel(PlayerEntity InPlayer)
            {
                Country = InPlayer.Country;
                EntityId = InPlayer.PlayerId;
                Level = InPlayer.Experience.Level;
                PlayerName = InPlayer.PlayerName;
                PlayerStatus = InPlayer.Status();
                Experience = InPlayer.Experience;
                EloRatingScore = InPlayer.EloRatingScore;
                LastActivityDate = InPlayer.LastActivityDate.ToUnixTime();
                RegistrationDate = InPlayer.RegistrationDate.ToUnixTime();
                PremiumEndDate = InPlayer.PremiumEndDate?.ToUnixTime() ?? 0;
                Items = InPlayer.Items.Select(p => new PlayerStatisticItemModel(p)).ToList();
                DefaulItemModelId = InPlayer.DefaultItemEntity?.InventoryItemEntity?.ModelId ?? -1;
                Achievements = InPlayer.AchievementEntities.Select(p => new PlayerGameAchievement
                {
                    Key = p.AchievementId,
                    Value = p.Amount
                }).ToList();
            }
        }

        [Authorize, HttpPost]
        public IHttpActionResult GetPlayerStatistic(FReqestOneParam<Guid?> id)
        {
            //========================================
            if (id == null || id.Value == null || id == Guid.Empty)
            {
                id = PlayerAccountEntity.PlayerId;
            }

            //========================================
            var PlayerEntity = User.Db.AccountPlayerEntity.AsNoTracking().SingleOrDefault(p => p.PlayerId == id);

            //========================================
            if (PlayerEntity == null)
            {
                return NotFound();
            }

            //========================================
            var result = new PlayerStatisticModel(PlayerEntity);

            //========================================
            return Json(result);
        }


    }
}