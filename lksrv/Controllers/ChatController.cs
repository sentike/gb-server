﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Loka.Infrastructure;
using Ninject;
using System.Linq;
using System.Web;
using Loka.Common.Chat.Chanel;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Chat;
using Loka.Server.Player.Infrastructure;
using Loka.Server.Player.Models;
using VRS.Infrastructure;
using WebApi.OutputCache.V2;

namespace Loka.Server.Chat
{
    public class ChatController : AbstractApiController
    {
        [Authorize, HttpGet]
        public IHttpActionResult GetListOfChanels()
        {
            using (var db = new DataRepository())
            {
                //----------------------------------------
                db.Configuration.AutoDetectChangesEnabled = false;

                //----------------------------------------
                var members = db.ChatChanelMemberEntities.Where(p => p.PlayerId == UserId).ToArray();

                //----------------------------------------
                var chanels = members.Select(p => p.ChanelEntity).ToArray();

                //----------------------------------------
                var models = chanels.Select(p => new ChatChanelModel(p)).ToArray();

                //----------------------------------------
                return Json(models);
            }
        }

        [Authorize, HttpGet]
        public IHttpActionResult GetListOfMembers(/*ICollection<Guid> chanels*/)
        {
            using (var db = new DataRepository())
            {
                //----------------------------------------
                db.Configuration.AutoDetectChangesEnabled = false;

                //----------------------------------------
                var members = db.ChatChanelMemberEntities.Where(p => p.PlayerId == UserId).ToArray();
                //var members = db.ChatChanelMemberEntities.Where(p => chanels.Contains(p.ChanelId)).ToArray();

                //----------------------------------------
                var models = members.Select(p => new ChatMemberModel(p)).ToArray();

                //----------------------------------------
                return Json(models);
            }
        }


        [Authorize, HttpGet]
        public IHttpActionResult GetListOfMessages(/*ICollection<Guid> chanels*/)
        {
            using (var db = new DataRepository())
            {
                //var six = DateTime.UtcNow.AddHours(-6);
                //----------------------------------------
                //var members = db.ChatChanelMemberEntities.Where(p => chanels.Contains(p.ChanelId));
                var members = db.ChatChanelMemberEntities.Where(p => p.PlayerId == UserId);


                //----------------------------------------
                //var avalible = db.ChatChanelMessageEntities.Where(p => chanels.Contains(p.ChanelId) && p.CreatedDate >= six);
                var avalible = db.ChatChanelMessageEntities.AsQueryable();

                //----------------------------------------
                var messages = members.SelectMany(m => avalible.Where(p => p.ChanelId == m.ChanelId && p.CreatedDate >= m.LastActivityDate).OrderBy(p => p.CreatedDate)).ToArray();

                //----------------------------------------
                var models = messages.Select(p => new ChatMessageReciveModel(p)).ToArray();

                //----------------------------------------
                foreach (var member in members)
                {
                    member.LastActivityDate = DateTime.UtcNow;
                }

                //----------------------------------------
                db.SaveChanges();

                //----------------------------------------
                return Json(models);
            }
        }

        [Authorize, HttpPost]
        public IHttpActionResult PostMessageTo(ChatMessageSendModel message)
        {
            using (var db = new DataRepository())
            {
                //----------------------------------------
                var member = db.ChatChanelMemberEntities.SingleOrDefault(p => p.ChanelId == message.ChanelId && p.PlayerId == UserId);

                //----------------------------------------
                if (member == null)
                {
                    return NotFound();
                }

                //----------------------------------------
                db.ChatChanelMessageEntities.Add(new ChatChanelMessageEntity
                {
                    MemberId = member.MemberId,
                    ChanelId = message.ChanelId,
                    CreatedDate = DateTime.UtcNow,
                    MessageId = Guid.NewGuid(),
                    Message = message.Message,
                });

                //----------------------------------------
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    LoggerContainer.DataBaseLogger.WriteExceptionMessage($"[PostMessageTo][{message.ChanelId}][Message: {message.Message}]", e);
                    throw;
                }

                //----------------------------------------
                return Ok();
            }
        }

    }
}