﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Loka.Common.Player.Inventory;
using Loka.Common.Tutorial;
using Loka.Infrastructure;
using Loka.Server.Infrastructure;
using VRS.Infrastructure;

namespace Loka.Server.Controllers.IdentityController
{
    public partial class IdentityController : AbstractApiController
    {

        public class StartTutorialView
        {
            public long TutorialId { get; set; }
            public long StepId { get; set; }
            public bool TutorialComplete { get; set; }
        }

        public class OnConfirmTutorialStepResponse : StartTutorialView
        {
            public long NextStepId { get; set; }
            public bool RewardReceived { get; set; }
        }



        [Authorize, HttpPost]
        public IHttpActionResult OnStartTutorial(FReqestOneParam<long> id)
        {
            try
            {
                //===================================================================================
                var tutorial = User.Db.TutorialInstanceEntities.SingleOrDefault(p => p.TutorialId == id.Value);
                if (tutorial == null)
                {
                    LoggerContainer.TutorialLogger.Warn($"OnStartTutorial: [{UserId}] [{UserName}] | Error[1]: Tutorial[{id.Value}] was null");
                    return new StatusCodeResult(HttpStatusCode.BadRequest, this);
                }

                //===================================================================================
                var step = tutorial.StepEntities.OrderBy(p => p.StepId).FirstOrDefault();
                if (step == null)
                {
                    LoggerContainer.TutorialLogger.Warn($"OnStartTutorial: [{UserId}] [{UserName}] | Error[2]: Tutorial[{id.Value}] first step was null");
                    return new StatusCodeResult(HttpStatusCode.Forbidden, this);
                }

                //===================================================================================
                var progress = PlayerAccountEntity.PlayerTutorialEntities.FirstOrDefault(p => p.TutorialId == id.Value);
                if (progress == null)
                {
                    //----------------------------------------------------------
                    var entity = new PlayerTutorialEntity
                    {
                        ProgressId = Guid.NewGuid(),
                        PlayerId = UserId,
                        TutorialId = tutorial.TutorialId,
                        StepId = step.StepKey,
                        StepEntity = step,
                    };

                    //----------------------------------------------------------
                    PlayerAccountEntity.PlayerTutorialEntities.Add(entity);
                    PlayerAccountEntity.CurrentTutorialId = entity.ProgressId;

                    //----------------------------------------------------------
                    User.Db.SaveChanges();

                    //----------------------------------------------------------
                    return Json(new StartTutorialView
                    {
                        TutorialId = entity.TutorialId,
                        StepId = entity.StepEntity.StepId
                    });
                }
                //===================================================================================
                else
                {
                    progress.Complete = tutorial.StepEntities.OrderBy(p => p.StepId).FirstOrDefault(p => p.StepId > progress.StepEntity.StepId) == null;
                    LoggerContainer.TutorialLogger.Warn($"OnStartTutorial: [{UserId}] [{UserName}] | Error[3]: Tutorial[{id.Value}] in progress, step: {progress.StepId} / Complete: {progress.Complete}");
                    return Content(HttpStatusCode.BadGateway, new StartTutorialView
                    {
                        TutorialId = progress.TutorialId,
                        StepId = progress.StepEntity.StepId,
                        TutorialComplete = progress.Complete
                    });
                }
            }
            catch (Exception e)
            {
                LoggerContainer.TutorialLogger.WriteExceptionMessage($"[OnStartTutorial][{id.Value}]", e);
                throw;
            }
        }



        [Authorize, HttpPost]
        public IHttpActionResult OnConfirmTutorialStep(StartTutorialView id)
        {
            try
            {
                //===================================================================================
                var progress = PlayerAccountEntity.PlayerTutorialEntities.FirstOrDefault(p => p.TutorialId == id.TutorialId);
                if (progress == null)
                {
                    LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] | Error[1]: Progress[{id.TutorialId}] was null");
                    return BadRequest($"Progress {id.TutorialId} not found!");
                }

                //===================================================================================
                var step = progress.StepEntity;
                var tutorial = progress.TutorialEntity;

                LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] / {PlayerAccountEntity?.CashMoney} | Info[1]: Progress[{id.TutorialId}]");
                //===================================================================================
                if (PlayerAccountEntity.CurrentTutorialId != progress.ProgressId && PlayerAccountEntity.CurrentTutorialId != null)
                {
                    LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] | Error[2]: not equal steps {id.TutorialId} of {PlayerAccountEntity.CurrentTutorialId}");
                    return StatusCode(HttpStatusCode.Forbidden);
                }

                if (progress.StepEntity?.StepId != id.StepId)
                {
                    LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] | Error[3]: not equal steps {id.StepId} of {PlayerAccountEntity.CurrentTutorialId}");
                    return StatusCode(HttpStatusCode.Forbidden);
                }

                //===================================================================================
                var next = tutorial.StepEntities.OrderBy(p => p.StepId).ToArray().FirstOrDefault(p => p.StepId > step.StepId);
                var complete = progress.Complete;
                progress.Complete = next == null;

                LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] | info[3]: step {step.StepId}[{step.StepKey}] to {next?.StepId}[{next?.StepKey}]");
                //===================================================================================
                var view = new OnConfirmTutorialStepResponse
                {
                    TutorialId = id.TutorialId,
                    StepId = id.StepId,
                    TutorialComplete = progress.Complete,
                    RewardReceived = true
                };

                //===================================================================================
                if (step.RewardModelId == null)
                {
                    LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] | Error[11]: reward: not set");
                    view.RewardReceived = false;
                }
                else
                {
                    var reward = User.Db.StoreItemInstance.FirstOrDefault(p => p.ModelId == step.RewardModelId);
                    if (reward == null)
                    {
                        LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] | Error[3]: reward was null, step[{step.TutorialId}]");
                        view.RewardReceived = false;
                    }
                    else
                    {
                        if (complete == false)
                        {
                            PlayerInventoryItemEntity item = null;
                            try
                            {
                                //item = reward.Buy(PlayerAccountEntity, PlayerAccountEntity, step.RewardGive, true, true);
                                //if (item == null)
                                //{
                                //    LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] |   Error[5]: item was null");
                                //    view.RewardReceived = false;
                                //}
                            }
                            catch (Exception e)
                            {
                                LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] |  | Error[4]: [{e}]");
                                view.RewardReceived = false;
                            }

                            try
                            {
                                if (item != null)
                                {
                                    if (item.PlayerInstance == null)
                                    {
                                        LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] |  | Error[7]: PlayerInstance was null");
                                    }
                                    else if (item.PlayerInstance.CashMoney == null)
                                    {
                                        LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] |  | Error[8]: PlayerInstance.Cash was null");
                                    }

                                    if (item.ItemModelEntity == null)
                                    {
                                        LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] |  | Error[9]: ItemModelEntity was null");
                                    }

                                    LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}]  {PlayerAccountEntity.CashMoney} | Info[2]: Progress[{id.TutorialId}]");
                                }
                            }
                            catch (Exception e)
                            {
                                LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] |  | Error[6]: [{e}]");
                                view.RewardReceived = false;
                            }
                        }
                        else
                        {
                            LoggerContainer.TutorialLogger.Error($"OnConfirmTutorialStep: [{UserId}] [{UserName}] |   Error[10]: complete");
                        }
                    }
                }

                if (next == null)
                {
                    view.NextStepId = -1;
                }
                else
                {
                    view.NextStepId = next.StepId;
                    progress.StepId = next.StepKey;
                }

                User.Db.SaveChanges();
                return Json(view);

            }
            catch (Exception e)
            {
                LoggerContainer.TutorialLogger.WriteExceptionMessage($"OnConfirmTutorialStep: [{UserId}] [{UserName}] | Progress[{id.TutorialId}] Error[6]", e);
                throw;
            }
        }
    }
}