﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

using Loka.Infrastructure;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using VRS.Infrastructure;
using System.Data.Entity;
using System.Threading.Tasks;
using Loka.Common.Player.Instance;
using Loka.Server.Player.Models;

namespace Loka.Server.Session
{
    public enum PlayersNotifyCategory
    {
        None,
        GameUpdate,
        PlayerRating,
        PlayerReturn,
        PlayerCases,
    }

    public enum PlayersNotifyStatus
    {
        None,

        NoAvailableNotify,
        InvalidInputData,
        Successfully,
        Exception,

        PlayerNotFound,
        EntityNotFound,

        AllowNotificationBroadcastFalse,
        AllowNotificationTargetFalse,

        
    }


    public class PlayersNotify
    {
        public PlayersNotifyStatus Status { get; set; }
        public PlayersNotifyCategory Category { get; set; }
        public int[] NotifyIds { get; set; } = new int[0];
        public bool IsAllowBroadcast => Status == PlayersNotifyStatus.Successfully;


        public PlayersNotify(PlayersNotifyCategory InCategory, int InId)
        {
            NotifyIds = new[] {InId};
            Category = InCategory;
            Status = PlayersNotifyStatus.Successfully;
        }

        public PlayersNotify(PlayersNotifyCategory InCategory, int[] InIds)
        {
            NotifyIds = InIds;
            Category = InCategory;
            Status = PlayersNotifyStatus.Successfully;
        }


        public PlayersNotify(PlayersNotifyStatus InStatus)
        {
            Status = InStatus;
        }

        public override string ToString()
        {
            return $"[PlayersNotify][{Status}][{Category}][{string.Join(", ", NotifyIds)}][IsAllowBroadcast: {IsAllowBroadcast}]";
        }
    }



    public class NotifyV2Controller : AbstractApiController
    {
        public delegate Task<PlayersNotify> ExceptionHandlerDelegate(DataRepository InDataRepository, PlayerEntity InPlayerEntity, string InVersion);
            

        [NonAction]
        public async Task<PlayersNotify> ExceptionHandler(string InActionName, ExceptionHandlerDelegate InDelegate, DataRepository InDataRepository, PlayerEntity InPlayerEntity, string InVersion)
        {
            try
            {
                return await InDelegate.Invoke(InDataRepository, InPlayerEntity, InVersion);
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"{InActionName}", e);
                return new PlayersNotify(PlayersNotifyStatus.Exception);
            }
        }

        [NonAction]
        public async Task<PlayersNotify> GetAvailablePlayersNotifyListProxy(Guid? Id, string Version = null)
        {
            //--------------------------------------------------------
            using (var db = new DataRepository())
            {
                //-----------------------------------------
                var notify = new PlayersNotify(PlayersNotifyStatus.None);

                //-----------------------------------------
                var PlayerEntity = await db.AccountPlayerEntity.SingleOrDefaultAsync(p => p.PlayerId == Id);
                if (PlayerEntity == null)
                {
                    return new PlayersNotify(PlayersNotifyStatus.PlayerNotFound);
                }

                //-----------------------------------------
                if (PlayerEntity.Notifications.IsAllowNotificationBroadcast == false)
                {
                    return new PlayersNotify(PlayersNotifyStatus.AllowNotificationBroadcastFalse);
                }

                //-----------------------------------------
                notify = await ExceptionHandler($"[GetAvailablePlayersNotify][CheckRating][{PlayerEntity.PlayerName} / {PlayerEntity.PlayerId}]", CheckRating, db, PlayerEntity, Version);
                if (notify.IsAllowBroadcast)
                {
                    return notify;
                }

                //-----------------------------------------
                notify = await ExceptionHandler($"[GetAvailablePlayersNotify][CheckPlayerReturn][{PlayerEntity.PlayerName} / {PlayerEntity.PlayerId}]", CheckPlayerReturn, db, PlayerEntity, Version);
                if (notify.IsAllowBroadcast)
                {
                    return notify;
                }

                //-----------------------------------------
                notify = await ExceptionHandler($"[GetAvailablePlayersNotify][CheckPlayerCases][{PlayerEntity.PlayerName} / {PlayerEntity.PlayerId}]", CheckPlayerCases, db, PlayerEntity, Version);
                if (notify.IsAllowBroadcast)
                {
                    return notify;
                }

                //-----------------------------------------
                notify = await ExceptionHandler($"[GetAvailablePlayersNotify][CheckGameVersion][{PlayerEntity.PlayerName} / {PlayerEntity.PlayerId}]", CheckGameVersion, db, PlayerEntity, Version);
                return notify;
            }
        }

        [AllowAnonymous, HttpGet]
        public async Task<IHttpActionResult> GetTestNotify(Guid Id, int Type, bool ReturnResult, string Version)
        {
            //--------------------------------------------------------
            using (var db = new DataRepository())
            {
                //-----------------------------------------
                var notify = new PlayersNotify(PlayersNotifyStatus.None);

                //-----------------------------------------
                var PlayerEntity = await db.AccountPlayerEntity.SingleOrDefaultAsync(p => p.PlayerId == Id);
                if (PlayerEntity == null)
                {
                    return Json(new PlayersNotify(PlayersNotifyStatus.PlayerNotFound));
                }

                //-----------------------------------------
                PlayerEntity.Notifications.LastNotifyDate = DateTime.UtcNow.AddDays(-1);

                //-----------------------------------------
                if (Type == 1)
                {
                    PlayerEntity.Notifications.AfterLevelUpNotifyNextDate = DateTime.UtcNow.AddDays(-1);
                }
                else if (Type == 2)
                {
                    PlayerEntity.Notifications.AfterMatchNotifyNextDate = DateTime.UtcNow.AddDays(-1);
                }
                else if (Type == 3)
                {
                    PlayerEntity.Notifications.BeforeLevelUpNotifyNextDate = DateTime.UtcNow.AddDays(-1);
                }
                else if (Type == 4)
                {
                    PlayerEntity.Notifications.LowActivityNotifyNextDate = DateTime.UtcNow.AddDays(-1);
                }
                else if (Type == 5)
                {
                    PlayerEntity.Notifications.RatingNextNotifyDate = DateTime.UtcNow.AddDays(-1);
                }
                else if (Type == 6)
                {
                    PlayerEntity.Notifications.UpdateExistNotifyNextDate = DateTime.UtcNow.AddDays(-1);
                }
                else if (Type == 7)
                {
                    foreach (var c in PlayerEntity.CaseEntities)
                    {
                        if (c.NextNotifyDate != null)
                        {
                            c.NextNotifyDate = DateTime.UtcNow.AddDays(-1);
                        }
                    }
                }

                //-----------------------------------------
                if (ReturnResult)
                {
                    return await GetAvailablePlayersNotifyList(Id, Version);
                }

                //-----------------------------------------
                return Ok($"[TestNotify][{PlayerEntity.PlayerName} / {PlayerEntity.PlayerId}][IsAllowNotificationBroadcast: {PlayerEntity.Notifications.IsAllowNotificationBroadcast}][Type: {Type}][ReturnResult: {ReturnResult}][Version: {Version}]");
            }

        }

        [AllowAnonymous, HttpGet]
        public async Task<IHttpActionResult> GetAvailablePlayersNotifyList(Guid? Id, string Version = null)
        {
            try
            {
                //-----------------------------------------
                Logger.Error($"[GetAvailablePlayersNotifyList][{Id}][Version: {Version}][Begin]");

                //-----------------------------------------
                var notify = await GetAvailablePlayersNotifyListProxy(Id, Version);
                
                //-----------------------------------------
                Logger.Error($"[GetAvailablePlayersNotifyList][{Id}][Version: {Version}][{notify}]");

                //-----------------------------------------
                return Json(notify);
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[GetAvailablePlayersNotifyList][{Id}][Version: {Version}]", e);
                throw;
            }
        }

        [NonAction]
        public async Task<PlayersNotify> CheckRating(DataRepository InDataRepository, PlayerEntity InPlayerEntity, string InVersion)
        {
            //-----------------------------------------
            if (InPlayerEntity.Notifications.IsAllowRatingNotify == false)
            {
                return new PlayersNotify(PlayersNotifyStatus.AllowNotificationTargetFalse);
            }

            //-----------------------------------------
            //  Если игрок потерял позицию  
            //  1500 > 1000
            //  1500 - 1000 = 500
            if (InPlayerEntity.EloRatingPosition > InPlayerEntity.NotifiedEloRatingPosition)
            {
                //-----------------------------------------
                var avg = InPlayerEntity.EloRatingPosition - InPlayerEntity.NotifiedEloRatingPosition;
                InPlayerEntity.NotifiedEloRatingPosition = InPlayerEntity.EloRatingPosition;
                InPlayerEntity.Notifications.UpdateRatingNextNotifyDate();

                //-----------------------------------------
                await InDataRepository.SaveChangesAsync();

                //-----------------------------------------
                return new PlayersNotify(PlayersNotifyCategory.PlayerRating, avg);
            }

            //--------------------------------------------------------
            return new PlayersNotify(PlayersNotifyStatus.NoAvailableNotify);
        }

        [NonAction]
        public async Task<PlayersNotify> CheckGameVersion(DataRepository InDataRepository, PlayerEntity InPlayerEntity, string InVersion = null)
        {
            //============================================
            long LongVersion = 0;

            //============================================
            if (string.IsNullOrWhiteSpace(InVersion))
            {
                return new PlayersNotify(PlayersNotifyStatus.InvalidInputData);
            }

            //============================================
            if (long.TryParse(InVersion, out LongVersion) == false)
            {
                return new PlayersNotify(PlayersNotifyStatus.InvalidInputData);
            }

            //============================================
            if (InPlayerEntity.Notifications.IsAllowUpdateExistNotify == false)
            {
                return new PlayersNotify(PlayersNotifyStatus.AllowNotificationTargetFalse);
            }

            //==================================================
            var TargetGameVesrion = await InDataRepository.GameVersionEntities.AsNoTracking()
                    .SingleOrDefaultAsync(p => p.EntityId == LongVersion);

            //==================================================
            if (TargetGameVesrion == null)
            {
                return new PlayersNotify(PlayersNotifyStatus.EntityNotFound);
            }

            //  Доступные опубликованные версии
            var AvalibleGameVersionEntities = await InDataRepository.GameVersionEntities.Where(p =>
                p.DeployTarget == TargetGameVesrion.DeployTarget &&
                p.EntityId > TargetGameVesrion.EntityId &&
                p.HasPublished).ToArrayAsync();

            //  Если есть доступные версии с учетом времени публикации
            if (AvalibleGameVersionEntities.Any(p => p.HasAvalibleByDeliveryTime))
            {
                InPlayerEntity.Notifications.AddUpdateExistNotify();

                //  Если текущая версия еще опубликована, то доступно новое обновление
                if (TargetGameVesrion.HasPublished)
                {
                    return new PlayersNotify(PlayersNotifyCategory.GameUpdate, (int)CheckGameVersionResponse.UpdateAvailable);
                }
                //  Если текущая версия уже удалена, то требуется обновление
                else
                {
                    return new PlayersNotify(PlayersNotifyCategory.GameUpdate, (int)CheckGameVersionResponse.UpdateRequired);
                }
            }
            return new PlayersNotify(PlayersNotifyCategory.GameUpdate, (int)CheckGameVersionResponse.UpdateNotExist);
        }

        [NonAction]
        public async Task<PlayersNotify> CheckPlayerReturn(DataRepository InDataRepository, PlayerEntity InPlayerEntity, string InVersion = null)
        {
            //--------------------------------------------------------
            if (InPlayerEntity.IsLowActivity)
            {
                InPlayerEntity.Notifications.AddLowActivityNotify();
                return new PlayersNotify(PlayersNotifyCategory.PlayerReturn, (int)PlayerActivityNotificationType.LowActivity);
            }

            //--------------------------------------------------------
            else if(InPlayerEntity.Notifications.IsAllowAfterLevelUpNotify)
            {
                InPlayerEntity.Notifications.AddAfterLevelUpNotify();
                return new PlayersNotify(PlayersNotifyCategory.PlayerReturn, (int)PlayerActivityNotificationType.AfterLevelUp);
            }

            //--------------------------------------------------------
            else if (InPlayerEntity.Notifications.IsAllowAfterMatchNotif)
            {
                InPlayerEntity.Notifications.AddAfterMatchNotify(false);
                return new PlayersNotify(PlayersNotifyCategory.PlayerReturn, (int)PlayerActivityNotificationType.AfterMatch);
            }

            //--------------------------------------------------------
            else if(InPlayerEntity.Notifications.IsAllowBeforeLevelUpNotify)
            {
                InPlayerEntity.Notifications.AddBeforeLevelUpNotify();
                return new PlayersNotify(PlayersNotifyCategory.PlayerReturn, (int)PlayerActivityNotificationType.BeforeLevelUp);
            }

            return new PlayersNotify(PlayersNotifyStatus.NoAvailableNotify);
        }

        [NonAction]
        public async Task<PlayersNotify> CheckPlayerCases(DataRepository InDataRepository, PlayerEntity InPlayerEntity, string InVersion = null)
        {
            //--------------------------------------------
            var PlayerCases = InDataRepository.PlayerCaseEntities.Include(p => p.CaseEntity).Where(p => p.PlayerId == InPlayerEntity.PlayerId);
            var AvalibleCases = await PlayerCases.Where(p => DateTime.UtcNow >= p.NextNotifyDate).ToArrayAsync();
            var TargetPlayerCase = AvalibleCases.FirstOrDefault(p => p.CaseEntity.IsEveryDayCase && p.IsAllowUseByTime);

            //--------------------------------------------
            if (TargetPlayerCase?.CaseEntity?.NextNotifyDelay != null)
            {
                TargetPlayerCase.NextNotifyDate = DateTime.UtcNow.AddNightSafeTime(TargetPlayerCase.CaseEntity.NextNotifyDelay.Value);
            }
            else if (TargetPlayerCase != null)
            {
                TargetPlayerCase.NextNotifyDate = DateTime.UtcNow.AddNightSafeTime(TimeSpan.FromHours(3));
            }

            //--------------------------------------------
            LoggerContainer.AccountLogger.Warn($"[GetAvalibleCaseIds][{InPlayerEntity.PlayerName}][PlayerCases: {PlayerCases.Count()} |AvalibleCases: {AvalibleCases.Length}][TargetPlayerCase: {TargetPlayerCase?.EntityId} / {TargetPlayerCase?.CaseId}]");
            
            //===================================================
            if (TargetPlayerCase == null)
            {
                return new PlayersNotify(PlayersNotifyStatus.NoAvailableNotify);
            }
            //===================================================
            InPlayerEntity.Notifications.UpdateLastNotifyDate();
            return new PlayersNotify(PlayersNotifyCategory.PlayerCases, TargetPlayerCase.CaseId);
        }
    }
}
