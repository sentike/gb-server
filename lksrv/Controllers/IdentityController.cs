﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;


using Loka.Common.Cluster;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;

using Loka.Common.Player.Instance;
using Loka.Common.Store.Abstract;

using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Controllers.IdentityController.Operation;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Player;
using Loka.Server.Player.Infrastructure;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using Loka.Common.Player.Statistic;
using Loka.Common.Store;
using Loka.Common.Tutorial;
using Loka.Server.Models;

using Loka.Server.Session;
using Microsoft.AspNet.Identity.Owin;
using Ninject;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using Newtonsoft.Json;
using SteamSharp;
using VRS.Infrastructure;
using VRS.Infrastructure.Environment;
using Loka.Common.Chat.Chanel;
using Loka.Common.Payments;
using Loka.Common.Player;
using Loka.Common.Player.Profile.Slot;
using Loka.Server.Models.Arsenal;
using VRS.Infrastructure.IpAddress;

namespace Loka.Server.Controllers.IdentityController
{
    public class FAttachAccountService
    {
        public EAccountService Service { get; set; }
        public string Token { get; set; }
        public string Login { get; set; }

        public FAttachAccountService() { }

        public FAttachAccountService(FAttachAccountService InService)
        {
            Service = InService.Service;
            Token = InService.Token;
            Login = InService.Login;
        }

        public override string ToString()
        {
            return $"[{Service}][Login: {Login}][Token: {Token}]";
        }
    }

    public enum EReAttachAccountServicePolicy
    {
        None,

        // Использовать текущий
        UseCurrent,

        // Использовать предыдущий
        UsePrevious
    }


    public class FReAttachAccountService : FAttachAccountService
    {
        public EReAttachAccountServicePolicy Policy { get; set; }

        public override string ToString()
        {
            return $"[{Service}][Login: {Login}][Token: {Token}][Policy: {Policy}]";
        }
    }

    public class FAttachAccountServiceExist : FAttachAccountService
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public long Money { get; set; }
        public long Donate { get; set; }

        public long CreatedDate { get; set; }
        public long LastActivityDate { get; set; }

        public FAttachAccountServiceExist()
        {

        }

        public FAttachAccountServiceExist(PlayerEntity InEntity, FAttachAccountService InService)
            : base(InService)
        {
            Name = InEntity.PlayerName;
            Level = InEntity.Experience.Level;
            Money = InEntity.CashMoney?.Amount ?? -1;
            Donate = InEntity.CashDonate?.Amount ?? -1;
            CreatedDate = InEntity.RegistrationDate.ToUnixTime();
            LastActivityDate = InEntity.LastActivityDate.ToUnixTime();
        }

    }

    public class FAttachAccountServiceExistSuccessfully 
    {
        public string Token { get; set; }
        public string Account { get; set; }

        public FAttachAccountServiceExistSuccessfully()
        {

        }

        public FAttachAccountServiceExistSuccessfully(string InToken, string InAccount)
        {
            Token = InToken;
            Account = InAccount;
        }

    }


    public partial class IdentityController : AbstractApiController
    {
        private async Task<IHttpActionResult> OnAttachAccountServiceProxy(FAttachAccountService InAccountService)
        {
            if (InAccountService.Service != EAccountService.Google)
            {
                throw new NotImplementedException();

            }

            var account = await User.Db.PlayerAccountServiceEntities.FirstOrDefaultAsync(p => p.Service == InAccountService.Service && p.Locked == false && (p.Login == InAccountService.Login || p.Token == InAccountService.Token));

            LoggerContainer.AccountLogger.Info($"[OnAttachAccountService][0]{InAccountService} [account: {account?.PlayerEntity?.PlayerName}]");
            if (account == null)
            {
                //========================================
                if (string.IsNullOrWhiteSpace(PlayerAccountEntity.PlayerEmail))
                {
                    //========================================
                    var PlayerEntityByEmail = await User.Db.AccountPlayerEntity.FirstOrDefaultAsync(p => p.PlayerEmail == InAccountService.Login && p.Locked == false && p.PlayerId != UserId);

                    //========================================
                    if (PlayerEntityByEmail != null)
                    {
                        LoggerContainer.AccountLogger.Info($"[OnAttachAccountService][1]{InAccountService}[owned: {PlayerEntityByEmail.PlayerName} / Level: {PlayerEntityByEmail.Experience.Level} | PlayerId: {PlayerEntityByEmail.PlayerId}]");
                        return Content(HttpStatusCode.BadRequest, new FAttachAccountServiceExist(PlayerEntityByEmail, InAccountService));
                    }

                    //========================================
                    PlayerAccountEntity.PlayerEmail = InAccountService.Login;
                }

                //========================================
                User.Db.PlayerAccountServiceEntities.Add(new PlayerAccountServiceEntity
                {
                    EntityId = Guid.NewGuid(),
                    PlayerId = UserId,
                    Login = InAccountService.Login,
                    Token = InAccountService.Token,
                    Service = InAccountService.Service,
                    CreatedDate = DateTime.UtcNow,
                    Locked = false,
                });

                //========================================
                await User.Db.SaveChangesAsync();

                //========================================
                return Ok(new FReqestOneParam<EAccountService>(InAccountService.Service));
            }
            else if (account.PlayerId != UserId)
            {
                LoggerContainer.AccountLogger.Info($"[OnAttachAccountService][2]{InAccountService}[owned: {account.PlayerEntity.PlayerName} / Level: {account.PlayerEntity?.Experience.Level} | PlayerId: {account.PlayerEntity.PlayerId}]");
                return Content(HttpStatusCode.BadRequest, new FAttachAccountServiceExist(account.PlayerEntity, InAccountService));
            }
            return Ok(new FReqestOneParam<EAccountService>(InAccountService.Service));
        }

        [Authorize, HttpPost]
        public async Task<IHttpActionResult> OnAttachAccountService(FAttachAccountService InAccountService)
        {
            //=========================
            try
            {
                return await OnAttachAccountServiceProxy(InAccountService);
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[OnAttachAccountService]{InAccountService}", e);
                throw;
            }
        }

        private async Task<IHttpActionResult> OnReAttachAccountServiceProxy(FReAttachAccountService InAccountService)
        {
            if (InAccountService.Service != EAccountService.Google)
            {
                throw new NotImplementedException();

            }

            //=========================
            var previousAccount = await User.Db.PlayerAccountServiceEntities.FirstOrDefaultAsync(p => p.Service == InAccountService.Service && p.Locked == false && (p.Login == InAccountService.Login || p.Token == InAccountService.Token));
            var previousPlayerEntity = previousAccount?.PlayerEntity;

            LoggerContainer.AccountLogger.Info($"[OnReAttachAccountServiceProxy]{InAccountService} | previousAccount: {previousAccount?.EntityId} | previousPlayerEntity: {previousPlayerEntity?.PlayerName} / {previousPlayerEntity != null}");

            if (previousPlayerEntity == null)
            {
                previousPlayerEntity = await User.Db.AccountPlayerEntity.FirstOrDefaultAsync(p =>
                    p.PlayerEmail == InAccountService.Login && p.Locked == false && p.PlayerId != UserId);
            }


            //=========================
            if (InAccountService.Policy == EReAttachAccountServicePolicy.UseCurrent)
            {
                //=========================
                //  Блокируем аккаунт и освобождаем емаил и имя игрока

                //================================
                await User.Db.SaveChangesAsync();

                //=========================
                previousPlayerEntity.LockAccount();

                //=========================
                await User.Db.SaveChangesAsync();

                //=========================
                return await OnAttachAccountService(InAccountService);
            }
            else if (InAccountService.Policy == EReAttachAccountServicePolicy.UsePrevious)
            {
                //=========================
                var response = TrySessionStart(DataRepository, previousPlayerEntity.PlayerId);

                //=========================
                if (response.Status == LoginFromRemoteStatus.SessionExist)
                {
                    return StatusCode(HttpStatusCode.Forbidden);    
                }

                //=========================
                var token = new FAttachAccountServiceExistSuccessfully(response.AccountId.ToString(), previousPlayerEntity.GooglePlayPlayerId);

                //=========================
                if (previousPlayerEntity.Locked == false && PlayerAccountEntity.Locked)
                {
                    return Content(HttpStatusCode.Conflict, token);
                }

                //================================
                //  Блокируем текущий аккаунт и освобождаем емаил и имя игрока
                PlayerAccountEntity.LockAccount();

                //================================
                await User.Db.SaveChangesAsync();

                //  Разблокируем старый аккаунт
                previousPlayerEntity.UnLockAccount();

                //================================
                await User.Db.SaveChangesAsync();

                //================================  
                return Content(HttpStatusCode.Conflict, token);
            }
            return NotFound();
        }


        [Authorize, HttpPost]
        public async Task<IHttpActionResult> OnReAttachAccountService(FReAttachAccountService InAccountService)
        {
            //=========================
            LoggerContainer.AccountLogger.Info($"[OnReAttachAccountService]{InAccountService}");
            try
            {
                return await OnReAttachAccountServiceProxy(InAccountService);
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[OnReAttachAccountService]{InAccountService}", e);
                throw;
            }
        }


        [Authorize, HttpGet]
        public IHttpActionResult RequestLootData()
        {
            //========================================================================
            LoggerContainer.AccountLogger.Info($"[RequestLootData][{PlayerAccountEntity.PlayerName}]");

            //========================================================================
            var global = User.Db.ChatChanelEntities.SingleOrDefault(p => p.ChanelGroup == ChatChanelGroup.Global)
                         ?? User.Db.ChatChanelEntities.Add(new ChatChanelEntity
                         {
                             ChanelId = Guid.NewGuid(),
                             ChanelGroup = ChatChanelGroup.Global,
                         });

            var member = User.Db.ChatChanelMemberEntities.FirstOrDefault(p => p.PlayerId == UserId && p.ChanelId == global.ChanelId)
                         ?? User.Db.ChatChanelMemberEntities.Add(new ChatChanelMemberEntity
                         {
                             MemberId = Guid.NewGuid(),
                             PlayerId = UserId,
                             ChanelId = global.ChanelId,
                         });

            member.LastActivityDate = DateTime.UtcNow.AddHours(-6);
            User.Db.SaveChanges();

            //========================================================================
            try
            {
                PlayerAccountEntity.AddDefaultDiscountCoupones();
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}][Failed AddDefaultDiscountCoupones]", e);
            }

            //========================================================================
            try
            {
                PlayerAccountEntity.AddDefaultInventoryItem(1, PlayerProfileInstanceSlotId.Costume);
                LoggerContainer.AccountLogger.Info($"[RequestLootData][{PlayerAccountEntity.PlayerName}] > added Costume");
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}][Failed AddDefaultInventoryItem : Costume]", e);
            }

            //========================================================================
            try
            {
                PlayerAccountEntity.AddDefaultInventoryItem(101, PlayerProfileInstanceSlotId.Pistol);
                LoggerContainer.AccountLogger.Info($"[RequestLootData][{PlayerAccountEntity.PlayerName}] > added Pistol");
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}][Failed AddDefaultInventoryItem : Pistol]", e);
            }

            //========================================================================
            try
            {
                PlayerAccountEntity.AddDefaultInventoryItem(701, PlayerProfileInstanceSlotId.AidKit, 5);
                LoggerContainer.AccountLogger.Info($"[RequestLootData][{PlayerAccountEntity.PlayerName}] > added Aid");
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}][Failed AddDefaultInventoryItem : Aid]", e);
            }

            //========================================================================
            try
            {
                PlayerAccountEntity.AddDefaultInventoryItem(702, PlayerProfileInstanceSlotId.Grenade, 5);
                LoggerContainer.AccountLogger.Info($"[RequestLootData][{PlayerAccountEntity.PlayerName}] > added Grenade");
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}][Failed AddDefaultInventoryItem : Grenade]", e);
            }


            //========================================================================
            try
            {
                var rifle = PlayerAccountEntity.AddDefaultInventoryItem(201, PlayerProfileInstanceSlotId.Rifle);
                if (PlayerAccountEntity.DefaultItemId == null || PlayerAccountEntity.DefaultItemId == Guid.Empty)
                {
                    PlayerAccountEntity.DefaultItemId = rifle?.ProfileItemId;
                }

                LoggerContainer.AccountLogger.Info($"[RequestLootData][{PlayerAccountEntity.PlayerName}] > added Rifle");
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}][Failed AddDefaultInventoryItem : Rifle]", e);
            }

            //========================================================================
            try
            {
                PlayerAccountEntity.AddDefaultInventoryItem(501, PlayerProfileInstanceSlotId.Knife);
                LoggerContainer.AccountLogger.Info($"[RequestLootData][{PlayerAccountEntity.PlayerName}] > added Knife");
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}][Failed AddDefaultInventoryItem : Knife]", e);
            }

            //========================================================================
            try
            {
                DataRepository.SaveChanges();
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}][Failed SaveChanges]", e);
            }

            //========================================================================
            try
            {
                if(PlayerAccountEntity.AddDefaultPlayerCases())
                {
                    DataRepository.SaveChanges();
                }
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}][Failed AddDefaultPlayerCases]", e);
            }


            //========================================================================
            try
            {
                var response = PlayerAccountEntity.InventoryItemList
                    .Select(p => new PlayerInventoryItemModel(p))
                    .ToArray();
                return Json(response);
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}] [Response][1]", e);
                throw;
            }
        }

        [Authorize, HttpPost]
        public IHttpActionResult OnChangePlayerName(FReqestOneParam<string> InName)
        {
            //========================================================================
            var SafeName = InName?.Value?.Trim();
            if (string.IsNullOrWhiteSpace(SafeName))
            {
                return NotFound();
            }

            //========================================================================
            var NameNormilized = SafeName.ToLowerInvariant();

            //========================================================================
            if (DataRepository.AccountPlayerEntity.AsNoTracking().Any(p => p.PlayerNameNormilized == NameNormilized && p.PlayerId != UserId))
            {
                return Content(HttpStatusCode.Conflict, InName);
            }

            //========================================================================
            if (PlayerAccountEntity.PlayerNameNormilized == NameNormilized)
            {
                PlayerAccountEntity.Experience.IsNameConfirmed = true;
                DataRepository.SaveChanges();
                return Content(HttpStatusCode.OK, InName);
            }

            //========================================================================
            if (PlayerAccountEntity.Experience.ChangeNameTickets >= 1)
            {
                PlayerAccountEntity.Experience.ChangeNameTickets--;
            }
            else
            {
                var ChangeNameTicketCost = new StoreItemCost(50, GameCurrency.Donate);
                if (PlayerAccountEntity.Pay(ChangeNameTicketCost) == false)
                {
                    return Content(HttpStatusCode.PaymentRequired, InName);
                }
            }

            //========================================================================
            PlayerAccountEntity.PlayerName = SafeName;
            PlayerAccountEntity.PlayerNameNormilized = NameNormilized;
            PlayerAccountEntity.Experience.IsNameConfirmed = true;

            //========================================================================
            DataRepository.SaveChanges();

            //========================================================================
            return Content(HttpStatusCode.OK, InName);
        }

        [Authorize, HttpPost]
        public IHttpActionResult OnChangeTargetServerVersion(FReqestOneParam<long> InTargetVersionId)
        {
            //========================================================================
            if (InTargetVersionId == null || InTargetVersionId.Value < 0)
            {
                return BadRequest();
            }

            //========================================================================
            PlayerAccountEntity.Experience.TargetServerVersion = InTargetVersionId;

            //========================================================================
            return Ok();
        }


        [Authorize, HttpGet]
        public IHttpActionResult OnRequestIsoCountry()
        {
            try
            {
                Logger.Warn($"[OnRequestIsoCountry][{UserName}][Country: {PlayerAccountEntity.Country}] -> [{UserCountry}]");
                PlayerAccountEntity.Country = UserCountry;
                return Json(new FReqestOneParam<EIsoCountry>(PlayerAccountEntity.Country));
            }
            catch(Exception e)
            {
                Logger.WriteExceptionMessage($"[OnRequestIsoCountry][{UserName}][Country: {PlayerAccountEntity.Country}]", e);
                throw e;
            }
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult OnRequestIsoCountryTest()
        {
            return Json(new FReqestOneParam<EIsoCountry>(UserCountry));
        }


        [Authorize, HttpGet]
        public IHttpActionResult OnRequestClientData()
        {
            if (PlayerAccountEntity.RatingPosition <= 0)
            {
                PlayerAccountEntity.RatingPosition = PlayerEntity.LastRatingPosition;
                DataRepository.SaveChanges();
            }

            //==================================================
            try
            {
                PlayerAccountEntity.SafeEveryDayAwardEntity.UpdateOrGiveEveryDayBonus(DataRepository);
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[OnRequestClientData][{UserName}][UpdateOrGiveEveryDayBonus]", e);
            }

            //==================================================
            //  Пытаемся обновить бонусные матчи, если уже прошло достаточно времени
            PlayerAccountEntity.Experience.ResetPrimeMatches();

            //==================================================
            //  Обновляет уровень игрока
            var UpdatedLevels = PlayerAccountEntity.Experience.Calculate();
            if (UpdatedLevels.Any())
            {
                //-----------------------------------------------------
                PlayerAccountEntity.Notifications.AddAfterLevelUpNotify(true);

                //-----------------------------------------------------
                var UpdatedLevelEntyties = DataRepository.PlayerLevelAwardEntities.Where(p => UpdatedLevels.Contains(p.EntityId)).ToArray();
                foreach (var level in UpdatedLevelEntyties)
                {
                    //----------------------------------------
                    PlayerAccountEntity.Give(GameCurrency.Money, level.Money);
                    PlayerAccountEntity.Give(GameCurrency.Donate, level.Donate);

                    //----------------------------------------
                    foreach (var reward in level.ItemEntities)
                    {
                        PlayerInventoryItemEntity tmp = null;
                        reward.ModelEntity.Give(PlayerAccountEntity, reward.Amount, out tmp);
                    }
                }
            }

            //==================================================
            if(PlayerAccountEntity.Experience.BeforeLevelUp)
            {
                PlayerAccountEntity.Notifications.AddBeforeLevelUpNotify();
            }

            //==================================================
            //  Собираем статистику по входам в игру
            if (User.PlayerGameSessionEntity.SessionAttach == PlayerSessionAttach.Client)
            {
                PlayerAccountEntity.LastSessionId = User.PlayerGameSessionEntity.SessionId;
            }

            //==================================================
            return Json(new RequestClientData.Response(PlayerAccountEntity));
        }

        [Authorize, HttpPost]
        public IHttpActionResult OnDump(FReqestOneParam<string> InData)
        {
            LoggerContainer.AccountLogger.Error($"[OnDump][{PlayerAccountEntity.PlayerName} / {UserId}]Data: {InData}");
            return Ok();
        }

        [Authorize, HttpGet]
        public IHttpActionResult OnRequestUpdateClientData()
        {
            //==================================================
            var response = OnRequestClientData();

            //==================================================
            try
            {
                PlayerAccountEntity.Experience.IsLevelUpNotified = true;
                PlayerAccountEntity.SafeEveryDayAwardEntity.State = PlayerEveryDayAwardState.None;
                DataRepository.SaveChanges();
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[OnRequestUpdateClientData][{UserName}]", e);
            }

            //==================================================
            return response;
        }

        //=====================================================================================================

        [Inject]
        public PlayerRepository PlayerRepository { get; set; }
    }
}
