﻿//using System;
//using System.Web.Http;
//using Loka.Infrastructure;
//using Ninject;
//using System.Linq;
//using System.Web;
//using AutoMapper;
//using Loka.Common.Player.Profile.Slot;
//using Loka.Common.Store;
//using Loka.Common.Store.Abstract;
//

//
//using Loka.Server.Infrastructure;
//using Loka.Server.Infrastructure.DataRepository;
//using Loka.Server.Player.Infrastructure;
//using Loka.Server.Player.Models;
//using VRS.Infrastructure;
//using WebApi.OutputCache.V2;

//namespace Loka.Server.Controllers
//{
//    public struct LoadItemRequest
//    {
//        public short ModelId { get; set; }
//        public CategoryTypeId CategoryTypeId { get; set; }
//    }

//    public struct EditorItemProperty
//    {
//        private static readonly IMapper Mapper;

//        static EditorItemProperty()
//        {
//            Mapper = new MapperConfiguration(c =>
//            {
//                //==========================================================================
//                c.CreateMap<ItemInstanceEntity, EditorItemProperty>();
//            }).CreateMapper();
//        }

//        public static EditorItemProperty From(ItemInstanceEntity entity)
//        {
//            return Mapper.Map<EditorItemProperty>(entity);
//        }

//        public short ModelId { get; set; }
//        public CategoryTypeId CategoryTypeId { get; set; }
//        public CharacterModelFlagId CharacterModelsFlag { get; set; }
//        public ItemInstanceFlags Flags { get; set; }
//        public int Level { get; set; }
//        public ItemSkinTypeId DefaultSkinId { get; set; }
//        public FractionTypeId FractionId { get; set; }
//        public short DefaultAmount { get; set; }
//        public StoreItemCost Cost { get; set; }
//        public StoreItemCost SubscribeCost { get; set; }
//        public PlayerProfileTypeId TargetProfileId { get; set; }
//        public ItemMigrationAction MigrationAction { get; set; }
//        public long Duration { get; set; }
//    }

//    [AllowAnonymous]
//    public class EditorController : ApiController
//    {
//        [HttpPost]
//        public IHttpActionResult LoadItem(LoadItemRequest model)
//        {
//            using (var db = new DataRepository())
//            {
//                //===============================================
//                ItemInstanceEntity item = db.StoreItemInstance.SingleOrDefault(p => p.CategoryTypeId == model.CategoryTypeId && p.ModelId == model.ModelId);
//                if (item == null)
//                {
//                    return NotFound();
//                }

//                //===============================================
//                return Json(EditorItemProperty.From(item));
//            }
//        }

//        [HttpPost]
//        public IHttpActionResult UpdateItem(EditorItemProperty property)
//        {
//            using (var db = new DataRepository())
//            {
//                //===============================================
//                ItemInstanceEntity item = db.StoreItemInstance.SingleOrDefault(p => p.CategoryTypeId == property.CategoryTypeId && p.ModelId == property.ModelId);
//                if (item == null)
//                {
//                    return NotFound();
//                }

//                //===============================================
//                LoggerContainer.StoreLogger.Warn($"UpdateItem[{property.ModelId} -- {property.CategoryTypeId}][Cost: {property.Cost.Amount} - {property.Cost.Currency}]");

//                //===============================================
//                if (property.Cost == null)
//                {
//                    throw new ArgumentNullException(nameof(property.Cost));
//                }

//                //===============================================
//                if (property.SubscribeCost == null)
//                {
//                    throw new ArgumentNullException(nameof(property.SubscribeCost));
//                }

//                item.Cost = property.Cost;
//                item.SubscribeCost = property.SubscribeCost;
//                item.DefaultAmount = property.DefaultAmount;


//                item.FractionId = property.FractionId;
//                item.Level = property.Level;

//                db.SaveChanges();
//                //===============================================
//                return Json(EditorItemProperty.From(item));
//            }
//        }
//    }
//}