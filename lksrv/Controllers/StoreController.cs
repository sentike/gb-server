﻿using System.Collections.Generic;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using System.Web.Http;
using System.Linq;
using Loka.Common.Payments;
using Loka.Common.Store.Abstract;
using Loka.Server.Service;

namespace Loka.Server.Controllers.StoreController
{
    public partial class StoreController : AbstractApiController
    {
        public class ListOfInstanceResponse
        {
            public ItemBaseContainer[] Items { get; set; }
            public CaseContainer[] Cases { get; set; }
            public ProductContainer[] Products { get; set; }
            public FLevelAwardContainer[] Level { get; set; }
            public FEveryDayAwardContainer[] EveryDay { get; set; }
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult ListOfInstance()
        {
            using (var db = new DataRepository())
            {
                var items = db.StoreItemInstance.ToArray().Select(p => new ItemBaseContainer(p)).ToArray();
                return Json(items);
            }
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult ListOfCosts()
        {
            GooglePlayPayments.UpdateMarketCosts();
            return Json(GooglePlayPayments.GetProductCosts());
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult ListOfInstanceV2()
        {
            using (var db = new DataRepository())
            {
                var cases = db.CaseEntities.ToArray().Select(p => new CaseContainer(p)).ToArray();
                var items = db.StoreItemInstance.ToArray().Select(p => new ItemBaseContainer(p)).ToArray();
                var products = db.MarketProductEntities.ToArray().Select(p => new ProductContainer(p)).ToArray();
                var level = db.PlayerLevelAwardEntities.ToArray().Select(p => new FLevelAwardContainer(p)).ToArray();
                var days = db.EveryDayAwardEntities.ToArray().Select(p => new FEveryDayAwardContainer(p)).ToArray();

                var response = new ListOfInstanceResponse
                {
                    Items = items,
                    Cases = cases,
                    Products = products,
                    EveryDay = days,
                    Level = level,
                };

                return Json(response);
            }
        }
    }
}