﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Infrastructure;
using Loka.Infrastructure;
using Loka.Server.Player.Models;

namespace Loka.Common.Cases
{
    public class PlayerCaseEntity
    {
        public static int LootBoxWinne {get;} = 5;
        public static int LootBoxEveryDay { get; } = 6;
        public static int TrohyCaseEveryDay { get; } = 7;
        public static int AdsFreeCase { get; } = 24;
        public static int FirstEveryDayCase { get; } = 8;

        public Guid EntityId { get; set; }

        public int CaseId { get; set; }
        public virtual CaseEntity CaseEntity { get; set; }

        //public bool IsEveryDayCase { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public DateTime? NextNotifyDate { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime LastActiveDate { get; set; }


        public bool IsAllowUseByTime => LastActiveDate.IsElapsed(CaseEntity?.Delay, true);

        public bool IsAllowUse => LastActiveDate.IsElapsed(CaseEntity?.Delay, true) && HasAnyCases;
        public bool HasAnyCases => Amount > 0;

        public int Amount { get; set; }
        public int LastAmount { get; set; }
        public long LastAmountHash { get; set; }

        public void GiveAmount(int val)
        {
            LastAmount = Amount;
            LastAmountHash = Amount.Crc64Hash();
            Amount += val;
        }

        public void SetAmount(int val)
        {
            LastAmount = Amount;
            LastAmountHash = Amount.Crc64Hash();
            Amount = val;
        }

        public static PlayerCaseEntity Factory(PlayerEntity player, int InEntityId, int amount = 1)
        {
            //==================================
            DateTime? NextNotifyDate = null;

            //==================================
            var Entity = CaseEntity.GetCaseEntity(InEntityId);

            //==================================
            if(Entity?.FirstNotifyDelay != null && (Entity?.IsEveryDayCase ?? false))
            {
                NextNotifyDate = DateTime.UtcNow.AddNightSafeTime(Entity.FirstNotifyDelay.Value);
            }

            //==================================
            return new PlayerCaseEntity
            {
                EntityId = Guid.NewGuid(),
                CaseId = InEntityId,
                PlayerId = player.PlayerId,
                PlayerEntity = player,
                CreatedDate = DateTime.UtcNow,
                LastActiveDate = DateTime.UtcNow.AddDays(-2),
                Amount = amount,
                LastAmount = amount,
                LastAmountHash = amount.Crc64Hash(),
                NextNotifyDate = NextNotifyDate
            };
        }

        //================================================
        public class Configuration : EntityTypeConfiguration<PlayerCaseEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                HasRequired(p => p.CaseEntity).WithMany().HasForeignKey(p => p.CaseId).WillCascadeOnDelete(true);
                HasRequired(p => p.PlayerEntity).WithMany(p => p.CaseEntities).HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(true);

                ToTable("players.PlayerCaseEntity");
            }
        }

        public void Use()
        {
            GiveAmount(-1);
        }
    }
}
