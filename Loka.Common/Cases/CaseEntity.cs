﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Player.Models;

namespace Loka.Common.Cases
{
    public class CaseEntity
    {
        //-------------------------------------------
        public const int FreeCaseId = 1;

        //-------------------------------------------
        /// <summary>
        /// Not Allow to use for Entity reference
        /// </summary>
        /// <param name="InEntityId">Case instance ID</param>
        /// <returns>Tempory CaseEntity</returns>
        public static CaseEntity GetCaseEntity(int InEntityId)
        {
            using (var db = new DataRepository())
            {
                return db.CaseEntities.SingleOrDefault(p => p.EntityId == InEntityId);
            }           
        }

        //-------------------------------------------
        public int EntityId { get; set; }
        public TimeSpan? Delay { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
        public string AdminComment { get; set; }
        
        public short? RandomMaxDropItems { get; set; } = 5;

        public virtual List<CaseItemEntity> ItemEntities { get; set; } = new List<CaseItemEntity>(4);


        public bool IsEveryDayCase => Delay != null;

        //-------------------------------------------
        public TimeSpan? FirstNotifyDelay { get; set; }
        public TimeSpan? NextNotifyDelay { get; set; }
        public bool IsAllowGetBonusCase { get; set; }


        //-------------------------------------------
        public CaseDropItem[] GiveAwardToPlayer(PlayerEntity player)
        {
            //===============================
            var rnd = new Random();

            //===============================
            var entities = TakeRandomEntities();

            //===============================
            CaseDropItem[] items = entities.Select(p => p.TakeItem(rnd)).Where(p => p.Amount > 0).ToArray();

            //===============================
            foreach (var item in items)
            {
                item.ItemModelEntity.Give(player, item.Amount, out _, false);
            }

            //===============================
            return items;
        }

        public CaseItemEntity[] TakeRandomEntities()
        {
            //===============================
            var rnd = new Random();

            //===============================
            var shuffle = ItemEntities.Shuffle().ToArray();
            var guaranteed = shuffle.Where(p => p.DropMethod == CaseItemDropMethod.Guaranteed).ToArray();

            //===============================
            int max = Math.Min(RandomMaxDropItems ?? shuffle.Length, shuffle.Length) - guaranteed.Length;

            //===============================
            return (from item in shuffle
                let random = rnd.Next(0, 10000000)
                where item.DropMethod == CaseItemDropMethod.Random && item.Chance <= random
                select item).ToArray().PickRandom(max).Concat(guaranteed).ToArray();
        }

        //================================================
        public class Configuration : EntityTypeConfiguration<CaseEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                HasMany(p => p.ItemEntities).WithRequired(p => p.CaseEntity).HasForeignKey(p => p.CaseId).WillCascadeOnDelete(true);
                Property(p => p.AdminComment).HasMaxLength(128);
                ToTable("store.CaseEntity");
            }
        }
    }
}
