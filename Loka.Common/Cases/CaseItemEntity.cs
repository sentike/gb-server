﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Abstract;

namespace Loka.Common.Cases
{
    public enum CaseItemDropMethod : short
    {
        None,
        Random,
        Guaranteed
    }

    public class CaseDropItemModel
    {
        public int ItemModelId { get; set; }

        public int Amount { get; set; }
        public CaseDropItemModel() { }
        public CaseDropItemModel(int model, int amount)
        {
            ItemModelId = model;
            Amount = amount;
        }
    }

    public class CaseDropItem : CaseDropItemModel
    {
        public ItemInstanceEntity ItemModelEntity { get; set; }
    }



    public class CaseItemEntity
    {
        public int EntityId { get; set; }

        public int CaseId { get; set; }
        public virtual CaseEntity CaseEntity { get; set; }

        public int ItemModelId { get; set; }
        public virtual ItemInstanceEntity ItemModelEntity { get; set; }

        public short RandomMin { get; set; } = 1;
        public short RandomMax { get; set; } = 10;

        public CaseItemDropMethod DropMethod { get; set; }

        //  Шанс выпадениея на 10000000
        public short Chance { get; set; } = 1000;

        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
        public string AdminComment { get; set; }

        public CaseDropItem TakeItem()
        {            
            //===============================
            var rnd = new Random();

            //===============================
            return TakeItem(rnd);
        }

        public CaseDropItem TakeItem(Random random)
        {
            //===============================
            return new CaseDropItem
            {
                ItemModelId = ItemModelId,
                ItemModelEntity = ItemModelEntity,
                Amount = random.Next(RandomMin, RandomMax)
            };
        }

        //================================================
        public class Configuration : EntityTypeConfiguration<CaseItemEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("store.CaseItemEntity");
                Property(p => p.AdminComment).HasMaxLength(128);
                HasRequired(p => p.ItemModelEntity).WithMany().HasForeignKey(p => p.ItemModelId).WillCascadeOnDelete(true);
            }
        }
    }
}
