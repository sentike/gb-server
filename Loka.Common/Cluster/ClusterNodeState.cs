﻿namespace Loka.Common.Cluster
{
    public enum NodeInstanceState
    {
        /// <summary>
        /// An instance is created, but does not have a state
        /// </summary>
        NotStarted,

        /// <summary>
        /// A copy has been stopped due to Crash
        /// </summary>
        StopedCrash,

        /// <summary>
        /// A copy has been stopped due to content update
        /// </summary>
        StopedUpdate,

        /// <summary>
        /// Instance has been stopped due to the end of the work. (Signal from the application)
        /// </summary>
        StopedExit,

        /// <summary>
        /// It is expected to register an instance of the local computer
        /// </summary>
        WaitingRegistrationOnCluster,

        /// <summary>
        /// Expected instance registration on the master server
        /// </summary>
        WaitingRegistrationOnMaster,

        /// <summary>
        /// instance in idle, waiting for the start of the game
        /// </summary>
        WaitingStartGame,

        /// <summary>
        /// instance in expectation end of the game
        /// </summary>
        WaitingEndGame,


        /// <summary>
        /// instance in expectation the beginning of the game session. 
        /// The game is created, but the session has not yet started
        /// </summary>
        PrepareGame,
    }
}