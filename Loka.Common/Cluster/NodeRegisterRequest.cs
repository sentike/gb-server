﻿using System;

namespace Loka.Server.ClusterManager.Infrastructures
{
    public sealed class NodeRegisterRequest
    {
        public Guid NodeId { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Address => $"{Host}:{Port}";
    };
}