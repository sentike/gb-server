﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Discount;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using VRS.Infrastructure.Environment;

namespace Loka.Common.Payments
{
    public enum MarketProductType
    {
        None,
        Money,
        Donate,
        Booster,
    }

    public enum IsoCurrency : short
    {
        None,

        /// <summary>
        /// Рубль
        /// </summary>
        RUB = 6,

        /// <summary>
        /// Доллар
        /// </summary>
        USD,

        /// <summary>
        /// Евро
        /// </summary>
        EUR,

        /// <summary>
        /// Гривна
        /// </summary>
        UAH = 9,

        /// <summary>
        /// Белорусский рубль
        /// </summary>
        BRL,

        /// <summary>
        /// Казахстанский тенге
        /// </summary>
        KZT,
        End,
    }

    public class MarketProductCostEntity
    {
        public MarketProductCostEntity() { }
        public MarketProductCostEntity(short productId, EIsoCountry country, IsoCurrency currency, int amount, string sku, EProductDiscountCoupone InDiscount)
        {
            ProductId = productId;
            Country = country;
            Currency = currency;
            Amount = amount;
            AdminComment = sku;
            Discount = InDiscount;
        }

        public short EntityId { get; set; }
        public short ProductId { get; set; }
        public virtual MarketProductEntity ProductEntity { get; set; }
        public EProductDiscountCoupone Discount { get; set; }
        public EIsoCountry Country { get; set; }
        public IsoCurrency Currency { get; set; }

        /// <summary>
        /// Currency in Cents
        /// </summary>
        public int Amount { get; set; }
        public string AdminComment { get; set; }

        public override string ToString()
        {
            return $"[MarketProductCost][{EntityId}][ProductId: {ProductId}][{Country}][{Currency}][Amount: {Amount}]";
        }

        public class Configuration : EntityTypeConfiguration<MarketProductCostEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                Property(a => a.EntityId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                ToTable("store.MarketProductCostEntity");

                Property(a => a.AdminComment).HasMaxLength(256);

                Property(p => p.ProductId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ProductId_Country_Discount", 0) { IsUnique = true }));
                Property(p => p.Country).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ProductId_Country_Discount", 1) { IsUnique = true }));
                Property(p => p.Discount).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ProductId_Country_Discount", 2) { IsUnique = true }));
            }
        }
    }

    public class MarketProductEntity
    {
        public short ProductId { get; set; }
        public int? TargetProductId { get; set; }
        public virtual ItemInstanceEntity TargetProductEntity { get; set; }

        public string ProductName { get; set; }
        public MarketProductType ProductType { get; set; }
        public string AdminComment { get; set; }
        public long Amount { get; set; }
        public long Cost { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual List<MarketProductCostEntity> CostEntities { get; set; } = new List<MarketProductCostEntity>();
        public MarketProductCostEntity DefaultCost => CostEntities?.FirstOrDefault(p =>  p.Country == EIsoCountry.RU || p.Currency == IsoCurrency.RUB);

        public override string ToString()
        {
            return $"[MarketProduct][{ProductId}][{ProductName}][{ProductType}][{DefaultCost}][AdminComment: {AdminComment}]";
        }

        public class Configuration : EntityTypeConfiguration<MarketProductEntity>
        {
            public Configuration()
            {
                HasKey(p => p.ProductId);
                ToTable("store.MarketProductEntity");

                Property(a => a.AdminComment).HasMaxLength(256);
                HasMany(p => p.CostEntities).WithRequired(p => p.ProductEntity).HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);
                HasOptional(p => p.TargetProductEntity).WithMany().HasForeignKey(p => p.TargetProductId);

                Property(a => a.ProductId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                Property(p => p.ProductName).HasMaxLength(256).IsRequired().HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("MarketProductEntity_ProductName", 0) { IsUnique = true }));
            }
        }

        public void AddOrUpdatePrice(ProductCostContainer container, string sku, EProductDiscountCoupone InDiscount)
        {
            var Price = CostEntities.SingleOrDefault(p => p.Country == container.Country && p.Discount == InDiscount);
            if (Price == null)
            {
                CostEntities.Add(new MarketProductCostEntity(ProductId, container.Country, container.Currency, container.Amount, sku, InDiscount));
            }
            else
            {
                Price.Currency = container.Currency;
                Price.Amount = container.Amount;
            }
        }
    }

    public class ProductCostContainer
    {
        public static bool Parse(string InCountry, string InCurrency, string InAmount, out ProductCostContainer cost)
        {
            cost = null;

            if
            (
                Enum.TryParse(InCountry, out EIsoCountry Country) &&
                Enum.TryParse(InCurrency, out IsoCurrency Currency) &&
                int.TryParse(InAmount, out int Amount)
            )
            {
                cost = new ProductCostContainer
                {
                    Currency = Currency,
                    Country = Country,
                    Amount = Amount
                };
                return true;
            }

            return false;
        }
        public ProductCostContainer() { }
        public ProductCostContainer(MarketProductCostEntity entity)
        {
            Country = entity.Country;
            Currency = entity.Currency;
            Amount = entity.Amount;
        }

        public EIsoCountry Country { get; set; }
        public IsoCurrency Currency { get; set; }

        /// <summary>
        /// Currency in Cents
        /// </summary>
        public int Amount { get; set; }
    }

    public struct FProductItemCostCollection
    {
        public EProductDiscountCoupone Discount { get; set; }
        public ProductCostContainer[] Costs { get; set; }

        public FProductItemCostCollection(EProductDiscountCoupone discount, MarketProductCostEntity[] costs)
        {
            Discount = discount;
            Costs = costs.Select(p => new ProductCostContainer(p)).ToArray();
        }
    }

    public class ProductContainer
    {
        public string ProductId { get; set; }
        public ProductCostContainer[] Costs { get; set; }
        public FProductItemCostCollection[] CostsV2 { get; set; }

        public ProductContainer()
        {
        }

        public ProductContainer(MarketProductEntity entity)
        {
            ProductId = entity.ProductName;
            Costs = entity.CostEntities.Where(p => p.Discount == EProductDiscountCoupone.None).ToArray().Select(p => new ProductCostContainer(p)).ToArray();
            CostsV2 = entity.CostEntities.GroupBy(p => p.Discount, p => p).Select(p => new FProductItemCostCollection(p.Key, p.ToArray())).ToArray();
        }
    }
}
