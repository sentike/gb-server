﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Player.Models;

namespace Loka.Common.Payments
{
    public class PlayerPaymentEntity
    {
        //================================
        public Guid EntityId { get; set; }

        //================================
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        //================================
        public short ProductId { get; set; } 
        public virtual MarketProductEntity ProductEntity { get; set; }

        //================================
        public string OrderId { get; set; }
        public string TokenId { get; set; }

        //================================
        public DateTime CreatedDate { get; set; }

        public PlayerPaymentEntity() { }
        public PlayerPaymentEntity(short inProductId, string inOrderId, string inTokenId)
        {
            EntityId = Guid.NewGuid();
            ProductId = inProductId;
            OrderId = inOrderId;
            TokenId = inTokenId;
            CreatedDate = DateTime.UtcNow;
        }

        //================================
        public class Configuration : EntityTypeConfiguration<PlayerPaymentEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("players.PlayerPaymentEntity");

                HasRequired(p => p.PlayerEntity).WithMany(p => p.PaymentEntities).HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(true);
                HasRequired(p => p.ProductEntity).WithMany().HasForeignKey(p => p.ProductId);

                Property(p => p.OrderId).HasMaxLength(256).IsRequired().HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerPaymentEntity_OrderId", 0) { IsUnique = true }));
                Property(p => p.TokenId).HasMaxLength(256).IsRequired().HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerPaymentEntity_TokenId", 0) { IsUnique = true }));

            }
        }
    }
}
