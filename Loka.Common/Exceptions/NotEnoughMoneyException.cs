﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Instance;
using Loka.Common.Store;

namespace Loka.Common.Exceptions
{
    public class NotEnoughMoneyException : OperationException
    {
        public PlayerCashEntity Balance { get;private set ;}
        public StoreItemCost Cost { get; private set; }

        public NotEnoughMoneyException(int responseCode, object responseData, string exception, PlayerCashEntity balance, StoreItemCost cost) 
            : base(responseCode, responseData, exception)
        {
            Balance = balance;
            Cost = cost;
        }

        public NotEnoughMoneyException(int responseCode, object responseData, string message, System.Exception exception, PlayerCashEntity balance, StoreItemCost cost) 
            : base(responseCode, responseData, message, exception)
        {
            Balance = balance;
            Cost = cost;
        }

        public NotEnoughMoneyException(int responseCode, PlayerCashEntity balance, StoreItemCost cost) 
            : base(responseCode)
        {
            Balance = balance;
            Cost = cost;
        }

        public NotEnoughMoneyException(int responseCode, string exception, PlayerCashEntity balance, StoreItemCost cost) 
            : base(responseCode, exception)
        {
            Balance = balance;
            Cost = cost;
        }

        public NotEnoughMoneyException(int responseCode, string message, System.Exception exception, PlayerCashEntity balance, StoreItemCost cost) 
            : base(responseCode, message, exception)
        {
            Balance = balance;
            Cost = cost;
        }
    }
}
