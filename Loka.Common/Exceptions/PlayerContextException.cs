﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Player.Models;

namespace Loka.Common.Exceptions
{
    public class PlayerContextException : Exception
    {
        public PlayerEntity PlayerEntity { get; private set; }

        public PlayerContextException(PlayerEntity playerEntity, OperationException exception)
            : base($"Exception on player context {playerEntity.PlayerName} [{playerEntity.PlayerId}] ", exception)
        {
            PlayerEntity = playerEntity;
        }

        public PlayerContextException(PlayerEntity playerEntity, string message, OperationException exception)
            : base($"Exception on player context {playerEntity.PlayerName} [{playerEntity.PlayerId}]", new Exception(message, exception))
        {
            PlayerEntity = playerEntity;
        }

    }
}
