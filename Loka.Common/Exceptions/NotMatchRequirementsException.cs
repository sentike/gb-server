﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Exceptions
{
    public class NotMatchRequirementsException : OperationException
    {
        public NotMatchRequirementsException(int responseCode, object responseData, string exception) : base(responseCode, responseData, exception)
        {
        }

        public NotMatchRequirementsException(int responseCode, object responseData, string message, System.Exception exception) : base(responseCode, responseData, message, exception)
        {
        }

        public NotMatchRequirementsException(int responseCode) : base(responseCode)
        {
        }

        public NotMatchRequirementsException(int responseCode, string exception) : base(responseCode, exception)
        {
        }

        public NotMatchRequirementsException(int responseCode, string message, System.Exception exception) : base(responseCode, message, exception)
        {
        }
    }
}
