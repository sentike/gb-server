﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Exceptions
{
    public enum OperationExceptionResponseCode
    {
        ObjectNotFound,

    }

    public class OperationException : System.Exception
    {
        public OperationException(int responseCode, object responseData, string exception)
            : base(exception)
        {
            ResponseCode = responseCode;
            ResponseData = responseData;
        }

        public OperationException(int responseCode, object responseData, string message, System.Exception exception)
            : base(message, exception)
        {
            ResponseCode = responseCode;
            ResponseData = responseData;
        }

        public OperationException(int responseCode)
        {
            ResponseCode = responseCode;
            ResponseData = null;
        }

        public OperationException(int responseCode, string exception)
            : base(exception)
        {
            ResponseCode = responseCode;
            ResponseData = null;
        }

        public OperationException(int responseCode, string message, System.Exception exception)
            : base(message, exception)
        {
            ResponseCode = responseCode;
            ResponseData = null;
        }

        public int ResponseCode { get; private set; }
        public object ResponseData { get; private set; }
    }
}
