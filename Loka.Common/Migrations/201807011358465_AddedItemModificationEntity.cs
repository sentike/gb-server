namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedItemModificationEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.ItemModificationEntity",
                c => new
                    {
                        EntityId = c.Int(nullable: false, identity: true),
                        ModelId = c.Int(nullable: false),
                        Cost_Amount = c.Int(nullable: false),
                        Cost_Currency = c.Short(nullable: false),
                        Damage = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.AbstractItemInstance", t => t.ModelId, cascadeDelete: true)
                .Index(t => t.ModelId);
            
            DropColumn("store.AbstractItemInstance", "Modification_Cost_Amount");
            DropColumn("store.AbstractItemInstance", "Modification_Cost_Currency");
            DropColumn("store.AbstractItemInstance", "Modification_LastLevelCost_Amount");
            DropColumn("store.AbstractItemInstance", "Modification_LastLevelCost_Currency");
            DropColumn("store.AbstractItemInstance", "Modification_NumberOfLevels");
            DropColumn("store.AbstractItemInstance", "Modification_CostCoefficient");
            DropColumn("store.AbstractItemInstance", "Modification_DamageCoefficient");
        }
        
        public override void Down()
        {
            AddColumn("store.AbstractItemInstance", "Modification_DamageCoefficient", c => c.Single(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_CostCoefficient", c => c.Single(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_NumberOfLevels", c => c.Short(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_LastLevelCost_Currency", c => c.Short(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_LastLevelCost_Amount", c => c.Int(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_Cost_Currency", c => c.Short(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_Cost_Amount", c => c.Int(nullable: false));
            DropForeignKey("store.ItemModificationEntity", "ModelId", "store.AbstractItemInstance");
            DropIndex("store.ItemModificationEntity", new[] { "ModelId" });
            DropTable("store.ItemModificationEntity");
        }
    }
}
