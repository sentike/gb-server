namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedItemModificationPosition : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.ItemModificationEntity", "Position", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.ItemModificationEntity", "Position");
        }
    }
}
