namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTargetWeaponUpgrade : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "TargetWeaponUpgrade", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchMember", "TargetWeaponUpgrade");
        }
    }
}
