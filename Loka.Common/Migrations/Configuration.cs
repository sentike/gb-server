using Loka.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;



namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataRepository>
    {
        public Configuration()
        {
            AutomaticMigrationDataLossAllowed = false; 
            AutomaticMigrationsEnabled = false;
        }
    

        protected override void Seed(DataRepository context)
        {
            //  This method will be called after migrating to the latest version.
    
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
