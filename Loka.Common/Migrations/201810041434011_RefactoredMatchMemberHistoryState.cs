namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoredMatchMemberHistoryState : DbMigration
    {
        public override void Up()
        {
            AddColumn("history.MatchMember", "State", c => c.Short(nullable: false, defaultValue: 4, defaultValueSql: "4"));
            AlterColumn("history.MatchMember", "Squad", c => c.Short(nullable: false));
            DropColumn("history.MatchMember", "HasUsedPremiumAccount");
            DropColumn("history.MatchMember", "HasNotified");
        }
        
        public override void Down()
        {
            AddColumn("history.MatchMember", "HasNotified", c => c.Boolean(nullable: false));
            AddColumn("history.MatchMember", "HasUsedPremiumAccount", c => c.Boolean(nullable: false));
            AlterColumn("history.MatchMember", "Squad", c => c.Int(nullable: false));
            DropColumn("history.MatchMember", "State");
        }
    }
}
