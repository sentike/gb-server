namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerEveryDayAwardState : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEveryDayAwardEntity", "State", c => c.Short(nullable: false));
            DropColumn("players.PlayerEveryDayAwardEntity", "IsNotified");
        }
        
        public override void Down()
        {
            AddColumn("players.PlayerEveryDayAwardEntity", "IsNotified", c => c.Boolean(nullable: false));
            DropColumn("players.PlayerEveryDayAwardEntity", "State");
        }
    }
}
