namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMatchHistory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "history.MatchEntity",
                c => new
                    {
                        MatchId = c.Guid(nullable: false),
                        MachineAddress = c.String(),
                        Options_GameMap = c.Int(nullable: false),
                        Options_GameMode = c.Int(nullable: false),
                        Options_Region = c.Short(nullable: false),
                        Options_RoundTimeInSeconds = c.Short(nullable: false),
                        Options_UniversalId = c.Guid(),
                        Options_Bet_Amount = c.Int(nullable: false),
                        Options_Bet_Currency = c.Short(nullable: false),
                        Options_Difficulty = c.Double(nullable: false),
                        Options_AdditionalFlag = c.Int(nullable: false),
                        Options_NumberOfBots = c.Short(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Finished = c.DateTime(nullable: false),
                        WinnerTeamId = c.Guid(),
                    })
                .PrimaryKey(t => t.MatchId)
                .ForeignKey("history.MatchTeam", t => t.WinnerTeamId)
                .Index(t => t.WinnerTeamId);
            
            CreateTable(
                "history.MatchMember",
                c => new
                    {
                        MemberId = c.Guid(nullable: false),
                        MatchId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        TeamId = c.Guid(nullable: false),
                        HasNotified = c.Boolean(nullable: false),
                        Level = c.Short(nullable: false),
                        Squad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MemberId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("history.MatchTeam", t => t.TeamId, cascadeDelete: true)
                .ForeignKey("history.MatchEntity", t => t.MatchId, cascadeDelete: true)
                .Index(t => t.MatchId)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId);
            
            CreateTable(
                "history.MatchMemberAchievement",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        MemberId = c.Guid(nullable: false),
                        AchievementId = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.AchievementEntity", t => t.AchievementId, cascadeDelete: true)
                .ForeignKey("history.MatchMember", t => t.MemberId, cascadeDelete: true)
                .Index(t => new { t.MemberId, t.AchievementId }, unique: true, name: "MemberId_AchievementId");
            
            CreateTable(
                "history.MatchTeam",
                c => new
                    {
                        TeamId = c.Guid(nullable: false),
                        MatchId = c.Guid(nullable: false),
                        Score = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.TeamId)
                .ForeignKey("history.MatchEntity", t => t.MatchId, cascadeDelete: true)
                .Index(t => t.MatchId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("history.MatchEntity", "WinnerTeamId", "history.MatchTeam");
            DropForeignKey("history.MatchTeam", "MatchId", "history.MatchEntity");
            DropForeignKey("history.MatchMember", "MatchId", "history.MatchEntity");
            DropForeignKey("history.MatchMember", "TeamId", "history.MatchTeam");
            DropForeignKey("history.MatchMember", "PlayerId", "players.PlayerEntity");
            DropForeignKey("history.MatchMemberAchievement", "MemberId", "history.MatchMember");
            DropForeignKey("history.MatchMemberAchievement", "AchievementId", "store.AchievementEntity");
            DropIndex("history.MatchTeam", new[] { "MatchId" });
            DropIndex("history.MatchMemberAchievement", "MemberId_AchievementId");
            DropIndex("history.MatchMember", new[] { "TeamId" });
            DropIndex("history.MatchMember", new[] { "PlayerId" });
            DropIndex("history.MatchMember", new[] { "MatchId" });
            DropIndex("history.MatchEntity", new[] { "WinnerTeamId" });
            DropTable("history.MatchTeam");
            DropTable("history.MatchMemberAchievement");
            DropTable("history.MatchMember");
            DropTable("history.MatchEntity");
        }
    }
}
