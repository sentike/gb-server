namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedChangeNameTickets : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Experience_ChangeNameTickets", c => c.Short(nullable: false, defaultValue: 1, defaultValueSql: "1"));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Experience_ChangeNameTickets");
        }
    }
}
