namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRatingPosition : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Experience_RatingPosition", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Experience_RatingPosition");
        }
    }
}
