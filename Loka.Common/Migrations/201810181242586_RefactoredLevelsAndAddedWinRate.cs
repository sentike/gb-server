namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoredLevelsAndAddedWinRate : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "WinRate", c => c.Short(nullable: false));
            AddColumn("history.MatchMember", "WinRate", c => c.Short(nullable: false));
            AlterColumn("store.AbstractItemInstance", "Level", c => c.Short(nullable: false));
            AlterColumn("players.InstanceOfPlayerItem", "Level", c => c.Short(nullable: false));
            AlterColumn("public.QueueEntity", "Level_Min", c => c.Short(nullable: false));
            AlterColumn("public.QueueEntity", "Level_Max", c => c.Short(nullable: false));
            DropColumn("matches.MatchMember", "Difficulty");
        }
        
        public override void Down()
        {
            AddColumn("matches.MatchMember", "Difficulty", c => c.Double(nullable: false));
            AlterColumn("public.QueueEntity", "Level_Max", c => c.Int(nullable: false));
            AlterColumn("public.QueueEntity", "Level_Min", c => c.Int(nullable: false));
            AlterColumn("players.InstanceOfPlayerItem", "Level", c => c.Int(nullable: false));
            AlterColumn("store.AbstractItemInstance", "Level", c => c.Int(nullable: false));
            DropColumn("history.MatchMember", "WinRate");
            DropColumn("matches.MatchMember", "WinRate");
        }
    }
}
