namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProfileItem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerProfileItemEntity",
                c => new
                    {
                        ProfileItemId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        SlotId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProfileItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => new { t.PlayerId, t.SlotId }, unique: true, name: "ProfileItemSlot")
                .Index(t => t.ItemId);
            
            CreateIndex("players.PlayerEntity", "DefaultItemId");
            AddForeignKey("players.PlayerEntity", "DefaultItemId", "players.PlayerProfileItemEntity", "ProfileItemId");
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerEntity", "DefaultItemId", "players.PlayerProfileItemEntity");
            DropForeignKey("players.PlayerProfileItemEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerProfileItemEntity", "ItemId", "players.InstanceOfPlayerItem");
            DropIndex("players.PlayerProfileItemEntity", new[] { "ItemId" });
            DropIndex("players.PlayerProfileItemEntity", "ProfileItemSlot");
            DropIndex("players.PlayerEntity", new[] { "DefaultItemId" });
            DropTable("players.PlayerProfileItemEntity");
        }
    }
}
