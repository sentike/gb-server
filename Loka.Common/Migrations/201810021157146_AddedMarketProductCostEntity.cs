namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMarketProductCostEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.MarketProductCostEntity",
                c => new
                    {
                        EntityId = c.Short(nullable: false, identity: true),
                        ProductId = c.Short(nullable: false),
                        Country = c.Short(nullable: false),
                        Currency = c.Short(nullable: false),
                        Amount = c.Int(nullable: false),
                        AdminComment = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.MarketProductEntity", t => t.ProductId, cascadeDelete: true)
                .Index(t => new { t.ProductId, t.Country }, unique: true, name: "ProductId_Country")
                .Index(t => t.Currency, unique: true, name: "ProductId_Currency");
            
            AddColumn("store.MarketProductEntity", "AdminComment", c => c.String(maxLength: 256));
            AddColumn("store.MarketProductEntity", "LastUpdateDate", c => c.DateTime(nullable: false));
            AddColumn("store.MarketProductEntity", "CreatedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("store.MarketProductCostEntity", "ProductId", "store.MarketProductEntity");
            DropIndex("store.MarketProductCostEntity", "ProductId_Currency");
            DropIndex("store.MarketProductCostEntity", "ProductId_Country");
            DropColumn("store.MarketProductEntity", "CreatedDate");
            DropColumn("store.MarketProductEntity", "LastUpdateDate");
            DropColumn("store.MarketProductEntity", "AdminComment");
            DropTable("store.MarketProductCostEntity");
        }
    }
}
