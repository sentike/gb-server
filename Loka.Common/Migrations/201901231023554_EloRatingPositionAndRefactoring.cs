namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EloRatingPositionAndRefactoring : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("store.PlayerEveryDayAwardEntity", "ModelId", "store.AbstractItemInstance");
            DropIndex("store.PlayerEveryDayAwardEntity", "Day_Index");
            DropIndex("store.PlayerEveryDayAwardEntity", new[] { "ModelId" });
            DropTable("store.PlayerEveryDayAwardEntity");

            CreateTable(
                "award.EveryDayAwardEntity",
                c => new
                    {
                        EntityId = c.Short(nullable: false, identity: true),
                        AdminComment = c.String(),
                        Money = c.Short(nullable: false),
                        Donate = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId);
            
            CreateTable(
                "award.EveryDayAwardItemEntity",
                c => new
                    {
                        EntityId = c.Short(nullable: false, identity: true),
                        Level = c.Short(nullable: false),
                        AdminComment = c.String(maxLength: 128),
                        Amount = c.Int(nullable: false),
                        ModelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("award.EveryDayAwardEntity", t => t.Level, cascadeDelete: true)
                .ForeignKey("store.AbstractItemInstance", t => t.ModelId, cascadeDelete: true)
                .Index(t => t.Level)
                .Index(t => t.ModelId);
            
            AddColumn("players.PlayerEntity", "EloRatingPosition", c => c.Int(nullable: false));
            DropColumn("players.PlayerEntity", "WeekBonus_JoinDate");
            DropColumn("players.PlayerEntity", "WeekBonus_NotifyDate");
            DropColumn("players.PlayerEntity", "WeekBonus_JoinCount");
        }
        
        public override void Down()
        {
            CreateTable(
                "store.PlayerEveryDayAwardEntity",
                c => new
                    {
                        EntityId = c.Short(nullable: false, identity: true),
                        AdminComment = c.String(maxLength: 128),
                        Day = c.Short(nullable: false),
                        Amount = c.Int(nullable: false),
                        ModelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId);
            
            AddColumn("players.PlayerEntity", "WeekBonus_JoinCount", c => c.Short(nullable: false));
            AddColumn("players.PlayerEntity", "WeekBonus_NotifyDate", c => c.DateTime(nullable: false));
            AddColumn("players.PlayerEntity", "WeekBonus_JoinDate", c => c.DateTime(nullable: false));
            DropForeignKey("award.EveryDayAwardItemEntity", "ModelId", "store.AbstractItemInstance");
            DropForeignKey("award.EveryDayAwardItemEntity", "Level", "award.EveryDayAwardEntity");
            DropIndex("award.EveryDayAwardItemEntity", new[] { "ModelId" });
            DropIndex("award.EveryDayAwardItemEntity", new[] { "Level" });
            DropColumn("players.PlayerEntity", "EloRatingPosition");
            DropTable("award.EveryDayAwardItemEntity");
            DropTable("award.EveryDayAwardEntity");
            CreateIndex("store.PlayerEveryDayAwardEntity", "ModelId");
            CreateIndex("store.PlayerEveryDayAwardEntity", "Day", name: "Day_Index");
            AddForeignKey("store.PlayerEveryDayAwardEntity", "ModelId", "store.AbstractItemInstance", "ModelId", cascadeDelete: true);
        }
    }
}
