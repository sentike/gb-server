namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEveryDayPrimeTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "EloRatingScore", c => c.Short(nullable: false));
            AddColumn("players.PlayerEntity", "Experience_PrimeMatchesCount", c => c.Short(nullable: false));
            AddColumn("players.PlayerEntity", "Experience_PrimeMatchesNextDate", c => c.DateTime(nullable: false));
            AddColumn("matches.MatchMember", "BoosterFlags", c => c.Short(nullable: false));
            DropColumn("players.PlayerEntity", "MoneyLimit");
            DropColumn("matches.MatchMember", "HasUsedPremiumAccount");
        }
        
        public override void Down()
        {
            AddColumn("matches.MatchMember", "HasUsedPremiumAccount", c => c.Boolean(nullable: false));
            AddColumn("players.PlayerEntity", "MoneyLimit", c => c.Long(nullable: false));
            DropColumn("matches.MatchMember", "BoosterFlags");
            DropColumn("players.PlayerEntity", "Experience_PrimeMatchesNextDate");
            DropColumn("players.PlayerEntity", "Experience_PrimeMatchesCount");
            DropColumn("players.PlayerEntity", "EloRatingScore");
        }
    }
}
