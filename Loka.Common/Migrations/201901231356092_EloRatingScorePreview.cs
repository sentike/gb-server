namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EloRatingScorePreview : DbMigration
    {
        public override void Up()
        {
            AddColumn("history.MatchMember", "EloRatingScorePreview", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("history.MatchMember", "EloRatingScorePreview");
        }
    }
}
