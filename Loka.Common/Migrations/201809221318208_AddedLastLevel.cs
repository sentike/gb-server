namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLastLevel : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Experience_LastLevel", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Experience_LastLevel");
        }
    }
}
