namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedEloRatingScore : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "EloRatingScore", c => c.Short(nullable: false));
            AddColumn("history.MatchMember", "EloRatingScore", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("history.MatchMember", "EloRatingScore");
            DropColumn("matches.MatchMember", "EloRatingScore");
        }
    }
}
