namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMarketProductCostEntity1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("store.MarketProductCostEntity", "ProductId_Currency");
        }
        
        public override void Down()
        {
            CreateIndex("store.MarketProductCostEntity", "Currency", unique: true, name: "ProductId_Currency");
        }
    }
}
