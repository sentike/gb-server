namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBoughtAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.AbstractItemInstance", "Amount", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.AbstractItemInstance", "Amount");
        }
    }
}
