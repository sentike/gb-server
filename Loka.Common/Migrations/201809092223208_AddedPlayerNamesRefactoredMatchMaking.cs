namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerNamesRefactoredMatchMaking : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("public.GameVersionEntity");
            CreateTable(
                "public.ServerVersionEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeployTarget = c.Short(nullable: false),
                        AdminComment = c.String(maxLength: 128),
                        HasPublished = c.Boolean(nullable: false),
                        HasDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId);
            
            AddColumn("players.PlayerEntity", "PlayerNameNormilized", c => c.String(nullable: false, maxLength: 64));
            AddColumn("players.PlayerEntity", "Experience_IsNameConfirmed", c => c.Boolean(nullable: false));
            AddColumn("players.PlayerEntity", "Experience_TargetServerVersion", c => c.Long(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_TargetServerVersion", c => c.Long(nullable: false));
            AddColumn("public.QueueEntity", "Options_TargetServerVersion", c => c.Long(nullable: false));
            AddColumn("matches.MatchEntity", "Options_TargetServerVersion", c => c.Long(nullable: false));
            AddColumn("public.GameVersionEntity", "ServerVersionId", c => c.Long(nullable: false));
            AddColumn("public.GameVersionEntity", "DeployTarget", c => c.Short(nullable: false));
            AddColumn("public.GameVersionEntity", "AdminComment", c => c.String(maxLength: 128));
            AddColumn("public.GameVersionEntity", "HasPublished", c => c.Boolean(nullable: false));
            AddColumn("history.MatchEntity", "Options_TargetServerVersion", c => c.Long(nullable: false));

            DropColumn("public.GameVersionEntity", "EntityId");
            AddColumn("public.GameVersionEntity", "EntityId", c => c.Long(nullable: false));

            AddPrimaryKey("public.GameVersionEntity", "EntityId");
            DropColumn("public.GameVersionEntity", "StoreVersionId");
            DropColumn("public.GameVersionEntity", "DisplayVersionId");
            DropColumn("public.GameVersionEntity", "Flags");
        }
        
        public override void Down()
        {
            AddColumn("public.GameVersionEntity", "Flags", c => c.Short(nullable: false));
            AddColumn("public.GameVersionEntity", "DisplayVersionId", c => c.String(maxLength: 32));
            AddColumn("public.GameVersionEntity", "StoreVersionId", c => c.Long(nullable: false));
            DropPrimaryKey("public.GameVersionEntity");
            AlterColumn("public.GameVersionEntity", "EntityId", c => c.Guid(nullable: false));
            DropColumn("history.MatchEntity", "Options_TargetServerVersion");
            DropColumn("public.GameVersionEntity", "HasPublished");
            DropColumn("public.GameVersionEntity", "AdminComment");
            DropColumn("public.GameVersionEntity", "DeployTarget");
            DropColumn("public.GameVersionEntity", "ServerVersionId");
            DropColumn("matches.MatchEntity", "Options_TargetServerVersion");
            DropColumn("public.QueueEntity", "Options_TargetServerVersion");
            DropColumn("public.SquadInviteEntity", "Options_TargetServerVersion");
            DropColumn("players.PlayerEntity", "Experience_TargetServerVersion");
            DropColumn("players.PlayerEntity", "Experience_IsNameConfirmed");
            DropColumn("players.PlayerEntity", "PlayerNameNormilized");
            DropTable("public.ServerVersionEntity");
            AddPrimaryKey("public.GameVersionEntity", "EntityId");
        }
    }
}
