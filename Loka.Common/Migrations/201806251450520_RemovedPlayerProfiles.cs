namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedPlayerProfiles : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("players.PlayerProfileItemEntity", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.PlayerProfileItemEntity", "PlayerProfileId", "players.PlayerProfileEntity");
            DropForeignKey("players.PlayerProfileEntity", "DefaultItemId", "players.PlayerProfileItemEntity");
            DropForeignKey("players.PlayerProfileEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("players.PlayerProfileEntity", new[] { "PlayerId" });
            DropIndex("players.PlayerProfileEntity", new[] { "DefaultItemId" });
            DropIndex("players.PlayerProfileItemEntity", "ProfileItemSlot");
            DropIndex("players.PlayerProfileItemEntity", new[] { "ItemId" });
            AddColumn("players.PlayerEntity", "DefaultItemId", c => c.Guid());
            DropTable("players.PlayerProfileEntity");
            DropTable("players.PlayerProfileItemEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "players.PlayerProfileItemEntity",
                c => new
                    {
                        ProfileItemId = c.Guid(nullable: false),
                        PlayerProfileId = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        SlotId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProfileItemId);
            
            CreateTable(
                "players.PlayerProfileEntity",
                c => new
                    {
                        ProfileId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        DefaultItemId = c.Guid(),
                    })
                .PrimaryKey(t => t.ProfileId);
            
            DropColumn("players.PlayerEntity", "DefaultItemId");
            CreateIndex("players.PlayerProfileItemEntity", "ItemId");
            CreateIndex("players.PlayerProfileItemEntity", new[] { "PlayerProfileId", "SlotId" }, unique: true, name: "ProfileItemSlot");
            CreateIndex("players.PlayerProfileEntity", "DefaultItemId");
            CreateIndex("players.PlayerProfileEntity", "PlayerId");
            AddForeignKey("players.PlayerProfileEntity", "PlayerId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
            AddForeignKey("players.PlayerProfileEntity", "DefaultItemId", "players.PlayerProfileItemEntity", "ProfileItemId");
            AddForeignKey("players.PlayerProfileItemEntity", "PlayerProfileId", "players.PlayerProfileEntity", "ProfileId", cascadeDelete: true);
            AddForeignKey("players.PlayerProfileItemEntity", "ItemId", "players.InstanceOfPlayerItem", "ItemId", cascadeDelete: true);
        }
    }
}
