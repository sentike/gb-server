namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMatchMemberBotHistoryEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "history.MatchMemberBotHistoryEntity",
                c => new
                    {
                        MemberId = c.Guid(nullable: false),
                        TeamId = c.Guid(nullable: false),
                        MatchId = c.Guid(nullable: false),
                        PlayerName = c.String(),
                        Kills = c.Int(nullable: false),
                        Deads = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MemberId)
                .ForeignKey("history.MatchEntity", t => t.MatchId, cascadeDelete: true)
                .Index(t => t.MatchId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("history.MatchMemberBotHistoryEntity", "MatchId", "history.MatchEntity");
            DropIndex("history.MatchMemberBotHistoryEntity", new[] { "MatchId" });
            DropTable("history.MatchMemberBotHistoryEntity");
        }
    }
}
