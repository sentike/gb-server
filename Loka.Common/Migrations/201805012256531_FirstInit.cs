namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstInit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerEntity",
                c => new
                    {
                        PlayerId = c.Guid(nullable: false),
                        PlayerName = c.String(maxLength: 64),
                        PremiumEndDate = c.DateTime(),
                        Experience_Level = c.Int(nullable: false),
                        Experience_Experience = c.Long(nullable: false),
                        Experience_WeekExperience = c.Long(nullable: false),
                        Experience_WeekActivityTime = c.Long(nullable: false),
                        WeekBonus_JoinDate = c.DateTime(nullable: false),
                        WeekBonus_NotifyDate = c.DateTime(nullable: false),
                        WeekBonus_JoinCount = c.Short(nullable: false),
                        Role = c.Short(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                        Language = c.Short(nullable: false),
                        MigrationAction = c.Int(nullable: false),
                        LastSessionId = c.Guid(nullable: false),
                        MoneyLimit = c.Long(nullable: false),
                        CurrentTutorialId = c.Guid(),
                        RowVersion = c.Guid(nullable: false),
                        SubscribeEndDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.PlayerId)
                .ForeignKey("store.PlayerTutorialEntity", t => t.CurrentTutorialId)
                .Index(t => t.CurrentTutorialId);
            
            CreateTable(
                "players.PlayerCashEntity",
                c => new
                    {
                        CashId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        Amount = c.Long(nullable: false),
                        LastAmount = c.Long(nullable: false),
                        Currency = c.Short(nullable: false),
                        LastChangedDate = c.DateTime(nullable: false),
                        Hash = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CashId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => new { t.PlayerId, t.Currency }, unique: true, name: "PlayerId_Currency");
            
            CreateTable(
                "store.PlayerTutorialEntity",
                c => new
                    {
                        ProgressId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        TutorialId = c.Long(nullable: false),
                        StepId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CompleteDate = c.DateTime(nullable: false),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProgressId)
                .ForeignKey("store.TutorialStepEntity", t => t.StepId, cascadeDelete: true)
                .ForeignKey("store.TutorialInstanceEntity", t => t.TutorialId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.TutorialId)
                .Index(t => t.StepId);
            
            CreateTable(
                "store.TutorialStepEntity",
                c => new
                    {
                        StepKey = c.Long(nullable: false, identity: true),
                        TutorialId = c.Long(nullable: false),
                        StepId = c.Long(nullable: false),
                        RewardModelId = c.Int(),
                        RewardActivate = c.Long(nullable: false),
                        RewardGive = c.Long(nullable: false),
                        AdminComment = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.StepKey)
                .ForeignKey("store.AbstractItemInstance", t => t.RewardModelId)
                .ForeignKey("store.TutorialInstanceEntity", t => t.TutorialId, cascadeDelete: true)
                .Index(t => new { t.TutorialId, t.StepId }, unique: true, name: "Tutorial_Step")
                .Index(t => t.RewardModelId);
            
            CreateTable(
                "store.AbstractItemInstance",
                c => new
                    {
                        ModelId = c.Int(nullable: false, identity: true),
                        Cost_Amount = c.Int(nullable: false),
                        Cost_Currency = c.Short(nullable: false),
                        AdminComment = c.String(maxLength: 128),
                        DisplayPosition = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                        Flags = c.Int(nullable: false),
                        MigrationAction = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ModelId);
            
            CreateTable(
                "store.TutorialInstanceEntity",
                c => new
                    {
                        TutorialId = c.Long(nullable: false, identity: true),
                        TutorialName = c.String(nullable: false, maxLength: 128),
                        AdminComment = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TutorialId)
                .Index(t => t.TutorialName, unique: true);
            
            CreateTable(
                "players.FriendEntity",
                c => new
                    {
                        MemberId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        FriendId = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MemberId)
                .ForeignKey("players.PlayerEntity", t => t.FriendId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.FriendId);
            
            CreateTable(
                "public.SquadInviteEntity",
                c => new
                    {
                        InviteId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        SquadId = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                        SubmittedDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                        Options_GameMap = c.Int(nullable: false),
                        Options_GameMode = c.Int(nullable: false),
                        Options_Region = c.Short(nullable: false),
                        Options_RoundTimeInSeconds = c.Short(nullable: false),
                        Options_UniversalId = c.Guid(),
                        Options_Bet_Amount = c.Int(nullable: false),
                        Options_Bet_Currency = c.Short(nullable: false),
                        Options_Difficulty = c.Double(nullable: false),
                        Options_AdditionalFlag = c.Int(nullable: false),
                        Options_NumberOfBots = c.Short(nullable: false),
                        Direction = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InviteId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.SquadId, cascadeDelete: true)
                .ForeignKey("public.QueueEntity", t => t.SquadId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.SquadId);
            
            CreateTable(
                "public.QueueEntity",
                c => new
                    {
                        QueueId = c.Guid(nullable: false),
                        PartyId = c.Guid(nullable: false),
                        Options_GameMap = c.Int(nullable: false),
                        Options_GameMode = c.Int(nullable: false),
                        Options_Region = c.Short(nullable: false),
                        Options_RoundTimeInSeconds = c.Short(nullable: false),
                        Options_UniversalId = c.Guid(),
                        Options_Bet_Amount = c.Int(nullable: false),
                        Options_Bet_Currency = c.Short(nullable: false),
                        Options_Difficulty = c.Double(nullable: false),
                        Options_AdditionalFlag = c.Int(nullable: false),
                        Options_NumberOfBots = c.Short(nullable: false),
                        CheckDate = c.DateTime(nullable: false),
                        JoinDate = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                        Level_Min = c.Int(nullable: false),
                        Level_Max = c.Int(nullable: false),
                        Ready = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QueueId)
                .ForeignKey("public.QueueEntity", t => t.PartyId)
                .ForeignKey("players.PlayerEntity", t => t.QueueId, cascadeDelete: true)
                .Index(t => t.QueueId)
                .Index(t => t.PartyId);
            
            CreateTable(
                "players.InstanceOfPlayerItem",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                        ModelId = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                        PurchaseDate = c.DateTime(nullable: false),
                        LastUseDate = c.DateTime(),
                        PlayerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("store.AbstractItemInstance", t => t.ModelId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => new { t.PlayerId, t.ModelId }, unique: true, name: "ix_instanceofplayerItem_itemsingleton");
            
            CreateTable(
                "players.PlayerProfileEntity",
                c => new
                    {
                        ProfileId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ProfileId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "players.PlayerProfileItemEntity",
                c => new
                    {
                        ProfileItemId = c.Guid(nullable: false),
                        PlayerProfileId = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        SlotId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProfileItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("players.PlayerProfileEntity", t => t.PlayerProfileId, cascadeDelete: true)
                .Index(t => new { t.PlayerProfileId, t.SlotId }, unique: true, name: "ProfileItemSlot")
                .Index(t => t.ItemId);
            
            CreateTable(
                "matches.MatchMember",
                c => new
                    {
                        MemberId = c.Guid(nullable: false),
                        MatchId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        TeamId = c.Guid(nullable: false),
                        Level = c.Short(nullable: false),
                        Squad = c.Int(nullable: false),
                        Difficulty = c.Double(nullable: false),
                        State = c.Int(nullable: false),
                        SessionToken = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.MemberId)
                .ForeignKey("matches.MatchEntity", t => t.MatchId, cascadeDelete: true)
                .ForeignKey("matches.MatchTeam", t => t.TeamId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.MatchId)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId);
            
            CreateTable(
                "matches.MatchEntity",
                c => new
                    {
                        MatchId = c.Guid(nullable: false),
                        MachineAddress = c.String(nullable: false, maxLength: 64),
                        MachinePort = c.Short(nullable: false),
                        Options_GameMap = c.Int(nullable: false),
                        Options_GameMode = c.Int(nullable: false),
                        Options_Region = c.Short(nullable: false),
                        Options_RoundTimeInSeconds = c.Short(nullable: false),
                        Options_UniversalId = c.Guid(),
                        Options_Bet_Amount = c.Int(nullable: false),
                        Options_Bet_Currency = c.Short(nullable: false),
                        Options_Difficulty = c.Double(nullable: false),
                        Options_AdditionalFlag = c.Int(nullable: false),
                        Options_NumberOfBots = c.Short(nullable: false),
                        IsAllowJoinNewPlayers = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                        StartedOfMatch = c.DateTime(),
                        StartedOfProcecess = c.DateTime(),
                        Finished = c.DateTime(),
                        Shutdown = c.DateTime(),
                        LastCheckDate = c.DateTime(),
                        LastActivityDate = c.DateTime(),
                        ShutdownCode = c.Int(nullable: false),
                        ProcessId = c.Int(nullable: false),
                        GameVersion = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                        WinnerTeamId = c.Guid(),
                        TimeLeftInSeconds = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.MatchId)
                .ForeignKey("public.ClusterInstanceEntity", t => t.MachineAddress)
                .ForeignKey("matches.MatchTeam", t => t.WinnerTeamId)
                .Index(t => t.MachineAddress)
                .Index(t => t.WinnerTeamId);
            
            CreateTable(
                "public.ClusterInstanceEntity",
                c => new
                    {
                        MachineAddress = c.String(nullable: false, maxLength: 64),
                        MachineName = c.String(maxLength: 64),
                        RegionTypeId = c.Short(nullable: false),
                        LimitActiveNodes = c.Int(nullable: false),
                        Performance_CurrentAvalibleMemory = c.Long(nullable: false),
                        Performance_TotalAvalibleMemory = c.Long(nullable: false),
                        Performance_NumberOfCores = c.Short(nullable: false),
                        Performance_ProcessorLoad = c.Short(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                        RowVersion = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.MachineAddress);
            
            CreateTable(
                "matches.MatchTeam",
                c => new
                    {
                        TeamId = c.Guid(nullable: false),
                        MatchId = c.Guid(nullable: false),
                        Score = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.TeamId)
                .ForeignKey("matches.MatchEntity", t => t.MatchId, cascadeDelete: true)
                .Index(t => t.MatchId);
            
            CreateTable(
                "chat.ChatChanelEntity",
                c => new
                    {
                        ChanelId = c.Guid(nullable: false),
                        ChanelName = c.String(maxLength: 256),
                        GroupId = c.Guid(),
                        ChanelGroup = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ChanelId);
            
            CreateTable(
                "chat.ChatChanelMemberEntity",
                c => new
                    {
                        MemberId = c.Guid(nullable: false),
                        MemberFlags = c.Int(nullable: false),
                        ChanelId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.MemberId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("chat.ChatChanelEntity", t => t.ChanelId, cascadeDelete: true)
                .Index(t => t.ChanelId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "chat.ChatChanelMessageEntity",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        ChanelId = c.Guid(nullable: false),
                        MemberId = c.Guid(nullable: false),
                        Message = c.String(nullable: false, maxLength: 256),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("chat.ChatChanelMemberEntity", t => t.MemberId, cascadeDelete: true)
                .ForeignKey("chat.ChatChanelEntity", t => t.ChanelId, cascadeDelete: true)
                .Index(t => t.ChanelId)
                .Index(t => t.MemberId);
            
            CreateTable(
                "gamemode.GameMapEntity",
                c => new
                    {
                        InstanceId = c.Int(nullable: false, identity: true),
                        GameModeId = c.Int(nullable: false),
                        MapId = c.Int(nullable: false),
                        Members_Min = c.Int(nullable: false),
                        Members_Max = c.Int(nullable: false),
                        RoundTimeInSeconds = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InstanceId)
                .ForeignKey("gamemode.GameModeEntity", t => t.GameModeId, cascadeDelete: true)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "gamemode.GameModeEntity",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                        Level_Min = c.Int(nullable: false),
                        Level_Max = c.Int(nullable: false),
                        PrepareTimeInSeconds = c.Int(nullable: false),
                        JoinCost_Amount = c.Int(nullable: false),
                        JoinCost_Currency = c.Short(nullable: false),
                        Priority = c.Short(nullable: false),
                        ScoreLimit = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId);
            
            CreateTable(
                "public.GameSessionEntity",
                c => new
                    {
                        SessionId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        LastMessageAccess = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                        LastMoneyGenerate = c.DateTime(nullable: false),
                        SessionAttach = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SessionId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "transaction.MatchTransactionEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModelId = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                        LastAmount = c.Long(nullable: false),
                        SessionId = c.Guid(nullable: false),
                        MatchId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("players.PlayerEntity", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId)
                .Index(t => t.SessionId)
                .Index(t => t.MatchId);
            
            CreateTable(
                "transaction.OrderTransactionEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModelId = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                        LastAmount = c.Long(nullable: false),
                        OrderId = c.Guid(nullable: false),
                        Notified = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("players.PlayerEntity", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "transaction.StoreTransactionEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModelId = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                        LastAmount = c.Long(nullable: false),
                        SessionId = c.Guid(nullable: false),
                        RecipientId = c.Guid(nullable: false),
                        Cost = c.Long(nullable: false),
                        Balanace = c.Long(nullable: false),
                        Currency = c.Short(nullable: false),
                        Notified = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("players.PlayerEntity", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId)
                .Index(t => t.RecipientId);
            
            CreateTable(
                "gamemode.LastHeroGameModeEntity",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.GameModeEntity", t => t.GameModeId)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "gamemode.TeamGameModeEntity",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.GameModeEntity", t => t.GameModeId)
                .Index(t => t.GameModeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("gamemode.TeamGameModeEntity", "GameModeId", "gamemode.GameModeEntity");
            DropForeignKey("gamemode.LastHeroGameModeEntity", "GameModeId", "gamemode.GameModeEntity");
            DropForeignKey("transaction.StoreTransactionEntity", "AccountId", "players.PlayerEntity");
            DropForeignKey("transaction.OrderTransactionEntity", "AccountId", "players.PlayerEntity");
            DropForeignKey("transaction.MatchTransactionEntity", "AccountId", "players.PlayerEntity");
            DropForeignKey("public.GameSessionEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("gamemode.GameMapEntity", "GameModeId", "gamemode.GameModeEntity");
            DropForeignKey("chat.ChatChanelMessageEntity", "ChanelId", "chat.ChatChanelEntity");
            DropForeignKey("chat.ChatChanelMemberEntity", "ChanelId", "chat.ChatChanelEntity");
            DropForeignKey("chat.ChatChanelMemberEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("chat.ChatChanelMessageEntity", "MemberId", "chat.ChatChanelMemberEntity");
            DropForeignKey("store.PlayerTutorialEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("matches.MatchMember", "PlayerId", "players.PlayerEntity");
            DropForeignKey("matches.MatchEntity", "WinnerTeamId", "matches.MatchTeam");
            DropForeignKey("matches.MatchTeam", "MatchId", "matches.MatchEntity");
            DropForeignKey("matches.MatchMember", "TeamId", "matches.MatchTeam");
            DropForeignKey("matches.MatchMember", "MatchId", "matches.MatchEntity");
            DropForeignKey("matches.MatchEntity", "MachineAddress", "public.ClusterInstanceEntity");
            DropForeignKey("players.PlayerProfileEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerProfileItemEntity", "PlayerProfileId", "players.PlayerProfileEntity");
            DropForeignKey("players.PlayerProfileItemEntity", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerItem", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.InstanceOfPlayerItem", "ModelId", "store.AbstractItemInstance");
            DropForeignKey("public.SquadInviteEntity", "SquadId", "public.QueueEntity");
            DropForeignKey("public.QueueEntity", "QueueId", "players.PlayerEntity");
            DropForeignKey("public.QueueEntity", "PartyId", "public.QueueEntity");
            DropForeignKey("public.SquadInviteEntity", "SquadId", "players.PlayerEntity");
            DropForeignKey("public.SquadInviteEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.FriendEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.FriendEntity", "FriendId", "players.PlayerEntity");
            DropForeignKey("players.PlayerEntity", "CurrentTutorialId", "store.PlayerTutorialEntity");
            DropForeignKey("store.PlayerTutorialEntity", "TutorialId", "store.TutorialInstanceEntity");
            DropForeignKey("store.PlayerTutorialEntity", "StepId", "store.TutorialStepEntity");
            DropForeignKey("store.TutorialStepEntity", "TutorialId", "store.TutorialInstanceEntity");
            DropForeignKey("store.TutorialStepEntity", "RewardModelId", "store.AbstractItemInstance");
            DropForeignKey("players.PlayerCashEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("gamemode.TeamGameModeEntity", new[] { "GameModeId" });
            DropIndex("gamemode.LastHeroGameModeEntity", new[] { "GameModeId" });
            DropIndex("transaction.StoreTransactionEntity", new[] { "RecipientId" });
            DropIndex("transaction.StoreTransactionEntity", new[] { "AccountId" });
            DropIndex("transaction.OrderTransactionEntity", new[] { "OrderId" });
            DropIndex("transaction.OrderTransactionEntity", new[] { "AccountId" });
            DropIndex("transaction.MatchTransactionEntity", new[] { "MatchId" });
            DropIndex("transaction.MatchTransactionEntity", new[] { "SessionId" });
            DropIndex("transaction.MatchTransactionEntity", new[] { "AccountId" });
            DropIndex("public.GameSessionEntity", new[] { "PlayerId" });
            DropIndex("gamemode.GameMapEntity", new[] { "GameModeId" });
            DropIndex("chat.ChatChanelMessageEntity", new[] { "MemberId" });
            DropIndex("chat.ChatChanelMessageEntity", new[] { "ChanelId" });
            DropIndex("chat.ChatChanelMemberEntity", new[] { "PlayerId" });
            DropIndex("chat.ChatChanelMemberEntity", new[] { "ChanelId" });
            DropIndex("matches.MatchTeam", new[] { "MatchId" });
            DropIndex("matches.MatchEntity", new[] { "WinnerTeamId" });
            DropIndex("matches.MatchEntity", new[] { "MachineAddress" });
            DropIndex("matches.MatchMember", new[] { "TeamId" });
            DropIndex("matches.MatchMember", new[] { "PlayerId" });
            DropIndex("matches.MatchMember", new[] { "MatchId" });
            DropIndex("players.PlayerProfileItemEntity", new[] { "ItemId" });
            DropIndex("players.PlayerProfileItemEntity", "ProfileItemSlot");
            DropIndex("players.PlayerProfileEntity", new[] { "PlayerId" });
            DropIndex("players.InstanceOfPlayerItem", "ix_instanceofplayerItem_itemsingleton");
            DropIndex("public.QueueEntity", new[] { "PartyId" });
            DropIndex("public.QueueEntity", new[] { "QueueId" });
            DropIndex("public.SquadInviteEntity", new[] { "SquadId" });
            DropIndex("public.SquadInviteEntity", new[] { "PlayerId" });
            DropIndex("players.FriendEntity", new[] { "FriendId" });
            DropIndex("players.FriendEntity", new[] { "PlayerId" });
            DropIndex("store.TutorialInstanceEntity", new[] { "TutorialName" });
            DropIndex("store.TutorialStepEntity", new[] { "RewardModelId" });
            DropIndex("store.TutorialStepEntity", "Tutorial_Step");
            DropIndex("store.PlayerTutorialEntity", new[] { "StepId" });
            DropIndex("store.PlayerTutorialEntity", new[] { "TutorialId" });
            DropIndex("store.PlayerTutorialEntity", new[] { "PlayerId" });
            DropIndex("players.PlayerCashEntity", "PlayerId_Currency");
            DropIndex("players.PlayerEntity", new[] { "CurrentTutorialId" });
            DropTable("gamemode.TeamGameModeEntity");
            DropTable("gamemode.LastHeroGameModeEntity");
            DropTable("transaction.StoreTransactionEntity");
            DropTable("transaction.OrderTransactionEntity");
            DropTable("transaction.MatchTransactionEntity");
            DropTable("public.GameSessionEntity");
            DropTable("gamemode.GameModeEntity");
            DropTable("gamemode.GameMapEntity");
            DropTable("chat.ChatChanelMessageEntity");
            DropTable("chat.ChatChanelMemberEntity");
            DropTable("chat.ChatChanelEntity");
            DropTable("matches.MatchTeam");
            DropTable("public.ClusterInstanceEntity");
            DropTable("matches.MatchEntity");
            DropTable("matches.MatchMember");
            DropTable("players.PlayerProfileItemEntity");
            DropTable("players.PlayerProfileEntity");
            DropTable("players.InstanceOfPlayerItem");
            DropTable("public.QueueEntity");
            DropTable("public.SquadInviteEntity");
            DropTable("players.FriendEntity");
            DropTable("store.TutorialInstanceEntity");
            DropTable("store.AbstractItemInstance");
            DropTable("store.TutorialStepEntity");
            DropTable("store.PlayerTutorialEntity");
            DropTable("players.PlayerCashEntity");
            DropTable("players.PlayerEntity");
        }
    }
}
