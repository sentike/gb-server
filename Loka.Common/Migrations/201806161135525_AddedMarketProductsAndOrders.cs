namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMarketProductsAndOrders : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerPaymentEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        ProductId = c.Short(nullable: false),
                        OrderId = c.String(nullable: false, maxLength: 256),
                        TokenId = c.String(nullable: false, maxLength: 256),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("store.MarketProductEntity", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.ProductId)
                .Index(t => t.OrderId, unique: true, name: "PlayerPaymentEntity_OrderId")
                .Index(t => t.TokenId, unique: true, name: "PlayerPaymentEntity_TokenId");
            
            CreateTable(
                "store.MarketProductEntity",
                c => new
                    {
                        ProductId = c.Short(nullable: false, identity: true),
                        ProductName = c.String(nullable: false, maxLength: 256),
                        Amount = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .Index(t => t.ProductName, unique: true, name: "MarketProductEntity_ProductName");
            
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerPaymentEntity", "ProductId", "store.MarketProductEntity");
            DropForeignKey("players.PlayerPaymentEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("store.MarketProductEntity", "MarketProductEntity_ProductName");
            DropIndex("players.PlayerPaymentEntity", "PlayerPaymentEntity_TokenId");
            DropIndex("players.PlayerPaymentEntity", "PlayerPaymentEntity_OrderId");
            DropIndex("players.PlayerPaymentEntity", new[] { "ProductId" });
            DropIndex("players.PlayerPaymentEntity", new[] { "PlayerId" });
            DropTable("store.MarketProductEntity");
            DropTable("players.PlayerPaymentEntity");
        }
    }
}
