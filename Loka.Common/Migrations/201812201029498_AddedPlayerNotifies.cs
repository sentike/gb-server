namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerNotifies : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Notifications_AfterMatchNotifyNextDate", c => c.DateTime(nullable: false));
            AddColumn("players.PlayerEntity", "Notifications_LowActivityNotifyNextDate", c => c.DateTime(nullable: false));
            AddColumn("players.PlayerEntity", "Notifications_BeforeLevelUpNotifyNextDate", c => c.DateTime(nullable: false));
            AddColumn("players.PlayerEntity", "Notifications_AfterLevelUpNotifyNextDate", c => c.DateTime(nullable: false));
            AddColumn("players.PlayerEntity", "Notifications_AfterLevelUpNotifyCount", c => c.Short(nullable: false));
            AddColumn("players.PlayerCaseEntity", "NextNotifyDate", c => c.DateTime());
            AddColumn("store.CaseEntity", "FirstNotifyDelay", c => c.Time(precision: 6));
            AddColumn("store.CaseEntity", "NextNotifyDelay", c => c.Time(precision: 6));
        }
        
        public override void Down()
        {
            DropColumn("store.CaseEntity", "NextNotifyDelay");
            DropColumn("store.CaseEntity", "FirstNotifyDelay");
            DropColumn("players.PlayerCaseEntity", "NextNotifyDate");
            DropColumn("players.PlayerEntity", "Notifications_AfterLevelUpNotifyCount");
            DropColumn("players.PlayerEntity", "Notifications_AfterLevelUpNotifyNextDate");
            DropColumn("players.PlayerEntity", "Notifications_BeforeLevelUpNotifyNextDate");
            DropColumn("players.PlayerEntity", "Notifications_LowActivityNotifyNextDate");
            DropColumn("players.PlayerEntity", "Notifications_AfterMatchNotifyNextDate");
        }
    }
}
