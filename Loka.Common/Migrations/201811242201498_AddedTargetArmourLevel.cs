namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTargetArmourLevel : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "TargetArmourLevel", c => c.Short(nullable: false));
            RenameColumn("matches.MatchMember", "Level", "TargetWeaponLevel");
        }
        
        public override void Down()
        {
            AddColumn("matches.MatchMember", "Level", c => c.Short(nullable: false));
            DropColumn("matches.MatchMember", "TargetArmourLevel");
            DropColumn("matches.MatchMember", "TargetWeaponLevel");
        }
    }
}
