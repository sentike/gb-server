namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringPlayerLevelAwardEntity : DbMigration
    {
        public override void Up()
        {
            DropIndex("store.PlayerLevelAwardEntity", "Day_Index");
            DropIndex("store.PlayerLevelAwardEntity", new[] { "ModelId" });
            DropColumn("store.PlayerLevelAwardEntity", "AdminComment");
            DropColumn("store.PlayerLevelAwardEntity", "Level");
            DropColumn("store.PlayerLevelAwardEntity", "Amount");
            DropColumn("store.PlayerLevelAwardEntity", "ModelId");
            DropForeignKey("store.PlayerLevelAwardEntity", "ModelId", "store.AbstractItemInstance");

            MoveTable(name: "store.PlayerLevelAwardEntity", newSchema: "award");

            CreateTable(
                "award.PlayerLevelRewardItemEntity",
                c => new
                    {
                        EntityId = c.Short(nullable: false, identity: true),
                        Level = c.Short(nullable: false),
                        AdminComment = c.String(maxLength: 128),
                        Amount = c.Int(nullable: false),
                        ModelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("award.PlayerLevelAwardEntity", t => t.Level, cascadeDelete: true)
                .ForeignKey("store.AbstractItemInstance", t => t.ModelId, cascadeDelete: true)
                .Index(t => t.Level)
                .Index(t => t.ModelId);
            
            AddColumn("award.PlayerLevelAwardEntity", "Money", c => c.Short(nullable: false));
            AddColumn("award.PlayerLevelAwardEntity", "Donate", c => c.Short(nullable: false));

        }
        
        public override void Down()
        {
            AddColumn("award.PlayerLevelAwardEntity", "ModelId", c => c.Int(nullable: false));
            AddColumn("award.PlayerLevelAwardEntity", "Amount", c => c.Int(nullable: false));
            AddColumn("award.PlayerLevelAwardEntity", "Level", c => c.Short(nullable: false));
            AddColumn("award.PlayerLevelAwardEntity", "AdminComment", c => c.String(maxLength: 128));
            DropForeignKey("award.PlayerLevelRewardItemEntity", "ModelId", "store.AbstractItemInstance");
            DropForeignKey("award.PlayerLevelRewardItemEntity", "Level", "award.PlayerLevelAwardEntity");
            DropIndex("award.PlayerLevelRewardItemEntity", new[] { "ModelId" });
            DropIndex("award.PlayerLevelRewardItemEntity", new[] { "Level" });
            DropColumn("award.PlayerLevelAwardEntity", "Donate");
            DropColumn("award.PlayerLevelAwardEntity", "Money");
            DropTable("award.PlayerLevelRewardItemEntity");
            CreateIndex("award.PlayerLevelAwardEntity", "ModelId");
            CreateIndex("award.PlayerLevelAwardEntity", "Level", name: "Day_Index");
            AddForeignKey("store.PlayerLevelAwardEntity", "ModelId", "store.AbstractItemInstance", "ModelId", cascadeDelete: true);
            MoveTable(name: "award.PlayerLevelAwardEntity", newSchema: "store");
        }
    }
}
