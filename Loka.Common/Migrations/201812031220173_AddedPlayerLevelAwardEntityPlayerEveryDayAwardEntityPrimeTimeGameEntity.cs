namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerLevelAwardEntityPlayerEveryDayAwardEntityPrimeTimeGameEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gamemode.PrimeTimeGameEntity",
                c => new
                    {
                        EntityId = c.Int(nullable: false, identity: true),
                        GameModeId = c.Int(nullable: false),
                        Days = c.Int(nullable: false),
                        From = c.Time(nullable: false, precision: 6),
                        End = c.Time(nullable: false, precision: 6),
                        Boost_Money = c.Int(nullable: false),
                        Boost_Experience = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("gamemode.GameModeEntity", t => t.GameModeId, cascadeDelete: true)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "store.PlayerEveryDayAwardEntity",
                c => new
                    {
                        EntityId = c.Short(nullable: false, identity: true),
                        AdminComment = c.String(maxLength: 128),
                        Day = c.Short(nullable: false),
                        Amount = c.Int(nullable: false),
                        ModelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.AbstractItemInstance", t => t.ModelId, cascadeDelete: true)
                .Index(t => t.Day, name: "Day_Index")
                .Index(t => t.ModelId);
            
            CreateTable(
                "store.PlayerLevelAwardEntity",
                c => new
                    {
                        EntityId = c.Short(nullable: false, identity: true),
                        AdminComment = c.String(maxLength: 128),
                        Level = c.Short(nullable: false),
                        Amount = c.Int(nullable: false),
                        ModelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.AbstractItemInstance", t => t.ModelId, cascadeDelete: true)
                .Index(t => t.Level, name: "Day_Index")
                .Index(t => t.ModelId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("store.PlayerLevelAwardEntity", "ModelId", "store.AbstractItemInstance");
            DropForeignKey("store.PlayerEveryDayAwardEntity", "ModelId", "store.AbstractItemInstance");
            DropForeignKey("gamemode.PrimeTimeGameEntity", "GameModeId", "gamemode.GameModeEntity");
            DropIndex("store.PlayerLevelAwardEntity", new[] { "ModelId" });
            DropIndex("store.PlayerLevelAwardEntity", "Day_Index");
            DropIndex("store.PlayerEveryDayAwardEntity", new[] { "ModelId" });
            DropIndex("store.PlayerEveryDayAwardEntity", "Day_Index");
            DropIndex("gamemode.PrimeTimeGameEntity", new[] { "GameModeId" });
            DropTable("store.PlayerLevelAwardEntity");
            DropTable("store.PlayerEveryDayAwardEntity");
            DropTable("gamemode.PrimeTimeGameEntity");
        }
    }
}
