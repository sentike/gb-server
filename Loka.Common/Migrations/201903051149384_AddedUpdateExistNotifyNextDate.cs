namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUpdateExistNotifyNextDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Notifications_UpdateExistNotifyNextDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Notifications_UpdateExistNotifyNextDate");
        }
    }
}
