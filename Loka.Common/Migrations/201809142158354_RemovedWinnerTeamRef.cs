namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedWinnerTeamRef : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("matches.MatchEntity", "WinnerTeamId", "matches.MatchTeam");
            DropIndex("matches.MatchEntity", new[] { "WinnerTeamId" });
        }
        
        public override void Down()
        {
            CreateIndex("matches.MatchEntity", "WinnerTeamId");
            AddForeignKey("matches.MatchEntity", "WinnerTeamId", "matches.MatchTeam", "TeamId");
        }
    }
}
