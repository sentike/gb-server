namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerDiscountEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerDiscountEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        ModelId = c.Int(nullable: false),
                        Coupone = c.Short(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUsingDate = c.DateTime(),
                        NextAvalibleDate = c.DateTime(),
                        EndDiscountDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.AbstractItemInstance", t => t.ModelId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => new { t.PlayerId, t.ModelId }, unique: true, name: "PlayerId_ModelId");
            
            AddColumn("players.PlayerEntity", "Country", c => c.Short(nullable: false));
            CreateIndex("players.PlayerEntity", "Country", name: "Country_Index");
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerDiscountEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerDiscountEntity", "ModelId", "store.AbstractItemInstance");
            DropIndex("players.PlayerDiscountEntity", "PlayerId_ModelId");
            DropIndex("players.PlayerEntity", "Country_Index");
            DropColumn("players.PlayerEntity", "Country");
            DropTable("players.PlayerDiscountEntity");
        }
    }
}
