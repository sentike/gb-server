namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAccountEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerAccountServiceEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        Service = c.Short(nullable: false),
                        Token = c.String(maxLength: 256),
                        Login = c.String(maxLength: 64),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerAccountServiceEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("players.PlayerAccountServiceEntity", new[] { "PlayerId" });
            DropTable("players.PlayerAccountServiceEntity");
        }
    }
}
