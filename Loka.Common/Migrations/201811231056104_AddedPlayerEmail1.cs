namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerEmail1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("players.PlayerEntity", "PlayerEmail_Unique");
            AlterColumn("players.PlayerEntity", "PlayerEmail", c => c.String(maxLength: 64));
            CreateIndex("players.PlayerEntity", "PlayerEmail", unique: true, name: "PlayerEmail_Unique");
        }
        
        public override void Down()
        {
            DropIndex("players.PlayerEntity", "PlayerEmail_Unique");
            AlterColumn("players.PlayerEntity", "PlayerEmail", c => c.String(maxLength: 40));
            CreateIndex("players.PlayerEntity", "PlayerEmail", unique: true, name: "PlayerEmail_Unique");
        }
    }
}
