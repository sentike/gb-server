namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGameVersionEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.GameVersionEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        StoreVersionId = c.Long(nullable: false),
                        DisplayVersionId = c.String(maxLength: 32),
                        Flags = c.Short(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId);
            
        }
        
        public override void Down()
        {
            DropTable("public.GameVersionEntity");
        }
    }
}
