namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedItemModificationType : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.AbstractItemInstance", "Modification_Cost_Amount", c => c.Int(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_Cost_Currency", c => c.Short(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_LastLevelCost_Amount", c => c.Int(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_LastLevelCost_Currency", c => c.Short(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_NumberOfLevels", c => c.Short(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_CostCoefficient", c => c.Single(nullable: false));
            AddColumn("store.AbstractItemInstance", "Modification_DamageCoefficient", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.AbstractItemInstance", "Modification_DamageCoefficient");
            DropColumn("store.AbstractItemInstance", "Modification_CostCoefficient");
            DropColumn("store.AbstractItemInstance", "Modification_NumberOfLevels");
            DropColumn("store.AbstractItemInstance", "Modification_LastLevelCost_Currency");
            DropColumn("store.AbstractItemInstance", "Modification_LastLevelCost_Amount");
            DropColumn("store.AbstractItemInstance", "Modification_Cost_Currency");
            DropColumn("store.AbstractItemInstance", "Modification_Cost_Amount");
        }
    }
}
