namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEveryDayAwardEnabledFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("award.EveryDayAwardEntity", "Enabled", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("award.EveryDayAwardEntity", "Enabled");
        }
    }
}
