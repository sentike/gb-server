namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTargetLevel : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchEntity", "TargetLevel", c => c.Short(nullable: false));
            AddColumn("history.MatchEntity", "TargetLevel", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("history.MatchEntity", "TargetLevel");
            DropColumn("matches.MatchEntity", "TargetLevel");
        }
    }
}
