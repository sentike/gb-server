namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MovingRatingPosition : DbMigration
    {
        public override void Up()
        {
            RenameColumn("players.PlayerEntity", "Experience_RatingPosition", "RatingPosition");
        }
        
        public override void Down()
        {
            AddColumn("players.PlayerEntity", "Experience_RatingPosition", c => c.Int(nullable: false));
            DropColumn("players.PlayerEntity", "RatingPosition");
        }
    }
}
