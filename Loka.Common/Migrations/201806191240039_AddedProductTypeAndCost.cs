namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProductTypeAndCost : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.MarketProductEntity", "ProductType", c => c.Int(nullable: false));
            AddColumn("store.MarketProductEntity", "Cost", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.MarketProductEntity", "Cost");
            DropColumn("store.MarketProductEntity", "ProductType");
        }
    }
}
