namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDiscountGroupForCost : DbMigration
    {
        public override void Up()
        {
            DropIndex("store.MarketProductCostEntity", "ProductId_Country");
            AddColumn("store.MarketProductCostEntity", "Discount", c => c.Short(nullable: false));
            CreateIndex("store.MarketProductCostEntity", new[] { "ProductId", "Country", "Discount" }, unique: true, name: "ProductId_Country_Discount");
        }
        
        public override void Down()
        {
            DropIndex("store.MarketProductCostEntity", "ProductId_Country_Discount");
            DropColumn("store.MarketProductCostEntity", "Discount");
            CreateIndex("store.MarketProductCostEntity", new[] { "ProductId", "Country" }, unique: true, name: "ProductId_Country");
        }
    }
}
