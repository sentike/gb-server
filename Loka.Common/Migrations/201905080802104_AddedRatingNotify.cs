namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRatingNotify : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "NotifiedEloRatingPosition", c => c.Int(nullable: false));
            AddColumn("players.PlayerEntity", "Notifications_RatingNextNotifyDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Notifications_RatingNextNotifyDate");
            DropColumn("players.PlayerEntity", "NotifiedEloRatingPosition");
        }
    }
}
