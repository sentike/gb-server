namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoingEveryDayAward : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("award.EveryDayAwardItemEntity", "Level", "award.EveryDayAwardEntity");
            DropForeignKey("award.EveryDayAwardItemEntity", "ModelId", "store.AbstractItemInstance");
            DropIndex("award.EveryDayAwardItemEntity", new[] { "Level" });
            DropIndex("award.EveryDayAwardItemEntity", new[] { "ModelId" });
            CreateTable(
                "players.PlayerEveryDayAwardEntity",
                c => new
                    {
                        PlayerId = c.Guid(nullable: false),
                        JoinCount = c.Short(),
                        JoinDate = c.DateTime(nullable: false),
                        IsNotified = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerId)
                .ForeignKey("award.EveryDayAwardEntity", t => t.JoinCount)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.JoinCount);
            
            AddColumn("award.EveryDayAwardEntity", "RewardCaseId", c => c.Int(nullable: false));
            AddColumn("award.EveryDayAwardEntity", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("award.EveryDayAwardEntity", "AdminComment", c => c.String(maxLength: 256));
            CreateIndex("award.EveryDayAwardEntity", "RewardCaseId");
            AddForeignKey("award.EveryDayAwardEntity", "RewardCaseId", "store.CaseEntity", "EntityId", cascadeDelete: true);
            DropColumn("award.EveryDayAwardEntity", "Money");
            DropColumn("award.EveryDayAwardEntity", "Donate");
            DropTable("award.EveryDayAwardItemEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "award.EveryDayAwardItemEntity",
                c => new
                    {
                        EntityId = c.Short(nullable: false, identity: true),
                        Level = c.Short(nullable: false),
                        AdminComment = c.String(maxLength: 128),
                        Amount = c.Int(nullable: false),
                        ModelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId);
            
            AddColumn("award.EveryDayAwardEntity", "Donate", c => c.Short(nullable: false));
            AddColumn("award.EveryDayAwardEntity", "Money", c => c.Short(nullable: false));
            DropForeignKey("players.PlayerEveryDayAwardEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerEveryDayAwardEntity", "JoinCount", "award.EveryDayAwardEntity");
            DropForeignKey("award.EveryDayAwardEntity", "RewardCaseId", "store.CaseEntity");
            DropIndex("award.EveryDayAwardEntity", new[] { "RewardCaseId" });
            DropIndex("players.PlayerEveryDayAwardEntity", new[] { "JoinCount" });
            DropIndex("players.PlayerEveryDayAwardEntity", new[] { "PlayerId" });
            AlterColumn("award.EveryDayAwardEntity", "AdminComment", c => c.String());
            DropColumn("award.EveryDayAwardEntity", "CreatedDate");
            DropColumn("award.EveryDayAwardEntity", "RewardCaseId");
            DropTable("players.PlayerEveryDayAwardEntity");
            CreateIndex("award.EveryDayAwardItemEntity", "ModelId");
            CreateIndex("award.EveryDayAwardItemEntity", "Level");
            AddForeignKey("award.EveryDayAwardItemEntity", "ModelId", "store.AbstractItemInstance", "ModelId", cascadeDelete: true);
            AddForeignKey("award.EveryDayAwardItemEntity", "Level", "award.EveryDayAwardEntity", "EntityId", cascadeDelete: true);
        }
    }
}
