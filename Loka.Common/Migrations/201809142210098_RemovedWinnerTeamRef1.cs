namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedWinnerTeamRef1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("history.MatchEntity", "WinnerTeamId", "history.MatchTeam");
            DropIndex("history.MatchEntity", new[] { "WinnerTeamId" });
        }
        
        public override void Down()
        {
            CreateIndex("history.MatchEntity", "WinnerTeamId");
            AddForeignKey("history.MatchEntity", "WinnerTeamId", "history.MatchTeam", "TeamId");
        }
    }
}
