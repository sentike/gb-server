namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedHasUsedPremiumAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "HasUsedPremiumAccount", c => c.Boolean(nullable: false));
            AddColumn("history.MatchMember", "HasUsedPremiumAccount", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("history.MatchMember", "HasUsedPremiumAccount");
            DropColumn("matches.MatchMember", "HasUsedPremiumAccount");
        }
    }
}
