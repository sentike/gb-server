namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCases : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerCaseEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        CaseId = c.Int(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastActiveDate = c.DateTime(nullable: false),
                        Amount = c.Int(nullable: false),
                        LastAmount = c.Int(nullable: false),
                        LastAmountHash = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.CaseEntity", t => t.CaseId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.CaseId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "store.CaseEntity",
                c => new
                    {
                        EntityId = c.Int(nullable: false, identity: true),
                        Delay = c.Time(precision: 6),
                        CreatedDate = c.DateTime(nullable: false),
                        AdminComment = c.String(maxLength: 128),
                        RandomMaxDropItems = c.Short(),
                    })
                .PrimaryKey(t => t.EntityId);
            
            CreateTable(
                "store.CaseItemEntity",
                c => new
                    {
                        EntityId = c.Int(nullable: false, identity: true),
                        CaseId = c.Int(nullable: false),
                        ItemModelId = c.Int(nullable: false),
                        RandomMin = c.Short(nullable: false),
                        RandomMax = c.Short(nullable: false),
                        DropMethod = c.Short(nullable: false),
                        Chance = c.Short(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        AdminComment = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.AbstractItemInstance", t => t.ItemModelId, cascadeDelete: true)
                .ForeignKey("store.CaseEntity", t => t.CaseId, cascadeDelete: true)
                .Index(t => t.CaseId)
                .Index(t => t.ItemModelId);
            
            AddColumn("store.AbstractItemInstance", "AdditionalId", c => c.Int());
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerCaseEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerCaseEntity", "CaseId", "store.CaseEntity");
            DropForeignKey("store.CaseItemEntity", "CaseId", "store.CaseEntity");
            DropForeignKey("store.CaseItemEntity", "ItemModelId", "store.AbstractItemInstance");
            DropIndex("store.CaseItemEntity", new[] { "ItemModelId" });
            DropIndex("store.CaseItemEntity", new[] { "CaseId" });
            DropIndex("players.PlayerCaseEntity", new[] { "PlayerId" });
            DropIndex("players.PlayerCaseEntity", new[] { "CaseId" });
            DropColumn("store.AbstractItemInstance", "AdditionalId");
            DropTable("store.CaseItemEntity");
            DropTable("store.CaseEntity");
            DropTable("players.PlayerCaseEntity");
        }
    }
}
