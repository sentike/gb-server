namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedTargetProductId : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.MarketProductEntity", "TargetProductId", c => c.Int());
            CreateIndex("store.MarketProductEntity", "TargetProductId");
            AddForeignKey("store.MarketProductEntity", "TargetProductId", "store.AbstractItemInstance", "ModelId");
        }
        
        public override void Down()
        {
            DropForeignKey("store.MarketProductEntity", "TargetProductId", "store.AbstractItemInstance");
            DropIndex("store.MarketProductEntity", new[] { "TargetProductId" });
            DropColumn("store.MarketProductEntity", "TargetProductId");
        }
    }
}
