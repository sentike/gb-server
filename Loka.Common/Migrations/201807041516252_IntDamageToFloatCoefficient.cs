namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IntDamageToFloatCoefficient : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.ItemModificationEntity", "Coefficient", c => c.Single(nullable: false));
            DropColumn("store.ItemModificationEntity", "Damage");
        }
        
        public override void Down()
        {
            AddColumn("store.ItemModificationEntity", "Damage", c => c.Int(nullable: false));
            DropColumn("store.ItemModificationEntity", "Coefficient");
        }
    }
}
