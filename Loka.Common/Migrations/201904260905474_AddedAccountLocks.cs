namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAccountLocks : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Locked", c => c.Boolean(nullable: false));
            AddColumn("players.PlayerEntity", "LastPlayerEmail", c => c.String(maxLength: 64));
            AddColumn("players.PlayerAccountServiceEntity", "Locked", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerAccountServiceEntity", "Locked");
            DropColumn("players.PlayerEntity", "LastPlayerEmail");
            DropColumn("players.PlayerEntity", "Locked");
        }
    }
}
