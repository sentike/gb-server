namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDefaultItemIdInProfile : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerProfileEntity", "DefaultItemId", c => c.Guid());
            CreateIndex("players.PlayerProfileEntity", "DefaultItemId");
            AddForeignKey("players.PlayerProfileEntity", "DefaultItemId", "players.PlayerProfileItemEntity", "ProfileItemId");
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerProfileEntity", "DefaultItemId", "players.PlayerProfileItemEntity");
            DropIndex("players.PlayerProfileEntity", new[] { "DefaultItemId" });
            DropColumn("players.PlayerProfileEntity", "DefaultItemId");
        }
    }
}
