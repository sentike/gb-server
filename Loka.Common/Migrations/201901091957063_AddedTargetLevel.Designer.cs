// <auto-generated />
namespace Loka.Server.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddedTargetLevel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedTargetLevel));
        
        string IMigrationMetadata.Id
        {
            get { return "201901091957063_AddedTargetLevel"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
