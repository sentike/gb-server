namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsLevelUpNotified : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Experience_IsLevelUpNotified", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Experience_IsLevelUpNotified");
        }
    }
}
