namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGooglePlayUserId : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "GooglePlayPlayerId", c => c.String(maxLength: 32));
            AlterColumn("players.PlayerEntity", "PlayerName", c => c.String(nullable: false, maxLength: 64));
            CreateIndex("players.PlayerEntity", "PlayerName", unique: true, name: "PlayerId_Unique");
            CreateIndex("players.PlayerEntity", "GooglePlayPlayerId", unique: true, name: "GooglePlayPlayerId_Unique");
        }
        
        public override void Down()
        {
            DropIndex("players.PlayerEntity", "GooglePlayPlayerId_Unique");
            DropIndex("players.PlayerEntity", "PlayerId_Unique");
            AlterColumn("players.PlayerEntity", "PlayerName", c => c.String(maxLength: 64));
            DropColumn("players.PlayerEntity", "GooglePlayPlayerId");
        }
    }
}
