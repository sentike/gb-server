namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAchievements : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "player.PlayerAchievementEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        AchievementId = c.Int(nullable: false),
                        LastAchievementPropertyId = c.Int(),
                        PlayerId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        Amount = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.AchievementEntity", t => t.AchievementId, cascadeDelete: true)
                .ForeignKey("store.AchievementPropertyEntity", t => t.LastAchievementPropertyId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => new { t.PlayerId, t.AchievementId }, unique: true, name: "PlayerId_AchievementId")
                .Index(t => t.LastAchievementPropertyId);
            
            CreateTable(
                "store.AchievementEntity",
                c => new
                    {
                        ModelId = c.Int(nullable: false),
                        MethodId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ModelId);
            
            CreateTable(
                "store.AchievementPropertyEntity",
                c => new
                    {
                        AchievementPropertyId = c.Int(nullable: false, identity: true),
                        ModelId = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                        AwardModelId = c.Int(),
                        AwardAmount = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.AchievementPropertyId)
                .ForeignKey("store.AbstractItemInstance", t => t.AwardModelId)
                .ForeignKey("store.AchievementEntity", t => t.ModelId, cascadeDelete: true)
                .Index(t => t.ModelId)
                .Index(t => t.AwardModelId);
            
            CreateTable(
                "matches.MatchMemberAchievement",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        MemberId = c.Guid(nullable: false),
                        AchievementId = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.AchievementEntity", t => t.AchievementId, cascadeDelete: true)
                .ForeignKey("matches.MatchMember", t => t.MemberId, cascadeDelete: true)
                .Index(t => new { t.MemberId, t.AchievementId }, unique: true, name: "MemberId_AchievementId");
            
        }
        
        public override void Down()
        {
            DropForeignKey("matches.MatchMemberAchievement", "MemberId", "matches.MatchMember");
            DropForeignKey("matches.MatchMemberAchievement", "AchievementId", "store.AchievementEntity");
            DropForeignKey("player.PlayerAchievementEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("player.PlayerAchievementEntity", "LastAchievementPropertyId", "store.AchievementPropertyEntity");
            DropForeignKey("player.PlayerAchievementEntity", "AchievementId", "store.AchievementEntity");
            DropForeignKey("store.AchievementPropertyEntity", "ModelId", "store.AchievementEntity");
            DropForeignKey("store.AchievementPropertyEntity", "AwardModelId", "store.AbstractItemInstance");
            DropIndex("matches.MatchMemberAchievement", "MemberId_AchievementId");
            DropIndex("store.AchievementPropertyEntity", new[] { "AwardModelId" });
            DropIndex("store.AchievementPropertyEntity", new[] { "ModelId" });
            DropIndex("player.PlayerAchievementEntity", new[] { "LastAchievementPropertyId" });
            DropIndex("player.PlayerAchievementEntity", "PlayerId_AchievementId");
            DropTable("matches.MatchMemberAchievement");
            DropTable("store.AchievementPropertyEntity");
            DropTable("store.AchievementEntity");
            DropTable("player.PlayerAchievementEntity");
        }
    }
}
