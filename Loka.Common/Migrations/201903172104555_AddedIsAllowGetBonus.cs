namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsAllowGetBonus : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.CaseEntity", "IsAllowGetBonusCase", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.CaseEntity", "IsAllowGetBonusCase");
        }
    }
}
