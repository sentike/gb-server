namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoredServerVersionFlags : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.ServerVersionEntity", "LastUpdateDate", c => c.DateTime(nullable: false));
            AddColumn("public.ServerVersionEntity", "DeleteDate", c => c.DateTime(nullable: false));
            AddColumn("public.ServerVersionEntity", "Flags", c => c.Short(nullable: false));
            DropColumn("public.ServerVersionEntity", "HasPublished");
            DropColumn("public.ServerVersionEntity", "HasDeleted");
        }
        
        public override void Down()
        {
            AddColumn("public.ServerVersionEntity", "HasDeleted", c => c.Boolean(nullable: false));
            AddColumn("public.ServerVersionEntity", "HasPublished", c => c.Boolean(nullable: false));
            DropColumn("public.ServerVersionEntity", "Flags");
            DropColumn("public.ServerVersionEntity", "DeleteDate");
            DropColumn("public.ServerVersionEntity", "LastUpdateDate");
        }
    }
}
