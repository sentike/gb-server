namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "PlayerEmail", c => c.String(maxLength: 40));
            CreateIndex("players.PlayerEntity", "PlayerEmail", unique: true, name: "PlayerEmail_Unique");
        }
        
        public override void Down()
        {
            DropIndex("players.PlayerEntity", "PlayerEmail_Unique");
            DropColumn("players.PlayerEntity", "PlayerEmail");
        }
    }
}
