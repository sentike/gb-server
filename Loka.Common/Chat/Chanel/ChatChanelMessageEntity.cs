﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Player.Models;

namespace Loka.Common.Chat.Chanel
{
    public class ChatChanelMessageEntity
    {
        public Guid MessageId { get; set; }

        //============================================================
        public Guid ChanelId { get; set; }
        public virtual ChatChanelEntity ChanelEntity { get; set; }

        //============================================================
        public Guid MemberId { get; set; }
        public virtual ChatChanelMemberEntity MemberEntity { get; set; }

        //============================================================
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }

        //===========================================
        public class Configuration : EntityTypeConfiguration<ChatChanelMessageEntity>
        {
            public Configuration()
            {
                HasKey(p => p.MessageId);
                ToTable("chat.ChatChanelMessageEntity");
                Property(p => p.Message).IsRequired().HasMaxLength(256);
            }
        }
    }
}
