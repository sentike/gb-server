﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Player.Models;

namespace Loka.Common.Chat.Chanel
{
    [Flags]
    public enum ChatChanelMemberFlags
    {
        None = 0,
        AllowInvite = 1,
        AllowKick = 2,

        Leaved = 4,
    }

    public class ChatChanelMemberEntity
    {
        public Guid MemberId { get; set; }
        public ChatChanelMemberFlags MemberFlags { get; set; }

        //============================================================
        public Guid ChanelId { get; set; }
        public virtual ChatChanelEntity ChanelEntity { get; set; }

        //============================================================
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }
        public DateTime LastActivityDate { get; set; }

        //============================================================
        public virtual ICollection<ChatChanelMessageEntity> MessageEntities { get; } = new List<ChatChanelMessageEntity>(16);


        //===========================================
        public class Configuration : EntityTypeConfiguration<ChatChanelMemberEntity>
        {
            public Configuration()
            {
                HasKey(p => p.MemberId);
                ToTable("chat.ChatChanelMemberEntity");
                HasRequired(p => p.PlayerEntity).WithMany().HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(true);
                HasMany(p => p.MessageEntities).WithRequired(p => p.MemberEntity).HasForeignKey(p => p.MemberId);
            }
        }

    }
}
