﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Chat.Chanel
{
    public enum ChatChanelGroup
    {
        Global,
        Private,
        Squad,
        Match,
        Clan,
    }
    public class ChatChanelEntity
    {
        public Guid ChanelId { get; set; }
        public string ChanelName { get; set; }

        //====================================================
        public Guid? GroupId { get; set; }
        public ChatChanelGroup ChanelGroup { get; set; }

        //====================================================
        public virtual ICollection<ChatChanelMemberEntity> MemberEntities { get; } = new List<ChatChanelMemberEntity>(16);
        public virtual ICollection<ChatChanelMessageEntity> MessageEntities { get; } = new List<ChatChanelMessageEntity>(16);
        
        //===========================================
        public class Configuration : EntityTypeConfiguration<ChatChanelEntity>
        {
            public Configuration()
            {
                HasKey(p => p.ChanelId);
                ToTable("chat.ChatChanelEntity");
                Property(p => p.ChanelName).IsOptional().HasMaxLength(256);
                HasMany(p => p.MemberEntities).WithRequired(p => p.ChanelEntity).HasForeignKey(p => p.ChanelId);
                HasMany(p => p.MessageEntities).WithRequired(p => p.ChanelEntity).HasForeignKey(p => p.ChanelId);
            }
        }
    }
}
