﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Fraction;

namespace Loka.Common.Store
{
    public class BasicItemEntity
    {
        //======================================================
        public int EntityId { get; set; }
        public int DisplayPosition { get; set; }
        public string DisplayName { get; set; }
        public string AdminComment { get; set; }

        //======================================================
        public byte ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }
        public ItemInstanceFlags Flags { get; set; }


        //======================================================
        public FractionTypeId? FractionId { get; set; }
        public short Level { get; set; }

        //======================================================
        public TimeSpan? Duration { get; set; }

        public long AmountDefault { get; set; } = 1;
        public long AmountInStack { get; set; } = 1;
        public long AmountMinimum { get; set; } = 1;
        public long? AmountMaximum { get; set; }

        //======================================================
        public StoreItemCost Cost { get; set; }
        public StoreItemCost SubscribeCost { get; set; }

        //======================================================
        //  Производительность 
        public long? Performance { get; set; } 
        //  Вместимость 
        public long? Storage { get; set; }


        //======================================================
        public class Configuration : EntityTypeConfiguration<BasicItemEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("store.BasicItemEntity");

                Property(p => p.ModelId).IsRequired();
                Property(p => p.CategoryTypeId).IsRequired();

                Property(e => e.ModelId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ModelId_CategoryTypeId") { IsUnique = true, Order = 0}));
                Property(e => e.CategoryTypeId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ModelId_CategoryTypeId") { IsUnique = true, Order = 1 }));

                
                Property(p => p.AdminComment).IsOptional().HasMaxLength(128);
                Property(p => p.DisplayName).IsOptional().HasMaxLength(128);
            }
        }
    }
}
