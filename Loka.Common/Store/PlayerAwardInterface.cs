﻿using Loka.Common.Store.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store
{
    public interface IPlayerAwardInterface
    {
        //===================================
        short EntityId { get; set; }

        //===================================
        int Step { get; }
        int Amount { get; set; }
        int ModelId { get; set; }
    }
}
