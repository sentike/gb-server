﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Exceptions;
using Loka.Common.Player.Profile.Slot;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;

using Loka.Server.Infrastructure.DataRepository;
using VRS.Infrastructure;
using Loka.Common.Transaction;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Common.Cases;

namespace Loka.Common.Store.Abstract
{
    public enum ItemMigrationAction
    {
        None,
        RemoveIfExist,
        CreateIfNotExist
    }

    [Flags]
    public enum ItemInstanceFlags
    {
        None,
        Hidded = 1,
        Subscribe = 2,
        AllowSome = 4,
        IgnoreRequirements = 8,

        Product = 16,
        Service = 32,
        Currency = 64,
        Case = 128,
    }

    public class ItemModificationModel
    {
        //================================================
        public StoreItemCost Cost { get; set; }
        public float Coefficient { get; set; }

        public int Position { get; set; }
        public int Level { get; set; }

        public ItemModificationModel() { }
        public ItemModificationModel(ItemModificationEntity entity)
        {
            Cost = entity.Cost;
            Coefficient = entity.Coefficient;
            Position = entity.Position;
            Level = entity.Level;
        }

    }

    public class ItemModificationEntity
    {
        public int EntityId { get; set; }

        //================================================
        public int ModelId { get; set; }
        public virtual ItemInstanceEntity ModelEntity { get; set; }

        //================================================
        public StoreItemCost Cost { get; set; } = new StoreItemCost();
        public float Coefficient { get; set; }
        public int Position { get; set; }
        public int Level { get; set; }


        //================================================
        public class Configuration : EntityTypeConfiguration<ItemModificationEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("store.ItemModificationEntity");                
            }
        }
    }

    public class ItemInstanceEntity
	{
        public int ModelId { get; set; }
        public StoreItemCost Cost { get; set; }
        public string AdminComment { get; set; }
        public int DisplayPosition { get; set; }
        public short Level { get; set; }
        public long Amount { get; set; }

        public int? AdditionalId { get; set; }


        public long SafeAmount => Math.Min(Math.Max(1, Amount), 1000);

        public ItemInstanceFlags Flags { get; set; }

        public virtual List<ItemModificationEntity> Modifications { get; set; } = new List<ItemModificationEntity>(5);
        public ItemMigrationAction MigrationAction { get; set; }

	    public bool IsAllowSome => Flags.HasFlag(ItemInstanceFlags.AllowSome);
	    public bool HasIgnoreRequirements => Flags.HasFlag(ItemInstanceFlags.IgnoreRequirements);
        
        public bool IsService => Flags.HasFlag(ItemInstanceFlags.Service);
	    public bool IsCurrency => Flags.HasFlag(ItemInstanceFlags.Currency);
	    public bool IsProduct => Flags.HasFlag(ItemInstanceFlags.Product);
	    public bool IsCase => Flags.HasFlag(ItemInstanceFlags.Case);


        public ItemInstanceEntity() { }

        public ItemInstanceEntity(ItemInstanceEntity instance)
        {
            ModelId = instance.ModelId;
            Level = instance.Level;
            Cost = instance.Cost;
        }

        public class Configuration : EntityTypeConfiguration<ItemInstanceEntity>
        {
            public Configuration()
            {
                HasKey(p => p.ModelId);
                ToTable("store.AbstractItemInstance");
                Property(p => p.ModelId).IsRequired();
                Property(p => p.AdminComment).IsOptional().HasMaxLength(128);
                HasMany(p => p.Modifications).WithRequired(p => p.ModelEntity).HasForeignKey(p => p.ModelId).WillCascadeOnDelete(true);
			}
		}

        /// <summary>
        /// Выдать предмет игроку.
        /// <para>Выдает монеты из <see cref="Cost"/> , если  <see cref="IsCurrency"/> = true </para>
        /// <para>Выдает премиум аккаует из <paramref name="InCount"/>, если <see cref="IsService"/> = true. <paramref name="InCount"/> - количество дней  </para>
        /// <para>Выдает несколько предметов, <see cref="IsAllowSome"/> = true  </para>
        /// </summary>
        /// <param name="InPlayer">Игрок</param>
        /// <param name="InCount">Количество</param>
        public bool Give(PlayerEntity InPlayer, long InCount, out PlayerInventoryItemEntity OutItem, bool InUseSafeAmountAsMultipler = true)
        {
            OutItem = null;
                
            if (IsCurrency)
            {
                InPlayer.Give( new StoreItemCost(InCount, (GameCurrency)AdditionalId));
            }
            else if (IsService)
            {
                var days = InCount;
                InPlayer.GivePremiumAccount(days);
            }
            else if (IsCase)
            {
                //-----------------------------------------------------
                if (AdditionalId == null)
                {
                    throw new ArgumentNullException(nameof(AdditionalId), "CaseId (AdditionalId) was nullptr");
                }

                //-----------------------------------------------------
                var PlayerCase = InPlayer.CaseEntities.SingleOrDefault(p => p.CaseId == AdditionalId);
                if (PlayerCase == null)
                {
                    InPlayer.CaseEntities.Add(PlayerCase = PlayerCaseEntity.Factory(InPlayer, AdditionalId.Value, 0));
                }
                   
                //-----------------------------------------------------
                PlayerCase.GiveAmount((int)SafeAmount);

                //-----------------------------------------------------
                OutItem = new PlayerInventoryItemEntity(InPlayer.PlayerId, ModelId, PlayerCase.Amount);
            }
            else
            {
                OutItem = InPlayer.InventoryItemList.SingleOrDefault(p => p.ModelId == ModelId);
                if (OutItem == null)
                {
                    //---------------------------------
                    long Amount = InCount;

                    //---------------------------------
                    if (InUseSafeAmountAsMultipler)
                    {
                        Amount *= SafeAmount;
                    }

                    //---------------------------------
                    InPlayer.InventoryItemList.Add(OutItem = new PlayerInventoryItemEntity(InPlayer.PlayerId, this, Amount));
                }
                else if (IsAllowSome)
                {                   
                    //---------------------------------
                    long Amount = InCount;

                    //---------------------------------
                    if (InUseSafeAmountAsMultipler)
                    {
                        Amount *= SafeAmount;
                    }

                    //---------------------------------
                    OutItem.Amount += Amount;
                }
                else return false;
            }
            return true;
        }

        public long Bought(PlayerEntity InPlayer, long InCount)
	    {
            //===================================================
	        if (IsAllowSome)
	        {
	            var avalible = InPlayer.Count(Cost);
	            var bought = Math.Min(avalible, InCount);
	            return bought;
	        }

	        //===================================================
            else if (InPlayer.IsAllowPay(Cost))
	        {
	            return 1;
	        }

	        //===================================================
            return 0;
        }
	}
}
