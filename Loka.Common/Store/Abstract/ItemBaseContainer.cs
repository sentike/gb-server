﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loka.Common.Award.EveryDayAward;
using Loka.Common.Award.PlayerLevelAward;
using Loka.Common.Cases;
using Loka.Common.Payments;
using Newtonsoft.Json;


namespace Loka.Common.Store.Abstract
{
    public class CaseItemContainer
    {
        public int EntityId { get; set; }

        public int ItemModelId { get; set; }
        public short RandomMin { get; set; }
        public short RandomMax { get; set; } 

        public CaseItemDropMethod DropMethod { get; set; }

        //  Шанс выпадениея на 10000000
        public short Chance { get; set; } 

        public CaseItemContainer() { }

        public CaseItemContainer(CaseItemEntity entity)
        {
            EntityId = entity.EntityId;
            ItemModelId = entity.ItemModelId;
            RandomMin = entity.RandomMin;
            RandomMax = entity.RandomMax;
            DropMethod = entity.DropMethod;
            Chance = entity.Chance;
        }

    }

    public class CaseContainer
    {
        public int EntityId { get; set; }
        public int Dalay { get; set; }
        public bool IsAllowGetBonusCase { get; set; }

        public List<CaseItemContainer> Items { get; set; }

        public CaseContainer() { }

        public CaseContainer(CaseEntity entity)
        {
            EntityId = entity.EntityId;
            IsAllowGetBonusCase = entity.IsAllowGetBonusCase;
            Dalay = Convert.ToInt32(entity.Delay?.TotalSeconds ?? 0.0);
            Items = entity.ItemEntities.Select(p => new CaseItemContainer(p)).ToList();
        }

    }

    public class PlayerAwardItemContainer
    {
        public int ModelId { get; set; }
        public int Amount { get; set; }

        public PlayerAwardItemContainer(int InModelId, int InAmount)
        {
            ModelId = InModelId;
            Amount = InAmount;
        }
    }

    public class FLevelRewardContainer
    {
        public PlayerAwardItemContainer[] Items { get; set; }
        public short Donate { get; set; }
        public short Money { get; set; }

    }

    public class FEveryDayAwardContainer
    {
        public int EntityId { get; set; }
        public int RewardCaseId { get; set; }

        public FEveryDayAwardContainer(EveryDayAwardEntity InAwardEntity)
        {
            EntityId = InAwardEntity.EntityId;
            RewardCaseId = InAwardEntity.RewardCaseId;
        }
    }

    public class FLevelAwardContainer
    {
        public int EntityId { get; set; }
        public FLevelRewardContainer Reward { get; set; }

        public FLevelAwardContainer(PlayerLevelAwardEntity entity)
        {
            EntityId = entity.EntityId;
            Reward = new FLevelRewardContainer
            {
                Money = entity.Money,
                Donate = entity.Donate,
                Items = entity.ItemEntities.Select(p => new PlayerAwardItemContainer(p.ModelId, p.Amount)).ToArray()
            };
        }
    }

    public class ItemBaseContainer
    {
        public ItemBaseContainer(ItemInstanceEntity entity)
        {
            ModelId = entity.ModelId;
			Level = entity.Level;
            Cost = entity.Cost;
            Flags = entity.Flags;

            if (entity.Modifications.Any())
            {
                Modifications = entity.Modifications.Select(p => new ItemModificationModel(p)).ToList();
            }

            AdditionalId = entity.AdditionalId;
            AmountInProduct = entity.Amount;


            if (entity.DisplayPosition == 0)
            {
                DisplayPosition = null;
            }
            else
            {
                DisplayPosition = entity.DisplayPosition;
            }
        }

        public int ModelId { get; set; }
        public int Level { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? AdditionalId { get; set; }
        public long AmountInProduct { get; set; }


        public StoreItemCost Cost { get; set; }
        public ItemInstanceFlags Flags { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ItemModificationModel> Modifications { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? DisplayPosition { get; set; }
    }
}
