﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Version
{
    public class GameVersionModel
    {
        public long StoreVersionId { get; set; }
        public long ServerVersionId { get; set; }
        public DeployTarget DeployTarget { get; set; }
    }

    public class GameVersionPublishModel : GameVersionModel
    {
        public bool DeprecatePreviewVersions { get; set; }
        public string AdminComment { get; set; }
    }

    public class ServerVersionPublishModel
    {
        public long ServerVersionId { get; set; }
        public bool DeprecatePreviewVersions { get; set; }
        public DeployTarget DeployTarget { get; set; }
        public string AdminComment { get; set; }
    }

}
