﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Version
{
    public enum DeployTarget : short
    {
        None,

        Internal,
        Alpha,
        Beta,
        Production,
    }

    public class GameVersionEntity
    {
        public long EntityId { get; set; }
        public long ServerVersionId { get; set; }
        public DeployTarget DeployTarget { get; set; }
        public DateTime CreatedDate { get; set; }
        public string AdminComment { get; set; }
        public bool HasPublished { get; set; }

        public bool HasAvalibleByDeliveryTime
        {
            get
            {
                if (DeployTarget == DeployTarget.Production)
                {
                    return DateTime.UtcNow >= CreatedDate + TimeSpan.FromHours(4);
                }
                return DateTime.UtcNow >= CreatedDate + TimeSpan.FromMinutes(10);
            }
        }

        public class Configuration : EntityTypeConfiguration<GameVersionEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId).Property(p => p.EntityId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
                ToTable("public.GameVersionEntity");
                Property(p => p.AdminComment).HasMaxLength(128);
            }
        }
    }

    [Flags]
    public enum ServerVersionFlags : short
    {
        None,

        Published = 1,
        Deleted = 2,
        Updated = 4,
        Updating = 8,
    }

    public class ServerVersionEntity
    {
        public long EntityId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DateTime DeleteDate { get; set; }

        public DeployTarget DeployTarget { get; set; }
        public ServerVersionFlags Flags { get; set; }

        public bool HasPublished
        {
            get { return Flags.HasFlag(ServerVersionFlags.Published); }
            set
            {
                if (value)
                {
                    Flags |= ServerVersionFlags.Published;
                }
                else
                {
                    Flags &= ~ServerVersionFlags.Published;
                }
            }
        }

        public bool HasDeleted
        {
            get { return Flags.HasFlag(ServerVersionFlags.Deleted); }
            set
            {
                if (value)
                {
                    Flags |= ServerVersionFlags.Deleted;
                }
                else
                {
                    Flags &= ~ServerVersionFlags.Deleted;
                }
            }
        }      
        
        public bool HasUpdated
        {
            get { return Flags.HasFlag(ServerVersionFlags.Updated); }
            set
            {
                if (value)
                {
                    Flags |= ServerVersionFlags.Updated;
                }
                else
                {
                    Flags &= ~ServerVersionFlags.Updated;
                }
            }
        }

        public bool HasUpdating
        {
            get { return Flags.HasFlag(ServerVersionFlags.Updating); }
            set
            {
                if (value)
                {
                    Flags |= ServerVersionFlags.Updating;
                }
                else
                {
                    Flags &= ~ServerVersionFlags.Updating;
                }
            }
        }

        public string AdminComment { get; set; }

        public class Configuration : EntityTypeConfiguration<ServerVersionEntity>
        {
            public Configuration()
            {
                ToTable("public.ServerVersionEntity");
                HasKey(p => p.EntityId).Property(p => p.EntityId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
                Property(p => p.AdminComment).HasMaxLength(128);
                Ignore(p => p.HasPublished);
                Ignore(p => p.HasUpdated);
                Ignore(p => p.HasDeleted);
                Ignore(p => p.HasUpdating);
            }
        }
    }

}
