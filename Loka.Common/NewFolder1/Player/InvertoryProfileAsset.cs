﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Player
{
    public interface IAbstractPlayerProfile
    {
        string Name { get; }

        bool IsEnabled { get; }

        Guid AssetId { get; }

        Guid PlayerId { get; }


        Guid CharacterId { get; }


        //Guid? HelmetId { get; }
        //
        //Guid? MaskId { get; }
        //
        //Guid? ArmourId { get; }
        //
        //Guid? BackpackId { get; }
        //
        //Guid? GlovesId { get; }
        //
        //Guid? PantsId { get; }
        //
        //Guid? BootsId { get; }
        //
        //Guid? PrimaryWeaponId { get; }
        //InvertoryProfileWeaponAmmunation PrimaryWeaponAmmo { get; }
        //
        //
        //Guid? SecondaryWeaponId { get; }
        //InvertoryProfileWeaponAmmunation SecondaryWeaponAmmo { get; }
    }
}
