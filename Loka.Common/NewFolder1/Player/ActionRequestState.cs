﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Player
{
    public enum ActionPlayerRequestState
    {
        Waiting,
        Confirmed,
        ConfirmedWaiting,
        Rejected,
        Blocked,
        None,
    }
}
