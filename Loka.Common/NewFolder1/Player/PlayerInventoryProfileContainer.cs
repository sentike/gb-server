﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Profile.Slot;
using Loka.Infrastructure;

namespace Loka.Player
{
    public struct PlayerInventoryItemContainer
    {
        public PlayerInventoryItemContainer(Guid itemId, PlayerProfileInstanceSlotId slotId)
        {
            ItemId = itemId;
            SlotId = slotId;
        }

        public Guid ItemId { get; set; }
        public PlayerProfileInstanceSlotId SlotId { get; set; }
    }

    public class PlayerProfileEntityContainer : IAbstractPlayerProfile
    {
        public string Name { get; set; }
        public bool IsEnabled { get; set; }

        public Guid AssetId { get; set; }
        public Guid PlayerId { get; set; }
        public Guid CharacterId { get; set; }

        public IEnumerable<PlayerInventoryItemContainer> Items { get; set; }
        public IEnumerable<Guid> ItemIds => Items.Select(i => i.ItemId).ToArray();

        public PlayerProfileEntityContainer(IAbstractPlayerProfile profile)
        {
            Name = profile.Name;
            IsEnabled = true;
            //IsEnabled = profile.IsEnabled;
            AssetId = profile.AssetId;
            PlayerId = profile.PlayerId;
            CharacterId = profile.CharacterId;
        }

        //public InvertoryProfileWeaponAmmunation PrimaryWeaponAmmo { get; set; }
        //public InvertoryProfileWeaponAmmunation SecondaryWeaponAmmo { get; set; }


    }
}
