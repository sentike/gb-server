﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using VRS.Infrastructure;

namespace Loka.Infrastructure
{
    public static class CMath
    {
        public static readonly Random random = new Random();
        public static readonly Regex EmailRegex = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public static DateTime AddNightSafeTime(this DateTime dt, TimeSpan InValue)
        {
            var TargetDate = dt.Add(InValue);
            //if(TargetDate.Hour >= 22)
            //{
            //    //  7 утра
            //    TargetDate = TargetDate.AddHours(11).AddSeconds(InValue.TotalSeconds / 2);
            //}
            //else if(TargetDate.Hour < 7)
            //{
            //    return TargetDate = TargetDate.AddHours(6).AddSeconds(InValue.TotalSeconds / 2);
            //}
            return TargetDate;
        }

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }

        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            if (val.CompareTo(max) > 0) return max;
            return val;
        }

        public static bool IsPowerOfTwo(this uint val)
        {
            return val != 0 && (val & (val - 1)) == 0;
        }

        public static double GetRandomNumber(double minimum, double maximum)
        {
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public static Int32 UnixTime() => (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

        public static long ToUnixTime(this DateTime time) => (long)(time.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

        public static long ToUnixTime(this DateTime? time)
        {
            if (time == null || time.HasValue == false)
            {
                return 0;
            }
            return ToUnixTime(time.Value);
        }

        public static Int64 ToSecondsTime(this DateTime time) => Convert.ToInt64((time - DateTime.UtcNow).TotalSeconds);

        public static Int64 ToElapsedSeconds(this DateTime? time) => Math.Abs(Convert.ToInt64(((time ?? DateTime.UtcNow) - DateTime.UtcNow).TotalSeconds));

        public static Int64 ToElapsedSeconds(this DateTime time) => Math.Abs(Convert.ToInt64((time - DateTime.UtcNow).TotalSeconds));
        public static Int64 ToLocalElapsedSeconds(this DateTime time) => Math.Abs(Convert.ToInt64((time - DateTime.Now).TotalSeconds));

        //  Нужно       сейчас
        //  12:00:00  - 14:00:00 = -2:00:00 | -2:00:00 > 1:00:00 - false
        //  14:00:00  - 12:00:00 = 2:00:00 | 2:00:00 > 1:00:00 - true

        public static bool ElapsedMore(this DateTime time, TimeSpan InElapsed)
        {
            var elapsed = TimeSpan.FromSeconds(-(time - DateTime.UtcNow).TotalSeconds);
            LoggerContainer.AccountLogger.Warn($"[ElapsedMore][{time.ToShortTimeString()} - {DateTime.UtcNow.ToShortTimeString()}][{elapsed} >= {InElapsed}][{elapsed >= InElapsed}]");

            return elapsed >= InElapsed;
        }

        public static bool IsElapsed(this DateTime time, TimeSpan elapsed) => (time + elapsed ) - DateTime.UtcNow <= TimeSpan.Zero;
        public static bool IsElapsed(this DateTime time, TimeSpan? elapsed, bool @default = true)
        {
            if (elapsed == null)
            {
                return @default;
            }
            return time.IsElapsed(elapsed.Value);
        }


        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(unixTimeStamp);
        }


        public static string GetHash(this HashAlgorithm hash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

    }
}