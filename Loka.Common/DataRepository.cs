﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Reflection;
using System.Web;
using InteractivePreGeneratedViews;
using Loka.Common.Achievement;
using Loka.Common.Cases;
using Loka.Common.Chat.Chanel;
using Loka.Common.Cluster;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Common.Payments;
using Loka.Common.Player.Instance;
using Loka.Common.Player.Profile.Item;
using Loka.Common.Player.Profile.Profile;
using Loka.Common.Player.Statistic;



using Loka.Server.Models.Queue;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;


using Loka.Common.Store;
using Loka.Common.Store.Abstract;


using Loka.Common.Transaction;
using Loka.Common.Tutorial;
using Loka.Common.Version;
using VRS.Infrastructure;
using Loka.Common.Award.PlayerLevelAward;
using Loka.Common.Award.EveryDayAward;
using Loka.Common.Player;

namespace Loka.Server.Infrastructure.DataRepository
{

    public class DataRepository : DbContext
    {
        static DataRepository()
        {
            try
            {
                using (var db = new DataRepository())
                {
                    var file = new FileViewCacheFactory($"{HttpRuntime.AppDomainAppPath}/Content/Cache.xml");
                    InteractiveViews.SetViewCacheFactory(db, file);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public DataRepository()
            : base("name=Loka.Server"/*, throwIfV1Schema: false*/)
        {
           //Configuration.AutoDetectChangesEnabled = true;
           //Configuration.ProxyCreationEnabled = true;
           //Configuration.LazyLoadingEnabled = true;
           // Configuration.ValidateOnSaveEnabled = false;
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                SaveChanges();
            }
            catch (Exception e)
            {
                LoggerContainer.DataBaseLogger.WriteExceptionMessage($"[DataRepository][Dispose][StackTrace: {Environment.StackTrace}]", e);
            }

            base.Dispose(disposing);
        }

        public DbSet<GameVersionEntity> GameVersionEntities { get; set; }
        public DbSet<ServerVersionEntity> ServerVersionEntities { get; set; }

        

        public DbSet<ChatChanelEntity> ChatChanelEntities { get; set; }
        public DbSet<ChatChanelMemberEntity> ChatChanelMemberEntities { get; set; }
        public DbSet<ChatChanelMessageEntity> ChatChanelMessageEntities { get; set; }


        //==============================================================================================================================================================================
        //  Improvements
        //==============================================================================================================================================================================
        public DbSet<MatchTransactionEntity> MatchTransactionEntities { get; set; }
        public DbSet<OrderTransactionEntity> OrderTransactionEntities { get; set; }
        public DbSet<StoreTransactionEntity> StoreTransactionEntities { get; set; }

        public DbSet<PlayerPaymentEntity> PlayerPaymentEntities { get; set; }
        public DbSet<MarketProductEntity> MarketProductEntities { get; set; }

        public DbSet<EveryDayAwardEntity> EveryDayAwardEntities { get; set; }
        public DbSet<PlayerLevelAwardEntity> PlayerLevelAwardEntities { get; set; }



        public readonly object SyncRoot = new object();
        public DbSet<MatchEntity> MatchEntity { get; set; }
        public DbSet<MatchHistoryEntity> MatchHistoryEntity { get; set; }

        public DbSet<MatchMemberEntity> MatchMember { get; set; }
        public DbSet<MatchMemberHistoryEntity> MatchMemberHistoryEntities { get; set; }


        public DbSet<MatchTeamEntity> MatchTeam { get; set; }

        public DbSet<PlayerGameSessionEntity> GameSessionEntity { get; set; }

        public DbSet<ClusterInstance> ClusterInstance { get; set; }


        public DbSet<QueueEntity> QueueEntity { get; set; }
        public DbSet<SquadInviteEntity> SquadInviteEntities { get; set; }

        //public DbSet<QueueMember> QueueMember { get; set; }


        public DbSet<GameMapEntity> GameMapEntity { get; set; }
        public DbSet<GameModeEntity> GameModeEntity { get; set; }


        //==============================================================================================================================================================================
        //  Store
        //==============================================================================================================================================================================
        public DbSet<ItemInstanceEntity> StoreItemInstance { get; set; }

        //==============================================================================================================================================================================
        //                                  [ Player ]
        //==============================================================================================================================================================================

        public DbSet<TutorialInstanceEntity> TutorialInstanceEntities { get; set; }
        public DbSet<TutorialStepEntity> TutorialStepEntities { get; set; }

        public DbSet<PlayerCashEntity> PlayerCashEntities { get; set; }
        public DbSet<PlayerCaseEntity> PlayerCaseEntities { get; set; }

        public DbSet<PlayerEntity> AccountPlayerEntity { get; set; }
        public DbSet<PlayerEveryDayAwardEntity> PlayerEveryDayAwardEntity { get; set; }

        public DbSet<PlayerAccountServiceEntity> PlayerAccountServiceEntities { get; set; }



        public DbSet<PlayerFriendEntity> PlayerFriendEntity { get; set; }
        public DbSet<PlayerProfileItemEntity> PlayerProfileItem { get; set; }


        public DbSet<CaseEntity> CaseEntities { get; set; }




        public DbSet<AchievementEntity> AchievementEntities { get; set; }
        public DbSet<AchievementPropertyEntity> AchievementPropertyEntities { get; set; }


        //public DbSet<PlayerProfileEntity> AccountPlayerPreSetEntity { get; set; }
        public DbSet<PlayerInventoryItemEntity> PlayerInventortItem { get; set; }
        public DbSet<PlayerTutorialEntity> PlayerTutorialEntities { get; set; }


        public static DataRepository Create()
        {
            return new DataRepository();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(DataRepository)));
        }

        //public override int SaveChanges()
        //{
        //    foreach (var entry in ChangeTracker.Entries<IVersionedEntity>().ToList())
        //    {
        //        if (entry.State == EntityState.Unchanged)
        //        {
        //            continue;
        //        }
        //        entry.Entity.RowVersion = Guid.NewGuid();
        //    }
        //
        //    return base.SaveChanges();
        //}
    }

}
