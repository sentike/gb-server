﻿using Loka.Server.Player.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Player
{
    public enum EAccountService : short
    {
        None,
        Google,
        Facebook,
        Vkontakte,
        END,
    };

    public class PlayerAccountServiceEntity
    {
        //=======================================
        public Guid EntityId { get; set; }
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }


        //=======================================
        public EAccountService Service { get; set; }
        public string Token { get; set; }
        public string Login { get; set; }

        //=======================================
        public DateTime CreatedDate { get; set; }

        //=======================================
        public bool Locked { get; set; }

        public class Configuration : EntityTypeConfiguration<PlayerAccountServiceEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("players.PlayerAccountServiceEntity");
                Property(p => p.Token).HasMaxLength(256);
                Property(p => p.Login).HasMaxLength(64);
            }
        }
    }
}
