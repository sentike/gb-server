﻿using Loka.Server.Player.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Achievement;

namespace Loka.Common.Player
{
    public class PlayerAchievementEntity
    {
        public Guid EntityId { get; set; }

        public AchievementModelId AchievementId { get; set; }
        public virtual AchievementEntity AchievementEntity { get; set; }

        public int? LastAchievementPropertyId { get; set; }
        public virtual AchievementPropertyEntity LastAchievementProperty { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }



        public DateTime CreatedDate { get; set; }
        public long Amount { get; set; }

        public PlayerAchievementEntity() { }

        public PlayerAchievementEntity(AchievementModelId model, long amount)
        {
            EntityId = Guid.NewGuid();
            CreatedDate = DateTime.UtcNow;           
            AchievementId = model;
            Amount = amount;
        }

        public void UpdateValue(long? value)
        {
            if (AchievementEntity != null)
            {
                UpdateValue(value, AchievementEntity.MethodId);
            }
        }

        public void UpdateValue(long? value, bool InOverrideMethod, AchievementMethodId method)
        {
            if (InOverrideMethod)
            {
                UpdateValue(value, method);
            }
            else if (AchievementEntity == null)
            {
                UpdateValue(value, method);
            }
            else
            {
                UpdateValue(value, AchievementEntity.MethodId);
            }
        }

        public void UpdateValue(long? value, AchievementMethodId method)
        {
            if (method == AchievementMethodId.Incrementable)
            {
                Amount += (value ?? 0);
            }
            else if (method == AchievementMethodId.Settable)
            {
                Amount = (value ?? 0);
            }
        }


        public class Configuration : EntityTypeConfiguration<PlayerAchievementEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("player.PlayerAchievementEntity");
                HasRequired(p => p.AchievementEntity).WithMany().HasForeignKey(p => p.AchievementId);
                HasOptional(p => p.LastAchievementProperty).WithMany().HasForeignKey(p => p.LastAchievementPropertyId);


                Property(p => p.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_AchievementId", 0) { IsUnique = true }));
                Property(p => p.AchievementId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_AchievementId", 1) { IsUnique = true }));

            }
        }

    }
}
