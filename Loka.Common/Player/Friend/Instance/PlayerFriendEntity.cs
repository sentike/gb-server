﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics.CodeAnalysis;
using Loka.Infrastructure;
using Loka.Player;


namespace Loka.Server.Player.Models
{
    [Flags]
    public enum PlayerFriendStatus
    {
        None,

        Offline = 1,
        Online = 2,
        SearchGame = 4,
        PlayGame = 8,

        Ready = 16,
        Leader = 32,
        Global = 64,
    }

    public struct PlayerFriend
    {
        public readonly Guid PlayerId;
        public readonly string Name;
        public readonly PlayerFriendStatus Status;
        public readonly long LastActivityDate;
        public readonly ActionPlayerRequestState State;

        public PlayerFriend(PlayerEntity player, bool isOnlineList = false)
        {
            PlayerId = player.PlayerId;
            Name = player.PlayerName;

            Status = player.Status();

            LastActivityDate = player.LastActivityDate.ToElapsedSeconds();
            State = ActionPlayerRequestState.None;

            if(isOnlineList) Status ^= PlayerFriendStatus.Global;
        }

        public PlayerFriend(PlayerFriendEntity player) 
            : this(player.FriendEntity)
        {
            State = player.State;

        }
    }



    [SuppressMessage("ReSharper", "ClassWithVirtualMembersNeverInherited.Global")]
    public class PlayerFriendEntity
    {
        public Guid MemberId { get; set; }


        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }


        public Guid FriendId { get; set; }
        public virtual PlayerEntity FriendEntity { get; set; }
        public ActionPlayerRequestState State { get; set; }


        public override string ToString()
        {
            return $"{FriendEntity.PlayerName} friend for {PlayerEntity.PlayerName}, {State}";
        }

        public static PlayerFriendEntity Factory(PlayerEntity player, PlayerEntity friend, ActionPlayerRequestState state)
        {
            return new PlayerFriendEntity
            {
                MemberId = Guid.NewGuid(),
                PlayerId = player.PlayerId,
                FriendId = friend.PlayerId,
                State = state
            };
        }

        public class Configuration : EntityTypeConfiguration<PlayerFriendEntity>
        {
            public Configuration()
            {
                HasKey(p => p.MemberId);
                ToTable("players.FriendEntity");
                HasRequired(p => p.PlayerEntity).WithMany(p=> p.FriendList).HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(true);
                HasRequired(p => p.FriendEntity).WithMany().HasForeignKey(p => p.FriendId).WillCascadeOnDelete(true);
            }
        }
    }
}
