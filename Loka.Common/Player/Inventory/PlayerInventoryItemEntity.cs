﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using JetBrains.Annotations;

using System.Data.Entity.Infrastructure.Annotations;
using Loka.Common.Store.Abstract;
using Loka.Server.Player.Models;


namespace Loka.Common.Player.Inventory
{
    public class PlayerInventoryItemEntity 
    {
        public PlayerInventoryItemEntity() { }
        public PlayerInventoryItemEntity(Guid playerId, ItemInstanceEntity entity, long amount)
        {
            ModelId = entity.ModelId;
            PlayerId = playerId;
            ItemId = Guid.NewGuid();
            PurchaseDate = DateTime.UtcNow;
            Amount = amount;
        }

        public PlayerInventoryItemEntity(Guid playerId, int model, long amount)
        {
            ModelId = model;
            PlayerId = playerId;
            ItemId = Guid.NewGuid();
            PurchaseDate = DateTime.UtcNow;
            Amount = amount;
        }


        /// <summary>
        /// The unique identifier in the object database
        /// </summary>
        public Guid ItemId { get; set; }
     
        /// <summary>
        /// Object Identifier on the client
        /// </summary>
        public int ModelId { get; set; }
        public short Level { get; set; }
        public short NextLevel => Convert.ToInt16(Level + 1);

        public long Amount { get; set; }

        /// <summary>
        /// Date when the object was purchased
        /// </summary>
        public DateTime PurchaseDate { get; set; }
       
        /// <summary>
        /// Date the last time the object was used. (The last time I wore a character)
        /// </summary>
        public DateTime? LastUseDate { get; set; }
        
        public enum UpgradeStatus
        {
            None,
            Successfully,
            NotEnouthMoney,
            MaximumLevelReached,
            RequestedInvalidLevel,
        }

        public UpgradeStatus Upgrade(int InLevel)
        {
            //=============================
            if (InLevel != NextLevel)
            {
                return UpgradeStatus.RequestedInvalidLevel;
            }

            //=============================
            var next = ItemModelEntity.Modifications.FirstOrDefault(p => p.Position == NextLevel);

            //=============================
            if(next == null)
            {
                return UpgradeStatus.MaximumLevelReached;
            }

            //=============================
            if (PlayerInstance.Pay(next.Cost))
            {
                Level++;
            }
            else
            {
                return UpgradeStatus.NotEnouthMoney;
            }

            //=============================
            return UpgradeStatus.Successfully;
        }

 

        public virtual ItemInstanceEntity ItemModelEntity { get; set; }

        /// <summary>
        /// Player Identifier owner
        /// </summary>
        public Guid PlayerId { get; set; }

        /// <summary>
        /// Player instance
        /// </summary>
        public virtual PlayerEntity PlayerInstance { get; set; }


        public class Configuration : EntityTypeConfiguration<PlayerInventoryItemEntity>
        {
            public Configuration()
            {
                ToTable("players.InstanceOfPlayerItem");
                HasKey(p => p.ItemId);

                HasRequired(p => p.PlayerInstance).WithMany(i => i.InventoryItemList).HasForeignKey(f => f.PlayerId);
                HasRequired(i => i.ItemModelEntity).WithMany().HasForeignKey(i => i.ModelId);

                Property(x => x.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_instanceofplayerItem_itemsingleton", 1){IsUnique = true}));
                Property(x => x.ModelId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_instanceofplayerItem_itemsingleton", 2) { IsUnique = true }));
            }
        }

        public long Use(int InUsed)
        {
            //====================
            if (Amount > 0)
            {
                Amount = Math.Max(0, Amount - InUsed);
            }

            //====================
            return Amount;
        }
    }
}