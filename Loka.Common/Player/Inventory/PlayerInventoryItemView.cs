﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Player.Inventory
{
    public class PlayerInventoryItemView
    {
        public Guid EntityId { get; set; }
        public short ModelId { get; set; }
        //public IEnumerable<AddonContainer> AvalibleAddons { get; set; }
        //public IEnumerable<InstalledAddonContainer> InstalledAddons { get; set; }
    }
}
