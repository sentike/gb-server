﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Web;
using Loka.Common.Cluster;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.GameMode;
using Loka.Common.Store;
using Loka.Infrastructure;
using Loka.Server.Models.Queue;

namespace Loka.Server.Infrastructure.Session
{
    /// <summary>
    /// Параметры игровой сессии, такие как
    /// <para>
    ///  <see cref="GameMap"/> - Игровая локация
    /// </para> 
    /// <para>
    ///  <see cref="GameMode"/> - Режим игры
    /// </para> 
    /// <para>
    ///  <see cref="Region"/> - Регион сервера
    /// </para> 
    /// <para>
    ///  <see cref="Bet"/> - Ставка для дуэли
    /// </para> 
    /// <para>
    ///  <see cref="WeaponType"/> - Режим игры
    /// </para> 
    /// <para>
    ///  <see cref="Difficulty"/> - Уровень сложности ботов, если они есть
    /// </para> 
    /// <para>
    ///  <see cref="NumberOfLives"/> - количество жизней для дуэли
    /// </para> 
    /// </summary>
    [ComplexType]
    public class SearchSessionOptions
    {
        public static SearchSessionOptions Default { get; } = new SearchSessionOptions
        {
            GameMode = GameModeFlags.None,
            Region = ClusterRegionId.None,
            Bet = new StoreItemCost(0, GameCurrency.Money),
            AdditionalFlag = 0,
            GameMap = GameMapFlags.End,
            Difficulty = 0,
            RoundTimeInSeconds = 0
        };

        public override string ToString()
        {
            var sb = new StringBuilder(512);
            sb.AppendLine($"{GameMode}({(int)GameMode}) - {GameMap}({(int)GameMap}) [Region: {Region}] | ");  
            sb.AppendLine($"[RoundTime: {TimeSpan.FromSeconds(RoundTimeInSeconds)}] | Difficulty: {Difficulty:F} | ");
            return sb.ToString();
        }

        //============================================
        /// <summary>
        /// Игровая локация
        /// </summary>
        public GameMapFlags GameMap { get; set; }
        
        /// <summary>
        /// Режим игры
        /// </summary>
        public GameModeFlags GameMode { get; set; }

        /// <summary>
        /// Регион сервера
        /// </summary>
        public ClusterRegionId Region  { get; set; }

        public long TargetServerVersion { get; set; }

        /// <summary>
        /// Длительность раунда в секундах
        /// </summary>
        public short RoundTimeInSeconds { get; set; }

        ///// <summary>
        ///// Уровень амуниции игроков
        ///// </summary>
        //public byte ArmourLevel { get; set; }
        
        /// <summary>
        /// Идентификатор игрока на которого нападают, которому было отправленно предложение на дуэль
        /// </summary>
        public Guid? UniversalId { get; set; }

        //============================================
        /// <summary>
        /// Ставка для дуэли
        /// </summary>
        public StoreItemCost Bet { get; set; }


        /// <summary>
        /// Уровень сложности ботов, если они есть
        /// </summary>
        public double Difficulty { get; set; } = 4;

        /// <summary>
        /// Дополнительные параметры матча
        /// <para>
        ///  <see cref="NumberOfLives"/> - количество жизней для дуэли
        /// </para> 
        /// <para>
        ///  <see cref="WaveNumber"/> - количество волн в PvE - режиме
        /// </para>     
        /// <para>
        ///  <see cref="ScoreLimit"/> - Максимальное количество очков
        /// </para>         
        /// </summary>
        public int AdditionalFlag { get; set; }

        /// <summary>
        /// Возвращает количество жизней для дуэли, полученное из AdditionalFlag
        /// </summary>
        public int NumberOfLives => AdditionalFlag;

        public byte NumberOfBots { get; set; }

        /// <summary>
        /// Возвращает максимальное количество очков, полученное из AdditionalFlag
        /// </summary>
        public int ScoreLimit => AdditionalFlag;

        /// <summary>
        /// Возвращает номер волны, полученный из AdditionalFlag
        /// </summary>
        public int WaveNumber => AdditionalFlag;

        public SearchSessionOptions()
        {
            
        }

        public SearchSessionOptions(GameModeFlags gm, GameMapFlags map, TimeSpan round, long InTargetServerVersion, int lives = 50)
        {
            GameMode = gm;
            GameMap = map;
            Bet = new StoreItemCost();
            AdditionalFlag = lives;
            RoundTimeInSeconds = Convert.ToInt16(round.TotalSeconds);
            TargetServerVersion = InTargetServerVersion;
            Difficulty = 4;
        }

    }
}