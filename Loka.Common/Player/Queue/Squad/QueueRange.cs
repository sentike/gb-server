﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Loka.Server.Models.Queue
{
    [ComplexType]
    public class QueueRange
    {
        public QueueRange()
        {
            
        }

        public QueueRange(short value)
        {
            Min = Max = value;
        }

        public bool InRange(short value) => value >= Min && value <= Max;

        public override string ToString()
        {
            return $"Range [{Min} - {Max}]: {Max - Min + 1}";
        }

        public short Min { get; set; }
        public short Max { get; set; }
    }
}