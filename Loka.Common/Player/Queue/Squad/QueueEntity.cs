﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Player.Models;


namespace Loka.Server.Models.Queue
{
    public struct QueueMemberContainer
    {
        public Guid PlayerId { get; set; }
        public string Name { get; set; }
        public PlayerFriendStatus Status { get; set; }

        public QueueMemberContainer(QueueEntity member)
        {
            PlayerId = member.QueueId;
            Name = member.PlayerEntity.PlayerName;
            Status = member.PlayerEntity.Status();


            if (member.IsLeader)
            {
                Status ^= PlayerFriendStatus.Leader;
            }
            else if (member.Ready.HasFlag(QueueState.Searching) && Status.HasFlag(PlayerFriendStatus.Offline) == false)
            {
                Status = PlayerFriendStatus.Ready ^ PlayerFriendStatus.Online;
            }
        }

        public override string ToString()
        {
            return $"{Name} : {Status}";
        }
    }

    [Flags]
    public enum QueueState
    {
        None,
        Searching = 1,
        Building = 2,
        Playing = 4,
        WaitingPlayers = 8
    }

    public class QueueEntity
    {
        public QueueEntity()
        {
            
        }

        public QueueEntity(Guid partyId, Guid playerId, SearchSessionOptions options, QueueState ready = QueueState.None)
        {
            QueueId = playerId;
            PartyId = partyId;

            JoinDate = DateTime.UtcNow;
            CheckDate = DateTime.UtcNow;

            Options = options;
            Level = new QueueRange(1);
            Ready = ready;
        }

        public Guid QueueId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public Guid PartyId { get; set; }
        public virtual QueueEntity PartyEntity { get; set; }

        public bool InSquad => PartyId != QueueId && Members.Count != 1;
        public bool IsLeader => PartyId == QueueId;
        public virtual List<QueueEntity> Members { get; private set; } = new List<QueueEntity>(2);

        public SearchSessionOptions Options { get; set; }


        public long ElsapsedSecondsFromLastCheck => Math.Abs(CheckDate.ToSecondsTime());
        public DateTime CheckDate { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public QueueRange Level { get; set; }

        public QueueState Ready { get; set; }

        public bool PartyReady =>
            PartyEntity.Members.Where(m => m.IsLeader == false)
            .Any(m => m.Ready.HasFlag(QueueState.Searching) == false) == false &&
            PartyEntity.Ready.HasFlag(QueueState.Searching);

        public bool IsOnline => LastActivityDate.ToSecondsTime() > 5;
        public bool IsMembersReady => PartyEntity.Members.Where(m => m.IsLeader == false).Any(m => m.Ready.HasFlag(QueueState.Searching) == false) == false;

	    public bool HasCurrentMatchMember => PlayerEntity?.MatchMembers?.Any(p => p.Finished == false && p.MatchEntity.IsMbFinished == false) ?? false;

		public MatchMemberEntity CurrentMatchMember
        {
            get
            {
                var members = PlayerEntity.MatchMembers.Where(p => p.Finished == false && p.MatchEntity.IsMbFinished == false).ToArray();
                return members.FirstOrDefault();
            }
        }
            


        public override string ToString()
        {
            return $"Queue {Ready} {Level}, create by {QueueId} {PlayerEntity.PlayerName}";
        }

        public virtual void SetDefaultSquad(QueueState ready = QueueState.None)
        {
            PartyId = QueueId;
            Ready = ready;
        }

        public class Configuration : EntityTypeConfiguration<QueueEntity>
        {
            public Configuration()
            {
                HasKey(p => p.QueueId);
                ToTable("public.QueueEntity");

                HasRequired(p => p.PlayerEntity).WithOptional(p => p.QueueEntity).WillCascadeOnDelete(true);
                HasMany(p => p.Members).WithRequired(m => m.PartyEntity).HasForeignKey(p => p.PartyId);
            }
        }
    }
}
