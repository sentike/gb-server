﻿//using System;
//using System.Collections.Generic;
//using System.Data.Entity.ModelConfiguration;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Loka.Server.Player.Models;
//

//namespace Loka.Server.Models.Queue
//{
//    public class QueueMember
//    {
//        public QueueMember()
//        {
            
//        }

//        public QueueMember(Guid queueId, Guid userId)
//        {
//            PlayerId = userId;
//            QueueId = queueId;
//        }

//        public Guid PlayerId { get; set; }
//        public virtual PlayerEntity PlayerEntity { get; set; }


//        public Guid QueueId { get; set; }
//        public virtual QueueEntity QueueEntity { get; set; }

//        public Guid? MatchMemberId { get; set; }
//        public virtual MatchMember MatchMember { get; set; }


//        public bool Ready { get; set; }

//        public class Configuration : EntityTypeConfiguration<QueueMember>
//        {
//            public Configuration()
//            {
//                HasKey(p => p.PlayerId);
//                ToTable("public.QueueMember");
//                HasRequired(p => p.QueueEntity).WithMany(p => p.Members).HasForeignKey(q => q.QueueId).WillCascadeOnDelete(true);
//                HasRequired(p => p.PlayerEntity).WithOptional(p => p.QueueMember).WillCascadeOnDelete(true);

//                HasOptional(m => m.MatchMember).WithMany().HasForeignKey(m => m.MatchMemberId);
//            }
//        }
//    }
//}
