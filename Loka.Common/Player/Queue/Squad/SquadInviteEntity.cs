﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Player.Models;

namespace Loka.Server.Models.Queue
{
    [Flags]
    public enum SquadInviteState
    {
        None,

        Submitted = 1,
        Received = 2,

        Approved = 4,
        Rejected = 8,

        Notified = 16,
    }

    public enum SquadInviteType
    {
        Squad,
        Duel
    }

    public enum SquadInviteDirection
    {
        Invite,
        Request
    }


    public class SquadInviteEntity
    {
        public Guid InviteId { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public Guid SquadId { get; set; }
        public virtual QueueEntity SquadEntity { get; set; }
        public virtual PlayerEntity SenderPlayerEntity { get; set; }


        public SquadInviteState State { get; set; }
        public DateTime SubmittedDate { get; set; }

        public SquadInviteType Type { get; set; }
        public SearchSessionOptions Options { get; set; }
        public SquadInviteDirection Direction { get; set; }

        public static SquadInviteEntity Factory(Guid targetId, Guid squadId, SquadInviteDirection direction, SearchSessionOptions inviteOptions)
        {
            return new SquadInviteEntity
            {
                InviteId = Guid.NewGuid(),
                PlayerId = targetId,
                SquadId = squadId,
                State = SquadInviteState.Submitted,
                SubmittedDate = DateTime.UtcNow,
                Type = SquadInviteType.Squad,
                Direction = direction,
                Options = inviteOptions
            };
        }


        public class Configuration : EntityTypeConfiguration<SquadInviteEntity>
        {
            public Configuration()
            {
                HasKey(p => p.InviteId);
                ToTable("public.SquadInviteEntity");

                HasRequired(p => p.SenderPlayerEntity).WithMany(p => p.OutgoingSquadInvites).HasForeignKey(p => p.SquadId).WillCascadeOnDelete(true);
                HasRequired(p => p.PlayerEntity).WithMany(p => p.IncomingSquadInvites).HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(true);
                HasRequired(p => p.SquadEntity).WithMany().HasForeignKey(p => p.SquadId).WillCascadeOnDelete(true);
            }
        }
    }
}