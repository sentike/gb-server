﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models.Queue;

namespace Loka.Server.Infrastructure.Queue
{
    public struct FSquadInviteInformation
    {
        public Guid InviteId { get; set; }

        public Guid PlayerId { get; set; }
        public string PlayerName { get; set; }

        public SquadInviteDirection Direction { get; set; }
        public SearchSessionOptions Options { get; set; }
        public SquadInviteState State { get; set; }

        public long SubmittedDate { get; set; }

        public override string ToString()
        {
            return $"[{InviteId}] - [{PlayerName}][{State}][{Options}]";
        }


        public FSquadInviteInformation(SquadInviteEntity inviteEntity, bool isNotify = false)
        {
            InviteId = inviteEntity.InviteId;

            if (isNotify)
            {
                PlayerId = inviteEntity.PlayerEntity.PlayerId;
                PlayerName = inviteEntity.PlayerEntity.PlayerName;
            }
            else
            {
                PlayerId = inviteEntity.SquadEntity.PlayerEntity.PlayerId;
                PlayerName = inviteEntity.SquadEntity.PlayerEntity.PlayerName;
            }

            SubmittedDate = inviteEntity.SubmittedDate.ToUnixTime();
            Direction = inviteEntity.Direction;
            Options = inviteEntity.Options;
            State = inviteEntity.State;
        }
    }

 
}
