using System;
using System.Data.Entity.ModelConfiguration;

using Loka.Infrastructure;
using Loka.Server.Player.Models;

namespace Loka.Common.Player.Statistic
{
    public enum PlayerSessionAttach
    {
        Client,
        Server
    }

    public class PlayerGameSessionEntity
    {
        public Guid SessionId { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public DateTime LastMessageAccess { get; set; }
        public DateTime LastActivityDate { get; set; }
        public DateTime LastMoneyGenerate { get; set; }
        public PlayerSessionAttach SessionAttach { get; set; }

        public PlayerGameSessionEntity()
        {
            
        }

        public PlayerGameSessionEntity(Guid userAccountId, PlayerSessionAttach attach = PlayerSessionAttach.Client)
        {
            SessionId = Guid.NewGuid();
            PlayerId = userAccountId;
            SessionAttach = attach;
            LastActivityDate = DateTime.UtcNow.AddSeconds(15);
            LastMessageAccess = DateTime.UtcNow.AddSeconds(-30);
        }

        public override string ToString()
        {
            return $"{SessionId} | {PlayerEntity?.PlayerName} [{PlayerId}] | LastActivityDate: {LastActivityDate} [{CMath.ToElapsedSeconds(LastActivityDate)} seconds ago]";
        }

        public class Configuration : EntityTypeConfiguration<PlayerGameSessionEntity>
        {
            public Configuration()
            {
                HasKey(p => p.SessionId);
                ToTable("public.GameSessionEntity");
                HasRequired(l => l.PlayerEntity).WithMany().HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(true);
               // HasOptional(l => l.SessionQueue).WithMany().HasForeignKey(p => p.QueueId).WillCascadeOnDelete(true);
            }
        }
    }
}