﻿namespace Loka.Common.Player.Profile.Slot
{
    /// <summary>
    /// It specifies the object slot
    /// </summary>
    public enum PlayerProfileInstanceSlotId
    {
        None,


        //================================
        Rifle,
        Shotgun,
        SniperRifle,
        MachineGun,
        Pistol,
        Knife,

        //================================
        Head,
        Torso,
        Legs,

        //================================
        Grenade,
        AidKit,
        Stimulant,
        RepairKit,

        //================================
        Skin,
        Mask,
        Costume,

        End
	};
}
