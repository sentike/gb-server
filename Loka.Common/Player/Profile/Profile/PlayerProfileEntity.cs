﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Profile.Item;
using Loka.Common.Player.Profile.Slot;
using Loka.Player;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using VRS.Infrastructure;

namespace Loka.Common.Player.Profile.Profile
{
    ///// <summary>
    ///// Is a player profile, which contains the list of installed items. In particular, weapons, armor, character and special tools. Keeps the profile states his name and expiration date.
    ///// </summary>
    //public class PlayerProfileEntity
    //{
    //    public static PlayerProfileEntity Factory(PlayerEntity player)
    //    {
    //        return new PlayerProfileEntity
    //        {
    //            ProfileId = Guid.NewGuid(),
    //            PlayerId = player.PlayerId,
    //            PlayerEntity = player
    //        };
    //    }

    //    public Guid ProfileId { get; set; }

    //    public Guid PlayerId { get; set; }
    //    public virtual PlayerEntity PlayerEntity { get; set; }
    //    public virtual List<PlayerProfileItemEntity> Items { get; private set; } = new List<PlayerProfileItemEntity>(32);

    //    public Guid? DefaultItemId { get; set; }
    //    public virtual PlayerProfileItemEntity DefaultItemEntity { get; set; }

    //    public override string ToString()
    //    {
    //        return $"Profile {ProfileId} | Owner: {PlayerEntity?.PlayerName} [{PlayerId}] | Items: {Items.Count} | DefaultItemId: {DefaultItemId}";
    //    }

    //    public PlayerProfileItemEntity Equip(PlayerInventoryItemEntity inventoryItem, PlayerProfileInstanceSlotId slotId)
    //    {
    //        PlayerProfileItemEntity slotEntity = Items.SingleOrDefault(i => i.SlotId == slotId);

    //        var from = $"[{slotEntity?.SlotId}][{slotEntity?.InventoryItemEntity?.ModelId}]";
    //        var to = $"[{inventoryItem.ItemId}][{inventoryItem.ModelId}]";

    //        LoggerContainer.StoreLogger.Info($"[Equip][{slotId}]{from} -> {to} | DefaultItemId: {DefaultItemId} / {DefaultItemEntity?.InventoryItemEntity?.ModelId}");
    //        if (slotEntity == null)
    //        {
    //            Items.Add(slotEntity = PlayerProfileItemEntity.Factory(inventoryItem, slotId));
    //            if(DefaultItemId == null || DefaultItemEntity == null)
    //            {
    //                DefaultItemId = slotEntity.ProfileItemId;
    //                DefaultItemEntity = slotEntity;
    //            }
    //        }
    //        else
    //        {
    //            //======================================
    //            if(slotEntity.ItemId == inventoryItem.ItemId)
    //            {
    //                DefaultItemId = slotEntity.ProfileItemId;
    //                DefaultItemEntity = slotEntity;
    //            }

    //            //======================================
    //            slotEntity.ItemId = inventoryItem.ItemId;
    //            slotEntity.InventoryItemEntity = inventoryItem;
    //        }

    //        return slotEntity;
    //    }

    //    public class Configuration : EntityTypeConfiguration<PlayerProfileEntity>
    //    {
    //        public Configuration()
    //        {
    //            HasKey(p => p.ProfileId);
    //            ToTable("players.PlayerProfileEntity");
    //            HasRequired(i => i.PlayerEntity).WithMany(i => i.InventoryProfileList).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);
    //            HasOptional(i => i.DefaultItemEntity).WithMany().HasForeignKey(i => i.DefaultItemId);
    //        }
    //    }
    //}
}
