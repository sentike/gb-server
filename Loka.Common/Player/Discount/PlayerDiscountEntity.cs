﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Server.Player.Models;

namespace Loka.Common.Player.Discount
{
    public enum DiscountCouponeModels
    {
        premium_1 = -2000,      //  +
        premium_7 = -2005,      //  +
        premium_30 = -2001,     //  +

        crystals_1200 = -1003,  //  +
        crystals_7000 = -1004,  //  +
        crystals_575 = -1002,   //  +

        money_18000 = -3002,
        LootBox_100 = 803,
        Grenade_01 = 702,
        SDAW_Rifle_2 = 202,
        SDAW_Pistol_3 = 103,


        MotherFuck = 302,

    }

    public enum EProductDiscountCoupone : short
    {
        None,
        Percent_5,
        Percent_15,
        Percent_30,
        Percent_50,
        Percent_70,
    }

    public static class DiscountCouponeExtensions
    {
        public static int GetDiscountPercent(this EProductDiscountCoupone coupone)
        {
            switch (coupone)
            {
                case EProductDiscountCoupone.Percent_5: return 5;
                case EProductDiscountCoupone.Percent_15:return 15;
                case EProductDiscountCoupone.Percent_30:return 30;
                case EProductDiscountCoupone.Percent_50:return 50;
                case EProductDiscountCoupone.Percent_70:return 70;
            }
            return 0;
        }
    }


    public class PlayerDiscountEntity
    {
        public Guid EntityId { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public int ModelId { get; set; }
        public virtual ItemInstanceEntity ModelEntity { get; set; }

        public EProductDiscountCoupone Coupone { get; set; }

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Отображает информацию о последней дате использования. Null если скидка еще не использовалась
        /// </summary>
        public DateTime? LastUsingDate { get; set; }

        /// <summary>
        /// Информация о том, когда купон будет доступен снова. Если есть значение - то после этой даты скидка появится сама автоматически. Если значения нет, то скидка была одноразовая или значение устанавливается вручную
        /// </summary>
        public DateTime? NextAvalibleDate { get; set; }

        /// <summary>
        /// Информация о том, когда действие акции будет окончено. Если есть значение - то после этой даты скидка будет не действительна. Если значения нет, то скидка действует бессрочно
        /// </summary>
        public DateTime? EndDiscountDate { get; set; }

        public class Configuration : EntityTypeConfiguration<PlayerDiscountEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("players.PlayerDiscountEntity");
                HasRequired(p => p.ModelEntity).WithMany().HasForeignKey(p => p.ModelId);

                Property(p => p.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_ModelId", 0) { IsUnique = true }));
                Property(p => p.ModelId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_ModelId", 1) { IsUnique = true }));

            }
        }

        public bool HasAvalible => Coupone != EProductDiscountCoupone.None && (NextAvalibleDate.HasValue && DateTime.UtcNow >= NextAvalibleDate) && (EndDiscountDate == null || DateTime.UtcNow < EndDiscountDate);

        public void UseDiscountCoupone()
        {
            LastUsingDate = DateTime.UtcNow;
            NextAvalibleDate = null;
            EndDiscountDate = null;
        }

        public StoreItemCost ApplyDiscount(StoreItemCost InCost)
        {
            var Percent = Coupone.GetDiscountPercent();
            var DiscountedCost = InCost.Amount * ((100 - Percent) / 100.0);
            var ItemCost = new StoreItemCost(DiscountedCost, InCost.Currency);
            UseDiscountCoupone();
            return ItemCost;
        }
    }
}
