﻿using Loka.Common.Award.EveryDayAward;
using Loka.Server.Player.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Infrastructure.DataRepository;
using VRS.Infrastructure;

namespace Loka.Common.Player.Instance
{
    public enum PlayerEveryDayAwardState : short
    {
        None,
        RequireGiveReward,
        RequirClientNotify,
    }

    public class PlayerEveryDayAwardEntity
    {
        //=================================================
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        //=================================================
        public short? JoinCount { get; set; }
        public virtual EveryDayAwardEntity JoinAwardEntity { get; set; }

        //=================================================
        public DateTime JoinDate { get; set; }
        public PlayerEveryDayAwardState State { get; set; }


        //=================================================
        /// <summary>
        /// Выдаем бонус игроку и обновляем флаг 
        /// </summary>
        public void UpdateOrGiveEveryDayBonus(DataRepository db)
        {
            //-----------------------------------------
            var IsAllowGiveReward = UpdateEveryDayBonus();

            //----------------------------------------- 
            if (IsAllowGiveReward && State == PlayerEveryDayAwardState.RequireGiveReward)
            {
                db.Entry(this).Reference(p => p.JoinAwardEntity).Load();
            }

            //-----------------------------------------
            LoggerContainer.AccountLogger.Error($"[0][UpdateEveryDayBonus][{PlayerEntity.PlayerName}][IsAllowGiveReward: {IsAllowGiveReward}][State: {State}][Case: {JoinAwardEntity?.RewardCaseId} / Exist: {JoinAwardEntity?.RewardCaseEntity != null}]");

            //-----------------------------------------
            if (IsAllowGiveReward && State == PlayerEveryDayAwardState.RequireGiveReward && JoinAwardEntity?.RewardCaseEntity != null)
            {
                //================================================
                State = PlayerEveryDayAwardState.RequirClientNotify;

                //================================================
                //  Выдаем кейс игроку в качестве награды за вход
                //  Если у игрока уже есть кейс за этот день, то выдаем еще один
                var Case = PlayerEntity.AddOrGetPlayerCase(JoinAwardEntity.RewardCaseId, out var _, true);
                JoinAwardEntity.RewardCaseEntity.GiveAwardToPlayer(PlayerEntity);
                Case.SetAmount(0);

                //================================================
                db.SaveChanges();
            }
        }
        
        private bool UpdateEveryDayBonus()
        {
            //-------------------------------------
            //  JoinDate = 29.01.2019
            //  DateTime.UtcNow = 30.01.2019
            //  JoinSessionBroken = false

            //-------------------------------------
            //  Сколько дней прошло с момента последнего входа в игру
            var ElapsedDays = DateTime.UtcNow.DayOfYear - JoinDate.DayOfYear;
            var JoinSessionBroken = ElapsedDays > 1;

            //-------------------------------------
            LoggerContainer.AccountLogger.Error($"[1][UpdateEveryDayBonus][{PlayerEntity.PlayerName}][ElapsedDays: {DateTime.UtcNow.DayOfYear} - {JoinDate.DayOfYear} = {ElapsedDays} / JoinSessionBroken: {JoinSessionBroken}][UtcNow > JoinDate: {DateTime.UtcNow > JoinDate} | {JoinDate.Date != DateTime.UtcNow.Date}]");

            //-------------------------------------
            //  Если сессия входов в игру прервана, то обнуляем счетчик
            if (JoinSessionBroken)
            {
                JoinCount = null;
                JoinDate = DateTime.UtcNow;
            }
            else if (DateTime.UtcNow > JoinDate && (JoinDate.Date != DateTime.UtcNow.Date && State == PlayerEveryDayAwardState.None))
            {
                var LastJoinCount = JoinCount;
                var NextJoinCount = JoinCount.GetNextSafeAwardId(); ;

                LoggerContainer.AccountLogger.Error($"[2][UpdateEveryDayBonus][{PlayerEntity.PlayerName}][JoinCount: {LastJoinCount} -> {NextJoinCount}]");

                JoinCount = JoinCount.GetNextSafeAwardId();
                JoinDate = DateTime.UtcNow;
                State = PlayerEveryDayAwardState.RequireGiveReward;
                return true;
            }

            //-------------------------------------
            return false;
        }
    
        //================================================
        public class Configuration : EntityTypeConfiguration<PlayerEveryDayAwardEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PlayerId);
                ToTable("players.PlayerEveryDayAwardEntity");
                HasOptional(p => p.JoinAwardEntity).WithMany().HasForeignKey(p => p.JoinCount);
            }
        }
    }
}
