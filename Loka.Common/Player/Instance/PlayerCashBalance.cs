﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using Loka.Common.Infrastructure;
using Loka.Common.Store;
using Loka.Common.Transaction;
using Loka.Server.Player.Models;


namespace Loka.Common.Player.Instance
{
    public class PlayerCashEntity : ICountable
    {
        public Guid CashId { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public long Amount { get; set; }
        public long LastAmount { get; set; }
        public GameCurrency Currency { get; set; }
        public DateTime LastChangedDate { get; set; }

        public long Hash { get; set; }

        public Guid EntityId => CashId;
        public long Count => Amount;

        public PlayerCashEntity() { }

        public PlayerCashEntity(GameCurrency currency, long amount)
        {
            CashId = Guid.NewGuid();
            LastChangedDate = DateTime.UtcNow;
            Currency = currency;
            Amount = amount;
            LastAmount = 0;
        }

        public override string ToString()
        {
            return $"{Currency}: {Amount} | LastAmount: {LastAmount} | Owner: {PlayerEntity?.PlayerName}";
        }

        public bool IsAllowPay(StoreItemCost cost)
        {
            return IsAllowPay(cost.Amount) && cost.Currency == Currency;
        }

        public bool IsAllowPay(long cost)
        {
            return Amount >= cost;
        }

        public void Give(StoreItemCost amount)
        {
            Give(amount.Amount);
        }

        public void Give(long amount)
        {
            //-----------------------------
            LastChangedDate = DateTime.UtcNow;

            //-----------------------------
            LastAmount = Amount;

            //-----------------------------
            Amount += amount;
            Hash = Amount.Crc64Hash();
        }

        public void ForcePay(StoreItemCost cost)
        {
            Give(cost.Invert());
        }

        public long AvalibleCount(StoreItemCost cost)
        {
            var count = Amount * 1.0 / cost.Amount;
            var amount = Math.Floor(count);
            var result = (long)amount;
            return result;
        }

        public bool Pay(long cost)
        {
            if (IsAllowPay(cost))
            {
                Give(-cost);
                return true;
            }
            return false;
        }

	    public bool Pay(StoreItemCost cost)
	    {
		    if (IsAllowPay(cost))
		    {
			    Give(cost.Invert());
			    return true;
		    }
		    return false;
	    }

		public class Configuration : EntityTypeConfiguration<PlayerCashEntity>
        {
            public Configuration()
            {
                HasKey(p => p.CashId);
                ToTable("players.PlayerCashEntity");

                Property(p => p.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_Currency", 0) { IsUnique = true }));
                Property(p => p.Currency).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_Currency", 1) { IsUnique = true }));
            }
        }
    }
}
