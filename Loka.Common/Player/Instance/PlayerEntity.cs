﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Loka.Common.Achievement;
using Loka.Common.Cluster;


using Loka.Common.Infrastructure;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Player;
using Loka.Common.Player.Instance;
using Loka.Common.Player.Profile.Profile;
using Loka.Common.Store;

using Loka.Player;
using Loka.Server.Models.Queue;
using Loka.Common.Player.Inventory;

using Loka.Common.Player.Profile.Item;

using Loka.Common.Store.Abstract;

using Loka.Common.Tutorial;
using Loka.Infrastructure;

using VRS.Infrastructure;
using VRS.Infrastructure.Environment;
using Loka.Common.Match.GameMode;
using System.Data.Entity.Infrastructure.Annotations;
using Loka.Common.Cases;
using Loka.Common.Payments;
using Loka.Common.Player.Discount;
using Loka.Common.Player.Profile.Slot;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Common.Award.EveryDayAward;

namespace Loka.Server.Player.Models
{
    public enum AccountMigrationAction
    {
        None,
        AddNotExistProfilesAndCharacters,
        AddBonus500Money,
        CreateCashEntities
    }

    [Flags]
    public enum PlayerRole : short
    {
        None = 0,
        Partner = 1,
        Moderator = 2,
        Administrator = 4,
        Developer = 8,
        BetaTester = 16,
    }

    [SuppressMessage("ReSharper", "ClassWithVirtualMembersNeverInherited.Global")]
    public class PlayerEntity 
        : IVersionedEntity
        , IWithIsoCountry
    {
        public static int LastRatingPosition { get; set; } = 100000;

        //=======================================
        public void UnLockAccount()
        {
            //=========================
            Locked = false;

            //=========================
            PlayerName = PlayerName.Replace("_Blocked", string.Empty);
            PlayerNameNormilized = PlayerNameNormilized.Replace("_blocked", string.Empty);

            //=========================
            if (string.IsNullOrWhiteSpace(PlayerEmail) && string.IsNullOrWhiteSpace(LastPlayerEmail) == false)
            {
                PlayerEmail = LastPlayerEmail;
            }

            //=========================  
            foreach (var account in AccountEntities)
            {
                account.Locked = false;
            }
        }

        //=======================================
        public void LockAccount()
        {
            //=========================
            Locked = true;

            //=========================
            PlayerName += "_Blocked";
            PlayerNameNormilized += "_blocked";

            //=========================
            LastPlayerEmail = PlayerEmail;
            PlayerEmail = null;

            //=========================  
            foreach (var account in AccountEntities)
            {
                account.Locked = true;
            }
        }

        //=======================================
        public bool Locked { get; set; }

        public Guid PlayerId { get; set; }
        public string PlayerEmail { get; set; }
        public string LastPlayerEmail { get; set; }

        public string PlayerName { get; set; }
        public string PlayerNameNormilized { get; set; }

        public string GooglePlayPlayerId { get; set; }

        public DateTime? PremiumEndDate { get; set; }
        public bool HasPremiumAccount => PremiumEndDate != null && PremiumEndDate >= DateTime.UtcNow;

        public int RatingPosition { get; set; }
        public short EloRatingScore { get; set; } = 1000;
        public int EloRatingPosition { get; set; }
        public int NotifiedEloRatingPosition { get; set; }

        public short SafeEloRatingScore
        {
            get
            {
                if(EloRatingScore <= 0)
                {
                    EloRatingScore = 1000;
                }
                return EloRatingScore;
            }
        }

        public PlayerNotifications Notifications { get; set; }
        public PlayerExperience Experience { get; set; }
        public PlayerRole Role { get; set; }
        public EIsoCountry Country { get; set; }

        public void GivePremiumAccount(long days)
        {
            if(PremiumEndDate == null || DateTime.UtcNow > PremiumEndDate)
            {
                PremiumEndDate = DateTime.UtcNow.AddDays(days);
            }
            else
            {
                PremiumEndDate = PremiumEndDate.Value.AddDays(days);
            }
        }

        public DateTime RegistrationDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public LanguageTypeId Language { get; set; }


        public bool IsLowActivity => LastActivityDate.ElapsedMore(TimeSpan.FromHours(12)) && Notifications.IsAllowLowActivityNotify;


        public AccountMigrationAction MigrationAction { get; set; }
        public AccountMigrationAction NextMigrationAction => MigrationAction + 1;
        public Guid LastSessionId { get; set; }

        //public virtual ApplicationUser User { get; set; }
        public bool IsOnline => (DateTime.UtcNow - LastActivityDate).TotalSeconds < 10;

        #region Inventory
        public virtual ICollection<PlayerInventoryItemEntity> InventoryItemList { get; } = new List<PlayerInventoryItemEntity>(64);

        public PlayerProfileItemEntity AddDefaultInventoryItem(int model, PlayerProfileInstanceSlotId slot, long amount = 1)
        {
            LoggerContainer.AccountLogger.Info($"[AddDefaultInventoryItem][{PlayerName}][{model} > {slot}][Items: {Items.Count}][InventoryItemList: {InventoryItemList.Count}]");

            //====================================================
            if (Items.All(p => p.SlotId != slot))
            {
                var item = InventoryItemList.SingleOrDefault(p => p.ModelId == model);
                if (item == null)
                {
                    InventoryItemList.Add(item = new PlayerInventoryItemEntity(PlayerId, model, amount));
                }

                Equip(item, slot);
            }

            //====================================================
            return Items.SingleOrDefault(p => p.SlotId == slot);
        }


        //public virtual ICollection<PlayerProfileEntity> InventoryProfileList { get; } =  new List<PlayerProfileEntity>(5);

        //public PlayerProfileEntity GetPlayerProfile(DataRepository db)
        //{
        //    //====================================
        //    if (InventoryProfileList.Count == 1)
        //    {
        //        return InventoryProfileList.Single();
        //    }
        //
        //    //====================================
        //    if (InventoryProfileList.Count > 1)
        //    {
        //        //---------------------------------------------
        //        var profile = InventoryProfileList.OrderByDescending(p => p.Items.Count).First();
        //
        //        //---------------------------------------------
        //        var deletes = InventoryProfileList.Where(p => p.ProfileId != profile.ProfileId).ToArray();
        //
        //        //---------------------------------------------
        //        foreach (var deleted in deletes)
        //        {
        //            InventoryProfileList.Remove(deleted);
        //        }
        //
        //        //---------------------------------------------
        //        db.AccountPlayerPreSetEntity.RemoveRange(deletes);
        //        db.SaveChanges();
        //        return profile;
        //    }
        //    else
        //    {
        //        var profile = db.AccountPlayerPreSetEntity.Add(PlayerProfileEntity.Factory(this));
        //        db.SaveChanges();
        //        return profile;
        //    }
        //}

        #endregion

        public byte GetNormilizedWinRate()
        {
            //======================================
            var total = Convert.ToDouble(AchievementEntities.SingleOrDefault(p => p.AchievementId == AchievementModelId.MatchesTotal)?.Amount ?? 1);
            var wins = Convert.ToDouble(AchievementEntities.SingleOrDefault(p => p.AchievementId == AchievementModelId.MatchesWins)?.Amount ?? 1);

            //======================================
            var rate = Math.Max(wins, 1.0) / Math.Max(total, 1.0);
            var percent = rate * 100.0;

            //======================================
            return Convert.ToByte(percent);
        }

        public virtual List<PlayerAchievementEntity> AchievementEntities { get; } = new List<PlayerAchievementEntity>();
        public virtual List<PlayerPaymentEntity> PaymentEntities { get; } = new List<PlayerPaymentEntity>();
        public virtual List<PlayerCaseEntity> CaseEntities { get; } = new List<PlayerCaseEntity>(8);

        public bool AddDefaultPlayerCases()
        {
            bool bNeedDataSave = false;
            foreach(var CaseId in DefaultPlayerCases)
            {
                //==================================
                AddOrGetPlayerCase(CaseId, out var created);

                //==================================
                if (bNeedDataSave == false && created)
                {
                    bNeedDataSave = true;
                }
            }
            return bNeedDataSave;
        }

        public PlayerCaseEntity AddOrGetPlayerCase(int InCaseId, out bool OutNeedDataSave, bool InAllowAIAmountIfExist = false)
        {
            OutNeedDataSave = false;
            var CaseEntity = CaseEntities.SingleOrDefault(p => p.CaseId == InCaseId);
            if (CaseEntity == null)
            {
                CaseEntities.Add(CaseEntity = PlayerCaseEntity.Factory(this, InCaseId, 1));
                OutNeedDataSave = true;
            }
            else if(InAllowAIAmountIfExist)
            {
                CaseEntity.Amount++;
            }

            return CaseEntity;
        }

        public static int[] DefaultPlayerCases { get; } = new int[]
        {
            //PlayerCaseEntity.LootBoxWinner,           // Ежедневный обычный кейс за победу
            PlayerCaseEntity.LootBoxEveryDay,           // Ежедневный обычный кейс
            PlayerCaseEntity.TrohyCaseEveryDay,         // Ежедневный трофейный кейс
            PlayerCaseEntity.AdsFreeCase,               // Ежедневный кейс за рекламу
            PlayerCaseEntity.FirstEveryDayCase,         // Кейс за первый ежедневных вход в игру
        };

        public PlayerAchievementEntity GetAchievement(AchievementModelId achievement)
        {
            return AchievementEntities.SingleOrDefault(p => p.AchievementId == achievement);
        }

        public PlayerAchievementEntity GiveAchievement(PlayerGameAchievement achievement)
        {
            //---------------------------------------------
            var entity = AchievementEntities.SingleOrDefault(p => p.AchievementId == achievement.Key);

            //---------------------------------------------
            if (entity == null)
            {
                AchievementEntities.Add(new PlayerAchievementEntity(achievement.Key, achievement.Value));
            }
            else
            {
                entity.UpdateValue(achievement.Value, false, AchievementMethodId.Incrementable);
            }

            return entity;
        }

        public void GiveAchievements(List<PlayerGameAchievement> achievements)
        {
            foreach (var a in achievements)
            {
                GiveAchievement(a);
            }
        }


        //==================================================================================================================
        //  Cash

        public const long DefaultMoneyAmount = 500;

        public virtual ICollection<PlayerCashEntity> CashEntities { get; private set; } = new List<PlayerCashEntity>(8);

        public PlayerCashEntity TakeCash(GameCurrency currency)
        {
            var cash = CashEntities.FirstOrDefault(p => p.Currency == currency);
            if (cash == null)
            {
                CashEntities.Add(cash = new PlayerCashEntity(currency, 0));
            }
            return cash;
        } 

        public PlayerCashEntity TakeCash(StoreItemCost cost) => TakeCash(cost.Currency);

        public PlayerCashEntity CashMoney => TakeCash(GameCurrency.Money);
        public PlayerCashEntity CashDonate => TakeCash(GameCurrency.Donate);
        public PlayerCashEntity CashCoin => TakeCash(GameCurrency.Coin);

        public bool Pay(StoreItemCost cost)
        {
            return TakeCash(cost).Pay(cost);
        }

	    public bool Pay(GameCurrency currency, long amount)
	    {
		    return TakeCash(currency).Pay(amount);
	    }

		public long Count(StoreItemCost cost)
        {
            return TakeCash(cost).AvalibleCount(cost);
        }

        public void ForcePay(StoreItemCost cost)
        {
            TakeCash(cost).ForcePay(cost);
        }

        public void ForcePay(GameCurrency currency, long amount)
        {
            TakeCash(currency).Give(amount);
        }

        public void Give(GameCurrency currency, long amount)
        {
            TakeCash(currency).Give(amount);
        }

        public void Give(StoreItemCost amount)
        {
            TakeCash(amount).Give(amount);
        }

        public bool IsAllowPay(StoreItemCost cost)
        {
            return TakeCash(cost).IsAllowPay(cost);
        }

        public bool IsAllowPay(GameCurrency currency, long cost)
        {
            return TakeCash(currency).IsAllowPay(cost);
        }

        //==================================================================================================================
        public virtual ICollection<PlayerFriendEntity> FriendList { get; } = new List<PlayerFriendEntity>(5);

        public virtual ICollection<MatchMemberEntity> MatchMembers { get; } = new List<MatchMemberEntity>(1000);

        #region SquadInvites

        /// <summary>
        /// Incoming squad or duel invite list
        /// </summary>
        public virtual List<SquadInviteEntity> IncomingSquadInvites { get; } = new List<SquadInviteEntity>(1);

        /// <summary>
        /// Outgoing squad or duel invite list
        /// </summary>
        public virtual List<SquadInviteEntity> OutgoingSquadInvites { get; } = new List<SquadInviteEntity>(1);
        #endregion

        public Guid? CurrentTutorialId { get; set; }
        public virtual PlayerTutorialEntity CurrentTutorialEntity { get; set; }
        public virtual ICollection<PlayerTutorialEntity> PlayerTutorialEntities { get; } = new List<PlayerTutorialEntity>(16);


        //-------------------------------------------------------------------------------------------------------------------------------

        public virtual QueueEntity QueueEntity { get; set; }
        public bool IsSquadLeader => QueueEntity != null && QueueEntity.IsLeader;

        public void AddDefaultDiscountCoupones()
        {
            AddOrGetDiscountCoupone((int)DiscountCouponeModels.premium_30, EProductDiscountCoupone.Percent_30, DateTime.UtcNow, DateTime.UtcNow.AddDays(6));
            AddOrGetDiscountCoupone((int)DiscountCouponeModels.crystals_7000, EProductDiscountCoupone.Percent_70, DateTime.UtcNow, DateTime.UtcNow.AddDays(5));

            AddOrGetDiscountCoupone((int)DiscountCouponeModels.money_18000, EProductDiscountCoupone.Percent_50, DateTime.UtcNow, DateTime.UtcNow.AddDays(3));
            AddOrGetDiscountCoupone((int)DiscountCouponeModels.LootBox_100, EProductDiscountCoupone.Percent_15, DateTime.UtcNow, DateTime.UtcNow.AddDays(9));
            AddOrGetDiscountCoupone((int)DiscountCouponeModels.Grenade_01, EProductDiscountCoupone.Percent_30, DateTime.UtcNow, DateTime.UtcNow.AddDays(7));
            AddOrGetDiscountCoupone((int)DiscountCouponeModels.SDAW_Rifle_2, EProductDiscountCoupone.Percent_5, DateTime.UtcNow, DateTime.UtcNow.AddDays(4));
            AddOrGetDiscountCoupone((int)DiscountCouponeModels.SDAW_Pistol_3, EProductDiscountCoupone.Percent_70, DateTime.UtcNow, DateTime.UtcNow.AddDays(5));
            AddOrGetDiscountCoupone((int)DiscountCouponeModels.MotherFuck, EProductDiscountCoupone.Percent_70, DateTime.UtcNow, DateTime.UtcNow.AddDays(10));
        }

        public static PlayerEntity Factory(Guid playerId, string playerName, string google, string InEmailAddress = null)
        {
            //========================================================
            var Player = FactoryProxy(playerId, playerName, google, InEmailAddress);

            //========================================================
            Player.AddDefaultDiscountCoupones();

            //========================================================
            return Player;
        }

        private static PlayerEntity FactoryProxy(Guid playerId, string playerName, string google, string InEmailAddress = null)
        {
            return new PlayerEntity
            {
                PlayerId = playerId,
                PlayerName = playerName,
                PlayerEmail = InEmailAddress,
                PlayerNameNormilized = playerName.ToLowerInvariant(),
                GooglePlayPlayerId = google,
                RatingPosition = LastRatingPosition,
                Experience = new PlayerExperience
                {
                    IsLevelUpNotified = false,
                    IsNameConfirmed = false,
                    PrimeMatchesCount = 5,
                    ChangeNameTickets = 2,
                    PrimeMatchesNextDate = DateTime.UtcNow,                    
                    Experience = 0,
                    Level = 0
                },

                Notifications = new PlayerNotifications
                {
                    LowActivityNotifyNextDate = DateTime.UtcNow.AddHours(6),
                    AfterMatchNotifyNextDate = DateTime.UtcNow.AddHours(3),
                    AfterLevelUpNotifyNextDate = DateTime.UtcNow.AddHours(2),
                    BeforeLevelUpNotifyNextDate = DateTime.UtcNow.AddHours(1),
                    UpdateExistNotifyNextDate = DateTime.UtcNow.AddHours(18),
                    LastNotifyDate = DateTime.UtcNow.AddHours(1),
                    AfterLevelUpNotifyCount = 0
                },

                CashEntities = new List<PlayerCashEntity>()
                {
                    new PlayerCashEntity(GameCurrency.Money, DefaultMoneyAmount),
                    new PlayerCashEntity(GameCurrency.Donate, 10),
                    new PlayerCashEntity(GameCurrency.Coin, 0),
                },

                //  Защита новичков на 8 часов от нападений
                RegistrationDate = DateTime.UtcNow,
                LastActivityDate = DateTime.UtcNow,
                PremiumEndDate = DateTime.UtcNow.AddHours(1),
                SubscribeEndDate = DateTime.UtcNow.AddDays(-7),
            };
        }

        public override string ToString()
        {
            return $"{PlayerId}: {PlayerName} [{Experience.Level}]";
        }

        public PlayerFriendStatus Status()
        {
            PlayerFriendStatus status;
	        if (QueueEntity == null)
	        {
		        if (IsOnline)
		        {
			        status = PlayerFriendStatus.Online;
		        }
		        else
		        {
					status = PlayerFriendStatus.Offline;
				}
			}
            else
            {
	            if (IsOnline)
	            {
		            if (QueueEntity.Ready.HasFlag(QueueState.Searching))
		            {
			            status = PlayerFriendStatus.SearchGame;
		            }
		            else
		            {
			            var match = QueueEntity.CurrentMatchMember;
			            if (match?.MatchEntity == null)
			            {
							status = PlayerFriendStatus.Online;
						}
			            else
			            {
							status = PlayerFriendStatus.PlayGame;
						}
					}
	            }
	            else
	            {
					status = PlayerFriendStatus.Offline;
				}
			}
            return status;
        }

        public Guid RowVersion { get; set; }
        public DateTime? SubscribeEndDate { get; set; }

        public Guid? DefaultItemId { get; set; }
        public virtual PlayerProfileItemEntity DefaultItemEntity { get; set; }

        public virtual List<PlayerProfileItemEntity> Items { get; private set; } = new List<PlayerProfileItemEntity>(32);
        public virtual List<PlayerDiscountEntity> DiscountEntities { get; private set; } = new List<PlayerDiscountEntity>(8);
        public virtual List<PlayerAccountServiceEntity> AccountEntities { get; private set; } = new List<PlayerAccountServiceEntity>(8);


        

        public PlayerProfileItemEntity Equip(PlayerInventoryItemEntity inventoryItem, PlayerProfileInstanceSlotId slotId)
        {
            PlayerProfileItemEntity slotEntity = Items.SingleOrDefault(i => i.SlotId == slotId);

            var from = $"[{slotEntity?.SlotId}][{slotEntity?.InventoryItemEntity?.ModelId}]";
            var to = $"[{inventoryItem.ItemId}][{inventoryItem.ModelId}]";

            LoggerContainer.StoreLogger.Info($"[Equip][{slotId}]{from} -> {to} | DefaultItemId: {DefaultItemId} / {DefaultItemEntity?.InventoryItemEntity?.ModelId}");
            if (slotEntity == null)
            {
                Items.Add(slotEntity = PlayerProfileItemEntity.Factory(inventoryItem, slotId));
                if ((DefaultItemId == null || DefaultItemEntity == null) && slotId > PlayerProfileInstanceSlotId.None && slotId < PlayerProfileInstanceSlotId.Pistol)
                {
                    DefaultItemId = slotEntity.ProfileItemId;
                    DefaultItemEntity = slotEntity;
                }
            }
            else
            {
                //======================================
                if (slotEntity.ItemId == inventoryItem.ItemId && slotId > PlayerProfileInstanceSlotId.None && slotId < PlayerProfileInstanceSlotId.Pistol)
                {
                    DefaultItemId = slotEntity.ProfileItemId;
                    DefaultItemEntity = slotEntity;
                }

                //======================================
                slotEntity.ItemId = inventoryItem.ItemId;
                slotEntity.InventoryItemEntity = inventoryItem;
            }

            return slotEntity;
        }

        public PlayerDiscountEntity GetDiscountCoupone(int InModelId)
        {
            return DiscountEntities.SingleOrDefault(p => p.ModelId == InModelId);
        }


        public PlayerDiscountEntity AddOrGetDiscountCoupone(int InModelId, EProductDiscountCoupone InDiscountCoupone, DateTime? InNextAvalibleDate = null, DateTime? InEndDiscountDate = null)
        {
            var Coupone = GetDiscountCoupone(InModelId);
            if (Coupone == null)
            {
                DiscountEntities.Add(Coupone = new PlayerDiscountEntity
                {
                    EntityId = Guid.NewGuid(),
                    CreatedDate = DateTime.UtcNow,
                    LastUsingDate = null,
                    NextAvalibleDate = InNextAvalibleDate,
                    EndDiscountDate = InEndDiscountDate,
                    ModelId = InModelId,
                    Coupone = InDiscountCoupone,
                    PlayerId = PlayerId,
                    PlayerEntity = this,
                });
            }
            return Coupone;
        }

        public virtual PlayerEveryDayAwardEntity EveryDayAwardEntity { get; set; }
        
        public PlayerEveryDayAwardEntity SafeEveryDayAwardEntity
        {
            get
            {
                if(EveryDayAwardEntity == null)
                {
                    EveryDayAwardEntity = new PlayerEveryDayAwardEntity
                    {
                        PlayerId = PlayerId,
                        PlayerEntity = this,
                        JoinCount = null, //EveryDayAwardExtensions.GetNextSafeAwardId(null),
                        JoinDate = DateTime.UtcNow.AddDays(-1)
                    };
                }
                return EveryDayAwardEntity;
            }
        }

        public class Configuration : EntityTypeConfiguration<PlayerEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PlayerId);
                ToTable("players.PlayerEntity");

                HasOptional(f => f.CurrentTutorialEntity).WithMany().HasForeignKey(f => f.CurrentTutorialId);
                HasOptional(f => f.EveryDayAwardEntity).WithRequired(p => p.PlayerEntity).WillCascadeOnDelete(true);
                HasMany(p => p.PlayerTutorialEntities).WithRequired(p => p.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);
                HasMany(p => p.CashEntities).WithRequired(p => p.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);


                HasMany(i => i.InventoryItemList).WithRequired(i => i.PlayerInstance).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);
                HasMany(i => i.AchievementEntities).WithRequired(i => i.PlayerEntity).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);
                HasMany(i => i.DiscountEntities).WithRequired(i => i.PlayerEntity).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);
                HasMany(i => i.AccountEntities).WithRequired(i => i.PlayerEntity).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);

                



                HasMany(p => p.MatchMembers).WithRequired(m => m.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);
                
                Property(p => p.PlayerEmail).HasMaxLength(64);
                Property(p => p.LastPlayerEmail).HasMaxLength(64);
                Property(p => p.PlayerName).HasMaxLength(64).IsRequired();
                Property(p => p.PlayerNameNormilized).HasMaxLength(64).IsRequired();



                Property(p => p.GooglePlayPlayerId).HasMaxLength(32).IsOptional();

                Property(p => p.Country).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("Country_Index")));


                Property(p => p.PlayerName).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_Unique", 0) { IsUnique = true }));
                Property(p => p.GooglePlayPlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("GooglePlayPlayerId_Unique", 0) { IsUnique = true }));

                Property(p => p.PlayerEmail).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerEmail_Unique", 0) { IsUnique = true }));


                //Property(p => p.PlayerNameNormilized).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerNameNormilized", 1) { IsUnique = true }));


                HasOptional(i => i.DefaultItemEntity).WithMany().HasForeignKey(i => i.DefaultItemId);


                Property(p => p.RowVersion).IsConcurrencyToken(false);  
            }
        }

    }
}
