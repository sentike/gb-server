﻿using Loka.Infrastructure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Player.Instance
{
    public enum PlayerActivityNotificationType
    {
        None,

        /// <summary>
        /// Когда показывать следующее уведомление, после окончания боя
        /// </summary>
        AfterMatch,

        /// <summary>
        /// Когда показывать следующее уведомление, если игрок давно не заходил в игру
        /// </summary>
        LowActivity,

        /// <summary>
        /// Когда показывать следующее уведомления, если игрок почти получил свой уровень
        /// </summary>
        BeforeLevelUp,

        /// <summary>
        /// Когда показывать следующее уведомления после повышения уровня
        /// </summary>
        AfterLevelUp,
    }

    [ComplexType]
    public class PlayerNotifications
    {
        /// <summary>
        /// Когда в последний раз показывались уведомления
        /// </summary>
        public DateTime LastNotifyDate { get; set; }
        public static TimeSpan NotificationBroadcastDelay { get; } = TimeSpan.FromHours(6);
        public bool IsAllowNotificationBroadcast => LastNotifyDate.ElapsedMore(NotificationBroadcastDelay);

        public void UpdateLastNotifyDate()
        {
            LastNotifyDate = DateTime.UtcNow;
        }

        //==================================================
        public DateTime RatingNextNotifyDate { get; set; }
        public bool IsAllowRatingNotify => DateTime.UtcNow >= RatingNextNotifyDate;

        public void UpdateRatingNextNotifyDate()
        {
            LastNotifyDate = DateTime.UtcNow.AddHours(16);
        }

        //==================================================
        /// <summary>
        /// Когда показывать следующее уведомление, после окончания боя
        /// </summary>
        public DateTime AfterMatchNotifyNextDate { get; set; }
        public bool IsAllowAfterMatchNotif => DateTime.UtcNow >= AfterMatchNotifyNextDate;

        //==================================================
        /// <summary>
        /// Когда показывать следующее уведомление, если игрок давно не заходил в игру
        /// </summary>
        public DateTime LowActivityNotifyNextDate { get; set; }
        public bool IsAllowLowActivityNotify => DateTime.UtcNow >= LowActivityNotifyNextDate;


        //==================================================
        /// <summary>
        /// Когда показывать следующее уведомления, если игрок почти получил свой уровень
        /// </summary>
        public DateTime BeforeLevelUpNotifyNextDate { get; set; }
        public bool IsAllowBeforeLevelUpNotify => DateTime.UtcNow >= BeforeLevelUpNotifyNextDate;

        //==================================================
        public DateTime UpdateExistNotifyNextDate { get; set; }
        public bool IsAllowUpdateExistNotify => DateTime.UtcNow >= UpdateExistNotifyNextDate;


        //==================================================
        /// <summary>
        /// Когда показывать следующее уведомления после повышения уровня
        /// </summary>
        public DateTime AfterLevelUpNotifyNextDate { get; set; }
        public bool IsAllowAfterLevelUpNotify => AfterLevelUpNotifyCount < 5 && DateTime.UtcNow >= AfterLevelUpNotifyNextDate;

        /// <summary>
        /// Сколько раз было показано уведомления после повышения уровня, нужно что бы не сильно флудило
        /// </summary>
        public byte AfterLevelUpNotifyCount { get; set; }

        public void AddUpdateExistNotify()
        {
            UpdateLastNotifyDate();
            UpdateExistNotifyNextDate = DateTime.UtcNow.AddNightSafeTime(TimeSpan.FromHours(18));
        }

        public void AddLowActivityNotify()
        {
            UpdateLastNotifyDate();
            LowActivityNotifyNextDate = DateTime.UtcNow.AddNightSafeTime(TimeSpan.FromHours(12));
        }

        public void AddBeforeLevelUpNotify()
        {
            UpdateLastNotifyDate();
            if (DateTime.UtcNow > BeforeLevelUpNotifyNextDate)
            {
                BeforeLevelUpNotifyNextDate = DateTime.UtcNow.AddNightSafeTime(TimeSpan.FromHours(12));
            }
        }

        public void AddAfterLevelUpNotify(bool InForce = false)
        {
            UpdateLastNotifyDate();
            if (InForce)
            {
                AfterLevelUpNotifyCount = 0;
            }

            if (InForce || AfterLevelUpNotifyCount <= 5)
            {
                AfterLevelUpNotifyNextDate = DateTime.UtcNow.AddNightSafeTime(TimeSpan.FromHours(3));
                AfterLevelUpNotifyCount++;
            }
        }

        public void AddAfterMatchNotify(bool InIsFirstCall)
        {
            UpdateLastNotifyDate();
            if (InIsFirstCall)
            {
                AfterMatchNotifyNextDate = DateTime.UtcNow.AddNightSafeTime(TimeSpan.FromMinutes(20));
            }
            else
            {
                AfterMatchNotifyNextDate = DateTime.UtcNow.AddNightSafeTime(TimeSpan.FromHours(20));
            }
        }

    }

    [ComplexType]
    public class PlayerExperience
    {
        public static short DefaultPrimeMatchesCount { get; } = 5;

        public short PrimeMatchesCount { get; set; }
        public DateTime PrimeMatchesNextDate { get; set; }

        public bool IsAllowUsePrimeMatch()
        {
            return PrimeMatchesCount > 0;
        }

        public bool UsePrimeMatch()
        {
            ResetPrimeMatches();

            if (IsAllowUsePrimeMatch())
            {
                PrimeMatchesCount--;
                return true;
            }

            return false;
        }

        private bool IsElapsedPrimeMatchesNextDate => DateTime.UtcNow >= PrimeMatchesNextDate;

        public void ResetPrimeMatches()
        {
            if(IsElapsedPrimeMatchesNextDate)
            {
                //=============================================
                PrimeMatchesCount = DefaultPrimeMatchesCount;

                //=============================================
                //  Бонус начинает действовать в 00 следующего дня
                PrimeMatchesNextDate = DateTime.UtcNow.Date.AddDays(1);
            }
        }

        public bool IsLevelUpNotified { get; set; }
        public bool IsNameConfirmed { get; set; }
        public byte ChangeNameTickets { get; set; }

        public long TargetServerVersion { get; set; }

        /// <summary>
        /// Player Account Level 
        /// </summary>
        public int Level { get; set; }
        public int LastLevel { get; set; }

        public long Experience { get; set; }

        public long NextExperience => Convert.ToInt64(2000L + 8000L * Level);

        /// <summary>
        /// The experience gained during the tournament
        /// </summary>
        public long WeekExperience { get; set; }

        /// <summary>
        /// The time spent in the game during the tournament
        /// </summary>
        public long WeekActivityTime { get; set; }

        public TimeSpan WeekActivity => TimeSpan.FromSeconds(WeekActivityTime);

        /// <summary>
        /// Когда осталось немного до следующего уровня
        /// </summary>
        public bool BeforeLevelUp => (Convert.ToSingle(Experience) / Convert.ToSingle(NextExperience) * 100.0) >= 75;

        public IList<int> Calculate()
        {
            //==================================
            var CurrentLevel = Level;

            //==================================
            List<int> Levels = new List<int>(8);

            //==================================
            while (Experience >= NextExperience)
            {
                Experience -= NextExperience;
                Level++;
                Levels.Add(Level);
            }

            //==================================
            if (Level > CurrentLevel)
            {
                LastLevel = CurrentLevel;
                IsLevelUpNotified = false;
            }

            //==================================
            return Levels;
        }

        public bool IsAccess(int level) => Level >= level;
    };


}
