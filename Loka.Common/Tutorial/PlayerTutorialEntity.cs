﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Player.Models;

namespace Loka.Common.Tutorial
{
    public class PlayerTutorialEntity
    {
        public Guid ProgressId { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public long TutorialId { get; set; }
        public virtual TutorialInstanceEntity TutorialEntity { get; set; }

        public long StepId { get; set; }
        public virtual TutorialStepEntity StepEntity { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime CompleteDate { get; set; }
        public bool Complete { get; set; }

        public class Configuration : EntityTypeConfiguration<PlayerTutorialEntity>
        {
            public Configuration()
            {
                HasKey(i => i.ProgressId);
                ToTable("store.PlayerTutorialEntity");
                HasRequired(p => p.TutorialEntity).WithMany().HasForeignKey(p => p.TutorialId).WillCascadeOnDelete(true);
                HasRequired(p => p.StepEntity).WithMany().HasForeignKey(p => p.StepId).WillCascadeOnDelete(true);
            }
        }
    }
}
