﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;

namespace Loka.Common.Tutorial
{
    public class TutorialStepEntity
    {
        public long StepKey { get; set; }
        //==================================================================
        public virtual TutorialInstanceEntity TutorialEntity { get; set; }
        public long TutorialId { get; set; }
        public long StepId { get; set; }

        //==================================================================
        public virtual ItemInstanceEntity RewardEntity { get; set; }
        public int? RewardModelId { get; set; }
        public long RewardActivate { get; set; }
        public long RewardGive { get; set; }

        //==================================================================
        public string AdminComment { get; set; }
        public DateTime CreatedDate { get; set; }

        public class Configuration : EntityTypeConfiguration<TutorialStepEntity>
        {
            public Configuration()
            {
                ToTable("store.TutorialStepEntity");
                HasKey(i => i.StepKey);
                Property(p => p.TutorialId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("Tutorial_Step", 0) { IsUnique = true }));
                Property(p => p.StepId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("Tutorial_Step", 1) { IsUnique = true }));

                HasOptional(p => p.RewardEntity).WithMany().HasForeignKey(p => p.RewardModelId);
                Property(p => p.AdminComment).IsOptional().HasMaxLength(128);
            }
        }
    }
}
