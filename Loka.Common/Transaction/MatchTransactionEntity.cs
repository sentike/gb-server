﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;

namespace Loka.Common.Transaction
{
    public class MatchTransactionEntity : BasicTransactionEntity
    {        
        public Guid SessionId { get; set; }
        public Guid MatchId { get; set; }

        public MatchTransactionEntity() { }

        public MatchTransactionEntity(Guid account, int model, long amount, long last)
            : base(account, model, amount, last)
        {
            
        }

        //=====================================
        public new class Configuration : EntityTypeConfiguration<MatchTransactionEntity>
        {
            public Configuration()
            {
                Map(configuration =>
                {
                    configuration.ToTable("transaction.MatchTransactionEntity");
                    configuration.MapInheritedProperties();
                });

                Property(p => p.SessionId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
                Property(p => p.MatchId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            }
        }
    }
}
