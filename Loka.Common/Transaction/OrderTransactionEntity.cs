﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;

namespace Loka.Common.Transaction
{
    public class OrderTransactionEntity : BasicTransactionEntity
    {
        public Guid OrderId { get; set; }
        public bool Notified { get; set; }

        //=====================================
        public OrderTransactionEntity() { }
        public OrderTransactionEntity(Guid account, int model, long amount, long last)
            : base(account, model, amount, last)
        {

        }

        //=====================================
        public new class Configuration : EntityTypeConfiguration<OrderTransactionEntity>
        {
            public Configuration()
            {
                Map(configuration =>
                {
                    configuration.ToTable("transaction.OrderTransactionEntity");
                    configuration.MapInheritedProperties();
                });

                Property(p => p.OrderId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            }
        }
    }

}
