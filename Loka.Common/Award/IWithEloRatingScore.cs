﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Award
{
    public interface IWithEloRatingScore
    {
        short EloRatingScore { get; set; }
    }

    public interface IWithPreviewEloRatingScore
    {
        short EloRatingScorePreview { get; set; }
    }
}
