﻿using Loka.Common.Store.Abstract;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Award.PlayerLevelAward
{
    public class PlayerLevelRewardItemEntity
    {
        public short EntityId { get; set; }

        //===================================
        public short Level { get; set; }
        public virtual PlayerLevelAwardEntity LevelEntity { get; set; }

        //===================================
        public string AdminComment { get; set; }

        //===================================
        public int Amount { get; set; }
        public int ModelId { get; set; }
        public virtual ItemInstanceEntity ModelEntity { get; set; }

        //================================================
        public class Configuration : EntityTypeConfiguration<PlayerLevelRewardItemEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("award.PlayerLevelRewardItemEntity");
                Property(p => p.AdminComment).HasMaxLength(128);
                HasRequired(p => p.ModelEntity).WithMany().HasForeignKey(p => p.ModelId).WillCascadeOnDelete(true);
                HasRequired(p => p.LevelEntity).WithMany(p => p.ItemEntities).HasForeignKey(p => p.Level).WillCascadeOnDelete(true);
            }
        }
    }
}
