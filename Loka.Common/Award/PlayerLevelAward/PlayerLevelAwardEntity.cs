﻿using Loka.Common.Store.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Award.PlayerLevelAward
{
    public class PlayerLevelAwardEntity
    {
        //===================================
        public short EntityId { get; set; }

        //================================================
        public short Money { get; set; }
        public short Donate { get; set; }

        //================================================
        public virtual List<PlayerLevelRewardItemEntity> ItemEntities { get; set; } = new List<PlayerLevelRewardItemEntity>();

        //================================================
        public class Configuration : EntityTypeConfiguration<PlayerLevelAwardEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("award.PlayerLevelAwardEntity");
            }
        }
    }
}
