﻿using Loka.Server.Infrastructure.DataRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Award.EveryDayAward
{
    public static class EveryDayAwardExtensions
    {
        public static short[] CachedIds { get; private set; }

        public static short? GetNextSafeAwardId(this short? InValue)
        {            
            //---------------------------------------
            //  Сохраняем список всех наград
            if (CachedIds == null)
            {
                using (var db = new DataRepository())
                {
                    CachedIds = db.EveryDayAwardEntities.Where(p => p.Enabled).OrderBy(p => p.EntityId).Select(p => p.EntityId).ToArray();
                }
            }

            //---------------------------------------
            if (InValue != null)
            {
                //  Если есть бонус за следующий день, то получем ид этого дня
                if (CachedIds.Any(p => p > InValue))
                {
                    return CachedIds.First(p => p > InValue);
                }
            }

            //  Если есть вообще бонусы, то получаем ид бонуса за первый день
            if (CachedIds.Any())
            {
                return CachedIds.FirstOrDefault();
            }

            //  Если бонусов нет, то null
            return null;
        }


    }
}
