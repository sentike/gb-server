﻿using Loka.Common.Cases;
using Loka.Server.Infrastructure.DataRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Award.EveryDayAward
{
    public class EveryDayAwardEntity
    {
        //===================================
        public short EntityId { get; set; }
        public string AdminComment { get; set; }

        //================================================
        public int RewardCaseId { get; set; }
        public virtual CaseEntity RewardCaseEntity { get; set; }

        //================================================
        public DateTime CreatedDate { get; set; }
        public bool Enabled { get; set; }

        //================================================
        public class Configuration : EntityTypeConfiguration<EveryDayAwardEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("award.EveryDayAwardEntity");
                Property(p => p.AdminComment).HasMaxLength(256);
                HasRequired(p => p.RewardCaseEntity).WithMany().HasForeignKey(p => p.RewardCaseId).WillCascadeOnDelete(true);
            }
        }
    }
}
