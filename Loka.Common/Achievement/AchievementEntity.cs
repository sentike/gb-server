﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Achievement
{
    public enum AchievementModelId
    {
        //----------------
        None,

        //----------------
        Kills,
        Deads,
        Score,

        //----------------
        Money,
        Experience,

        //----------------
        FirstBlood,
        Assist,


        //----------------
        KillsByPistol,
        KillsByShotGun,
        KillsBySniperRifle,
        KillsByRifle,
        KillsByKnife,
        KillsByGrenade,

        //----------------
        SeriesKills,
        SeriesKillsByPistol,
        SeriesKillsByShotGun,
        SeriesKillsBySniperRifle,
        SeriesKillsByRifle,
        SeriesKillsByKnife,
        SeriesKillsByGrenade,

        //----------------
        MatchesTotal,

        MatchesWins,
        MatchesLoses,
        MatchesDefeats,

        //----------------
        HeadShot,

        //----------------
        //	TimeInGame in Seconds
        TimeInGame,

        //----------------
        BestScore,
        BestKills,

        //----------------
        FirstInMatch,
        SecondInMatch,
        ThirdInMatch,

        //----------------
        End
    }

    public enum AchievementMethodId
    {
        /// <summary>
        /// Used for repeated actions. For example: the number of murders, the number of fights, wins
        /// </summary>
        Incrementable,

        /// <summary>
        /// Used for actions that can be performed once, but have a lot of steps. For example a series of murders
        /// </summary>
        Settable,

        End
    }

    public class AchievementEntity
    {
        public AchievementModelId ModelId { get; set; }
        public AchievementMethodId MethodId { get; set; }
        public virtual List<AchievementPropertyEntity> PropertyEntities { get; set; } = new List<AchievementPropertyEntity>(4);

        public class Configuration : EntityTypeConfiguration<AchievementEntity>
        {
            public Configuration()
            {
                HasKey(p => p.ModelId);
                ToTable("store.AchievementEntity");
                HasMany(p => p.PropertyEntities).WithRequired(p =>p.ModelEntity).HasForeignKey(p => p.ModelId).WillCascadeOnDelete(true);
            }
        }

    }
}
