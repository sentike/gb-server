﻿using Loka.Common.Store.Abstract;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Achievement
{
    public class AchievementPropertyEntity
    {
        //=====================================
        public int AchievementPropertyId { get; set; }


        //=====================================
        public AchievementModelId ModelId { get; set; }
        public virtual AchievementEntity ModelEntity { get; set; }

        //=====================================
        public long Amount { get; set; }

        //=====================================
        public int? AwardModelId { get; set; }
        public virtual ItemInstanceEntity AwardModelEntity { get; set; }

        public long AwardAmount { get; set; }

        public class Configuration : EntityTypeConfiguration<AchievementPropertyEntity>
        {
            public Configuration()
            {
                HasKey(p => p.AchievementPropertyId);
                ToTable("store.AchievementPropertyEntity");
                HasRequired(p => p.ModelEntity).WithMany().HasForeignKey(p => p.ModelId);
                HasOptional(p => p.AwardModelEntity).WithMany().HasForeignKey(p => p.AwardModelId);
            }
        }

    }
}
