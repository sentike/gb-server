﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Loka.Common.Achievement;
using Loka.Common.Cases;
using Loka.Common.Cluster;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Prime;
using Loka.Common.Player.Statistic;
using Loka.Common.Store;
using Loka.Infrastructure;

using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Queue;
using MoreLinq;
using VRS.Infrastructure;

namespace Loka.Common.Match.GameMode
{
    public class PlayerGameAchievement
    {
        public AchievementModelId Key { get; set; }
        public long Value { get; set; }

        public PlayerGameAchievement() { }

        public PlayerGameAchievement(AchievementModelId key, long value)
        {
            Key = key;
            Value = value;
        }

        public override string ToString()
        {
            return $"[{Key}, {Value}]";
        }

    }

    public class MatchMemberResultModel
    {
        public Guid MemberId { get; set; }
        public bool HasUsedPremiumAccount { get; set; }
        public List<PlayerGameAchievement> Achievements { get; set; } = new List<PlayerGameAchievement>();
    }

    public class MatchMemberBotResultModel
    {
        public Guid TeamId { get; set; }
        public string PlayerName { get; set; }
        public int Kills { get; set; }
        public int Deads { get; set; }
        public int Score { get; set; }
    }


    public class MatchResultView
    {
        public Guid? WinnerTeamId { get; set; }

        public Dictionary<Guid, short> Scores { get; set; } = new Dictionary<Guid, short>();
        public List<MatchMemberResultModel> Members { get; set; } = new List<MatchMemberResultModel>();
        public List<MatchMemberBotResultModel> Bots { get; set; } = new List<MatchMemberBotResultModel>();
    }

    public enum ProcessCreateTargetLevel : short
    {
        None, ZeroLevel, FirstLevel, HighLevel
    }

    public abstract class GameModeEntity
    {        
        public GameModeTypeId GameModeId { get; set; } 
        public GameModeFlags GameModeFlag => GameModeId.AsFlag();

        public MembersRange Level { get; set; }
        public int PrepareTimeInSeconds { get; set; }
        public int HalthPrepareTimeInSeconds => PrepareTimeInSeconds/2;
        public StoreItemCost JoinCost { get; set; }
        public short Priority { get; set; }
        public short ScoreLimit { get; set; }
        //public DateTime LastMatchMakingDate { get; set; }


        
        public virtual List<PrimeTimeGameEntity> PrimeTimes { get; private set; } = new List<PrimeTimeGameEntity>(10);

        //public bool HasActivePrimeTime()
        //{
        //    return PrimeTimes.Any(p => p.Days)
        //}

        public virtual List<GameMapEntity> Maps { get; private set; } = new List<GameMapEntity>(10);
        protected readonly object SyncObject = new object();
        public abstract void Tick();

        public GameModeEntity() { }

        protected GameModeEntity(GameModeTypeId gm, short score)
        {
            GameModeId = gm;
            Level = new MembersRange();
            PrepareTimeInSeconds = 10;
            JoinCost = new StoreItemCost();
            ScoreLimit = score;
        }

        public void AddMaps()
        {
            Maps = Enum.GetValues(typeof(GameMapTypeId)).Cast<GameMapTypeId>().ToArray()
                .Select(p => new GameMapEntity(GameModeId, p)).ToList();
        }

        public void UpdateEloRating()
        {

        }

        public virtual void Finish(MatchEntity session, MatchResultView result, DataRepository db)
        {
            try
            {
                //-------------------------------------------------------------
                //db.AchievementEntities.Include(p => p.PropertyEntities).Load();

                //-------------------------------------------------------------
                session.WinnerTeamId = result.WinnerTeamId;

                //-------------------------------------------------------------
                Logger.Info($"Match ended on {session.MatchId} | {session.Options.GameMode} - {session.Options.GameMap} | WinnerTeamId:{session.WinnerTeamId} | Players {result.Members.Count}");

                //-------------------------------------------------------------

                foreach (var score in result.Scores)
                {
                    var team = session.TeamList.SingleOrDefault(p => p.TeamId == score.Key);
                    if (team != null)
                    {
                        team.Score = score.Value;
                    }
                }
  
                //-------------------------------------------------------------
                var ResultMembers = result.Members.Where(m => m.MemberId != Guid.Empty).ToArray();
                foreach (var m in ResultMembers)
                {
                    MatchMemberEntity member = session.MemberList.SingleOrDefault(p => p.MemberId == m.MemberId);
                    if (member != null)
                    {
                        //======================================================================================================================
                        if (member.State == MatchMemberState.Deserted)
                        {
                            Logger.Warn($"Member has Deserted, memberId: {m.MemberId} | match: {session.MatchId}");
                            continue;
                        }

                        //======================================================================================================================
                        double multipler = 1.0f;

                        //======================================================================================================================
                        db.Entry(member).Reference(p => p.PlayerEntity).Load();
                        member.PlayerEntity.QueueEntity.Ready = QueueState.None;

                        //======================================================================================================================
                        db.Entry(member.PlayerEntity).Collection(p => p.CashEntities).Load();
                        db.Entry(member.PlayerEntity).Collection(p => p.AchievementEntities).Load();


                        //======================================================================================================================
                        var experience = m.Achievements.SingleOrDefault(p => p.Key == AchievementModelId.Experience);
                        var money = m.Achievements.SingleOrDefault(p => p.Key == AchievementModelId.Money);

                        var score = (m.Achievements.SingleOrDefault(p => p.Key == AchievementModelId.Score) ??
                                    m.Achievements.SingleOrDefault(p => p.Key == AchievementModelId.Money))?.Value ?? 0;
                        var kills = m.Achievements.SingleOrDefault(p => p.Key == AchievementModelId.Kills)?.Value ?? 0;

                        //======================================================================================================================
                        member.GiveAchievements(m.Achievements);
                        member.PlayerEntity.Experience.WeekExperience += score;

                        //======================================================================================================================
                        if(m.HasUsedPremiumAccount)
                        {
                            member.BoosterFlags |= MatchMemberBoosterFlags.HasUsedPremiumAccount;
                        }

                        //======================================================================================================================
                        var BestScore = member.PlayerEntity.GetAchievement(AchievementModelId.BestScore) ?? member.PlayerEntity.GiveAchievement(new PlayerGameAchievement() { Key = AchievementModelId.BestScore, Value = 0 });
                        var BestKills = member.PlayerEntity.GetAchievement(AchievementModelId.BestKills) ?? member.PlayerEntity.GiveAchievement(new PlayerGameAchievement() { Key = AchievementModelId.BestKills, Value = 0 });
                        
                        //======================================================================================================================
                        //LoggerContainer.MatchMakingLogger.Warn($"[Finish][{member.PlayerEntity.PlayerName}][IsWinner: {member.TeamId == session.WinnerTeamId} / {member.TeamId}][WinnerTeamId: {session.WinnerTeamId}] | Teams: {string.Join(",", session.TeamList.Select(p => p.TeamId).ToArray())}");
                        
                        //======================================================================================================================
                        if (BestScore != null)
                        {
                            if (score > 0 &&score > BestScore.Amount)
                            {
                                BestScore.UpdateValue(score, AchievementMethodId.Settable);
                            }
                        }

                        //======================================================================================================================
                        if (BestKills != null)
                        {
                            if (kills > 0 && kills > BestKills.Amount)
                            {
                                BestKills.UpdateValue(kills, AchievementMethodId.Settable);
                            }
                        }

                        //======================================================================================================================
                        if (member.BoosterFlags.HasFlag(MatchMemberBoosterFlags.HasUsedPremiumAccount))
                        {
                            multipler += 1.0f;
                        }

                        //======================================================================================================================
                        if (member.BoosterFlags.HasFlag(MatchMemberBoosterFlags.HasUsedEveryDayBooster))
                        {
                            multipler += 1.0f;
                        }

                        //======================================================================================================================
                        if (member.BoosterFlags.HasFlag(MatchMemberBoosterFlags.HasUsedPrimeTimeBooster))
                        {
                            multipler += 0.5f;
                        }

                        //======================================================================================================================
                        if (experience != null)
                        {
                            member.GiveExperience(experience.Value, multipler);
                        }

                        if (money != null)
                        {
                            member.GiveCurrency(GameCurrency.Money, money.Value, multipler);
                        }

                        //======================================================================================================================
                        if(member.PlayerEntity.Experience.Level == 0)
                        {
                            member.PlayerEntity.Experience.IsLevelUpNotified = false;
                            member.PlayerEntity.Experience.Level = 1;
                        }

                        //======================================================================================================================
                        //var achievementString = string.Join(",", m.Achievements);
                        //var cashString = string.Join(",", member.PlayerEntity.CashEntities);
                        //
                        //LoggerContainer.MatchMakingLogger.Warn($"[{member.PlayerEntity.PlayerName}] -- Money[{money}] | Experience[{experience}] | Achievements: {achievementString} / {cashString}");

                        //======================================================================================================================
                        member.State = MatchMemberState.Finished;
                    }
                    else
                    {
                        Logger.Error($"Member not found, memberId: {m.MemberId} | match: {session.MatchId} | Finish[0]");
                    }
                }

                //-------------------------------------------------------------
                db.SaveChanges();
            }
            catch (Exception exception)
            {
                Logger.WriteExceptionMessage($"Failed match [{session.MatchId}] Finish[1]", exception);
            }

            //-------------------------------------------------------------
            try
            {
                UpdateEloRatings(session, session.WinnerTeamId ?? Guid.Empty);
            }
            catch (Exception exception)
            {
                Logger.WriteExceptionMessage($"[UpdateEloRatings]", exception);
            }

            //-------------------------------------------------------------
            try
            {
                var history = MatchHistoryEntity.Factory(session);

                foreach (var bot in result.Bots)
                {
                    history.BotList.Add(new MatchMemberBotHistoryEntity
                    {
                        MatchEntity = history,
                        MatchId = history.MatchId,
                        PlayerName = bot.PlayerName,
                        Deads = bot.Deads,
                        Kills = bot.Kills,
                        Score = bot.Score,
                        MemberId = Guid.NewGuid(),
                        TeamId = bot.TeamId
                    });
                }

                db.MatchHistoryEntity.Add(history);
                db.SaveChanges();
            }
            catch (Exception exception)
            {
                Logger.WriteExceptionMessage($"Failed match [{session.MatchId}] Finish[2]", exception);
            }
        }

        protected virtual void UpdateEloRatings(MatchEntity InMatchEntity, Guid InWinnerTeamId)
        {

        }

        public override string ToString()
        {
            return $"{GameModeFlag} | {Maps.Count} maps: { string.Join(", ", Maps.Select(m => $"{m.MapId} [{m.Members}]"))}";
        }

        public class Configuration : EntityTypeConfiguration<GameModeEntity>
        {
            public Configuration()
            {
                HasKey(p => p.GameModeId);
                ToTable("gamemode.GameModeEntity");

                HasMany(p => p.PrimeTimes).WithRequired(p => p.GameModeEntity).HasForeignKey(p => p.GameModeId);
                HasMany(p => p.Maps).WithRequired(p => p.GameModeEntityEntity).HasForeignKey(p => p.GameModeId);
                //Property(p => p.LastMatchMakingDate).IsConcurrencyToken();
                //Ignore(p => p.SyncObject);
            }
        }

        protected virtual MatchMemberEntity FactoryMember(DataRepository db, ref int? squad, QueueEntity entity)
        {
            //-----------------------------------------
            var session = db.GameSessionEntity.Add(new PlayerGameSessionEntity(entity.QueueId, PlayerSessionAttach.Server));
            var member = MatchMemberEntity.Factory(entity, ref squad, session.SessionId);

            //-----------------------------------------
            return member;
        }


        protected virtual void ApplyMatchPropertyForMember(IEnumerable<QueueEntity> members, QueueState state = QueueState.Playing)
        {
            foreach (var member in members)
            {
                member.Ready = state;
            }
        }

	    protected virtual GameMapEntity TakeRandomMap(DataRepository repository, QueueEntity[] queue, long TargetServerVersion)
	    {
			//==================================================
		    var ignore = new List<GameMapFlags>
		    {
			    GameMapFlags.None,
			    GameMapFlags.End,
			    GameMapFlags.Lobby,
            };

            //==================================================
            //  LOKA_AW internal v1881  / LOKA_AW production v1884
            if (TargetServerVersion <= 258)
            {
                ignore.Add(GameMapFlags.Coastline);
            }

            if (TargetServerVersion <= 296)
            {
                ignore.Add(GameMapFlags.CoastlineCity);
            }
           

            //==================================================
            var maps = queue.Select(p => p.Options.GameMap).ToArray()
				.Where(p => ignore.Contains(p) == false).ToArray();

		    if (maps.Length > 1)
		    {
			    var groups = maps.GroupBy(p => p).ToArray();

			    // Генерируем случайную карту
				var randomed = TakeRandomMap();

			    // Получаем информацию о популярных картах
				var populate = groups.Max();

			    // Получаем информацию о не популярных картах
				var recomend = groups.Min();

				//	Если популярная и рекомендованная карта одинаковы,
				//	то ищем другую карту
			    if (populate.Key == recomend.Key)
			    {
				    if (randomed.MapAsFlag == recomend.Key)
				    {
						randomed = TakeRandomMap();
					}
				    return randomed;
			    }

				//	Если есть непопулярная карта, то возвращаем её
				//	если нет, то выдаем случайную
			    return Maps.FirstOrDefault(p => p.MapAsFlag == recomend.Key) ?? randomed;
		    }
		    return TakeRandomMap();
	    }

		protected virtual GameMapEntity TakeRandomMap()
        {
            return Maps.PickRandom();
        }

        protected virtual QueueEntity[] TakeQueueEntities(DataRepository repository)
        {
            return repository.QueueEntity.Where
            (
                q => (q.Options.GameMode.HasFlag(GameModeFlag) || q.Options.GameMode == GameModeFlags.None) && q.Ready.HasFlag(QueueState.Searching)
            ).ToArray().Where(q => q.IsOnline && q.PartyReady && q.JoinDate.ToElapsedSeconds() > 5).ToArray();
        }

        protected virtual QueueEntity[] TakeSquadEntities(QueueEntity[] queueEntities)
        {
            return queueEntities/*.Where(m => m.Level.InRange(l))*/.ToArray();
        }

        protected virtual MatchEntity TakeAvalibleMatch(DataRepository repository, long version)
        {
            return repository.MatchEntity/*.ToArray()*/.Where(m => 
            m.State == MatchGameState.Started &&
            m.Options.GameMode.HasFlag(GameModeFlag) &&
            m.Options.TargetServerVersion == version &&
            m.StartedOfMatch != null &&
            m.Finished == null &&
            m.WinnerTeamId == null &&
            m.TimeLeftInSeconds >= m.Options.RoundTimeInSeconds / 2 &&
            m.IsAllowJoinNewPlayers &&
            m.LastActivityDate != null
            ).ToArray().Where(m=> 
            m.Created.ToElapsedSeconds() < 300 &&
            m.LastActivityDate.HasValue &&
            m.LastActivityDate.Value.ToElapsedSeconds() < 15
            ).ToArray().PickRandom();
        }

        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public virtual bool AttackTo(DataRepository db, Guid attacker, Guid target)
        {
            throw new NotImplementedException("Only for Harvest game mode");
        }
    }
}