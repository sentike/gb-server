﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;


namespace Loka.Common.Match.GameMode.PvP
{
    public class LastHeroGameModeEntity : GameModeEntity
    {
        public LastHeroGameModeEntity() { }
        public LastHeroGameModeEntity(short score) : base(GameModeTypeId.LostDeadMatch, score) { }

        public override void Tick()
        {
            lock (SyncObject)
            using (var db = new DataRepository())
            {
                var queues = TakeQueueEntities(db);
                Logger.Info($"[LastHeroGameModeEntity] Begin make match, gm: {GameModeFlag} | queues: {queues.Length}");
                if (queues.Any() == false) return;

                var gameMap = TakeRandomMap(db, queues, 0);

				var squads = TakeSquadEntities(queues);
                var members = squads.SelectMany(s => s.Members).Take(gameMap.Members.Max).ToArray();
                if (members.Length < gameMap.Members.Min && members.Any(m => m.ElsapsedSecondsFromLastCheck > PrepareTimeInSeconds) == false)
                {
                    return;
                }

                var numberOfBots = Convert.ToByte(Math.Min(5, gameMap.Members.Min - members.Length));

                int? squad = null;
                //  Reserve memory for all members
                var teams = new List<MatchTeamEntity>(gameMap.Members.Max);
                teams.AddRange(members.Select(member => new MatchTeamEntity
                {
                    TeamId = Guid.NewGuid(),
                    MemberList = new List<MatchMemberEntity>
                    {
                        FactoryMember(db, ref squad, member)
                    }
                }));

                var match = new MatchEntity(teams, new SearchSessionOptions(GameModeFlags.LostDeadMatch, gameMap.MapAsFlag,
                    TimeSpan.FromSeconds(gameMap.RoundTimeInSeconds), 0, 3)
                {
                    NumberOfBots = numberOfBots,
					AdditionalFlag = 3
                });
    
                ApplyMatchPropertyForMember(members);


                db.MatchEntity.Add(match);
                db.SaveChanges();
            }
        }

        public new class Configuration : EntityTypeConfiguration<LastHeroGameModeEntity>
        {
            public Configuration()
            {
                ToTable("gamemode.LastHeroGameModeEntity");
            }
        }
    }
}
