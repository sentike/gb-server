using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Threading;

using Loka.Common.Cluster;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models.Queue;
using VRS.Infrastructure;


namespace Loka.Common.Match.GameMode.PvP
{
    public class TeamGameModeEntity : GameModeEntity
    {
        public TeamGameModeEntity() { }
        public TeamGameModeEntity(GameModeTypeId gm, short score) : base(gm, score) { }

        public new class Configuration : EntityTypeConfiguration<TeamGameModeEntity>
        {
            public Configuration()
            {
                ToTable("gamemode.TeamGameModeEntity");
            }
        }

        private void ProcessContinueProxy(DataRepository db, MatchEntity match, QueueEntityContainer[] QueueEntities)
        {
            try
            {
                ProcessContinue(db, match, QueueEntities);
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"[ProcessContinue][Match: {match?.MatchId}]", e);
            }
        }

        protected override void UpdateEloRatings(MatchEntity InMatchEntity, Guid InWinnerTeamId)
        {
            //===============================================
            var TeamA = InMatchEntity.TeamList.FirstOrDefault();
            var TeamB = InMatchEntity.TeamList.LastOrDefault();

            //===============================================
            var TeamATotalRating = TeamA.GetTotalRating();
            var TeamBTransformedRating = TeamB.GetTransformedRating();

            //===============================================
            TeamA.UpdateRatings(TeamA.TeamId == InWinnerTeamId, TeamBTransformedRating, TeamATotalRating);
            TeamB.UpdateRatings(TeamB.TeamId == InWinnerTeamId, TeamBTransformedRating, TeamATotalRating);
        }

        private void ProcessContinue(DataRepository db, MatchEntity match, QueueEntityContainer[] QueueEntities)
        {
            //==============================================
            var AvalibleQueueEntities = QueueEntities.Where(p => p.Group != QueueEntityGroup.Created).ToArray();

            //==============================================
            var Team1 = new List<MatchMemberEntity>(8);
            var Team2 = new List<MatchMemberEntity>(8);

            //==============================================
            int? SquadCount = 0;

            //==============================================
            //  Определяем в какую группу попадет игрок
            //  В существующий бой отправляем только игроков (без отряда)
            foreach (var entity in AvalibleQueueEntities)
            {
                //  Игроки в отряде не могут быть докинуты в бой
                //  Докидывание в бой возможно, если игрок удовлетворяет IsAllowJoinPlayer
                if (entity.InSquad == false && match.IsAllowJoinPlayer(entity.Entity))
                {
                    //-----------------------------------------------------------------
                    //  Разный способ докидования игроков в бой в зависимости от уровня
                    var PlayerLevel = entity.Entity.PlayerEntity?.Experience?.Level ?? 0;

                    //-----------------------------------------------------------------
                    //  Любой игрок с уровнем 2+ может быть докинут в бой
                    if (PlayerLevel >= 2)
                    {
                        entity.Group = QueueEntityGroup.Continue;
                    }

                    //-----------------------------------------------------------------
                    //  Игрок с уровнем 0 / 1 может быть докинут только на MilitaryBase
                    else if (match.Options.GameMap.HasFlag(GameMapFlags.MilitaryBase))
                    {
                        entity.Group = QueueEntityGroup.Continue;
                    }

                    //-----------------------------------------------------------------
                    //  Игрок с уровнем 0 / 1 может быть докинут только на MilitaryBase или попасть в бой только с картой MilitaryBase
                    else
                    {
                        entity.Group = QueueEntityGroup.Create;
                    }
                }
                else
                {
                    entity.Group = QueueEntityGroup.Create;
                }
            }

            //==============================================
            var Team1Ref = match.TeamList[0].MemberList;
            var Team2Ref = match.TeamList[1].MemberList;

            //==============================================
            //  Получаем список игроков в командах
            var Team1Count = Team1Ref.Count(p => p.State != MatchMemberState.Deserted);
            var Team2Count = Team2Ref.Count(p => p.State != MatchMemberState.Deserted);
            var PreviewTeamSumCount = Team1Count + Team2Count;

            //==============================================
            var QueueEntitiesToMatch = QueueEntities.Where(p => p.Group == QueueEntityGroup.Continue).Take(match.MaxPlayersInMatch - PreviewTeamSumCount).ToArray();

            //==============================================
            //  Балансируем команды
            foreach (var entity in QueueEntitiesToMatch)
            {
                //-----------------------------------------
                MatchMemberEntity member = FactoryMember(db, ref SquadCount, entity.Entity);
                member.State = MatchMemberState.Created;
                member.MatchId = match.MatchId;

                //-----------------------------------------
                if (Team1Count > Team2Count)
                {
                    Team2.Add(member);
                    Team2Count++;
                }
                else
                {
                    Team1.Add(member);
                    Team1Count++;
                }

                //-----------------------------------------
                entity.Group = QueueEntityGroup.Created;
            }

            //==============================================
            //  Добавляем в бой новых игроков
            Team1Ref.AddRange(Team1);
            Team2Ref.AddRange(Team2);

            //==============================================
            var FinalTeamSumCount = Team1Count + Team2Count;

            //==============================================
            match.IsAllowJoinNewPlayers = match.MaxPlayersInMatch > FinalTeamSumCount;

            //==============================================
            db.SaveChanges();
        }



        private void ProcessCreateProxy(DataRepository db, QueueEntityContainer[] QueueEntities, long TargetServerVersion)
        {
            //======================================
            var AvalibleMembersToCreate = QueueEntities.Where(p => p.Group == QueueEntityGroup.Create || p.Group == QueueEntityGroup.None).Select(p => p.Entity).ToArray();

            //======================================
            var ZeroLevel = AvalibleMembersToCreate.Where(p => p.PlayerEntity.Experience.Level == 0).ToArray();
            var FirstLevel = AvalibleMembersToCreate.Where(p => p.PlayerEntity.Experience.Level == 1).ToArray();
            var HighLevel = AvalibleMembersToCreate.Where(p => p.PlayerEntity.Experience.Level >= 2).ToArray();

            //======================================
            //  Матч мейкинг для новичков
            try
            {
                //------------------------------------------
                GameMapEntity gameMap = Maps.FirstOrDefault(p => p.MapId == GameMapTypeId.MilitaryBase) ?? TakeRandomMap(db, ZeroLevel, TargetServerVersion);

                //------------------------------------------
                ProcessCreate(db, gameMap, ZeroLevel, TargetServerVersion, ProcessCreateTargetLevel.ZeroLevel);
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"[ProcessCreate][1][ZeroLevel: {ZeroLevel.Length}][TargetServerVersion: {TargetServerVersion}]", e);
            }

            //======================================
            //  Матч мейкинг для тех кто играл
            try
            {
                //------------------------------------------
                GameMapEntity gameMap = null;

                //------------------------------------------
                Random random = new Random();

                //------------------------------------------
                var number = random.Next(0, 1000000);
                if (number <= 800000)
                {
                    gameMap = /*Maps.FirstOrDefault(p => p.MapId == GameMapTypeId.MilitaryBase) ??*/ TakeRandomMap(db, FirstLevel, TargetServerVersion);
                }
                else
                {
                    gameMap = TakeRandomMap(db, FirstLevel, TargetServerVersion);
                }

                //------------------------------------------
                ProcessCreate(db, gameMap, FirstLevel, TargetServerVersion, ProcessCreateTargetLevel.FirstLevel);
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"[ProcessCreate][1][FirstLevel: {FirstLevel.Length}][TargetServerVersion: {TargetServerVersion}]", e);
            }

            //======================================
            //  Матч мейкинг для опытных игроков
            try
            {
                var gameMap = TakeRandomMap(db, HighLevel, TargetServerVersion);
                ProcessCreate(db, gameMap, HighLevel, TargetServerVersion, ProcessCreateTargetLevel.HighLevel);
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"[ProcessCreate][0][HighLevel: {HighLevel.Length}][TargetServerVersion: {TargetServerVersion}]", e);
            }
        }

        public void ProcessTeamBalancingProxy(DataRepository db, ref QueueEntity[] InMembers, ProcessCreateTargetLevel InTargetLevel, int InMinGameMapMembers, int InMaxGameMapMembers, ref MatchTeamEntity InTeam1, ref MatchTeamEntity InTeam2)
        {
            if(InTargetLevel == ProcessCreateTargetLevel.ZeroLevel)
            {
                ProcessTeamBalancingLowLevel(db, ref InMembers, InMinGameMapMembers, InMaxGameMapMembers, ref InTeam1, ref InTeam2);
            }
            else
            {
                ProcessTeamBalancingStandart(db, ref InMembers, InMinGameMapMembers, InMaxGameMapMembers, ref InTeam1, ref InTeam2);
            }
        }

        public void ProcessTeamBalancingLowLevel(DataRepository db, ref QueueEntity[] InMembers, int InMinGameMapMembers, int InMaxGameMapMembers, ref MatchTeamEntity team1, ref MatchTeamEntity team2)
        {
            //==============================================
            //  Adding all members in the first team
            {
                int? squadCount = 0;
                team1.MemberList.AddRange(InMembers.Take(InMaxGameMapMembers).ToArray().Select(m =>
                    FactoryMember(db, ref squadCount, m)));
            }
        }

        public void ProcessTeamBalancingStandart(DataRepository db, ref QueueEntity[] InMembers, int InMinGameMapMembers, int InMaxGameMapMembers, ref MatchTeamEntity team1, ref MatchTeamEntity team2)
        {
            //==============================================
            var startDateTime = DateTime.UtcNow;

            //==============================================
            //  Adding all members in the first team
            {
                int? squadCount = 0;
                team1.MemberList.AddRange(InMembers.Select(m =>
                    FactoryMember(db, ref squadCount, m)));
            }

            //==============================================
            var buffer = new MatchTeamEntity() { MemberList = new List<MatchMemberEntity>(32) };

            //==============================================
            if (team1.MemberList.Count >= 2)
            {
                int NumItteration = 0;

                var memberCount = team1.MemberList.Count;
                while (true)
                {
                    NumItteration++;

                    if (team1.Lenght == team2.Lenght && team1.Lenght >= InMinGameMapMembers / 2)
                        break;

                    if (NumItteration >= 100)
                    {
                        break;
                    }

                    if (memberCount % 2 == 0)
                    {
                        if (team1.Lenght == team2.Lenght && (Math.Abs(team1.WinRate - team2.WinRate) >= 20 || team1.WinRate == team2.WinRate || NumItteration >= 15))
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (memberCount > 1)
                        {
                            if (team1.Lenght + team2.Lenght == memberCount && team1.Lenght > 0 && team2.Lenght > 0 && Convert.ToDouble(team2.Lenght) / team1.Lenght > 0.5 && (Math.Abs(team1.WinRate - team2.WinRate) >= 20 || team1.WinRate == team2.WinRate || NumItteration >= 15))
                            {
                                break;
                            }
                        }
                        else if (team1.Lenght + team2.Lenght == memberCount && (Math.Abs(team1.WinRate - team2.WinRate) >= 20 || team1.WinRate == team2.WinRate || NumItteration >= 15))
                        {
                            break;
                        }
                    }


                    if (team1.WinRate > team2.WinRate)
                    {
                        //--------------------------------------------
                        //  Получаем информацию о лучшем WinRate
                        var BestWinRateInTeam = team1.MemberList.Any() ? team1.MemberList.Max(p => p.WinRate) : 0;

                        //--------------------------------------------
                        // Merge We reserve on the team players only in the squads
                        if (team1.MemberList.Any(m => m.Squad > 0))
                        {
                            //--------------------------------------------
                            //  Перемещаем всех игроков без отряда в буффер
                            team1.MergeMember(buffer, team1.MemberList.Where(m => m.Squad == 0).ToList());

                            //--------------------------------------------
                            //  Получаем номер отряда с лучшим WinRate
                            var squadId = team1.MemberList.First(s => s.WinRate == BestWinRateInTeam).Squad;

                            //--------------------------------------------
                            //  Переносим участников лучшего отряда во вражескую комманду
                            team1.MergeMember(team2, team1.MemberList.Where(m => m.Squad == squadId).ToList());
                        }
                        else
                        {
                            //  Переносим игрока с лучшим WinRate в другую команду
                            team1.MergeMember(team2, team1.MemberList.First(s => s.WinRate == BestWinRateInTeam));
                        }
                    }
                    else
                    {
                        //--------------------------------------------
                        if (team2.MemberList.Any())
                        {
                            //--------------------------------------------
                            //  Получаем информацию о лучшем WinRate
                            var BestWinRateInTeam = team2.MemberList.Any() ? team2.MemberList.Max(p => p.WinRate) : 0;
                            var MinWinRateInOurTeam = team1.MemberList.Any() ? team1.MemberList.Min(p => p.WinRate) : 0;

                            if (team2.MemberList.Any(m => m.Squad > 0))
                            {
                                //--------------------------------------------
                                //  Получаем номер отряда с лучшим WinRate
                                var squadId = team2.MemberList.First(s => s.WinRate == BestWinRateInTeam).Squad;

                                //--------------------------------------------
                                //  Переносим участников лучшего отряда во вражескую комманду
                                team2.MergeMember(team1, team1.MemberList.Where(m => m.Squad == squadId).ToList());
                            }
                            else
                            {
                                //  Переносим игрока с худшим WinRate в нашу команду
                                var BadPlayers = team1.MemberList.Where(s => s.WinRate == MinWinRateInOurTeam).Take(4).ToList();
                                team1.MergeMember(team2, BadPlayers);


                                //  Переносим игрока с худшим WinRate в нашу команду
                                //var BadPlayer2 = team1.MemberList.FirstOrDefault(s => s.WinRate == MinWinRateInOurTeam);
                                //if (BadPlayer2 != null)
                                //{
                                //    team1.MergeMember(team2, BadPlayer);
                                //}

                                //  Переносим игрока с лучшим WinRate в другую команду
                                var BestPlayer = team2.MemberList.Where(s => s.WinRate == BestWinRateInTeam).Take(3).ToList();
                                //if (BestPlayer != null)
                                {
                                    team2.MergeMember(team1, BestPlayer);
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (buffer.MemberList.Any())
                    {
                        if (team2.Lenght > team1.Lenght)
                        {
                            buffer.MergeMember(team1, buffer.MemberList.First());
                        }
                        else
                        {
                            buffer.MergeMember(team2, buffer.MemberList.First());
                        }
                    }

                    if (team1.Lenght == team2.Lenght && team1.Lenght >= InMinGameMapMembers / 2)
                        break;

                    if (startDateTime.ToElapsedSeconds() > HalthPrepareTimeInSeconds)
                    {
                        break;
                    }

                    //if (team1.Lenght + team2.Lenght == memberCount && team1.Lenght + team2.Lenght >= MinGameMapMembers && Math.Abs(team1.WinRate - team2.WinRate) >= 30)
                    //{
                    //    InNumberOfBots = Convert.ToByte(MinGameMapMembers - (team1.Lenght + team2.Lenght));
                    //    break;
                    //}
                }
            }
        }

        private void ProcessCreate(DataRepository db, GameMapEntity gameMap, QueueEntity[] MembersToCreate, long TargetServerVersion, ProcessCreateTargetLevel InTargetLevel)
        {
            //==============================================
            LoggerContainer.MatchMakingLogger.Info($"[ProcessCreate][{TargetServerVersion}][0]");

            //==============================================
            LoggerContainer.MatchMakingLogger.Info($"[ProcessCreate][{TargetServerVersion}][MembersToCreate: {MembersToCreate.Length}][GameMap: {gameMap.MapId}][1]");

            //==============================================
            var team1 = new MatchTeamEntity() { TeamId = Guid.NewGuid() };
            var team2 = new MatchTeamEntity() { TeamId = Guid.NewGuid() };

            //==============================================
            //  List of squads that can play at the current level of equipment
            var squads = TakeSquadEntities(MembersToCreate);
            var members = squads.SelectMany(s => s.Members).ToArray();

            //==============================================
            LoggerContainer.MatchMakingLogger.Info($"[ProcessCreate][{TargetServerVersion}][MembersToCreate: {MembersToCreate.Length}][GameMap: {gameMap.MapId}][2]");

            //==============================================
            //  If the selected map does not have enough players who are looking for a game to the next level
            if (members.Any(m => m.ElsapsedSecondsFromLastCheck > PrepareTimeInSeconds) == false)
            {
                if (squads.Sum(s => s.Members.Count) < gameMap.Members.Min)
                    return;
            }

            //==============================================
            LoggerContainer.MatchMakingLogger.Info($"[ProcessCreate][{TargetServerVersion}][MembersToCreate: {MembersToCreate.Length}][GameMap: {gameMap.MapId}][3]");

            var MinGameMapMembers = gameMap.Members.Min;
            var MaxGameMapMembers = gameMap.Members.Min;

            ProcessTeamBalancingProxy(db, ref members, InTargetLevel, MinGameMapMembers, MaxGameMapMembers, ref team1, ref team2);
                
            LoggerContainer.MatchMakingLogger.Info($"[ProcessCreate][{TargetServerVersion}][MembersToCreate: {MembersToCreate.Length}][GameMap: {gameMap.MapId}][4]");

            //==============================================
            int RoundTimeInSeconds = gameMap.RoundTimeInSeconds;

            //==============================================
            if(InTargetLevel == ProcessCreateTargetLevel.ZeroLevel)
            {
                RoundTimeInSeconds = 120;
            }
            else if (InTargetLevel == ProcessCreateTargetLevel.FirstLevel)
            {
                RoundTimeInSeconds = 180;
            }

            //==============================================
            var TeamList = new List<MatchTeamEntity> {team1, team2};
            var RoundTime = TimeSpan.FromSeconds(RoundTimeInSeconds);
            var MapAsFlag = gameMap.MapAsFlag;

            LoggerContainer.MatchMakingLogger.Info($"[ProcessCreate][{TargetServerVersion}][MembersToCreate: {MembersToCreate.Length}][GameMap: {gameMap.MapId}][5]");
            //==============================================
            var SessionOptions = new SearchSessionOptions
            (
                GameModeFlag, 
                MapAsFlag,
                RoundTime, 
                TargetServerVersion, 
                ScoreLimit
            )
            {
                NumberOfBots = (byte) gameMap.Members.Min,
            };

            //==============================================
            var match = new MatchEntity(TeamList, SessionOptions)
            {
                MaxPlayersInMatch = gameMap.Members.Max,
                TargetLevel = InTargetLevel,
            };

            //==============================================
            db.MatchEntity.Add(match);
            ApplyMatchPropertyForMember(members);

            //==============================================
            LoggerContainer.MatchMakingLogger.Info($"[ProcessCreate][{TargetServerVersion}][MembersToCreate: {MembersToCreate.Length}][GameMap: {gameMap.MapId}][Match Created: {match.MatchId}][6]");

            //==============================================
            var changes = db.SaveChanges();

            //==============================================
            LoggerContainer.MatchMakingLogger.Info($"[ProcessCreate][{TargetServerVersion}][MembersToCreate: {MembersToCreate.Length}][GameMap: {gameMap.MapId}][Match Saved: {match.MatchId}][changes: {changes}][7]");
        }

        public enum QueueEntityGroup
        {
            None,
            Continue,
            Created,
            Create,
        }

        public class QueueEntityContainer
        {
            public QueueEntityGroup Group { get; set; }
            public QueueEntity Entity { get; set; }
            public List<QueueEntity> Members => Entity?.Members;
            public bool InSquad => Entity?.InSquad ?? false;

            public QueueEntityContainer(QueueEntity entity)
            {
                Entity = entity;
            }
        }

        private void Process(DataRepository db, QueueEntity[] queues, long TargetServerVersion)
        {
            //==============================================
            MatchEntity match = TakeAvalibleMatch(db, TargetServerVersion);

            //==============================================
            var QueueEntities = queues.Select(p => new QueueEntityContainer(p)).ToArray();

            //==============================================
            if (QueueEntities.Any() == false)
            {
                LoggerContainer.MatchMakingLogger.Info($"Begin make match[1.9], gm: {GameModeFlag} | queues: {queues.Length} | TargetServerVersion: {TargetServerVersion}");
                return;
            }

            //==============================================
            //  Получаем активный матч
            if (match != null)
            {
                ProcessContinueProxy(db, match, QueueEntities);
                //match = TakeAvalibleMatch(db, TargetServerVersion);
            }

            //==============================================
            ProcessCreateProxy(db, QueueEntities, TargetServerVersion);

            //==============================================
            ApplyMatchPropertyForMember(queues);
            db.SaveChanges();
        }


        public override void Tick()
        {
            if (PrepareTimeInSeconds < 25)
            {
                PrepareTimeInSeconds = 25;
            }

            try
            {
                using (var db = new DataRepository())
                {
                    //----------------------------------------------
                    var queues = TakeQueueEntities(db).GroupBy(p => p.Options.TargetServerVersion).ToArray();

                    //----------------------------------------------
                    LoggerContainer.MatchMakingLogger.Info($"Begin make match[1], gm: {GameModeFlag} | queues: {queues.Length}");

                    //----------------------------------------------
                    if (queues.Any() == false)
                    {
                        LoggerContainer.MatchMakingLogger.Info($"Begin make match[1.5], gm: {GameModeFlag} | queues: {queues.Length}");
                        return;
                    }

                    //----------------------------------------------
                    foreach (var queue in queues)
                    {
                        Process(db, queue.ToArray(), queue.Key);
                    }

                }
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"TeamGameModeEntity[{GameModeFlag}]", e);
            }
        }
    }
}