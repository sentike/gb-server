﻿using Loka.Common.Match.GameMode;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Match.Prime
{
    [ComplexType]
    public class PrimeTimeGameBoost
    {
        public int Money { get; set; }
        public int Experience { get; set; }
    }

    [Flags]
    public enum PrimeTimeWeekDays
    {
        Everyday,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 4,
        Thursday = 8,
        Friday = 16,
        Saturday = 32,
        Sunday = 64
    }

    public class PrimeTimeGameEntity
    {
        //=================================
        public int EntityId { get; set; }

        //=================================
        public GameModeTypeId GameModeId { get; set; }
        public virtual GameModeEntity GameModeEntity { get; set; }

        //=================================
        public PrimeTimeWeekDays Days { get; set; }
        public TimeSpan From { get; set; }
        public TimeSpan End { get; set; }

        //=================================
        public PrimeTimeGameBoost Boost { get; set; }

        //=================================
        public class Configuration : EntityTypeConfiguration<PrimeTimeGameEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("gamemode.PrimeTimeGameEntity");
            }
        }
    }
}
