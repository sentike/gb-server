﻿using Loka.Server.Player.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using AutoMapper;
using Loka.Common.Achievement;
using Loka.Common.Match.Match;
using Loka.Common.Match.Team;
using Loka.Common.Store;
using Loka.Common.Match.GameMode;
using Loka.Server.Models.Queue;
using Loka.Common.Award;

namespace Loka.Common.Match.Member
{
    public enum MatchMemberState
    {
        /// <summary>
        /// Player has been added to the game the match but has not yet entered the game
        /// </summary>
        Created,

        /// <summary>
        /// Player has been added to the game the match and started to play
        /// </summary>
        Started,

        /// <summary>
        /// Player has been added to the game the match but left the game
        /// </summary>
        Deserted,

        Finished,

        Notified,

        End
    }

    [Flags]
    public enum MatchMemberBoosterFlags : short
    {
        None = 0,

        HasUsedPremiumAccount = 2,

        HasUsedEveryDayBooster = 4,

        HasUsedPrimeTimeBooster = 8
    }

    public class MatchMemberEntity 
        : IWithEloRatingScore
    {
        public static int CalucalteTargetArmourLevel(PlayerEntity player)
        {          
            //===============================
            //  Количество брони доступной ботам
            //  0ед - нет брони
            //  1ед - желет
            //  2ед - шлем
            //  3ед - штаны
            //===============================
            //  Изначально доступно 30ед брони
            //  В каждой команде по 5 ботов
            //  Каждые 10с выдаем ботам новую броню. 
            //  За 50 секунд будет потрачено 1ед * 5 = 05ед брони на желеты. осталось 25
            //  За 50 секунд будет потрачено 2ед * 5 = 10ед брони на шлемы. осталось 15
            //  За 50 секунд будет потрачено 3ед * 5 = 15ед брони на штаны. осталось 0
            int TargetArmourLevel = 0;

            //===============================
            if (player.Experience.Level == 0)
            {
                //  Для новичков, боты не улучшают броню
                if (player.Experience.Experience < 0.7f * player.Experience.NextExperience)
                {
                    TargetArmourLevel = 0;
                }

                //  Если новичек почти дошел до первого уровня, то улучшаем одного бота
                else
                {
                    TargetArmourLevel = 1;
                }
            }

            //===============================
            //  Для игроков 1 уровня, боты надевают броню
            else if (player.Experience.Level == 1)
            {
                if (player.Experience.Experience < 0.7f * player.Experience.NextExperience)
                {
                    TargetArmourLevel = 1;
                }
                else
                {
                    TargetArmourLevel = 2;
                }
            }

            //  Для игроков 2 уровня, боты надевают броню и штаны
            else if (player.Experience.Level == 2)
            {
                if (player.Experience.Experience < 0.7f * player.Experience.NextExperience)
                {
                    TargetArmourLevel = 2;
                }
                else
                {
                    TargetArmourLevel = 3;
                }
            }

            //  Для игроков 3+ уровня, боты надевают броню, штаны и шлем
            else
            {
                //================================
                //  Очки для брони, штанов и шлема
                int ScorePerLevel = 3;

                //================================
                //  Для 3 уровня дано 11 очков брони
                //  За 50 секунд будет потрачено 1ед * 5 = 05ед брони на желеты. осталось 4
                //  За 50 секунд будет потрачено 2ед * 5 = 10ед брони на шлемы. 2 бота из 5 - получат шлемы (плюс один бот)

                //================================
                //  Для 5 уровня дано 17 очков брони
                //  За 50 секунд будет потрачено 1ед * 5 = 05ед брони на желеты. осталось 10
                //  За 50 секунд будет потрачено 2ед * 5 = 10ед брони на шлемы. осталось 0

                //================================
                TargetArmourLevel = (player.Experience.Level * ScorePerLevel) + 2;
            }

            return TargetArmourLevel;
        }

        public static MatchMemberEntity Factory(QueueEntity Queue, ref int? squadIndex, Guid session)
        {
            //===============================
            var player = Queue.PlayerEntity;

            //===============================
            int TargetArmourLevel = CalucalteTargetArmourLevel(player);
            int TargetWeaponUpgrade = CalucalteTargetWeaponUpgrade(player);

            //===============================
            MatchMemberBoosterFlags BoosterFlags = MatchMemberBoosterFlags.None;

            if(player.HasPremiumAccount)
            {
                BoosterFlags |= MatchMemberBoosterFlags.HasUsedPremiumAccount;
            }

            if (player.Experience.UsePrimeMatch())
            {
                BoosterFlags |= MatchMemberBoosterFlags.HasUsedEveryDayBooster;
            }

            //===============================
            //if (squadIndex != null && player.QueueEntity.Members.Count > 1) squadIndex++;
            return new MatchMemberEntity
            {
                MemberId = Guid.NewGuid(),
                PlayerEntity = player,
                PlayerId = player.PlayerId,
                State = MatchMemberState.Started,
                Squad = squadIndex ?? 0,
                WinRate = player.GetNormilizedWinRate(),
                TargetWeaponLevel = Queue.Level.Max,
                TargetArmourLevel = (short) TargetArmourLevel,
                TargetWeaponUpgrade = (short)TargetWeaponUpgrade,
                BoosterFlags = BoosterFlags,
                EloRatingScore = player.SafeEloRatingScore,
                SessionToken = session
            };
        }

        private static int CalucalteTargetWeaponUpgrade(PlayerEntity player)
        {
            //===============================
            int TargetWeaponUpgrade = 0;

            //===============================
            if (player.Experience.Level <= 2)
            {
                TargetWeaponUpgrade = 0;
            }
            else
            {
                TargetWeaponUpgrade = 1;
            }

            //===============================
            return TargetWeaponUpgrade;
        }

        public Guid MemberId { get; set; }

        public MatchMemberBoosterFlags BoosterFlags { get; set; }

        public virtual Guid MatchId { get; set; }
        public virtual MatchEntity MatchEntity { get; set; }

        public virtual Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public virtual Guid TeamId { get; set; }
        public virtual MatchTeamEntity TeamEntity { get; set; }

        /// <summary>
        /// Соотношение побед к поражениям
        /// </summary>
        public short WinRate { get; set; }
        public short TargetWeaponLevel { get; set; }
        public short TargetArmourLevel { get; set; }
        public short TargetWeaponUpgrade { get; set; }
        public short EloRatingScore { get; set; }


        public int Squad { get; set; }


        public MatchMemberState State { get; set; }
        public bool Finished => State == MatchMemberState.Finished || State == MatchMemberState.Deserted;

        public override string ToString()
        {
            return $"{MemberId} | {PlayerEntity?.PlayerName} | Squad: {Squad} | WinRate: {WinRate} | Team: {TeamId}";
        }

        public Guid SessionToken { get; set; }
        public virtual List<MatchMemberAchievementEntity> AchievementEntities { get; set; } = new List<MatchMemberAchievementEntity>(8);


        public void GiveCurrency(GameCurrency currency, long? amount, double InMultipler)
        {
            var TotalAmount = (amount ?? 0) * InMultipler;
            var CastedTotalAmount = Convert.ToInt64(Math.Floor(TotalAmount));

            PlayerEntity.Give(currency, CastedTotalAmount);
        }

        public void GiveExperience(long? amount, double InMultipler)
        {
            var TotalAmount = (amount ?? 0) * InMultipler;
            var CastedTotalAmount = Convert.ToInt64(Math.Floor(TotalAmount));

            PlayerEntity.Experience.Experience += CastedTotalAmount;
        }

        public void GiveAchievements(List<PlayerGameAchievement> achievements)
        {
            //================================================
            AchievementEntities.AddRange(achievements.Select(p => new MatchMemberAchievementEntity(p.Key, p.Value)).ToArray());

            //================================================
            PlayerEntity.GiveAchievements(achievements);
        }


        public class Configuration : EntityTypeConfiguration<MatchMemberEntity>
        {
            public Configuration()
            {
                HasKey(p => p.MemberId);
                ToTable("matches.MatchMember");
                HasRequired(p => p.PlayerEntity).WithMany().HasForeignKey(p => p.PlayerId);
   
                HasMany(p => p. AchievementEntities).WithRequired(p => p.MemberEntity).HasForeignKey(p => p.MemberId).WillCascadeOnDelete(true);

            }
        }


    }
}
