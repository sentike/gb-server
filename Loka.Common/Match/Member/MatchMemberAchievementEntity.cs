﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Achievement;

namespace Loka.Common.Match.Member
{
    public class MatchMemberAchievementEntity
    {
        public Guid EntityId { get; set; }

        public Guid MemberId { get; set; }
        public virtual MatchMemberEntity MemberEntity { get; set; }

        public AchievementModelId AchievementId { get; set; }
        public virtual AchievementEntity AchievementEntity { get; set; }

        public long Amount { get; set; }


        public MatchMemberAchievementEntity() { }

        public MatchMemberAchievementEntity(AchievementModelId model, long amount)
        {
            EntityId = Guid.NewGuid();
            AchievementId = model;
            Amount = amount;
        }

        public class Configuration : EntityTypeConfiguration<MatchMemberAchievementEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("matches.MatchMemberAchievement");
                HasRequired(p => p.AchievementEntity).WithMany().HasForeignKey(p => p.AchievementId);

                Property(p => p.MemberId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("MemberId_AchievementId", 0) { IsUnique = true }));
                Property(p => p.AchievementId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("MemberId_AchievementId", 1) { IsUnique = true }));
            }
        }
    }
}
