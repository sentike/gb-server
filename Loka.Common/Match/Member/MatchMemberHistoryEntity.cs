﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Award;
using Loka.Common.Match.Match;
using Loka.Common.Match.Team;
using Loka.Server.Player.Models;

namespace Loka.Common.Match.Member
{
    public class MatchMemberBotHistoryEntity
    {
        public Guid MemberId { get; set; }
        public Guid TeamId { get; set; }

        public virtual Guid MatchId { get; set; }
        public virtual MatchHistoryEntity MatchEntity { get; set; }

        public string PlayerName { get; set; }
        public int Kills { get; set; }
        public int Deads { get; set; }
        public int Score { get; set; }

        public class Configuration : EntityTypeConfiguration<MatchMemberBotHistoryEntity>
        {
            public Configuration()
            {
                HasKey(p => p.MemberId);
                ToTable("history.MatchMemberBotHistoryEntity");
            }
        }
    }

    [Flags]
    public enum MatchMemberHistoryState : short
    {
        None = 0,

        Deserted = 1,

        HasUsedPremiumAccount = 2,

        HasNotified = 4,


        HasUsedEveryDayBooster = 8,

        HasUsedPrimeTimeBooster = 16,

        HasUsedAdsBooster = 32,
    }

    public class MatchMemberHistoryEntity 
        : IWithEloRatingScore
        , IWithPreviewEloRatingScore
    {
        public Guid MemberId { get; set; }


        public virtual Guid MatchId { get; set; }
        public virtual MatchHistoryEntity MatchEntity { get; set; }

        public virtual Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public virtual Guid TeamId { get; set; }
        public virtual MatchTeamHistoryEntity TeamEntity { get; set; }

        public virtual List<MatchMemberAchievementHistoryEntity> AchievementEntities { get; set; } = new List<MatchMemberAchievementHistoryEntity>(8);

        public MatchMemberHistoryState State { get; set; }

        
        public bool HasUsedAdsBooster => State.HasFlag(MatchMemberHistoryState.HasUsedAdsBooster);

        public bool HasUsedPremiumAccount => State.HasFlag(MatchMemberHistoryState.HasUsedPremiumAccount);
        public bool HasNotified => State.HasFlag(MatchMemberHistoryState.HasNotified);

        public short Level { get; set; }
        public short WinRate { get; set; }
        public short Squad { get; set; }
        public short EloRatingScore { get; set; }
        public short EloRatingScorePreview { get; set; }

        public static MatchMemberHistoryEntity Factory(MatchMemberEntity entity)
        {
            //==========================================
            MatchMemberHistoryState State = MatchMemberHistoryState.None;

            //==========================================
            //  Для покинувших игроков не показываем уведомления
            if (entity.State == MatchMemberState.Deserted)
            {
                State |= MatchMemberHistoryState.Deserted;
                State |= MatchMemberHistoryState.HasNotified;
            }

            //==========================================
            if (entity.BoosterFlags.HasFlag(MatchMemberBoosterFlags.HasUsedPremiumAccount))
            {
                State |= MatchMemberHistoryState.HasUsedPremiumAccount;
            }

            if (entity.BoosterFlags.HasFlag(MatchMemberBoosterFlags.HasUsedEveryDayBooster))
            {
                State |= MatchMemberHistoryState.HasUsedEveryDayBooster;
            }

            if (entity.BoosterFlags.HasFlag(MatchMemberBoosterFlags.HasUsedPrimeTimeBooster))
            {
                State |= MatchMemberHistoryState.HasUsedPrimeTimeBooster;
            }

            //==========================================
            return new MatchMemberHistoryEntity
            {
                MatchId = entity.MatchId,
                PlayerId = entity.PlayerId,
                EloRatingScore = entity.PlayerEntity?.SafeEloRatingScore ?? entity.EloRatingScore,
                EloRatingScorePreview = entity.EloRatingScore,
                PlayerEntity = entity.PlayerEntity,
                TeamId = entity.TeamId,
                MemberId = entity.MemberId,
                WinRate = entity.WinRate,
                Level = entity.TargetWeaponLevel,
                Squad = (short)entity.Squad,
                State = State,
                AchievementEntities = entity.AchievementEntities.Select(MatchMemberAchievementHistoryEntity.Factory).ToList()
            };
        }

        public class Configuration : EntityTypeConfiguration<MatchMemberHistoryEntity>
        {
            public Configuration()
            {
                HasKey(p => p.MemberId);
                ToTable("history.MatchMember");
                HasRequired(p => p.PlayerEntity).WithMany().HasForeignKey(p => p.PlayerId);

                Ignore(p => p.HasUsedPremiumAccount);
                Ignore(p => p.HasNotified);

                HasMany(p => p.AchievementEntities).WithRequired(p => p.MemberEntity).HasForeignKey(p => p.MemberId).WillCascadeOnDelete(true);

            }
        }
    }
}
