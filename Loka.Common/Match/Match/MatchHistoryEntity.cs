﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Server.Infrastructure.Session;

namespace Loka.Common.Match.Match
{
    public class MatchHistoryEntity
    {
        public Guid MatchId { get; set; }
        public string MachineAddress { get; set; }
        public SearchSessionOptions Options { get; set; }

        public DateTime Created { get; set; }
        public DateTime Finished { get; set; }

        public Guid? WinnerTeamId { get; set; }

        public ProcessCreateTargetLevel TargetLevel { get; set; }
        public virtual List<MatchTeamHistoryEntity> TeamList { get; set; } = new List<MatchTeamHistoryEntity>(2);
        public virtual List<MatchMemberHistoryEntity> MemberList { get; set; } = new List<MatchMemberHistoryEntity>(8);

        public virtual List<MatchMemberBotHistoryEntity> BotList { get; set; } = new List<MatchMemberBotHistoryEntity>(8);


        
        public static MatchHistoryEntity Factory(MatchEntity entity)
        {
            return new MatchHistoryEntity
            {
                MatchId = entity.MatchId,
                Created = entity.Created,
                TargetLevel = entity.TargetLevel,
                Finished = entity.Finished ?? DateTime.UtcNow,
                MachineAddress = entity.MachineAddress,
                Options = entity.Options,
                WinnerTeamId = entity.WinnerTeamId,
                TeamList = entity.TeamList.Select(MatchTeamHistoryEntity.Factory).ToList(),
                MemberList = entity.MemberList.Select(MatchMemberHistoryEntity.Factory).ToList(), 
            };
        }


        public class Configuration : EntityTypeConfiguration<MatchHistoryEntity>
        {
            public Configuration()
            {
                HasKey(p => p.MatchId);
                ToTable("history.MatchEntity");
                HasMany(p => p.TeamList).WithRequired(p => p.MatchEntity).HasForeignKey(p => p.MatchId);
                HasMany(p => p.MemberList).WithRequired(p => p.MatchEntity).HasForeignKey(p => p.MatchId);
                HasMany(p => p.BotList).WithRequired(p => p.MatchEntity).HasForeignKey(p => p.MatchId);
            }
        }
    }
}
