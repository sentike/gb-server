﻿using Loka.Common.Match.Team;
using Loka.Server.Player.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRS.Infrastructure;

namespace Loka.Common.Match.Match
{
    public static class MatchEloWrapper
    {
        public static short DefaultEloRatingScore { get; } = 1000;
        public static short DefaultNumberOfPlayers { get; } = 5;

        public static double K { get; } = 32.0;

        // r(1)
        public static double GetTotalRating(this MatchTeamEntity InTeam)
        {
            if (InTeam.MemberList.Any())
            {
                return InTeam.MemberList.Sum(p => p.EloRatingScore);
            }

            //  Если нет игроков, то считает ботов как средних игроков
            return DefaultNumberOfPlayers * DefaultEloRatingScore;
        }

        // R(1)
        public static double GetTransformedRating(this MatchTeamEntity InTeam)
        {
            var TotalRating = InTeam.GetTotalRating();
            var TotalRatingDivRaw = TotalRating / 400.0;
            var ResultRaw = Math.Pow(10.0, TotalRatingDivRaw);
            return ResultRaw;       
        }

        // E(1)
        public static double ExpectedScore(this MatchTeamEntity InTeam, double InEnemyTransformedRating)
        {
            var TransformedRating = InTeam.GetTransformedRating();
            var ResultRaw = TransformedRating / (TransformedRating + InEnemyTransformedRating);
            return ResultRaw;
        }

        //  r'(1)
        public static double GetUpdatedRating(this MatchTeamEntity InTeam, bool IsWinnerTeam, double InEnemyTransformedRating)
        {
            var Score = Convert.ToDouble(IsWinnerTeam);
            var TotalRating = InTeam.GetTotalRating();
            var Expected = InTeam.ExpectedScore(InEnemyTransformedRating);
            var Result = TotalRating + K * (Score - Expected);
            return Result;
        }

        public static double GetFinalRating(this MatchTeamEntity InTeam, bool IsWinnerTeam, double InEnemyTransformedRating)
        {
            var TotalRating = InTeam.GetTotalRating();
            var UpdatedRating = InTeam.GetUpdatedRating(IsWinnerTeam, InEnemyTransformedRating);
            var Result = UpdatedRating - TotalRating;
            return Result;
        }

        public static double GetPlayerPercentRating(this PlayerEntity InPlayer, double InTeamTransformedRating)
        {
            var Rating = InPlayer.EloRatingScore;
            var Div = Rating / InTeamTransformedRating;
            return Div * 100.0;
        }

        public static void UpdateRatings(this MatchTeamEntity InTeam, bool IsWinnerTeam, double InEnemyTransformedRating, double InTeamATotalRating)
        {
            //=============================
            var FinalRating = InTeam.GetFinalRating(IsWinnerTeam, InEnemyTransformedRating);

            //==================================
            PlayerEntity[] Players = InTeam.MemberList.Select(p => p.PlayerEntity).OrderByDescending(p => p.SafeEloRatingScore).ToArray();

            //=============================
            LoggerContainer.MatchMakingLogger.Warn($"[UpdateRatings][{InTeam.TeamId} / IsWinnerTeam: {IsWinnerTeam}][InEnemyTransformedRating: {InEnemyTransformedRating}][InTeamATotalRating: {InTeamATotalRating}][FinalRating: {FinalRating}][Players: {Players.Length}]");

            //==================================
            var RatingTable = Players.Select(p => p.GetPlayerPercentRating(InTeamATotalRating)).ToArray();

            //==================================
            for (int i = 0; i < Players.Length; i++)
            {
                var Player = Players[i];
                double RatingTableVal = 0;
                if (IsWinnerTeam)
                {
                    RatingTableVal = RatingTable[Players.Length - i - 1];
                }
                else
                {
                    RatingTableVal = RatingTable[i];
                }

                var PercentRating = FinalRating * RatingTableVal / 100.0;
                var NewPlayerEloRatingScore = Player.EloRatingScore + Math.Floor(PercentRating);
                LoggerContainer.MatchMakingLogger.Warn($"> [UpdateRatings][{InTeam.TeamId} / IsWinnerTeam: {IsWinnerTeam}][{Player.PlayerName}][{Player.EloRatingScore} -> {NewPlayerEloRatingScore}]");
                Player.EloRatingScore = Convert.ToInt16(NewPlayerEloRatingScore);
            }
        }
    }
}
