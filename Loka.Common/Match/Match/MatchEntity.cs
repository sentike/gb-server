using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Hosting;
using AutoMapper;
using Loka.Common.Cluster;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Infrastructure;

using Loka.Server.Player.Models;
using System.Threading;
using System.Net;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models.Queue;
using VRS.Infrastructure;

namespace Loka.Common.Match.Match
{
    public enum MatchGameState
    {
        Created,
        Started,
        Finished,
        CreatedToStarted,
        FinishedToShutdown,
        Shutdown,
        End
    }

    public class MatchEntity
    {
        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();


        public MatchEntity() { }

        public void InitTeamList(List<MatchTeamEntity> teams)
        {
            TeamList = teams;
            MemberList = teams.SelectMany(m => m.MemberList).ToList();
            Options.Difficulty = 5;

            //= teams.SelectMany(m => m.MemberList)
            //.Select(m => m.DifficultyFloat)
            //.DefaultIfEmpty(1.5)
            //.Average();
        }


        private MatchEntity(List<MatchTeamEntity> teams)
        {
            MatchId = Guid.NewGuid();
            Options = SearchSessionOptions.Default;
            Created = DateTime.UtcNow.AddSeconds(5);
            MachineAddress = ClusterInstance.ThisMachineAddress.ToString();
            State = MatchGameState.Created;

            IsAllowJoinNewPlayers = true;
            InitTeamList(teams);
        }

        public MatchEntity(List<MatchTeamEntity> teams, SearchSessionOptions options)
            : this(teams)
        {
            Options = options;
        }


        public Guid MatchId { get; set; }
        public string MachineAddress { get; set; }
        public IPAddress IpAddress => HostAddressContainer.IpAddressFromCidr(MachineAddress);
        public HostAddressContainer HostAddress => new HostAddressContainer(MachineAddress, MachinePort);

        public virtual ClusterInstance MachineInstance { get; set; }
        public short MachinePort { get; set; }

        public long TargetServerVersion => Options?.TargetServerVersion ?? 0;

        public ProcessCreateTargetLevel TargetLevel { get; set; }
        public SearchSessionOptions Options { get; set; }
        public bool IsAllowJoinNewPlayers { get; set; }


        public DateTime Created { get; set; }
        public DateTime? StartedOfMatch { get; set; }
        public DateTime? StartedOfProcecess { get; set; }
        public DateTime? Finished { get; set; }
        public DateTime? Shutdown { get; set; }
        public DateTime? LastCheckDate { get; set; }
        public DateTime? LastActivityDate { get; set; }

        //public DateTime? LastHeartBeatOnServer { get; set; }
        //public DateTime? LastHeartBeatOnMaster { get; set; }

        public int ShutdownCode { get; set; }
        public int ProcessId { get; set; }
        public int GameVersion { get; set; }

        public MatchGameState State { get; set; }
        public bool IsMatchInProgress => State == MatchGameState.Started || State == MatchGameState.Finished || State == MatchGameState.FinishedToShutdown;
        public bool IsMatchInStopping => State == MatchGameState.Finished || State == MatchGameState.FinishedToShutdown;
        public bool IsMatchInStarting => State == MatchGameState.Created || State == MatchGameState.CreatedToStarted;

        public bool IsServerInOnline => LastActivityDate.HasValue && LastActivityDate.Value.ToElapsedSeconds() < 15;

        public bool IsCompleteState => State == MatchGameState.Finished || State == MatchGameState.FinishedToShutdown || State == MatchGameState.Shutdown;
        public bool IsRunningState => State == MatchGameState.Started || State == MatchGameState.Finished;
        public bool IsRunningStatus => StartedOfMatch.HasValue && LastActivityDate.HasValue && LastActivityDate.Value.ToElapsedSeconds() < 15;

        public GameModeEntity GameModeEntity(DataRepository db)
        {
            return db.GameModeEntity.ToArray().SingleOrDefault(p => p.GameModeFlag.HasFlag(Options.GameMode));
        }

        public bool IsFailedStart =>
            (State == MatchGameState.FinishedToShutdown || State == MatchGameState.Shutdown) &&
            (StartedOfMatch == null ||
             StartedOfProcecess == null ||
             LastActivityDate == null ||
             ShutdownCode > 0 ||
             ProcessId == 0);


        public virtual List<MatchTeamEntity> TeamList { get; set; } = new List<MatchTeamEntity>(2);
        public virtual List<MatchMemberEntity> MemberList { get; set; } = new List<MatchMemberEntity>(32);
        public MatchMemberEntity TakeMember(Guid? id) => MemberList.SingleOrDefault(p => p.MemberId == id);
        public MatchMemberEntity TakePlayer(Guid? id) => MemberList.SingleOrDefault(p => p.PlayerId == id);

        public Guid? WinnerTeamId { get; set; }

        [NotMapped] public int MaxPlayersInMatch { get; set; } = 10;

        public short TimeLeftInSeconds { get; set; }


        public bool IsMbFinished => Created.ToElapsedSeconds() >= Options.RoundTimeInSeconds
                                    || StartedOfMatch != null && StartedOfMatch.Value.ToElapsedSeconds() >= Options.RoundTimeInSeconds
                                    || State == MatchGameState.Finished || State == MatchGameState.FinishedToShutdown
                                    || State == MatchGameState.Shutdown || Finished != null || Shutdown != null
                                    || LastActivityDate != null && LastActivityDate.Value.ToElapsedSeconds() > 15;

        public override string ToString()
        {
            return $"{MatchId} | {Options.GameMode} | {Options.GameMap} | Difficulty: {Options.Difficulty}";
        }

        public class Configuration : EntityTypeConfiguration<MatchEntity>
        {
            public Configuration()
            {
                HasKey(p => p.MatchId);
                ToTable("matches.MatchEntity");
                HasMany(p => p.TeamList).WithRequired(p => p.MatchEntity).HasForeignKey(p => p.MatchId);
                HasMany(p => p.MemberList).WithRequired(p => p.MatchEntity).HasForeignKey(p => p.MatchId);

            }
        }

        public static string ServerRepositoryLocation { get; } = HostingEnvironment.MapPath(@"~/Server/LokaGameServer");
        public static string ServerRootDirectoryLocation(long InVersion) => Path.Combine(ServerRepositoryLocation, /*"branches",*/ $"Server_{InVersion}");
        public static string ServerExecutableLocation(long InVersion) => Path.Combine(ServerRootDirectoryLocation(InVersion), "LOKA_AW", "Binaries", "Win64", "ShooterGameServer.exe");

        public static bool IsExistContent(long InVersion, out Exception exception)
        {
            try
            {
                exception = null;
                var ServerRootDirectory = ServerRootDirectoryLocation(InVersion);
                var bExistRoot = Directory.Exists(ServerRootDirectory);
                var bExistEngine = Directory.Exists(Path.Combine(ServerRootDirectory, "Engine"));
                var bExistGame = Directory.Exists(Path.Combine(ServerRootDirectory, "LOKA_AW"));
                var bExistGameContent = Directory.Exists(Path.Combine(ServerRootDirectory, "LOKA_AW", "Content"));
                var bExistExecutable = File.Exists(ServerExecutableLocation(InVersion));

                return bExistRoot && bExistEngine && bExistGame && bExistGameContent && bExistExecutable;
            }
            catch (Exception e)
            {
                exception = e;
                return false;
            }
        }

        public static bool IsExistContent(long InVersion)
        {
            Exception exception;
            return IsExistContent(InVersion, out exception);
        }

        public void OnStartNode(short port)
        {
            if (State == MatchGameState.CreatedToStarted)
            {
                State = MatchGameState.Started;
                StartedOfMatch = DateTime.UtcNow;
                MachinePort = port;
            }
        }

        public void AssertCheckExistContent()
        {
            Exception exception;
            if (IsExistContent(TargetServerVersion, out exception) == false)
            {
                var sb = new StringBuilder(512);
                sb.AppendLine($"Server content not found! MatchId[{MatchId}]");
                sb.AppendLine($"ServerRootDirectoryLocation: {ServerRootDirectoryLocation(TargetServerVersion)}");
                sb.AppendLine($"ServerExecutableLocation: {ServerExecutableLocation(TargetServerVersion)}");
                throw new DirectoryNotFoundException(sb.ToString(), exception);
            }
        }


        public void OnStartNode(/*out Exception exception*/)
        {
            var process = new Process();

            try
            {
                LoggerContainer.ClusterLogger.Warn($"[OnStartNode] [{MatchId}] Begin | TargetServerVersion: {TargetServerVersion}");

                if (MemberList.Any() == false)
                {
                    throw new NullReferenceException("MemberList was empty!");
                }

                AssertCheckExistContent();
                //-----------------------------------------------------------------------
                //  Prepare options instance startup
                var args = new StringBuilder(256);
                //args.Append($" ?game=TDM Hangar");
                args.Append($" ?ogame={(int)Options.GameMode}?omap={(int)Options.GameMap}");


                args.Append($" -server");
                args.Append($" -NOSTEAM");
                //args.Append($" -multihome={IpAddress}");
                args.Append($" -nodeId={MatchId}");
                args.Append($" -log LOG=\"{Created:MM.dd.yyyy}/LokaGame_{MatchId}.log\"");

                //-----------------------------------------------------------------------
                //  Prepare to run the process parameters
                process.StartInfo = new ProcessStartInfo
                {
                    FileName = ServerExecutableLocation(TargetServerVersion),
                    WorkingDirectory = Path.GetPathRoot(ServerExecutableLocation(TargetServerVersion)) ?? string.Empty,
                    Arguments = args.ToString(),
                    UseShellExecute = false
                };

                //-----------------------------------------------------------------------
                //  Start the the process of and assign the new event for him
                if (process.Start())
                {
                    Thread.Sleep(100);
                    process.Refresh();
                    State = MatchGameState.CreatedToStarted;
                    StartedOfProcecess = DateTime.UtcNow.AddSeconds(5);
                    ProcessId = process.Id;
                    LoggerContainer.ClusterLogger.Trace($"[OnStartNode] {MatchId} was sucsess started. ProcessId {ProcessId} | TargetServerVersion: {TargetServerVersion}");
                }
                else
                {
                    ShutdownCode = 1000002;
                    LoggerContainer.ClusterLogger.Warn($"[OnStartNode] Failed start node {MatchId} | TargetServerVersion: {TargetServerVersion}");
                    State = MatchGameState.Shutdown;
                }
            }
            catch (Exception e)
            {
                try
                {

                    if (process.Id == 0)
                    {
                        State = MatchGameState.Shutdown;
                        ShutdownCode = 1000001;
                    }
                    else
                    {
                        State = MatchGameState.CreatedToStarted;
                        StartedOfProcecess = DateTime.UtcNow.AddSeconds(5);
                        ProcessId = process.Id;
                    }
                }
                catch
                {
                    State = MatchGameState.Shutdown;
                    ShutdownCode = 1000003;
                }

                LoggerContainer.ClusterLogger.WriteExceptionMessage($"[OnStartNode] {MatchId} | ServerExecutableLocation: {ServerExecutableLocation(TargetServerVersion)} | TargetServerVersion: {TargetServerVersion}", e);
            }
        }

        public void OnStopNode()
        {
            try
            {
                if (ProcessId > 0)
                {
                    var process = Process.GetProcesses().FirstOrDefault(p => p.Id == ProcessId);
                    Logger.Info($"!OnStopNode {MatchId}, process {ProcessId} is null: {process == null} | HasExited {process?.HasExited} #1");
                    if (process == null || process.HasExited)
                    {
                        State = MatchGameState.Shutdown;
                        ShutdownCode = process?.ExitCode ?? 0;
                        Shutdown = process?.ExitTime ?? DateTime.UtcNow;
                    }

                    if (process != null && process.ProcessName.Contains("Loka"))
                    {
                        Shutdown = DateTime.UtcNow;
                        process.Kill();
                    }
                }
                else
                {
                    Logger.Info($"!OnStopNode {MatchId}, process {ProcessId} is null, #2");

                    State = MatchGameState.Shutdown;
                    Shutdown = DateTime.UtcNow;
                }
            }
            catch (Exception e)
            {
                Logger.Info($"!OnStopNode {MatchId}, process {ProcessId} is null: {e}, #3");

                State = MatchGameState.Shutdown;
                Shutdown = DateTime.UtcNow;
                Console.WriteLine(e);
            }
        }

        public bool IsAllowJoinPlayer(IList<QueueEntity> queueEntities)
        {
            //  Если одному из участников нельяз войти в бой, Any - вернет true
            //  Функция вернет True, если Any - вернет false
            return queueEntities.Any(p => IsAllowJoinPlayer(p) == false) == false;
        }

        public bool IsAllowJoinPlayer(QueueEntity queueEntity)
        {
            return TakePlayer(queueEntity.QueueId) == null /*|| queueEntity.Level*/;
        }
    }
}