﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;

namespace Loka.Common.Match.Team
{
    public class MatchTeamHistoryEntity
    {
        public Guid TeamId { get; set; }

        public virtual Guid MatchId { get; set; }
        public virtual MatchHistoryEntity MatchEntity { get; set; }
        public virtual List<MatchMemberHistoryEntity> MemberList { get; set; } = new List<MatchMemberHistoryEntity>(4);

        public short Score { get; set; }

        public static MatchTeamHistoryEntity Factory(MatchTeamEntity entity)
        {
            return new MatchTeamHistoryEntity
            {
                MatchId = entity.MatchId,
                TeamId = entity.TeamId,
                Score = entity.Score
            };
        }

        public class Configuration : EntityTypeConfiguration<MatchTeamHistoryEntity>
        {
            public Configuration()
            {
                HasKey(p => p.TeamId);
                ToTable("history.MatchTeam");
                HasMany(p => p.MemberList).WithRequired(p => p.TeamEntity).HasForeignKey(p => p.TeamId);
            }
        }
    }
}
