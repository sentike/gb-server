using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;

namespace Loka.Common.Match.Team
{
    public class MatchTeamEntity
    {
        public Guid TeamId { get; set; }
        public virtual List<MatchMemberEntity> MemberList { get; set; } = new List<MatchMemberEntity>(4);
        public int Lenght => MemberList.Count;

        public virtual Guid MatchId { get; set; }
        public virtual MatchEntity MatchEntity { get; set; }
        public short Score { get; set; }

        public override string ToString()
        {
            return $"{TeamId} | Count: {Lenght} | WinRate: {WinRate}";
        }

        public void MergeMember(MatchTeamEntity to, MatchMemberEntity member)
        {
            to.MemberList.Add(member);
            this.MemberList.Remove(member);
        }

        public void MergeMember(MatchTeamEntity to, List<MatchMemberEntity> member)
        {
            to.MemberList.AddRange(member);
            this.MemberList.RemoveAll(member.Contains);
        }


        [NotMapped]
        public double WinRate => MemberList.Any() ? MemberList.Average(m => m.WinRate) : 0;

        public class Configuration : EntityTypeConfiguration<MatchTeamEntity>
        {
            public Configuration()
            {
                HasKey(p => p.TeamId);
                ToTable("matches.MatchTeam");
                HasMany(p => p.MemberList).WithRequired(p => p.TeamEntity).HasForeignKey(p => p.TeamId);
            }
        }
    }
}