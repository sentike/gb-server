﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using VRS.Models.Payment;
using VRS.Models.User;

namespace VRS.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationDbContext()
            : base("name=VRS.Server")
        {
        }

        public DbSet<ApplicationUserSessionEntity> SessionEntities { get; set; }
        public DbSet<ApplicationUserLogin> UserLogins { get; set; }

        public virtual DbSet<UserPaymentEntity> PaymentEntities { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            base.OnModelCreating(modelBuilder);
            //==============================================================================================================================================================================
            //                                  [ User ]
            //==============================================================================================================================================================================

            // modelBuilder.Configurations.Add(new ApplicationUser.Configuration());
            modelBuilder.Entity<ApplicationUser>().ToTable("vrs.Users");
            modelBuilder.Entity<ApplicationUser>().Property(p => p.FirstName).HasMaxLength(16).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.LastName).HasMaxLength(16).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.MiddleName).HasMaxLength(16).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.UserName).HasMaxLength(64);
            modelBuilder.Entity<ApplicationUser>().Property(p => p.Email).HasMaxLength(64).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.EmailSafeValue).HasMaxLength(64).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.EmailReserveValue).HasMaxLength(64).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.SecretAnswer).HasMaxLength(16).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.PhoneNumber).HasMaxLength(20).IsOptional();

            modelBuilder.Entity<ApplicationUser>().Property(p => p.PhoneNumber).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
            modelBuilder.Entity<ApplicationUser>().Property(p => p.Email).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
            modelBuilder.Entity<ApplicationUser>().Property(p => p.UserName).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
            modelBuilder.Entity<ApplicationUser>().Property(p => p.LastActivityDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            modelBuilder.Entity<ApplicationUser>().Property(p => p.RegistrationDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            modelBuilder.Entity<ApplicationUser>().HasMany(p => p.SessionEntities).WithRequired(p => p.AccountEntity).HasForeignKey(p => p.AccountId).WillCascadeOnDelete(true);

           //modelBuilder.Entity<ApplicationUser>().HasOptional(u => u.PlayerEntity).WithRequired(u => u.User);

            modelBuilder.Entity<ApplicationUserRole>().ToTable("vrs.UserRoles");
            modelBuilder.Entity<ApplicationUserLogin>().ToTable("vrs.UserLogins");
            modelBuilder.Entity<ApplicationUserClaim>().ToTable("vrs.UserClaims");
            modelBuilder.Entity<ApplicationRole>().ToTable("vrs.Roles");

            modelBuilder.Configurations.Add(new ApplicationUserSessionEntity.Configuration());
            modelBuilder.Configurations.Add(new UserPaymentEntity.Configuration());

        }

    }
}
