﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace VRS.Infrastructure.Identity
{
    public class EmailService : IIdentityMessageService
    {
        /// <summary>
        /// Send email message
        /// </summary>
        /// <param name="message"></param>
        /// <returns>Async Task</returns>
        ///<exception cref = "ArgumentException"> description </exception >
        ///<exception cref = "ArgumentNullException"> description </exception >
        ///<exception cref = "FormatException"> description </exception >
        ///<exception cref = "ArgumentOutOfRangeException"> description </exception >
        public Task SendAsync(IdentityMessage message)
        {
            SmtpClient smtpClient = new SmtpClient("smtp.yandex.ru", 25)
            {
                Credentials = new System.Net.NetworkCredential("noreplay@lokagame.com", "lokagame"),
                EnableSsl = true
            };

            MailMessage mail = new MailMessage("noreplay@lokagame.com", message.Destination, message.Subject, message.Body)
            {
                IsBodyHtml = true
            };
            return smtpClient.SendMailAsync(mail);
        }
    }
}
