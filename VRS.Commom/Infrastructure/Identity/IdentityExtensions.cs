﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace VRS.Infrastructure.Identity
{
    public static class IdentityExtensions
    {
        public static Guid GetUserGuid(this IIdentity identity)
        {
            try
            {
                if(identity == null || identity.IsAuthenticated == false) return Guid.Empty;
                return Guid.Parse(Microsoft.AspNet.Identity.IdentityExtensions.GetUserId(identity));
            }
            catch
            {
                return Guid.Empty;
            }
        }

        public static string GetUserClaim(this IIdentity identity, string claimName)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst(claimName);
            return (claim != null) ? claim.Value : string.Empty;
        }
    }
}
