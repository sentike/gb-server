﻿using System.Diagnostics.CodeAnalysis;

namespace VRS.Models.Payment
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum PaymentCurrencyId : byte
    {
        None,
        Rub,    // Российский Рубль
        Eur,    // Евро
        Usd,    // Доллар США
        GBP,    // Фунт стерлингов
        JPY,    // Японская Иена
        CNY,    // Китайский юань
        KRW,    // Южнокорейская вона
        TWD,    // Новый тайваньский доллар
    }
}