﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRS.Models.User;

namespace VRS.Models.Payment
{
    public enum PaymentService : short
    {
        Yandex,
        Cryptonator,
    }

    public enum UserPaymentStatus : byte
    {
        /// <summary>
        /// Новый заказ
        /// </summary>
        Unpaid,

        /// <summary>
        /// Заказ оплачен
        /// </summary>
        Payed,

        /// <summary>
        /// Заказ оплачен, ждет подтверждения
        /// </summary>
        Confirming,

        /// <summary>
        /// Заказ оплачен, но сумам оплаты меньше суммы счета
        /// </summary>
        Mispaid,

        /// <summary>
        /// Заказ оплачен и получен
        /// </summary>
        Completed,

        /// <summary>
        /// Заказ отменен
        /// </summary>
        Canceled,
    }


    public class UserPaymentEntity
    {
        public Guid PaymentId { get; set; }
        public Guid UserId { get; set; }
        public virtual ApplicationUser UserEntity { get; set; }

        public int ItemId { get; set; }
        public Guid? InvoiceId { get; set; }
        //=====================================================
        public long Amount { get; set; }

        public Decimal Cost { get; set; }
        public PaymentCurrencyId CurrencyId { get; set; }

        //=====================================================
        public Decimal TotalCost => Cost * Amount;


        //=====================================================
        public DateTime CreatedDate { get; set; }
        public DateTime? PaymentDate { get; set; }

        //=====================================================
        public PaymentService PaymentService { get; set; }
        public UserPaymentStatus Status { get; set; }

        //=====================================================
        public class Configuration : EntityTypeConfiguration<UserPaymentEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PaymentId);
                ToTable("payment.UserPaymentEntity");
                HasRequired(i => i.UserEntity).WithMany(i => i.PaymentEntities).HasForeignKey(i => i.UserId).WillCascadeOnDelete(true);
            }
        }

        public void ConfirmDelivery()
        {
            Status = UserPaymentStatus.Completed;
        }

        public static Guid ToGuid(long value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes);
        }

        public void Confirm(Guid invoiceId)
        {
            InvoiceId = invoiceId;
            Status = UserPaymentStatus.Payed;
            PaymentDate = DateTime.UtcNow;
        }

        public void Confirm(long invoiceId)
        {
            Confirm(ToGuid(invoiceId));
        }
    }
}
