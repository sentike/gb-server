﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration;

namespace VRS.Models.User
{
    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public class Configuration : EntityTypeConfiguration<ApplicationRole>
        {
            public Configuration()
            {
                ToTable("vrs.Roles");
            }
        }
    }
}
