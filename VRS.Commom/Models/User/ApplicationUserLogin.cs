﻿using System;
using System.Data.Entity.ModelConfiguration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VRS.Models.User
{
    public class ApplicationUserLogin : IdentityUserLogin<Guid>
    {
        //public DateTime RegistrationDate { get; set; }
        //public bool AllowIdentity { get; set; }

        public class Configuration : EntityTypeConfiguration<ApplicationUserLogin>
        {
            public Configuration()
            {
                //HasKey(u => u.UserId);
                ToTable("vrs.UserLogins");
            }
        }
    }
}
