﻿using System;
using System.Data.Entity.ModelConfiguration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VRS.Models.User
{
    public class ApplicationUserRole : IdentityUserRole<Guid>
    {
        //public new virtual Guid UserId
        //{
        //    get { return Guid.Parse(base.UserId); }
        //    set { base.UserId = value.ToString(); }
        //}
        //
        //public new virtual Guid RoleId
        //{
        //    get { return Guid.Parse(base.RoleId); }
        //    set { base.RoleId = value.ToString(); }
        //}

        public class Configuration : EntityTypeConfiguration<ApplicationUserRole>
        {
            public Configuration()
            {
                //HasKey(u => u.UserId);
                ToTable("vrs.UserRoles");
            }
        }
    }
}
