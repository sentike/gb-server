﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using VRS.Infrastructure.Environment;
using VRS.Models.Payment;

namespace VRS.Models.User
{
    public class ApplicationUser : IdentityUser<Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim> 
    {
        public long Balance { get; set; }

        public string SecretAnswer { get; set; }
        public string EmailReserveValue { get; set; }
        public string EmailSafeValue { get; set; }
        public DateTime EmailChangeDate { get; set; }
        public DateTime PhoneChangeDate { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, Guid> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            //userIdentity.AddClaim(new Claim(nameof(Balance), Balance.ToString(), "Money"));

            return userIdentity;
        }

        public override string ToString()
        {
            return $"{Id} | {UserName}";
        }
        //===============================================================================
        //                                  VRS
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        
        public GenderTypeId Gender { get; set; }
        public EIsoCountry Country { get; set; }      
        public LanguageTypeId Language { get; set; }

        public EIsoCountry City { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        /// <summary>
        /// The value is NULL if the player has the full version of the game
        /// </summary>
        public DateTime? SubscribeEndDate { get; set; }
        public DateTime BirthDate { get; set; }

        public virtual List<ApplicationUserSessionEntity> SessionEntities { get; set; } = new List<ApplicationUserSessionEntity>();
        public virtual List<UserPaymentEntity> PaymentEntities { get; set; } = new List<UserPaymentEntity>();
    }
}
