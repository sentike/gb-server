namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPaymentStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("payment.UserPaymentEntity", "Status", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("payment.UserPaymentEntity", "Status");
        }
    }
}
