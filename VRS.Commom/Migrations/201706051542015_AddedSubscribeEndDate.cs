namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSubscribeEndDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("vrs.Users", "SubscribeEndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("vrs.Users", "SubscribeEndDate");
        }
    }
}
