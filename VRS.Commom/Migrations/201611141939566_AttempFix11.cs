namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AttempFix11 : DbMigration
    {
        public override void Up()
        {
            RenameIndex(table: "vrs.Users", name: "UserNameIndex", newName: "IX_UserName");
            CreateIndex("vrs.Users", "RegistrationDate");
            CreateIndex("vrs.Users", "LastActivityDate");
            CreateIndex("vrs.Users", "Email", unique: true);
            CreateIndex("vrs.Users", "PhoneNumber", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("vrs.Users", new[] { "PhoneNumber" });
            DropIndex("vrs.Users", new[] { "Email" });
            DropIndex("vrs.Users", new[] { "LastActivityDate" });
            DropIndex("vrs.Users", new[] { "RegistrationDate" });
            RenameIndex(table: "vrs.Users", name: "IX_UserName", newName: "UserNameIndex");
        }
    }
}
