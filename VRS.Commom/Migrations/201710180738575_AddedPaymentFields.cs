namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPaymentFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("payment.UserPaymentEntity", "Amount", c => c.Long(nullable: false));
            AddColumn("payment.UserPaymentEntity", "PaymentService", c => c.Short(nullable: false));

            DropColumn("payment.UserPaymentEntity", "InvoiceId");

            AddColumn("payment.UserPaymentEntity", "InvoiceId", c => c.Guid());
            CreateIndex("payment.UserPaymentEntity", "InvoiceId", true);

            AlterColumn("payment.UserPaymentEntity", "Cost", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("payment.UserPaymentEntity", "Cost", c => c.Single(nullable: false));
            AlterColumn("payment.UserPaymentEntity", "InvoiceId", c => c.Long());
            DropColumn("payment.UserPaymentEntity", "PaymentService");
            DropColumn("payment.UserPaymentEntity", "Amount");
        }
    }
}
