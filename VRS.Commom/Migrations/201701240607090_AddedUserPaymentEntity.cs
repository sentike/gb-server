namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserPaymentEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "payment.UserPaymentEntity",
                c => new
                    {
                        PaymentId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        ItemId = c.Int(nullable: false),
                        Cost = c.Single(nullable: false),
                        CurrencyId = c.Short(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.PaymentId)
                .ForeignKey("vrs.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            AlterColumn("vrs.Users", "Gender", c => c.Short(nullable: false));
            AlterColumn("vrs.Users", "Country", c => c.Short(nullable: false));
            AlterColumn("vrs.Users", "Language", c => c.Short(nullable: false));
            AlterColumn("vrs.Users", "City", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("payment.UserPaymentEntity", "UserId", "vrs.Users");
            DropIndex("payment.UserPaymentEntity", new[] { "UserId" });
            AlterColumn("vrs.Users", "City", c => c.Int(nullable: false));
            AlterColumn("vrs.Users", "Language", c => c.Int(nullable: false));
            AlterColumn("vrs.Users", "Country", c => c.Int(nullable: false));
            AlterColumn("vrs.Users", "Gender", c => c.Int(nullable: false));
            DropTable("payment.UserPaymentEntity");
        }
    }
}
