namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AttempFix1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("vrs.Users", new[] { "RegistrationDate" });
            DropIndex("vrs.Users", new[] { "LastActivityDate" });
            DropIndex("vrs.Users", new[] { "Email" });
            DropIndex("vrs.Users", new[] { "PhoneNumber" });
            RenameIndex(table: "vrs.Users", name: "IX_UserName", newName: "UserNameIndex");
        }
        
        public override void Down()
        {
            RenameIndex(table: "vrs.Users", name: "UserNameIndex", newName: "IX_UserName");
            CreateIndex("vrs.Users", "PhoneNumber", unique: true);
            CreateIndex("vrs.Users", "Email", unique: true);
            CreateIndex("vrs.Users", "LastActivityDate");
            CreateIndex("vrs.Users", "RegistrationDate");
        }
    }
}
