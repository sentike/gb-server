﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloRating
{
    class Program
    {
        // Function to calculate the Probability 
        static float Probability(float rating1,
                                     float rating2)
        {
            return 1.0f * 1.0f / (1 + 1.0f *
                   (float)(Math.Pow(10, 1.0f *
                     (rating1 - rating2) / 400)));
        }

        // Function to calculate Elo rating 
        // K is a constant. 
        // d determines whether Player A wins or 
        // Player B.  
        static void EloRating(float Ra, float Rb,
                                    int K, bool d)
        {

            // To calculate the Winning 
            // Probability of Player B 
            float Pb = Probability(Ra, Rb);

            // To calculate the Winning 
            // Probability of Player A 
            float Pa = Probability(Rb, Ra);

            // Case -1 When Player A wins 
            // Updating the Elo Ratings 
            if (d == true)
            {
                Ra = Ra + K * (1 - Pa);
                Rb = Rb + K * (0 - Pb);
            }

            // Case -2 When Player B wins 
            // Updating the Elo Ratings 
            else
            {
                Ra = Ra + K * (0 - Pa);
                Rb = Rb + K * (1 - Pb);
            }

            Console.Write("Updated Ratings:-\n");

            Console.Write("Ra = " + (Math.Round(Ra
                         * 1000000.0) / 1000000.0)
                        + " Rb = " + Math.Round(Rb
                         * 1000000.0) / 1000000.0);
        }

        //driver code 
        public static void Main()
        {
            var chars = new HashSet<char>(new[] { 'м', 'р', 'е', 'а', 'с', 'т' });
            foreach (var s in AddAllFrom(chars, 4))
            {
                if (s[0] == 'м' /*&& s[3] == 'о'*/)
                {
                    Console.WriteLine(s);
                }
            }

            Console.ReadKey();


            var TeamA = new Team
            {
                Name= "A",
                IsWinnerTeam = true,
                Players = new Player[]
                {
                    new Player("A", 1000),
                    new Player("B", 1200),
                    new Player("C", 1300),
                }
            };

            var TeamB = new Team
            {
                Name = "B",
                Players = new Player[]
                {
                    new Player("F", 1200),
                    new Player("G", 1300),
                    new Player("H", 1400),
                }
            };

            TeamA.Dump();
            TeamB.Dump();

            //TeamA.UpdateRatings(TeamB.TransformedRating, TeamA.TotalRating);
            TeamB.UpdateRatings(TeamA.TransformedRating, TeamA.TotalRating);

            TeamA.Dump();
            TeamB.Dump();

            Console.ReadKey();

        }

        static IEnumerable<string> AddAllFrom(HashSet<char> chars, int n)
        {
            if (n == 0)
                yield return "";
            foreach (var c in chars.ToList())
            {
                chars.Remove(c);
                foreach (var s in AddAllFrom(chars, n - 1))
                    yield return c + s;
                chars.Add(c);
            }
        }

        public class Team
        {
            public string Name { get; set; }
            public static double K { get; } = 32.0;

            public Player[] Players { get; set; } = new Player[0];

            // r(1)
            public double TotalRating => Players/*.DefaultIfEmpty(new Player(string.Empty, 0))*/.Sum(p => p.Rating);

            // R(1)
            public double TransformedRating
            {
                get
                {
                    var TotalRatingDivRaw = TotalRating / 400.0;
                    var ResultRaw = Math.Pow(10.0, TotalRatingDivRaw);
                    return ResultRaw;
                }
            }

            // E(1)
            public double ExpectedScore(double InEnemyTransformedRating)
            {
                var ResultRaw = TransformedRating / (TransformedRating + InEnemyTransformedRating);
                return ResultRaw;
            }

            public bool IsWinnerTeam { get; set; }

            // S(1)
            public Double Score => Convert.ToDouble(IsWinnerTeam);

            //  r'(1)
            public double GetUpdatedRating(double InEnemyTransformedRating)
            {
                var Expected = ExpectedScore(InEnemyTransformedRating);
                var Result = TotalRating + K * (Score - Expected);
                return Result;
            }

            public double GetFinalRating(double InEnemyTransformedRating)
            {
                var UpdatedRating = GetUpdatedRating(InEnemyTransformedRating);
                var Result = UpdatedRating - TotalRating;
                return Result;
            }

            public void UpdateRatings(double InEnemyTransformedRating, double InTeamATotalRating)
            {
                //=============================
                var FinalRating = GetFinalRating(InEnemyTransformedRating);

                //==================================
                Player[] Members = Players.OrderByDescending(p => p.Rating).ToArray(); ;
      
                var RatingTable = Members.Select(p => p.PercentRating(InTeamATotalRating)).ToArray();

                //==================================
                for (int i = 0; i < Members.Length; i++)
                {
                    var Member = Members[i];
                    double RatingTableVal = 0;
                    if (IsWinnerTeam)
                    {
                        RatingTableVal = RatingTable[Members.Length - i - 1];
                    }
                    else
                    {
                        RatingTableVal = RatingTable[i];
                    }

                    var PercentRating = FinalRating * RatingTableVal / 100.0;
                    Member.Result = Member.Rating + PercentRating;
                }
            }

            public void Dump()
            {
                Console.WriteLine();
                Console.WriteLine($"============== {Name} =================");
                foreach (var p in Players)
                {
                    Console.WriteLine(p.ToString());
                }
            }
        }

        public class Player
        {
            public Player(string n, double r)
            {
                Name = n;
                Rating = r;
            }

            public override string ToString()
            {
                return $"> {Name} | {Rating} | {Result}";
            }

            public string Name { get; set; }
            public double Rating { get; set; }
            public double Result { get; set; }

            public double PercentRating(double InTeamTransformedRating)
            {
                var Div = Rating / InTeamTransformedRating;
                return Div * 100.0;
            }
        }
            

    }
}
