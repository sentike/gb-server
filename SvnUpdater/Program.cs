﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CliWrap;
using CliWrap.Models;

namespace SvnUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            int qq = 0;
            var Output = StartAndWaitProcess(@"C:\TortoiseSVN\bin\svn.exe", $"update \"{ServerRepositoryLocation}\"", out qq);
            Console.WriteLine("Ok");
            Console.WriteLine(Output);
            Console.ReadLine();
        }

        public static string ServerRepositoryLocation { get; } = @"E:\WebSites\GunsOfBoom.Server\Server\LokaGameServer\Server_5";

        static string StartAndWaitProcess(string path, string arguments, out int code)
        {
            //============================================
            code = -1;

            //============================================
            using (var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = path,
                    Arguments = arguments ?? string.Empty,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    //LoadUserProfile = true,
                    //UserName = "Administrator",
                    //Password = UpdaterPassword,
                    //Domain = "WIN-MASTERSRV"
                    Verb = "runas"
                }
            })
            {
                process.OutputDataReceived += (sender, args) => Console.WriteLine($"> {args.Data}");
                process.ErrorDataReceived += (sender, args) => Console.WriteLine($"! {args.Data}");

                //============================================
                if (process.Start())
                {
                    process.BeginErrorReadLine();
                    process.BeginOutputReadLine();

                    //------------------
                    //  Ждем когда обновится информация о процессе
                    Thread.Sleep(1000);

                    //------------------
                    //  Ждем завершения процесса
                    process.WaitForExit();

                    //------------------
                    code = process.ExitCode;

                    //------------------
                    //return process.StandardOutput.ReadToEnd();
                }
            }
            return String.Empty;
        }

    }
}
