﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var matches = 0;
            var matches2 = 0;
            var matches3 = 0;

            for (int i = 0; i < 35; i++)
            {
                matches += MatchesPerScore(500, i);
                matches2 += MatchesPerScore(1000, i);
                matches3 += MatchesPerScore(1500, i);

                Console.WriteLine($"[{i}][0500] -> {1500L + 0500L * i} | {MatchesPerScore(0500, i)} | Total: {matches} | Time: {TimeSpan.FromMinutes(matches * 5)}");
                Console.WriteLine($"[{i}][1000] -> {1500L + 1000L * i} | {MatchesPerScore(1000, i)} | Total: {matches2} | Time: {TimeSpan.FromMinutes(matches2 * 5)}");
                Console.WriteLine($"[{i}][1500] -> {1500L + 1500L * i} | {MatchesPerScore(1500, i)} | Total: {matches3} | Time: {TimeSpan.FromMinutes(matches3  * 5)}\n");

            }
            Console.ReadLine();
        }

        static int MatchesPerScore(int score, int level)
        {
            return 1 * (score / 500) + (1 * score / 500) * level;
        }

    }
}
