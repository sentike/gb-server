﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegexTest
{
    class Program
    {
        static string DiscountSuff { get; } = "_d_";
        static Regex RegexDiscountInProductSKU { get; } = new Regex($"{DiscountSuff}([0-9]*)", RegexOptions.Compiled | RegexOptions.Singleline);

        static void Main(string[] args)
        {
            var names = new[]
            {
                "pr_30",  "pr_10", "pr_30_d_5","pr_30_d_35","pr_30_d_75","pr_30_d_5","pr_30_d_70","pr_30_d_55",
                "cr_30",  "cr_10", "rc_30_d_5","cr_30_d_35","cr_30_d_75","cr_30_d_5","cr_30_d_70","cr_30_d_55",
                "cr_7000",  "cr_7555", "rc_7575_d_5","cr_654654_d_35","cr_987987_d_75","cr_546456_d_5","cr_654654_d_70","cr_66987_d_55",
            };


            Console.WriteLine("Start application");
            foreach(var sku in names)
            {
                var newsku = RemoveDiscountInProductSKU(sku);
                Console.WriteLine($"> sku {sku} -> {newsku}");
            }

            Console.ReadKey();
        }

        public static string RemoveDiscountInProductSKU(string InSku)
        {
            return RegexDiscountInProductSKU.Replace(InSku, string.Empty);
        }
    }
}
