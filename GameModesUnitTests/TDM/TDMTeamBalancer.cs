﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loka.Common.Achievement;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Common.Player;
using Loka.Common.Player.Inventory;
using Loka.Common.Player.Profile.Item;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Store.Abstract;
using Loka.Server.Models.Queue;
using Loka.Server.Player.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameModesUnitTests.TDM
{
    [TestClass]
    public class TDMTeamBalancer
    {
        public int MinGameMapMembers { get; } = 10;


        public QueueEntity FactoryQueueEntity(string Name, short ArmourLevel, int Matches, int Wins)
        {
            //=============================================
            var Player = new PlayerEntity()
            {
                PlayerName = Name,
                AchievementEntities =
                {
                    new PlayerAchievementEntity(AchievementModelId.MatchesTotal, Matches),
                    new PlayerAchievementEntity(AchievementModelId.MatchesWins, Wins),
                },
                Items =
                {
                    PlayerProfileItemEntity.Factory(new PlayerInventoryItemEntity(Guid.Empty, new ItemInstanceEntity() { Level = ArmourLevel }, 1), PlayerProfileInstanceSlotId.Rifle)
                }
            };

            //=============================================
            return new QueueEntity
            {
                PlayerEntity = Player,
                Level = new QueueRange(ArmourLevel)
            };
        }

        [TestMethod]
        public void TeamBalanceOneMembers()
        {
            TeamBalance(new []{FactoryQueueEntity("SeNTike", 1, 15, 9)});
        }

        [TestMethod]
        public void TeamBalanceTwoDiffrentMembers()
        {
            TeamBalance(new[]
            {
                FactoryQueueEntity("SeNTike", 1, 15, 9),
                FactoryQueueEntity("Serinc", 1, 0, 0),
            });
        }

        [TestMethod]
        public void TeamBalanceTwoEqualMembers()
        {
            TeamBalance(new[]
            {
                FactoryQueueEntity("SeNTike", 1, 15, 9),
                FactoryQueueEntity("Serinc", 1, 15, 9),
            });
        }

        [TestMethod]
        public void TeamBalanceThreeDiffrentMembers()
        {
            TeamBalance(new[]
            {
                FactoryQueueEntity("SeNTike", 1, 15, 9),
                FactoryQueueEntity("Prynec", 1, 30, 6),
                FactoryQueueEntity("Serinc", 1, 0, 0),
            });
        }

        [TestMethod]
        public void TeamBalanceThreeEqualMembers()
        {
            TeamBalance(new[]
            {
                FactoryQueueEntity("SeNTike", 1, 15, 9),
                FactoryQueueEntity("Prynec", 1, 15, 9),
                FactoryQueueEntity("Serinc", 1, 15, 9),
            });
        }

        [TestMethod]
        public void TeamBalanceTwoEqualAndOne1Members()
        {
            TeamBalance(new[]
            {
                FactoryQueueEntity("SeNTike", 1, 15, 9),
                FactoryQueueEntity("Prynec", 1, 15, 9),
                FactoryQueueEntity("Serinc", 1, 10, 9),
            });
        }

        [TestMethod]
        public void TeamBalanceTwoEqualAndOne2Members()
        {
            TeamBalance(new[]
            {
                FactoryQueueEntity("SeNTike", 1, 15, 9),
                FactoryQueueEntity("Prynec", 1, 15, 9),
                FactoryQueueEntity("Serinc", 1, 0, 0),
            });
        }

        [TestMethod]
        public void TeamBalanceFourDiffrentMembers()
        {
            TeamBalance(new[]
            {
                FactoryQueueEntity("SeNTike", 1, 15, 9),
                FactoryQueueEntity("Prynec", 1, 30, 6),
                FactoryQueueEntity("Nirozzy", 1, 30, 30),
                FactoryQueueEntity("Serinc", 1, 0, 0),
            });
        }

        [TestMethod]
        public void TeamBalanceTenEqualMembers()
        {
            List<QueueEntity> QueueEntities = new List<QueueEntity>(16);
            for (int i = 1; i <= 10; i++)
            {
                QueueEntities.Add(FactoryQueueEntity($"Player_{i}", 1, 15, 9));
            }
            TeamBalance(QueueEntities.ToArray());
        }

        [TestMethod]
        public void TeamBalanceTenRadomMembers()
        {
            var rnd = new Random();
            List<QueueEntity> QueueEntities = new List<QueueEntity>(16);
            for (int i = 1; i <= 10; i++)
            {
                var matches = rnd.Next(10, 1000);
                var wins = rnd.Next(1, 900);

                if (wins > matches)
                {
                    wins = matches - 1;
                }

                QueueEntities.Add(FactoryQueueEntity($"Player_{i}", 1, matches, wins));
            }
            TeamBalance(QueueEntities.ToArray());
        }

        [TestMethod]
        public void TeamBalanceFourRadomMembers()
        {
            var rnd = new Random();
            List<string> Names = new List<string>();
            List<QueueEntity> QueueEntities = new List<QueueEntity>(16);
            for (int i = 1; i <= 4; i++)
            {
                var matches = rnd.Next(10, 1000);
                var wins = rnd.Next(1, 900);

                if (wins > matches)
                {
                    wins = matches - 1;
                }

                Names.Add($"> {matches} | {wins}");
                QueueEntities.Add(FactoryQueueEntity($"Player_{i}", 1, matches, wins));
            }
            TeamBalance(QueueEntities.ToArray());
        }

        [TestMethod]
        public void TeamBalanceFourBadlyMembers()
        {
            TeamBalance(new[]
            {
                FactoryQueueEntity("SeNTike", 1, 728, 50),
                FactoryQueueEntity("Prynec", 1, 805, 696),
                FactoryQueueEntity("Nirozzy", 1, 316, 315),
                FactoryQueueEntity("Serinc", 1, 967, 681),
            });
        }

        [TestMethod]
        public void TeamBalanceTwoRadomMembers()
        {
            var rnd = new Random();
            List<QueueEntity> QueueEntities = new List<QueueEntity>(16);
            for (int i = 1; i <= 2; i++)
            {
                var matches = rnd.Next(10, 1000);
                var wins = rnd.Next(1, 900);

                if (wins > matches)
                {
                    wins = matches - 1;
                }

                QueueEntities.Add(FactoryQueueEntity($"Player_{i}", 1, matches, wins));
            }
            TeamBalance(QueueEntities.ToArray());
        }

        [TestMethod]
        public void TeamBalanceNineEqualMembers()
        {
            List<QueueEntity> QueueEntities = new List<QueueEntity>(16);
            for (int i = 1; i <= 9; i++)
            {
                QueueEntities.Add(FactoryQueueEntity($"Player_{i}", 1, 15, 9));
            }
            TeamBalance(QueueEntities.ToArray());
        }

        public void TeamBalance(QueueEntity[] members)
        {           
            //==============================================
            var startDateTime = DateTime.UtcNow;
            byte numberOfBots = 0;

            //==============================================
            var team1 = new MatchTeamEntity() { TeamId = Guid.NewGuid() };
            var team2 = new MatchTeamEntity() { TeamId = Guid.NewGuid() };
            var buffer = new MatchTeamEntity() { MemberList = new List<MatchMemberEntity>(256) };

            //==============================================
            //  Adding all members in the first team
            {
                int? squadCount = 0;
                team1.MemberList.AddRange(members.Select(m => FactoryMember(ref squadCount, m)));
            }

            //==============================================
            if (team1.MemberList.Count >= 2)
            {
                int NumItteration = 0;

                var memberCount = team1.MemberList.Count;
                while (true)
                {
                    NumItteration++;

                    if (team1.Lenght == team2.Lenght && team1.Lenght >= MinGameMapMembers / 2)
                        break;

                    if (NumItteration >= 100)
                    {
                        break;
                    }

                    if (memberCount % 2 == 0)
                    {
                        if (team1.Lenght == team2.Lenght && (Math.Abs(team1.WinRate - team2.WinRate) >= 20 || team1.WinRate == team2.WinRate || NumItteration >= 15))
                        {
                            numberOfBots = Convert.ToByte(MinGameMapMembers - (team1.Lenght + team2.Lenght));
                            break;
                        }
                    }
                    else
                    {
                        if(memberCount > 1)
                        { 
                            if (team1.Lenght + team2.Lenght == memberCount && team1.Lenght > 0 && team2.Lenght > 0 && Convert.ToDouble(team2.Lenght) / team1.Lenght > 0.5 && (Math.Abs(team1.WinRate - team2.WinRate) >= 20 || team1.WinRate == team2.WinRate || NumItteration >= 15))
                            {
                                numberOfBots = Convert.ToByte(MinGameMapMembers - (team1.Lenght + team2.Lenght));
                                break;
                            }
                        }
                        else if (team1.Lenght + team2.Lenght == memberCount && (Math.Abs(team1.WinRate - team2.WinRate) >= 20 || team1.WinRate == team2.WinRate || NumItteration >= 15))
                        {
                            numberOfBots = Convert.ToByte(MinGameMapMembers - (team1.Lenght + team2.Lenght));
                            break;
                        }
                    }


                    if (team1.WinRate > team2.WinRate)
                    {
                        //--------------------------------------------
                        //  Получаем информацию о лучшем WinRate
                        var BestWinRateInTeam = team1.MemberList.Any() ? team1.MemberList.Max(p => p.WinRate) : 0;

                        //--------------------------------------------
                        // Merge We reserve on the team players only in the squads
                        if (team1.MemberList.Any(m => m.Squad > 0))
                        {
                            //--------------------------------------------
                            //  Перемещаем всех игроков без отряда в буффер
                            team1.MergeMember(buffer, team1.MemberList.Where(m => m.Squad == 0).ToList());

                            //--------------------------------------------
                            //  Получаем номер отряда с лучшим WinRate
                            var squadId = team1.MemberList.First(s => s.WinRate == BestWinRateInTeam).Squad;

                            //--------------------------------------------
                            //  Переносим участников лучшего отряда во вражескую комманду
                            team1.MergeMember(team2, team1.MemberList.Where(m => m.Squad == squadId).ToList());
                        }
                        else
                        {
                            //  Переносим игрока с лучшим WinRate в другую команду
                            team1.MergeMember(team2, team1.MemberList.First(s => s.WinRate == BestWinRateInTeam));
                        }
                    }
                    else
                    {
                        //--------------------------------------------
                        if (team2.MemberList.Any())
                        {
                            //--------------------------------------------
                            //  Получаем информацию о лучшем WinRate
                            var BestWinRateInTeam = team2.MemberList.Any() ? team2.MemberList.Max(p => p.WinRate) : 0;
                            var MinWinRateInOurTeam = team1.MemberList.Any() ? team1.MemberList.Min(p => p.WinRate) : 0;

                            if (team2.MemberList.Any(m => m.Squad > 0))
                            {
                                //--------------------------------------------
                                //  Получаем номер отряда с лучшим WinRate
                                var squadId = team2.MemberList.First(s => s.WinRate == BestWinRateInTeam).Squad;

                                //--------------------------------------------
                                //  Переносим участников лучшего отряда во вражескую комманду
                                team2.MergeMember(team1, team1.MemberList.Where(m => m.Squad == squadId).ToList());
                            }
                            else
                            {
                                //  Переносим игрока с худшим WinRate в нашу команду
                                var BadPlayers = team1.MemberList.Where(s => s.WinRate == MinWinRateInOurTeam).Take(4).ToList();
                                team1.MergeMember(team2, BadPlayers);


                                //  Переносим игрока с худшим WinRate в нашу команду
                                //var BadPlayer2 = team1.MemberList.FirstOrDefault(s => s.WinRate == MinWinRateInOurTeam);
                                //if (BadPlayer2 != null)
                                //{
                                //    team1.MergeMember(team2, BadPlayer);
                                //}

                                //  Переносим игрока с лучшим WinRate в другую команду
                                var BestPlayer = team2.MemberList.Where(s => s.WinRate == BestWinRateInTeam).Take(3).ToList();
                                //if (BestPlayer != null)
                                {
                                    team2.MergeMember(team1, BestPlayer);
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (buffer.MemberList.Any())
                    {
                        if (team2.Lenght > team1.Lenght)
                        {
                            buffer.MergeMember(team1, buffer.MemberList.First());
                        }
                        else
                        {
                            buffer.MergeMember(team2, buffer.MemberList.First());
                        }
                    }

                    if (team1.Lenght == team2.Lenght && team1.Lenght >= MinGameMapMembers / 2)
                        break;

                    //if (team1.Lenght + team2.Lenght == memberCount && team1.Lenght + team2.Lenght >= MinGameMapMembers && Math.Abs(team1.WinRate - team2.WinRate) >= 30)
                    //{
                    //    numberOfBots = Convert.ToByte(MinGameMapMembers - (team1.Lenght + team2.Lenght));
                    //    break;
                    //}
                }
            }

            //==============================================
            if (members.Length < 2)
            {
                return;
            }

            //==============================================
            if (members.Length % 2 == 0)
            {
                Assert.IsTrue(team1.Lenght == members.Length / 2, $"Несбалансированные команды! #1 | Lenght: {team1.Lenght} vs {team2.Lenght} | WinRate: {team1.WinRate} vs {team2.WinRate}");
            }
            else
            {
                if(Math.Abs(team1.Lenght - team2.Lenght) > 1)
                { 
                    Assert.IsTrue(team1.Lenght == members.Length, $"Несбалансированные команды! #2 | Lenght: {team1.Lenght} vs {team2.Lenght} | WinRate: {team1.WinRate} vs {team2.WinRate}");
                    Assert.IsTrue(Math.Abs(team1.WinRate - team2.WinRate) >= 20, $"Несбалансированные команды! #3 | Lenght: {team1.Lenght} vs {team2.Lenght} | WinRate: {team1.WinRate} vs {team2.WinRate}");

                }
            }

        }

        protected MatchMemberEntity FactoryMember(ref int? squad, QueueEntity entity)
        {
            return MatchMemberEntity.Factory(entity, ref squad, Guid.Empty); ;
        }

    }
}
